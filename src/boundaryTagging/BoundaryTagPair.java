package boundaryTagging;

public class BoundaryTagPair {
	private final String leftTag;
	private final String rightTag;

	private BoundaryTagPair(String leftTag, String rightTag) {
		this.leftTag = leftTag;
		this.rightTag = rightTag;
	}

	public static BoundaryTagPair createBoundaryTagPair(String leftTag, String rightTag) {
		return new BoundaryTagPair(leftTag, rightTag);
	}

	public String getLeftTag() {
		return this.leftTag;
	}

	public String getRightTag() {
		return this.rightTag;
	}
	
	public String getPairString()
	{
		return leftTag +":" + rightTag;
	}
}
