package boundaryTagging;

public class BoundaryTagSet {

	private final BoundaryTagPair sourceInsideTags;
	private final BoundaryTagPair sourceOutsideTags;
	private final BoundaryTagPair targetInsideTags;
	private final BoundaryTagPair targetOutsideTags;

	protected BoundaryTagSet(BoundaryTagPair sourceInsideTags, BoundaryTagPair sourceOutsideTags, BoundaryTagPair targetInsideTags, BoundaryTagPair targetOutsideTags) {
		this.sourceInsideTags = sourceInsideTags;
		this.sourceOutsideTags = sourceOutsideTags;
		this.targetInsideTags = targetInsideTags;
		this.targetOutsideTags = targetOutsideTags;
	}

	public BoundaryTagPair getSourceInsideTags() {
		return this.sourceInsideTags;
	}

	public BoundaryTagPair getSourceOutsideTags() {
		return this.sourceOutsideTags;
	}

	public BoundaryTagPair getTargetInsideTags() {
		return this.targetInsideTags;
	}

	public BoundaryTagPair getTargetOutsideTags() {
		return this.targetOutsideTags;
	}
}
