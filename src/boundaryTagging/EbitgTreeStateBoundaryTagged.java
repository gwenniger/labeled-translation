package boundaryTagging;

import java.util.ArrayList;
import extended_bitg.EbitgInference;
import extended_bitg.EbitgNode;
import extended_bitg.EbitgTreeState;
import extended_bitg.EbitgTreeStateChartBacking;
import extended_bitg.TreeRepresentation;

public class EbitgTreeStateBoundaryTagged extends EbitgTreeState<EbitgLexicalizedInferenceBoundaryTagged, EbitgTreeStateBoundaryTagged> {

	protected EbitgTreeStateBoundaryTagged(EbitgTreeStateChartBacking<EbitgLexicalizedInferenceBoundaryTagged, EbitgTreeStateBoundaryTagged> treeStateChartBacking, EbitgNode node,
			ArrayList<EbitgInference> inferences, EbitgTreeStateBoundaryTagged parent, int parentRelativeChildNumber, int parentRelativePermutationGroup, String nodeID, String nodeLabel,
			boolean hasMoreInferences) {
		super(treeStateChartBacking, node, inferences, parent, parentRelativeChildNumber, parentRelativePermutationGroup, nodeID, nodeLabel, hasMoreInferences);
	}

	@Override
	public EbitgTreeStateBoundaryTagged createEbitgTreeState(EbitgTreeStateChartBacking<EbitgLexicalizedInferenceBoundaryTagged, EbitgTreeStateBoundaryTagged> treeStateChartBacking, EbitgNode node,
			ArrayList<EbitgInference> inferences, EbitgTreeStateBoundaryTagged parent, int parentRelativeChildNumber, int parentRelativePermutationGroup) {
		throw new RuntimeException("Not implemented");
	}

	@Override
	protected void collectInformationForExtraLabels() {
		throw new RuntimeException("Not implemented");
	}

	@Override
	protected TreeRepresentation<EbitgLexicalizedInferenceBoundaryTagged, EbitgTreeStateBoundaryTagged> createTreeRepresentation(EbitgTreeStateBoundaryTagged containingState) {
		// TODO Auto-generated method stub
		return null;
	}

}
