package boundaryTagging;

import util.Span;
import grammarExtraction.boundaryTagLabeledGrammarExtraction.TagPositionsSelector;
import grammarExtraction.sourceEnrichment.TagTripleEnrichedSourceFileCreator;

import java.util.List;

import util.Pair;

public class SpanBoundaryTagger {

	private static final int FIRST_REAL_TAG_OFFSET = 2;
	private final List<String> beginAndEndTagExtendedTagList;
	private final TagPositionsSelector insideTagPositionsSelector;
	private final TagPositionsSelector outsideTagPositionsSelector;

	private SpanBoundaryTagger(List<String> tagList, TagPositionsSelector insideTagPositionsSelector, TagPositionsSelector outsideTagPositionsSelector) {
		this.beginAndEndTagExtendedTagList = tagList;
		this.insideTagPositionsSelector = insideTagPositionsSelector;
		this.outsideTagPositionsSelector = outsideTagPositionsSelector;
	}

	public static SpanBoundaryTagger createSpanBoundaryTagger(List<String> tagList, TagPositionsSelector insideTagPositionsSelector, TagPositionsSelector outsideTagPositionsSelector) {
		return new SpanBoundaryTagger(TagTripleEnrichedSourceFileCreator.createDoubleBeginAndEndTagExtendedTagList(tagList), insideTagPositionsSelector, outsideTagPositionsSelector);
	}

	public BoundaryTagPair getSpanInsideBoundaryTags(Span span) {
		Pair<Integer> insideTagPositions = insideTagPositionsSelector.getTagPositionsForSpan(span);
		if(insideTagPositions == null)
		{
			return null;
		}
		String leftTag = this.beginAndEndTagExtendedTagList.get(insideTagPositions.getFirst() + FIRST_REAL_TAG_OFFSET);
		String rightTag = this.beginAndEndTagExtendedTagList.get(insideTagPositions.getSecond() + FIRST_REAL_TAG_OFFSET);
		return BoundaryTagPair.createBoundaryTagPair(leftTag, rightTag);
	}

	public BoundaryTagPair getSpanOutsideBoundaryTags(Span span) {
		Pair<Integer> outsideTagPositions = outsideTagPositionsSelector.getTagPositionsForSpan(span);
		if(outsideTagPositions == null)
		{
			return null;
		}
		String leftTag = this.beginAndEndTagExtendedTagList.get(outsideTagPositions.getFirst() + FIRST_REAL_TAG_OFFSET);
		String rightTag = this.beginAndEndTagExtendedTagList.get(outsideTagPositions.getSecond() + FIRST_REAL_TAG_OFFSET);
		return BoundaryTagPair.createBoundaryTagPair(leftTag, rightTag);
	}

}
