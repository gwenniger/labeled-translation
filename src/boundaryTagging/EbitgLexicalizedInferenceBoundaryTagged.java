package boundaryTagging;

import java.util.List;
import ccgLabeling.CCGLabels;
import extended_bitg.EbitgChartEntry;
import extended_bitg.EbitgInferenceLexicalization;
import extended_bitg.EbitgInferencePermutationProperties;
import extended_bitg.EbitgInferenceTargetProperties;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgNode;

public class EbitgLexicalizedInferenceBoundaryTagged extends EbitgLexicalizedInference {

	private final BoundaryTagSet boundaryTagSet;

	private EbitgLexicalizedInferenceBoundaryTagged(EbitgInferenceTargetProperties targetProperties, List<EbitgNode> nodes, List<EbitgChartEntry> nodeChartEntries, boolean inverted,
			InferenceType inferenceType, EbitgChartEntry containingChartEntry, EbitgInferenceLexicalization lexicalization, EbitgInferencePermutationProperties permutationProperties,
			CCGLabels ccgLabels, BoundaryTagSet boundaryTagSet) {
		super(targetProperties, nodes, nodeChartEntries, inverted, inferenceType, containingChartEntry, lexicalization, permutationProperties, ccgLabels);
		this.boundaryTagSet = boundaryTagSet;
	}

	public static EbitgLexicalizedInferenceBoundaryTagged createEbitgLexicalizedInferenceBoundaryTagged(EbitgLexicalizedInference lexicalizedInference, BoundaryTagSet boundaryTagSet) {
		return new EbitgLexicalizedInferenceBoundaryTagged(lexicalizedInference.getTargetProperties(), lexicalizedInference.getNodes(), lexicalizedInference.getNodeChartEntries(),
				lexicalizedInference.isInverted(), lexicalizedInference.getInferenceType(), lexicalizedInference.getContainingChartEntry(), lexicalizedInference.getLexicalization(),
				lexicalizedInference.getPermutationProperties(), lexicalizedInference.getCCGLabels(), boundaryTagSet);
	}

	public BoundaryTagPair getSourceInsideTags() {
		return this.boundaryTagSet.getTargetInsideTags();
	}

	public BoundaryTagPair getSourceOutsideTags() {
		return this.boundaryTagSet.getSourceOutsideTags();
	}

	public BoundaryTagPair getTargetInsideTags() {
		return this.boundaryTagSet.getTargetInsideTags();
	}

	public BoundaryTagPair getTargetOutsideTags() {
		return this.boundaryTagSet.getTargetOutsideTags();
	}

}
