package boundaryTagging;

import ccgLabeling.CCGLabeler;
import hat_lexicalization.Lexicalizer;
import extended_bitg.EbitgChart;
import extended_bitg.EbitgChartBuilder;
import extended_bitg.EbitgChartFiller;
import extended_bitg.EbitgChartInitializer;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgLexicalizedInferenceCacher;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.HATComplexityTypeComputer;

public class EbitgChartBuilderBoundaryTagged extends EbitgChartBuilder<EbitgLexicalizedInferenceBoundaryTagged, EbitgTreeStateBoundaryTagged> {

	private final BoundaryTagger boundaryTagger;

	protected EbitgChartBuilderBoundaryTagged(Lexicalizer lexicalizer,
			EbitgLexicalizedInferenceCacher<EbitgLexicalizedInferenceBoundaryTagged> lexicalizedInferenceCacher, EbitgChart theChart,
			EbitgChartInitializer chartInitializer, EbitgChartFiller chartFiller, boolean useCaching,HATComplexityTypeComputer hatComplexityTypeComputer, BoundaryTagger boundaryTagger) {
		super(lexicalizer, lexicalizedInferenceCacher, theChart, chartInitializer, chartFiller, useCaching,hatComplexityTypeComputer);
		this.boundaryTagger = boundaryTagger;
	}

	private static EbitgChartBuilderBoundaryTagged createEbitgChartBuilderBoundaryTagged(String sourceString, String targetString, String alignmentString,
			int maxAllowedInferencesPerNode, boolean useGreedyNullBinding, Lexicalizer lexicalizer, boolean useCaching, BoundaryTagger boundaryTagger) {
		// System.out.println(" EbitgChartBuilderCoreContextLabeled" +
		// lexicalizer.getChartInternalAlignmentChunking());
		EbitgChart theChart = createInitialChart(lexicalizer);
		EbitgChartInitializer chartInitializer = EbitgChartInitializer.createChartInitializer(theChart, lexicalizer, useGreedyNullBinding);
		EbitgChartFiller chartFiller = new EbitgChartFiller(maxAllowedInferencesPerNode, theChart);
		chartInitializer.initializeChart();

		EbitgLexicalizedInferenceCacher<EbitgLexicalizedInferenceBoundaryTagged> lexicalizedInferenceCacher = EbitgLexicalizedInferenceCacher
				.createEbitgLexicalizedInferenceCacher();
		HATComplexityTypeComputer hatComplexityTypeComputer = HATComplexityTypeComputer.createHAComplexityTypeComputer(theChart);
		EbitgChartBuilderBoundaryTagged theBuilder = new EbitgChartBuilderBoundaryTagged(lexicalizer, lexicalizedInferenceCacher, theChart, chartInitializer,
				chartFiller, useCaching,hatComplexityTypeComputer, boundaryTagger);
		theBuilder.checkChartSizeConsistency();
		theBuilder.findDerivationsAlignment();

		return theBuilder;
	}

	public static EbitgChartBuilderBoundaryTagged createEbitgChartBuilderBoundaryTagged(String sourceString, String targetString, String alignmentString,
			int maxAllowedInferencesPerNode, boolean useGreedyNullBinding, boolean useCaching, BoundaryTagger boundaryTagger, CCGLabeler ccgLabeler) {
		Lexicalizer lexicalizer = Lexicalizer.createCompactLexicalizer(alignmentString, sourceString, targetString, ccgLabeler);
		return createEbitgChartBuilderBoundaryTagged(sourceString, targetString, alignmentString, maxAllowedInferencesPerNode, useGreedyNullBinding,
				lexicalizer, useCaching, boundaryTagger);
	}

	@Override
	protected EbitgLexicalizedInferenceBoundaryTagged createLabelEnrichedInferenceFromLexicalizedInference(EbitgLexicalizedInference ebitgLexicalizedInference) {
		if (ebitgLexicalizedInference != null) {
			return boundaryTagger.createEbitgLexicalizedInferenceBoundaryTagged(ebitgLexicalizedInference);
		}
		return null;
	}

	@Override
	public EbitgTreeGrower<EbitgLexicalizedInferenceBoundaryTagged, EbitgTreeStateBoundaryTagged> createEbitgTreeGrower() {
		throw new RuntimeException("Not implemented");
	}

}