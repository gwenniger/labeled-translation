package boundaryTagging;

import java.util.Arrays;
import java.util.List;
import extended_bitg.EbitgLexicalizedInference;
import util.Span;
import grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagLabelingSettings;

public class BoundaryTagger {

	private final SpanBoundaryTagger sourceBoundaryTagger;
	private final SpanBoundaryTagger targetBoundaryTagger;
	private static final String WHITE_SPACE_EXPRESSION = "\\s";

	private BoundaryTagger(SpanBoundaryTagger sourceBoundaryTagger, SpanBoundaryTagger targetBoundaryTagger) {
		this.sourceBoundaryTagger = sourceBoundaryTagger;
		this.targetBoundaryTagger = targetBoundaryTagger;
	}

	public static BoundaryTagger createBoundaryTagger(List<String> sourceTags, List<String> targetTags, BoundaryTagLabelingSettings boundaryTagLabelingSettings) {
		return new BoundaryTagger(SpanBoundaryTagger.createSpanBoundaryTagger(sourceTags, boundaryTagLabelingSettings.getSourceInsideTagPosisionsSelector(),
				boundaryTagLabelingSettings.getSourceOutsideTagPosisionsSelector()), SpanBoundaryTagger.createSpanBoundaryTagger(targetTags,
				boundaryTagLabelingSettings.getTargetInsideTagPosisionsSelector(), boundaryTagLabelingSettings.getTargetOutsideTagPosisionsSelector()));
	}

	public static BoundaryTagger createBoundaryTagger(String sourceTagsString, String targetTagsString, BoundaryTagLabelingSettings boundaryTagLabelingSettings) {
		List<String> sourceTags = Arrays.asList(sourceTagsString.split(WHITE_SPACE_EXPRESSION));
		List<String> targetTags = Arrays.asList(targetTagsString.split(WHITE_SPACE_EXPRESSION));
		return createBoundaryTagger(sourceTags, targetTags, boundaryTagLabelingSettings);
	}

	public EbitgLexicalizedInferenceBoundaryTagged createEbitgLexicalizedInferenceBoundaryTagged(EbitgLexicalizedInference lexicalizedInference) {
		return EbitgLexicalizedInferenceBoundaryTagged.createEbitgLexicalizedInferenceBoundaryTagged(lexicalizedInference, getBoundaryTagSetForLexicalizedInference(lexicalizedInference));
	}

	private BoundaryTagSet getBoundaryTagSetForLexicalizedInference(EbitgLexicalizedInference lexicalizedInference) {
		return new BoundaryTagSet(getSourceInsideBoundaryTags(lexicalizedInference), getSourceOutsideBoundaryTags(lexicalizedInference), getTargetInsideBoundaryTags(lexicalizedInference),
				getTargetOutsideBoundaryTags(lexicalizedInference));
	}

	private BoundaryTagPair getSourceInsideBoundaryTags(EbitgLexicalizedInference lexicalizedInference) {
		Span sourceSpan = lexicalizedInference.getSourceSpan();
		return sourceBoundaryTagger.getSpanInsideBoundaryTags(sourceSpan);
	}

	private BoundaryTagPair getSourceOutsideBoundaryTags(EbitgLexicalizedInference lexicalizedInference) {
		Span sourceSpan = lexicalizedInference.getSourceSpan();
		return sourceBoundaryTagger.getSpanOutsideBoundaryTags(sourceSpan);
	}

	private BoundaryTagPair getTargetInsideBoundaryTags(EbitgLexicalizedInference lexicalizedInference) {
		Span targetSpan = lexicalizedInference.getTargetSpan();
		return targetBoundaryTagger.getSpanInsideBoundaryTags(targetSpan);
	}

	private BoundaryTagPair getTargetOutsideBoundaryTags(EbitgLexicalizedInference lexicalizedInference) {
		Span targetSpan = lexicalizedInference.getTargetSpan();
		return targetBoundaryTagger.getSpanOutsideBoundaryTags(targetSpan);
	}

}
