package artificalCorpusCreation;

import grammarExtraction.chiangGrammarExtraction.GrammarExtractionConstraints;
import grammarExtraction.chiangGrammarExtraction.LabelSideSettings;
import grammarExtraction.grammarExtractionFoundation.GrammarStructureSettings;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.labelSmoothing.LabeledRulesSmoother;
import grammarExtraction.samtGrammarExtraction.SAMTGrammarExtractionConstraints;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import mt_pipeline.mt_config.MTGrammarExtractionConfig;
import reorderingLabeling.FinePhraseCentricReorderingLabelBasic;
import alignment.AlignmentTriple;
import alignment.ArtificialCorpusCreator;
import alignment.ArtificialCorpusProperties;
import alignment.ConfigFileCreater;

public class ArtificialTranslationCorpusCreater extends ArtificialCorpusCreator {

	protected ArtificialTranslationCorpusCreater(List<AlignmentTriple> alignmentTriples, BufferedWriter sourceWriter, BufferedWriter targetWriter, BufferedWriter alignmentWriter,
			BufferedWriter sourceTestFilterWriter, ConfigFileCreater configFileCreater, BufferedWriter targetParseFileWriter, List<String> targetParses,String testCorpusFolderName) {
		super(alignmentTriples, sourceWriter, targetWriter, alignmentWriter, sourceTestFilterWriter, configFileCreater, targetParseFileWriter, targetParses,testCorpusFolderName);
	}

	protected static ArtificialCorpusCreator createArtificialCorpusCreatorTranslation(List<AlignmentTriple> alignmentTriples, List<String> targetParses,
			String testCorpusFolderName, boolean useExtraFeatures,boolean useReorderingLabels) throws IOException {

		ConfigFileCreater configFileCreater = createConfigFileCreaterTranslation(testCorpusFolderName, useExtraFeatures,useReorderingLabels);
		return createArtificialCorpusCreator(configFileCreater, alignmentTriples, targetParses, testCorpusFolderName);
	}
	
	private static ConfigFileCreater createConfigFileCreaterTranslation(String testCorpusFolderName, boolean useExtraFeatures, boolean useReorderingLabels) {

		String extraSamtFeaturesString = "false";
		if (useExtraFeatures) {
			extraSamtFeaturesString = "true";
		}

		ConfigFileCreater configFileCreater = ConfigFileCreater.createStandardConfigFileCreaterWithCasedFiles(testCorpusFolderName);
		configFileCreater.addProperty(MTGrammarExtractionConfig.GRAMMAR_SOURCE_FILTER_PATH_CONFIG_PROPERTY, testCorpusFolderName
				+ GrammarSourceFilterFileName);
		configFileCreater.addProperty(TargetParseProperty, TargetParseFileName);
		configFileCreater.addProperty(SAMTGrammarExtractionConstraints.USE_NON_UNINIFORM_GLUE_RULE_WEIGHTS_PROPERTY, "false");
		configFileCreater.addProperty(SAMTGrammarExtractionConstraints.USE_EXTRA_SAMT_FEATURES_PROPERTY, extraSamtFeaturesString);
		configFileCreater.addProperty(GrammarExtractionConstraints.ALLOW_DANGLING_SECONDNOND_TERMINAL_PROPERTY, "true");
		configFileCreater.addProperty(GrammarExtractionConstraints.ALLOW_CONSECUTIVE_NONTERMINALS_IN_NON_ABSTRACTRULES_PROPERTY, "true");
		configFileCreater.addProperty(GrammarExtractionConstraints.ALLOW_SOURCE_ABSTRACT_PROPERTY, "true");
		configFileCreater.addProperty(GrammarExtractionConstraints.ALLOW_TARGET_WORDS_WITHOUT_SOURCE_PROPERTY, "false");
		configFileCreater.addProperty(GrammarExtractionConstraints.ALLOW_DOUBLE_PLUS_PROPERTY, "true");
		configFileCreater.addProperty(GrammarExtractionConstraints.ALLOW_POS_TAG_LABEL_FALLBACK_PROPERTY, "true");
		configFileCreater.addProperty(GrammarExtractionConstraints.UNARY_CATEGORY_HANDLER_TYPE_PROPERTY, "bottom");
		
		configFileCreater.addProperty(MTGrammarExtractionConfig.USE_SOURCE_SIDE_LABELS_IN_RULE_FILTERING_CONFIG_PROPERTY, "false");
		addReorderingLabelingSettings(configFileCreater, useReorderingLabels);
		configFileCreater.addProperty(LabeledRulesSmoother.STRIP_SOURCE_PART_LABELS_FOR_SMOOTHING_DOUBLE_LABELD_RULES, "true");
		configFileCreater.addProperty(GrammarStructureSettings.WRITE_PLAIN_HIERO_RULES_FOR_SMOOTHING, "false");
		configFileCreater.addProperty(LabelSideSettings.LABEL_SIDE_SETTING_PROPERTY, LabelSideSettings.LABEL_BOTH_SIDES_STRING);
		configFileCreater.addProperty(MTGrammarExtractionConfig.USE_CANONICAL_FORM_LABELED_RULES_PROPERTY, "false");
		configFileCreater.addProperty(SystemIdentity.MAX_SOURCE_AND_TARGET_LENGTH_PROPERTY, ""+10);
		return configFileCreater;
	}
	
	public static void addReorderingLabelingSettings(ConfigFileCreater configFileCreater, boolean useReorderingLabels)
	{
		if(useReorderingLabels)
		{	
			configFileCreater.addProperty(GrammarExtractionConstraints.USE_REORDERING_LABEL_EXTENSION_PROPERTY, "true");
			configFileCreater.addProperty(LabeledRulesSmoother.REORDERING_LABEL_RULE_SMOOTHING_PROPERTY, "false");
			configFileCreater.addProperty(GrammarExtractionConstraints.REORDER_LABEL_PHRASE_PAIRS_PROPERTY, "true");
			configFileCreater.addProperty(LabeledRulesSmoother.LABEL_SMOOTHING_TYPE_PROPERTY, LabeledRulesSmoother.NO_LABEL_SMOOTHING);
		}
		else
		{
			configFileCreater.addProperty(GrammarExtractionConstraints.USE_REORDERING_LABEL_EXTENSION_PROPERTY, "false");
			configFileCreater.addProperty(LabeledRulesSmoother.REORDERING_LABEL_RULE_SMOOTHING_PROPERTY, "false");
			configFileCreater.addProperty(GrammarExtractionConstraints.REORDER_LABEL_PHRASE_PAIRS_PROPERTY, "false");
			configFileCreater.addProperty(LabeledRulesSmoother.LABEL_SMOOTHING_TYPE_PROPERTY, LabeledRulesSmoother.NO_LABEL_SMOOTHING);
		}
		configFileCreater.addProperty(GrammarExtractionConstraints.REORDER_LABEL_TYPE_PROPERTY, FinePhraseCentricReorderingLabelBasic.getLabelTypeName());
		configFileCreater.addProperty(GrammarExtractionConstraints.USE_REORDERING_LABEL_BINARY_FEATURES_PROPERTY, "false");
	}
	
	public static ArtificialCorpusCreator createArtificialCorpusCreatorTranslation(ArtificialCorpusProperties artificialCorpusProperties, String testCorpusFolderName,
			boolean useExtraFeatures,boolean useReorderingLabels) throws IOException {
		List<AlignmentTriple> alignmentTriples = ArtificialCorpusCreator.createAlignemntTriples(artificialCorpusProperties);
		return createArtificialCorpusCreatorTranslation(alignmentTriples, null, testCorpusFolderName, useExtraFeatures,useReorderingLabels);
	}

	
	public static ArtificialCorpusCreator createToyTranslationArtificialCorpusCreator(String testCorpusFolderName,boolean useExtraFeatures) throws IOException {
		List<AlignmentTriple> alignmentTriples = createToyTranslationAlignmentTriples();
		return createArtificialCorpusCreatorTranslation(alignmentTriples, null, testCorpusFolderName,useExtraFeatures,false);
	}

	public static ArtificialCorpusCreator createToyTranslationArtificialCorpusCreatorFrenchEnglish(String testCorpusFolderName, boolean useExtraFeatures) throws IOException {
		List<AlignmentTriple> alignmentTriples = createToyTranslationAlignmentTriplesFrenchEnglish();
		return createArtificialCorpusCreatorTranslation(alignmentTriples, null, testCorpusFolderName,useExtraFeatures,false);
	}
	
	public static ArtificialCorpusCreator createToyTranslationArtificialCorpusCreatorFrenchEnglishCanonicalFormRulesTesting(String testCorpusFolderName, boolean useExtraFeatures, int numCopiesMono, int numCopiesAtomic, int numCopiesHAT) throws IOException {
		List<AlignmentTriple> alignmentTriples = createToyTranslationAlignmentTriplesDutchEnglishMoreMonotoneAtomic(numCopiesMono, numCopiesAtomic, numCopiesHAT);
		return createArtificialCorpusCreatorTranslation(alignmentTriples, null, testCorpusFolderName,useExtraFeatures,true);
	}


	public static ArtificialCorpusCreator createToyTranslationArtificialCorpusCreatorSAMT(String testCorpusFolderName) throws IOException {
		List<AlignmentTriple> alignmentTriples = createToyTranslationAlignmentTriplesSAMT();
		return createArtificialCorpusCreatorTranslation(alignmentTriples, createToyTranslationParsesSAMT(), testCorpusFolderName,true,false);
	}

	public static ArtificialCorpusCreator createToyTranslationArtificialCorpusCreatorSAMT2(String testCorpusFolderName) throws IOException {
		List<AlignmentTriple> alignmentTriples = createToyTranslationAlignmentTriplesSAMT2();
		return createArtificialCorpusCreatorTranslation(alignmentTriples, createToyTranslationParsesSAMT2(), testCorpusFolderName,true,false);
	}

	protected static List<AlignmentTriple> createToyTranslationAlignmentTriples() {
		List<AlignmentTriple> result = new ArrayList<AlignmentTriple>();
		String alignmentString = "0-0 1-1 2-2 3-3 4-4 5-5 6-6";
		String sourceString = "the man walks fast to his car";
		result.add(AlignmentTriple.createAlignmentTriple(sourceString, "de man loopt snel naar zijn auto", alignmentString));
		result.add(AlignmentTriple.createAlignmentTriple(sourceString, "de man wandelt snel naar zijn wagen", alignmentString));
		result.add(AlignmentTriple.createAlignmentTriple(sourceString, "de persoon loopt snel naar zijn auto", "0-0 1-1 2-2 3-3 4-4 5-5 6-6"));
		return result;
	}

	protected static List<AlignmentTriple> createToyTranslationAlignmentTriplesCoreContext() {
		List<AlignmentTriple> result = createToyTranslationAlignmentTriples();
		String alignmentString = "0-0 1-1 2-2 3-3 4-4 5-5 6-6";
		String sourceString = "the man walks fast to his car";
		result.add(AlignmentTriple.createAlignmentTriple(sourceString, "het persoon huppelt rap richting de wagen", alignmentString));
		result.add(AlignmentTriple.createAlignmentTriple(sourceString, "een meneer slentert rap richting zijn hummer", alignmentString));
		return result;
	}

	private static List<AlignmentTriple> createToyTranslationAlignmentTriplesSAMT() {
		List<AlignmentTriple> alignmentTriples = new ArrayList<AlignmentTriple>();
		String alignmentString = "0-0 1-1 2-2 3-3 4-4 5-5 6-6";
		String sourceString = "de man loopt snel naar zijn auto";
		alignmentTriples.add(AlignmentTriple.createAlignmentTriple(sourceString, "the man walks fast to his car", alignmentString));
		alignmentTriples.add(AlignmentTriple.createAlignmentTriple(sourceString, "the man slenders quickly to his car", alignmentString));
		alignmentTriples.add(AlignmentTriple.createAlignmentTriple(sourceString, "the person walks fast to his ride", "0-0 1-1 2-2 3-3 4-4 5-5 6-6"));
		return alignmentTriples;
	}

	private static List<AlignmentTriple> createToyTranslationAlignmentTriplesSAMT2() {
		List<AlignmentTriple> alignmentTriples = new ArrayList<AlignmentTriple>();
		String alignmentString = "0-3 1-4 2-5 4-6 3-7 5-7 6-8 7-9 8-10 9-11 10-12 10-13 11-13 12-13 13-14 15-15 14-16 14-17 16-18";
		String sourceString = "les villes tombent alors en ruines et sont incapables de mener à bien les modernisations nécessaires .";
		String targetString = "this is why these cities fall into disrepair and are unable to carry out the necessary renovation works .";
		alignmentTriples.add(AlignmentTriple.createAlignmentTriple(sourceString, targetString, alignmentString));
		return alignmentTriples;
	}

	private static List<AlignmentTriple> createToyTranslationAlignmentTriplesFrenchEnglish() {
		List<AlignmentTriple> alignmentTriples = new ArrayList<AlignmentTriple>();
		String sourceString1 = "La Traduction automatique n' est pas une tâche facile";
		String targetString1 = "Machine Translation is not an easy task";
		String alignmentString1 = "0-0 0-1 1-0 1-1 2-0 2-1 3-3 4-2 5-3 6-4 7-6 8-5";

		String sourceString2 = "L'étiquettage peut aider";
		String targetString2 = "Labeling can help";
		String alignmentString2 = "0-0 1-1 2-2";

		String sourceString3 = "Cela ne vas pas sans mal";
		String targetString3 = "It doesn't go without pain";
		String alignmentString3 = "0-0 1-1 2-2 3-1 4-3 5-4";

		alignmentTriples.add(AlignmentTriple.createAlignmentTriple(sourceString1, targetString1, alignmentString1));
		alignmentTriples.add(AlignmentTriple.createAlignmentTriple(sourceString2, targetString2, alignmentString2));
		alignmentTriples.add(AlignmentTriple.createAlignmentTriple(sourceString3, targetString3, alignmentString3));
		return alignmentTriples;
	}
	
	private static List<AlignmentTriple> createToyTranslationAlignmentTriplesDutchEnglishMoreMonotoneAtomic(int numCopiesMono, int numCopiesAtomic, int numCopiesHAT) {
		List<AlignmentTriple> alignmentTriples = new ArrayList<AlignmentTriple>();
		// Monotone configuration
		String sourceString1 = "de man loopt";
		String targetString1 = "the man walks";
		String alignmentString1 = "0-0 1-1 2-2";
		
		// Atomic configuration
		String alignmentString2 = "0-0 0-1 0-2 1-0 1-1 1-2 2-0 2-1 2-2";

		// HAT configuration
		String alignmentString3 = "0-0 0-2 1-1 2-0 2-2";
		
		//Add copies of the mono version
		for(int i = 0; i < numCopiesMono; i++)
		{    
		    alignmentTriples.add(AlignmentTriple.createAlignmentTriple(sourceString1, targetString1, alignmentString1));
		}
		// Add copies of the atomic version
		for(int i = 0; i < numCopiesAtomic; i++)
		{    
		    alignmentTriples.add(AlignmentTriple.createAlignmentTriple(sourceString1, targetString1, alignmentString2));
		}    
		
		// Add copies of the HAT version
		for(int i = 0; i < numCopiesHAT; i++)
		{    
		    alignmentTriples.add(AlignmentTriple.createAlignmentTriple(sourceString1, targetString1, alignmentString3));
		}   
		
		 alignmentTriples.add(AlignmentTriple.createAlignmentTriple(sourceString1, "the person moves", alignmentString3));
		
		return alignmentTriples;
	}

	private static List<String> createToyTranslationParsesSAMT2() {
		List<String> targetParses = new ArrayList<String>();
		targetParses
				.add("(S (NP (DT this)) (VP (VBZ is) (SBAR (WHADVP (WRB why)) (S (NP (DT these) (NNS cities)) (VP (VP (VBP fall) (PP (IN into) (NP (NN disrepair)))) (CC and) (VP (VBP are) (ADJP (JJ unable) (S (VP (TO to) (VP (VB carry) (PRT (RP out)) (NP (DT the) (JJ necessary) (NN renovation) (NNS works))))))))))) (. .))");

		return targetParses;
	}

	private static List<String> createToyTranslationParsesSAMT() {
		List<String> targetParses = new ArrayList<String>();
		// targetParses.add("( (S (NP (DT the) (NN man)) (VP (VBZ walks) (ADVP (RB fast)) (PP (TO to) (NP (PRP$ his) (NN car)))))");
		// targetParses.add("( (S (NP (DT the) (NN man)) (VP (VBZ slenders) (ADVP (RB quickly)) (PP (TO to) (NP (PRP$ his) (NN car))))))");
		// targetParses.add("(S (NP (DT the) (NN person)) (VP (VBZ walks) (ADVP (RB fast)) (PP (TO to) (NP (PRP$ his) (NN ride))))))");
		targetParses.add("(ROOT  (S(NP (DT the) (NN man))(VP (VBZ walks)(ADVP (RB fast))(PP (TO to)(NP (PRP$ his) (NN car))))))");
		targetParses.add("(ROOT(S(NP (DT the) (NN man))(VP (VBZ slenders)(ADVP (RB quickly))(PP (TO to)(NP (PRP$ his) (NN car))))))");
		targetParses.add("(ROOT(S(NP (DT the) (NN person))(VP (VBZ walks)(ADVP (RB fast))(PP (TO to)(NP (PRP$ his) (NN ride))))))");

		return targetParses;
	}

}
