package artificalCorpusCreation;

import grammarExtraction.chiangGrammarExtraction.LabelSideSettings;
import grammarExtraction.grammarExtractionFoundation.GrammarStructureSettings;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.minimalPhrasePairExtraction.MultiThreadGrammarExtractorMinimalPhrasePairs;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

import mt_pipeline.mt_config.MTDecoderConfig;
import mt_pipeline.mt_config.MTGrammarExtractionConfig;
import alignment.AlignmentTriple;
import alignment.ArtificialCorpusCreator;
import alignment.ConfigFileCreater;
import coreContextLabeling.CoreContextLabelsTable;
import coreContextLabeling.EbitgChartBuilderCoreContextLabeled;

public class ArtificialCoreContextCorpusCreater extends ArtificialTranslationCorpusCreater {
	private static String CoreContextLabelTableFileName = "coreContextLabelTable.serialized";
	public static String MINIMAL_PHRASE_PAIR_GRAMMAR_NAME = "minimalPhrasePairGrammar.txt";

	protected ArtificialCoreContextCorpusCreater(List<AlignmentTriple> alignmentTriples, BufferedWriter sourceWriter, BufferedWriter targetWriter, BufferedWriter alignmentWriter,
			BufferedWriter sourceTestFilterWriter, ConfigFileCreater configFileCreater, BufferedWriter targetParseFileWriter, List<String> targetParses, String testCorpusFolderName) {
		super(alignmentTriples, sourceWriter, targetWriter, alignmentWriter, sourceTestFilterWriter, configFileCreater, targetParseFileWriter, targetParses, testCorpusFolderName);
	}

	public static ArtificialCorpusCreator createToyCoreContextArtificialCorpusCreator(String testCorpusFolderName) throws IOException {
		List<AlignmentTriple> alignmentTriples = createToyTranslationAlignmentTriplesCoreContext();
		return createArtificialCorpusCreator(createConfigFileCreater(testCorpusFolderName), alignmentTriples, null, testCorpusFolderName);
	}

	private static ConfigFileCreater createConfigFileCreater(String testCorpusFolderName) {
		ConfigFileCreater configFileCreater = ConfigFileCreater.createStandardConfigFileCreaterWithCasedFiles(testCorpusFolderName);
		configFileCreater.addProperty(MTGrammarExtractionConfig.GRAMMAR_SOURCE_FILTER_PATH_CONFIG_PROPERTY, testCorpusFolderName + ArtificialCorpusCreator.GrammarSourceFilterFileName);
		configFileCreater.addProperty(EbitgChartBuilderCoreContextLabeled.CoreContextLabelTableFileNameConfigurationProperty, getCoreContextTableSerializationFilePath(testCorpusFolderName));
		configFileCreater.addProperty(MultiThreadGrammarExtractorMinimalPhrasePairs.MinimalPhrasePairTableFileNameConfigurationProperty, MINIMAL_PHRASE_PAIR_GRAMMAR_NAME);
		configFileCreater.addProperty(CoreContextLabelsTable.CoreContextLabelProbabilitySettingProperty, CoreContextLabelsTable.CoreContextLabelSourceTargetProbabilitySetting);
		configFileCreater.addProperty(CoreContextLabelsTable.RelativeEntropyWeightCoreContextLabelsProperty, "0.5");
		configFileCreater.addProperty(LabelSideSettings.LABEL_SIDE_SETTING_PROPERTY, LabelSideSettings.LABEL_BOTH_SIDES_STRING);
		configFileCreater.addProperty(MTGrammarExtractionConfig.USE_CANONICAL_FORM_LABELED_RULES_PROPERTY,"false");
		configFileCreater.addProperty(GrammarStructureSettings.WRITE_PLAIN_HIERO_RULES_FOR_SMOOTHING,"false");
		configFileCreater.addProperty(SystemIdentity.MAX_SOURCE_AND_TARGET_LENGTH_PROPERTY,""+SystemIdentity.STANDARD_MAX_SOURCE_AND_TARGET_LENGTH);
		return configFileCreater;
	}

	public static String getCoreContextTableSerializationFilePath(String testCorpusFolderName) {
		return testCorpusFolderName + CoreContextLabelTableFileName;
	}

}
