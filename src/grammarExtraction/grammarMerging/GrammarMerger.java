package grammarExtraction.grammarMerging;

import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;
import grammarExtraction.translationRuleDiskTables.MultiThreadSort;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import util.ConfigFile;
import util.FileStatistics;
import util.FileUtil;
import util.Pair;

public class GrammarMerger {

    private static final String TEMP_MERGED_GRAMMAR_FILE_PATH_PROPERTY = "tempAllGrammarsMergedFilePath";
    private static final String NUM_SORTING_THREADS_PROPERTY = "numSortingThreads";
    private static String GRAMMAR_FILE_PATHS_PROPERTY = "grammarFilePaths";
    private static String RESULT_GRAMMAR_FILE_PATH_PROPERTY = "resultFilePath";
    private static String SORTED_SUFFIX = ".sorted";
    private static String TEMP_SORTING_WORK_DIRECTORY_NAME = "tempSortingWorkDir";
    private final List<GrammarReader> grammarReaders;
    private final String tempAllGrammarsMergedFileName;
    private final String tempAllGrammarsMergedSortedFileName;
    private final Writer temAllGrammarsMergedFileWriter;
    private final Writer resultGrammarOutputWriter;
    private final int numSortingThreads;

    private GrammarMerger(List<GrammarReader> grammarReaders, String tempAllGrammarsMergedFileName,
	    String tempAllGrammarsMergedSortedFileName, Writer temAllGrammarsMergedFileWriter,
	    Writer resultGrammarOutputWriter,int numSortingThreads) {
	this.grammarReaders = grammarReaders;
	this.tempAllGrammarsMergedFileName = tempAllGrammarsMergedFileName;
	this.tempAllGrammarsMergedSortedFileName = tempAllGrammarsMergedSortedFileName;
	this.temAllGrammarsMergedFileWriter = temAllGrammarsMergedFileWriter;
	this.resultGrammarOutputWriter = resultGrammarOutputWriter;
	this.numSortingThreads = numSortingThreads;
    }

    private static List<GrammarReader> createGrammarReaders(List<String> grammarFilePaths) {
	List<GrammarReader> result = new ArrayList<GrammarReader>();
	for (String grammarFilePath : grammarFilePaths) {
	    result.add(GrammarReader.createGrammarReader(grammarFilePath));
	}
	return result;
    }

    private static String getTempAllGrammarsMergedSortedFileName(
	    String tempAllGrammarsMergedFileName) {
	return tempAllGrammarsMergedFileName + SORTED_SUFFIX;
    }

    public static GrammarMerger createGrammarMerger(List<String> grammarFilePaths,
	    String tempAllGrammarsMergedFileName, String grammarOutputFilePath,int numSortingThreads) {
	try {
	    return new GrammarMerger(createGrammarReaders(grammarFilePaths),
		    tempAllGrammarsMergedFileName,
		    getTempAllGrammarsMergedSortedFileName(tempAllGrammarsMergedFileName),
		    new BufferedWriter(new FileWriter(tempAllGrammarsMergedFileName)),
		    new BufferedWriter(new FileWriter(grammarOutputFilePath)),numSortingThreads);
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private static List<String> getGrammarFilePathsFromConfig(ConfigFile theConfig) {
	return theConfig.getMultiValueParmeterAsListWithPresenceCheck(GRAMMAR_FILE_PATHS_PROPERTY);
    }

    private static String getTempMergedGrammarsFilePathFromConfig(ConfigFile theConfig) {
	return theConfig.getValueWithPresenceCheck(TEMP_MERGED_GRAMMAR_FILE_PATH_PROPERTY);
    }

    private static String getResultGrammarFilePathFromConfig(ConfigFile theConfig) {
	return theConfig.getValueWithPresenceCheck(RESULT_GRAMMAR_FILE_PATH_PROPERTY);
    }
    
    public static int getNumSortingThreads(ConfigFile theConfig) {
	return Integer.parseInt(theConfig.getValueWithPresenceCheck(NUM_SORTING_THREADS_PROPERTY));
    }

    public static GrammarMerger createGrammarMergerFromConfigFile(String configFilePath) {
	try {
	    ConfigFile theConfig = new ConfigFile(configFilePath);
	    return createGrammarMerger(getGrammarFilePathsFromConfig(theConfig),
		    getTempMergedGrammarsFilePathFromConfig(theConfig),
		    getResultGrammarFilePathFromConfig(theConfig),getNumSortingThreads(theConfig));
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    /**
     * This method will add all the rules (with a rule identity not yet present)
     * from the GrammarReader to the map of ruleIdentity->ruleWeights tuples
     * 
     * @param grammarReader
     * @throws IOException
     */
    private void addRulesFromGrammarReader(GrammarReader grammarReader) throws IOException {
	int ruleNumber = 0;
	while (grammarReader.hasMoreRuleIdentityRuleWeightPairs()) {

	    if ((ruleNumber % 10000) == 0) {
		System.out.println("GrammarReader: Added " + ruleNumber + " rules to temporary merged grammar");
	    }

	    Pair<String> ruleIdentityRuleWeightPair = grammarReader
		    .getNextRuleIdentityRuleWeightsPair();

	    String ruleIdentity = ruleIdentityRuleWeightPair.getFirst();
	    String ruleWeights = ruleIdentityRuleWeightPair.getSecond();
	    String ruleString = reconstructRule(ruleIdentity, ruleWeights);

	    this.temAllGrammarsMergedFileWriter.write(ruleString + "\n");

	    ruleNumber++;
	}
    }

    private void collectRulesFromAllGrammarReaders() throws IOException {
	for (GrammarReader grammarReader : grammarReaders) {
	    addRulesFromGrammarReader(grammarReader);
	}
	FileUtil.closeCloseableIfNotNull(temAllGrammarsMergedFileWriter);
    }

    private String getTempSortingDirectory() {
	return "./" + TEMP_SORTING_WORK_DIRECTORY_NAME;
    }

    private void sortMergedGrammarsFile() {
	FileUtil.createFolderIfNotExisting(getTempSortingDirectory());
	MultiThreadSort multiThreadSort = MultiThreadSort.createMultiThreadSort(numSortingThreads,
		tempAllGrammarsMergedFileName, tempAllGrammarsMergedSortedFileName,
		getTempSortingDirectory());
	multiThreadSort.sortLexicographically();
    }

    private String reconstructRule(String ruleIdentity, String ruleWeights) {
	return ruleIdentity + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR + ruleWeights;
    }

    private void writeSortedRulesList(Writer outputWriter) throws IOException {

	LineIterator it = FileUtils.lineIterator(
		new File(this.tempAllGrammarsMergedSortedFileName), "UTF-8");

	String previousRuleIdentity = null;
	
	int numRulesWritten = 0;
	int numDuplicatesDiscarded = 0;

	try {

	    // TODO fixme!!! -
	    // Loop over the rule lines in the sorted grammar and keep only the
	    // first copy of rules with the same
	    // rule identity and different weights

	    while (it.hasNext()) {
		String ruleline = it.nextLine();
		String ruleIdentity = JoshuaStyle.getRuleIdentity(ruleline);

		// We only keep the first of every appearing rule identity, and
		// throw away the other
		// duplicates, with possibly different weights
		if (!ruleIdentity.equals(previousRuleIdentity)) {
		    outputWriter.write(ruleline + "\n");
		    previousRuleIdentity = ruleIdentity;
		    numRulesWritten++;
		}
		else
		{
		    System.out.println("Found duplicate for rule Identity " + ruleIdentity + " -- skipping");
		    numDuplicatesDiscarded++;
		}
		// do something with line
	    }
	} finally {
	    it.close();
	}
	System.out.println(" ########### Grammar Merger Summary ###########");
	System.out.println("Written " + numRulesWritten + " unique rules");
	System.out.println("Discarded " + numDuplicatesDiscarded + " duplicate rules");
    }

    public void computeAndWriteMergedGrammar() {
	try {
	    collectRulesFromAllGrammarReaders();
	    sortMergedGrammarsFile();
	    writeSortedRulesList(resultGrammarOutputWriter);
	} catch (IOException e) {
	    e.printStackTrace();
	    FileUtil.closeCloseableIfNotNull(resultGrammarOutputWriter);
	    throw new RuntimeException(e);
	}

	FileUtil.closeCloseableIfNotNull(resultGrammarOutputWriter);

    }

    public static void main(String[] args) {
	if (args.length != 1) {
	    System.out.println("Usage grammarMerger CONFIG_FILE_PATH");
	    System.exit(1);
	}
	GrammarMerger grammarMerger = GrammarMerger.createGrammarMergerFromConfigFile(args[0]);
	grammarMerger.computeAndWriteMergedGrammar();
    }

}
