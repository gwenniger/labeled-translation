package grammarExtraction.grammarMerging;

import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.LineIterator;

import util.Pair;

public class GrammarReader {

    private final LineIterator ruleLineIterator;

    private GrammarReader(LineIterator ruleLineIterator) {
	this.ruleLineIterator = ruleLineIterator;
    }

    public static GrammarReader createGrammarReader(String filePath) {
	try {
	    return new GrammarReader(org.apache.commons.io.FileUtils.lineIterator(
		    new File(filePath), "UTF8"));
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private List<String> getRuleParts(String ruleLine) {
	return Arrays.asList(JoshuaStyle.spitOnRuleSeparator(ruleLine));
    }

    private String getRuleWeights(List<String> ruleParts) {
	return ruleParts.get(ruleParts.size() - 1);
    }

    private String getRuleIdentity(List<String> ruleParts) {
	String result = ruleParts.get(0);

	for (int i = 1; i < ruleParts.size() - 1; i++) {
	    result += JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR + ruleParts.get(i);
	}
	return result;
    }

    public boolean hasMoreRuleIdentityRuleWeightPairs() {
	return this.ruleLineIterator.hasNext();
    }

    public Pair<String> getNextRuleIdentityRuleWeightsPair() {
	String ruleLine = ruleLineIterator.nextLine();

	List<String> ruleParts = getRuleParts(ruleLine);
	String ruleIndentity = getRuleIdentity(ruleParts);
	String ruleWeights = getRuleWeights(ruleParts);
	return new Pair<String>(ruleIndentity, ruleWeights);
    }

}
