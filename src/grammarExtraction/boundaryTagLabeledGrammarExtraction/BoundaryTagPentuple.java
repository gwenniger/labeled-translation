package grammarExtraction.boundaryTagLabeledGrammarExtraction;

import alignment.AlignmentStringTriple;

public class BoundaryTagPentuple extends AlignmentStringTriple {

	private final String sourceTagsString;
	private final String targetTagsString;

	protected BoundaryTagPentuple(String sourceString, String targetString, String alignmentString, String sourceTagsString, String targetTagsString) {
		super(sourceString, targetString, alignmentString);
		this.sourceTagsString = sourceTagsString;
		this.targetTagsString = targetTagsString;
	}

	public static BoundaryTagPentuple createBoundaryTagsPentuple(String sourceString, String targetString, String alignmentString, String sourceTagsString, String targetTagsString) {

		sourceString = sourceString.trim();
		targetString = targetString.trim();
		alignmentString = alignmentString.trim();
		sourceTagsString = sourceTagsString.trim();
		targetTagsString = targetTagsString.trim();
		return new BoundaryTagPentuple(sourceString, targetString, alignmentString, sourceTagsString, targetTagsString);
	}

	public String getSourceTagsString() {
		return this.sourceTagsString;
	}

	public String getTargetTagsString() {
		return this.targetTagsString;
	}
}
