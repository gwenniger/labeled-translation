package grammarExtraction.boundaryTagLabeledGrammarExtraction;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;

import alignmentStatistics.InputStringsEnumeration;

public class BoundaryTagFilterPairEnumeration extends InputStringsEnumeration<BoundaryTagFilterPair> {

	protected BoundaryTagFilterPairEnumeration(List<BufferedReader> inputReaders) {
		super(inputReaders);
	}

	public static BoundaryTagFilterPairEnumeration createBoundaryTagFilterPairEnumeration(String sourceFileName, String sourceTagFileName) {
		try {
			BufferedReader sourceReader = new BufferedReader(new FileReader(sourceFileName));
			BufferedReader sourceTagReader = new BufferedReader(new FileReader(sourceTagFileName));

			List<BufferedReader> inputReaders = Arrays.asList(sourceReader, sourceTagReader);
			return new BoundaryTagFilterPairEnumeration(inputReaders);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	@Override
	protected BoundaryTagFilterPair getNextT(List<String> nextInputStrings) {
		return BoundaryTagFilterPair.createBoundaryTagFilterPair(nextInputStrings.get(0), nextInputStrings.get(1));
	}

	@Override
	protected boolean inputStringsAreAcceptable(List<String> nextInputStrings) {
		return true;
	}

}
