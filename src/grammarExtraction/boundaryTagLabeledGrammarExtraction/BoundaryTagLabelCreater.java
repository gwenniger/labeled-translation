package grammarExtraction.boundaryTagLabeledGrammarExtraction;

import junit.framework.Assert;
import boundaryTagging.BoundaryTagPair;
import boundaryTagging.EbitgLexicalizedInferenceBoundaryTagged;
import util.ConfigFile;
import util.XMLTagging;
import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;
import grammarExtraction.translationRules.LightWeightTranslationRule;

public class BoundaryTagLabelCreater {

	private static final String SOURCE_INSIDE_LABEL = "SI";
	private static final String SOURCE_OUTSIDE_LABEL = "SO";
	private static final String TARGET_INSIDE_LABEL = "TI";
	private static final String TARGET_OUTSIDE_LABEL = "TO";

	private final BoundaryTagLabelingSettings boundaryTagLablingLabelingSettings;

	private BoundaryTagLabelCreater(BoundaryTagLabelingSettings boundaryTagLablingLabelingSettings) {
		this.boundaryTagLablingLabelingSettings = boundaryTagLablingLabelingSettings;
	}

	public static BoundaryTagLabelCreater createBoundaryTagLabelCreater(ConfigFile theConfig) {
		return new BoundaryTagLabelCreater(BoundaryTagLabelingSettings.createBoundaryTagLabelingSettingsFromConfig(theConfig));
	}

	private String createSourceInsideLabel(BoundaryTagPair sourceInsideTags) {
		return XMLTagging.getTagSandwidchedString(SOURCE_INSIDE_LABEL, sourceInsideTags.getPairString());
	}

	private String createSourceInsideLabel(EbitgLexicalizedInferenceBoundaryTagged inference) {
		return createSourceInsideLabel(inference.getSourceInsideTags());
	}

	private String createSourceOutsideLabel(BoundaryTagPair sourceOutsideTags) {
		return XMLTagging.getTagSandwidchedString(SOURCE_OUTSIDE_LABEL, sourceOutsideTags.getPairString());
	}

	private String createSourceOutsideLabel(EbitgLexicalizedInferenceBoundaryTagged inference) {
		return createSourceOutsideLabel(inference.getSourceOutsideTags());
	}

	/**
	 * Whether we will do description wrapping. Only done when both the target inside tags and target outside tags are used.
	 * Otherwise these tags are omitted to increase readability of the produced grammar. 
	 * @return
	 */
	private boolean useInsideOutsideDescriptionWrappingTarget(){
	    return this.boundaryTagLablingLabelingSettings.useTargetInsideTags() && this.boundaryTagLablingLabelingSettings.useTargetOutsideTags();
	}
	
	private String createTargetInsideLabel(BoundaryTagPair targetInsideTags) {
	    if(useInsideOutsideDescriptionWrappingTarget()){
		return XMLTagging.getTagSandwidchedString(TARGET_INSIDE_LABEL, targetInsideTags.getPairString());
	    }
	    else{
		return targetInsideTags.getPairString();
	    }
	}

	private String createTargetInsideLabel(EbitgLexicalizedInferenceBoundaryTagged inference) {
		return createTargetInsideLabel(inference.getTargetInsideTags());
	}

	private String createTargetOutsideLabel(BoundaryTagPair targetOutsideTags) {
	    if(useInsideOutsideDescriptionWrappingTarget()){
		return XMLTagging.getTagSandwidchedString(TARGET_OUTSIDE_LABEL, targetOutsideTags.getPairString());
	    }
	    else{
		return targetOutsideTags.getPairString();
	    }
	}

	private String createTargetOutsideLabel(EbitgLexicalizedInferenceBoundaryTagged inference) {
		return createTargetOutsideLabel(inference.getTargetOutsideTags());
	}

	private final void checkAtLeastOneTypeTagsUsed() {
		String errorString = "No tag labels used in BoundaryTagLabelRuleGapCreater. At least one of the following properties must be set to true in the configuration file: ";
		for (String tagUsageProperty : BoundaryTagLabelingSettings.TAG_USAGE_PROPERTIES_NAMES_PROPERTIES_LIST) {
			errorString += "\n" + tagUsageProperty;
		}

		if (!this.boundaryTagLablingLabelingSettings.useAnyTagLabels()) {
			throw new RuntimeException(errorString);
		}
	}

	public String createSourceLabels(BoundaryTagPair sourceInsideLabels, BoundaryTagPair sourceOutsideLabels) {

		String sourceLabels = "";
		if (this.boundaryTagLablingLabelingSettings.useSourceInsideTags()) {
			Assert.assertNotNull(sourceInsideLabels);
			sourceLabels += createSourceInsideLabel(sourceInsideLabels);
		}
		if (this.boundaryTagLablingLabelingSettings.useSourceOutsideTags()) {
			Assert.assertNotNull(sourceOutsideLabels);
			sourceLabels += createSourceOutsideLabel(sourceOutsideLabels);
		}
		sourceLabels = XMLTagging.getTagSandwidchedString(LightWeightTranslationRule.SOURCE_LABELS, sourceLabels);
		return sourceLabels;
	}

	private String createSourceLabels(EbitgLexicalizedInferenceBoundaryTagged inference, String sourceReorderingLabels) {

		String sourceLabels = "";
		if (this.boundaryTagLablingLabelingSettings.useSourceTagLabels() || (sourceReorderingLabels != null)) {
			if (this.boundaryTagLablingLabelingSettings.useSourceInsideTags()) {
				sourceLabels += createSourceInsideLabel(inference);
			}
			if (this.boundaryTagLablingLabelingSettings.useSourceOutsideTags()) {
				sourceLabels += createSourceOutsideLabel(inference);
			}
			sourceLabels += sourceReorderingLabels;
			sourceLabels = XMLTagging.getTagSandwidchedString(LightWeightTranslationRule.SOURCE_LABELS, sourceLabels);
		}
		return sourceLabels;
	}

	private String createSourceLabels(EbitgLexicalizedInferenceBoundaryTagged inference) {
		return createSourceLabels(inference, null);
	}

	private boolean usesBoundaryTagsOnSourceAndTargetSide(){
	    return this.boundaryTagLablingLabelingSettings.useSourceTagLabels() && this.boundaryTagLablingLabelingSettings.useTargetTagLabels();
	}
	
	
	private String createTargetLabels(EbitgLexicalizedInferenceBoundaryTagged inference) {

		String targetLabels = "";
		if (this.boundaryTagLablingLabelingSettings.useTargetTagLabels()) {

			if (this.boundaryTagLablingLabelingSettings.useTargetInsideTags()) {
				targetLabels += createTargetInsideLabel(inference);
			}
			if (this.boundaryTagLablingLabelingSettings.useTargetOutsideTags()) {
				targetLabels += createTargetOutsideLabel(inference);
			}
			
			// We only add additional descriptive tags if we are labeling both source and target side
			if(usesBoundaryTagsOnSourceAndTargetSide()){
			    targetLabels = XMLTagging.getTagSandwidchedString(LightWeightTranslationRule.TARGET_LABELS, targetLabels);
			}
		}
		return targetLabels;
	}

	public String creatBoundaryTagLabel(EbitgLexicalizedInferenceBoundaryTagged inference) {

		checkAtLeastOneTypeTagsUsed();
		String result = createSourceLabels(inference) + createTargetLabels(inference);
		return JoshuaStyle.convertToJoshuaStyleLabel(result);
	}

	public String creatBoundaryTagLabel(EbitgLexicalizedInferenceBoundaryTagged inference, String sourceReorderingLabel) {

		checkAtLeastOneTypeTagsUsed();
		String result = createSourceLabels(inference, sourceReorderingLabel) + createTargetLabels(inference);
		return JoshuaStyle.convertToJoshuaStyleLabel(result);
	}

}
