package grammarExtraction.boundaryTagLabeledGrammarExtraction;

import java.io.Serializable;

import util.Span;
import grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagLabelingSettings.BoundaryTagsType;
import util.Pair;

public abstract class TagPositionsSelector implements Serializable{

	private static final long serialVersionUID = 1L;

	public abstract Pair<Integer> getTagPositionsForSpan(Span span);

	public abstract boolean hasTags();
	
	public abstract String getTagSidesString();

	public static TagPositionsSelector createInsideTagPositionsSelector(BoundaryTagLabelingSettings.BoundaryTagsType boundaryTagsType) {
		if (boundaryTagsType.equals(BoundaryTagsType.LEFT_TAGS)) {
			return new InsideLeftTagPositionsSelector();
		} else if (boundaryTagsType.equals(BoundaryTagsType.RIGHT_TAGS)) {
			return new InsideRightTagPositionsSelector();
		} else if (boundaryTagsType.equals(BoundaryTagsType.BOTH_SIDES_TAGS)) {
			return new InsideBothSidesTagPositionsSelector();
		} else {
			return new TagPositionsSelectorEmpty();
		}
	}

	public static TagPositionsSelector createOutsideTagPositionsSelector(BoundaryTagLabelingSettings.BoundaryTagsType boundaryTagsType) {
		if (boundaryTagsType.equals(BoundaryTagsType.LEFT_TAGS)) {
			return new OutsideLeftTagPositionsSelector();
		} else if (boundaryTagsType.equals(BoundaryTagsType.RIGHT_TAGS)) {
			return new OutsideRightTagPositionsSelector();
		} else if (boundaryTagsType.equals(BoundaryTagsType.BOTH_SIDES_TAGS)) {
			return new OutsideBothSidesTagPositionsSelector();
		} else {
			return new TagPositionsSelectorEmpty();
		}
	}

	private static class TagPositionsSelectorEmpty extends TagPositionsSelector {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public boolean hasTags() {
			return false;
		}

		@Override
		public Pair<Integer> getTagPositionsForSpan(Span span) {
			return null;
		}

		@Override
		public String getTagSidesString() {
		   return BoundaryTagLabelingSettings.EMPTY_STRING;
		}
	}

	private abstract static class TagPositionsSelectorNonEmpty extends TagPositionsSelector {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public boolean hasTags() {
			return true;
		}
	}

	private static class InsideBothSidesTagPositionsSelector extends TagPositionsSelectorNonEmpty {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public Pair<Integer> getTagPositionsForSpan(Span span) {
			return new Pair<Integer>(span.getFirst(), span.getSecond());
		}
		
		@Override
		public String getTagSidesString() {
		   return BoundaryTagLabelingSettings.BOTH_SIDES_TAGS_STRING;
		}
	}

	private static class InsideLeftTagPositionsSelector extends TagPositionsSelectorNonEmpty {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public Pair<Integer> getTagPositionsForSpan(Span span) {
			return new Pair<Integer>(span.getFirst(), span.getFirst() + 1);
		}
		
		@Override
		public String getTagSidesString() {
		   return BoundaryTagLabelingSettings.LEFT_TAGS_STRING;
		}
	}

	private static class InsideRightTagPositionsSelector extends TagPositionsSelectorNonEmpty {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public Pair<Integer> getTagPositionsForSpan(Span span) {
			return new Pair<Integer>(span.getSecond() - 1, span.getSecond());
		}
		
		@Override
		public String getTagSidesString() {
		   return BoundaryTagLabelingSettings.RIGHT_TAGS_STRING;
		}
	}

	private static class OutsideBothSidesTagPositionsSelector extends TagPositionsSelectorNonEmpty {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public Pair<Integer> getTagPositionsForSpan(Span span) {
			return new Pair<Integer>(span.getFirst() - 1, span.getSecond() + 1);
		}
		
		@Override
		public String getTagSidesString() {
		   return BoundaryTagLabelingSettings.BOTH_SIDES_TAGS_STRING;
		}
	}

	private static class OutsideLeftTagPositionsSelector extends TagPositionsSelectorNonEmpty {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public Pair<Integer> getTagPositionsForSpan(Span span) {
			return new Pair<Integer>(span.getFirst() - 2, span.getFirst() - 1);
		}
		
		@Override
		public String getTagSidesString() {
		   return BoundaryTagLabelingSettings.LEFT_TAGS_STRING;
		}
	}

	private static class OutsideRightTagPositionsSelector extends TagPositionsSelectorNonEmpty {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public Pair<Integer> getTagPositionsForSpan(Span span) {
			return new Pair<Integer>(span.getSecond() + 1, span.getSecond() + 2);
		}
		
		@Override
		public String getTagSidesString() {
		   return BoundaryTagLabelingSettings.RIGHT_TAGS_STRING;
		}
	}

}
