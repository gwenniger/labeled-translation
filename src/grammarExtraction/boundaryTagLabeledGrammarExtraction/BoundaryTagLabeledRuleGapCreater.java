package grammarExtraction.boundaryTagLabeledGrammarExtraction;

import util.ConfigFile;
import boundaryTagging.EbitgLexicalizedInferenceBoundaryTagged;
import grammarExtraction.chiangGrammarExtraction.BasicRuleGapCreater;
import grammarExtraction.chiangGrammarExtraction.ReorderLabelingSettings;
import grammarExtraction.translationRules.RuleLabelCreater;

public class BoundaryTagLabeledRuleGapCreater extends BasicRuleGapCreater<EbitgLexicalizedInferenceBoundaryTagged> {

	private final BoundaryTagLabelCreater boundaryTagLabelCreater;

	private BoundaryTagLabeledRuleGapCreater(ReorderLabelingSettings reorderLabelingSettings,RuleLabelCreater ruleLabelCreater, BoundaryTagLabelCreater boundaryTagLabelCreater) {
		super(reorderLabelingSettings,ruleLabelCreater);
		this.boundaryTagLabelCreater = boundaryTagLabelCreater;
	}

	public static BoundaryTagLabeledRuleGapCreater createBoundaryTagLabeledRuleGapCreaterFromConfig(ConfigFile theConfig, ReorderLabelingSettings reorderLabelingSettings,RuleLabelCreater ruleLabelCreater) {
		return new BoundaryTagLabeledRuleGapCreater(reorderLabelingSettings,ruleLabelCreater, BoundaryTagLabelCreater.createBoundaryTagLabelCreater(theConfig));
	}

	public String creatBoundaryTagLabel(EbitgLexicalizedInferenceBoundaryTagged ruleInference) {
		if (this.getReorderLabelingSettings().useReorderingLabelExtension()) {
			return this.boundaryTagLabelCreater.creatBoundaryTagLabel(ruleInference, this.getReorderingLabelExtension(ruleInference));
		} else {
			return this.boundaryTagLabelCreater.creatBoundaryTagLabel(ruleInference);
		}
	}

	@Override
	protected String getReorderingLabelPhrasePairLabel(EbitgLexicalizedInferenceBoundaryTagged ruleInference) {
		return this.boundaryTagLabelCreater.creatBoundaryTagLabel(ruleInference, this.getPhrasePairReorderingLabelExtension());
	}

	@Override
	protected String getBasicLabel(EbitgLexicalizedInferenceBoundaryTagged inference) {
		return creatBoundaryTagLabel(inference);
	}

}
