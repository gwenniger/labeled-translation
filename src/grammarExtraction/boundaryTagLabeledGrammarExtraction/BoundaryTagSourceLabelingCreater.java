package grammarExtraction.boundaryTagLabeledGrammarExtraction;

import java.util.List;
import util.ConfigFile;
import util.Span;
import grammarExtraction.chiangGrammarExtraction.ChiangRuleCreater;
import grammarExtraction.grammarExtractionFoundation.SourceLabelingCreater;
import boundaryTagging.SpanBoundaryTagger;

public class BoundaryTagSourceLabelingCreater implements SourceLabelingCreater {
	private final SpanBoundaryTagger sourceBoundaryTagger;
	private final BoundaryTagLabelCreater boundaryTagLabelCreater;

	private BoundaryTagSourceLabelingCreater(SpanBoundaryTagger sourceBoundaryTagger, BoundaryTagLabelCreater boundaryTagLabelCreater) {
		this.sourceBoundaryTagger = sourceBoundaryTagger;
		this.boundaryTagLabelCreater = boundaryTagLabelCreater;
	}

	public static BoundaryTagSourceLabelingCreater createBoundaryTagSourceLabelingCreater(List<String> sourceTags, ConfigFile theConfig, BoundaryTagLabelingSettings boundaryTagLabelingSettings) {
		SpanBoundaryTagger sourceBoundaryTagger = SpanBoundaryTagger.createSpanBoundaryTagger(sourceTags, boundaryTagLabelingSettings.getSourceInsideTagPosisionsSelector(),
				boundaryTagLabelingSettings.getSourceOutsideTagPosisionsSelector());
		BoundaryTagLabelCreater boundaryTagLabelCreater = BoundaryTagLabelCreater.createBoundaryTagLabelCreater(theConfig);
		return new BoundaryTagSourceLabelingCreater(sourceBoundaryTagger, boundaryTagLabelCreater);
	}

	@Override
	public String createSourceRuleLabelingForSourceSpan(Span sourceSpan) {
		return this.boundaryTagLabelCreater.createSourceLabels(this.sourceBoundaryTagger.getSpanInsideBoundaryTags(sourceSpan), this.sourceBoundaryTagger.getSpanOutsideBoundaryTags(sourceSpan));
	}

	@Override
	public String createSourceGapLabelingForSourceSpan(Span sourceSpan) {
		return ChiangRuleCreater.ChiangLabel;
	}

}
