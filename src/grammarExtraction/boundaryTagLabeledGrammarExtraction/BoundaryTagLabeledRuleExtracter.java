package grammarExtraction.boundaryTagLabeledGrammarExtraction;

import ccgLabeling.CCGLabeler;
import junit.framework.Assert;
import alignmentStatistics.InputStringsEnumeration;
import alignmentStatistics.MultiThreadComputationParameters;
import boundaryTagging.BoundaryTagger;
import boundaryTagging.EbitgChartBuilderBoundaryTagged;
import boundaryTagging.EbitgLexicalizedInferenceBoundaryTagged;
import grammarExtraction.chiangGrammarExtraction.GrammarExtractionConstraints;
import grammarExtraction.chiangGrammarExtraction.HierarchicalPhraseBasedRuleExtractor;
import grammarExtraction.grammarExtractionFoundation.RuleExtracter;
import grammarExtraction.samtGrammarExtraction.ComplexLabelsGrammarExtractionParameters;
import grammarExtraction.samtGrammarExtraction.SAMTGrammarExtractionConstraints;

public class BoundaryTagLabeledRuleExtracter extends RuleExtracter<EbitgLexicalizedInferenceBoundaryTagged, BoundaryTagPentuple> {

	private final BoundaryTagLabeledRuleGapCreater boundaryTagLabeledRuleGapCreater;
	private final BoundaryTagLabelingSettings boundaryTagLabelingSettings;

	protected BoundaryTagLabeledRuleExtracter(GrammarExtractionConstraints grammarExtractionConstraints, int maxAllowedInferencesPerNode, boolean useCaching,
			BoundaryTagLabeledRuleGapCreater boundaryTagLabeledRuleGapCreater, BoundaryTagLabelingSettings boundaryTagLabelingSettings) {
		super(grammarExtractionConstraints, maxAllowedInferencesPerNode, useCaching);
		this.boundaryTagLabeledRuleGapCreater = boundaryTagLabeledRuleGapCreater;
		this.boundaryTagLabelingSettings = boundaryTagLabelingSettings;
	}

	public static BoundaryTagLabeledRuleExtracter createBoundaryTagLabeledRuleExtracter(SAMTGrammarExtractionConstraints chiangGrammarExtractionConstraints, int maxAllowedInferencesPerNode,
			boolean useCaching, BoundaryTagLabeledRuleGapCreater boundaryTagLabeledRuleGapCreater, BoundaryTagLabelingSettings boundaryTagLabelingSettings) {

		return new BoundaryTagLabeledRuleExtracter(chiangGrammarExtractionConstraints, maxAllowedInferencesPerNode, useCaching, boundaryTagLabeledRuleGapCreater, boundaryTagLabelingSettings);
	}

	public static BoundaryTagLabeledRuleExtracter createBoundaryTagLabeledRuleExtracter(ComplexLabelsGrammarExtractionParameters complexLabelGrammarExtractionParameters,
			BoundaryTagLabeledRuleGapCreater boundaryTagLabeledRuleGapCreater, BoundaryTagLabelingSettings boundaryTagLabelingSettings) {
		return createBoundaryTagLabeledRuleExtracter(complexLabelGrammarExtractionParameters.samtGrammarExtractionConstraints,
				complexLabelGrammarExtractionParameters.parameters.getMaxAllowedInferencesPerNode(), complexLabelGrammarExtractionParameters.parameters.getUseCaching(),
				boundaryTagLabeledRuleGapCreater, boundaryTagLabelingSettings);
	}

	@Override
	public HierarchicalPhraseBasedRuleExtractor<EbitgLexicalizedInferenceBoundaryTagged> createTranslationRuleExtractor(BoundaryTagPentuple inputType) {
		return HierarchicalPhraseBasedRuleExtractor.createHierarchicalPhraseBasedRulesExtractorChiangBoundaryTagged(
				createChartBuilderBoundaryTagged(inputType.getSourceString(), inputType.getTargetString(), inputType.getAlignmentString(), inputType.getSourceTagsString(),
						inputType.getTargetTagsString(), boundaryTagLabelingSettings), grammarExtractionConstraints,
				BoundaryTagLabeledRuleCreater.createBoundaryTagLabeledRuleCreater(this.boundaryTagLabeledRuleGapCreater));
	}

	@Override
	public InputStringsEnumeration<BoundaryTagPentuple> createInputStringEnumeration(MultiThreadComputationParameters parameters) {
		return BoundaryTagPentupleEnumeration.createBoundaryTagPentupleEnumeration(parameters.getSourceFileName(), parameters.getTargetFileName(), parameters.getAlignmentsFileName(),
				parameters.getSourceTagFileName(), parameters.getTargetTagFileName());
	}

	protected EbitgChartBuilderBoundaryTagged createChartBuilderBoundaryTagged(String sourceString, String targetString, String alignmentString, String sourceTags, String targetTags,
			BoundaryTagLabelingSettings boundaryTagLabelingSettings) {

		BoundaryTagger boundaryTagger = BoundaryTagger.createBoundaryTagger(sourceTags, targetTags, boundaryTagLabelingSettings);
		EbitgChartBuilderBoundaryTagged ebitgChartBuilder = EbitgChartBuilderBoundaryTagged.createEbitgChartBuilderBoundaryTagged(sourceString, targetString, alignmentString,
				maxAllowedInferencesPerNode, true, useCaching, boundaryTagger, CCGLabeler.createEmptyCCGLabeler());
		assert (maxAllowedInferencesPerNode > 10000);
		boolean result = ebitgChartBuilder.findDerivationsAlignment();
		Assert.assertEquals(true, result);
		return ebitgChartBuilder;
	}

}
