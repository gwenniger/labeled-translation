package grammarExtraction.boundaryTagLabeledGrammarExtraction;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import util.ConfigFile;

public class BoundaryTagLabelingSettings implements Serializable{
	private static final long serialVersionUID = 1L;

	public static enum BoundaryTagsType {
		NO_TAGS, LEFT_TAGS, RIGHT_TAGS, BOTH_SIDES_TAGS
	};

	public static final String EMPTY_STRING = "empty";
	public static final String LEFT_TAGS_STRING = "leftSideTags";
	public static final String RIGHT_TAGS_STRING = "rightSideTags";
	public static final String BOTH_SIDES_TAGS_STRING = "bothSidesTags";
	public static final List<String> BOUNDARY_TAGS_TYPE_NAMES_LIST = Arrays.asList(EMPTY_STRING, LEFT_TAGS_STRING, RIGHT_TAGS_STRING, BOTH_SIDES_TAGS_STRING);
	public static final String SOURCE_INSIDE_TAGS_TYPE_PROPERTY = "sourceInsideTagsType";
	public static final String SOURCE_OUTSIDE_TAGS_TYPE_PROPERTY = "sourceOutsideTagsType";
	public static final String TARGET_INSIDE_TAGS_TYPE_PROPERTY = "targetInsideTagsType";
	public static final String TARGET_OUTSIDE_TAGS_TYPE_PROPERTY = "targetOutsideTagsType";
	public static final List<String> TAG_USAGE_PROPERTIES_NAMES_PROPERTIES_LIST = Arrays.asList(SOURCE_INSIDE_TAGS_TYPE_PROPERTY,
			SOURCE_OUTSIDE_TAGS_TYPE_PROPERTY, TARGET_INSIDE_TAGS_TYPE_PROPERTY, TARGET_OUTSIDE_TAGS_TYPE_PROPERTY);

	private final TagPositionsSelector sourceInsideTagsPositionsSelector;
	private final TagPositionsSelector sourceOutsideTagsPositionsSelector;
	private final TagPositionsSelector targetInsideTagsPositionsSelector;
	private final TagPositionsSelector targetOutsideTagsPositionsSelector;

	private BoundaryTagLabelingSettings(TagPositionsSelector sourceInsideTagsPositionsSelector, TagPositionsSelector sourceOutsideTagsPositionsSelector,
			TagPositionsSelector targetInsideTagsPositionsSelector, TagPositionsSelector targetOutsideTagsPositionsSelector) {
		this.sourceInsideTagsPositionsSelector = sourceInsideTagsPositionsSelector;
		this.sourceOutsideTagsPositionsSelector = sourceOutsideTagsPositionsSelector;
		this.targetInsideTagsPositionsSelector = targetInsideTagsPositionsSelector;
		this.targetOutsideTagsPositionsSelector = targetOutsideTagsPositionsSelector;
	}

	public static BoundaryTagsType getBoundaryTagsType(ConfigFile theConfig, String tagsTypeProperty) {
		String tagsType = theConfig.getValueWithPresenceCheck(tagsTypeProperty);

		if (tagsType.equals(EMPTY_STRING)) {
			return BoundaryTagsType.NO_TAGS;
		} else if (tagsType.equals(LEFT_TAGS_STRING)) {
			return BoundaryTagsType.LEFT_TAGS;
		} else if (tagsType.equals(RIGHT_TAGS_STRING)) {
			return BoundaryTagsType.RIGHT_TAGS;
		} else if (tagsType.equals(BOTH_SIDES_TAGS_STRING)) {
			return BoundaryTagsType.BOTH_SIDES_TAGS;
		} else {
			String errorString = "Unrecognided tags type property. \n Please provide one of the following: ";
			for (String type : BOUNDARY_TAGS_TYPE_NAMES_LIST) {
				errorString += type + "\t";
			}
			throw new RuntimeException(errorString);
		}
	}

	public static BoundaryTagLabelingSettings createBoundaryTagLabelingSettingsFromConfig(ConfigFile theConfig) {
		BoundaryTagsType sourceInsideTagsType = getBoundaryTagsType(theConfig, SOURCE_INSIDE_TAGS_TYPE_PROPERTY);
		BoundaryTagsType sourceOutsideTagsType = getBoundaryTagsType(theConfig, SOURCE_OUTSIDE_TAGS_TYPE_PROPERTY);
		BoundaryTagsType targetInsideTagsType = getBoundaryTagsType(theConfig, TARGET_INSIDE_TAGS_TYPE_PROPERTY);
		BoundaryTagsType targetOutsideTagsType = getBoundaryTagsType(theConfig, TARGET_OUTSIDE_TAGS_TYPE_PROPERTY);
		return new BoundaryTagLabelingSettings(TagPositionsSelector.createInsideTagPositionsSelector(sourceInsideTagsType),
				TagPositionsSelector.createOutsideTagPositionsSelector(sourceOutsideTagsType),
				TagPositionsSelector.createInsideTagPositionsSelector(targetInsideTagsType),
				TagPositionsSelector.createOutsideTagPositionsSelector(targetOutsideTagsType));
	}

	public static BoundaryTagLabelingSettings createAllEmptyBoundaryTagLabelingSettings() {
		return new BoundaryTagLabelingSettings(TagPositionsSelector.createInsideTagPositionsSelector(BoundaryTagsType.NO_TAGS),
				TagPositionsSelector.createOutsideTagPositionsSelector(BoundaryTagsType.NO_TAGS),
				TagPositionsSelector.createInsideTagPositionsSelector(BoundaryTagsType.NO_TAGS),
				TagPositionsSelector.createOutsideTagPositionsSelector(BoundaryTagsType.NO_TAGS));
	}
	
	public static BoundaryTagLabelingSettings createTargetInsideTagsBoundaryTagLabelingSettings() {
		return new BoundaryTagLabelingSettings(TagPositionsSelector.createInsideTagPositionsSelector(BoundaryTagsType.NO_TAGS),
				TagPositionsSelector.createOutsideTagPositionsSelector(BoundaryTagsType.NO_TAGS),
				TagPositionsSelector.createInsideTagPositionsSelector(BoundaryTagsType.BOTH_SIDES_TAGS),
				TagPositionsSelector.createOutsideTagPositionsSelector(BoundaryTagsType.NO_TAGS));
	}

	public boolean useAnyTagLabels() {
		return (useSourceTagLabels() || useTargetTagLabels());
	}

	public boolean useSourceTagLabels() {
		return (useSourceInsideTags() || useSourceOutsideTags());
	}

	public boolean useTargetTagLabels() {
		return (useTargetInsideTags() || useTargetOutsideTags());
	}

	public boolean useSourceInsideTags() {
		return this.sourceInsideTagsPositionsSelector.hasTags();
	}

	public boolean useSourceOutsideTags() {
		return this.sourceOutsideTagsPositionsSelector.hasTags();
	}

	public boolean useTargetInsideTags() {
		return this.targetInsideTagsPositionsSelector.hasTags();
	}

	public boolean useTargetOutsideTags() {
		return this.targetOutsideTagsPositionsSelector.hasTags();
	}

	public TagPositionsSelector getSourceInsideTagPosisionsSelector() {
		return this.sourceInsideTagsPositionsSelector;
	}

	public TagPositionsSelector getSourceOutsideTagPosisionsSelector() {
		return this.sourceOutsideTagsPositionsSelector;
	}

	public TagPositionsSelector getTargetInsideTagPosisionsSelector() {
		return this.targetInsideTagsPositionsSelector;
	}

	public TagPositionsSelector getTargetOutsideTagPosisionsSelector() {
		return this.targetOutsideTagsPositionsSelector;
	}
	
	public String getSourceInsideTagsTagSidesString(){
	    return this.sourceInsideTagsPositionsSelector.getTagSidesString();
	}
	
	public String getSourceOutsideTagsTagSidesString(){
	    return this.sourceOutsideTagsPositionsSelector.getTagSidesString();
	}

	public String getTargetInsideTagsTagSidesString(){
	    return this.targetInsideTagsPositionsSelector.getTagSidesString();
	}
	
	public String getTargetOutsideTagsTagSidesString(){
	    return this.targetOutsideTagsPositionsSelector.getTagSidesString();
	}
	
}
