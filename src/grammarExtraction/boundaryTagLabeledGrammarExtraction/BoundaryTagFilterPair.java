package grammarExtraction.boundaryTagLabeledGrammarExtraction;

import alignment.SourceString;

public class BoundaryTagFilterPair extends SourceString{
	private final String sourceTagsString;

	protected BoundaryTagFilterPair(String sourceString, String sourceTagsString) {
		super(sourceString);
		this.sourceTagsString = sourceTagsString;
	}

	public static BoundaryTagFilterPair createBoundaryTagFilterPair(String sourceString, String sourceTagsString) {

		sourceString = sourceString.trim();
		sourceTagsString = sourceTagsString.trim();
		return new BoundaryTagFilterPair(sourceString, sourceTagsString);
	}

	public String getSourceTagsString() {
		return this.sourceTagsString;
	}
}
