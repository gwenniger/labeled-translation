package grammarExtraction.boundaryTagLabeledGrammarExtraction;

import java.util.ArrayList;

import boundaryTagging.EbitgLexicalizedInferenceBoundaryTagged;
import grammarExtraction.chiangGrammarExtraction.BasicTranslationRuleCreater;
import grammarExtraction.translationRules.RuleGap;
import grammarExtraction.translationRules.TranslationRule;
import grammarExtraction.translationRules.TranslationRuleLexicalProperties;
import hat_lexicalization.Lexicalizer;

public class BoundaryTagLabeledRuleCreater extends BasicTranslationRuleCreater<EbitgLexicalizedInferenceBoundaryTagged, BoundaryTagLabeledRuleGapCreater> {

	private BoundaryTagLabeledRuleCreater(BoundaryTagLabeledRuleGapCreater boundaryTagLabeledRuleGapCreater) {
		super(boundaryTagLabeledRuleGapCreater);
	}

	public static BoundaryTagLabeledRuleCreater createBoundaryTagLabeledRuleCreater(BoundaryTagLabeledRuleGapCreater boundaryTagLabeledRuleGapCreater) {
		return new BoundaryTagLabeledRuleCreater(boundaryTagLabeledRuleGapCreater);
	}

	public TranslationRule<EbitgLexicalizedInferenceBoundaryTagged> createBoundaryTagLabeledTranslationRule(Lexicalizer lexicalizer, EbitgLexicalizedInferenceBoundaryTagged ruleInference) {
		return new TranslationRule<EbitgLexicalizedInferenceBoundaryTagged>(createRuleLabelFromLabelString(basicRuleGapCreater.creatBoundaryTagLabel(ruleInference)),
				TranslationRuleLexicalProperties.createTranslationRuleLexicalProperties(lexicalizer, ruleInference), new ArrayList<RuleGap>(), new ArrayList<RuleGap>(), this.basicRuleGapCreater);
	}

	@Override
	public TranslationRule<EbitgLexicalizedInferenceBoundaryTagged> createBaseTranslationRuleLexicalizedHieroRules(Lexicalizer lexicalizer, EbitgLexicalizedInferenceBoundaryTagged ruleInference) {
		return createBoundaryTagLabeledTranslationRule(lexicalizer, ruleInference);
	}

}
