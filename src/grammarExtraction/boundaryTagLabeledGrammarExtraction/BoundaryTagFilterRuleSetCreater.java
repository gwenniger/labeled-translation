package grammarExtraction.boundaryTagLabeledGrammarExtraction;

import java.util.List;

import alignmentStatistics.InputStringsEnumeration;

import util.ConfigFile;

import grammarExtraction.chiangGrammarExtraction.GrammarExtractionConstraints;
import grammarExtraction.grammarExtractionFoundation.FilterRuleSetCreater;
import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;
import grammarExtraction.grammarExtractionFoundation.MonotoneFilterRulesetCreater;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

public class BoundaryTagFilterRuleSetCreater implements FilterRuleSetCreater<BoundaryTagFilterPair> {

	private final ConfigFile theConfig;
	private final String sourceFileName;
	private final String sourceTagFileName;

	public BoundaryTagFilterRuleSetCreater(ConfigFile theConfig, String sourceFileName, String sourceTagFileName) {
		this.theConfig = theConfig;
		this.sourceFileName = sourceFileName;
		this.sourceTagFileName = sourceTagFileName;
	}

	@Override
	public List<LightWeightPhrasePairRuleSet> produceSoureSideConsistentRuleSets(BoundaryTagFilterPair input, WordKeyMappingTable wordKeyMappingTable,
			GrammarExtractionConstraints chiangGrammarExtractionConstraints) {
		MonotoneFilterRulesetCreater ruleSetCreater = MonotoneFilterRulesetCreater.createMonotoneFilterRulesetCreaterSourceBoundaryTagLabeled(input, wordKeyMappingTable,
				chiangGrammarExtractionConstraints, theConfig);
		return ruleSetCreater.produceSoureSideConsistentRuleSets();
	}

	@Override
	public InputStringsEnumeration<BoundaryTagFilterPair> getFilterSentencesEnumeration() {
		return BoundaryTagFilterPairEnumeration.createBoundaryTagFilterPairEnumeration(sourceFileName, sourceTagFileName);
	}
}
