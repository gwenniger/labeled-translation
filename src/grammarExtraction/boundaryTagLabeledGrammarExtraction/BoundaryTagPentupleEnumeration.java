package grammarExtraction.boundaryTagLabeledGrammarExtraction;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;
import alignmentStatistics.InputStringsEnumeration;

public class BoundaryTagPentupleEnumeration extends InputStringsEnumeration<BoundaryTagPentuple> {

	protected BoundaryTagPentupleEnumeration(List<BufferedReader> inputReaders) {
		super(inputReaders);
	}

	public static BoundaryTagPentupleEnumeration createBoundaryTagPentupleEnumeration(String sourceFileName, String targetFileName, String alignmentsFileName, String sourceTagsFileName,
			String targetTagsFileName) {
		try {
			BufferedReader sourceReader = new BufferedReader(new FileReader(sourceFileName));
			BufferedReader targetReader = new BufferedReader(new FileReader(targetFileName));
			BufferedReader alignmentsReader = new BufferedReader(new FileReader(alignmentsFileName));
			BufferedReader sourceTagsReader = new BufferedReader(new FileReader(sourceTagsFileName));
			BufferedReader targetTagsReader = new BufferedReader(new FileReader(targetTagsFileName));
			List<BufferedReader> inputReaders = Arrays.asList(sourceReader, targetReader, alignmentsReader, sourceTagsReader, targetTagsReader);
			return new BoundaryTagPentupleEnumeration(inputReaders);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	@Override
	protected BoundaryTagPentuple getNextT(List<String> nextInputStrings) {
		return BoundaryTagPentuple.createBoundaryTagsPentuple(nextInputStrings.get(0), nextInputStrings.get(1), nextInputStrings.get(2), nextInputStrings.get(3), nextInputStrings.get(4));
	}

	@Override
	protected boolean inputStringsAreAcceptable(List<String> nextInputStrings) {
		return true;
	}

}
