package grammarExtraction.translationRuleDiskTables;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import mt_pipeline.MTPipeline;

import util.FilePartSplitter;
import util.FileUtil;
import util.Utility;

public class MultiThreadSort {

	private final int numThreads;
	private final String inputFileName;
	private final String resultFileName;
	private final String temporaryDirectoryPath;

	private MultiThreadSort(int numThreads, String inputFileName, String resultFileName, String temporaryDirectoryPath) {
		this.numThreads = numThreads;
		this.inputFileName = inputFileName;
		this.resultFileName = resultFileName;
		this.temporaryDirectoryPath = temporaryDirectoryPath;
	}

	public static MultiThreadSort createMultiThreadSort(int numThreads, String inputFileName, String resultFileName, String temporaryDirectoryPath) {
		return new MultiThreadSort(numThreads, inputFileName, resultFileName, temporaryDirectoryPath);
	}

	public static boolean isSortedLexicographically(String fileName) {
		BufferedReader inputReader = null;
		try {
			inputReader = new BufferedReader(new FileReader(fileName));

			String line, previousLine;
			line = previousLine = inputReader.readLine();

			while ((line = inputReader.readLine()) != null) {
				if (line.compareTo(previousLine) < 0) {
					return false;
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			try {
				if (inputReader != null) {
					inputReader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	private void splitFileIntoParts() {
		System.out.println("Splitting file into parts...");
		FilePartSplitter.splitFileIntoParts(inputFileName, numThreads);
		// System.exit(0);
	}

	private List<String> getPartFileNames() {
		List<String> result = new ArrayList<String>();

		for (int partNum = 1; partNum <= numThreads; partNum++) {
			String partFileName = FilePartSplitter.partFileName(partNum, inputFileName);
			result.add(partFileName);
		}
		return result;
	}

	private List<String> getSortedPartFileNames() {
		List<String> result = new ArrayList<String>();

		for (String partFileName : getPartFileNames()) {
			result.add(partFileName + LinuxSortProcess.SORTED_SUFFIX);
		}
		return result;
	}

	private void sortParts() {

		List<LinuxSortProcess> sortProcesses = new ArrayList<LinuxSortProcess>();
		for (String partFileName : getPartFileNames()) {
			sortProcesses.add(LinuxSortProcess.createLinuxSortProcess(Collections.singletonList(partFileName),
					LinuxSortProcess.createResultFileName(partFileName), this.temporaryDirectoryPath));
		}
		MTPipeline.performThreadComputation(sortProcesses, numThreads);
		// mergePartResultsAndProduceFinalResult(baseInputFileName, numParts);
	}

	private void mergeAndSortSortedParts() {
		List<LinuxSortProcess> sortProcesses = new ArrayList<LinuxSortProcess>();
		sortProcesses.add(LinuxSortProcess.createLinuxSortProcess(getSortedPartFileNames(), resultFileName, this.temporaryDirectoryPath));
		MTPipeline.performThreadComputation(sortProcesses, 1);
	}

	private void deleteTemporaryParts() {
		for (String partFileName : getPartFileNames()) {
			Utility.deleteFile(partFileName);
		}
		for (String sortedPartFileName : getSortedPartFileNames()) {
			Utility.deleteFile(sortedPartFileName);
		}
	}

	public void sortLexicographically() {
		FileUtil.copyFileNative(inputFileName, resultFileName, true);
		splitFileIntoParts();
		sortParts();
		mergeAndSortSortedParts();
		deleteTemporaryParts();
	}

}
