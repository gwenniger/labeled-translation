package grammarExtraction.translationRuleDiskTables;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

import util.LinuxInteractor;
import util.Utility;

public class LinuxSortProcess implements Callable<LinuxSortProcess> {
	protected static final String SORTED_SUFFIX = ".sorted";

	private final List<String> baseFileNames;
	String resultFileName;
	private final boolean reverseSort;
	private final boolean ignoreCase;
	private final String temporaryDirectoryPath;

	private LinuxSortProcess(List<String> baseFileNames, String resultFileName, boolean reverseSort, boolean ignoreCase, String temporaryDirectoryPath) {
		this.baseFileNames = baseFileNames;
		this.resultFileName = resultFileName;
		this.reverseSort = reverseSort;
		this.ignoreCase = ignoreCase;
		this.temporaryDirectoryPath = temporaryDirectoryPath;
	}

	public static LinuxSortProcess createLinuxSortProcess(List<String> baseFileNames, String resultFileName, String temporaryDirectoryPath) {
		return new LinuxSortProcess(baseFileNames, resultFileName, false, false, temporaryDirectoryPath);
	}

	@Override
	public LinuxSortProcess call() throws Exception {
		sortPart();
		return this;
	}

	protected static String createResultFileName(String baseFileName) {
		return createResultFileName(Collections.singletonList(baseFileName));
	}

	protected static String createResultFileName(List<String> baseFileNames) {
		String result = "";
		for (int i = 0; i < baseFileNames.size() - 1; i++) {
			result += baseFileNames.get(i) + "+";
		}
		if (baseFileNames.size() > 0) {
			result += baseFileNames.get(baseFileNames.size() - 1);
		}
		result += SORTED_SUFFIX;
		return result;
	}

	private String sortOptions() {
		String result = "";

		result += "--temporary-directory " + temporaryDirectoryPath;

		if (ignoreCase) {
			result += "--ignore-case";
		}
		if (reverseSort) {
			result += " --reverse";
		}
		return result;
	}

	private void sortPart() {
		// String partFileName =
		String sortCommand = "nohup sort " + sortOptions() + " -S900M" + " " + Utility.stringListStringWithoutBrackets(baseFileNames) + " > " + resultFileName;
		LinuxInteractor.executeCommand(sortCommand, true);
	}

}
