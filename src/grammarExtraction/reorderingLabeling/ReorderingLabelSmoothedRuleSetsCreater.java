package grammarExtraction.reorderingLabeling;

import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;
import grammarExtraction.labelSmoothing.AlternativelyLabeledRuleSetsListCreater;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.LightWeightTranslationRule;
import grammarExtraction.translationRules.LightWeightRuleLabel;
import grammarExtraction.translationRules.RuleLabel;
import grammarExtraction.translationRules.RuleLabelCreater;

import java.util.ArrayList;
import java.util.List;
import reorderingLabeling.ReorderingLabel;
import reorderingLabeling.ReorderingLabelCreater;

import junit.framework.Assert;

public class ReorderingLabelSmoothedRuleSetsCreater implements AlternativelyLabeledRuleSetsListCreater {

	private final WordKeyMappingTable wordKeyMappingTable;
	private final double reorderingLabelSmoothingWeight;
	private final ReorderingLabelCreater smoothingReorderingLabelCreater;
	private final RuleLabelCreater ruleLabelCreater;

	private ReorderingLabelSmoothedRuleSetsCreater(WordKeyMappingTable wordKeyMappingTable, double reorderingLabelSmoothingWeight, ReorderingLabelCreater smoothingReorderingLabelCreater,RuleLabelCreater ruleLabelCreater) {
		this.wordKeyMappingTable = wordKeyMappingTable;
		this.reorderingLabelSmoothingWeight = reorderingLabelSmoothingWeight;
		this.smoothingReorderingLabelCreater = smoothingReorderingLabelCreater;
		this.ruleLabelCreater = ruleLabelCreater;
	}

	public static ReorderingLabelSmoothedRuleSetsCreater createReorderingLabelSmoothedRuleSetsCreater(WordKeyMappingTable wordKeyMappingTable, double reorderingLabelSmoothingWeight,
			ReorderingLabelCreater smoothingReorderingLabelCreater,RuleLabelCreater ruleLabelCreater) {
		return new ReorderingLabelSmoothedRuleSetsCreater(wordKeyMappingTable, reorderingLabelSmoothingWeight, smoothingReorderingLabelCreater,ruleLabelCreater);
	}

	private List<String> getReorderingLabelsListWithoutLabel(String label) {
		List<String> rawLabels = new ArrayList<String>();
		for (ReorderingLabel parentRelativeReorderingLabel : smoothingReorderingLabelCreater.getReorderingLabelsConsideredForSmoothing()) {
			rawLabels.add(parentRelativeReorderingLabel.getLabel());
		}
		rawLabels.remove(label);
		List<String> ruleLabels = new ArrayList<String>();
		for (String rawLabel : rawLabels) {
			ruleLabels.add(RuleLabel.ruleLabelString(rawLabel));
		}
		return ruleLabels;
	}

	private LightWeightTranslationRule createRelabeledTranslationRule(LightWeightTranslationRule translationRule, LightWeightRuleLabel newLabel) {
		if (translationRule != null) {
			return translationRule.createLightWeightTranslationRuleWithNewLeftHandSideLabel(newLabel);
		}
		return null;
	}

	private List<LightWeightTranslationRule> createRelabeledTranslationRuleList(List<LightWeightTranslationRule> originalRules, LightWeightRuleLabel newLabel) {
		List<LightWeightTranslationRule> result = new ArrayList<LightWeightTranslationRule>();
		for (LightWeightTranslationRule originalRule : originalRules) {
			result.add(createRelabeledTranslationRule(originalRule, newLabel));
		}
		return result;
	}

	public static  <T> void assertNoNullElements(List<T> list) {
		for (T element : list) {
			Assert.assertNotNull(element);
		}
	}

	private LightWeightPhrasePairRuleSet createLightWeightPhrasePairRuleSetWithNewLabel(LightWeightPhrasePairRuleSet originalRuleSet, RuleLabel newLabel) {
		LightWeightRuleLabel newLabelLightWeight = LightWeightRuleLabel.createLightWeightRuleLabel(newLabel, wordKeyMappingTable);
		LightWeightTranslationRule newBaseRule = createRelabeledTranslationRule(originalRuleSet.getBaseTranslationRule(), newLabelLightWeight);
		List<LightWeightTranslationRule> newOneGapRules = createRelabeledTranslationRuleList(originalRuleSet.getOneGapTranslationRules(), newLabelLightWeight);
		List<LightWeightTranslationRule> newTwoGapRules = createRelabeledTranslationRuleList(originalRuleSet.getTwoGapTranslationRules(), newLabelLightWeight);
		assertNoNullElements(newOneGapRules);
		assertNoNullElements(newTwoGapRules);
		LightWeightPhrasePairRuleSet result = LightWeightPhrasePairRuleSet.createLightWeightPhrasePairRuleSet(newBaseRule, newOneGapRules, newTwoGapRules, newLabelLightWeight, reorderingLabelSmoothingWeight);
		Assert.assertTrue(result.getRuleSetWeight() < 1);
		return result;
	}

	private List<LightWeightPhrasePairRuleSet> createReorderingLabelSmoothedLightWeightRuleSets(LightWeightPhrasePairRuleSet originalRuleSet) {
		List<LightWeightPhrasePairRuleSet> result = new ArrayList<LightWeightPhrasePairRuleSet>();

		Assert.assertNotNull(this.wordKeyMappingTable);
		String originalLabel = this.wordKeyMappingTable.getWordForKey(originalRuleSet.getLeftHandSideLabel().getSourceSideLabel());
		// System.out.println("originalLabel:" + originalLabel);

		for (String alternativeReorderingLabel : getReorderingLabelsListWithoutLabel(originalLabel)) {
			result.add(createLightWeightPhrasePairRuleSetWithNewLabel(originalRuleSet, ruleLabelCreater.createRuleLabelFromLabelString(alternativeReorderingLabel)));
		}
		return result;
	}

	@Override
	public List<LightWeightPhrasePairRuleSet> createAlternativelyLabeledLightWeightRuleSets(LightWeightPhrasePairRuleSet originalRuleSet) {
		return createReorderingLabelSmoothedLightWeightRuleSets(originalRuleSet);
	}
}
