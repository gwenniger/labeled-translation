/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */

package grammarExtraction.reorderingLabeling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import reorderingLabeling.PhraseCentricReorderingLabel;
import reorderingLabeling.ReorderingLabel;
import reorderingLabeling.ReorderingLabelCreater;
import extended_bitg.EbitgChartEntry.HATComplexityType;
import extended_bitg.EbitgInference;

public class HATComplexityTypeReorderingLabel extends PhraseCentricReorderingLabel {

    private static final long serialVersionUID = 1L;

    public static final HATComplexityTypeReorderingLabel PHRASE_PAIR_LABEL = new HATComplexityTypeReorderingLabel(
	    PHRASE_PAIR_LABEL_TAG);
    public static final HATComplexityTypeReorderingLabel ATOMIC_LABEL = new HATComplexityTypeReorderingLabel(
	    ATOMIC_STRING);
    public static final HATComplexityTypeReorderingLabel MONOTONE_LABEL = new HATComplexityTypeReorderingLabel(
	    MONOTONE_STRING);
    public static final HATComplexityTypeReorderingLabel INVERTED_LABEL = new HATComplexityTypeReorderingLabel(
	    INVERTED_STRING);
    public static final HATComplexityTypeReorderingLabel PERMUTATION_LABEL = new HATComplexityTypeReorderingLabel(
	    PERMUTATION_STRING);
    public static final HATComplexityTypeReorderingLabel HAT_LABEL = new HATComplexityTypeReorderingLabel(
	    HAT_STRING);

    private static final List<HATComplexityTypeReorderingLabel> ALL_REORDERING_LABELS = Arrays
	    .asList(ATOMIC_LABEL, MONOTONE_LABEL, INVERTED_LABEL, PERMUTATION_LABEL, HAT_LABEL);

    private static final String HAT_COMPLEXITY_TYPE_REORDERING_LABEL = "hatComplexityTypeReorderingLabel";

    protected HATComplexityTypeReorderingLabel(String labelString) {
	super(labelString);
    }
    
    private HATComplexityTypeReorderingLabel(){
	super(null);
    }
    
    public static ReorderingLabelCreater createReorderingLabelCreater() {
  	return new HATComplexityTypeReorderingLabel();
      }

    @Override
    public ReorderingLabel getReorderingLabelForReorderingLabelContainingString(
	    String reorderingLabelContainingString) {
	return createPhraseCentricReorderingLabelForLabelContainingString(reorderingLabelContainingString);
    }

    public static String getLabelTypeName() {
	return HAT_COMPLEXITY_TYPE_REORDERING_LABEL;
    }

    protected ReorderingLabel getPhrasePairLabel() {
	return PHRASE_PAIR_LABEL;
    }

    protected ReorderingLabel getAtomicLabel() {
	return ATOMIC_LABEL;
    }

    protected ReorderingLabel getMonotoneLabel() {
	return MONOTONE_LABEL;
    }

    protected ReorderingLabel getInvertedLabel() {
	return INVERTED_LABEL;
    }

    protected ReorderingLabel getPermutationLabel() {
	return PERMUTATION_LABEL;
    }

    protected ReorderingLabel getHATLabel() {
	return HAT_LABEL;
    }

    private ReorderingLabel createPhraseCentricReorderingLabelForLabelContainingString(
	    String labelContainingString) {
	if (labelContainingString.contains(ReorderingLabel.PHRASE_PAIR_LABEL_TAG)) {
	    return getPhrasePairLabel();
	} else if (labelContainingString.contains(ATOMIC_STRING)) {
	    return getAtomicLabel();
	} else if (labelContainingString.contains(MONOTONE_STRING)) {
	    return getMonotoneLabel();
	} else if (labelContainingString.contains(INVERTED_STRING)) {
	    return getInvertedLabel();
	} else if (labelContainingString.contains(PERMUTATION_STRING)) {
	    return getPermutationLabel();
	} else if (labelContainingString.contains(HAT_STRING)) {
	    return getHATLabel();
	} else {
	    throw new RuntimeException(
		    "Error :  createParentRelativeReorderingLabelForLabelContainingString( - String "
			    + labelContainingString + " contains no recognized reordering label");
	}
    }

    @Override
    public ReorderingLabel createReorderingLabelFromLabelName(String labelName) {
	if (labelName.equals(ATOMIC_STRING)) {
	    return getAtomicLabel();
	} else if (labelName.equals(MONOTONE_STRING)) {
	    return getMonotoneLabel();
	} else if (labelName.equals(INVERTED_STRING)) {
	    return getInvertedLabel();
	} else if (labelName.equals(PERMUTATION_STRING)) {
	    return getPermutationLabel();
	} else if (labelName.contains(HAT_STRING)) {
	    return getHATLabel();
	} else if (labelName.equals(PHRASE_PAIR_LABEL_TAG)) {
	    return getPhrasePairLabel();
	} else {
	    throw new RuntimeException("Error: unrecognized label name");
	}
    }

    @Override
    public ReorderingLabel getReorderingLabelForInference(EbitgInference inference) {

	HATComplexityType hatComplexityType = inference.getHATComplexityType();
	// The inference is considered 'atomic' since it can not be further
	// decomposed, and is labeled as such
	if (hatComplexityType.equals(HATComplexityType.HAT)) {
	    return getHATLabel();
	} else if (hatComplexityType.equals(HATComplexityType.PET)) {
	    return getPermutationLabel();
	} else if (hatComplexityType.equals(HATComplexityType.BITT_INVERTED)) {
	    return getInvertedLabel();
	} else if (hatComplexityType.equals(HATComplexityType.BITT_MONO)) {
	    return getMonotoneLabel();
	} else if (hatComplexityType.equals(HATComplexityType.ATOMIC)) {
	    return getAtomicLabel();
	} else {
	    System.out.println(" getReorderingLabelForInference: " + inference);
	    throw new RuntimeException(
		    "Error : HATComplexityTypeReorderingLabel. getReorderingLabelForInference("
			    + inference + ") - Something went wrong in getting HATComplexityType");
	}
    }

    @Override
    public int getHatFineReorderingFeaturesCategoryMappingForLabel() {
	if (this.equals(getAtomicLabel())) {
	    return 0;
	} else if (this.equals(getMonotoneLabel())) {
	    return 1;
	} else if (this.equals(getInvertedLabel())) {
	    return 2;
	} else if (this.equals(getPermutationLabel())) {
	    return 3;
	} else if (this.isHatLabel()) {
	    return 4;
	} else {
	    return -1;
	}
    }

    @Override
    public int getCoarseReorderingFeaturesCategoryMappingForLabel() {
	if (this.equals(getAtomicLabel()) || this.equals(getMonotoneLabel())) {
	    return 0;
	} else if (this.equals(getInvertedLabel()) || this.equals(getPermutationLabel())
		|| this.isHatLabel()) {
	    return 1;
	} else {
	    return -1;
	}
    }

    public boolean isHatLabel() {
	return this.labelString.contains(HAT_STRING);
    }

    @Override
    public int getNumberOfReorderingLabelsUsedForFineHatReorderingFeatures() {
	return 5;
    }

    @Override
    public int getNumberOfReorderingLabelsUsedForCoarseReorderingFeatures() {
	return 2;
    }

    @Override
    public boolean isBinaryReorderingLabel() {
	return (this.equals(getMonotoneLabel()) || this.equals(getInvertedLabel()));
    }

    @Override
    public boolean producesDoubleReorderingLabels() {
	return false;
    }

    @Override
    protected List<ReorderingLabel> getAllReorderingLabelsExceptPhrase() {
	List<ReorderingLabel> result = new ArrayList<ReorderingLabel>(ALL_REORDERING_LABELS);
	return result;
    }

  

}
