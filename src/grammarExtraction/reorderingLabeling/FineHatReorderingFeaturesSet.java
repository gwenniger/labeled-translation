package grammarExtraction.reorderingLabeling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import reorderingLabeling.ReorderingLabel;

import junit.framework.Assert;
import util.XMLTagging;
import grammarExtraction.chiangGrammarExtraction.ReorderLabelingSettings;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatter;
import grammarExtraction.phraseProbabilityWeights.NamedFeature;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.TranslationRuleGapInformation;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

public class FineHatReorderingFeaturesSet extends ReorderingFeaturesSet {

	public static String FINE_HAT_REORDERING_FEATURES_SET_NAME = "fineHatReorderingFeaturesSet";

	private final MultiDimensionalDiscreteCaseFeature twoGapRuleFeatureBasic;
	private final MultiDimensionalDiscreteCaseFeature twoGapRuleFeatureWithReorderingInformation;
	private final MultiDimensionalDiscreteCaseFeature separateGapFeatureFirstGapBasic;
	private final MultiDimensionalDiscreteCaseFeature separateGapFeatureSecondGapBasic;
	private final MultiDimensionalDiscreteCaseFeature separateGapFeatureFirstGapWithReorderingInformation;
	private final MultiDimensionalDiscreteCaseFeature separateGapFeatureSecondGapWithReorderingInformation;

	protected FineHatReorderingFeaturesSet(FeatureValueFormatter featureValueFormatter, MultiDimensionalDiscreteCaseFeature oneGapRuleFeature, MultiDimensionalDiscreteCaseFeature twoGapRuleFeatureBasic,
			MultiDimensionalDiscreteCaseFeature twoGapRuleFeatureWithReorderingInformation,
			MultiDimensionalDiscreteCaseFeature separateGapFeatureFirstGapBasic, MultiDimensionalDiscreteCaseFeature separateGapFeatureSecondGapBasic,
			MultiDimensionalDiscreteCaseFeature separateGapFeatureFirstGapWithReorderingInformation,
			MultiDimensionalDiscreteCaseFeature separateGapFeatureSecondGapWithReorderingInformation) {
		super(featureValueFormatter, oneGapRuleFeature);
		this.twoGapRuleFeatureBasic = twoGapRuleFeatureBasic;
		this.twoGapRuleFeatureWithReorderingInformation = twoGapRuleFeatureWithReorderingInformation;
		this.separateGapFeatureFirstGapBasic = separateGapFeatureFirstGapBasic;
		this.separateGapFeatureSecondGapBasic = separateGapFeatureSecondGapBasic;
		this.separateGapFeatureFirstGapWithReorderingInformation = separateGapFeatureFirstGapWithReorderingInformation;
		this.separateGapFeatureSecondGapWithReorderingInformation = separateGapFeatureSecondGapWithReorderingInformation;
		checkConsistencyFeatureNames();
	}

	
	protected void checkConsistencyFeatureNames() {
		Set<String> uniqueFeatureNames = new HashSet<String>();
		for (MultiDimensionalDiscreteCaseFeature featurName : getReorderingFeaturesInOrder()) {
			uniqueFeatureNames.add(featurName.getFeatureName());
		}
		Assert.assertTrue(uniqueFeatureNames.size() == 7);
	}
	public static FineHatReorderingFeaturesSet createFineHatReorderingFeaturesSet(TranslationRuleSignatureTriple translationRuleSignatureTriple,

	WordKeyMappingTable wordKeyMappingTable, ReorderLabelingSettings reorderLabelingSettings,FeatureValueFormatter featureValueFormatter) {
		ReorderingFeatureCreater reorderingFeatureCreater = ReorderingFeatureCreater.createFineHatReorderingFeatureCreater(reorderLabelingSettings,featureValueFormatter);
		return createFineHatReorderingFeaturesSet(translationRuleSignatureTriple, wordKeyMappingTable, reorderingFeatureCreater);
	}

	public static FineHatReorderingFeaturesSet createFineHatReorderingFeaturesSet(TranslationRuleSignatureTriple translationRuleSignatureTriple,

	WordKeyMappingTable wordKeyMappingTable, ReorderingFeatureCreater reorderingFeatureCreater) {
		TranslationRuleGapInformation translationRuleGapInformation = TranslationRuleGapInformation.createTranslationRuleGapInformation(
				translationRuleSignatureTriple, wordKeyMappingTable);
		List<ReorderingLabel> reorderingLabels = extractReorderingLabelsForTranslationRule(translationRuleSignatureTriple, wordKeyMappingTable,
				reorderingFeatureCreater.getReorderLabelingSettings(), translationRuleGapInformation);

		if (reorderingLabels.size() >= 2) {
			if (reorderingLabels.size() >= 3) {
				return createTwoGapRuleReorderingFeatures(translationRuleGapInformation, reorderingLabels, reorderingFeatureCreater);

			} else {
				return createOneGapRuleReorderingFeatures(translationRuleGapInformation, reorderingLabels, reorderingFeatureCreater);
			}
		}
		return createAllNullReorderingFeatures(reorderingFeatureCreater);
	}

	private static FineHatReorderingFeaturesSet createTwoGapRuleReorderingFeatures(TranslationRuleGapInformation translationRuleGapInformation,
			List<ReorderingLabel> reorderingLabels, ReorderingFeatureCreater reorderingFeatureCreater) {
		MultiDimensionalDiscreteCaseFeature twoGapRuleFeatureBasic, twoGapRuleFeatureWithReorderingInformation, separateGapFeatureFirstGapBasic, separateGapFeatureFirstGapWithReorderingInformation, separateGapFeatureSecondGapBasic, separateGapFeatureSecondGapWithReorderingInformation, oneGapRuleFeature;
		boolean isInverted = !translationRuleGapInformation.ruleIsMonotonic();
		twoGapRuleFeatureBasic = reorderingFeatureCreater.createTwoGapRuleFeatureBasic(reorderingLabels.get(0), reorderingLabels.get(1),
				reorderingLabels.get(2), ruleFeatureName(1));
		twoGapRuleFeatureWithReorderingInformation = reorderingFeatureCreater.createTwoGapRulesFeatureWithReorderingInformation(reorderingLabels, isInverted,
				ruleFeatureName(2));
		separateGapFeatureFirstGapBasic = reorderingFeatureCreater.createTwoGapRuleSeparateGapFeatureBasic(reorderingLabels.get(0), reorderingLabels.get(1),
				ruleFeatureName(3));
		separateGapFeatureSecondGapBasic = reorderingFeatureCreater.createTwoGapRuleSeparateGapFeatureBasic(reorderingLabels.get(0), reorderingLabels.get(2),
				ruleFeatureName(4));
		separateGapFeatureFirstGapWithReorderingInformation = reorderingFeatureCreater.createTwoGapRuleSeparateGapFeatureWithReorderingInformation(
				reorderingLabels.get(0), reorderingLabels.get(1), isInverted, ruleFeatureName(5));
		separateGapFeatureSecondGapWithReorderingInformation = reorderingFeatureCreater.createTwoGapRuleSeparateGapFeatureWithReorderingInformation(
				reorderingLabels.get(0), reorderingLabels.get(2), isInverted, ruleFeatureName(6));
		oneGapRuleFeature = reorderingFeatureCreater.createTwoLabelsRuleNullFeatureBasic(ruleFeatureName(0));

		return new FineHatReorderingFeaturesSet(reorderingFeatureCreater.getFeatureValueFormatter(),oneGapRuleFeature, twoGapRuleFeatureBasic, twoGapRuleFeatureWithReorderingInformation,
				separateGapFeatureFirstGapBasic, separateGapFeatureSecondGapBasic, separateGapFeatureFirstGapWithReorderingInformation,
				separateGapFeatureSecondGapWithReorderingInformation);
	}

	private static FineHatReorderingFeaturesSet createOneGapRuleReorderingFeatures(TranslationRuleGapInformation translationRuleGapInformation,
			List<ReorderingLabel> reorderingLabels, ReorderingFeatureCreater reorderingFeatureCreater) {
		
		return createOneOrNoGapsRuleReorderingFeatures(
				reorderingFeatureCreater.createOneGapRuleFeature(reorderingLabels.get(0), reorderingLabels.get(1), ruleFeatureName(0)),
				reorderingFeatureCreater);
	}

	public static FineHatReorderingFeaturesSet createAllNullReorderingFeatures(ReorderLabelingSettings reorderLabelingSettings,FeatureValueFormatter featureValueFormatter) {
		ReorderingFeatureCreater reorderingFeatureCreater = ReorderingFeatureCreater.createFineHatReorderingFeatureCreater(reorderLabelingSettings,featureValueFormatter);
		return createOneOrNoGapsRuleReorderingFeatures(reorderingFeatureCreater.createTwoLabelsRuleNullFeatureBasic(ruleFeatureName(0)),
				reorderingFeatureCreater);
	}

	public static FineHatReorderingFeaturesSet createAllNullReorderingFeatures(ReorderingFeatureCreater reorderingFeatureCreater) {
		return createOneOrNoGapsRuleReorderingFeatures(reorderingFeatureCreater.createTwoLabelsRuleNullFeatureBasic(ruleFeatureName(0)),
				reorderingFeatureCreater);
	}

	private static FineHatReorderingFeaturesSet createOneOrNoGapsRuleReorderingFeatures(MultiDimensionalDiscreteCaseFeature oneGapRuleFeature,
			ReorderingFeatureCreater reorderingFeatureCreater) {
		MultiDimensionalDiscreteCaseFeature twoGapRuleFeatureBasic, twoGapRuleFeatureWithReorderingInformation, separateGapFeatureFirstGapBasic, separateGapFeatureFirstGapWithReorderingInformation, separateGapFeatureSecondGapBasic, separateGapFeatureSecondGapWithReorderingInformation;
		twoGapRuleFeatureBasic = reorderingFeatureCreater.createThreeLabelsRuleNullFeatureBasic(ruleFeatureName(1));
		twoGapRuleFeatureWithReorderingInformation = reorderingFeatureCreater.createThreeLabelsRuleNullFeatureWithReorderingInformation(ruleFeatureName(2));
		separateGapFeatureFirstGapBasic = reorderingFeatureCreater.createTwoLabelsRuleNullFeatureBasic(ruleFeatureName(3));
		separateGapFeatureSecondGapBasic = reorderingFeatureCreater.createTwoLabelsRuleNullFeatureBasic(ruleFeatureName(4));
		separateGapFeatureFirstGapWithReorderingInformation = reorderingFeatureCreater
				.createTwoLabelsRuleNullFeatureWithReorderingInformation(ruleFeatureName(5));
		separateGapFeatureSecondGapWithReorderingInformation = reorderingFeatureCreater
				.createTwoLabelsRuleNullFeatureWithReorderingInformation(ruleFeatureName(6));

		return new FineHatReorderingFeaturesSet(reorderingFeatureCreater.getFeatureValueFormatter(),oneGapRuleFeature, twoGapRuleFeatureBasic, twoGapRuleFeatureWithReorderingInformation,
				separateGapFeatureFirstGapBasic, separateGapFeatureSecondGapBasic, separateGapFeatureFirstGapWithReorderingInformation,
				separateGapFeatureSecondGapWithReorderingInformation);
	}

	public static List<NamedFeature> getRepresentativeZeroNamedFeatures(SystemIdentity systemIdentity) {

		FineHatReorderingFeaturesSet allNullReorderingFeatures = FineHatReorderingFeaturesSet.createAllNullReorderingFeatures(systemIdentity.getReorderLabelingSettings(),systemIdentity.getFeatureValueFormatter());
		return allNullReorderingFeatures.getNamedFeatures();
	}


	@Override
	protected List<MultiDimensionalDiscreteCaseFeature> getReorderingFeaturesInOrder() {
		List<MultiDimensionalDiscreteCaseFeature> result = Arrays.asList(this.oneGapRuleFeature, this.twoGapRuleFeatureBasic,
				this.twoGapRuleFeatureWithReorderingInformation, this.separateGapFeatureFirstGapBasic, this.separateGapFeatureSecondGapBasic,
				this.separateGapFeatureFirstGapWithReorderingInformation, this.separateGapFeatureSecondGapWithReorderingInformation);
		Assert.assertTrue(result.size() == 7);
		return result;
	}

	private static List<MultiDimensionalDiscreteCaseFeature> getReorderingFeaturesFromString(String reorderingFeaturesString) {
		List<String> reorderingFeatureParts = Arrays.asList(reorderingFeaturesString.split(" "));
		return getReorderingFeaturesListFromFeatureStringsList(reorderingFeatureParts);
	}

	private static List<MultiDimensionalDiscreteCaseFeature> getReorderingFeaturesListFromFeatureStringsList(List<String> reorderingFeatureParts) {
		List<MultiDimensionalDiscreteCaseFeature> result = new ArrayList<MultiDimensionalDiscreteCaseFeature>();
		for (String part : reorderingFeatureParts) {
			String contentsString = XMLTagging.extractNamedPropertyFromString(SPARSE_DISCRETE_CASE_FEATURE_LABEL, part);
			result.add(MultiDimensionalDiscreteCaseFeature.parseMultiDimensionalDiscreteCaseFeatureCompressedFormat(contentsString));
		}
		return result;
	}

	public static FineHatReorderingFeaturesSet createFineHatReorderingFeaturesSetFromFeatureStringsList(FeatureValueFormatter featureValueFormatter,List<String> reorderingFeatureParts) {
		List<MultiDimensionalDiscreteCaseFeature> multiDimensionalDiscreteCaseFeatures = getReorderingFeaturesListFromFeatureStringsList(reorderingFeatureParts);
		return new FineHatReorderingFeaturesSet(featureValueFormatter,multiDimensionalDiscreteCaseFeatures.get(0), multiDimensionalDiscreteCaseFeatures.get(1),
				multiDimensionalDiscreteCaseFeatures.get(2), multiDimensionalDiscreteCaseFeatures.get(3), multiDimensionalDiscreteCaseFeatures.get(4),
				multiDimensionalDiscreteCaseFeatures.get(5), multiDimensionalDiscreteCaseFeatures.get(6));
	}

	private static String getDenseReorderingFeaturesString(List<MultiDimensionalDiscreteCaseFeature> reorderingFeaturesInOrder) {
		String result = "";
		for (MultiDimensionalDiscreteCaseFeature reorderingFeature : reorderingFeaturesInOrder) {
			result += reorderingFeature.getDenseLabeledFeatureRepresentationString();
		}
		return result;
	}

	public String getDenseLabeledReorderingFeaturesString() {
		return getDenseReorderingFeaturesString(this.getReorderingFeaturesInOrder());
	}

	public static String getDenseReorderingFeaturesStringFromCompressedReorderingFeaturesString(String compressedReorderingFeaturesString) {
		List<MultiDimensionalDiscreteCaseFeature> reorderingFeatures = getReorderingFeaturesFromString(compressedReorderingFeaturesString);
		return getDenseReorderingFeaturesString(reorderingFeatures);
	}

	public MultiDimensionalDiscreteCaseFeature getOneGapRuleFeature() {
		return this.oneGapRuleFeature;
	}

	public MultiDimensionalDiscreteCaseFeature getTwoGapRuleFeatureBasic() {
		return this.twoGapRuleFeatureBasic;
	}

	public MultiDimensionalDiscreteCaseFeature getTwoGapRuleFeatureWithReorderingInformation() {
		return this.twoGapRuleFeatureWithReorderingInformation;
	}

	public MultiDimensionalDiscreteCaseFeature getSeperateGapFeatureFirstGapBasic() {
		return this.separateGapFeatureFirstGapBasic;
	}

	public MultiDimensionalDiscreteCaseFeature getSeperateGapFeatureSecondGapBasic() {
		return this.separateGapFeatureSecondGapBasic;
	}

	public MultiDimensionalDiscreteCaseFeature getSeperateGapFeatureFirstGapWithReorderingInformation() {
		return this.separateGapFeatureFirstGapWithReorderingInformation;
	}

	public MultiDimensionalDiscreteCaseFeature getSeperateGapFeatureSecondGapWithReorderingInformation() {
		return this.separateGapFeatureSecondGapWithReorderingInformation;
	}
}
