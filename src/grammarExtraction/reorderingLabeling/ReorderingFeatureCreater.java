package grammarExtraction.reorderingLabeling;

import grammarExtraction.chiangGrammarExtraction.ReorderLabelingSettings;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import reorderingLabeling.ReorderingLabel;

public class ReorderingFeatureCreater {

	private final FeatureValueFormatter featureValueFormatter;
	private final ReorderingLabelToReorderingFeatureIndexMapper reorderingLabelToReorderingFeatureIndexMapper;

	private ReorderingFeatureCreater(FeatureValueFormatter featureValueFormatter,ReorderingLabelToReorderingFeatureIndexMapper reorderingLabelToReorderingFeatureIndexMapper) {
		this.featureValueFormatter = featureValueFormatter;
		this.reorderingLabelToReorderingFeatureIndexMapper = reorderingLabelToReorderingFeatureIndexMapper;
	}

	public static ReorderingFeatureCreater createFineHatReorderingFeatureCreater(ReorderLabelingSettings reorderLabelingSettings,FeatureValueFormatter featureValueFormatter) {
		return new ReorderingFeatureCreater(featureValueFormatter,new FineHatReorderingFeatureIndexMapper(reorderLabelingSettings));
	}

	public static ReorderingFeatureCreater createCoarseReorderingFeatureCreater(ReorderLabelingSettings reorderLabelingSettings,FeatureValueFormatter featureValueFormatter) {
		return new ReorderingFeatureCreater(featureValueFormatter,new CoarseReorderingFeatureIndexMapper(reorderLabelingSettings));
	}

	private int getNumberOfCategoriesDistinguishedForFeatures() {
		return this.reorderingLabelToReorderingFeatureIndexMapper.getNumberOfReorderingLabelsUsedForReorderingFeatures();
	}

	private int getReorderingCategoryIndexForLabel(ReorderingLabel reorderingLabel) {
		return this.reorderingLabelToReorderingFeatureIndexMapper.getReorderingCategoryIndexForLabel(reorderingLabel);
	}

	public ReorderLabelingSettings getReorderLabelingSettings() {
		return this.reorderingLabelToReorderingFeatureIndexMapper.getReorderLabelingSettings();
	}

	public MultiDimensionalDiscreteCaseFeature createTwoGapRuleFeatureBasic(ReorderingLabel leftHandSideReorderingLabel,
			ReorderingLabel firstGapReorderingLabel, ReorderingLabel secondGapReorderingLabel, String ruleFeatureName) {
		List<Integer> numberOfCasesPerDimensionList = createNumberOfCasesPerDimensionList(
				leftHandSideReorderingLabel.getNumberOfReorderingLabelsUsedForFineHatReorderingFeatures(), 3, false);
		List<Integer> chosenCaseForDimensionList = Arrays.asList(leftHandSideReorderingLabel.getHatFineReorderingFeaturesCategoryMappingForLabel(),
				firstGapReorderingLabel.getHatFineReorderingFeaturesCategoryMappingForLabel(),
				secondGapReorderingLabel.getHatFineReorderingFeaturesCategoryMappingForLabel());
		return MultiDimensionalDiscreteCaseFeature.createMultiDimensionalDiscreteCaseFeature(numberOfCasesPerDimensionList, chosenCaseForDimensionList,
				ruleFeatureName);
	}

	public MultiDimensionalDiscreteCaseFeature createTwoGapRulesFeatureWithReorderingInformation(List<ReorderingLabel> reorderingLabels, boolean isInverted,
			String ruleFeatureName) {

		ReorderingLabel leftHandSideReorderingLabel = reorderingLabels.get(0);
		ReorderingLabel firstGapReorderingLabel = reorderingLabels.get(1);
		ReorderingLabel secondGapReorderingLabel = reorderingLabels.get(1);

		int invertionValue = (isInverted) ? 1 : 0;
		List<Integer> numberOfCasesPerDimensionList = createNumberOfCasesPerDimensionList(getNumberOfCategoriesDistinguishedForFeatures(), 3, true);
		List<Integer> chosenCaseForDimensionList = Arrays.asList(getReorderingCategoryIndexForLabel(leftHandSideReorderingLabel),
				getReorderingCategoryIndexForLabel(firstGapReorderingLabel), getReorderingCategoryIndexForLabel(secondGapReorderingLabel), invertionValue);
		return MultiDimensionalDiscreteCaseFeature.createMultiDimensionalDiscreteCaseFeature(numberOfCasesPerDimensionList, chosenCaseForDimensionList,
				ruleFeatureName);
	}

	public MultiDimensionalDiscreteCaseFeature createTwoLabelsRuleNullFeatureBasic(String featureName) {
		return MultiDimensionalDiscreteCaseFeature.createNullCaseMultiDimensionalDiscreteCaseFeature(
				createNumberOfCasesPerDimensionList(getNumberOfCategoriesDistinguishedForFeatures(), 2, false), featureName);
	}

	public MultiDimensionalDiscreteCaseFeature createThreeLabelsRuleNullFeatureWithReorderingInformation(String featureName) {
		return MultiDimensionalDiscreteCaseFeature.createNullCaseMultiDimensionalDiscreteCaseFeature(
				createNumberOfCasesPerDimensionList(getNumberOfCategoriesDistinguishedForFeatures(), 3, true), featureName);
	}

	public MultiDimensionalDiscreteCaseFeature createOneGapRuleFeature(ReorderingLabel leftHandSideReorderingLabel, ReorderingLabel gapReorderingLabel,
			String featureName) {
		List<Integer> numberOfCasesPerDimensionList = createNumberOfCasesPerDimensionList(getNumberOfCategoriesDistinguishedForFeatures(), 2, false);
		List<Integer> chosenCaseForDimensionList = Arrays.asList(getReorderingCategoryIndexForLabel(leftHandSideReorderingLabel),
				getReorderingCategoryIndexForLabel(gapReorderingLabel));
		return MultiDimensionalDiscreteCaseFeature.createMultiDimensionalDiscreteCaseFeature(numberOfCasesPerDimensionList, chosenCaseForDimensionList,
				featureName);
	}

	public MultiDimensionalDiscreteCaseFeature createTwoGapRuleSeparateGapFeatureBasic(ReorderingLabel leftHandSideReorderingLabel,
			ReorderingLabel chosenGapReorderingLabel, String featureName) {
		return createOneGapRuleFeature(leftHandSideReorderingLabel, chosenGapReorderingLabel, featureName);
	}

	public MultiDimensionalDiscreteCaseFeature createTwoLabelsRuleNullFeatureWithReorderingInformation(String featureName) {
		return MultiDimensionalDiscreteCaseFeature.createNullCaseMultiDimensionalDiscreteCaseFeature(
				createNumberOfCasesPerDimensionList(getNumberOfCategoriesDistinguishedForFeatures(), 2, true), featureName);
	}

	public MultiDimensionalDiscreteCaseFeature createThreeLabelsRuleNullFeatureBasic(String featureName) {
		return MultiDimensionalDiscreteCaseFeature.createNullCaseMultiDimensionalDiscreteCaseFeature(
				createNumberOfCasesPerDimensionList(getNumberOfCategoriesDistinguishedForFeatures(), 3, false), featureName);
	}

	public MultiDimensionalDiscreteCaseFeature createTwoGapRuleSeparateGapFeatureWithReorderingInformation(ReorderingLabel leftHandSideReorderingLabel,
			ReorderingLabel chosenGapReorderingLabel, boolean isInverted, String featureName) {
		int invertionValue = (isInverted) ? 1 : 0;
		List<Integer> numberOfCasesPerDimensionList = createNumberOfCasesPerDimensionList(getNumberOfCategoriesDistinguishedForFeatures(), 2, true);
		List<Integer> chosenCaseForDimensionList = Arrays.asList(getReorderingCategoryIndexForLabel(leftHandSideReorderingLabel),
				getReorderingCategoryIndexForLabel(chosenGapReorderingLabel), invertionValue);
		return MultiDimensionalDiscreteCaseFeature.createMultiDimensionalDiscreteCaseFeature(numberOfCasesPerDimensionList, chosenCaseForDimensionList,
				featureName);
	}

	protected static List<Integer> createNumberOfCasesPerDimensionList(int numberOfCases, int numberOfLabels, boolean addInversionInformation) {
		List<Integer> numberOfCasesPerDimensionList = new ArrayList<Integer>();
		for (int i = 0; i < numberOfLabels; i++) {
			numberOfCasesPerDimensionList.add(numberOfCases);
		}
		if (addInversionInformation) {
			numberOfCasesPerDimensionList.add(2);
		}
		return numberOfCasesPerDimensionList;
	}

	private static abstract class ReorderingLabelToReorderingFeatureIndexMapper {

		protected final ReorderLabelingSettings reorderLabelingSettings;

		protected ReorderingLabelToReorderingFeatureIndexMapper(ReorderLabelingSettings reorderLabelingSettings) {
			this.reorderLabelingSettings = reorderLabelingSettings;
		}

		protected ReorderLabelingSettings getReorderLabelingSettings() {
			return this.reorderLabelingSettings;
		}

		protected abstract int getNumberOfReorderingLabelsUsedForReorderingFeatures();

		protected abstract int getReorderingCategoryIndexForLabel(ReorderingLabel reorderingLabel);

	}

	private static class FineHatReorderingFeatureIndexMapper extends ReorderingLabelToReorderingFeatureIndexMapper {

		protected FineHatReorderingFeatureIndexMapper(ReorderLabelingSettings reorderLabelingSettings) {
			super(reorderLabelingSettings);
		}

		@Override
		public int getNumberOfReorderingLabelsUsedForReorderingFeatures() {
			return reorderLabelingSettings.getNumberOfReorderingCategoriesUsedForFineHatReorderingFeatures();
		}

		@Override
		public int getReorderingCategoryIndexForLabel(ReorderingLabel reorderingLabel) {
			return reorderingLabel.getHatFineReorderingFeaturesCategoryMappingForLabel();
		}
	}

	private static class CoarseReorderingFeatureIndexMapper extends ReorderingLabelToReorderingFeatureIndexMapper {
		protected CoarseReorderingFeatureIndexMapper(ReorderLabelingSettings reorderLabelingSettings) {
			super(reorderLabelingSettings);
		}

		@Override
		protected int getNumberOfReorderingLabelsUsedForReorderingFeatures() {
			return reorderLabelingSettings.getNumberOfReorderingCategoriesUsedForCoarseReorderingFeatures();
		}

		@Override
		protected int getReorderingCategoryIndexForLabel(ReorderingLabel reorderingLabel) {
			return reorderingLabel.getCoarseReorderingFeaturesCategoryMappingForLabel();
		}
	}
	
	public FeatureValueFormatter getFeatureValueFormatter()
	{
		return this.featureValueFormatter;
	}

}
