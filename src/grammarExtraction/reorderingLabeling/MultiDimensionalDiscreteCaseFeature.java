package grammarExtraction.reorderingLabeling;

import grammarExtraction.phraseProbabilityWeights.NamedBooleanFeature;
import grammarExtraction.phraseProbabilityWeights.NamedFeature;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import util.Utility;

/**
 * This class stores a multidimensional discrete case feature. This means that this is a feature consisting of multiple discrete dimensions, each dimension
 * having a limited number of mutually exclusive choices. The specific feature is used by choosing a specific value for each dimension. The total number of
 * cases for each dimension is kept, so that the total number of different cases can be computed. This can than be used to map the feature to one final integer
 * value.
 * 
 * @author gemaille
 * 
 */
public class MultiDimensionalDiscreteCaseFeature {
	private final List<Integer> numberOfCasesPerDimensionList;
	private final List<Integer> chosenCaseForDimensionList;
	private final String featureName;

	private MultiDimensionalDiscreteCaseFeature(List<Integer> numberOfCasesPerDimensionList, List<Integer> chosenCaseForDimensionList, String featureName) {
		this.numberOfCasesPerDimensionList = numberOfCasesPerDimensionList;
		this.chosenCaseForDimensionList = chosenCaseForDimensionList;
		this.featureName = featureName;
	}

	public static MultiDimensionalDiscreteCaseFeature createMultiDimensionalDiscreteCaseFeature(List<Integer> numberOfCasesPerDimensionList, List<Integer> chosenCaseForDimensionList,
			String featureName) {
		consistencyCheck(numberOfCasesPerDimensionList, chosenCaseForDimensionList);
		return new MultiDimensionalDiscreteCaseFeature(numberOfCasesPerDimensionList, chosenCaseForDimensionList, featureName);
	}

	public static MultiDimensionalDiscreteCaseFeature createNullCaseMultiDimensionalDiscreteCaseFeature(List<Integer> numberOfCasesPerDimensionList, String featureName) {
		return new MultiDimensionalDiscreteCaseFeature(numberOfCasesPerDimensionList, null, featureName);
	}

	public static void consistencyCheck(List<Integer> numberOfCasesPerDimensionList, List<Integer> chosenCaseForDimensionList) {
		if (numberOfCasesPerDimensionList.size() != chosenCaseForDimensionList.size()) {
			throw new RuntimeException("Error: the nuber of dimensions is not equal to the number of specified values");
		}
		for (int i = 0; i < numberOfCasesPerDimensionList.size(); i++) {
			if (chosenCaseForDimensionList.get(i) > (numberOfCasesPerDimensionList.get(i) - 1)) {
				throw new RuntimeException("Error : specified " + chosenCaseForDimensionList.get(i) + " cases for the " + i + "th dimension.\nThis means values from 0 to  "
						+ (chosenCaseForDimensionList.get(i) - 1) + "(inclusive) are allowed for that dimension. \nBut specified a value of " + chosenCaseForDimensionList.get(i));
			}
		}
	}

	public int totalNumberOfCases() {
		int result = 1;
		for (int casesForDimension : numberOfCasesPerDimensionList) {
			result *= casesForDimension;
		}
		Assert.assertTrue(result > 1);
		return result;
	}

	private int computeValueIthEntry(int i) {
		int valueIthEntry = 1;
		for (int j = 0; j < (i); j++) {
			valueIthEntry *= numberOfCasesPerDimensionList.get(j);
		}
		// System.out.println("valueEntry:" + valueIthEntry);
		return valueIthEntry;
	}

	public boolean isNullFeature() {
		if (this.chosenCaseForDimensionList == null) {
			return true;
		}
		return false;
	}

	public int getIntegerValue() {
		if (isNullFeature()) {
			return -1;
		}
		int result = 0;
		int dimensions = numberOfCasesPerDimensionList.size();
		for (int i = 0; i < dimensions; i++) {
			result += computeValueIthEntry(i) * chosenCaseForDimensionList.get(i);
		}
		return result;
	}

	private boolean getDenseRepresentationValue(int index) {
		if (index == getIntegerValue()) {
			return true;
		}
		return false;
	}

	public String toString() {
		return "R-feature - integerValue: " + getIntegerValue();
	}

	private NamedBooleanFeature getDenseReorderingFeaturePart(int j) {
		return NamedBooleanFeature.createNamedBooleanFeature(featureName + "_" + j, this.getDenseRepresentationValue(j));
	}

	public List<NamedFeature> getNamedFeatures() {
		List<NamedFeature> result = new ArrayList<NamedFeature>();
		Assert.assertTrue(this.totalNumberOfCases() > 0);
		for (int j = 0; j < this.totalNumberOfCases(); j++) {

			result.add(getDenseReorderingFeaturePart(j));
		}
		return result;
	}

	public List<NamedFeature> getNamedFeaturesWithNonZeroValue() {
		List<NamedFeature> result = new ArrayList<NamedFeature>();
		int nonZeroFeatureIndex = getIntegerValue();
		if (nonZeroFeatureIndex != -1) {
			result.add(getDenseReorderingFeaturePart(nonZeroFeatureIndex));
		}
		return result;
	}

	public String getDenseLabeledFeatureRepresentationString() {
		return NamedFeature.createLabeledFeatureStringListJoshuaFormat(getNamedFeatures());
	}

	public String getChosenCasesListAsString() {
		if (this.chosenCaseForDimensionList != null) {
			return Utility.objectListString(this.chosenCaseForDimensionList);
		}
		return "NULL";
	}

	public String getCasesPerDimensionListAsString() {
		return Utility.objectListString(this.numberOfCasesPerDimensionList);
	}

	private static List<Integer> parseIntegerList(String integerListString) {

		if (integerListString.equalsIgnoreCase("null")) {
			return null;
		}

		List<Integer> result = new ArrayList<Integer>();
		integerListString = integerListString.substring(1, integerListString.length() - 1);
		String[] parts = integerListString.split(",");
		for (String part : parts) {
			result.add(Integer.parseInt(part));
		}
		return result;
	}

	public static MultiDimensionalDiscreteCaseFeature parseMultiDimensionalDiscreteCaseFeatureCompressedFormat(String featureString) {
		String[] parts = featureString.split(":");

		if (parts.length != 2) {
			throw new RuntimeException(
					"Error : MultiDimensionalDiscreteCaseFeature.parseMultiDimensionalDiscreteCaseFeatureCompressedFormat - wrong format. Expected: \"FeatureName:ChosenCasen|NumberOfCasesForFeature\" but got:"
							+ featureString);
		}

		String featureName = parts[0];
		String caseInformation = parts[1];
		String[] caseInformationParts = caseInformation.split("\\|");

		if (caseInformationParts.length != 2) {
			System.out.println("caseInformationParts: " + caseInformationParts + " caseInformationParts.lengt: " + caseInformationParts.length);
			throw new RuntimeException(
					"Error : MultiDimensionalDiscreteCaseFeature.parseMultiDimensionalDiscreteCaseFeatureCompressedFormat - wrong format. Expected: \"FeatureName:ChosenCasen|NumberOfCasesForFeature\" but got: \""
							+ featureString + "\"");
		}

		List<Integer> chosenCaseForDimensionList = parseIntegerList(caseInformationParts[0]);
		List<Integer> numberOfCasesPerDimensionList = parseIntegerList(caseInformationParts[1]);

		if (chosenCaseForDimensionList == null) {
			return MultiDimensionalDiscreteCaseFeature.createNullCaseMultiDimensionalDiscreteCaseFeature(numberOfCasesPerDimensionList, featureName);
		} else {
			// System.out.println(" numberOfCasesPerDimensionList : " + numberOfCasesPerDimensionList);
			// System.out.println(" chosenCaseForDimensionList : " + chosenCaseForDimensionList);
			return MultiDimensionalDiscreteCaseFeature.createMultiDimensionalDiscreteCaseFeature(numberOfCasesPerDimensionList, chosenCaseForDimensionList, featureName);
		}
	}

	public String getFeatureName() {
		return this.featureName;
	}
}