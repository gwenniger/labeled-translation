package grammarExtraction.reorderingLabeling;

import java.util.ArrayList;
import java.util.List;

import reorderingLabeling.ReorderingLabel;
import util.XMLTagging;
import grammarExtraction.chiangGrammarExtraction.ReorderLabelingSettings;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatter;
import grammarExtraction.phraseProbabilityWeights.NamedFeature;
import grammarExtraction.samtGrammarExtraction.ComplexRuleFeaturesSet;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.TranslationRuleGapInformation;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

public abstract class ReorderingFeaturesSet extends ComplexRuleFeaturesSet {
	public static final String SPARSE_DISCRETE_CASE_FEATURE_LABEL = "SDCF";

	public static enum ReorderingFeaturesSetType {
		FINE_HAT_REORDERING_FEATURES_SET, COARSE_REORDERING_FEATURES_SET
	}

	protected final MultiDimensionalDiscreteCaseFeature oneGapRuleFeature;

	protected ReorderingFeaturesSet(FeatureValueFormatter featureValueFormatter, MultiDimensionalDiscreteCaseFeature oneGapRuleFeature) {
		super(featureValueFormatter);
		this.oneGapRuleFeature = oneGapRuleFeature;
	}

	public List<NamedFeature> getNamedFeatures() {
		List<NamedFeature> result = new ArrayList<NamedFeature>();
		for (MultiDimensionalDiscreteCaseFeature reorderingFeature : getReorderingFeaturesInOrder()) {
			result.addAll(reorderingFeature.getNamedFeatures());
		}
		return result;
	}

	public List<NamedFeature> getNamedFeaturesSparse() {
		List<NamedFeature> result = new ArrayList<NamedFeature>();
		for (MultiDimensionalDiscreteCaseFeature reorderingFeature : getReorderingFeaturesInOrder()) {
			result.addAll(reorderingFeature.getNamedFeaturesWithNonZeroValue());
		}
		return result;
	}

	protected abstract List<MultiDimensionalDiscreteCaseFeature> getReorderingFeaturesInOrder();


	protected static String ruleFeatureName(int featureNumber) {
		return "RF-" + featureNumber;
	}

	protected static List<ReorderingLabel> extractReorderingLabelsForTranslationRule(TranslationRuleSignatureTriple traRuleSignatureTriple,
			WordKeyMappingTable wordKeyMappingTable, ReorderLabelingSettings reLabelingSettings, TranslationRuleGapInformation translationRuleGapInformation) {
		List<ReorderingLabel> result = new ArrayList<ReorderingLabel>();

		ReorderingLabel leftHandSideLabel = reLabelingSettings.getReorderingLabelForReorderingLabelContainingString(traRuleSignatureTriple
				.getSourceSideRuleLabelString(wordKeyMappingTable));
		result.add(leftHandSideLabel);

		List<String> sourceGapStrings = translationRuleGapInformation.getSourceGapStrings(wordKeyMappingTable);
		for (String gapString : sourceGapStrings) {
			result.add(reLabelingSettings.getReorderingLabelForReorderingLabelContainingString(gapString));
		}

		return result;
	}

	private String getSparseDiscreteCaseFeatureString(MultiDimensionalDiscreteCaseFeature feature, int featureNumber) {
		return XMLTagging.getTagSandwidchedString(SPARSE_DISCRETE_CASE_FEATURE_LABEL, "RF-" + featureNumber + ":" + feature.getChosenCasesListAsString() + "|"
				+ feature.getCasesPerDimensionListAsString());
	}

	public String getCompressedReorderingFeaturesString() {
		String result = "";
		int index = 0;
		for (MultiDimensionalDiscreteCaseFeature reorderingFeature : getReorderingFeaturesInOrder()) {
			result += getSparseDiscreteCaseFeatureString(reorderingFeature, index) + " ";
			index++;
		}
		return result;
	}
}
