package grammarExtraction.reorderingLabeling;

import java.util.List;
import java.util.SortedSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import extended_bitg.EbitgInference;
import reorderingLabeling.FinePhraseCentricReorderingLabel;
import reorderingLabeling.ReorderingLabel;
import reorderingLabeling.ReorderingLabelCreater;

public class HatLabelRefinedPhraseCentricReorderingLabel extends
		FinePhraseCentricReorderingLabel {

	private static final long serialVersionUID = 1L;

	private static final String HAT_LABEL_REFINED_PHRASE_CENTRIC_REORDER_LABEL = "hatLabelRefinedPhraseCentricReorderLabel";

	public static final HatLabelRefinedPhraseCentricReorderingLabel PHRASE_PAIR_LABEL = new HatLabelRefinedPhraseCentricReorderingLabel(
			PHRASE_PAIR_LABEL_TAG);
	public static final HatLabelRefinedPhraseCentricReorderingLabel ATOMIC_LABEL = new HatLabelRefinedPhraseCentricReorderingLabel(
			ATOMIC_STRING);
	public static final HatLabelRefinedPhraseCentricReorderingLabel MONOTONE_LABEL = new HatLabelRefinedPhraseCentricReorderingLabel(
			MONOTONE_STRING);
	public static final HatLabelRefinedPhraseCentricReorderingLabel INVERTED_LABEL = new HatLabelRefinedPhraseCentricReorderingLabel(
			INVERTED_STRING);
	public static final HatLabelRefinedPhraseCentricReorderingLabel PERMUTATION_LABEL = new HatLabelRefinedPhraseCentricReorderingLabel(
			PERMUTATION_STRING);

	public HatLabelRefinedPhraseCentricReorderingLabel() {
		super(null);
	}

	protected HatLabelRefinedPhraseCentricReorderingLabel(String labelString) {
		super(labelString);
	}

	public static ReorderingLabelCreater createReorderingLabelCreater() {
		return new HatLabelRefinedPhraseCentricReorderingLabel();
	}

	@Override
	public ReorderingLabel createReorderingLabelFromLabelName(String labelName) {
		throw new RuntimeException("Not implemented");
	}

	@Override
	public List<ReorderingLabel> getAllReorderingLabelsExceptPhrase() {
		throw new RuntimeException("Not implemented");
	}

	public static String getLabelTypeName() {
		return HAT_LABEL_REFINED_PHRASE_CENTRIC_REORDER_LABEL;
	}

	@Override
	protected ReorderingLabel getPhrasePairLabel() {
		return PHRASE_PAIR_LABEL;
	}

	@Override
	protected ReorderingLabel getAtomicLabel() {
		return ATOMIC_LABEL;
	}

	@Override
	protected ReorderingLabel getMonotoneLabel() {
		return MONOTONE_LABEL;
	}

	@Override
	protected ReorderingLabel getInvertedLabel() {
		return INVERTED_LABEL;
	}

	@Override
	protected ReorderingLabel getPermutationLabel() {
		return PERMUTATION_LABEL;
	}

	private String getHATWithPermutationSpecificationString(
			String labelContainingString) {
		Pattern pattern = Pattern.compile(HAT_STRING + "-<(.*)>");
		Matcher matcher = pattern.matcher(labelContainingString);
		if (matcher.find()) {
			return matcher.group(0);
		}
		throw new RuntimeException(
				"Error : getPermutationStringPart could not find the permutation pattern in " + labelContainingString);
	}

	@Override
	protected ReorderingLabel getHatLabelForLabelContainingString(
			String labelContainingString) {
		String labelString = getHATWithPermutationSpecificationString(labelContainingString);
		return new HatLabelRefinedPhraseCentricReorderingLabel(labelString);
	}

	public ReorderingLabel getHatLabelForHatSetPermutation(
			List<SortedSet<Integer>> sortedsourceSetPermutation) {
		List<SortedSet<Integer>> subPhrasePairRelativePermutation = SubPhrasePairRelativePermutationComputer
				.createSubPhrasePairRelativePermutationComputer(
						sortedsourceSetPermutation)
				.getSubPhrasePairsRelativePermutation(
						sortedsourceSetPermutation);
		String subPhrasePairRelativePermutationString = EbitgInference
				.sortedPermutationString(subPhrasePairRelativePermutation);
		// Add enclosing brackets
		subPhrasePairRelativePermutationString = "<"
				+ subPhrasePairRelativePermutationString + ">";
		String labelString = HAT_STRING + "-"
				+ subPhrasePairRelativePermutationString;
		return new HatLabelRefinedPhraseCentricReorderingLabel(labelString);
	}

	@Override
	protected ReorderingLabel getHatLabelForLabelHATInference(
			EbitgInference inference) {
		return getHatLabelForHatSetPermutation(inference
				.getSortedSourceSetPermuation());
	}

	@Override
	public boolean producesDoubleReorderingLabels() {
	    return false;
	}
}
