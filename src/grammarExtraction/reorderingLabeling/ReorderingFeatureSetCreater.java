package grammarExtraction.reorderingLabeling;

import util.ConfigFile;
import mt_pipeline.mt_config.MTGrammarExtractionConfig;
import grammarExtraction.chiangGrammarExtraction.ReorderLabelingSettings;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatter;
import grammarExtraction.reorderingLabeling.ReorderingFeaturesSet.ReorderingFeaturesSetType;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

public abstract class ReorderingFeatureSetCreater {
	protected final ReorderingFeatureCreater reorderingFeatureCreater;

	protected ReorderingFeatureSetCreater(ReorderingFeatureCreater reorderingFeatureCreater) {
		this.reorderingFeatureCreater = reorderingFeatureCreater;
	}

	public static ReorderingFeatureSetCreater createReorderingFeatureSetCreater(boolean useReorderingLabelBinaryFeatures, ConfigFile configFile,
			ReorderLabelingSettings reorderLabelingSettings,FeatureValueFormatter featureValueFormatter) {
		if (!useReorderingLabelBinaryFeatures) {
			return null;
		} else {
			ReorderingFeaturesSetType reorderingFeaturesSetType = MTGrammarExtractionConfig.getReorderingFeaturesSetType(configFile);
			if (reorderingFeaturesSetType == ReorderingFeaturesSetType.COARSE_REORDERING_FEATURES_SET) {
				return CoarseReorderingFeatureSetCreater.createCoarseReorderingFeatureSetCreater(reorderLabelingSettings,featureValueFormatter);
			} else if (reorderingFeaturesSetType == ReorderingFeaturesSetType.FINE_HAT_REORDERING_FEATURES_SET) {
				return HatFineReorderingFeatureSetCreater.createHatFineReorderingFeatureSetCreater(reorderLabelingSettings,featureValueFormatter);
			} else {
				throw new RuntimeException("Error : unrecognized ReorderingFeatureSetType");
			}
		}
	}

	public static HatFineReorderingFeatureSetCreater createHatFineReorderingFeatureSetCreater(ReorderLabelingSettings reorderLabelingSettings,FeatureValueFormatter featureValueFormatter) {
		return new HatFineReorderingFeatureSetCreater(ReorderingFeatureCreater.createFineHatReorderingFeatureCreater(reorderLabelingSettings,featureValueFormatter));
	}
	
	public abstract ReorderingFeaturesSet createReorderingFeaturesSet(TranslationRuleSignatureTriple translationRuleSignatureTriple,
			WordKeyMappingTable wordKeyMappingTable);

	public abstract ReorderingFeaturesSet createAllNullReorderingFeatures();

	private static class HatFineReorderingFeatureSetCreater extends ReorderingFeatureSetCreater {
		protected HatFineReorderingFeatureSetCreater(ReorderingFeatureCreater reorderingFeatureCreater) {
			super(reorderingFeatureCreater);
		}

		@Override
		public ReorderingFeaturesSet createReorderingFeaturesSet(TranslationRuleSignatureTriple translationRuleSignatureTriple,
				WordKeyMappingTable wordKeyMappingTable) {
			return FineHatReorderingFeaturesSet.createFineHatReorderingFeaturesSet(translationRuleSignatureTriple, wordKeyMappingTable,
					reorderingFeatureCreater);
		}

		@Override
		public ReorderingFeaturesSet createAllNullReorderingFeatures() {
			return FineHatReorderingFeaturesSet.createAllNullReorderingFeatures(this.reorderingFeatureCreater);
		}

	}

	private static class CoarseReorderingFeatureSetCreater extends ReorderingFeatureSetCreater {
		protected CoarseReorderingFeatureSetCreater(ReorderingFeatureCreater reorderingFeatureCreater) {
			super(reorderingFeatureCreater);
		}

		protected static CoarseReorderingFeatureSetCreater createCoarseReorderingFeatureSetCreater(ReorderLabelingSettings reorderLabelingSettings,FeatureValueFormatter featureValueFormatter) {
			return new CoarseReorderingFeatureSetCreater(ReorderingFeatureCreater.createCoarseReorderingFeatureCreater(reorderLabelingSettings,featureValueFormatter));
		}

		@Override
		public ReorderingFeaturesSet createReorderingFeaturesSet(TranslationRuleSignatureTriple translationRuleSignatureTriple,
				WordKeyMappingTable wordKeyMappingTable) {
			return CoarseReorderingFeatureSet.createCoarseReorderingFeaturesSet(translationRuleSignatureTriple, wordKeyMappingTable, reorderingFeatureCreater);
		}

		@Override
		public ReorderingFeaturesSet createAllNullReorderingFeatures() {
			return CoarseReorderingFeatureSet.createAllNullReorderingFeatures(this.reorderingFeatureCreater);
		}
	}
	
	public FeatureValueFormatter getFeatureValueFormatter()
	{
		return this.reorderingFeatureCreater.getFeatureValueFormatter();
	}

}
