package grammarExtraction.reorderingLabeling;

import grammarExtraction.chiangGrammarExtraction.ReorderLabelingSettings;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatter;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.TranslationRuleGapInformation;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

import java.util.Arrays;
import java.util.List;

import reorderingLabeling.ReorderingLabel;

public class CoarseReorderingFeatureSet extends ReorderingFeaturesSet {
	public static String COARSE_REORDERING_FEATURES_SET_NAME = "coarseReorderingFeaturesSet";

	private final MultiDimensionalDiscreteCaseFeature twoGapLexicalizedRulesFeatureWithReorderingInformation;
	private final MultiDimensionalDiscreteCaseFeature twoGapAbstractRulesFeatureWithReorderingInformation;

	private CoarseReorderingFeatureSet(FeatureValueFormatter featureValueFormatter, MultiDimensionalDiscreteCaseFeature oneGapRuleFeature,
			MultiDimensionalDiscreteCaseFeature twoGapLexicalizedRulesFeatureWithReorderingInformation,
			MultiDimensionalDiscreteCaseFeature twoGapAbstractRulesFeatureWithReorderingInformation) {
		super(featureValueFormatter,oneGapRuleFeature);
		this.twoGapLexicalizedRulesFeatureWithReorderingInformation = twoGapLexicalizedRulesFeatureWithReorderingInformation;
		this.twoGapAbstractRulesFeatureWithReorderingInformation = twoGapAbstractRulesFeatureWithReorderingInformation;
	}

	public static ReorderingFeaturesSet createCoarseReorderingFeaturesSet(TranslationRuleSignatureTriple translationRuleSignatureTriple,

	WordKeyMappingTable wordKeyMappingTable, ReorderLabelingSettings reorderLabelingSettings,FeatureValueFormatter featureValueFormatter) {
		ReorderingFeatureCreater reorderingFeatureCreater = ReorderingFeatureCreater.createCoarseReorderingFeatureCreater(reorderLabelingSettings,featureValueFormatter);
		return createCoarseReorderingFeaturesSet(translationRuleSignatureTriple, wordKeyMappingTable, reorderingFeatureCreater);
	}

	public static ReorderingFeaturesSet createCoarseReorderingFeaturesSet(TranslationRuleSignatureTriple translationRuleSignatureTriple,

	WordKeyMappingTable wordKeyMappingTable, ReorderingFeatureCreater reorderingFeatureCreater) {
		TranslationRuleGapInformation translationRuleGapInformation = TranslationRuleGapInformation.createTranslationRuleGapInformation(
				translationRuleSignatureTriple, wordKeyMappingTable);
		List<ReorderingLabel> reorderingLabels = extractReorderingLabelsForTranslationRule(translationRuleSignatureTriple, wordKeyMappingTable,
				reorderingFeatureCreater.getReorderLabelingSettings(), translationRuleGapInformation);

		if (reorderingLabels.size() >= 2) {
			if (reorderingLabels.size() >= 3) {
				return createTwoGapRuleReorderingFeatures(translationRuleGapInformation, reorderingLabels,
						translationRuleSignatureTriple.isAbstractRule(wordKeyMappingTable), reorderingFeatureCreater);

			} else {
				return createOneGapRuleReorderingFeatures(translationRuleGapInformation, reorderingLabels, reorderingFeatureCreater);
			}
		}
		return createAllNullReorderingFeatures(reorderingFeatureCreater);
	}

	private static CoarseReorderingFeatureSet createTwoGapRuleReorderingFeatures(TranslationRuleGapInformation translationRuleGapInformation,
			List<ReorderingLabel> reorderingLabels, boolean ruleIsAbstract, ReorderingFeatureCreater reorderingFeatureCreater) {
		boolean isInverted = !translationRuleGapInformation.ruleIsMonotonic();
		MultiDimensionalDiscreteCaseFeature oneGapRuleFeature = reorderingFeatureCreater.createTwoLabelsRuleNullFeatureBasic(ruleFeatureName(0));
		MultiDimensionalDiscreteCaseFeature twoGapLexicalizedRulesFeatureWithReorderingInformation, twoGapAbstractRulesFeatureWithReorderingInformation;

		if (ruleIsAbstract) {
			twoGapLexicalizedRulesFeatureWithReorderingInformation = reorderingFeatureCreater
					.createThreeLabelsRuleNullFeatureWithReorderingInformation(ruleFeatureName(1));
			twoGapAbstractRulesFeatureWithReorderingInformation = reorderingFeatureCreater.createTwoGapRulesFeatureWithReorderingInformation(reorderingLabels,
					isInverted, ruleFeatureName(2));

		} else {

			twoGapLexicalizedRulesFeatureWithReorderingInformation = reorderingFeatureCreater.createTwoGapRulesFeatureWithReorderingInformation(
					reorderingLabels, isInverted, ruleFeatureName(1));
			twoGapAbstractRulesFeatureWithReorderingInformation = reorderingFeatureCreater
					.createThreeLabelsRuleNullFeatureWithReorderingInformation(ruleFeatureName(2));
		}

		return new CoarseReorderingFeatureSet(reorderingFeatureCreater.getFeatureValueFormatter(),oneGapRuleFeature, twoGapLexicalizedRulesFeatureWithReorderingInformation,
				twoGapAbstractRulesFeatureWithReorderingInformation);
	}

	private static CoarseReorderingFeatureSet createOneGapRuleReorderingFeatures(TranslationRuleGapInformation translationRuleGapInformation,
			List<ReorderingLabel> reorderingLabels, ReorderingFeatureCreater reorderingFeatureCreater) {
		return createOneOrNoGapsRuleReorderingFeatures(
				reorderingFeatureCreater.createOneGapRuleFeature(reorderingLabels.get(0), reorderingLabels.get(1), ruleFeatureName(0)),
				reorderingFeatureCreater);
	}

	public static CoarseReorderingFeatureSet createAllNullReorderingFeatures(ReorderLabelingSettings reorderLabelingSettings,FeatureValueFormatter featureValueFormatter) {
		ReorderingFeatureCreater reorderingFeatureCreater = ReorderingFeatureCreater.createCoarseReorderingFeatureCreater(reorderLabelingSettings,featureValueFormatter);
		return createAllNullReorderingFeatures(reorderingFeatureCreater);
	}

	protected static CoarseReorderingFeatureSet createAllNullReorderingFeatures(ReorderingFeatureCreater reorderingFeatureCreater) {
		return createOneOrNoGapsRuleReorderingFeatures(reorderingFeatureCreater.createTwoLabelsRuleNullFeatureBasic(ruleFeatureName(0)),
				reorderingFeatureCreater);
	}

	private static CoarseReorderingFeatureSet createOneOrNoGapsRuleReorderingFeatures(MultiDimensionalDiscreteCaseFeature oneGapRuleFeature,
			ReorderingFeatureCreater reorderingFeatureCreater) {
		MultiDimensionalDiscreteCaseFeature twoGapLexicalizedRulesFeatureWithReorderingInformation, twoGapAbstractRulesFeatureWithReorderingInformation;
		twoGapLexicalizedRulesFeatureWithReorderingInformation = reorderingFeatureCreater
				.createThreeLabelsRuleNullFeatureWithReorderingInformation(ruleFeatureName(1));
		twoGapAbstractRulesFeatureWithReorderingInformation = reorderingFeatureCreater
				.createThreeLabelsRuleNullFeatureWithReorderingInformation(ruleFeatureName(2));

		return new CoarseReorderingFeatureSet(reorderingFeatureCreater.getFeatureValueFormatter(),oneGapRuleFeature, twoGapLexicalizedRulesFeatureWithReorderingInformation,
				twoGapAbstractRulesFeatureWithReorderingInformation);
	}

	@Override
	protected List<MultiDimensionalDiscreteCaseFeature> getReorderingFeaturesInOrder() {
		return Arrays.asList(this.oneGapRuleFeature, this.twoGapLexicalizedRulesFeatureWithReorderingInformation,
				this.twoGapAbstractRulesFeatureWithReorderingInformation);
	}

	@Override
	public String getCompressedReorderingFeaturesString() {
		throw new RuntimeException("Not implemented");
	}

}