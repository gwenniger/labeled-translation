package grammarExtraction.reorderingLabeling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import grammarExtraction.chiangGrammarExtraction.BasicRuleFeaturesSet;
import grammarExtraction.chiangGrammarExtraction.RuleFeaturesBasic;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.phraseProbabilityWeights.NamedFeature;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsSet;
import grammarExtraction.samtGrammarExtraction.LabeledRuleFeaturesSet;
import grammarExtraction.samtGrammarExtraction.RuleFeaturesExtended;
import grammarExtraction.samtGrammarExtraction.UnknownWordRulesCreatorPosTags;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.TranslationRuleFeatures;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

public class RuleFeaturesReordering extends RuleFeaturesExtended {

	private final ReorderingFeaturesSet reorderingFeatureSet;

	protected RuleFeaturesReordering(BasicRuleFeaturesSet basicRuleFeaturesSet, LabeledRuleFeaturesSet labeledRuleFeaturesSet,
			ReorderingFeaturesSet reorderingFeatureSet) {
		super(basicRuleFeaturesSet, labeledRuleFeaturesSet);
		this.reorderingFeatureSet = reorderingFeatureSet;
	}

	public static RuleFeaturesReordering createRuleFeaturesReordering(double ruleGivenLabelGenerativeProbability,
			PhraseProbabilityWeightsSet phraseProbabilityWeightsSet, double lexicalWeightSourceToTarget, double lexicalWeightSourceGivenTarget,
			double ruleCorpusCount, boolean isHieroRule, boolean isHieroGlueRule, boolean isSyntacticGlueRule,
			TranslationRuleSignatureTriple translationRuleSignatureTriple, WordKeyMappingTable wordKeyMappingTable,
			ReorderingFeatureSetCreater reorderingFeatureSetCreater) {
		BasicRuleFeaturesSet basicRuleFeaturesSet = BasicRuleFeaturesSet.createBasicRuleFeatures(phraseProbabilityWeightsSet, lexicalWeightSourceToTarget,
				lexicalWeightSourceGivenTarget, reorderingFeatureSetCreater.getFeatureValueFormatter());
		LabeledRuleFeaturesSet labeledRuleFeaturesSet = LabeledRuleFeaturesSet.createLabeledRuleFeaturesSet(
				reorderingFeatureSetCreater.getFeatureValueFormatter(), ruleCorpusCount, isHieroRule, isHieroGlueRule, isSyntacticGlueRule,
				translationRuleSignatureTriple, ruleGivenLabelGenerativeProbability, wordKeyMappingTable);
		ReorderingFeaturesSet reorderingFeaturesSet = reorderingFeatureSetCreater.createReorderingFeaturesSet(translationRuleSignatureTriple,
				wordKeyMappingTable);
		return new RuleFeaturesReordering(basicRuleFeaturesSet, labeledRuleFeaturesSet, reorderingFeaturesSet);
	}

	public static RuleFeaturesReordering createCorrectSizeUnknnownWordRuleRuleFeaturesReordering(SystemIdentity systemIdentity, boolean isHieroRule) {
		return new RuleFeaturesReordering(RuleFeaturesBasic.getCorrectSizeBasicRuleFeaturesSetUniformValues(systemIdentity,
				UnknownWordRulesCreatorPosTags.UNKNOWN_WORD_PROBABILITY), LabeledRuleFeaturesSet.getExtraLabeledRuleFeaturesUnknownWordRule(systemIdentity,
				isHieroRule), systemIdentity.getReorderingFeatureSetCreater().createAllNullReorderingFeatures());
	}

	public static RuleFeaturesReordering createCorrectSizeReorderingLabelToPhraseLabelSwitchRuleFeaturesReordering(SystemIdentity systemIdentity) {
		return new RuleFeaturesReordering(RuleFeaturesBasic.getCorrectSizeBasicRuleFeaturesSetUniformValues(systemIdentity, 1),
				LabeledRuleFeaturesSet.getExtraLabeledRuleFeaturesReorderingSwitchRuleRule(systemIdentity), systemIdentity.getReorderingFeatureSetCreater()
						.createAllNullReorderingFeatures());
	}

	public static RuleFeaturesReordering createCorrectSizeGlueRuleFeaturesReordering(SystemIdentity systemIdentity, double generativeGlueLabelProbability,
			boolean isHieroRule, double rarityPenalty) {
		return new RuleFeaturesReordering(RuleFeaturesBasic.getCorrectSizeBasicRuleFeaturesSetUniformValues(systemIdentity, 1),
				LabeledRuleFeaturesSet.createGlueRuleLabeledRuleFeatureSet(systemIdentity.getFeatureValueFormatter(), generativeGlueLabelProbability,
						isHieroRule, rarityPenalty), systemIdentity.getReorderingFeatureSetCreater().createAllNullReorderingFeatures());
	}

	public static RuleFeaturesReordering createRuleFeaturesReorderingZeroValues(SystemIdentity systemIdentity) {

		return new RuleFeaturesReordering(RuleFeaturesBasic.getCorrectSizeBasicRuleFeaturesSetUniformValues(systemIdentity, 1),
				LabeledRuleFeaturesSet.createGlueRuleLabeledRuleFeatureSet(systemIdentity.getFeatureValueFormatter(), 0, false, 0), systemIdentity
						.getReorderingFeatureSetCreater().createAllNullReorderingFeatures());
	}

	@Override
	public List<Float> getDenseFeatureValues() {
		List<Float> result = basicRuleFeatures.getFeatureValues();
		result.addAll(this.labeledRuleFeatures.getFeatureValues());
		result.addAll(NamedFeature.createUnlabeledFeatureValuesListJoshuaFormat(this.reorderingFeatureSet.getNamedFeatures()));
		return result;
	}

	@Override
	public String getCompressedFeaturesRepresentation() {
		String result = "";
		result += NamedFeature.createUnlabeledFeatureStringListOriginalFormat(getStandardFeaturesList());
		result += " " + this.reorderingFeatureSet.getCompressedReorderingFeaturesString();
		return result;
	}

	@Override
	public List<NamedFeature> getNamedFeaturesList() {
		List<NamedFeature> result = new ArrayList<NamedFeature>();
		result.addAll(this.getStandardFeaturesList());
		result.addAll(this.reorderingFeatureSet.getNamedFeatures());
		return result;
	}

	private static List<Double> extractLexicalProbabilityAndPhraseProbabilityFeatures(List<String> partsList, int numLexicalPlusPhraseFeatures) {
		List<Double> result = new ArrayList<Double>();
		for (int i = 0; i < numLexicalPlusPhraseFeatures; i++) {
			result.add(Double.parseDouble(partsList.get(i)));
		}
		return result;
	}

	public static List<String> extractLabeledRuleFeaturesStrings(List<String> partsList, int numLexicalPlusPhraseFeatures) {
		List<String> extraFeaturesStringList = partsList.subList(numLexicalPlusPhraseFeatures, numLexicalPlusPhraseFeatures
				+ LabeledRuleFeaturesSet.NO_EXTRA_LABELED_RULE_FEATURES);
		return extraFeaturesStringList;
	}

	public static List<String> extractReorderingFeaturesStrings(List<String> partsList, int numLexicalPlusPhraseFeatures) {
		int offSet = LabeledRuleFeaturesSet.NO_EXTRA_LABELED_RULE_FEATURES + numLexicalPlusPhraseFeatures;
		List<String> extraFeaturesStringList = partsList.subList(offSet, offSet + 7);
		return extraFeaturesStringList;
	}

	public static RuleFeaturesReordering createRuleFeaturesReorderingFromCompressedUnlabeledFeaturesString(String featuresString, SystemIdentity systemIdentity) {
		List<String> partsList = Arrays.asList(featuresString.split(" "));
		int numLexicalPlusPhraseFeatures = systemIdentity.getNumLexicalPlusPhraseProbabilityFeatures();

		// System.out.println("featureString: " + featuresString +
		// " partsList: " + partsList);
		BasicRuleFeaturesSet basicRuleFeaturesSet = RuleFeaturesBasic.createBasicRuleFeatureSetFromProbabilitiesList(systemIdentity,
				extractLexicalProbabilityAndPhraseProbabilityFeatures(partsList, numLexicalPlusPhraseFeatures));

		List<String> extraFeaturesStringList = extractLabeledRuleFeaturesStrings(partsList, numLexicalPlusPhraseFeatures);
		LabeledRuleFeaturesSet labeledRuleFeaturesSet = LabeledRuleFeaturesSet.createLabeledFeatureSetFromOrderedValuesStringList(
				systemIdentity.getFeatureValueFormatter(), extraFeaturesStringList);
		List<String> reorderingFeaturesStringList = extractReorderingFeaturesStrings(partsList, numLexicalPlusPhraseFeatures);
		FineHatReorderingFeaturesSet reorderingFeatureSet = FineHatReorderingFeaturesSet.createFineHatReorderingFeaturesSetFromFeatureStringsList(
				systemIdentity.getFeatureValueFormatter(), reorderingFeaturesStringList);
		return new RuleFeaturesReordering(basicRuleFeaturesSet, labeledRuleFeaturesSet, reorderingFeatureSet);
	}

	public static List<NamedFeature> getOrderedRepresentativeNamedFeatures(SystemIdentity systemIdentity) {
		List<NamedFeature> result = new ArrayList<NamedFeature>();
		result.addAll(RuleFeaturesBasic.getRepresentativeBasicZeroNamedFeaturesForConfiguration(systemIdentity));
		result.addAll(LabeledRuleFeaturesSet.getRepresentativeZeroNamedFeatures(systemIdentity));
		result.addAll(FineHatReorderingFeaturesSet.getRepresentativeZeroNamedFeatures(systemIdentity));
		return result;
	}

	@Override
	public List<NamedFeature> getNamedFeaturesListSparse() {
		List<NamedFeature> result = new ArrayList<NamedFeature>();
		result.addAll(this.getStandardFeaturesList());
		result.addAll(this.reorderingFeatureSet.getNamedFeaturesSparse());
		return result;
	}

	@Override
	public List<NamedFeature> getDiscriminativeNamedFeaturesList() {
		return this.reorderingFeatureSet.getNamedFeatures();
	}

	@Override
	public List<NamedFeature> getDenseNamedFeaturesList() {
		return getStandardFeaturesList();
	}

	@Override
	public TranslationRuleFeatures createCopyWithScaledProbabilities(double scalingFactor) {
		return new RuleFeaturesReordering(this.basicRuleFeatures.createScaledCopyBasicRuleFeatures(scalingFactor), this.labeledRuleFeatures,
				reorderingFeatureSet);
	}
}
