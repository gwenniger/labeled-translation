package grammarExtraction.reorderingLabeling;

import java.util.ArrayList;
import java.util.List;

import reorderingLabeling.ReorderingLabel;
import grammarExtraction.chiangGrammarExtraction.ReorderLabelingSettings;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.translationRuleTrie.FeatureEnrichedGrammarRuleCreator;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.TranslationRuleFeatures;

public class ReorderingLabelToPhraseLabelSwitchRuleCreater {

	private final WordKeyMappingTable wordKeyMappingTable;
	private final SystemIdentity systemIdentity;

	private ReorderingLabelToPhraseLabelSwitchRuleCreater(WordKeyMappingTable wordKeyMappingTable, SystemIdentity systemIdentity) {
		this.wordKeyMappingTable = wordKeyMappingTable;
		this.systemIdentity = systemIdentity;
	}

	private ReorderLabelingSettings getReorderLabelingSettings() {
		return this.systemIdentity.getReorderLabelingSettings();
	}

	public static ReorderingLabelToPhraseLabelSwitchRuleCreater createReorderingLabelToPhraseLabelSwitchRuleCreater(WordKeyMappingTable wordKeyMappingTable,
			SystemIdentity systemIdentity) {
		return new ReorderingLabelToPhraseLabelSwitchRuleCreater(wordKeyMappingTable, systemIdentity);
	}

	public boolean reorderingLabelToReorderingLabelSwitchRulesAreRequired() {
		return (this.systemIdentity.useReorderingLabelExtension() && (!getReorderLabelingSettings().reorderLabelPhrasePairs()));
	}

	private List<String> labelStringsList(List<ReorderingLabel> reorderingLabels) {
		List<String> result = new ArrayList<String>();
		for (ReorderingLabel reorderingLabel : reorderingLabels) {
			result.add(reorderingLabel.getLabel());
		}
		return result;
	}

	private boolean isNonPhraseReorderingLabel(String label) {
		return !(getReorderLabelingSettings().getReorderingLabelForReorderingLabelContainingString(label).isPhraseReorderingLabel());
	}

	private List<String> getNonPhraseReorderingLabelsFromWordKeyMappingTable() {
		List<String> result = new ArrayList<String>();
		for (String complexRuleLabel : wordKeyMappingTable.getComplexRuleAndGapLabelsWithoutEnclosingBrackets()) {
			if (isNonPhraseReorderingLabel(complexRuleLabel)) {
				result.add(complexRuleLabel);
			}
		}
		return result;
	}

	private String replaceAllReorderingLabelsWithPhraseLabel(String originalString) {
		String result = originalString;
		for (String reorderingLabel : labelStringsList(getReorderLabelingSettings().getReorderingLabelsForPhraseSwitchRules())) {
			result = result.replace(reorderingLabel, ReorderingLabel.getPhraseReorderingLabel());
		}
		return result;
	}

	public List<String> createSwitchRules(FeatureEnrichedGrammarRuleCreator featureEnrichedGrammarRuleCreator) {
		List<String> result = new ArrayList<String>();
		if (reorderingLabelToReorderingLabelSwitchRulesAreRequired()) {
			for (String nonPhraseReorderingLabel : getNonPhraseReorderingLabelsFromWordKeyMappingTable()) {
				result.add(createReorderingLabelToPhraseLabelSwitchRule(nonPhraseReorderingLabel, featureEnrichedGrammarRuleCreator));
			}
		}
		return result;
	}

	private String getSwitchRuleWeightsJoshua(FeatureEnrichedGrammarRuleCreator featureEnrichedGrammarRuleCreator) {
		TranslationRuleFeatures translationRuleFeatures = featureEnrichedGrammarRuleCreator.getReorderingLabelToPhraseLabelSwitchRuleFeatures(systemIdentity);
		return systemIdentity.getCorrectFormatFeaturesRepresentation(translationRuleFeatures);
	}

	private String createReorderingLabelToPhraseLabelSwitchRule(String reorderingLabel, FeatureEnrichedGrammarRuleCreator featureEnrichedGrammarRuleCreator) {
		String phraseReplacedReorderingLabel = replaceAllReorderingLabelsWithPhraseLabel(reorderingLabel);
		
		String result = this.systemIdentity.getTranslationRuleRepresentationCreater().getSwitchRuleForLabels(reorderingLabel, phraseReplacedReorderingLabel, getSwitchRuleWeightsJoshua(featureEnrichedGrammarRuleCreator));
		/*
		String nonterminalRightSide = RuleGap.ruleGapString(phraseReplacedReorderingLabel, 0);
		String result = JoshuaStyle.createJoshuaStyleRuleLabel(reorderingLabel) + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR + nonterminalRightSide
				+ JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR + nonterminalRightSide + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR
				+ getSwitchRuleWeightsJoshua(featureEnrichedGrammarRuleCreator);*/
		return result;
	}
	
	

}
