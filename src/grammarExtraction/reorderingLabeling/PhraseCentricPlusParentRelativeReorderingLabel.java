package grammarExtraction.reorderingLabeling;

import java.util.ArrayList;
import java.util.List;

import extended_bitg.EbitgInference;
import reorderingLabeling.FineParentRelativeReorderingLabel;
import reorderingLabeling.FinePhraseCentricReorderingLabelBasic;
import reorderingLabeling.ReorderingLabel;
import reorderingLabeling.ReorderingLabelCreater;

public class PhraseCentricPlusParentRelativeReorderingLabel extends ReorderingLabel {

    private static final long serialVersionUID = 1L;
    private static final String LABEL_SEPARATOR = "<<>>";
    private static final String HAT_AND_PARENT_RELATIVE_REORDERING_LABEL_NAME = "hatAndParentRelativeReorderingLabel";

    private PhraseCentricPlusParentRelativeReorderingLabel() {
	super(null);
    }

    protected PhraseCentricPlusParentRelativeReorderingLabel(String labelString) {
	super(labelString);
    }

    public static ReorderingLabelCreater createReorderingLabelCreater() {
	return new PhraseCentricPlusParentRelativeReorderingLabel();
    }

    public static String getDoubleLabelSeparator(){
	return LABEL_SEPARATOR;
    }
    
    public static String getLabelTypeName() {
	return HAT_AND_PARENT_RELATIVE_REORDERING_LABEL_NAME;
    }

    private static String getLabelString(ReorderingLabel phraseCentricReorderingLabel,
	    ReorderingLabel parentRelativeReorderingLabel) {
	return phraseCentricReorderingLabel.getLabel() + getDoubleLabelSeparator()
		+ parentRelativeReorderingLabel.getLabel();
    }

    public static PhraseCentricPlusParentRelativeReorderingLabel createPhraseCentricPlusParentRelativeReorderingLabel(
	    ReorderingLabel phraseCentricReorderingLabel,
	    ReorderingLabel parentRelativeReorderingLabel) {
	return new PhraseCentricPlusParentRelativeReorderingLabel(getLabelString(
		phraseCentricReorderingLabel, parentRelativeReorderingLabel));
    }

    @Override
    public List<ReorderingLabel> getReorderingLabelsConsideredForSmoothing() {
	return java.util.Collections.emptyList();
    }

    @Override
    public ReorderingLabel getReorderingLabelForInference(EbitgInference inference) {
	ReorderingLabel phraseCentricReorderingLabel = inference
		.getFinePhraseCentricReorderingLabel();
	ReorderingLabel parentRelativeReorderingLabel = inference
		.getParentRelativeReorderingLabel();
	return PhraseCentricPlusParentRelativeReorderingLabel
		.createPhraseCentricPlusParentRelativeReorderingLabel(phraseCentricReorderingLabel,
			parentRelativeReorderingLabel);
    }

    private FinePhraseCentricReorderingLabelBasic getPhraseCentricReorderingLabel() {
	return FinePhraseCentricReorderingLabelBasic.createFinePhraseCentricReorderingLabelBasic();
    }

    private FineParentRelativeReorderingLabel getParentRelativeReorderingLabel() {
	return FineParentRelativeReorderingLabel.createParentRelativeReorderingLabel();
    }

    private ReorderingLabel getFinePhraseCentricReorderingLabellForReorderingLabelContainingString(
	    String reorderingLabelContainingString) {
	return getPhraseCentricReorderingLabel()
		.getReorderingLabelForReorderingLabelContainingString(
			reorderingLabelContainingString);
    }

    private ReorderingLabel getParentRelativeReorderingLabelForReorderingLabelContainingString(
	    String reorderingLabelContainingString) {
	return getParentRelativeReorderingLabel()
		.getReorderingLabelForReorderingLabelContainingString(
			reorderingLabelContainingString);
    }

    @Override
    public ReorderingLabel getReorderingLabelForReorderingLabelContainingString(
	    String reorderingLabelContainingString) {
	String[] reorderingSubLabelArray = reorderingLabelContainingString.split(getDoubleLabelSeparator());
	ReorderingLabel phraseCentricReorderingLabel = getFinePhraseCentricReorderingLabellForReorderingLabelContainingString(reorderingSubLabelArray[0]);
	ReorderingLabel parentRelativeReorderingLabel = getParentRelativeReorderingLabelForReorderingLabelContainingString(reorderingSubLabelArray[1]);
	return PhraseCentricPlusParentRelativeReorderingLabel
		.createPhraseCentricPlusParentRelativeReorderingLabel(phraseCentricReorderingLabel,
			parentRelativeReorderingLabel);
    }

    @Override
    public ReorderingLabel createReorderingLabelFromLabelName(String labelName) {
	throw new RuntimeException("Not implemented");
    }

    @Override
    public int getNumberOfReorderingLabelsUsedForFineHatReorderingFeatures() {
	throw new RuntimeException("Not implemented");
    }

    @Override
    public int getNumberOfReorderingLabelsUsedForCoarseReorderingFeatures() {
	throw new RuntimeException("Not implemented");
    }

    @Override
    public int getHatFineReorderingFeaturesCategoryMappingForLabel() {
	throw new RuntimeException("Not implemented");
    }

    @Override
    public int getCoarseReorderingFeaturesCategoryMappingForLabel() {
	throw new RuntimeException("Not implemented");
    }

    @Override
    public List<ReorderingLabel> getAllReorderingLabelsExceptPhrase() {
	ArrayList<ReorderingLabel> result = new ArrayList<ReorderingLabel>();
	for (ReorderingLabel phraseCentricReorderingLabel : getPhraseCentricReorderingLabel()
		.getAllReorderingLabelsExceptPhrase()) {
	    for (ReorderingLabel parentRelativeReorderingLabel : getParentRelativeReorderingLabel()
		    .getAllReorderingLabelsExceptPhrase()) {
		result.add(createPhraseCentricPlusParentRelativeReorderingLabel(
			phraseCentricReorderingLabel, parentRelativeReorderingLabel));
	    }
	}
	return result;
    }

    @Override
    public boolean isBinaryReorderingLabel() {
	throw new RuntimeException("not implemented");
    }

    @Override
    public boolean producesDoubleReorderingLabels() {
	return true;
    }

}
