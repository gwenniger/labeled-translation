package grammarExtraction.reorderingLabeling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

public class SubPhrasePairRelativePermutationComputer {

	private final List<SortedSet<Integer>> sortedSourceSetPermutation;

	private SubPhrasePairRelativePermutationComputer(
			List<SortedSet<Integer>> sourcePermutation) {
		this.sortedSourceSetPermutation = sourcePermutation;
	}

	public static SubPhrasePairRelativePermutationComputer createSubPhrasePairRelativePermutationComputer(List<SortedSet<Integer>> sortedSourceSetPermutation)
	{
		return new SubPhrasePairRelativePermutationComputer(sortedSourceSetPermutation);
	}
	
	
	public List<SortedSet<Integer>> getSubPhrasePairsRelativePermutation(
			List<SortedSet<Integer>> sortedsourceSetPermutation) {
		List<SortedSet<Integer>> properPhrasePairsOnlyRelativePermutation = getProperPhrasePairSingletonsOnlySetList();
		return computeReNormalizedSetPermutation(properPhrasePairsOnlyRelativePermutation);
	}
	
	private Map<Integer, Integer> computeIndexRenormalizationMap(
			List<SortedSet<Integer>> setPermutationWithGaps) {
		Map<Integer, Integer> result = new HashMap<>();
		SortedSet<Integer> uniqueIndices = new TreeSet<Integer>();
		for (SortedSet<Integer> set : setPermutationWithGaps) {
			uniqueIndices.addAll(set);
		}
		int i = 0;
		for (Integer uniqueIndex : uniqueIndices) {
			result.put(uniqueIndex, i);
			i++;
		}
		return result;
	}

	private List<SortedSet<Integer>> computeReNormalizedSetPermutation(
			List<SortedSet<Integer>> setPermutationWithGaps) {
		List<SortedSet<Integer>> result = new ArrayList<SortedSet<Integer>>();
		Map<Integer, Integer> indexRenormalizationMap = computeIndexRenormalizationMap(setPermutationWithGaps);

		for (SortedSet<Integer> set : setPermutationWithGaps) {
			SortedSet<Integer> renormalizedSet = new TreeSet<>();
			for (Integer element : set) {
				renormalizedSet.add(indexRenormalizationMap.get(element));
			}
			result.add(renormalizedSet);
		}

		return result;
	}

	private Map<Integer, Integer> createIndexFrequencyMap() {
		Map<Integer, Integer> result = new HashMap<>();
		for (SortedSet<Integer> set : sortedSourceSetPermutation) {
			for (Integer element : set) {
				if (result.containsKey(element)) {
					int previousCount = result.get(element);
					result.put(element, previousCount + 1);
				} else {
					result.put(element, 1);
				}
			}
		}
		return result;
	}

	private boolean indexOccursOnlyOnceInPermutation(
			Map<Integer, Integer> indexFrequencyMap, Integer index) {
		return (indexFrequencyMap.get(index) == 1);
	}

	private boolean isProperPhrasePairTargetSet(SortedSet<Integer> targetSet,
			Map<Integer, Integer> indexFrequencyMap) {
		return ((targetSet.size() == 1) && (indexOccursOnlyOnceInPermutation(
				indexFrequencyMap, targetSet.first())));
	}

	private List<SortedSet<Integer>> getProperPhrasePairSingletonsOnlySetList() {
		Map<Integer, Integer> indexFrequencyMap = createIndexFrequencyMap();
		List<SortedSet<Integer>> result = new ArrayList<>();
		for (SortedSet<Integer> set : sortedSourceSetPermutation) {
			if (isProperPhrasePairTargetSet(set, indexFrequencyMap)) {
				result.add(set);
			}
		}
		return result;
	}

}
