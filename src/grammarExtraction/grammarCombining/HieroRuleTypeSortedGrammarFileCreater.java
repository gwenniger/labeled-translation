package grammarExtraction.grammarCombining;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import util.FileUtil;
import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;
import grammarExtraction.translationRules.TranslationRuleStringLabelStripper;

public class HieroRuleTypeSortedGrammarFileCreater {

    public static final String TYPE_SORTED_GRAMMAR_UNLABELED_PREAMBLE_SEPARATOR = " #### ";
    private static final String UNSORTED_SUFFIX = ".unsorted";

    private final LineIterator inputLineIterator;;
    private final BufferedWriter outputWriter;
    private final String unsortedResultFilePath;
    private final String sortedResultFilePath;
    private final TranslationRuleStringLabelStripper translationRuleStringLabelStripper;

    private HieroRuleTypeSortedGrammarFileCreater(LineIterator inputLineIterator,
	    BufferedWriter outputWriter, String unsortedResultFilePath,
	    String sortedResultFilePath, 
	    TranslationRuleStringLabelStripper translationRuleStringLabelStripper) {

	this.inputLineIterator = inputLineIterator;
	this.outputWriter = outputWriter;
	this.unsortedResultFilePath = unsortedResultFilePath;
	this.sortedResultFilePath = sortedResultFilePath;
	this.translationRuleStringLabelStripper = translationRuleStringLabelStripper;

    }

    public static HieroRuleTypeSortedGrammarFileCreater createHieroRuleTypeSortedGrammarFileCreater(
	    String inputFilePath, String outputFilePath) {

	try {
	    LineIterator inputLineIterator = FileUtils.lineIterator(new File(inputFilePath),
		    "UTF-8");
	    BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFilePath
		    + UNSORTED_SUFFIX));

	    return new HieroRuleTypeSortedGrammarFileCreater(inputLineIterator, outputWriter,
		    outputFilePath + UNSORTED_SUFFIX, outputFilePath,		  
		    TranslationRuleStringLabelStripper.createTranslationRuleStringLabelStripper());
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    public void createSortedOutputFile() {
	writeOutputFile();
	sortOutputFile();
    }

    private void writeOutputFile() {
	System.out
		.println("Create (unsorted) output file with rules plus their Hiero unlabeled preamble ... ");
	try {
	    while (inputLineIterator.hasNext()) {
		String nextLine = inputLineIterator.nextLine();
		outputWriter.write(createTypeSortedGrammarRuleString(nextLine) + "\n");
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	} finally {
	    FileUtil.closeCloseableIfNotNull(outputWriter);
	}
    }

    private void sortOutputFile() {
	System.out.println("Create sorted output file, by sorting the unsorted output file ...");
	//MultiThreadSort multiThreadSort = MultiThreadSort.createMultiThreadSort(NUM_SORT_TRHEADS,
	//	unsortedResultFilePath, sortedResultFilePath, temporaryResultsDirectoryPath);
	//multiThreadSort.sortLexicographically();
	
	List<String> grammarRulesUnsortedList = FileUtil.getSentences(new File(unsortedResultFilePath), true);
	
	ArrayList<String> grammarRulesUnsortedListWithoutEmptyLines = new ArrayList<String>();
	for(String ruleLine : grammarRulesUnsortedList){
	    //System.out.println("line: " + ruleLine);
	    if(!ruleLine.isEmpty()){
		grammarRulesUnsortedListWithoutEmptyLines.add(ruleLine);
	    }
	}	
	
	//See: http://stackoverflow.com/questions/5374311/convert-arrayliststring-to-string-array
	String[] grammarRulesUnsortedArray = new String[grammarRulesUnsortedListWithoutEmptyLines.size()];
	grammarRulesUnsortedArray = grammarRulesUnsortedListWithoutEmptyLines.toArray(grammarRulesUnsortedArray);	

	Arrays.parallelSort(grammarRulesUnsortedArray);
	
	
	FileUtil.writeLinesListToFile(Arrays.asList(grammarRulesUnsortedArray), sortedResultFilePath);	
    }

    /**
     * This method creates a rule representation for sorting on Hiero rule type,
     * by attaching the label-stripped version of the rule as a preamble.
     * 
     * @param ruleString
     * @return
     */
    private String createTypeSortedGrammarRuleString(String ruleString) {
	return createLabelStrippedRuleStringWithoutWeights(ruleString)
		+ TYPE_SORTED_GRAMMAR_UNLABELED_PREAMBLE_SEPARATOR + ruleString;
    }

    /**
     * Example input and output: Input: [ADJ:ADJ] ||| [ADJ:ADJ,1] und effective
     * ||| [ADJ:ADJ,1] and effective ||| -0.0 -0.0 -0.0 -0.0 2.9529245 1.0 0.0
     * 0.0 1.0 0.0 0.0 0.0 0.0 1.0 2.0 2.2044458 Output [X] ||| [X,1] und
     * effective ||| [X,1] and effective ||| -0.0 -0.0 -0.0 -0.0 2.9529245 1.0
     * 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 2.0 2.2044458
     * 
     * @param ruleString
     * @return
     */

    public String createLabelStrippedRuleStringWithoutWeights(String ruleString) {
	return translationRuleStringLabelStripper
		.getLabelStrippedRuleStringWithoutWeights(ruleString);
    }

    public static String getLabeledRuleWithoutUnlabeledHieroRulePreamble(
	    String labeledRuleWithUnlabeledHieroRulePreamble) {
	return labeledRuleWithUnlabeledHieroRulePreamble
		.split(TYPE_SORTED_GRAMMAR_UNLABELED_PREAMBLE_SEPARATOR)[1];
    }
    
    public static String getUnlabeledHieroRulePreamble(
	    String labeledRuleWithUnlabeledHieroRulePreamble) {
	return labeledRuleWithUnlabeledHieroRulePreamble
		.split(TYPE_SORTED_GRAMMAR_UNLABELED_PREAMBLE_SEPARATOR)[0];
    }
    
    public static String getWeightsOnlyFromLabeledRuleWithtUnlabeledHieroRulePreamble(
	    String labeledRuleWithUnlabeledHieroRulePreamble){
	String ruleWithoutUnlabeldPreamble = getLabeledRuleWithoutUnlabeledHieroRulePreamble(labeledRuleWithUnlabeledHieroRulePreamble);
	String[] ruleParts = JoshuaStyle.spitOnRuleSeparator(ruleWithoutUnlabeldPreamble);
	// The last rule part contains the Weights String
	String result = ruleParts[ruleParts.length - 1];
	return result;
    }
    

    public static void main(String[] args) {
	String ruleString = "[ADJ:ADJ] ||| [ADJ:ADJ,1] und effective ||| [ADJ:ADJ,1] and effective ||| -0.0 -0.0 -0.0 -0.0 2.9529245 1.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 2.0 2.2044458";
	HieroRuleTypeSortedGrammarFileCreater hieroRuleTypeSortedGrammarFileCreater = HieroRuleTypeSortedGrammarFileCreater
		.createHieroRuleTypeSortedGrammarFileCreater(null, null);
	String strippedRuleString = hieroRuleTypeSortedGrammarFileCreater
		.createLabelStrippedRuleStringWithoutWeights(ruleString);
	System.out.println("Rule String: " + ruleString);
	System.out.println("Stripped Rule String: " + strippedRuleString);
    }

}
