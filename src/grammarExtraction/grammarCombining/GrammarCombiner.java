package grammarExtraction.grammarCombining;

import grammarExtraction.chiangGrammarExtraction.ChiangRuleCreater;
import grammarExtraction.chiangGrammarExtraction.ChiangRuleGapCreater;
import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;
import grammarExtraction.reorderingLabeling.PhraseCentricPlusParentRelativeReorderingLabel;
import grammarExtraction.translationRules.RuleGap;
import grammarExtraction.translationRules.TranslationRuleStringLabelExtracter;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import util.FileStatistics;
import util.FileUtil;

public class GrammarCombiner {

    private static String UNKNOWN_WORD_LABEL = "UnknownWord";
    private static String UNKNOWN_WORD_PRE_NONTERMINAL_LABEL = "UnknownWordPreNonterminal";

    private static String COMBINER_TEMP_FILE_SUFFIX = ".GrammarCombinerTemporaryFile";
    private final List<String> inputGrammarFilePaths;
    private final String outputGrammarFilePath;
    private final String workDirPath;
    private final boolean requireHieroRuleWeightsAreSame;
    private final int indexOfGrammarToUseWeightsFrom;

    private GrammarCombiner(List<String> inputGrammarFilePaths, String outputGrammarFilePath,
	    String workDirPath, boolean requireHieroRuleWeightsAreSame,
	    int indexOfGrammarToUseWeightsFrom) {
	this.inputGrammarFilePaths = inputGrammarFilePaths;
	this.outputGrammarFilePath = outputGrammarFilePath;
	this.workDirPath = workDirPath;
	this.requireHieroRuleWeightsAreSame = requireHieroRuleWeightsAreSame;
	this.indexOfGrammarToUseWeightsFrom = indexOfGrammarToUseWeightsFrom;
    }

    public static GrammarCombiner createGrammarCombiner(List<String> inputGrammarFilePaths,
	    String outputGrammarFilePath, String workDirPath,
	    boolean requireHieroRuleWeightsAreSame, int indexOfGrammarToUseWeightsFrom) {

	return new GrammarCombiner(inputGrammarFilePaths, outputGrammarFilePath, workDirPath,
		requireHieroRuleWeightsAreSame, indexOfGrammarToUseWeightsFrom);
    }

    private String getInputFileName(String inputFilePath) {
	return new File(inputFilePath).getName();
    }

    private String grammarNumberSufix(int grammarNumber) {
	return "-" + grammarNumber;
    }

    /**
     * Produce a temporary Hiero-preamble sorted grammar file path. The path
     * consists of the work directory path, followed by the input name appended
     * with a grammar number and a combiner temporary file suffix.
     * 
     * @param inputFilePath
     * @param grammarNumber
     * @return
     */
    private String getCombinerTemporaryHieroPreambledSortedFilePath(String inputFilePath,
	    int grammarNumber) {
	/**
	 * We add a grammar number suffix to the temporary sorted grammar file
	 * path, because more often than not the input grammar files have the
	 * same name, so we need to distinguish them to avoid one overwriting
	 * the other.
	 */
	return workDirPath + getInputFileName(inputFilePath) + grammarNumberSufix(grammarNumber)
		+ COMBINER_TEMP_FILE_SUFFIX;
    }

    private void createCombinerTemporaryHieroPreambledAndSortedFiles() {

	for (int i = 0; i < inputGrammarFilePaths.size(); i++) {
	    String inputGrammarFilePath = inputGrammarFilePaths.get(i);

	    System.out.println(
		    "Create temporary Hiero-preamble sorted grammar for " + inputGrammarFilePath
			    + "\n in : \n" + getCombinerTemporaryHieroPreambledSortedFilePath(
				    inputGrammarFilePath, grammarNumber(i)));
	    HieroRuleTypeSortedGrammarFileCreater hieroRuleTypeSortedGrammarFileCreater = HieroRuleTypeSortedGrammarFileCreater
		    .createHieroRuleTypeSortedGrammarFileCreater(inputGrammarFilePath,
			    getCombinerTemporaryHieroPreambledSortedFilePath(inputGrammarFilePath,
				    grammarNumber(i)));
	    hieroRuleTypeSortedGrammarFileCreater.createSortedOutputFile();
	}
	checkAllTemporarySortedGrammarFilesHaveSameNumberOfLines();
    }

    private int grammarNumber(int grammarIndex) {
	return grammarIndex + 1;
    }

    private List<String> getCombinerTemporaryHieroPreambledSortedFilePaths(
	    List<String> inputGrammarFilePaths) {
	List<String> result = new ArrayList<String>();
	for (int i = 0; i < inputGrammarFilePaths.size(); i++) {
	    String inputGrammarFilePath = inputGrammarFilePaths.get(i);
	    result.add(getCombinerTemporaryHieroPreambledSortedFilePath(inputGrammarFilePath,
		    grammarNumber(i)));
	}
	return result;
    }

    private String getCombinerHieroPreambleSortedGrammarToTakeWeightsFrom() {
	String grammarPath = getCombinerTemporaryHieroPreambledSortedFilePath(
		inputGrammarFilePaths.get(indexOfGrammarToUseWeightsFrom),
		grammarNumber(indexOfGrammarToUseWeightsFrom));
	return grammarPath;
    }

    private boolean allTemporarySortedGrammarFilesExist() {
	boolean result = true;

	for (String temporaryHieroPreambledSortedFilePath : getCombinerTemporaryHieroPreambledSortedFilePaths(
		inputGrammarFilePaths)) {
	    File file = new File(temporaryHieroPreambledSortedFilePath);
	    if (!file.exists()) {
		result = false;
	    }
	}
	return result;
    }

    private void checkAllTemporarySortedGrammarFilesHaveSameNumberOfLines() {

	int previousNumberOfLines = -1;

	for (String temporaryHieroPreambledSortedFilePath : getCombinerTemporaryHieroPreambledSortedFilePaths(
		inputGrammarFilePaths)) {
	    try {
		int numLines = FileStatistics.countLines(temporaryHieroPreambledSortedFilePath);

		if ((previousNumberOfLines >= 0) && (numLines != previousNumberOfLines)) {
		    throw new RuntimeException("Unequal number of lines: " + numLines + " v.s. "
			    + previousNumberOfLines);
		}

	    } catch (IOException e) {
		e.printStackTrace();
		throw new RuntimeException(e);
	    }
	}

    }

    private List<LineIterator> createTempSortedGrammarFilesLineIterators() {
	List<LineIterator> lineIterators = new ArrayList<LineIterator>();

	try {
	    for (String temporaryHieroPreambledSortedFilePath : getCombinerTemporaryHieroPreambledSortedFilePaths(
		    inputGrammarFilePaths)) {
		LineIterator lineIterator = FileUtils
			.lineIterator(new File(temporaryHieroPreambledSortedFilePath), "UTF-8");
		lineIterators.add(lineIterator);
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
	return lineIterators;
    }

    private boolean isUnknowWordLabel(String label) {
	return label.equals(UNKNOWN_WORD_LABEL);
    }

    private boolean isUnknowWordPreNonterminalLabel(String label) {
	return label.equals(UNKNOWN_WORD_PRE_NONTERMINAL_LABEL);
    }

    private boolean labelIsUnknownWordOrUnknownWordPreTerminalLabel(String label) {
	return isUnknowWordLabel(label) || isUnknowWordPreNonterminalLabel(label);
    }

    private String produceCombinedLabel(List<List<String>> listOfLabelsLists, int labelIndex) {
	String combinedLabel = "";

	for (int labelsListIndex = 0; labelsListIndex < listOfLabelsLists
		.size(); labelsListIndex++) {
	    List<String> labelsList = listOfLabelsLists.get(labelsListIndex);

	    String label = labelsList.get(labelIndex);
	    
	    // For the special case of UnknowWord and UnknownWordPrenTerminal labels
	    // we don't want to combine the labels: we just want to keep the labels as they are in either
	    // grammar
	    if(labelIsUnknownWordOrUnknownWordPreTerminalLabel(label)){
		return label;
	    }
	    
	    if (labelsListIndex > 0) {
		combinedLabel += PhraseCentricPlusParentRelativeReorderingLabel
			.getDoubleLabelSeparator();
	    }
	    combinedLabel += label;
	}
	return combinedLabel;
    }

    /**
     * This method takes a list of lists of rule labelings, and combines it into
     * a list of combined labels, by concatenating the corresponding labels of
     * each sublist
     * 
     * @param listOfLabelsLists
     * @return
     */
    private List<String> produceCombinedLabelsList(List<List<String>> listOfLabelsLists) {
	List<String> result = new ArrayList<String>();

	int numLabels = listOfLabelsLists.get(0).size();

	for (int labelIndex = 0; labelIndex < numLabels; labelIndex++) {
	    result.add(produceCombinedLabel(listOfLabelsLists, labelIndex));
	}
	return result;
    }

    private void checkHieroRuleIdentitiesAreSame(String previousHieroRuleIdentity,
	    String newHieroRuleIdentity) {
	if (previousHieroRuleIdentity != null) {
	    if (!previousHieroRuleIdentity.equals(newHieroRuleIdentity)) {
		throw new RuntimeException("Error: hiero rule identities do not agree: "
			+ previousHieroRuleIdentity + " versus " + newHieroRuleIdentity);
	    }
	}
    }

    private void checkRuleWeightStringsAreSame(String previousRuleWeightString,
	    String newRuleWeightString) {
	if (previousRuleWeightString != null) {
	    if (!previousRuleWeightString.equals(newRuleWeightString)) {
		throw new RuntimeException("Error: hiero rule weight strings do not agree: "
			+ previousRuleWeightString + " versus " + newRuleWeightString);
	    }
	}
    }

    private String getRelabeledRuleLHS(String ruleLHS, List<String> combinedLabelsList) {
	String leftHandSideCombinedLabel = combinedLabelsList.get(0);
	String result = ruleLHS.replaceFirst(ChiangRuleCreater.ChiangLabel,
		leftHandSideCombinedLabel);
	return result;
    }

    private String getRelabeldRulRHSPart(String ruleRHS, List<String> combinedLabelsList) {
	String result = "";
	List<String> ruleParts = Arrays.asList(ruleRHS.split(" "));

	int index = 0;
	for (String rulePart : ruleParts) {
	    if (index > 0) {
		result += " ";
	    }

	    String rulePartReplaced = rulePart;
	    if (rulePart.startsWith("[") && rulePart.endsWith("")) {
		if (rulePart.equals(ChiangRuleGapCreater.chiangRuleGapString(0))) {
		    String combinedGapOneLabel = combinedLabelsList.get(1);
		    rulePartReplaced = RuleGap.ruleGapString(combinedGapOneLabel, 0);
		} else if (rulePart.equals(ChiangRuleGapCreater.chiangRuleGapString(1))) {
		    String combinedGapTwoLabel = combinedLabelsList.get(2);
		    rulePartReplaced = RuleGap.ruleGapString(combinedGapTwoLabel, 1);
		}
	    }

	    result += rulePartReplaced;
	    index++;
	}

	return result;
    }

    private String createRelabeledHieroRule(String hieroRuleIndentity,
	    List<String> combinedLabelsList) {
	String[] ruleParts = JoshuaStyle.spitOnRuleSeparator(hieroRuleIndentity);
	String result = getRelabeledRuleLHS(ruleParts[0], combinedLabelsList);
	result += JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR;
	result += getRelabeldRulRHSPart(ruleParts[1], combinedLabelsList);
	result += JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR;
	result += getRelabeldRulRHSPart(ruleParts[2], combinedLabelsList);

	return result;
    }

    private String createRelabeledHieroRuleWithWeights(String hieroRuleIndentity,
	    List<String> combinedLabelsList, String weightsString) {
	return createRelabeledHieroRule(hieroRuleIndentity, combinedLabelsList)
		+ JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR + weightsString;
    }

    private BufferedWriter createOutputGrammarFileWriter() {
	BufferedWriter outputGrammarFileWriter = null;
	try {
	    outputGrammarFileWriter = new BufferedWriter(new FileWriter(outputGrammarFilePath));

	} catch (IOException e) {
	    e.printStackTrace();
	    FileUtil.closeCloseableIfNotNull(outputGrammarFileWriter);
	    throw new RuntimeException(e);
	}
	return outputGrammarFileWriter;
    }

    private int getNumLinesFirstHieroPreambledSortedGrammar() {
	String filePath = getCombinerTemporaryHieroPreambledSortedFilePath(
		this.inputGrammarFilePaths.get(0), grammarNumber(0));
	try {
	    return FileStatistics.countLines(filePath);
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private void writeGrammar(String[] grammarRulesArray) {

	BufferedWriter outputGrammarFileWriter = createOutputGrammarFileWriter();

	for (int i = 0; i < grammarRulesArray.length; i++) {
	    String relabeledRuleWithWeights = grammarRulesArray[i];
	    try {
		// Write the relabeled rule to the output file
		outputGrammarFileWriter.write(relabeledRuleWithWeights + "\n");
	    } catch (IOException e) {
		e.printStackTrace();
		throw new RuntimeException(e);
	    }
	}
	// Finally close the output writer
	FileUtil.closeCloseableIfNotNull(outputGrammarFileWriter);
    }

    public void createCombinedResultGrammarFile() {

	if (allTemporarySortedGrammarFilesExist()) {
	    System.out.println(
		    "All temporary sorted grammar rule files exist. Skipping recreating them");
	} else {
	    createCombinerTemporaryHieroPreambledAndSortedFiles();
	}

	System.out.println("Create combined grammar result file...");
	System.out.println(
		"Using weights from " + getCombinerHieroPreambleSortedGrammarToTakeWeightsFrom());
	TranslationRuleStringLabelExtracter translationRuleStringLabelExtracter = new TranslationRuleStringLabelExtracter();

	List<LineIterator> tempSortedFileLineIterators = createTempSortedGrammarFilesLineIterators();

	LineIterator firstLineIterator = tempSortedFileLineIterators.get(0);

	// Store the rules of the combined grammar in a String array, so that
	// later we can
	// sort it efficiently using Arrays.parallelSort()
	String[] combinedLabelsGrammarRulesArray = new String[getNumLinesFirstHieroPreambledSortedGrammar()];

	int lineNumber = 0;
	while (firstLineIterator.hasNext()) {
	    List<List<String>> listOfLabelsLists = new ArrayList<List<String>>();

	    String hieroRuleIdentity = null;
	    String weightsString = null;

	    int grammarIndex = 0;
	    String weightStringToUse = null;

	    for (LineIterator lineIterator : tempSortedFileLineIterators) {
		String lineIteratorNextRuleLine = lineIterator.nextLine();

		String lineIteratorNextHieroRuleIdentity = HieroRuleTypeSortedGrammarFileCreater
			.getUnlabeledHieroRulePreamble(lineIteratorNextRuleLine);

		// Check that the previous Hiero rule identity (if any) agrees
		// with the current one
		// to make sure that corresponding rules are matched up
		checkHieroRuleIdentitiesAreSame(hieroRuleIdentity,
			lineIteratorNextHieroRuleIdentity);
		hieroRuleIdentity = lineIteratorNextHieroRuleIdentity;

		String lineIteratorNextRuleWeight = HieroRuleTypeSortedGrammarFileCreater
			.getWeightsOnlyFromLabeledRuleWithtUnlabeledHieroRulePreamble(
				lineIteratorNextRuleLine);

		// Check that the rule weights are constant
		if (requireHieroRuleWeightsAreSame) {
		    checkRuleWeightStringsAreSame(weightsString, lineIteratorNextRuleWeight);
		}
		weightsString = lineIteratorNextRuleWeight;

		String labeledRuleWithoutUnlabeledHieroRulePreamble = HieroRuleTypeSortedGrammarFileCreater
			.getLabeledRuleWithoutUnlabeledHieroRulePreamble(lineIteratorNextRuleLine);
		// 1. Extract labels from the labeled rule
		List<String> labelsList = translationRuleStringLabelExtracter
			.getRuleLabelsForRuleString(labeledRuleWithoutUnlabeledHieroRulePreamble);
		listOfLabelsLists.add(labelsList);

		if (grammarIndex == indexOfGrammarToUseWeightsFrom) {
		    weightStringToUse = lineIteratorNextRuleWeight;
		}

		grammarIndex++;
	    }

	    // 3. Recombine the labels of the lists into new labels
	    List<String> combinedLabelsList = produceCombinedLabelsList(listOfLabelsLists);

	    // for (String combinedLabel : combinedLabelsList) {
	    // System.out.println("Combined label: " + combinedLabel);
	    // }

	    // 4. Produce a new labeled rule, using the combined labels
	    String relabeledRuleWithWeights = createRelabeledHieroRuleWithWeights(hieroRuleIdentity,
		    combinedLabelsList, weightStringToUse);

	    // Store the relabeled rule in the result array
	    combinedLabelsGrammarRulesArray[lineNumber] = relabeledRuleWithWeights;
	    lineNumber++;
	}

	// Sort the results array
	Arrays.parallelSort(combinedLabelsGrammarRulesArray);

	// Finally write the sorted grammar to the output file
	writeGrammar(combinedLabelsGrammarRulesArray);

	System.out.println("done");
    }

    public static void main(String[] args) {
	if (args.length != 6) {
	    System.out.println(
		    "Usage: combineGrammars GRAMMAR1_FILE_PATH GRAMMAR2_FILE_PATH COMBINED_GRAMMAR_OUTPUT_FILE_PATH WORD_DIR_PATH REQUIRE_HIERO_WEIGHTS_ARE_SAME INDEX_OF_GRAMMAR_TO_USE_WEIGHTS_FROM");
	    System.exit(1);
	}
	String grammarOneFilePath = args[0];
	String grammarTwoFilePath = args[1];
	String outputGrammarFilePath = args[2];
	String workDirPath = args[3];
	boolean requireHieroRuleWeightsAreSame = Boolean.parseBoolean(args[4]);
	int indexOfGrammarToUseWeightsFrom = Integer.parseInt(args[5]);

	List<String> inputGrammarFilePaths = Arrays.asList(grammarOneFilePath, grammarTwoFilePath);

	GrammarCombiner grammarComginer = GrammarCombiner.createGrammarCombiner(
		inputGrammarFilePaths, outputGrammarFilePath, workDirPath,
		requireHieroRuleWeightsAreSame, indexOfGrammarToUseWeightsFrom);
	grammarComginer.createCombinedResultGrammarFile();
    }

}
