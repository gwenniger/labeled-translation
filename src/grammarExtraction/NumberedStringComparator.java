package grammarExtraction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import bitg.Pair;

public class NumberedStringComparator implements Comparator<Pair<String, Double>>
{

	@Override
	public int compare(Pair<String, Double> o1, Pair<String, Double> o2) 
	{
		return Double.compare((Double)o1.last, (Double)o2.last);
	}


	public static void main(String[] args)
	{
		List<Pair<String, Double>> elementsList = new ArrayList<>();
		elementsList.add(new Pair<String, Double>("c",-1.0));
		elementsList.add(new Pair<String, Double>("a",-3.0));
		elementsList.add(new Pair<String, Double>("b",-2.0));
		
		System.out.println("before sorting: "  + elementsList);
		for(Pair<String, Double> pair : elementsList)
		{
			System.out.println(pair.last);
		}
		elementsList.sort(Collections.reverseOrder(new NumberedStringComparator()));
		System.out.println("after sorting: " + elementsList);
		for(Pair<String, Double> pair : elementsList)
		{
			System.out.println(pair.last);
		}
		
		elementsList = new ArrayList<>();
		elementsList.add(new Pair<String, Double>("3",3.0));
		elementsList.add(new Pair<String, Double>("1",1.0));
		elementsList.add(new Pair<String, Double>("2",2.0));
		
		System.out.println("before sorting: "  + elementsList);
		for(Pair<String, Double> pair : elementsList)
		{
			System.out.println(pair.last);
		}
		elementsList.sort(Collections.reverseOrder(new NumberedStringComparator()));
		System.out.println("after sorting: " + elementsList);
		for(Pair<String, Double> pair : elementsList)
		{
			System.out.println(pair.last);
		}
	}
	

}
