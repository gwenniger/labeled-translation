package grammarExtraction.minimalPhrasePairExtraction;

import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import extended_bitg.EbitgChartBuilder;
import extended_bitg.EbitgChartBuilderBasic;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeState;
import extended_bitg.EbitgTreeStateBasic;
import util.Span;
import grammarExtraction.chiangGrammarExtraction.ChiangRuleCreater;
import grammarExtraction.chiangGrammarExtraction.ReorderLabelingSettings;
import grammarExtraction.grammarExtractionFoundation.HeavyWeightPhrasePairRuleSet;
import grammarExtraction.grammarExtractionFoundation.HeavyWeightRuleExtractor;
import grammarExtraction.translationRules.RuleLabelCreaterFactory;
import grammarExtraction.translationRules.TranslationRule;
import grammarExtraction.translationRules.TranslationRuleCreater;

public class HeavyWeightMinimalPhrasePairRuleExtractor<T extends EbitgLexicalizedInference, T2 extends EbitgTreeState<T, T2>> extends HeavyWeightRuleExtractor<T, T2> {

	protected HeavyWeightMinimalPhrasePairRuleExtractor(EbitgChartBuilder<T, T2> chartBuilder, TranslationRuleCreater<T> translationRuleCreater) {
		super(chartBuilder, translationRuleCreater);
	}

	public static HeavyWeightMinimalPhrasePairRuleExtractor<EbitgLexicalizedInference, EbitgTreeStateBasic> createHeavyWeightMinimalPhrasePairRuleExtractor(String sourceString, String targetString,
			String alignmentString, int maxAllowedInferencesPerNode, boolean useCaching) {
		EbitgChartBuilder<EbitgLexicalizedInference, EbitgTreeStateBasic> ebitgChartBuilder = EbitgChartBuilderBasic.createCompactHATsEbitgChartBuilder(sourceString, targetString, alignmentString,
				maxAllowedInferencesPerNode, HeavyWeightRuleExtractor.USE_GREEDY_NULL_BINDING, useCaching);
		boolean result = ebitgChartBuilder.findDerivationsAlignment();
		Assert.assertEquals(true, result);
		return new HeavyWeightMinimalPhrasePairRuleExtractor<EbitgLexicalizedInference, EbitgTreeStateBasic>(ebitgChartBuilder, new ChiangRuleCreater(
				ReorderLabelingSettings.createNoReorderingLabelingSettings(),RuleLabelCreaterFactory.createBothSidesLabeler()));
	}
	
	

	public List<HeavyWeightPhrasePairRuleSet<T>> createHeavyWeightMinimalTranslationRules() {
		List<HeavyWeightPhrasePairRuleSet<T>> result = new ArrayList<HeavyWeightPhrasePairRuleSet<T>>();

		MinimalPhrasePairExtractor<T, T2> minimalPhrasePairExtractor = MinimalPhrasePairExtractor.createMinimalPhrasePairExtractor(chartBuilder);
		List<EbitgLexicalizedInference> minimalPhrasePairInferences = minimalPhrasePairExtractor.getMimimalPhrasePairInferences();
		// System.out.println("HeavyWeightMinimalPhrasePairRuleExtractor minimalPhrasePairInferences: " + minimalPhrasePairInferences);

		for (EbitgLexicalizedInference minimalPhrasePair : minimalPhrasePairInferences) {
			HeavyWeightPhrasePairRuleSet<T> minimalPhrasePairRuleSet = HeavyWeightPhrasePairRuleSet.createMinimalPhrasePairRuleSet(minimalPhrasePair.getSourceSpan(), this);
			result.add(minimalPhrasePairRuleSet);
		}
		return result;
	}

	public TranslationRule<T> createBaseTranslationRule(Span absoluteSourceSpan) {
		T firstLegalInference = getNoNullsOnEdgesSatisfyingFirstCompleteInference(absoluteSourceSpan);

		if (firstLegalInference != null) {
			return translationRuleCreater.createBaseTranslationRuleLexicalizedHieroRules(getLexicalizer(), firstLegalInference);
		}
		return null;
	}

}
