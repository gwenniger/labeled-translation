package grammarExtraction.minimalPhrasePairExtraction;

import extended_bitg.EbitgLexicalizedInference;
import grammarExtraction.chiangGrammarExtraction.ChiangRuleExtracter;
import grammarExtraction.grammarExtractionFoundation.ExtractorThreadFactory;
import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;
import grammarExtraction.grammarExtractionFoundation.MultiThreadGrammarExtractor;
import grammarExtraction.grammarExtractionFoundation.RuleExtracter;
import grammarExtraction.grammarExtractionFoundation.TranslationRuleExtractorThread;
import grammarExtraction.labelSmoothing.EmptyAlternativelyLabeledRuleSetsListCreater;
import grammarExtraction.labelSmoothing.NoLabelSmoothingRulesSmoother;
import grammarExtraction.labelSmoothing.RuleLabelsSmoother;
import grammarExtraction.translationRuleTrie.LabelProbabilityEstimator;
import grammarExtraction.translationRuleTrie.LexicalProbabilityTables;
import grammarExtraction.translationRuleTrie.TranslationRuleCountTable;

import java.util.List;

import alignment.AlignmentStringTriple;
import alignmentStatistics.AlignmentTriplesEnumeration;
import alignmentStatistics.MultiThreadComputationParameters;
import util.Pair;

public class MinimalPhrasePairRuleExtractionThread extends TranslationRuleExtractorThread<AlignmentStringTriple> implements ExtractorThreadFactory<MinimalPhrasePairRuleExtractionThread,MultiThreadGrammarExtractor>
{

	private static final boolean USE_CACHING = true;
	final LexicalProbabilityTables lexicalProbabilityTables;
	final LabelProbabilityEstimator labelProbabilityEstimator;
	
	private MinimalPhrasePairRuleExtractionThread(RuleExtracter<? extends EbitgLexicalizedInference,AlignmentStringTriple> ruleExtracter,RuleLabelsSmoother ruleLabelsSmoother,
		LabelProbabilityEstimator labelProbabilityEstimator)
	{
		super(ruleExtracter,ruleLabelsSmoother);
		lexicalProbabilityTables = null;
		this.labelProbabilityEstimator = labelProbabilityEstimator;
	}
	
	public MinimalPhrasePairRuleExtractionThread(
			int maxAllowedInferencesPerNode,
			Pair<Integer> minMaxSentencePairNum, 
			AlignmentTriplesEnumeration alignmentTripleEnumeration,
			int maxSentenceLength, int outputLevel,
			TranslationRuleCountTable translationRuleProbabilityTable,
			boolean useJoshuaStyleSimplifiedCountComputing,
			boolean useCaching,
			RuleExtracter<? extends EbitgLexicalizedInference,AlignmentStringTriple> ruleExtracter,
			RuleLabelsSmoother ruleLabelsSmoother,LabelProbabilityEstimator labelProbabilityEstimator
		)
			
	{
		super(maxAllowedInferencesPerNode, minMaxSentencePairNum,
				alignmentTripleEnumeration,
				maxSentenceLength, outputLevel, translationRuleProbabilityTable.getTranslationRuleTrie(),
				translationRuleProbabilityTable.getWordKeyMappingTable(), useJoshuaStyleSimplifiedCountComputing,
				useCaching,
				ruleExtracter,ruleLabelsSmoother);
		
		this.lexicalProbabilityTables = translationRuleProbabilityTable.getLexicalProbabilityTables();
		this.labelProbabilityEstimator = labelProbabilityEstimator;
	}
	
	@Override
	protected List<LightWeightPhrasePairRuleSet> extractRuleSets(AlignmentStringTriple input)
	{
		MinimalPhrasePairRuleExtractor translationRuleExtractor = MinimalPhrasePairRuleExtractor.createMinimalPhrasePairRuleExtractor(input.getSourceString(),input.getTargetString(),input.getAlignmentString(), this.getMaxAllowedInferencesPerNode(),USE_CACHING);
		List<LightWeightPhrasePairRuleSet> ruleSets = translationRuleExtractor.createLightWeightMinimalTranslationRulesWithLexicalProabilities(getWordKeyMappingTable(), lexicalProbabilityTables);
		return ruleSets;
	}
	
	@Override
	public MinimalPhrasePairRuleExtractionThread createExtractorThread(Pair<Integer> minMaxSentencePairNum, MultiThreadGrammarExtractor multiThreadGrammarExtractor) 
	{
		AlignmentTriplesEnumeration alignmentTripleEnumeration  = AlignmentTriplesEnumeration.createAlignmentTriplesEnumeration(multiThreadGrammarExtractor.parameters.getSourceFileName(), multiThreadGrammarExtractor.parameters.getTargetFileName(),
				multiThreadGrammarExtractor.parameters.getAlignmentsFileName());
		return  new  MinimalPhrasePairRuleExtractionThread
				(
				multiThreadGrammarExtractor.parameters.getMaxAllowedInferencesPerNode(), minMaxSentencePairNum,alignmentTripleEnumeration,  multiThreadGrammarExtractor.parameters.getMaxSentenceLength(), multiThreadGrammarExtractor.parameters.getOutputLevel(), multiThreadGrammarExtractor.getTranslationRuleCountTable(),
				MultiThreadGrammarExtractor.UseJoshuaStyleSimplifiedCountComputing, multiThreadGrammarExtractor.parameters.getUseCaching(), 
				TranslationRuleExtractorThread.createChiangRuleExtracter(multiThreadGrammarExtractor.parameters,multiThreadGrammarExtractor.getChiangGrammarExtractionConstraints()),this.ruleLabelsSmoother, multiThreadGrammarExtractor.getTranslationRuleCountTable().getLabelProbabilityEstimator());
	}
	
	public static ExtractorThreadFactory<MinimalPhrasePairRuleExtractionThread,MultiThreadGrammarExtractor> createExtractorThreadFactory(ChiangRuleExtracter ruleExtracter,MultiThreadComputationParameters parameters) 
	{
		return new MinimalPhrasePairRuleExtractionThread(ruleExtracter, NoLabelSmoothingRulesSmoother.createNoLabelSmoothingRulesSmoother(parameters,null,EmptyAlternativelyLabeledRuleSetsListCreater.createEmptyAlternativelyLabeledRuleSetsListCreater(),EmptyAlternativelyLabeledRuleSetsListCreater.createEmptyAlternativelyLabeledRuleSetsListCreater(), false), null);
	}
	
    @Override
	protected void processPhrasePairRuleSet(LightWeightPhrasePairRuleSet ruleSet)
	{
		getTranslationRuleTrie().addPhrasePairRuleSetToTable(ruleSet,this.labelProbabilityEstimator);	
	}
}
