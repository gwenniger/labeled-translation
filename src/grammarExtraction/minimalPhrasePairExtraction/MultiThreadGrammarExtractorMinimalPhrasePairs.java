package grammarExtraction.minimalPhrasePairExtraction;

import grammarExtraction.chiangGrammarExtraction.ChiangGrammarExtractionConstraints;
import grammarExtraction.chiangGrammarExtraction.LabelProbabilityEstimatorHiero;
import grammarExtraction.grammarExtractionFoundation.MultiThreadGrammarExtractor;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.grammarExtractionFoundation.TranslationRuleExtractorThread;
import grammarExtraction.labelSmoothing.EmptyAlternativelyLabeledRuleSetsListCreater;
import grammarExtraction.labelSmoothing.NoLabelSmoothingRulesSmoother;
import grammarExtraction.samtGrammarExtraction.UnknownWordRulesCreator;
import grammarExtraction.samtGrammarExtraction.UnknownWordRulesCreatorEmpty;
import grammarExtraction.translationRuleTrie.LexicalProbabilityTables;
import grammarExtraction.translationRuleTrie.TranslationRuleCountTable;
import grammarExtraction.translationRuleTrie.TranslationRuleProbabilityTable;
import grammarExtraction.translationRuleTrie.TranslationRuleTrie;
import java.io.FileNotFoundException;

import mt_pipeline.MTPipelineHatsHiero;
import coreContextLabeling.CoreContextLabelsTable;
import coreContextLabeling.EbitgChartBuilderCoreContextLabeled;
import util.ConfigFile;
import util.Serialization;
import alignmentStatistics.MultiThreadComputationParameters;

public class MultiThreadGrammarExtractorMinimalPhrasePairs extends MultiThreadGrammarExtractor {

	public static final String MinimalPhrasePairTableFileNameConfigurationProperty = "minimalPhrasePairTable";

	protected MultiThreadGrammarExtractorMinimalPhrasePairs(MultiThreadComputationParameters parameters, TranslationRuleCountTable translationRuleProbabilityTable,
			ChiangGrammarExtractionConstraints chiangGrammarExtractionConstraints, UnknownWordRulesCreator unknownWordsRulesCreator, boolean writeProbabilityTablesToFiles,
			boolean printProbabilityTables, SystemIdentity systemIdentity) {
		super(parameters, translationRuleProbabilityTable, chiangGrammarExtractionConstraints, unknownWordsRulesCreator, writeProbabilityTablesToFiles, printProbabilityTables, systemIdentity);
	}

	public static MultiThreadGrammarExtractorMinimalPhrasePairs createMultiThreadGrammarExtractorMinimalPhrasePairs(ConfigFile theConfig, int numThreads, boolean countCollisions) {
		MultiThreadComputationParameters parameters = MultiThreadComputationParameters.createMultiThreadComputationParameters(theConfig, numThreads);

		SystemIdentity systemIdentity = SystemIdentity.createSystemIdentityJoshuaSystemWithStandardLabels(theConfig, MTPipelineHatsHiero.SystemName);

		NoLabelSmoothingRulesSmoother ruleLabelsSmoother = NoLabelSmoothingRulesSmoother.createNoLabelSmoothingRulesSmoother(parameters, null,EmptyAlternativelyLabeledRuleSetsListCreater.createEmptyAlternativelyLabeledRuleSetsListCreater(),EmptyAlternativelyLabeledRuleSetsListCreater.createEmptyAlternativelyLabeledRuleSetsListCreater(),false);
		TranslationRuleTrie translationRuleTrie = TranslationRuleTrie.createTranslationRuleTrie(ruleLabelsSmoother.getWordKeyMappingTable(), UseJoshuaStyleSimplifiedCountComputing, false, systemIdentity.getCanonicalFormSettings());
		LexicalProbabilityTables lexicalProbabilityTables = LexicalProbabilityTables.createLexicalProbabilityTables(ruleLabelsSmoother.getWordKeyMappingTable(), parameters.getNumThreads());
		ChiangGrammarExtractionConstraints chiangGrammarExtractionConstraints = ChiangGrammarExtractionConstraints.createStandardChiangGrammarExtractionConstraints(false);
		TranslationRuleCountTable translationRuleCountTable = TranslationRuleCountTable.createTranslationRuleCountTable(translationRuleTrie, lexicalProbabilityTables,
				ruleLabelsSmoother.getWordKeyMappingTable(), LabelProbabilityEstimatorHiero.createLabelProbabilityEstimatorHiero(), ruleLabelsSmoother);
		return new MultiThreadGrammarExtractorMinimalPhrasePairs(parameters, translationRuleCountTable, chiangGrammarExtractionConstraints, UnknownWordRulesCreatorEmpty.createUnknownWordRulesCreatorEmpty(), false, false,
				systemIdentity);

	}

	private static TranslationRuleProbabilityTable extractMinimalPhrasePairsTableAndWriteGrammar(ConfigFile configFile, String resultFilePath, int numThreads) {
		MultiThreadGrammarExtractorMinimalPhrasePairs grammarExtractor = createMultiThreadGrammarExtractorMinimalPhrasePairs(configFile, numThreads, false);
		TranslationRuleProbabilityTable result = grammarExtractor.extractGrammarMultiThreadAndWriteResultToFile(resultFilePath, true);
		return result;
	}

	private static TranslationRuleProbabilityTable extractMinimalPhrasePairsTable(ConfigFile configFile, int numThreads) {
		MultiThreadGrammarExtractorMinimalPhrasePairs grammarExtractor = createMultiThreadGrammarExtractorMinimalPhrasePairs(configFile, numThreads, false);
		TranslationRuleProbabilityTable result = grammarExtractor.createTranslationRuleProbabilityTable();
		return result;
	}

	private static TranslationRuleProbabilityTable extractAndSerializeMinimalPhrasePairsTable(ConfigFile configFile, String resultFilePath, int numThreads) {
		String serializationFileName = configFile.getValueWithPresenceCheck(MinimalPhrasePairTableFileNameConfigurationProperty);
		TranslationRuleProbabilityTable result = extractMinimalPhrasePairsTableAndWriteGrammar(configFile, resultFilePath, numThreads);
		System.out.println("Serialize minimal phrase pair grammar to file: " + serializationFileName);
		Serialization.serializeOBject(result, serializationFileName);
		System.out.println("Finished serialization");
		return result;
	}

	private static void serializeCoreContextLabelsTable(CoreContextLabelsTable coreContextLabelsTable, ConfigFile configFile) {
		String serializationFileName = CoreContextLabelsTable.getCoreContextLabelsTableFileName(configFile);
		System.out.println("Serialize core context labels table to file: " + serializationFileName);
		Serialization.serializeOBject(coreContextLabelsTable, serializationFileName);
		System.out.println("Finished serialization");
	}

	private static void extractAndSerialzeCoreContextLabelsTable(TranslationRuleProbabilityTable translationRuleProbabilityTable, ConfigFile configFile, int numThreads) {
		System.out.println("Compute Core Context labels table");
		CoreContextLabelsTable coreContextLabelsTable = CoreContextLabelsTable.createCoreContextLabelsTableGettingProbabilitySettingFromConfig(translationRuleProbabilityTable, numThreads, configFile);
		serializeCoreContextLabelsTable(coreContextLabelsTable, configFile);
	}

	public static TranslationRuleProbabilityTable extractGrammarMultiThread(String configFilePath, int numThreads, String resultFilePath) {
		try {
			ConfigFile configFile = new ConfigFile(configFilePath);

			if (!configFile.hasValue(EbitgChartBuilderCoreContextLabeled.CoreContextLabelTableFileNameConfigurationProperty)) {
				System.out.println("Error : Serialization file output location unspecified. \n" + "Please make sure the property "
						+ EbitgChartBuilderCoreContextLabeled.CoreContextLabelTableFileNameConfigurationProperty + " is specified properly in the config file");
			}
			TranslationRuleProbabilityTable result = extractAndSerializeMinimalPhrasePairsTable(configFile, resultFilePath, numThreads);
			extractAndSerialzeCoreContextLabelsTable(result, configFile, numThreads);
			return result;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Wrong config file name");
		}

	}

	public static TranslationRuleProbabilityTable extractCoreContextLabelsTableMultiThread(String configFilePath, int numThreads) {
		try {
			ConfigFile configFile = new ConfigFile(configFilePath);

			if (!configFile.hasValue(EbitgChartBuilderCoreContextLabeled.CoreContextLabelTableFileNameConfigurationProperty)) {
				System.out.println("Error : Serialization file output location unspecified. \n" + "Please make sure the property "
						+ EbitgChartBuilderCoreContextLabeled.CoreContextLabelTableFileNameConfigurationProperty + " is specified properly in the config file");
			}
			TranslationRuleProbabilityTable result = extractMinimalPhrasePairsTable(configFile, numThreads);
			extractAndSerialzeCoreContextLabelsTable(result, configFile, numThreads);
			return result;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Wrong config file name");
		}

	}

	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println("Wrong usage, uage: java -jar extractGrammarMultiThreadMinimalPhrasePairs configFileName numThreads");
			return;
		}

		String configFileName = args[0];
		int numThreads = Integer.parseInt(args[1]);
		extractGrammarMultiThread(configFileName, numThreads, "translationGrammarSourceFiltered.txt");
	}

	protected double performTrieFillingBothDirections() {
		return performTrieFilling(MinimalPhrasePairRuleExtractionThread.createExtractorThreadFactory(
				TranslationRuleExtractorThread.createChiangRuleExtracter(parameters, getChiangGrammarExtractionConstraints()), parameters), this);
	}

	@Override
	protected void performTrieFilling() {
		System.out.println(" MultiThreadGrammarExtractorMinimalPhrasePairs.performTrieFilling");
		double totalCountCreatedBySourceToTargetThreads = performTrieFillingBothDirections();
		double totalCountCreatedByTargetToSourceThreads = 0;
		getTranslationRuleCountTable().printExtractedRulesStatistics(totalCountCreatedBySourceToTargetThreads, totalCountCreatedByTargetToSourceThreads);

	}

	@Override
	protected boolean useExtraSAMTFeatures() {
		return false;
	}

}
