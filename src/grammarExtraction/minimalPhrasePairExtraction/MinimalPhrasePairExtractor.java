package grammarExtraction.minimalPhrasePairExtraction;

import extended_bitg.EbitgChart;
import extended_bitg.EbitgChartBuilder;
import extended_bitg.EbitgChartEntry;
import extended_bitg.EbitgInference;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeState;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import util.Pair;

public class MinimalPhrasePairExtractor<T extends EbitgLexicalizedInference,T2 extends EbitgTreeState<T, T2>> 
{
	private final EbitgChartBuilder<T,T2> chartBuilder;
		
	private MinimalPhrasePairExtractor(EbitgChartBuilder<T,T2> chartBuilder)
	{
		this.chartBuilder = chartBuilder;
	}
	
	private Stack<EbitgChartEntry> createInitiallChartEntryStack()
	{
		EbitgChart chart = this.chartBuilder.getChart();
		Stack<EbitgChartEntry> stack = new Stack<EbitgChartEntry>(); 
		stack.push(chart.getTopChartEntry());
		return stack;
	}
	
	private List<EbitgChartEntry> getAllFirstHATChartEntries()
	{
		List<EbitgChartEntry> result = new ArrayList<EbitgChartEntry>();
		
		Stack<EbitgChartEntry> stack = createInitiallChartEntryStack();
		
		while(!stack.isEmpty())
		{
			EbitgChartEntry chartEntryTop = stack.pop();
			result.add(chartEntryTop);
			
			if(!chartEntryTop.isPrimitive())
			{
				EbitgInference inference = chartEntryTop.getFirstCompleteMinimumTargetBindingInference();
				stack.addAll(inference.getNodeChartEntries());
			}
		}
		
		return result;
	}
	
	public List<EbitgLexicalizedInference> getMimimalPhrasePairInferences()
	{
		List<EbitgLexicalizedInference> result = new ArrayList<EbitgLexicalizedInference>();
		
		for(EbitgChartEntry chartEntry : getAllFirstHATChartEntries())
		{
			EbitgInference inference  = chartEntry.getFirstCompleteMinimumTargetBindingInference();
			
			if(inference != null)
			{
				if(inference.isMinimalPhrasePairInference())
				{
					EbitgLexicalizedInference lexicalizedInference = this.chartBuilder.getLexicalizer().createTightLexicalizedInference(inference);
					result.add(lexicalizedInference);
				}
			}
		}
		
		return result;
	}
	
	
	public static <T extends EbitgLexicalizedInference,T2 extends EbitgTreeState<T, T2>> MinimalPhrasePairExtractor<T,T2> createMinimalPhrasePairExtractor(EbitgChartBuilder<T,T2> chartBuilder)
	{
		assert(!chartBuilder.getLexicalizer().nullsAreIncludedDuringParsing());
		return new MinimalPhrasePairExtractor<T,T2>(chartBuilder);
	}
	

	public boolean chartEntryContainsCompleteSubPhrase(Pair<Integer> sourceSpan)
	{
		EbitgChartEntry chartEntry = chartBuilder.getChart().getChartEntry(sourceSpan.getFirst(), sourceSpan.getSecond());
		EbitgInference inference = chartEntry.getFirstCompleteMinimumTargetBindingInference();
		
		if(inference != null)
		{
			if(inference.containsCompleteSubPhrase())
			{
				return true;
			}
		}
		return false;
	}
	
	void printMinimalPhrasePairInferences()
	{
		System.out.println("Minimal phrase pair inferences:");
		for(EbitgInference inference: this.getMimimalPhrasePairInferences())
		{
			System.out.println(inference);
		}
	}
	
}	
