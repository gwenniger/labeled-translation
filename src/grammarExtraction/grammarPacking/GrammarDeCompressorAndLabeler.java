package grammarExtraction.grammarPacking;

import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.reorderingLabeling.RuleFeaturesReordering;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Enumeration;

import util.ConfigFile;
import util.FileUtil;
import util.Serialization;

public class GrammarDeCompressorAndLabeler implements Serializable, GrammarRuleStringsEnumerationProducer {
	private static final long serialVersionUID = 1L;
	private static final String GRAMMAR_RULE_STRINGS_CREATER_SUFFIX = "GrammarRuleStringsCreater.serialized";
	private final String inputGrammarFilePath;
	private final SystemIdentity systemIdentity;

	private GrammarDeCompressorAndLabeler(String inputGrammarFilePath, SystemIdentity systemIdentity) {
		this.inputGrammarFilePath = inputGrammarFilePath;
		this.systemIdentity = systemIdentity;
	}

	public static GrammarDeCompressorAndLabeler createGrammarDeCompressorAndLabeler(String inputGrammarFilePath, SystemIdentity systemIdentity) {
		return new GrammarDeCompressorAndLabeler(inputGrammarFilePath, systemIdentity);
	}

	public static GrammarDeCompressorAndLabeler createGrammarDeCompressorAndLabeler(String configFilePath, String inputGrammarFilePath, String systemName) {
		try {
			ConfigFile theConfig = new ConfigFile(configFilePath);
			SystemIdentity systemIdentity = SystemIdentity.createSystemIdentityWithStandardLabelingSettings(theConfig, systemName);
			return createGrammarDeCompressorAndLabeler(inputGrammarFilePath, systemIdentity);
		} catch (FileNotFoundException e) {
			throw new RuntimeException("Error: GrammarDeCompressorAndLabeler.createGrammarDeCompressorAndLabeler - inputGrammar " + inputGrammarFilePath + " not found");
		}
	}

	private static DecompressedGrammarRulesEnumeration createDecompressedGrammarRulesEnumeration(String inputGrammarFilePath, SystemIdentity systemIdentity) {
		String currentLine = null;
		BufferedReader inputGrammarReader = null;
		try {
			inputGrammarReader = new BufferedReader(new FileReader(inputGrammarFilePath));
			currentLine = inputGrammarReader.readLine();
			return new DecompressedGrammarRulesEnumeration(inputGrammarReader, systemIdentity, currentLine);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public void writeCompressedGrammarToFile(String outputGrammarFilePath) {
		BufferedWriter outputGrammarWriter = null;
		try {
			outputGrammarWriter = new BufferedWriter(new FileWriter(outputGrammarFilePath));
			DecompressedGrammarRulesEnumeration decompressedGrammarRulesEnumeration = createDecompressedGrammarRulesEnumeration(inputGrammarFilePath, systemIdentity);
			while (decompressedGrammarRulesEnumeration.hasMoreElements()) {
				String nextElement = decompressedGrammarRulesEnumeration.nextElement();
				outputGrammarWriter.write(nextElement);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			FileUtil.closeCloseableIfNotNull(outputGrammarWriter);
		}
	}

	public void writeCompressedGrammarToStandardOutput() {

		DecompressedGrammarRulesEnumeration decompressedGrammarRulesEnumeration = createDecompressedGrammarRulesEnumeration(inputGrammarFilePath, systemIdentity);
		while (decompressedGrammarRulesEnumeration.hasMoreElements()) {
			String nextElement = decompressedGrammarRulesEnumeration.nextElement();
			System.out.println(nextElement);
		}
	}

	public static void main(String[] args) {
		if (args.length != 3) {
			throw new RuntimeException("usage:  decrompressAndLabelGrammar ConfigFilePath InputGrammarFilePath SystemName");
		}
		GrammarDeCompressorAndLabeler grammarDeCompressorAndLabeler = createGrammarDeCompressorAndLabeler(args[0], args[1], args[2]);
		grammarDeCompressorAndLabeler.writeCompressedGrammarToStandardOutput();
		grammarDeCompressorAndLabeler.serialize(args[1] + GRAMMAR_RULE_STRINGS_CREATER_SUFFIX);
	}

	private static class DecompressedGrammarRulesEnumeration implements Enumeration<String> {
		BufferedReader inputGrammarReader;
		private final SystemIdentity systemIdentity;
		private String currentLine;

		private DecompressedGrammarRulesEnumeration(BufferedReader inputGrammarReader, SystemIdentity systemIdentity, String currentLine) {
			this.inputGrammarReader = inputGrammarReader;
			this.systemIdentity = systemIdentity;
			this.currentLine = currentLine;
		}

		@Override
		public boolean hasMoreElements() {
			return (this.currentLine != null);
		}

		@Override
		public String nextElement() {
			String tempLine = this.currentLine;
			try {
				this.currentLine = this.inputGrammarReader.readLine();
				if (this.currentLine == null) {
					FileUtil.closeCloseableIfNotNull(this.inputGrammarReader);
				}
				return computeDeCompressedGrammarRuleString(tempLine);
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}

		}

		private String computeDeCompressedGrammarRuleString(String compressedGrammarRuleString) {
			String[] ruleParts = JoshuaStyle.spitOnRuleSeparator(compressedGrammarRuleString);

			String result = "";
			for (int i = 0; i < ruleParts.length - 1; i++) {
				result += ruleParts[i] + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR;
			}
			String featuresContainingString = ruleParts[ruleParts.length - 1];
			result += computeDeCompressedFeaturesString(featuresContainingString) + "\n";
			// System.out.println("Decompression result: " + result);
			return result;
		}

		private String computeDeCompressedFeaturesString(String compressedFeaturesString) {
			RuleFeaturesReordering ruleFeaturesReordering = RuleFeaturesReordering.createRuleFeaturesReorderingFromCompressedUnlabeledFeaturesString(compressedFeaturesString, systemIdentity);
			return ruleFeaturesReordering.getSparseLabeledFeaturesRepresentation();
		}

	}

	@Override
	public Enumeration<String> produceTranslationRuleStringsEnumeration() {
		return createDecompressedGrammarRulesEnumeration(inputGrammarFilePath, systemIdentity);

	}

	public void serialize(String serializationFileName) {
		Serialization.serializeOBject(this, serializationFileName);
	}

	@Override
	public String getInputFileName() {
		return this.inputGrammarFilePath;
	}
}
