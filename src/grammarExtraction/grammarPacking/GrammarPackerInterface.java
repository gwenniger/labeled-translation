package grammarExtraction.grammarPacking;

import util.FileUtil;
import util.LinuxInteractor;
import mt_pipeline.mt_config.MTConfigFile;

public class GrammarPackerInterface {

	// private static final String DEFAULT_CLASS_PATH_STRING = " -cp . ";
	private static final String PACKER_PROGRAM_NAME = "joshua.tools.GrammarPacker";
	private final MTConfigFile theConfig;

	private GrammarPackerInterface(MTConfigFile theConfig) {
		this.theConfig = theConfig;
	}

	public static GrammarPackerInterface createGrammarPackerInterface(MTConfigFile theConfig) {
		return new GrammarPackerInterface(theConfig);
	}

	public String packGrammarPackingCommand(String packedGrammarOutputFolderPath, String grammarInputFilePath) {
		String commandString = theConfig.decoderConfig.baseJoshuaJavaCallWithExtendedClassPath() + " " + PACKER_PROGRAM_NAME + " -c " + theConfig.filesConfig.getPackerConfigFilePath() + " -p "
				+ packedGrammarOutputFolderPath + " -g " + grammarInputFilePath ;
				//+ " -m " + this.theConfig.filesConfig.getSavedConfigFilePath() + " -s " + this.theConfig.getSystemName();
		return commandString;
	}

	public void copyPackerConfigAndDenseMapToPackedGrammarFolder(String packedGrammarOutputFolderPath) {
		System.out.println("copyPackerConfigAndDenseMapToPackedGrammarFolder");
		String outputFilePath = packedGrammarOutputFolderPath + "/" + theConfig.filesConfig.getDenseMapFileName();
		FileUtil.copyFileNative(theConfig.filesConfig.getDenseMapPath(), outputFilePath, true);
		outputFilePath = packedGrammarOutputFolderPath + "/" + theConfig.filesConfig.getPackerConfigFileName();
		FileUtil.copyFileNative(theConfig.filesConfig.getPackerConfigFilePath(), outputFilePath, true);
	}

	public boolean packGrammar(String grammarInputFilePath, String packedGrammarOutputFolderPath) {
		String packingCommand = packGrammarPackingCommand(packedGrammarOutputFolderPath, grammarInputFilePath);
		System.out.println("packing command: " + packingCommand);
		boolean result = LinuxInteractor.executeExtendedCommandDisplayOutput(packingCommand);
		copyPackerConfigAndDenseMapToPackedGrammarFolder(packedGrammarOutputFolderPath);
		return result;
	}

	public void packGrammars() {
		String runningProgramClassPath = System.getProperty("java.class.path");
		System.out.println("running program classpath: " + runningProgramClassPath);

		// OutputWriting.createFolderIfNotExisting(theConfig.filesConfig.getPackedMainGrammarFolderTest());
		packGrammar(theConfig.filesConfig.getFilteredGrammarPathTest(), theConfig.filesConfig.getPackedMainGrammarFolderTest());
		packGrammar(theConfig.filesConfig.getFilteredGrammarPathMert(), theConfig.filesConfig.getPackedMainGrammarFolderMert());
		//packGrammar(theConfig.filesConfig.getGlueGrammarPathTest(), theConfig.filesConfig.getPackedGlueGrammarFolderTest());
		//packGrammar(theConfig.filesConfig.getGlueGrammarPathMert(), theConfig.filesConfig.getPackedGlueGrammarFolderMert());
	}

}