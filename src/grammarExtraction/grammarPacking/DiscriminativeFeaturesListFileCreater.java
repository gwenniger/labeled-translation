package grammarExtraction.grammarPacking;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import util.FileUtil;

import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;

public class DiscriminativeFeaturesListFileCreater {
	private final SystemIdentity systemIdentity;

	private DiscriminativeFeaturesListFileCreater(SystemIdentity systemIdentity) {
		this.systemIdentity = systemIdentity;
	}

	public static DiscriminativeFeaturesListFileCreater createDiscriminativeFeaturesListFileCreater(SystemIdentity systemIdentity) {
		return new DiscriminativeFeaturesListFileCreater(systemIdentity);
	}

	public void createDiscriminativeFeaturesListFile(String outputFilePath) {
		BufferedWriter outputWriter = null;
		try {
			outputWriter = new BufferedWriter(new FileWriter(outputFilePath));
			for (String featureName : systemIdentity.getOrderedDiscriminativeFeatureNames()) {
				outputWriter.write(featureName + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR + 0.0 + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			FileUtil.closeCloseableIfNotNull(outputWriter);
		}

	}

}
