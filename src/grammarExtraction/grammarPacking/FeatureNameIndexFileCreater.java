package grammarExtraction.grammarPacking;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import mt_pipeline.mt_config.MTConfigFile;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.phraseProbabilityWeights.NamedFeature;
import grammarExtraction.reorderingLabeling.RuleFeaturesReordering;
import util.FileUtil;
import util.Utility;

public class FeatureNameIndexFileCreater {

	private static final int CHUNK_SIZE = 250000;
	private final SystemIdentity systemIdentity;

	private FeatureNameIndexFileCreater(SystemIdentity systemIdentity) {
		this.systemIdentity = systemIdentity;
	}

	public static FeatureNameIndexFileCreater createFeatureNameIndexFileCreater(SystemIdentity systemIdentity) {
		return new FeatureNameIndexFileCreater(systemIdentity);
	}

	private Map<String, List<String>> getQuantizerNameFeatureNamesMap() {
		Map<String, List<String>> result = new HashMap<String, List<String>>();
		List<NamedFeature> representativeFeatures = RuleFeaturesReordering.getOrderedRepresentativeNamedFeatures(systemIdentity);

		for (NamedFeature representativeFeature : representativeFeatures) {
			String featureName = representativeFeature.getFeatureName();
			String quantizationString = representativeFeature.getJoshuaQuantizationTypeString();

			if (result.containsKey(quantizationString)) {
				List<String> featureNamesList = result.get(quantizationString);
				featureNamesList.add(featureName);
			} else {
				List<String> featureNamesList = new ArrayList<String>();
				featureNamesList.add(featureName);
				result.put(quantizationString, featureNamesList);
			}
		}
		return result;
	}

	private String createQuantizerListString(String quantizerName, List<String> featureNamesForQuantizer) {
		return "quantizer" + "\t" + quantizerName + "\t\t" + Utility.stringListStringWithoutBracketsCommaSeparated(featureNamesForQuantizer);
	}

	public static void createPackerSupportingFiles(MTConfigFile mtConfigFile, SystemIdentity systemIdentity) {
		FeatureNameIndexFileCreater featureNameIndexFileCreater = FeatureNameIndexFileCreater.createFeatureNameIndexFileCreater(systemIdentity);
		featureNameIndexFileCreater.createFeatureNameIndexMapFile(mtConfigFile.filesConfig.getDenseMapPath());
		featureNameIndexFileCreater.writePackerConfigFile(mtConfigFile.filesConfig.getPackerConfigFilePath());
	}

	public void writePackerConfigFile(String packerConfigFileName) {
		BufferedWriter fileWriter = null;
		try {
			fileWriter = new BufferedWriter(new FileWriter(packerConfigFileName));
			fileWriter.write("chunk_size\t\t" + CHUNK_SIZE + "\n\n");

			Map<String, List<String>> quantizerNameFeatureNamesMap = getQuantizerNameFeatureNamesMap();
			for (String quantizerName : quantizerNameFeatureNamesMap.keySet()) {
				String quantizerString = createQuantizerListString(quantizerName, quantizerNameFeatureNamesMap.get(quantizerName));
				System.out.println("quantizerString: " + quantizerString);
				fileWriter.write(quantizerString + "\n");
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			FileUtil.closeCloseableIfNotNull(fileWriter);
		}
	}

	public void createFeatureNameIndexMapFile(String denseMapeFileName) {
		BufferedWriter fileWriter = null;
		try {
			fileWriter = new BufferedWriter(new FileWriter(denseMapeFileName));

			int index = 0;
			for (String featureName : systemIdentity.getOrderedFeatureNames()) {
				fileWriter.write(index + "\t" + featureName + "\n");
				index++;
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			FileUtil.closeCloseableIfNotNull(fileWriter);
		}
	}

}
