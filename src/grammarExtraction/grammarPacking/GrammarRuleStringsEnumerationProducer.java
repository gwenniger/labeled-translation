package grammarExtraction.grammarPacking;

import java.util.Enumeration;

public interface GrammarRuleStringsEnumerationProducer {
	
	public Enumeration<String> produceTranslationRuleStringsEnumeration();
	public String getInputFileName();

}
