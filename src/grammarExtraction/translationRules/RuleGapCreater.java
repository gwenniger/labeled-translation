package grammarExtraction.translationRules;

import extended_bitg.EbitgLexicalizedInference;
import util.Pair;

public interface RuleGapCreater<T extends EbitgLexicalizedInference> 
{
	public RuleGap creatLexicalizedHieroRuleGap(Pair<Integer> relativeSourceSpan, Pair<Integer> relativeTargetSpan, int gapNumber,T gapInference);
	public RuleGap createAbstractRuleGap(Pair<Integer> relativeSourceSpan, Pair<Integer> relativeTargetSpan, int gapNumber,T gapInference);
}
