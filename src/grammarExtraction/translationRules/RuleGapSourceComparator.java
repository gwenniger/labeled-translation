package grammarExtraction.translationRules;


import java.util.Comparator;

public class RuleGapSourceComparator implements Comparator<RuleGap>
{

	@Override
	public int compare(RuleGap ruleGap1, RuleGap ruleGap2) 
	{
		if(ruleGap1.getMaxSourcePos() < ruleGap2.getMinSourcePos())
		{
			return -1;
		}
		else if(ruleGap2.getMaxSourcePos() <  ruleGap1.getMinSourcePos())
		{
			return 1;
		}
		else
		{	
			return 0;
		}	
	}

}
