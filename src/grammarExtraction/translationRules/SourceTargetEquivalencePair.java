package grammarExtraction.translationRules;

import grammarExtraction.IntegerKeyRuleRepresentation;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import util.Pair;

public class SourceTargetEquivalencePair implements Serializable {
	private static final long serialVersionUID = 1L;
	private final IntegerKeyRuleRepresentation sourceSideRepresentation;
	private final IntegerKeyRuleRepresentation targetSideRepresentation;

	private SourceTargetEquivalencePair(IntegerKeyRuleRepresentation sourceSideRepresentation, IntegerKeyRuleRepresentation targetSideRepresentation) {
		this.sourceSideRepresentation = sourceSideRepresentation;
		this.targetSideRepresentation = targetSideRepresentation;
	}

	public static SourceTargetEquivalencePair createSourceTargetEquivalencePair(IntegerKeyRuleRepresentation sourceSideRepresentation, IntegerKeyRuleRepresentation targetSideRepresentation) {
		return new SourceTargetEquivalencePair(sourceSideRepresentation, targetSideRepresentation);
	}

	public static SourceTargetEquivalencePair createSourceTargetEquivalencePair(List<String> sourceSideWords, List<String> targetSideWords, WordKeyMappingTable wordKeyMappingTable) {
		IntegerKeyRuleRepresentation sourceSideRepresentation = IntegerKeyRuleRepresentation.createIntegerKeyRuleRepresentation(sourceSideWords, wordKeyMappingTable);
		IntegerKeyRuleRepresentation targetSideRepresentation = IntegerKeyRuleRepresentation.createIntegerKeyRuleRepresentation(targetSideWords, wordKeyMappingTable);

		return new SourceTargetEquivalencePair(sourceSideRepresentation, targetSideRepresentation);
	}

	public SourceTargetEquivalencePair createSourceTargetEquivalencePairWithNewLabel(Integer newLabel) {
		IntegerKeyRuleRepresentation newSourceRepresentation = this.sourceSideRepresentation.createIntegerKeyRuleRepresentationWithNewLastElement(newLabel);
		IntegerKeyRuleRepresentation newTargetRepresentation = this.targetSideRepresentation.createIntegerKeyRuleRepresentationWithNewLastElement(newLabel);
		return createSourceTargetEquivalencePair(newSourceRepresentation, newTargetRepresentation);
	}

	private static Integer getAdaptedGapKey(Map<Integer, Pair<Integer>> gapIndexToNewLabelKeysPairMap, PairElementChooser<Integer> pairElementChooser, Integer originalKey, Integer gapIndex) {
		if (gapIndexToNewLabelKeysPairMap.containsKey(gapIndex)) {
			return pairElementChooser.getElementFromPair(gapIndexToNewLabelKeysPairMap.get(gapIndex));
		} else {
			return originalKey;
		}
	}

	private static List<Integer> createAdaptedGapLabelsKeysList(WordKeyMappingTable wordKeyMappingTable, List<Integer> originalKeys, Map<Integer, Pair<Integer>> gapIndexToNewLabelKeysPairMap,
			PairElementChooser<Integer> pairElementChooser) {
		List<Integer> adaptedKeys = new ArrayList<Integer>();
		for (Integer sourceKey : originalKeys) {
			if (wordKeyMappingTable.isLabelOneKey(sourceKey)) {
				adaptedKeys.add(getAdaptedGapKey(gapIndexToNewLabelKeysPairMap, pairElementChooser, sourceKey, 1));
			} else if (wordKeyMappingTable.isLabelTwoKey(sourceKey)) {
				adaptedKeys.add(getAdaptedGapKey(gapIndexToNewLabelKeysPairMap, pairElementChooser, sourceKey, 2));
			} else {
				adaptedKeys.add(sourceKey);
			}
		}
		return adaptedKeys;
	}

	public static Map<Integer, Integer> createGapPositionsMap(WordKeyMappingTable wordKeyMappingTable, List<Integer> keysList) {
		Map<Integer, Integer> result = new HashMap<Integer, Integer>();

		for (int i = 0; i < keysList.size(); i++) {
			int sourceKey = keysList.get(i);
			if (wordKeyMappingTable.isLabelOneKey(sourceKey)) {
				result.put(1, i);
			} else if (wordKeyMappingTable.isLabelTwoKey(sourceKey)) {
				result.put(2, i);
			}
		}
		return result;
	}

	public Map<Integer, Pair<Integer>> createGapPositionsPairsMap(WordKeyMappingTable wordKeyMappingTable) {
		Map<Integer, Pair<Integer>> result = new HashMap<Integer, Pair<Integer>>();
		Map<Integer, Integer> sourceGapsMap = createGapPositionsMap(wordKeyMappingTable, this.getSourceKeys());
		Map<Integer, Integer> targetGapsMap = createGapPositionsMap(wordKeyMappingTable, this.getTargetKeys());

		for (int gapNumber = 1; gapNumber <= 2; gapNumber++) {
			if (sourceGapsMap.containsKey(gapNumber) && targetGapsMap.containsKey(gapNumber)) {
				result.put(gapNumber, new Pair<Integer>(sourceGapsMap.get(gapNumber), targetGapsMap.get(gapNumber)));
			}
		}
		return result;

	}

	public SourceTargetEquivalencePair createSourceTargetEquivalencePairWithAdaptedGapLabels(WordKeyMappingTable wordKeyMappingTable, Map<Integer, Pair<Integer>> gapIndexToNewLabelKeysPairMap) {
		List<Integer> adaptedSourceKeys = createAdaptedGapLabelsKeysList(wordKeyMappingTable, this.getSourceKeys(), gapIndexToNewLabelKeysPairMap, new SourceElementChooser<Integer>());
		List<Integer> adaptedTargetKeys = createAdaptedGapLabelsKeysList(wordKeyMappingTable, this.getTargetKeys(), gapIndexToNewLabelKeysPairMap, new TargetElementChooser<Integer>());
		return SourceTargetEquivalencePair.createSourceTargetEquivalencePair(IntegerKeyRuleRepresentation.createIntegerKeyRuleRepresentation(adaptedSourceKeys),
				IntegerKeyRuleRepresentation.createIntegerKeyRuleRepresentation(adaptedTargetKeys));
	}

	public boolean equals(Object ruleObject) {
		if (ruleObject instanceof SourceTargetEquivalencePair) {
			SourceTargetEquivalencePair sourceTargetEquivalencePair = (SourceTargetEquivalencePair) ruleObject;
			if ((sourceTargetEquivalencePair.getSourceSideRepresentation()).equals(this.getSourceSideRepresentation())
					&& (sourceTargetEquivalencePair.getTargetSideRepresentation()).equals(this.getTargetSideRepresentation())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = ((getSourceSideRepresentation() == null) ? 0 : getSourceSideRepresentation().hashCode());
		result = result + ((int) Math.pow(prime, getSourceSideRepresentation().getPartKeys().size() * 1)) * ((getTargetSideRepresentation() == null) ? 0 : getTargetSideRepresentation().hashCode());
		return result;
	}

	public List<Integer> getSourceKeys() {
		return this.getSourceSideRepresentation().getPartKeys();
	}

	public List<Integer> getTargetKeys() {
		return this.getTargetSideRepresentation().getPartKeys();
	}

	public int getSourceKey(int index) {
		return this.getSourceSideRepresentation().getPartKeys().get(index);
	}

	public IntegerKeyRuleRepresentation getSourceSideRepresentation() {
		return sourceSideRepresentation;
	}

	public IntegerKeyRuleRepresentation getTargetSideRepresentation() {
		return targetSideRepresentation;
	}

	private static interface PairElementChooser<T> {
		public T getElementFromPair(Pair<T> pair);
	}

	private static class SourceElementChooser<T> implements PairElementChooser<T> {
		@Override
		public T getElementFromPair(Pair<T> pair) {
			return pair.getFirst();
		}
	}

	private static class TargetElementChooser<T> implements PairElementChooser<T> {
		@Override
		public T getElementFromPair(Pair<T> pair) {
			return pair.getSecond();
		}
	}
}
