package grammarExtraction.translationRules;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import util.Pair;
import grammarExtraction.IntegerKeyRuleRepresentationWithLabel;
import grammarExtraction.chiangGrammarExtraction.ChiangRuleCreater;
import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatterMoses;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

public class MosesRuleRepresentationCreater extends TranslationRuleRepresentationCreater {

	private static final String ALIGNMENT_SEPARATOR = "-";
	private static final String MOSES_SOURCE_LEFT_HAND_SIDE_LABEL = "[X]";
	private static final String MOSES_GLUE_TARGET_LEFT_HAND_SIDE_LABEL = "[S]";
	private static final String MOSES_TOP_SYMBOL = "[TOP]";
	private static final String TWO_GAPS_MONOTONE_ALIGNMENT_STRING = "0-0 1-1";
	private static final String START_RULE_GAP_ALIGNMENT_STRING = "";
	private static final String ONE_GAP_MONOTONE_ALIGNMENT_STRING = "0-0";
	private static final String ONE_GAP_MONOTONE_ALIGNMENT_START_RULE_STRING = "1-1";
	private static final Pair<Double> GLUE_PHRASE_PAIR_FREQEUNCIES = new Pair<Double>(0.0, 0.0);
	private static final String MOSES_START_OF_SENTENCE_SYMBOL = "<s>";
	private static final String MOSES_END_OF_SENTENCE_SYMBOL = "</s>";

	protected MosesRuleRepresentationCreater(FeatureValueFormatterMoses featureValueFormatterMoses, WordKeyMappingTable wordKeyMappingTable,
			TranslationRuleSignatureTriple translationRuleSignatureTriple) {
		super(featureValueFormatterMoses, wordKeyMappingTable, translationRuleSignatureTriple);
	}

	public static MosesRuleRepresentationCreater createMosesRuleRepresentationCreater(WordKeyMappingTable wordKeyMappingTable,
			TranslationRuleSignatureTriple translationRuleSignatureTriple) {
		return new MosesRuleRepresentationCreater(new FeatureValueFormatterMoses(), wordKeyMappingTable, translationRuleSignatureTriple);
	}

	public static MosesRuleRepresentationCreater createMosesRuleRepresentationCreaterFactory() {
		return new MosesRuleRepresentationCreater(new FeatureValueFormatterMoses(), null, null);
	}

	private String getMosesLeftHandSideLabelSource() {
		return MOSES_SOURCE_LEFT_HAND_SIDE_LABEL;
	}

	private String getMosesLeftHandSideLabelTarget(WordKeyMappingTable wordKeyMappingTable) {
		return this.getTargetSideRepresentationWithLabel().getLeftHandSideLabelStringRepresentation(wordKeyMappingTable);
	}

	private String getMosesRuleRepresentation(String sourceSideExcludingLeftHandSide, String targetSideExcludingLeftHandSide, String targetLeftHandSide) {

		String result = sourceSideExcludingLeftHandSide + " " + getMosesLeftHandSideLabelSource() + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR
				+ targetSideExcludingLeftHandSide + " " + targetLeftHandSide;
		return result;

	}

	private String getMosesRuleRepresentation(WordKeyMappingTable wordKeyMappingTable) {
		return getMosesRuleRepresentation(
				this.getSourceSideRepresentationWithLabel().getStringRepresentationExcludingLeftHandSideLabelMoses(wordKeyMappingTable), this
						.getTargetSideRepresentationWithLabel().getStringRepresentationExcludingLeftHandSideLabelMoses(wordKeyMappingTable),
				getMosesLeftHandSideLabelTarget(wordKeyMappingTable));
	}

	private Map<Integer, Integer> getPositionFirstAndSecondGap(IntegerKeyRuleRepresentationWithLabel integerKeyRuleRepresentationWithLabel) {

		Map<Integer, Integer> result = new HashMap<Integer, Integer>();

		List<String> rulePartStrings = integerKeyRuleRepresentationWithLabel.getStringListRepresentationExcludingLeftHandSideLabel(wordKeyMappingTable);
		for (int i = 0; i < rulePartStrings.size(); i++) {
			String rulePartString = rulePartStrings.get(i);
			if (WordKeyMappingTable.isLabelOne(rulePartString)) {
				result.put(0, i);
			} else if (WordKeyMappingTable.isLabelTwo(rulePartString)) {
				result.put(1, i);
			}
		}
		return result;
	}

	private String createGapAlignmentSubString(Map<Integer, Integer> gapPositionsSource, Map<Integer, Integer> gapPositionsTarget, int gapNumber) {
		String result = "";
		result += gapPositionsSource.get(gapNumber) + ALIGNMENT_SEPARATOR + gapPositionsTarget.get(gapNumber);
		return result;
	}

	private String getGapAlignmentString() {
		Map<Integer, Integer> gapPositionsSource = getPositionFirstAndSecondGap(this.translationRuleSignatureTriple.getSourceSideRepresentationWithLabel());
		Map<Integer, Integer> gapPositionsTarget = getPositionFirstAndSecondGap(this.translationRuleSignatureTriple.getTargetSideRepresentationWithLabel());
		String result = "";
		List<Integer> sortedGapNumbers = new ArrayList<Integer>(gapPositionsSource.keySet());
		// Sort using null as argument, to sort by natural order (default comparator)
		sortedGapNumbers.sort(null);

		int i;
		for (i = 0; i < sortedGapNumbers.size() - 1; i++) {
			result += createGapAlignmentSubString(gapPositionsSource, gapPositionsTarget, sortedGapNumbers.get(i)) + " ";
		}
		if (!sortedGapNumbers.isEmpty()) {
			result += createGapAlignmentSubString(gapPositionsSource, gapPositionsTarget, sortedGapNumbers.get(i));
		}
		return result;
	}

	@Override
	public String getTranslationRuleRepresentation() {
		// return "This will be a moses rule!!!";
		return getMosesRuleRepresentation(wordKeyMappingTable);
	}

	@Override
	public TranslationRuleRepresentationCreater createTranslationRuleRepresentationCreater(WordKeyMappingTable wordKeyMappingTable,
			TranslationRuleSignatureTriple translationRuleSignatureTriple) {
		return createMosesRuleRepresentationCreater(wordKeyMappingTable, translationRuleSignatureTriple);
	}

	private String gapAlignmentPlusNextSeparator(String gapAlignmentsString) {
		String gapAlignmentStringPlusSeparator = "";
		if (gapAlignmentsString.length() > 0) {
			gapAlignmentStringPlusSeparator = gapAlignmentsString + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR;
		} else {
			gapAlignmentStringPlusSeparator = "||| ";
		}
		return gapAlignmentStringPlusSeparator;
	}

	private String getTranslationRuleRepresentationWithFeatures(String mosesRuleRepresentation, String featuresString, Pair<Double> phrasePairFrequencies,
			String gapAlignmentsString) {
		return mosesRuleRepresentation + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR + featuresString + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR
				+ gapAlignmentPlusNextSeparator(gapAlignmentsString) + phrasePairFrequencies.getFirst() + " " + phrasePairFrequencies.getSecond();
	}

	@Override
	public String getTranslationRuleRepresentationWithFeatures(String featuresString, Pair<Double> phrasePairFrequencies) {
		return getTranslationRuleRepresentationWithFeatures(getMosesRuleRepresentation(wordKeyMappingTable), featuresString, phrasePairFrequencies,
				getGapAlignmentString());
	}

	/*
	 * private String getStartRuleRepresentationWithFeatures(String
	 * featuresString) { String nonterminalRightSide =
	 * MOSES_START_OF_SENTENCE_SYMBOL; String mosesRuleRepresentation =
	 * getMosesRuleRepresentation(nonterminalRightSide, nonterminalRightSide,
	 * MOSES_GLUE_TARGET_LEFT_HAND_SIDE_LABEL); String result =
	 * getTranslationRuleRepresentationWithFeatures(mosesRuleRepresentation,
	 * featuresString, GLUE_PHRASE_PAIR_FREQEUNCIES,
	 * START_RULE_GAP_ALIGNMENT_STRING); return result; }
	 * 
	 * private String getEndRuleRepresentationWithFeatures(String
	 * featuresString) { String nonterminalRightSide =
	 * RuleGap.ruleGapStringMosesStringToTree(SentenceLabel) + " " +
	 * MOSES_END_OF_SENTENCE_SYMBOL; String mosesRuleRepresentation =
	 * getMosesRuleRepresentation(nonterminalRightSide, nonterminalRightSide,
	 * MOSES_TOP_SYMBOL); String result =
	 * getTranslationRuleRepresentationWithFeatures(mosesRuleRepresentation,
	 * featuresString, GLUE_PHRASE_PAIR_FREQEUNCIES,
	 * ONE_GAP_MONOTONE_ALIGNMENT_STRING); return result; }
	 * 
	 * public String getGlueRuleRepresentationWithFeatures(String complexLabel,
	 * String featuresString) { String nonterminalRightSide =
	 * RuleGap.ruleGapStringMosesStringToTree(SentenceLabel) + " " +
	 * RuleGap.ruleGapStringMosesStringToTree(complexLabel); String
	 * mosesRuleRepresentation =
	 * getMosesRuleRepresentation(nonterminalRightSide, nonterminalRightSide,
	 * MOSES_GLUE_TARGET_LEFT_HAND_SIDE_LABEL); String result =
	 * getTranslationRuleRepresentationWithFeatures(mosesRuleRepresentation,
	 * featuresString, GLUE_PHRASE_PAIR_FREQEUNCIES,
	 * TWO_GAPS_MONOTONE_ALIGNMENT_STRING); return result; }
	 */

	private String getStartRuleRepresentationWithFeatures(String featuresString) {
		String nonterminalRightSide = MOSES_START_OF_SENTENCE_SYMBOL + " " + RuleGap.ruleGapStringMosesStringToTree(SentenceLabel);
		String mosesRuleRepresentation = getMosesRuleRepresentation(nonterminalRightSide, nonterminalRightSide, MOSES_TOP_SYMBOL);
		String result = getTranslationRuleRepresentationWithFeatures(mosesRuleRepresentation, featuresString, GLUE_PHRASE_PAIR_FREQEUNCIES,
				ONE_GAP_MONOTONE_ALIGNMENT_START_RULE_STRING);
		return result;
	}

	private String getEndRuleRepresentationWithFeatures(String featuresString) {
		String nonterminalRightSide = MOSES_END_OF_SENTENCE_SYMBOL;
		String mosesRuleRepresentation = getMosesRuleRepresentation(nonterminalRightSide, nonterminalRightSide, MOSES_GLUE_TARGET_LEFT_HAND_SIDE_LABEL);
		String result = getTranslationRuleRepresentationWithFeatures(mosesRuleRepresentation, featuresString, GLUE_PHRASE_PAIR_FREQEUNCIES,
				START_RULE_GAP_ALIGNMENT_STRING);
		return result;
	}

	public String getGlueRuleRepresentationWithFeatures(String complexLabel, String featuresString) {
		String nonterminalRightSide = RuleGap.ruleGapStringMosesStringToTree(complexLabel) + " " + RuleGap.ruleGapStringMosesStringToTree(SentenceLabel);
		String mosesRuleRepresentation = getMosesRuleRepresentation(nonterminalRightSide, nonterminalRightSide, MOSES_GLUE_TARGET_LEFT_HAND_SIDE_LABEL);
		String result = getTranslationRuleRepresentationWithFeatures(mosesRuleRepresentation, featuresString, GLUE_PHRASE_PAIR_FREQEUNCIES,
				TWO_GAPS_MONOTONE_ALIGNMENT_STRING);
		return result;
	}

	@Override
	public List<String> getGlueRulesForLabel(String complexLabel, String featuresString) {
		List<String> result = new ArrayList<String>();
		result.add(getGlueRuleRepresentationWithFeatures(complexLabel, featuresString));
		return result;
	}

	@Override
	public List<String> getStartAndEndRules(String featuresString) {
		List<String> result = new ArrayList<String>();
		result.add(getStartRuleRepresentationWithFeatures(featuresString));
		result.add(getEndRuleRepresentationWithFeatures(featuresString));
		return result;
	}

	private String squareBracketWrapped(String s) {
		return "[" + s + "]";
	}

	@Override
	public String getSwitchRuleForLabels(String leftHandSideLabel, String rightHandSideLabel, String featuresString) {
		String nonterminalRightSide = RuleGap.ruleGapStringMosesStringToTree(rightHandSideLabel);
		String mosesRuleRepresentation = getMosesRuleRepresentation(nonterminalRightSide, nonterminalRightSide, squareBracketWrapped(leftHandSideLabel));
		String result = getTranslationRuleRepresentationWithFeatures(mosesRuleRepresentation, featuresString, GLUE_PHRASE_PAIR_FREQEUNCIES,
				ONE_GAP_MONOTONE_ALIGNMENT_STRING);
		return result;
	}

	@Override
	public String getUnknownWordRuleForLabel(String complexLabel, String featuresString) {
		return getGlueRuleRepresentationWithFeatures(getUnknownWordsLabel(), featuresString);
	}

	@Override
	public String getUnknownWordsLabel() {
		return ChiangRuleCreater.ChiangLabel;
	}

}
