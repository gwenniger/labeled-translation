package grammarExtraction.translationRules;

import util.XMLTagging;

public class RuleLabel {
	private final String sourceSideLabel;
	private final String targetSideLabel;
	
	private RuleLabel(String sourceSideLabel, String targetSideLabel)
	{
		this.sourceSideLabel = sourceSideLabel;
		this.targetSideLabel = targetSideLabel;
	}
	
	public static RuleLabel createRuleLabel(String sourceSideLabel, String targetSideLabel)
	{
		return new RuleLabel(sourceSideLabel, targetSideLabel);
	}

	
	public static RuleLabel createBothSidesSameRuleLabel(String labelStringWithoutBrackets)
	{
		return new RuleLabel(labelStringWithoutBrackets, labelStringWithoutBrackets);
	}
	
	public String getSourceSideLabel() {
		return sourceSideLabel;
	}

	public String getTargetSideLabel() {
		return targetSideLabel;
	}

	public static final String ruleLabelString(String label) {
		return "[" + label + "]";
	}
	
	public String toString()
	{
		return XMLTagging.getTagSandwidchedString("RuleLabel","sourceSideLabel:" + sourceSideLabel + "_" + "targetSideLabel:" + targetSideLabel);
	}
	

}
