package grammarExtraction.translationRules;

import grammarExtraction.chiangGrammarExtraction.ChiangRuleCreater;
import grammarExtraction.chiangGrammarExtraction.ChiangRuleGapCreater;
import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

public class TranslationRuleStringLabelStripper {

	

	private TranslationRuleStringLabelStripper() {
	}

	public static TranslationRuleStringLabelStripper createTranslationRuleStringLabelStripper() {
		return new TranslationRuleStringLabelStripper();
	}

	private String getFlattenedRuleComponent(String ruleComponent) {
		if (WordKeyMappingTable.isLabel(ruleComponent)) {
			if (WordKeyMappingTable.isRuleLabel(ruleComponent)) {
				return RuleLabel.ruleLabelString(ChiangRuleCreater.ChiangLabel);
			} else if (WordKeyMappingTable.isLabelOne(ruleComponent)) {
				return ChiangRuleGapCreater.chiangRuleGapString(0);
			} else if (WordKeyMappingTable.isLabelTwo(ruleComponent)) {
				return ChiangRuleGapCreater.chiangRuleGapString(1);
			}
			throw new RuntimeException("Error: something is wrong - a label should be either ruleLabel or chiang gap label");
		} else {
			return ruleComponent;
		}
	}

	private String getLabelStrippedRulePart(String rulePart) {
		String[] components = rulePart.split("\\s");
		String result = "";
		for (int i = 0; i < components.length - 1; i++) {
			String component = components[i];
			result += getFlattenedRuleComponent(component) + " ";			
		}
		if (components.length > 0) {
			result += getFlattenedRuleComponent(components[components.length - 1]);
		}
		result = result.trim();
		return result;
	}

	private String getLabelStrippedRuleStringWithoutWeightsFromParts(String[] ruleParts) {
		String result = "";
		for (int i = 0; i < ruleParts.length - 1; i++) {
			String rulePart = ruleParts[i];
			// System.out.println("rulePart: " + rulePart);
			result += getLabelStrippedRulePart(rulePart) + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR;
		}

		return result;
	}

	public String getLabelStrippedRuleString(String originalRuleString) {
		String[] ruleParts = JoshuaStyle.spitOnRuleSeparator(originalRuleString);
		String result = getLabelStrippedRuleStringWithoutWeightsFromParts(ruleParts);
		result += ruleParts[ruleParts.length - 1];
		// System.out.println("result: " + result);
		return result;
	}

	public String getLabelStrippedRuleStringWithoutWeights(String originalRuleString) {

		String[] ruleParts = JoshuaStyle.spitOnRuleSeparator(originalRuleString);
		String result = getLabelStrippedRuleStringWithoutWeightsFromParts(ruleParts);
		// System.out.println("result: " + result);
		return result;
	}

	public String getOriginalRuleStringWithoutWeights(String originalRuleString) {

		String[] ruleParts = JoshuaStyle.spitOnRuleSeparator(originalRuleString);
		String result = "";
		for (int i = 0; i < ruleParts.length - 1; i++) {
			String rulePart = ruleParts[i];
			// System.out.println("rulePart: " + rulePart);
			result += rulePart + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR;
		}
		return result;
	}

}
