package grammarExtraction.translationRules;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

public class TranslationRuleStringLabelExtracter {

    private String getStringWithoutBrackets(String string) {
	return string.substring(1, string.length() - 1);
    }

    private String getLeftHandSideLabel(String leftHandSidePart) {
	String trimmedPart = leftHandSidePart.trim();

	if (WordKeyMappingTable.isRuleLabel(trimmedPart)) {
	    // Remove the first and last position, containing the brackets "["
	    // and "]"
	    return getStringWithoutBrackets(trimmedPart);
	} else {
	    throw new RuntimeException(
		    "Error: called  TranslationRuleStringLabelExtracter.getLeftHandSideLabel for non-lhs String");
	}
    }

    private int getLastCommaIndex(String string) {
	return string.lastIndexOf(",");
    }

    private String getLabelForRuleGapString(String ruleGapString) {
	String ruleGapStringWithoutBrackets = getStringWithoutBrackets(ruleGapString);
	int lastCommaIndex = getLastCommaIndex(ruleGapStringWithoutBrackets);
	String ruleLabelString = ruleGapStringWithoutBrackets.substring(0, lastCommaIndex);
	return ruleLabelString;
    }

    private boolean isRuleGapString(String ruleRHSPartString) {
	return WordKeyMappingTable.isLabelOne(ruleRHSPartString)
		|| WordKeyMappingTable.isLabelTwo(ruleRHSPartString);
    }

    public List<String> getLabelsForRHSPart(String ruleRHSComponent, boolean keepIndexAndBrackets) {
	List<String> ruleRHSParts = Arrays.asList(ruleRHSComponent.split("\\s"));
	
	List<String> result = new ArrayList<String>();

	for (String rulePart : ruleRHSParts) {
	    if (isRuleGapString(rulePart)) {
		if(keepIndexAndBrackets){
		    result.add(rulePart);
		}
		else{
		    result.add(getLabelForRuleGapString(rulePart));
		}  
	    }
	}
	return result;
    }

    /**
     * This method collects the labels from a rule, starting with the LHS label
     * and followed by the gap labels in order, if any
     * @param ruleString
     * @return
     */
    public List<String> getRuleLabelsForRuleString(String ruleString) {
	List<String> result = new ArrayList<String>();
	String[] ruleParts = JoshuaStyle.spitOnRuleSeparator(ruleString);

	// Add the label from the LHS rule part
	result.add(getLeftHandSideLabel(ruleParts[0]));
	// Add the labels from the source RHS part
	
	result.addAll(getLabelsForRHSPart(ruleParts[1],false));

	return result;

    }

    public static void main(String[] args) {
	TranslationRuleStringLabelExtracter translationRuleStringLabelExtracter = new TranslationRuleStringLabelExtracter();
	String ruleGapString = "[Complex,1]";

    }

}
