package grammarExtraction.translationRules;

import junit.framework.Assert;
import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

public class LightWeightRuleLabel {
	private final Integer sourceSideLabel;
	private final Integer targetSideLabel;

	private LightWeightRuleLabel(Integer sourceSideLabel, Integer targetSideLabel) {
		this.sourceSideLabel = sourceSideLabel;
		this.targetSideLabel = targetSideLabel;
		testCorrectnessLightWeightRuleLabel();
	}

	public static LightWeightRuleLabel createLightWeightRuleLabel(Integer sourceSideLabel, Integer targetSideLabel) {
		return new LightWeightRuleLabel(sourceSideLabel, targetSideLabel);
	}

	private void testCorrectnessLightWeightRuleLabel() {
		Assert.assertTrue(WordKeyMappingTable.isLightWeightRuleLabelKeyStatic(this.sourceSideLabel));
		Assert.assertTrue(WordKeyMappingTable.isLightWeightRuleLabelKeyStatic(this.targetSideLabel));
	}

	public Integer getSourceSideLabel() {
		return this.sourceSideLabel;
	}

	public Integer getTargetSideLabel() {
		return this.targetSideLabel;
	}

	private static Integer getLightWeightRuleLabelKey(String ruleLabel, WordKeyMappingTable wordKeyMappingTable) {
		return wordKeyMappingTable.getKeyForWord(ruleLabel);
	}

	/**
	 * Create a LightWeightRuleLabel from a RuleLabel, adding the square brackets around the label strings and 
	 * converting them to numbers using wordKeyMappingTable 
	 * @param ruleLabel
	 * @param wordKeyMappingTable
	 * @return
	 */
	public static LightWeightRuleLabel createLightWeightRuleLabel(RuleLabel ruleLabel, WordKeyMappingTable wordKeyMappingTable) {
		Integer ruleLabelSourceKey = getLightWeightRuleLabelKey(JoshuaStyle.createJoshuaStyleRuleLabel(ruleLabel.getSourceSideLabel()), wordKeyMappingTable);
		Integer ruleLabelTargetKey = getLightWeightRuleLabelKey(JoshuaStyle.createJoshuaStyleRuleLabel(ruleLabel.getTargetSideLabel()), wordKeyMappingTable);
		return LightWeightRuleLabel.createLightWeightRuleLabel(new Integer(ruleLabelSourceKey), new Integer(ruleLabelTargetKey));
	}

}
