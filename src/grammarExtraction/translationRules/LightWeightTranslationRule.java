package grammarExtraction.translationRules;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.Assert;
import util.Pair;
import util.XMLTagging;
import grammarExtraction.IntegerKeyRuleRepresentation;
import grammarExtraction.IntegerKeyRuleRepresentationWithLabel;
import grammarExtraction.translationRuleTrie.LexicalProbabilityTables;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

public class LightWeightTranslationRule implements Serializable {
	public static final String SOURCE_LABELS = "SOURCE-L";
	public static final String TARGET_LABELS = "TARGET-L";
	public static final String SOURCE_LABEL_PATTERN_STRING = XMLTagging.getTagSandwidchedString(getSourceLabelsPropertyTag(), ".*?");
	public static final Pattern SOURCE_LABEL_PATTERN = Pattern.compile(SOURCE_LABEL_PATTERN_STRING);
	public static final String TARGET_LABEL_PATTERN_STRING = XMLTagging.getTagSandwidchedString(getTargetLabelsPropertyTag(), ".*?");
	public static final Pattern TARGET_LABEL_PATTERN = Pattern.compile(TARGET_LABEL_PATTERN_STRING);
	private static final long serialVersionUID = 1L;
	protected final TranslationRuleSignatureTriple translationRuleSignatureTriple;
	private final double lexicalWeightSourceToTarget;
	private final double lexicalWeightTargetToSource;

	protected LightWeightTranslationRule(TranslationRuleSignatureTriple translationRuleSignatureTriple, double lexicalWeightSourceToTarget,
			double lexicalWeightTargetToSource) {
		this.translationRuleSignatureTriple = translationRuleSignatureTriple;
		Assert.assertNotNull(translationRuleSignatureTriple.getRuleLabel());
		this.lexicalWeightSourceToTarget = lexicalWeightSourceToTarget;
		this.lexicalWeightTargetToSource = lexicalWeightTargetToSource;
	}

	public LightWeightTranslationRule createLightWeightTranslationRuleWithNewLeftHandSideLabel(LightWeightRuleLabel newLabel) {
		TranslationRuleSignatureTriple translationRuleSignatureTriple = TranslationRuleSignatureTriple
				.createTranslationRuleSignatureTripleWithNewLeftHandSideLabel(this.translationRuleSignatureTriple, newLabel);
		return new LightWeightTranslationRule(translationRuleSignatureTriple, lexicalWeightSourceToTarget, lexicalWeightTargetToSource);
	}

	public LightWeightTranslationRule createLightWeightTranslationRuleWithAdaptedGapLabels(Map<Integer, Pair<Integer>> gapIndexToNewLabelKeysPairMap,
			WordKeyMappingTable wordKeyMappingTable) {
		TranslationRuleSignatureTriple translationRuleSignatureTriple = TranslationRuleSignatureTriple
				.createTranslationRuleSignatureTripleWithAdaptedGapLabels(this.translationRuleSignatureTriple, gapIndexToNewLabelKeysPairMap,
						wordKeyMappingTable);
		return new LightWeightTranslationRule(translationRuleSignatureTriple, lexicalWeightSourceToTarget, lexicalWeightTargetToSource);
	}

	public static LightWeightTranslationRule createLightWeightTranslationRule(TranslationRuleSignatureTriple translationRuleSignatureTriple,
			double lexicalWeightSourceToTarget, double lexicalWeightTargetToSource) {
		return new LightWeightTranslationRule(translationRuleSignatureTriple, lexicalWeightSourceToTarget, lexicalWeightTargetToSource);
	}

	public static LightWeightTranslationRule createLightWeightTranslationRule(IntegerKeyRuleRepresentation sourceSideRepresentation,
			IntegerKeyRuleRepresentation targetSideRepresentation, LightWeightRuleLabel ruleLabel, double lexicalWeightSourceToTarget, double lexicalWeightTargetToSource) {
		TranslationRuleSignatureTriple translationRuleSignatureTriple = TranslationRuleSignatureTriple.createTranslationRuleSignatureTriple(
				sourceSideRepresentation, targetSideRepresentation, ruleLabel);
		return new LightWeightTranslationRule(translationRuleSignatureTriple, lexicalWeightSourceToTarget, lexicalWeightTargetToSource);
	}

	public static LightWeightTranslationRule createLightWeightTranslationRuleWithoutLexicalWeights(IntegerKeyRuleRepresentation sourceSideRepresentation,
			IntegerKeyRuleRepresentation targetSideRepresentation, LightWeightRuleLabel ruleLabel) {
		TranslationRuleSignatureTriple translationRuleSignatureTriple = TranslationRuleSignatureTriple.createTranslationRuleSignatureTriple(
				sourceSideRepresentation, targetSideRepresentation, ruleLabel);
		return new LightWeightTranslationRule(translationRuleSignatureTriple, -1, -1);
	}

	public static LightWeightTranslationRule createLightWeightTranslationRule(TranslationRule<?> translationRule, WordKeyMappingTable wordKeyMappingTable) {
		IntegerKeyRuleRepresentation sourceSideRepresentation = IntegerKeyRuleRepresentation.createIntegerKeyRuleRepresentation(
				translationRule.sourceSideRuleStringList(), wordKeyMappingTable);
		IntegerKeyRuleRepresentation targetSideRepresentation = IntegerKeyRuleRepresentation.createIntegerKeyRuleRepresentation(
				translationRule.targetSideRuleStringList(), wordKeyMappingTable);
		// System.out.println("LightWeightTranslationRule.createLightWeightTranslationRule"
		// + translationRule);
		// assert(sourceSideRepresentation.partKeys.size() <=
		// HierarchicalPhraseBasedRuleExtractor.MaxTerminalsPlusNonTerminalsSourceSide);
		// assert(targetSideRepresentation.partKeys.size() <=
		// HierarchicalPhraseBasedRuleExtractor.MaxTerminalsPlusNonTerminalsSourceSide);

		TranslationRuleSignatureTriple translationRuleSignatureTriple = TranslationRuleSignatureTriple.createTranslationRuleSignatureTriple(
				sourceSideRepresentation, targetSideRepresentation,
				LightWeightRuleLabel.createLightWeightRuleLabel(translationRule.getRuleLabel(), wordKeyMappingTable));
		return new LightWeightTranslationRule(translationRuleSignatureTriple, -1, -1);
	}

	public static LightWeightTranslationRule createLightWeightTranslationRule(TranslationRule<?> translationRule, WordKeyMappingTable wordKeyMappingTable,
			LexicalProbabilityTables lexicalProbabilityTables) {
		IntegerKeyRuleRepresentation sourceSideRepresentation = IntegerKeyRuleRepresentation.createIntegerKeyRuleRepresentation(
				translationRule.sourceSideRuleStringList(), wordKeyMappingTable);
		IntegerKeyRuleRepresentation targetSideRepresentation = IntegerKeyRuleRepresentation.createIntegerKeyRuleRepresentation(
				translationRule.targetSideRuleStringList(), wordKeyMappingTable);

		double lexicalWeightSourceToTarget = translationRule.computeSourceToTargetLexicalWeight(lexicalProbabilityTables);
		double lexicalWeightTargetToSource = translationRule.computeTargetToSourceLexicalWeight(lexicalProbabilityTables);
		assert (lexicalWeightSourceToTarget >= 0);
		assert (lexicalWeightTargetToSource >= 0);

		// System.out.println("LightWeightTranslationRule.createLightWeightTranslationRule"
		// + translationRule);
		// assert(sourceSideRepresentation.partKeys.size() <=
		// HierarchicalPhraseBasedRuleExtractor.MaxTerminalsPlusNonTerminalsSourceSide);
		// assert(targetSideRepresentation.partKeys.size() <=
		// HierarchicalPhraseBasedRuleExtractor.MaxTerminalsPlusNonTerminalsSourceSide);

		TranslationRuleSignatureTriple translationRuleSignatureTriple = TranslationRuleSignatureTriple.createTranslationRuleSignatureTriple(
				sourceSideRepresentation, targetSideRepresentation,
				LightWeightRuleLabel.createLightWeightRuleLabel(translationRule.getRuleLabel(), wordKeyMappingTable));
		return new LightWeightTranslationRule(translationRuleSignatureTriple, lexicalWeightSourceToTarget, lexicalWeightTargetToSource);
	}

	public boolean equals(Object ruleObject) {
		return super.equals(ruleObject);
	}

	public double getLexicalWeightSourceToTarget() {
		return lexicalWeightSourceToTarget;
	}

	public double getLexicalWeightTargetToSource() {
		return lexicalWeightTargetToSource;
	}

	public String toString() {
		String result = "LightWeightTranslationRule: ";
		result += "\n" + this.getSourceSideRepresentation() + " , " + this.getTargetSideRepresentation();
		return result;
	}

	public LightWeightRuleLabel getLefthHandSideLabel() {
		return this.translationRuleSignatureTriple.getRuleLabel();
	}

	public boolean isSourceSideLabeledRule(WordKeyMappingTable wordKeyMappingTable) {
		String labelString = wordKeyMappingTable.getWordForKey(this.getLefthHandSideLabel().getSourceSideLabel());
		// System.out.println("Source label pattern string: " +
		// SOURCE_LABEL_PATTERN_STRING);
		// System.out.println("isSourceSideLabeledRule() -- labelString: " +
		// labelString);
		Matcher matcher = SOURCE_LABEL_PATTERN.matcher(labelString);
		return matcher.find();
	}

	public static String getSourceLabelsPropertyTag() {
		return LightWeightTranslationRule.SOURCE_LABELS;
	}

	public static String getTargetLabelsPropertyTag() {
		return LightWeightTranslationRule.TARGET_LABELS;
	}

	@Override
	public int hashCode() {
		return translationRuleSignatureTriple.hashCode();
	}

	public List<Integer> getSourceKeys() {
		return this.getSourceSideRepresentation().getPartKeys();
	}

	public List<Integer> getTargetKeys() {
		return translationRuleSignatureTriple.getTargetKeys();
	}

	public int getSourceKey(int index) {
		return translationRuleSignatureTriple.getSourceKey(index);
	}

	public IntegerKeyRuleRepresentation getSourceSideRepresentation() {
		return translationRuleSignatureTriple.getSourceSideRepresentation();
	}

	public IntegerKeyRuleRepresentation getTargetSideRepresentation() {
		return translationRuleSignatureTriple.getTargetSideRepresentation();
	}

	public TranslationRuleSignatureTriple getTranslationRuleSignatureTriple() {
		return this.translationRuleSignatureTriple;
	}

	public IntegerKeyRuleRepresentationWithLabel getSourceSideRepresentationWithLabel() {
		return this.translationRuleSignatureTriple.getSourceSideRepresentationWithLabel();
	}

	public IntegerKeyRuleRepresentationWithLabel getTargetSideRepresentationWithLabel() {
		return this.translationRuleSignatureTriple.getTargetSideRepresentationWithLabel();
	}

	public String getJoshuaRuleRepresentation(WordKeyMappingTable wordKeyMappingTable) {
		TranslationRuleRepresentationCreater translationRuleRepresentationCreater = JoshuaRuleRepresentationCreater.createJoshuaRuleRepresentationCreater(
				wordKeyMappingTable, translationRuleSignatureTriple, null);
		return translationRuleRepresentationCreater.getTranslationRuleRepresentation();
	}

	public String getRuleRepresentation(WordKeyMappingTable wordKeyMappingTable, TranslationRuleRepresentationCreater translationRuleRepresentationCreater,
			String ruleFeaturesString, Pair<Double> phrasePairFrequencies) {

		TranslationRuleRepresentationCreater translationRuleRepresentationCreaterSpecific = translationRuleRepresentationCreater
				.createTranslationRuleRepresentationCreater(wordKeyMappingTable, translationRuleSignatureTriple);
		return translationRuleRepresentationCreaterSpecific.getTranslationRuleRepresentationWithFeatures(ruleFeaturesString, phrasePairFrequencies);
	}

	public int countNumberOfXLabeledGaps(WordKeyMappingTable wordKeyMappingTable) {
		return this.translationRuleSignatureTriple.countNumberOfXLabeledGaps(wordKeyMappingTable);
	}
	
	public List<Integer> getSourceGapLabelKeys(WordKeyMappingTable wordKeyMappingTable){
	    return this.translationRuleSignatureTriple.getSourceGapLabelKeys(wordKeyMappingTable);
	}
	
	public List<Integer> getTargetGapLabelKeys(WordKeyMappingTable wordKeyMappingTable){
	    return this.translationRuleSignatureTriple.getTargetGapLabelKeys(wordKeyMappingTable);
	}
	
	public boolean isGlueStartOrEndRule(WordKeyMappingTable wordKeyMappingTable){
	    return wordKeyMappingTable.isGoalOrSentenceLeftHandSideLabelKey(getLefthHandSideLabel().getSourceSideLabel());
	}
	
	public boolean isPureHieroRule(WordKeyMappingTable wordKeyMappingTable){ 
	    return this.translationRuleSignatureTriple.isPureHieroRule(wordKeyMappingTable);
	}

}
