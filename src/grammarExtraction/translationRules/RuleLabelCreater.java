package grammarExtraction.translationRules;

public interface RuleLabelCreater {

	public RuleLabel createRuleLabelFromLabelString(String labelString);
	public boolean labelSourceSide();
	public boolean labelTargetSide();
	
}
