package grammarExtraction.translationRules;

import java.util.ArrayList;
import java.util.List;

import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

public class TranslationRuleGapInformation {

	protected final TranslationRuleSignatureTriple translationRuleSignatureTriple;
	protected final WordKeyMappingTable wordKeyMappingTable;

	private TranslationRuleGapInformation(TranslationRuleSignatureTriple translationRuleSignatureTriple, WordKeyMappingTable wordKeyMappingTable) {
		this.translationRuleSignatureTriple = translationRuleSignatureTriple;
		this.wordKeyMappingTable = wordKeyMappingTable;
	}

	public static TranslationRuleGapInformation createTranslationRuleGapInformation(TranslationRuleSignatureTriple translationRuleSignatureTriple,
			WordKeyMappingTable wordKeyMappingTable) {
		return new TranslationRuleGapInformation(translationRuleSignatureTriple, wordKeyMappingTable);
	}

	public List<Integer> getSourceGapKeys(WordKeyMappingTable wordKeyMappingTable) {
		List<Integer> result = new ArrayList<Integer>();
		for (Integer sourceKey : this.translationRuleSignatureTriple.getSourceKeys()) {
			if (wordKeyMappingTable.isGapLabelKey(sourceKey)) {
				result.add(sourceKey);
			}
		}
		return result;
	}

	public List<String> getSourceGapStrings(WordKeyMappingTable wordKeyMappingTable) {
		List<String> result = new ArrayList<String>();
		for (Integer sourceKey : this.translationRuleSignatureTriple.getSourceKeys()) {
			if (wordKeyMappingTable.isGapLabelKey(sourceKey)) {
				result.add(wordKeyMappingTable.getWordForKey(sourceKey));
			}
		}
		return result;
	}

	public List<Integer> getTargetGapKeys(WordKeyMappingTable wordKeyMappingTable) {
		List<Integer> result = new ArrayList<Integer>();
		for (Integer targetKey : this.translationRuleSignatureTriple.getTargetKeys()) {
			if (wordKeyMappingTable.isGapLabelKey(targetKey)) {
				result.add(targetKey);
			}
		}
		return result;
	}

	private Integer getFirstSourceGapKey(WordKeyMappingTable wordKeyMappingTable) {
		List<Integer> sourceGapKeys = this.getSourceGapKeys(wordKeyMappingTable);
		if (!sourceGapKeys.isEmpty()) {
			return sourceGapKeys.get(0);
		}
		return null;
	}

	private Integer getFirstTargetGapKey(WordKeyMappingTable wordKeyMappingTable) {
		List<Integer> targetGapKeys = this.getTargetGapKeys(wordKeyMappingTable);
		if (!targetGapKeys.isEmpty()) {
			return targetGapKeys.get(0);
		}
		return null;
	}

	public boolean ruleIsMonotonic() {

		Integer firstSourceGapKey = getFirstSourceGapKey(wordKeyMappingTable);
		Integer firstTargetGapKey = getFirstTargetGapKey(wordKeyMappingTable);
		if (firstSourceGapKey == null) {
			return true;
		} else {
			if (firstSourceGapKey.equals(firstTargetGapKey)) {
				return true;
			}
			return false;
		}
	}

}
