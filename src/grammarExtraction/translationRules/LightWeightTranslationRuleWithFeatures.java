package grammarExtraction.translationRules;

import grammarExtraction.chiangGrammarExtraction.RuleFeaturesBasic;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatter;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsSet;
import grammarExtraction.reorderingLabeling.ReorderingFeatureSetCreater;
import grammarExtraction.reorderingLabeling.RuleFeaturesReordering;
import grammarExtraction.samtGrammarExtraction.RuleFeaturesStandard;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

public class LightWeightTranslationRuleWithFeatures extends LightWeightTranslationRule {

	private static final long serialVersionUID = 1L;
	private final TranslationRuleFeatures translationRuleFeatures;
	private final boolean isAbstractRule;

	protected LightWeightTranslationRuleWithFeatures(TranslationRuleSignatureTriple translationRuleSignatureTriple, double lexicalWeightSourceToTarget,
			double lexicalWeightTargetToSource, TranslationRuleFeatures translationRuleFeatures, boolean isAbstractRule) {
		super(translationRuleSignatureTriple, lexicalWeightSourceToTarget, lexicalWeightTargetToSource);
		this.translationRuleFeatures = translationRuleFeatures;
		this.isAbstractRule = isAbstractRule;
	}

	public static LightWeightTranslationRuleWithFeatures createLightWeightTranslationRuleWithBasicFeatures(
			TranslationRuleSignatureTriple translationRuleSignatureTriple, PhraseProbabilityWeightsSet phraseProbabilityWeightsSet,
			double lexicalWeightTargetGivenSource, double lexicalWeightSourceGivenTarget, boolean isAbstractRule, FeatureValueFormatter featureValueFormatter) {
		TranslationRuleFeatures translationRuleFeatures = RuleFeaturesBasic.createRuleFeaturesBasic(phraseProbabilityWeightsSet,
				lexicalWeightTargetGivenSource, lexicalWeightSourceGivenTarget, featureValueFormatter);
		return new LightWeightTranslationRuleWithFeatures(translationRuleSignatureTriple, lexicalWeightTargetGivenSource, lexicalWeightSourceGivenTarget,
				translationRuleFeatures, isAbstractRule);
	}

	public static LightWeightTranslationRuleWithFeatures createLightWeightTranslationRuleWithStandardFeatures(double ruleGivenLabelGenerativeProbability,
			TranslationRuleSignatureTriple translationRuleSignatureTriple, PhraseProbabilityWeightsSet phraseProbabilityWeightsSet,
			double lexicalWeightTargetGivenSource, double lexicalWeightSourceGivenTarget, double ruleCorpusCount, boolean isHieroRule, boolean isHieroGlueRule,
			boolean isSyntacticGlueRule, WordKeyMappingTable wordKeyMappingTable, FeatureValueFormatter featureValueFormatter) {
		TranslationRuleFeatures translationRuleFeatures = RuleFeaturesStandard.createRuleFeaturesStandard(ruleGivenLabelGenerativeProbability,
				phraseProbabilityWeightsSet, lexicalWeightTargetGivenSource, lexicalWeightSourceGivenTarget, ruleCorpusCount, isHieroRule, isHieroGlueRule,
				isSyntacticGlueRule, translationRuleSignatureTriple, wordKeyMappingTable, featureValueFormatter);
		boolean isAbstractRule = translationRuleSignatureTriple.isAbstractRule(wordKeyMappingTable);
		return new LightWeightTranslationRuleWithFeatures(translationRuleSignatureTriple, lexicalWeightTargetGivenSource, lexicalWeightSourceGivenTarget,
				translationRuleFeatures, isAbstractRule);
	}

	public static LightWeightTranslationRuleWithFeatures createLightWeightTranslationRuleWithReorderingInformationFeatures(
			double ruleGivenLabelGenerativeProbability, TranslationRuleSignatureTriple translationRuleSignatureTriple,
			PhraseProbabilityWeightsSet phraseProbabilityWeightsSet, double lexicalWeightTargetGivenSource, double lexicalWeightSourceGivenTarget,
			double ruleCorpusCount, boolean isHieroRule, boolean isHieroGlueRule, boolean isSyntacticGlueRule, WordKeyMappingTable wordKeyMappingTable,
			ReorderingFeatureSetCreater reorderingFeatureSetCreater) {
		TranslationRuleFeatures translationRuleFeatures = RuleFeaturesReordering.createRuleFeaturesReordering(ruleGivenLabelGenerativeProbability,
				phraseProbabilityWeightsSet, lexicalWeightTargetGivenSource, lexicalWeightSourceGivenTarget, ruleCorpusCount, isHieroRule, isHieroGlueRule,
				isSyntacticGlueRule, translationRuleSignatureTriple, wordKeyMappingTable, reorderingFeatureSetCreater);
		boolean isAbstractRule = translationRuleSignatureTriple.isAbstractRule(wordKeyMappingTable);
		return new LightWeightTranslationRuleWithFeatures(translationRuleSignatureTriple, lexicalWeightTargetGivenSource, lexicalWeightSourceGivenTarget,
				translationRuleFeatures, isAbstractRule);
	}

	public LightWeightTranslationRuleWithFeatures createLightWeightTranslationRuleWithScaledProbabilities(double scalingFactor) {
		return new LightWeightTranslationRuleWithFeatures(this.translationRuleSignatureTriple, getLexicalWeightSourceToTarget(),
				getLexicalWeightTargetToSource(), translationRuleFeatures.createCopyWithScaledProbabilities(scalingFactor), isAbstractRule);
	}

	/*
	 * public String getJoshuaRuleRepresentation(WordKeyMappingTable
	 * wordKeyMappingTable) { String result =
	 * super.getJoshuaRuleRepresentation(wordKeyMappingTable) +
	 * JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR +
	 * translationRuleFeatures.getJoshuaOriginalFeaturesRepresentation(); return
	 * result; }
	 */

	public String getNormalRuleRepresentation(WordKeyMappingTable wordKeyMappingTable, TranslationRuleRepresentationCreater translationRuleRepresentationCreater) {
		String result = super.getRuleRepresentation(wordKeyMappingTable, translationRuleRepresentationCreater,
				translationRuleFeatures.getJoshuaOriginalFeaturesRepresentation(), translationRuleFeatures.getSourceAndTargetPhraseFrequency());
		return result;
	}

	public String getCompressedFeaturesRuleRepresentation(WordKeyMappingTable wordKeyMappingTable,
			TranslationRuleRepresentationCreater translationRuleRepresentationCreater) {
		String result = super.getRuleRepresentation(wordKeyMappingTable, translationRuleRepresentationCreater,
				translationRuleFeatures.getCompressedFeaturesRepresentation(), translationRuleFeatures.getSourceAndTargetPhraseFrequency());
		return result;
	}

	public String getSparseFeaturesRuleRepresentation(WordKeyMappingTable wordKeyMappingTable,
			TranslationRuleRepresentationCreater translationRuleRepresentationCreater) {
		String result = super.getRuleRepresentation(wordKeyMappingTable, translationRuleRepresentationCreater,
				translationRuleFeatures.getSparseLabeledFeaturesRepresentation(), translationRuleFeatures.getSourceAndTargetPhraseFrequency());
		return result;
	}

	public boolean isAbstractRule() {
		return this.isAbstractRule;
	}

}
