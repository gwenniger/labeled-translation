package grammarExtraction.translationRules;

import extended_bitg.EbitgLexicalizedInference;
import util.Span;
import hat_lexicalization.Lexicalizer;

import java.util.List;

import alignment.AlignmentPair;

public class TranslationRuleLexicalProperties 
{
	private final Span sourceSpan;
	private final Span targetSpan;
	private final List<String> sourceWords;
	private final List<String> targetWords;
	private final List<AlignmentPair> alignments;
	
	private TranslationRuleLexicalProperties(Span sourceSpan,Span targetSpan, List<String> sourceWords, List<String> targetWords, List<AlignmentPair> alignments)
	{
		this.sourceSpan = sourceSpan;
		this.targetSpan = targetSpan;
		this.sourceWords = sourceWords;
		this.targetWords = targetWords;
		this.alignments = alignments;
	}

	public TranslationRuleLexicalProperties(TranslationRuleLexicalProperties toCopy)
	{
		this.sourceSpan = toCopy.sourceSpan;
		this.targetSpan = toCopy.targetSpan;
		this.sourceWords = toCopy.sourceWords;
		this.targetWords = toCopy.targetWords;
		this.alignments = toCopy.alignments;
	}
	
	public static TranslationRuleLexicalProperties createTranslationRuleLexicalProperties(Lexicalizer lexicalizer, EbitgLexicalizedInference ruleInference)
	{
		return new TranslationRuleLexicalProperties(getsSourceSpan(ruleInference), getsTargetSpan(ruleInference), getSourceWords(lexicalizer, ruleInference), getTargetWords(lexicalizer, ruleInference), getAlignments(lexicalizer, ruleInference));
	}
	
	
	public Span getSourceSpan()
	{
		return sourceSpan;
	}
	
	public Span getTargetSpan()
	{
		return targetSpan;
	}

	public List<String> getSourceWords()
	{
		return sourceWords;
	}
	
	public List<String> getTargetWords()
	{
		return targetWords;
	}
	
	public List<AlignmentPair> getAlignments()
	{
		return alignments;
	}
	
	private static Span getsSourceSpan(EbitgLexicalizedInference ruleInference)
	{
		return ruleInference.getSourceSpan();
	}

	private static Span getsTargetSpan(EbitgLexicalizedInference ruleInference)
	{
		return ruleInference.getTargetSpan();
	}
	
	private static List<String> getSourceWords(Lexicalizer lexicalizer, EbitgLexicalizedInference ruleInference)
	{
		Span sourceSpan = getsSourceSpan(ruleInference);
		List<String> sourceWords = lexicalizer.getSourceWords().subList(sourceSpan.getFirst(), sourceSpan.getSecond() + 1);
		return sourceWords;
	}
	
	private static List<String> getTargetWords(Lexicalizer lexicalizer, EbitgLexicalizedInference ruleInference)
	{
		Span targetSpan = getsTargetSpan(ruleInference);
		//System.out.println(">>>>TranslationRuleLexicalProperties : Target span: " + targetSpan);
		//System.out.println(">>>>TranslationRuleLexicalProperties :  lexicalizer.getTargetWords() " +  lexicalizer.getTargetWords());
		List<String> sourceWords = lexicalizer.getTargetWords().subList(targetSpan.getFirst(), targetSpan.getSecond() + 1);
		//System.out.println(">>>>TranslationRuleLexicalProperties : Target Words : " + sourceWords);
		return sourceWords;
	}
	private static List<AlignmentPair> getAlignments(Lexicalizer lexicalizer,EbitgLexicalizedInference ruleInference)
	{
		Span sourceSpan = getsSourceSpan(ruleInference);
		return lexicalizer.getOriginalSentencePairAlignmentChunking().getSourceSpanCoveredAlignments(sourceSpan);
	}
}