package grammarExtraction.translationRules;

import extended_bitg.EbitgLexicalizedInference;
import hat_lexicalization.Lexicalizer;

public interface TranslationRuleCreater<T extends EbitgLexicalizedInference> {
	public TranslationRule<T> createBaseTranslationRuleLexicalizedHieroRules(Lexicalizer lexicalizer, T ruleInference);
	public TranslationRule<T> createBaseTranslationRuleWithAbstractRuleLabeling(Lexicalizer lexicalizer, T ruleInference);
	public TranslationRule<T> createBaseTranslationRuleWithPhrasePairLabeling(Lexicalizer lexicalizer, T ruleInference);
}
