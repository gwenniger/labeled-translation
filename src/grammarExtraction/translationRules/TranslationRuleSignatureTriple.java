package grammarExtraction.translationRules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import util.Pair;
import junit.framework.Assert;
import grammarExtraction.IntegerKeyRuleRepresentation;
import grammarExtraction.IntegerKeyRuleRepresentationWithLabel;
import grammarExtraction.translationRuleTrie.RuleLabeling;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

public class TranslationRuleSignatureTriple extends TranslationEquivalenceItem {

	private static final long serialVersionUID = 1L;

	private final LightWeightRuleLabel ruleLabel;

	protected TranslationRuleSignatureTriple(SourceTargetEquivalencePair sourceTargetEquivalencePair, LightWeightRuleLabel ruleLabel) {
		super(sourceTargetEquivalencePair);
		Assert.assertNotNull(ruleLabel);
		this.ruleLabel = ruleLabel;
	}

	public static TranslationRuleSignatureTriple createTranslationRuleSignatureTriple(SourceTargetEquivalencePair sourceTargetEquivalencePair, LightWeightRuleLabel ruleLabel) {
		return new TranslationRuleSignatureTriple(sourceTargetEquivalencePair, ruleLabel);
	}

	public static TranslationRuleSignatureTriple createTranslationRuleSignatureTripleWithNewLeftHandSideLabel(TranslationRuleSignatureTriple translationRuleSignatureTriple,
			LightWeightRuleLabel newRuleLabel) {
		return new TranslationRuleSignatureTriple(translationRuleSignatureTriple.getSourceTargetEquivalencePair(), newRuleLabel);
	}

	public static TranslationRuleSignatureTriple createTranslationRuleSignatureTripleWithAdaptedGapLabels(TranslationRuleSignatureTriple translationRuleSignatureTriple,
			Map<Integer, Pair<Integer>> gapIndexToNewLabelKeysPairMap, WordKeyMappingTable wordKeyMappingTable) {
		return new TranslationRuleSignatureTriple(translationRuleSignatureTriple.getSourceTargetEquivalencePair().createSourceTargetEquivalencePairWithAdaptedGapLabels(wordKeyMappingTable,
				gapIndexToNewLabelKeysPairMap), translationRuleSignatureTriple.getRuleLabel());
	}

	private static Map<Integer, Pair<Integer>> createGapIndexToNewLabelKeysPairMap(RuleLabeling ruleLabeling,WordKeyMappingTable wordKeyMappingTable)
	{
	    Map<Integer, Pair<Integer>> result = new HashMap<Integer, Pair<Integer>>();
	    List<Integer> sourceGapIndicesOrderedByGapNumber = ruleLabeling.getSourceGapLabelIndicesOrderedByGapNumber();
	    List<Integer> targetGapIndicesOrderedByGapNumber = ruleLabeling.getTargetGapLabelIndicesOrderedByGapNumber(wordKeyMappingTable);
	    Assert.assertEquals(sourceGapIndicesOrderedByGapNumber.size(), targetGapIndicesOrderedByGapNumber.size());
	    for(int i = 0; i < sourceGapIndicesOrderedByGapNumber.size(); i++){
		
		    result.put((i +1), new Pair<Integer>(sourceGapIndicesOrderedByGapNumber.get(i),targetGapIndicesOrderedByGapNumber.get(i)));
	    }
	    return result;
	}
	
	public static TranslationRuleSignatureTriple createTranslationRuleSignatureTripleWithAdaptedLabels(TranslationRuleSignatureTriple translationRuleSignatureTriple,
		RuleLabeling ruleLabeling, WordKeyMappingTable wordKeyMappingTable) {
	    
	    return new TranslationRuleSignatureTriple(translationRuleSignatureTriple.getSourceTargetEquivalencePair().createSourceTargetEquivalencePairWithAdaptedGapLabels(wordKeyMappingTable,
			createGapIndexToNewLabelKeysPairMap(ruleLabeling, wordKeyMappingTable)), ruleLabeling.getRuleLabel());
	}
	
	public static TranslationRuleSignatureTriple createTranslationRuleSignatureTriple(IntegerKeyRuleRepresentation sourceSideRepresentation, IntegerKeyRuleRepresentation targetSideRepresentation,
			LightWeightRuleLabel ruleLabel) {
		return new TranslationRuleSignatureTriple(SourceTargetEquivalencePair.createSourceTargetEquivalencePair(sourceSideRepresentation, targetSideRepresentation), ruleLabel);
	}

	public static LightWeightRuleLabel createRuleLabel(IntegerKeyRuleRepresentationWithLabel sourceSideRepresentationWithLabel, IntegerKeyRuleRepresentationWithLabel targetSideRepresentationWithLabel) {
		return LightWeightRuleLabel.createLightWeightRuleLabel(sourceSideRepresentationWithLabel.getLeftHandSideLabel(), targetSideRepresentationWithLabel.getLeftHandSideLabel());
	}

	public static TranslationRuleSignatureTriple createTranslationRuleSignatureTriple(IntegerKeyRuleRepresentationWithLabel sourceSideRepresentationWithLabel,
			IntegerKeyRuleRepresentationWithLabel targetSideRepresentationWithLabel) {
		return new TranslationRuleSignatureTriple(SourceTargetEquivalencePair.createSourceTargetEquivalencePair(
				IntegerKeyRuleRepresentation.createIntegerKeyRuleRepresentationRemovingLabel(sourceSideRepresentationWithLabel),
				IntegerKeyRuleRepresentation.createIntegerKeyRuleRepresentationRemovingLabel(targetSideRepresentationWithLabel)), createRuleLabel(sourceSideRepresentationWithLabel,
				targetSideRepresentationWithLabel));
	}

	public LightWeightRuleLabel getRuleLabel() {
		return this.ruleLabel;
	}

	public String getSourceSideRuleLabelString(WordKeyMappingTable wordKeyMappingTable) {
		return wordKeyMappingTable.getWordForKey(this.ruleLabel.getSourceSideLabel());
	}

	public String getTargetSideRuleLabelString(WordKeyMappingTable wordKeyMappingTable) {
		return wordKeyMappingTable.getWordForKey(this.ruleLabel.getTargetSideLabel());
	}

	public IntegerKeyRuleRepresentationWithLabel getSourceSideRepresentationWithLabel() {
		return IntegerKeyRuleRepresentationWithLabel.createIntegerKeyRuleRepresentationWithLabel(getSourceSideRepresentation(), this.ruleLabel.getSourceSideLabel());
	}

	public IntegerKeyRuleRepresentationWithLabel getTargetSideRepresentationWithLabel() {
		return IntegerKeyRuleRepresentationWithLabel.createIntegerKeyRuleRepresentationWithLabel(getTargetSideRepresentation(), this.ruleLabel.getTargetSideLabel());
	}

	private boolean hasMoreThanTwoElementsOnSourceAndTargetRightHandSide() {
		return ((this.getSourceSideRepresentation().getPartKeys().size() >= 2) && (this.getTargetSideRepresentation().getPartKeys().size() >= 2));
	}

	private boolean sourceAndTargetHaveNoTerminals(WordKeyMappingTable wordKeyMappingTable) {
		return (!this.getSourceSideRepresentation().hasTerminals(wordKeyMappingTable)) && (!this.getTargetSideRepresentation().hasTerminals(wordKeyMappingTable));
	}

	public boolean isAbstractRule(WordKeyMappingTable wordKeyMappingTable) {
		return (sourceAndTargetHaveNoTerminals(wordKeyMappingTable) && hasMoreThanTwoElementsOnSourceAndTargetRightHandSide());
	}

	public int countNumberOfXLabeledGaps(WordKeyMappingTable wordKeyMappingTable) {
		int result = 0;
		for (Integer key : this.sourceTargetEquivalencePair.getSourceKeys()) {
			if (wordKeyMappingTable.isXLabeledGapOneKey(key) || wordKeyMappingTable.isXLabeledGapTwoKey(key)) {
				result++;
			}
		}
		return result;
	}

	private boolean leftHandSideIsBasicChiangLabel() {
		return WordKeyMappingTable.isBasicChiangRuleLabelKey(this.ruleLabel.getSourceSideLabel()) && WordKeyMappingTable.isBasicChiangRuleLabelKey(this.ruleLabel.getTargetSideLabel());
	}

	private static List<Integer> getGapLabelKeys(WordKeyMappingTable wordKeyMappingTable, List<Integer> keyList)
	{
	    List<Integer> result = new ArrayList<Integer>();
	    for (int key : keyList) {
		if (wordKeyMappingTable.isGapLabelKey(key)) {
			result.add(key);
		}
	}
	return result;
	}
	
	public List<Integer> getSourceGapLabelKeys(WordKeyMappingTable wordKeyMappingTable){
	    return getGapLabelKeys(wordKeyMappingTable, this.getSourceKeys());
	}
	
	public List<Integer> getTargetGapLabelKeys(WordKeyMappingTable wordKeyMappingTable){
	    return getGapLabelKeys(wordKeyMappingTable, this.getTargetKeys());
	}
	
	private boolean allGapKeysAreHieroGaps(WordKeyMappingTable wordKeyMappingTable, List<Integer> keyList) {
		for (int key : keyList) {
			if (wordKeyMappingTable.isGapLabelKey(key)) {
				if (!wordKeyMappingTable.isXLabeledGapKey(key)) {
					return false;
				}
			}
		}
		return true;
	}

	public boolean isPureHieroRule(WordKeyMappingTable wordKeyMappingTable) {
		return (leftHandSideIsBasicChiangLabel() && allGapKeysAreHieroGaps(wordKeyMappingTable, this.getSourceKeys()) && allGapKeysAreHieroGaps(wordKeyMappingTable, this.getTargetKeys()));
	}

	private static TranslationRuleSignatureTriple computeNewRuleComponentsListWithLabelsFromSourceSide(TranslationRuleSignatureTriple translationRuleSignatureTripleWithLabelsOnOneSide,
			WordKeyMappingTable wordKeyMappingTable) {
		Map<Integer, Pair<Integer>> gapPositionsMap = translationRuleSignatureTripleWithLabelsOnOneSide.getSourceTargetEquivalencePair().createGapPositionsPairsMap(wordKeyMappingTable);
		translationRuleSignatureTripleWithLabelsOnOneSide.countNumberOfXLabeledGaps(wordKeyMappingTable);
		List<String> sourceStringsOriginal = translationRuleSignatureTripleWithLabelsOnOneSide.getSourceSideRepresentation().getStringListRepresentation(wordKeyMappingTable);
		List<String> targetStringsOriginal = translationRuleSignatureTripleWithLabelsOnOneSide.getTargetSideRepresentation().getStringListRepresentation(wordKeyMappingTable);

		List<String> targetStringsReplaced = new ArrayList<>(targetStringsOriginal);
		for (Pair<Integer> gapPositionsPair : gapPositionsMap.values()) {
			String gapReplacementString = sourceStringsOriginal.get(gapPositionsPair.getFirst());
			targetStringsReplaced.set(gapPositionsPair.getSecond(), gapReplacementString);
		}
		int ruleLabelKeyLabeled = translationRuleSignatureTripleWithLabelsOnOneSide.getRuleLabel().getSourceSideLabel();
		LightWeightRuleLabel newRuleLabel = LightWeightRuleLabel.createLightWeightRuleLabel(ruleLabelKeyLabeled, ruleLabelKeyLabeled);
		SourceTargetEquivalencePair newSourceTargetEquivalencePair = SourceTargetEquivalencePair.createSourceTargetEquivalencePair(sourceStringsOriginal, targetStringsReplaced, wordKeyMappingTable);
		return new TranslationRuleSignatureTriple(newSourceTargetEquivalencePair, newRuleLabel);
	}

	private static TranslationRuleSignatureTriple computeNewRuleComponentsListWithLabelsFromTargetSide(TranslationRuleSignatureTriple translationRuleSignatureTripleWithLabelsOnOneSide,
			WordKeyMappingTable wordKeyMappingTable) {
		Map<Integer, Pair<Integer>> gapPositionsMap = translationRuleSignatureTripleWithLabelsOnOneSide.getSourceTargetEquivalencePair().createGapPositionsPairsMap(wordKeyMappingTable);
		translationRuleSignatureTripleWithLabelsOnOneSide.countNumberOfXLabeledGaps(wordKeyMappingTable);
		List<String> sourceStringsOriginal = translationRuleSignatureTripleWithLabelsOnOneSide.getSourceSideRepresentation().getStringListRepresentation(wordKeyMappingTable);
		List<String> targetStringsOriginal = translationRuleSignatureTripleWithLabelsOnOneSide.getTargetSideRepresentation().getStringListRepresentation(wordKeyMappingTable);
		Assert.assertEquals(sourceStringsOriginal.size(), targetStringsOriginal.size());

		List<String> sourceStringsReplaced = new ArrayList<>(sourceStringsOriginal);
		for (Pair<Integer> gapPositionsPair : gapPositionsMap.values()) {
			String gapReplacementString = sourceStringsOriginal.get(gapPositionsPair.getSecond());
			sourceStringsReplaced.set(gapPositionsPair.getFirst(), gapReplacementString);
		}
		int ruleLabelKeyLabeled = translationRuleSignatureTripleWithLabelsOnOneSide.getRuleLabel().getTargetSideLabel();
		LightWeightRuleLabel newRuleLabel = LightWeightRuleLabel.createLightWeightRuleLabel(ruleLabelKeyLabeled, ruleLabelKeyLabeled);
		SourceTargetEquivalencePair newSourceTargetEquivalencePair = SourceTargetEquivalencePair.createSourceTargetEquivalencePair(sourceStringsReplaced, targetStringsOriginal, wordKeyMappingTable);
		return new TranslationRuleSignatureTriple(newSourceTargetEquivalencePair, newRuleLabel);
	}

	public static TranslationRuleSignatureTriple createTranslationRuleSignatureTripleWithLabelsOnBothSides(TranslationRuleSignatureTriple translationRuleSignatureTripleWithLabelsOnOneSide,
			WordKeyMappingTable wordKeyMappingTable) {

		System.out.println("TranslationRuleSignatureTriple.createTranslationRuleSignatureTripleWithLabelsOnBothSides(");
		boolean sourceSideIsLabeled = translationRuleSignatureTripleWithLabelsOnOneSide.getSourceSideRepresentationWithLabel().isSyntacticRuleSide();
		boolean targetSideIsLabeled = translationRuleSignatureTripleWithLabelsOnOneSide.getTargetSideRepresentationWithLabel().isSyntacticRuleSide();

		if (sourceSideIsLabeled & targetSideIsLabeled) {
			throw new RuntimeException("Error:  createTranslationRuleSignatureTripleWithLabelsOnBothSides -Illegal use of method for both side labeled rule");
		}

		BothSidesLabeledTranslationRuleSignatureCreater bothSidesLabeledTranslationRuleSignatureCreater;
		if (sourceSideIsLabeled) {
			bothSidesLabeledTranslationRuleSignatureCreater = BothSidesLabeledTranslationRuleSignatureCreater.createBothSidesLabeledTranslationRuleSignatureCreaterSourceLabeled(
					translationRuleSignatureTripleWithLabelsOnOneSide, wordKeyMappingTable);
		} else {
			bothSidesLabeledTranslationRuleSignatureCreater = BothSidesLabeledTranslationRuleSignatureCreater.createBothSidesLabeledTranslationRuleSignatureCreaterTargetLabeled(
					translationRuleSignatureTripleWithLabelsOnOneSide, wordKeyMappingTable);
		}
		return bothSidesLabeledTranslationRuleSignatureCreater.computeBothSideLabeledTranslationRuleSignatureTriple();
	}

}
