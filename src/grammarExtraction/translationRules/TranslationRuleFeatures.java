package grammarExtraction.translationRules;

import grammarExtraction.phraseProbabilityWeights.NamedFeature;

import java.util.List;

import util.Pair;

public interface TranslationRuleFeatures {

	public List<Float> getDenseFeatureValues();
	public String getJoshuaOriginalFeaturesRepresentation();
	public List<NamedFeature> getNamedFeaturesList();
	public List<NamedFeature> getNamedFeaturesListSparse();
	public String getLabeledFeaturesRepresentation();
	public String getSparseLabeledFeaturesRepresentation();
	public String getLabeledFeaturesRepresentationOriginalFeatureFormat();
	public String getCompressedFeaturesRepresentation();
	public List<NamedFeature> getDiscriminativeNamedFeaturesList();
	public List<NamedFeature> getDenseNamedFeaturesList();
	public Pair<Double >getSourceAndTargetPhraseFrequency();
	TranslationRuleFeatures createCopyWithScaledProbabilities(double scalingFactor);
}
