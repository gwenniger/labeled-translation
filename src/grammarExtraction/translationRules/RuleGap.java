package grammarExtraction.translationRules;

import java.util.HashSet;
import java.util.Set;

import util.Pair;

public class RuleGap {

	private static final String MOSES_SOURCE_SIDE_GAP_LABEL = "[X]";

	protected final Pair<Integer> relativeSourceSpan;
	private final Pair<Integer> relativeTargetSpan;
	private int gapNumber;
	private RuleLabel  gapLabel;

	public RuleGap(Pair<Integer> relativeSourceSpan, Pair<Integer> relativeTargetSpan, int gapNumber, RuleLabel gapLabel) {
		this.relativeSourceSpan = relativeSourceSpan;
		this.relativeTargetSpan = relativeTargetSpan;
		this.gapNumber = gapNumber;
		this.gapLabel = gapLabel;
	}

	protected RuleGap(RuleGap toCopy) {
		this.relativeSourceSpan = toCopy.relativeSourceSpan;
		this.relativeTargetSpan = toCopy.relativeTargetSpan;
		this.gapNumber = toCopy.gapNumber;
		this.gapLabel = toCopy.gapLabel;
	}

	public int getMinSourcePos() {
		return this.relativeSourceSpan.getFirst();
	}

	public int getMaxSourcePos() {
		return this.relativeSourceSpan.getSecond();
	}

	public int getMinTargetPos() {
		return this.relativeTargetSpan.getFirst();
	}

	public int getMaxTargetPos() {
		return this.relativeTargetSpan.getSecond();
	}

	public int getRelativeTargetGapPos() {
		return this.gapNumber;
	}

	protected Set<Integer> targetPositions() {
		Set<Integer> result = new HashSet<Integer>();

		for (int i = this.getMinTargetPos(); i <= this.getMaxTargetPos(); i++) {
			result.add(i);
		}
		return result;
	}

	protected Set<Integer> sourcePositions() {
		Set<Integer> result = new HashSet<Integer>();

		for (int i = this.getMinSourcePos(); i <= this.getMaxSourcePos(); i++) {
			result.add(i);
		}
		return result;
	}

	public static String ruleGapString(String gapLabel, int gapNumber) {
		return "[" + gapLabel + "," + (gapNumber + 1) + "]";
	}

	public static String ruleGapStringMosesStringToTree(String gapLabel) {
		return MOSES_SOURCE_SIDE_GAP_LABEL + "[" + gapLabel + "]";
	}

	public String ruleGapSourceString() {
		return "[" + this.gapLabel.getSourceSideLabel() + "," + (gapNumber + 1) + "]";
	}
	
	public String ruleGapTargetString() {
		return "[" + this.gapLabel.getTargetSideLabel() + "," + (gapNumber + 1) + "]";
	}

	protected int getGapNumber() {
		return this.gapNumber;
	}

	public boolean isOnLeftSourcePhraseBoundary() {
		if (this.relativeSourceSpan.getFirst() == 0) {
			return true;
		}
		return false;
	}

	public boolean isOnLeftTargetPhraseBoundary() {
		if (this.relativeTargetSpan.getFirst() == 0) {
			return true;
		}
		return false;
	}

	public boolean isOnRighttSourcePhraseBoundary(int sourcePhraseLength) {
		if (this.relativeSourceSpan.getSecond() == (sourcePhraseLength - 1)) {
			return true;
		}
		return false;
	}

	public boolean isOnRightTargetPhraseBoundary(int targetPhraseLength) {
		if (this.relativeTargetSpan.getSecond() == (targetPhraseLength - 1)) {
			return true;
		}
		return false;
	}

}
