package grammarExtraction.translationRules;

import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import util.Pair;

public abstract class BothSidesLabeledTranslationRuleSignatureCreater {

	protected final TranslationRuleSignatureTriple translationRuleSignatureTripleOneSideLabeled;
	private final WordKeyMappingTable wordKeyMappingTable;

	protected BothSidesLabeledTranslationRuleSignatureCreater(TranslationRuleSignatureTriple translationRuleSignatureTripleOneSideLabeled, WordKeyMappingTable wordKeyMappingTable) {
		this.translationRuleSignatureTripleOneSideLabeled = translationRuleSignatureTripleOneSideLabeled;
		this.wordKeyMappingTable = wordKeyMappingTable;
	}

	public static BothSidesLabeledTranslationRuleSignatureCreaterSourceLabeled createBothSidesLabeledTranslationRuleSignatureCreaterSourceLabeled(
			TranslationRuleSignatureTriple translationRuleSignatureTripleOneSideLabeled, WordKeyMappingTable wordKeyMappingTable) {
		return new BothSidesLabeledTranslationRuleSignatureCreaterSourceLabeled(translationRuleSignatureTripleOneSideLabeled, wordKeyMappingTable);
	}

	public static BothSidesLabeledTranslationRuleSignatureCreaterTargetLabeled createBothSidesLabeledTranslationRuleSignatureCreaterTargetLabeled(
			TranslationRuleSignatureTriple translationRuleSignatureTripleOneSideLabeled, WordKeyMappingTable wordKeyMappingTable) {
		return new BothSidesLabeledTranslationRuleSignatureCreaterTargetLabeled(translationRuleSignatureTripleOneSideLabeled, wordKeyMappingTable);
	}

	protected abstract List<String> getReplacedSideStrings();

	protected abstract List<String> getReplacingSideStrings();

	protected abstract int getReplacedSideGapIndex(Pair<Integer> gapIndices);

	protected abstract int getReplacingSideGapIndex(Pair<Integer> gapIndices);

	protected abstract List<String> computeNewSourceSideStrings();

	protected abstract List<String> computeNewTargetSideStrings();

	protected abstract int getLabeledSideRuleLabel();

	public List<String> getSourceStringsOriginal() {
		return translationRuleSignatureTripleOneSideLabeled.getSourceSideRepresentation().getStringListRepresentation(wordKeyMappingTable);
	}

	public List<String> getTargetStringsOriginal() {
		return translationRuleSignatureTripleOneSideLabeled.getTargetSideRepresentation().getStringListRepresentation(wordKeyMappingTable);
	}

	protected Map<Integer, Pair<Integer>> getGapPositionsMap() {
		return translationRuleSignatureTripleOneSideLabeled.getSourceTargetEquivalencePair().createGapPositionsPairsMap(wordKeyMappingTable);
	}

	protected List<String> computeReplacedSideReplacedStrings() {
		List<String> result = new ArrayList<>(getReplacedSideStrings());
		List<String> stringsReplacing = getReplacingSideStrings();
		for (Pair<Integer> gapPositionsPair : getGapPositionsMap().values()) {
			String gapReplacementString = stringsReplacing.get(getReplacingSideGapIndex(gapPositionsPair));
			result.set(getReplacedSideGapIndex(gapPositionsPair), gapReplacementString);
		}
		return result;
	}

	protected LightWeightRuleLabel computeNewRuleLabel() {
		return LightWeightRuleLabel.createLightWeightRuleLabel(getLabeledSideRuleLabel(), getLabeledSideRuleLabel());
	}

	protected TranslationRuleSignatureTriple computeBothSideLabeledTranslationRuleSignatureTriple() {
		SourceTargetEquivalencePair newSourceTargetEquivalencePair = SourceTargetEquivalencePair.createSourceTargetEquivalencePair(computeNewSourceSideStrings(), computeNewTargetSideStrings(),
				wordKeyMappingTable);
		return new TranslationRuleSignatureTriple(newSourceTargetEquivalencePair, computeNewRuleLabel());
	}

	private static class BothSidesLabeledTranslationRuleSignatureCreaterSourceLabeled extends BothSidesLabeledTranslationRuleSignatureCreater {

		protected BothSidesLabeledTranslationRuleSignatureCreaterSourceLabeled(TranslationRuleSignatureTriple translationRuleSignatureTripleOneSideLabeled, WordKeyMappingTable wordKeyMappingTable) {
			super(translationRuleSignatureTripleOneSideLabeled, wordKeyMappingTable);
		}

		@Override
		protected List<String> getReplacedSideStrings() {
			return getTargetStringsOriginal();
		}

		@Override
		protected List<String> getReplacingSideStrings() {
			return getSourceStringsOriginal();
		}

		@Override
		protected int getReplacedSideGapIndex(Pair<Integer> gapIndices) {
			return gapIndices.getSecond();
		}

		@Override
		protected int getReplacingSideGapIndex(Pair<Integer> gapIndices) {
			return gapIndices.getFirst();
		}

		@Override
		protected List<String> computeNewSourceSideStrings() {
			return getSourceStringsOriginal();
		}

		@Override
		protected List<String> computeNewTargetSideStrings() {
			return computeReplacedSideReplacedStrings();
		}

		@Override
		protected LightWeightRuleLabel computeNewRuleLabel() {
			int ruleLabelKeyLabeled = this.translationRuleSignatureTripleOneSideLabeled.getRuleLabel().getSourceSideLabel();
			return LightWeightRuleLabel.createLightWeightRuleLabel(ruleLabelKeyLabeled, ruleLabelKeyLabeled);
		}

		@Override
		protected int getLabeledSideRuleLabel() {
			return this.translationRuleSignatureTripleOneSideLabeled.getRuleLabel().getSourceSideLabel();
		}
	}

	private static class BothSidesLabeledTranslationRuleSignatureCreaterTargetLabeled extends BothSidesLabeledTranslationRuleSignatureCreater {

		protected BothSidesLabeledTranslationRuleSignatureCreaterTargetLabeled(TranslationRuleSignatureTriple translationRuleSignatureTripleOneSideLabeled, WordKeyMappingTable wordKeyMappingTable) {
			super(translationRuleSignatureTripleOneSideLabeled, wordKeyMappingTable);
		}

		@Override
		protected List<String> getReplacedSideStrings() {
			return getSourceStringsOriginal();
		}

		@Override
		protected List<String> getReplacingSideStrings() {
			return getTargetStringsOriginal();
		}

		@Override
		protected int getReplacedSideGapIndex(Pair<Integer> gapIndices) {
			return gapIndices.getFirst();
		}

		@Override
		protected int getReplacingSideGapIndex(Pair<Integer> gapIndices) {
			return gapIndices.getSecond();
		}

		@Override
		protected List<String> computeNewSourceSideStrings() {
			return computeReplacedSideReplacedStrings();
		}

		@Override
		protected List<String> computeNewTargetSideStrings() {
			return getTargetStringsOriginal();
		}

		@Override
		protected int getLabeledSideRuleLabel() {
			return this.translationRuleSignatureTripleOneSideLabeled.getRuleLabel().getTargetSideLabel();
		}
	}
}