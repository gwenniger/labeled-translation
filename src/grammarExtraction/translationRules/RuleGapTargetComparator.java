package grammarExtraction.translationRules;


import java.util.Comparator;

public class RuleGapTargetComparator implements Comparator<RuleGap>
{

	@Override
	public int compare(RuleGap ruleGap1, RuleGap ruleGap2) 
	{
		if(ruleGap1.getMaxTargetPos() < ruleGap2.getMinTargetPos())
		{
			return -1;
		}
		else if(ruleGap2.getMaxTargetPos() <  ruleGap1.getMinTargetPos())
		{
			return 1;
		}
		else
		{	
			return 0;
		}	
	}

}
