package grammarExtraction.translationRules;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import util.Pair;
import grammarExtraction.chiangGrammarExtraction.ChiangRuleCreater;
import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatter;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatterJoshua;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

public class JoshuaRuleRepresentationCreater extends TranslationRuleRepresentationCreater implements Serializable {

    	/**
         * 
         */
        private static final long serialVersionUID = 1L;
	protected final String unknownWordsLabel;
	public static final String UnknownWordPreTerminalJoshuaComplexLabels = "UnknownWord";

	protected JoshuaRuleRepresentationCreater(FeatureValueFormatter featureValueFormatter, WordKeyMappingTable wordKeyMappingTable,
			TranslationRuleSignatureTriple translationRuleSignatureTriple, String unknownWordsLabel) {
		super(featureValueFormatter, wordKeyMappingTable, translationRuleSignatureTriple);
		this.unknownWordsLabel = unknownWordsLabel;
	}

	public static JoshuaRuleRepresentationCreater createJoshuaRuleRepresentationCreaterFactory(String unknownWordsLabel) {
		return new JoshuaRuleRepresentationCreater(new FeatureValueFormatterJoshua(), null, null, unknownWordsLabel);
	}
	
	
	public static JoshuaRuleRepresentationCreater createJoshuaRuleRepresentationCreaterFactoryHieroLabeled() {
		return new JoshuaRuleRepresentationCreater(new FeatureValueFormatterJoshua(), null, null, ChiangRuleCreater.ChiangLabel);
	}
	
	public static JoshuaRuleRepresentationCreater createJoshuaRuleRepresentationCreaterFactoryComplexLabeled() {
		return new JoshuaRuleRepresentationCreater(new FeatureValueFormatterJoshua(), null, null, UnknownWordPreTerminalJoshuaComplexLabels);
	}

	public static JoshuaRuleRepresentationCreater createJoshuaRuleRepresentationCreater(WordKeyMappingTable wordKeyMappingTable,
			TranslationRuleSignatureTriple translationRuleSignatureTriple, String unknownWordsLabel) {
		return new JoshuaRuleRepresentationCreater(new FeatureValueFormatterJoshua(), wordKeyMappingTable, translationRuleSignatureTriple, unknownWordsLabel);
	}

	@Override
	public String getTranslationRuleRepresentation() {
		return getJoshuaRuleRepresentation();
	}

	public String getJoshuaRuleRepresentation() {
		String result = this.getSourceSideRepresentationWithLabel().getLeftHandSideLabelStringRepresentation(wordKeyMappingTable)
				+ JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR
				+ this.getSourceSideRepresentationWithLabel().getStringRepresentationExcludingLeftHandSideLabelJoshua(wordKeyMappingTable)
				+ JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR
				+ this.getTargetSideRepresentationWithLabel().getStringRepresentationExcludingLeftHandSideLabelJoshua(wordKeyMappingTable);
		return result;
	}

	@Override
	public TranslationRuleRepresentationCreater createTranslationRuleRepresentationCreater(WordKeyMappingTable wordKeyMappingTable,
			TranslationRuleSignatureTriple translationRuleSignatureTriple) {
		return createJoshuaRuleRepresentationCreater(wordKeyMappingTable, translationRuleSignatureTriple, unknownWordsLabel);
	}

	@Override
	public String getTranslationRuleRepresentationWithFeatures(String featuresString, Pair<Double> phrasePairFrequencies) {
		return getJoshuaRuleRepresentation() + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR + featuresString;
	}

	private String getJoshuaRuleString(String leftHandSideLabel, String nonTerminalRightSide, String featuresString) {
		return leftHandSideLabel + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR + nonTerminalRightSide + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR
				+ nonTerminalRightSide + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR + featuresString;
	}

	public String getGlueRuleRepresentationWithFeatures(String complexLabel, String featuresString) {
		String nonterminalRightSide = RuleGap.ruleGapString(GOAL_LABEL, 0) + " " + RuleGap.ruleGapString(complexLabel, 1);
		String result = getJoshuaRuleString(JoshuaStyle.createJoshuaStyleRuleLabel(GOAL_LABEL), nonterminalRightSide, featuresString);
		return result;
	}

	public String getStartRuleRepresentationWithFeatures(String featuresString) {
		String nonterminalRightSide = "<s>";
		String result = getJoshuaRuleString(JoshuaStyle.createJoshuaStyleRuleLabel(GOAL_LABEL), nonterminalRightSide, featuresString);
		return result;
	}
	
	public String getEndRuleRepresentationWithFeatures(String featuresString) {
		String nonterminalRightSide =  RuleGap.ruleGapString(GOAL_LABEL, 0) + " " + "</s>";
		String result = getJoshuaRuleString(JoshuaStyle.createJoshuaStyleRuleLabel(GOAL_LABEL), nonterminalRightSide, featuresString);
		return result;
	}

	@Override
	public List<String> getGlueRulesForLabel(String complexLabel, String featuresString) {
		List<String> result = new ArrayList<String>();
		result.add(getGlueRuleRepresentationWithFeatures(complexLabel, featuresString));
		return result;
	}

	@Override
	public List<String> getStartAndEndRules(String featuresString) {
		List<String> result = new ArrayList<String>();
		result.add(getStartRuleRepresentationWithFeatures(featuresString));
		result.add(getEndRuleRepresentationWithFeatures(featuresString));
		return result;
	}

	@Override
	public String getSwitchRuleForLabels(String leftHandSideLabel, String rightHandSideLabel, String featuresString) {
		String nonterminalRightSide = RuleGap.ruleGapString(rightHandSideLabel, 0);
		String result = getJoshuaRuleString(JoshuaStyle.createJoshuaStyleRuleLabel(leftHandSideLabel), nonterminalRightSide, featuresString);
		return result;
	}

	@Override
	public String getUnknownWordRuleForLabel(String complexLabel, String featuresString) {

		String nonterminalRightSide = RuleGap.ruleGapString(unknownWordsLabel, 0);
		String result = getJoshuaRuleString(JoshuaStyle.createJoshuaStyleRuleLabel(complexLabel), nonterminalRightSide, featuresString);
		return result;
	}

	@Override
	public String getUnknownWordsLabel() {
		return this.unknownWordsLabel;
	}
}
