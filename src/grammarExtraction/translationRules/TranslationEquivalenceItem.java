package grammarExtraction.translationRules;

import grammarExtraction.IntegerKeyRuleRepresentation;
import grammarExtraction.chiangGrammarExtraction.ChiangRuleCreater;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class TranslationEquivalenceItem implements Serializable{

	private static final long serialVersionUID = 1L;
	protected final SourceTargetEquivalencePair sourceTargetEquivalencePair;

	protected TranslationEquivalenceItem(SourceTargetEquivalencePair sourceTargetEquivalencePair) {
		this.sourceTargetEquivalencePair = sourceTargetEquivalencePair;
	}

	public static TranslationEquivalenceItem createTranslationEquivalenceItem(IntegerKeyRuleRepresentation sourceSideRepresentation,
			IntegerKeyRuleRepresentation targetSideRepresentation) {
		return new TranslationEquivalenceItem(SourceTargetEquivalencePair.createSourceTargetEquivalencePair(sourceSideRepresentation, targetSideRepresentation));
	}

	public static TranslationEquivalenceItem createTranslationEquivalenceItem(List<String> sourceSideWords, List<String> targetSideWords,
			WordKeyMappingTable wordKeyMappingTable) {
		return new TranslationEquivalenceItem(SourceTargetEquivalencePair.createSourceTargetEquivalencePair(sourceSideWords, targetSideWords,
				wordKeyMappingTable));
	}

	public static TranslationEquivalenceItem createXLabelExtendedTranslationEquivalenceItem(List<String> sourceSideWords, List<String> targetSideWords,
			WordKeyMappingTable wordKeyMappingTable) {
		
		List<String> xLabelextendedSourceWords = new ArrayList<String>(sourceSideWords);
		xLabelextendedSourceWords.add(ChiangRuleCreater.ChiangRuleLabel);
		List<String> xLabelextendedTargetWords = new ArrayList<String>(targetSideWords);
		xLabelextendedTargetWords.add(ChiangRuleCreater.ChiangRuleLabel);
		
		return new TranslationEquivalenceItem(SourceTargetEquivalencePair.createSourceTargetEquivalencePair(xLabelextendedSourceWords, xLabelextendedTargetWords,
				wordKeyMappingTable));
	}

	
	public boolean equals(Object translationEquivalenceObject) {
		if (translationEquivalenceObject instanceof TranslationEquivalenceItem) {
			TranslationEquivalenceItem translationEquivalenceItem = (TranslationEquivalenceItem) translationEquivalenceObject;
			if ((translationEquivalenceItem.getSourceSideRepresentation()).equals(this.getSourceSideRepresentation())
					&& (translationEquivalenceItem.getTargetSideRepresentation()).equals(this.getTargetSideRepresentation())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return sourceTargetEquivalencePair.hashCode();
	}

	public List<Integer> getSourceKeys() {
		return this.getSourceSideRepresentation().getPartKeys();
	}

	public List<Integer> getTargetKeys() {
		return sourceTargetEquivalencePair.getTargetKeys();
	}

	public int getSourceKey(int index) {
		return sourceTargetEquivalencePair.getSourceKey(index);
	}

	public IntegerKeyRuleRepresentation getSourceSideRepresentation() {
		return sourceTargetEquivalencePair.getSourceSideRepresentation();
	}

	public IntegerKeyRuleRepresentation getTargetSideRepresentation() {
		return sourceTargetEquivalencePair.getTargetSideRepresentation();
	}

	public SourceTargetEquivalencePair getSourceTargetEquivalencePair() {
		return this.sourceTargetEquivalencePair;
	}

	public String toString()
	{
		return "<TranslationEquivalencePair>" +  this.sourceTargetEquivalencePair.getSourceSideRepresentation().toString() + this.sourceTargetEquivalencePair.getTargetSideRepresentation().toString() + "</TranslationEquivalencePair>";
	}
	
}
