package grammarExtraction.translationRules;

import grammarExtraction.chiangGrammarExtraction.ChiangRuleCreater;

public class RuleLabelCreaterFactory {

	public static RuleLabelCreater createBothSidesLabeler() {
		return new BothSidesLabeler();
	}

	public static RuleLabelCreater createSourceSideLabeler() {
		return new SourceSideLabeler();
	}

	public static RuleLabelCreater createTargetSideLabeler() {
		return new TargetSideLabeler();
	}

	private static class BothSidesLabeler implements RuleLabelCreater {

		@Override
		public RuleLabel createRuleLabelFromLabelString(String labelString) {
			return RuleLabel.createRuleLabel(labelString, labelString);
		}

		@Override
		public boolean labelSourceSide() {
			return true;
		}

		@Override
		public boolean labelTargetSide() {
			return true;
		}

	}

	private static class SourceSideLabeler implements RuleLabelCreater {

		@Override
		public RuleLabel createRuleLabelFromLabelString(String labelString) {
			return RuleLabel.createRuleLabel(labelString, ChiangRuleCreater.ChiangLabel);
		}

		@Override
		public boolean labelSourceSide() {
			return true;
		}

		@Override
		public boolean labelTargetSide() {
			return false;
		}
	}

	private static class TargetSideLabeler implements RuleLabelCreater {

		@Override
		public RuleLabel createRuleLabelFromLabelString(String labelString) {
			return RuleLabel.createRuleLabel(ChiangRuleCreater.ChiangLabel, labelString);
		}

		@Override
		public boolean labelSourceSide() {
			return false;
		}

		@Override
		public boolean labelTargetSide() {
			return true;
		}
	}
}
