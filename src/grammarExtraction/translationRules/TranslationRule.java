package grammarExtraction.translationRules;

import util.Span;
import grammarExtraction.translationRuleTrie.LexicalProbabilityTables;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import util.Utility;
import extended_bitg.EbitgInference;
import extended_bitg.EbitgLexicalizedInference;
import alignment.Alignment;
import alignment.AlignmentPair;
import alignment.AlignmentTriple;

public class TranslationRule<T extends EbitgLexicalizedInference> {
	private final RuleLabel ruleLabel;
	private final TranslationRuleLexicalProperties translationRuleLexicalProperties;
	private final List<RuleGap> ruleGapsSourceOrder;
	private final List<RuleGap> ruleGapsTargetOrder;
	private final RuleGapCreater<T> ruleGapCreater;

	public TranslationRule(RuleLabel ruleLabel, TranslationRuleLexicalProperties translationRuleLexicalProperties, List<RuleGap> ruleGapsSourceOrder, List<RuleGap> ruleGapsTargetOrder,
			RuleGapCreater<T> ruleGapCreater) {
		this.ruleLabel = ruleLabel;
		this.translationRuleLexicalProperties = translationRuleLexicalProperties;
		this.ruleGapsSourceOrder = ruleGapsSourceOrder;
		this.ruleGapsTargetOrder = ruleGapsTargetOrder;
		this.ruleGapCreater = ruleGapCreater;
	}

	public static <T extends EbitgLexicalizedInference> TranslationRule<T> copyTranslationRule(TranslationRule<T> toCopy) {
		return toCopy.copyTranslationRuleWithNewLabel(toCopy.ruleLabel);
	}

	public TranslationRule<T> copyTranslationRuleWithNewLabel(RuleLabel newLabel) {
		return new TranslationRule<T>(this.ruleLabel, new TranslationRuleLexicalProperties(this.translationRuleLexicalProperties), copyRuleGaps(this.ruleGapsSourceOrder),
				copyRuleGaps(this.ruleGapsTargetOrder), this.ruleGapCreater);
	}

	public void addRuleGap(Span absoluteSourceSpan, T gapInference) {
		RuleGap newRuleGap = ruleGapCreater.creatLexicalizedHieroRuleGap(this.findRelativeSourceSpan(absoluteSourceSpan), this.findRelativeTargetSpan(gapInference), this.getNoRuleGaps(), gapInference);
		this.ruleGapsSourceOrder.add(newRuleGap);
		this.ruleGapsTargetOrder.add(newRuleGap);

		// Recompute the rule gap orders (if necessary)
		this.computRuleGapOrders();
	}
	
	public void addRuleGapAbstractRule(Span absoluteSourceSpan, T gapInference) {
		RuleGap newRuleGap = ruleGapCreater.createAbstractRuleGap(this.findRelativeSourceSpan(absoluteSourceSpan), this.findRelativeTargetSpan(gapInference), this.getNoRuleGaps(), gapInference);
		this.ruleGapsSourceOrder.add(newRuleGap);
		this.ruleGapsTargetOrder.add(newRuleGap);

		// Recompute the rule gap orders (if necessary)
		this.computRuleGapOrders();
	}

	private Span findRelativeSourceSpan(Span absoluteSourceSpan) {
		int relativeSourceMin = absoluteSourceSpan.getFirst() - this.getMinSourcePos();
		int relativeSourceMax = absoluteSourceSpan.getSecond() - this.getMinSourcePos();
		return new Span(relativeSourceMin, relativeSourceMax);
	}

	private Span findRelativeTargetSpan(EbitgInference gapInference) {
		int relativeMinTargetPos = gapInference.getMinTargetPos() - this.getMinTargetPos();
		int relativeMaxTargetPos = gapInference.getMaxTargetPos() - this.getMinTargetPos();
		return new Span(relativeMinTargetPos, relativeMaxTargetPos);
	}

	public int getMinTargetPos() {
		return this.getTargetSpan().getFirst();
	}

	public int getMaxTargetPos() {
		return this.getTargetSpan().getSecond();
	}

	private Span getTargetSpan() {
		return this.translationRuleLexicalProperties.getTargetSpan();
	}

	public Span getSourceSpan() {
		return this.translationRuleLexicalProperties.getSourceSpan();
	}

	private List<String> getSourceWords() {
		return this.translationRuleLexicalProperties.getSourceWords();
	}

	private List<String> getTargetWords() {
		return this.translationRuleLexicalProperties.getTargetWords();
	}

	private List<AlignmentPair> getAlignments() {
		return this.translationRuleLexicalProperties.getAlignments();
	}

	private int getRelativeMinTargetPos() {
		return 0;
	}

	private int getRelativeMaxTargetPos() {
		return this.getMaxTargetPos() - this.getMinTargetPos();
	}

	private int getRelativeMinSourcePos() {
		return 0;
	}

	private int getRelativeMaxSourcePos() {
		return this.getMaxSourcePos() - this.getMinSourcePos();
	}

	private int getMinSourcePos() {
		return this.getSourceSpan().getFirst();
	}

	public int getMaxSourcePos() {
		return this.getSourceSpan().getSecond();
	}

	public int getSourceLength() {
		return getSourceSpan().getSpanLength();
	}

	public int getTargetLength() {
		return getTargetSpan().getSpanLength();
	}

	private List<RuleGap> copyRuleGaps(List<RuleGap> ruleGaps) {
		List<RuleGap> result = new ArrayList<RuleGap>();

		for (RuleGap ruleGap : ruleGaps) {
			result.add(ruleGap);
		}

		return result;
	}

	private int getNoRuleGaps() {
		return ruleGapsSourceOrder.size();
	}

	private void computRuleGapOrders() {
		// If there is something to be sorted
		if (this.getNoRuleGaps() > 1) {
			this.sortRuleGapsSourceOrder();
			this.sortRuleGapsTargetOrder();
		}
	}

	private void sortRuleGapsSourceOrder() {
		this.ruleGapsSourceOrder.sort(new RuleGapSourceComparator());
	}

	private void sortRuleGapsTargetOrder() {
		this.ruleGapsTargetOrder.sort(new RuleGapTargetComparator());
	}

	public List<String> sourceSideRuleStringList() {
		List<String> result = new ArrayList<String>();

		int i = 0;
		for (RuleGap ruleGapSource : this.ruleGapsSourceOrder) {
			while (i < ruleGapSource.getMinSourcePos()) {
				result.add(this.getSourceWords().get(i));
				i++;
			}
			i = ruleGapSource.getMaxSourcePos() + 1;
			result.add(ruleGapSource.ruleGapSourceString());
		}

		while (i < this.getSourceWords().size()) {
			result.add(this.getSourceWords().get(i));
			i++;
		}

		return result;
	}

	/*
	 * protected List<String> sourceSideRuleStringListWithLeftHandSideRuleLabel() { List<String> result = sourceSideRuleStringList();
	 * result.add(this.ruleLabel); return result; }
	 */

	public RuleLabel getRuleLabel() {
		return this.ruleLabel;
	}

	public int getSourceSideNumElements() {
		int result = 0;
		int i = 0;
		for (RuleGap ruleGapSource : this.ruleGapsSourceOrder) {
			while (i < ruleGapSource.getMinSourcePos()) {
				result++;
				i++;
			}
			i = ruleGapSource.getMaxSourcePos() + 1;
			result++;
		}

		while (i < this.getSourceWords().size()) {
			result++;
			i++;
		}

		return result;
	}

	private Set<Integer> getRelativeTargetPositions() {
		Set<Integer> result = new HashSet<Integer>();
		for (int i = this.getRelativeMinTargetPos(); i <= this.getRelativeMaxTargetPos(); i++) {
			result.add(i);
		}
		return result;
	}

	private Set<Integer> getRelativeSourcePositions() {
		Set<Integer> result = new HashSet<Integer>();
		for (int i = this.getRelativeMinSourcePos(); i <= this.getRelativeMaxSourcePos(); i++) {
			result.add(i);
		}
		return result;
	}

	private Set<Integer> getRelativeGapTargetPositions() {
		Set<Integer> result = new HashSet<Integer>();
		for (RuleGap ruleGap : this.ruleGapsTargetOrder) {
			result.addAll(ruleGap.targetPositions());
		}
		return result;
	}

	private Set<Integer> getRelativeGapSourcePositions() {
		Set<Integer> result = new HashSet<Integer>();
		for (RuleGap ruleGap : this.ruleGapsSourceOrder) {
			result.addAll(ruleGap.sourcePositions());
		}
		return result;
	}

	public Set<Integer> getRelativeOutsideGapTargetPositions() {
		Set<Integer> result = this.getRelativeTargetPositions();
		result.removeAll(this.getRelativeGapTargetPositions());
		return result;
	}

	public Set<Integer> getRelativeOutsideGapSourcePositions() {
		// FIXME : This was a bug
		// Set<Integer> result = this.getRelativeTargetPositions();

		Set<Integer> result = this.getRelativeSourcePositions();
		result.removeAll(this.getRelativeGapSourcePositions());
		return result;
	}

	public boolean hasOutsideGapTargetPositions() {
		return (getRelativeOutsideGapTargetPositions().size() > 0);
	}

	public List<String> targetSideRuleStringList() {
		List<String> result = new ArrayList<String>();

		int i = 0;
		for (RuleGap ruleGapTarget : this.ruleGapsTargetOrder) {
			while (i < ruleGapTarget.getMinTargetPos()) {
				result.add(this.getTargetWords().get(i));
				i++;

			}
			i = ruleGapTarget.getMaxTargetPos() + 1;
			result.add(ruleGapTarget.ruleGapTargetString());
		}

		while (i < this.getTargetWords().size()) {
			result.add(this.getTargetWords().get(i));
			i++;
		}

		return result;
	}

	/*
	 * protected List<String> targetSideRuleStringListWithLeftHandSideRuleLabel() { List<String> result = targetSideRuleStringList();
	 * result.add(this.ruleLabel); return result; }
	 */

	// protected String targetSideRuleString

	private AlignmentPair getRelativeAlignmentPair(AlignmentPair absoluteAlignmentPair) {
		return new AlignmentPair(absoluteAlignmentPair.getSourcePos() - this.getMinSourcePos(), absoluteAlignmentPair.getTargetPos() - this.getMinTargetPos());
	}

	private Alignment getRelativeOutsideGapNullExtendedSourceAlignments() {
		List<AlignmentPair> alignmentPairs = new ArrayList<AlignmentPair>();

		Set<Integer> relativeOutsideGapSourcePositions = getRelativeOutsideGapSourcePositions();
		Set<Integer> alignedOutsideGapRelativeSourcePositions = new HashSet<Integer>();

		for (AlignmentPair alignmentPair : getAlignments()) {
			AlignmentPair relativeAlignmentPair = getRelativeAlignmentPair(alignmentPair);

			if (relativeOutsideGapSourcePositions.contains(relativeAlignmentPair.getSourcePos())) {
				alignedOutsideGapRelativeSourcePositions.add(relativeAlignmentPair.getSourcePos());
				alignmentPairs.add(relativeAlignmentPair);
			}
		}

		Set<Integer> unAlignedOutsideGapRelativeSourcePositions = new HashSet<Integer>(relativeOutsideGapSourcePositions);
		unAlignedOutsideGapRelativeSourcePositions.removeAll(alignedOutsideGapRelativeSourcePositions);

		assert ((unAlignedOutsideGapRelativeSourcePositions.size() + alignedOutsideGapRelativeSourcePositions.size()) == relativeOutsideGapSourcePositions.size());

		int nullWordTargetPosition = this.getRelativeMaxTargetPos() + 1;
		for (int unalignedRelativeSourcePosition : unAlignedOutsideGapRelativeSourcePositions) {
			alignmentPairs.add(new AlignmentPair(unalignedRelativeSourcePosition, nullWordTargetPosition));
		}
		return Alignment.createAlignment(alignmentPairs);
	}

	private Alignment getRelativeOutsideGapNullExtendedAndInvertedTargetAlignments() {

		// System.out.println("TranslationRule.getRelativeOutsideGapNullExtendedAndInvertedTargetAlignments()");
		// System.out.println("Translation Rule:  " + this);

		List<AlignmentPair> alignmentPairs = new ArrayList<AlignmentPair>();

		Set<Integer> relativeOutsideGapTargetPositions = getRelativeOutsideGapTargetPositions();
		Set<Integer> alignedOutsideGapRelativeTargetPositions = new HashSet<Integer>();

		for (AlignmentPair alignmentPair : getAlignments()) {
			AlignmentPair relativeAlignmentPair = getRelativeAlignmentPair(alignmentPair);

			if (relativeOutsideGapTargetPositions.contains(relativeAlignmentPair.getTargetPos())) {
				alignedOutsideGapRelativeTargetPositions.add(relativeAlignmentPair.getTargetPos());
				alignmentPairs.add(AlignmentPair.createInvertedAlignmentPair(relativeAlignmentPair));
			}
		}

		Set<Integer> unAlignedOutsideGapRelativeTargetPositions = new HashSet<Integer>(relativeOutsideGapTargetPositions);
		unAlignedOutsideGapRelativeTargetPositions.removeAll(alignedOutsideGapRelativeTargetPositions);
		assert ((unAlignedOutsideGapRelativeTargetPositions.size() + alignedOutsideGapRelativeTargetPositions.size()) == relativeOutsideGapTargetPositions.size());

		int nullWordSourcePosition = this.getRelativeMaxSourcePos() + 1;
		for (int unalignedRelativeTargetPosition : unAlignedOutsideGapRelativeTargetPositions) {
			alignmentPairs.add(new AlignmentPair(unalignedRelativeTargetPosition, nullWordSourcePosition));
		}
		return Alignment.createAlignment(alignmentPairs);
	}

	private static List<String> getNullWordExtendedList(List<String> originalList) {
		List<String> result = new ArrayList<String>(originalList);
		result.add(AlignmentTriple.NullWordString);
		return result;
	}

	public double computeSourceToTargetLexicalWeight(LexicalProbabilityTables lexicalProbabilityTables) {
		return lexicalProbabilityTables.getLexicalProbabilityTableSourceGivenTarget().computeAlignedPhrasePairProbability(getSourceWords(), getNullWordExtendedList(getTargetWords()),
				getRelativeOutsideGapNullExtendedSourceAlignments());
	}

	public double computeTargetToSourceLexicalWeight(LexicalProbabilityTables lexicalProbabilityTables) {
		return lexicalProbabilityTables.getLexicalProbabilityTableTargetGivenSource().computeAlignedPhrasePairProbability(getTargetWords(), getNullWordExtendedList(getSourceWords()),
				getRelativeOutsideGapNullExtendedAndInvertedTargetAlignments());
	}

	/**
	 * See Andreas Zollman's Phd Thesis, page 31. This is part of an optimization to reject rules with two or more nonterminals where one is on the same
	 * boundary on both sides. As we understand the reason for rejecting such rules is that they increase decoding time a lot while not adding anything new that
	 * cannot be inferred without these rules
	 * 
	 * @return
	 */
	public boolean hasDanglingSecondNonterminal() {
		if (this.getNumberOfGaps() > 1) {
			for (RuleGap gap : this.ruleGapsSourceOrder) {
				if (this.gapIsOnSamePhraseBoundaryForSourceAndTarget(gap)) {
					return true;
				}
			}
		}
		return false;
	}

	private int getNumberOfGaps() {
		return this.ruleGapsSourceOrder.size();
	}

	private boolean gapIsOnSamePhraseBoundaryForSourceAndTarget(RuleGap gap) {
		return (gapIsOnLeftPhraseBoundaryForSourceAndTarget(gap) || gapIsOnRightPhraseBoundaryForSourceAndTarget(gap));
	}

	private boolean gapIsOnLeftPhraseBoundaryForSourceAndTarget(RuleGap gap) {
		if (gap.isOnLeftSourcePhraseBoundary() && gap.isOnLeftTargetPhraseBoundary()) {
			return true;
		}
		return false;
	}

	private boolean gapIsOnRightPhraseBoundaryForSourceAndTarget(RuleGap gap) {
		if (gap.isOnRighttSourcePhraseBoundary(this.getSourceLength()) && gap.isOnRightTargetPhraseBoundary(this.getTargetLength())) {
			return true;
		}
		return false;
	}

	public String toString() {
		String result = "TranslationRule:";
		result += "\n" + Utility.stringListStringWithoutBrackets(this.sourceSideRuleStringList());
		result += " , " + Utility.stringListStringWithoutBrackets(this.targetSideRuleStringList());
		result += "\nAlignments " + this.getAlignments();
		result += "\nGaps: " + this.ruleGapsSourceOrder;
		result += "\nruleLabel: " + this.ruleLabel;
		return result;
	}

}
