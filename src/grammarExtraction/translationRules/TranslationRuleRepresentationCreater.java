package grammarExtraction.translationRules;

import java.util.List;

import util.Pair;
import grammarExtraction.IntegerKeyRuleRepresentationWithLabel;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatter;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

public abstract class TranslationRuleRepresentationCreater {

	public static final String SentenceLabel = "S";
	public static final String GOAL_LABEL = "GOAL";

	protected final FeatureValueFormatter featureValueFormatter;
	protected final WordKeyMappingTable wordKeyMappingTable;
	protected final TranslationRuleSignatureTriple translationRuleSignatureTriple;

	protected TranslationRuleRepresentationCreater(FeatureValueFormatter featureValueFormatter, WordKeyMappingTable wordKeyMappingTable,
			TranslationRuleSignatureTriple translationRuleSignatureTriple) {
		this.featureValueFormatter = featureValueFormatter;
		this.wordKeyMappingTable = wordKeyMappingTable;
		this.translationRuleSignatureTriple = translationRuleSignatureTriple;
	}

	public abstract TranslationRuleRepresentationCreater createTranslationRuleRepresentationCreater(WordKeyMappingTable wordKeyMappingTable,
			TranslationRuleSignatureTriple translationRuleSignatureTriple);

	public abstract String getTranslationRuleRepresentation();

	public abstract String getTranslationRuleRepresentationWithFeatures(String featuresString, Pair<Double> phrasePairFrequencies);

	public abstract List<String> getGlueRulesForLabel(String complexLabel, String featuresString);

	public abstract String getSwitchRuleForLabels(String leftHandSideLabel, String rightHandSideLabel, String featuresString);

	public abstract String getUnknownWordRuleForLabel(String complexLabel, String featuresString);

	// public abstract String getGlueRuleRepresentationWithFeatures(String
	// complexLabel, String featuresString);
	// public abstract String getNonGlueRuleRepresentationWithFeatures(String
	// complexLabel, String featuresString);

	public IntegerKeyRuleRepresentationWithLabel getSourceSideRepresentationWithLabel() {
		return this.translationRuleSignatureTriple.getSourceSideRepresentationWithLabel();
	}

	public IntegerKeyRuleRepresentationWithLabel getTargetSideRepresentationWithLabel() {
		return this.translationRuleSignatureTriple.getTargetSideRepresentationWithLabel();
	}

	public abstract List<String> getStartAndEndRules(String featuresString);

	public FeatureValueFormatter getFeatureValueFormatter() {
		return this.featureValueFormatter;
	}
	
	public abstract String getUnknownWordsLabel();
}
