package grammarExtraction.phraseProbabilityWeights;

public class NamedIntegerFeature extends NamedFeature {

	private static final String JOSHUA_BYTE_TYPE_VALUE_STRING = "byte";
	private final int value;

	protected NamedIntegerFeature(String featureName, int value) {
		super(featureName);
		this.value = value;
	}

	public static NamedIntegerFeature createNamedCountFeature(String featureName, int value) {
		return new NamedIntegerFeature(featureName, value);
	}

	@Override
	protected float getUnlabeledJoshuaFeatureValue() {
		return value;
	}

	@Override
	protected String getLabeledJoshuaFeatureValue() {
		return NamedFeature.getLabeledFeaturRepresentation(featureName, value);
	}

	@Override
	protected float getUnlabeledMosesFeatureValue() {
		return computeMosesExponentRepresentationValue(value);
	}

	@Override
	protected String getLabeledMosesFeatureValue() {
		return NamedFeature.getLabeledFeaturRepresentation(featureName, computeMosesExponentRepresentationValue(value));
	}

	@Override
	public String getLabeledOriginalFeatureValue() {
		return getLabeledJoshuaFeatureValue();
	}

	@Override
	public Object getUnlabeledOriginalFeatureValue() {
		return this.value;
	}

	@Override
	public String getJoshuaQuantizationTypeString() {
		return JOSHUA_BYTE_TYPE_VALUE_STRING;
	}

	@Override
	protected boolean hasZeroValue() {
		return (this.value == 0);
	}

}
