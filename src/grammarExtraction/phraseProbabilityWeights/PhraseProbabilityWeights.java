package grammarExtraction.phraseProbabilityWeights;

import java.util.List;

import util.Pair;

public interface PhraseProbabilityWeights {

	public NamedProbabilityFeature getBasicPhraseProbabilityWeightTargetGivenSource();
	public NamedProbabilityFeature getBasicPhraseProbabilityWeightSourceGivenTarget();
	public List<NamedProbabilityFeature> getExtraSmoothingWeights();
	public List<NamedProbabilityFeature> getAllWeights();
	public Pair<Double> getSourceAndTargetPhraseFrequency();
	public PhraseProbabilityWeights createScaledCopy(double scalingFactor);
}
