package grammarExtraction.phraseProbabilityWeights;

import java.util.List;

import util.Pair;

public interface PhraseProbabilityWeightsSet {
	public NamedProbabilityFeature getBasicPhraseProbabilityWeightTargetGivenSource();
	public NamedProbabilityFeature getBasicPhraseProbabilityWeightSourceGivenTarget();
	public List<NamedProbabilityFeature> getExtraSmoothingWeights();
	public Pair<Double> getSourceAndTargetPhraseFrequency();
	// TODO : Implement createScaledCopy
	public PhraseProbabilityWeightsSet createScaledCopy(double scalingFactor);
}
