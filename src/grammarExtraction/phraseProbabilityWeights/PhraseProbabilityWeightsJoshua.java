package grammarExtraction.phraseProbabilityWeights;

import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsPair;
import java.util.ArrayList;
import java.util.List;
import util.Pair;

public class PhraseProbabilityWeightsJoshua implements PhraseProbabilityWeights {

	private final PhraseProbabilityWeightsPair basicPhraseProbabilityWeights;
	private final SmoothingWeights labelRefinementSmoothingWeights;
	private final Pair<Double> sourceAndTargetPhraseFrequency;

	protected PhraseProbabilityWeightsJoshua(PhraseProbabilityWeightsPair basicPhraseProbabilityWeights, SmoothingWeights labelRefinementSmoothingWeights,
			Pair<Double> sourceAndTargetPhraseFrequency) {
		this.basicPhraseProbabilityWeights = basicPhraseProbabilityWeights;
		this.labelRefinementSmoothingWeights = labelRefinementSmoothingWeights;
		this.sourceAndTargetPhraseFrequency = sourceAndTargetPhraseFrequency;
	}

	public static PhraseProbabilityWeightsJoshua createPhraseProbabilityWeightsJoshua(PhraseProbabilityWeightsPair basicPhraseProbabilityWeights, SmoothingWeights labelRefinementSmoothingWeights,
			Pair<Double> sourceAndTargetPhraseFrequency) {
		return new PhraseProbabilityWeightsJoshua(basicPhraseProbabilityWeights, labelRefinementSmoothingWeights, sourceAndTargetPhraseFrequency);
	}
	
	
	public static PhraseProbabilityWeightsJoshua createPhraseProbabilityWeightsJoshuaForTesting(PhraseProbabilityWeightsPair basicPhraseProbabilityWeights,
			SmoothingWeights labelRefinementSmoothingWeights, Pair<Double> sourceAndTargetPhraseFrequency) {
		return new PhraseProbabilityWeightsJoshua(basicPhraseProbabilityWeights, labelRefinementSmoothingWeights, sourceAndTargetPhraseFrequency);
	}

	


	@Override
	public NamedProbabilityFeature getBasicPhraseProbabilityWeightTargetGivenSource() {
		return this.basicPhraseProbabilityWeights.getBasicPhraseProbabilityWeightTargetGivenSource();
	}

	@Override
	public NamedProbabilityFeature getBasicPhraseProbabilityWeightSourceGivenTarget() {
		return this.basicPhraseProbabilityWeights.getBasicPhraseProbabilityWeightSourceGivenTarget();
	}

	@Override
	public List<NamedProbabilityFeature> getExtraSmoothingWeights() {
		return labelRefinementSmoothingWeights.getExtraSmoothingWeights();
	}

	@Override
	public List<NamedProbabilityFeature> getAllWeights() {
		List<NamedProbabilityFeature> result = new ArrayList<NamedProbabilityFeature>();
		result.addAll(this.basicPhraseProbabilityWeights.getAllProbabilities());
		result.addAll(this.getExtraSmoothingWeights());
		return null;
	}

	@Override
	public Pair<Double> getSourceAndTargetPhraseFrequency() {
		return sourceAndTargetPhraseFrequency;
	}

	@Override
	public PhraseProbabilityWeights createScaledCopy(double scalingFactor) {
		return new PhraseProbabilityWeightsJoshua(basicPhraseProbabilityWeights.createScaledCopy(scalingFactor),
				labelRefinementSmoothingWeights.createScaledCopy(scalingFactor), sourceAndTargetPhraseFrequency);
	}
}
