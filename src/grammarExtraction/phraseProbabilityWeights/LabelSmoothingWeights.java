package grammarExtraction.phraseProbabilityWeights;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

public class LabelSmoothingWeights implements SmoothingWeights {

	private final List<PhraseProbabilityWeightsPair> labelSmoothingWeights;

	protected LabelSmoothingWeights(List<PhraseProbabilityWeightsPair> labelSmoothingWeights) {
		this.labelSmoothingWeights = labelSmoothingWeights;
	}

	public static LabelSmoothingWeights createLabelSmoothingWeights(List<PhraseProbabilityWeightsPair> labelSmoothingWeights) {
		return new LabelSmoothingWeights(labelSmoothingWeights);
	}

	@Override
	public List<NamedProbabilityFeature> getExtraSmoothingWeights() {
		List<NamedProbabilityFeature> result = new ArrayList<NamedProbabilityFeature>();
		for (PhraseProbabilityWeightsPair weightsPair : labelSmoothingWeights) {
			result.addAll(weightsPair.getAllProbabilities());
		}
		return result;
	}

	public static SmoothingWeights createLabelSmoothingWeights(List<util.Pair<Double>> phraseProabilityPairs, List<String> smoothingPrefixStrings) {
		Assert.assertEquals(smoothingPrefixStrings.size(),phraseProabilityPairs.size());
		if (phraseProabilityPairs.isEmpty() || smoothingPrefixStrings.isEmpty()) {
			return EmptySmoothingWeights.createEmptySmoothingWeights();
		}

		List<PhraseProbabilityWeightsPair> smoothingProbabilityWeightPairs = new ArrayList<PhraseProbabilityWeightsPair>();

		for (int i = 0; i < phraseProabilityPairs.size(); i++) {
			util.Pair<Double> phraseProbabilities = phraseProabilityPairs.get(i);
			smoothingProbabilityWeightPairs.add(PhraseProbabilityWeightsPair.createPhraseProbabilityWeightsPair(phraseProbabilities.getFirst(),
					phraseProbabilities.getSecond(), smoothingPrefixStrings.get(i)));
		}
		Assert.assertTrue(smoothingProbabilityWeightPairs.size() > 0);
		return LabelSmoothingWeights.createLabelSmoothingWeights(smoothingProbabilityWeightPairs);
	}

	@Override
	public SmoothingWeights createScaledCopy(double scalingFactor) {
		List<PhraseProbabilityWeightsPair> scaledWeightsList = new ArrayList<PhraseProbabilityWeightsPair>();
		for (PhraseProbabilityWeightsPair phraseProbabilityWeightsPair : this.labelSmoothingWeights) {
			scaledWeightsList.add(phraseProbabilityWeightsPair.createScaledCopy(scalingFactor));
		}

		return new LabelSmoothingWeights(scaledWeightsList);
	}
}
