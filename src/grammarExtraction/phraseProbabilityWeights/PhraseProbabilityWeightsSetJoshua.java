package grammarExtraction.phraseProbabilityWeights;

import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeights;
import java.util.ArrayList;
import java.util.List;

import util.Pair;

public class PhraseProbabilityWeightsSetJoshua implements PhraseProbabilityWeightsSet {

	protected final PhraseProbabilityWeights basicPhraseProbabilityWeights;
	protected final PhraseProbabilityWeights wordEnrichmentSmoothingWeights;

	protected PhraseProbabilityWeightsSetJoshua(PhraseProbabilityWeights basicPhraseProbabilityWeights, PhraseProbabilityWeights wordEnrichmentSmoothingWeights) {
		this.basicPhraseProbabilityWeights = basicPhraseProbabilityWeights;
		this.wordEnrichmentSmoothingWeights = wordEnrichmentSmoothingWeights;
	}

	public static PhraseProbabilityWeightsSetJoshua createPhraseProbabilityWeightsSetJoshua(PhraseProbabilityWeights basicPhraseProbabilityWeights,
			PhraseProbabilityWeights wordEnrichmentSmoothingWeights) {
		return new PhraseProbabilityWeightsSetJoshua(basicPhraseProbabilityWeights, wordEnrichmentSmoothingWeights);
	}

	@Override
	public NamedProbabilityFeature getBasicPhraseProbabilityWeightTargetGivenSource() {
		return basicPhraseProbabilityWeights.getBasicPhraseProbabilityWeightTargetGivenSource();
	}

	@Override
	public NamedProbabilityFeature getBasicPhraseProbabilityWeightSourceGivenTarget() {
		return basicPhraseProbabilityWeights.getBasicPhraseProbabilityWeightSourceGivenTarget();
	}

	@Override
	public List<NamedProbabilityFeature> getExtraSmoothingWeights() {
		List<NamedProbabilityFeature> result = new ArrayList<NamedProbabilityFeature>();
		result.addAll(basicPhraseProbabilityWeights.getExtraSmoothingWeights());
		result.addAll(this.wordEnrichmentSmoothingWeights.getAllWeights());
		return result;
	}

	@Override
	public Pair<Double> getSourceAndTargetPhraseFrequency() {
		return this.basicPhraseProbabilityWeights.getSourceAndTargetPhraseFrequency();
	}

	@Override
	public PhraseProbabilityWeightsSet createScaledCopy(double scalingFactor) {
		return new PhraseProbabilityWeightsSetJoshua(basicPhraseProbabilityWeights.createScaledCopy(scalingFactor),
				wordEnrichmentSmoothingWeights.createScaledCopy(scalingFactor));
	}

}
