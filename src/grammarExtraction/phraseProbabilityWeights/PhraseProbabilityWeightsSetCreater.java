package grammarExtraction.phraseProbabilityWeights;

import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

public interface PhraseProbabilityWeightsSetCreater {
	public PhraseProbabilityWeightsSet createProbabilityWeightsSet(TranslationRuleSignatureTriple translationRuleSignatureTriple);
}
