package grammarExtraction.phraseProbabilityWeights;

import java.util.List;

public interface SmoothingWeights {
	 List<NamedProbabilityFeature> getExtraSmoothingWeights();
	public SmoothingWeights createScaledCopy(double scalingFactor);
}
