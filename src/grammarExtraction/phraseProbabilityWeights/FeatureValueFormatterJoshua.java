package grammarExtraction.phraseProbabilityWeights;

public class FeatureValueFormatterJoshua implements FeatureValueFormatter {

	@Override
	public float getUnlabeledFeatureValue(NamedFeature namedFeature) {
		return namedFeature.getUnlabeledJoshuaFeatureValue();
	}

	@Override
	public String getLabeledFeatureValue(NamedFeature namedFeature) {
		return namedFeature.getLabeledJoshuaFeatureValue();
	}

}
