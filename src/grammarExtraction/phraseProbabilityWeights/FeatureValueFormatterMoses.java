package grammarExtraction.phraseProbabilityWeights;

public class FeatureValueFormatterMoses implements FeatureValueFormatter {

	@Override
	public float getUnlabeledFeatureValue(NamedFeature namedFeature) {
		return namedFeature.getUnlabeledMosesFeatureValue();
	}

	@Override
	public String getLabeledFeatureValue(NamedFeature namedFeature) {
		return namedFeature.getLabeledMosesFeatureValue();
	}

}
