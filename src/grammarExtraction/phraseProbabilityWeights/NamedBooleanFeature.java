package grammarExtraction.phraseProbabilityWeights;

public class NamedBooleanFeature extends NamedFeature {

	private static Float JOSHUA_FALSE_BOOLEAN_VALUE = new Float(0);
	private static Float JOSHUA_TRUE_BOOLEAN_VALUE = new Float(1);
	private static Float MOSES_FALSE_BOOLEAN_VALUE = new Float(1);
	private static Float MOSES_TRUE_BOOLEAN_VALUE = new Float(Math.E);
	private static final String JOSHUA_BOOLEAN_TYPE_VALUE_STRING = "boolean";

	private final boolean featureValue;

	protected NamedBooleanFeature(String featureName, boolean featureValue) {
		super(featureName);
		this.featureValue = featureValue;
	}

	public static NamedBooleanFeature createNamedBooleanFeature(String featureName, boolean featureValue) {
		return new NamedBooleanFeature(featureName, featureValue);
	}

	@Override
	protected float getUnlabeledJoshuaFeatureValue() {
		return joshuaBinaryFeatureValue(featureValue).floatValue();
	}

	@Override
	protected String getLabeledJoshuaFeatureValue() {
		return getLabeledFeaturRepresentation(featureName, joshuaBinaryFeatureValue(featureValue));
	}

	@Override
	protected float getUnlabeledMosesFeatureValue() {
		return mosesBinaryFeatureValue(featureValue);
	}

	@Override
	protected String getLabeledMosesFeatureValue() {
		return getLabeledFeaturRepresentation(featureName, mosesBinaryFeatureValue(featureValue));
	}

	public static Float joshuaBinaryFeatureValue(boolean featureIsTrue) {
		if (featureIsTrue) {
			return JOSHUA_TRUE_BOOLEAN_VALUE;
		} else {
			return JOSHUA_FALSE_BOOLEAN_VALUE;
		}
	}

	public static Float mosesBinaryFeatureValue(boolean featureIsTrue) {
		if (featureIsTrue) {
			return MOSES_TRUE_BOOLEAN_VALUE;
		} else {
			return MOSES_FALSE_BOOLEAN_VALUE;
		}
	}

	public boolean getBooleanValue() {
		return this.featureValue;
	}

	@Override
	public String getLabeledOriginalFeatureValue() {
		return getLabeledFeaturRepresentation(featureName, new Boolean(getBooleanValue()));
	}

	@Override
	public Object getUnlabeledOriginalFeatureValue() {
		return new Boolean(getBooleanValue()).toString();
	}

	@Override
	public String getJoshuaQuantizationTypeString() {
		return JOSHUA_BOOLEAN_TYPE_VALUE_STRING;
	}

	@Override
	protected boolean hasZeroValue() {
		return !(this.featureValue == true); 
	}
}