package grammarExtraction.phraseProbabilityWeights;

import java.util.ArrayList;
import java.util.List;

import util.StringGenerator;
import util.Utility;

public abstract class NamedFeature {

	protected final String featureName;

	protected NamedFeature(String featureName) {
		this.featureName = featureName;
	}

	public String getFeatureName() {
		return this.featureName;
	}

	public abstract Object getUnlabeledOriginalFeatureValue();

	public abstract String getLabeledOriginalFeatureValue();

	protected abstract float getUnlabeledJoshuaFeatureValue();

	protected abstract String getLabeledJoshuaFeatureValue();

	protected abstract float getUnlabeledMosesFeatureValue();

	protected abstract String getLabeledMosesFeatureValue();
	
	protected abstract boolean hasZeroValue();

	public float getUnlabeledFeatureValue(FeatureValueFormatter featureValueFormatter) {
		return featureValueFormatter.getUnlabeledFeatureValue(this);
	}

	public String getLabeledFeatureValue(FeatureValueFormatter featureValueFormatter) {
		return featureValueFormatter.getLabeledFeatureValue(this);
	}

	public float getUnlabeledSystemFormattedFeatureValue(FeatureValueFormatter featureValueFormatter) {
		return featureValueFormatter.getUnlabeledFeatureValue(this);
	}

	public float geLabeledSystemFormattedFeatureValue(FeatureValueFormatter featureValueFormatter) {
		return featureValueFormatter.getUnlabeledFeatureValue(this);
	}

	public abstract String getJoshuaQuantizationTypeString();

	protected static String getLabeledFeaturRepresentation(String label, Object featureValue) {
		return label + "=" + featureValue;
	}

	public static String createLabeledFeatureStringListJoshuaFormat(List<NamedFeature> namedFeatureList) {
		List<StringGenerator> stringGeneratorsList = new ArrayList<StringGenerator>();
		for (NamedFeature namedFeature : namedFeatureList) {
			stringGeneratorsList.add(new NamedFeatureLabeledFeatureJoshuaFormatStringGenerator(namedFeature));
		}
		return Utility.stringGeneratorListStringWithoutBrackets(stringGeneratorsList);
	}

	public static String createLabeledFeatureStringListOriginalFormat(List<NamedFeature> namedFeatureList) {
		List<StringGenerator> stringGeneratorsList = new ArrayList<StringGenerator>();
		for (NamedFeature namedFeature : namedFeatureList) {
			stringGeneratorsList.add(new NamedFeatureLabeledFeatureOriginalFormatStringGenerator(namedFeature));
		}
		return Utility.stringGeneratorListStringWithoutBrackets(stringGeneratorsList);
	}

	public static String createUnlabeledFeatureStringListOriginalFormat(List<NamedFeature> namedFeatureList) {
		List<StringGenerator> stringGeneratorsList = new ArrayList<StringGenerator>();
		for (NamedFeature namedFeature : namedFeatureList) {
			stringGeneratorsList.add(new NamedFeatureUnLabeledFeatureOriginalFormatStringGenerator(namedFeature));
		}
		return Utility.stringGeneratorListStringWithoutBrackets(stringGeneratorsList);
	}

	public static List<Float> createUnlabeledFeatureValuesListJoshuaFormat(List<NamedFeature> namedFeatureList) {
		List<Float> result = new ArrayList<Float>();
		for (NamedFeature namedFeature : namedFeatureList) {
			result.add(namedFeature.getUnlabeledJoshuaFeatureValue());
		}
		return result;
	}

	private static class NamedFeatureLabeledFeatureJoshuaFormatStringGenerator implements StringGenerator {
		private final NamedFeature namedFeature;

		private NamedFeatureLabeledFeatureJoshuaFormatStringGenerator(NamedFeature namedFeature) {
			this.namedFeature = namedFeature;
		}

		@Override
		public String generateValue() {
			return this.namedFeature.getLabeledJoshuaFeatureValue();
		}
	}

	private static class NamedFeatureLabeledFeatureOriginalFormatStringGenerator implements StringGenerator {
		private final NamedFeature namedFeature;

		private NamedFeatureLabeledFeatureOriginalFormatStringGenerator(NamedFeature namedFeature) {
			this.namedFeature = namedFeature;
		}

		@Override
		public String generateValue() {
			return this.namedFeature.getLabeledOriginalFeatureValue();
		}
	}

	private static class NamedFeatureUnLabeledFeatureOriginalFormatStringGenerator implements StringGenerator {
		private final NamedFeature namedFeature;

		private NamedFeatureUnLabeledFeatureOriginalFormatStringGenerator(NamedFeature namedFeature) {
			this.namedFeature = namedFeature;
		}

		@Override
		public String generateValue() {
			return "" + this.namedFeature.getUnlabeledOriginalFeatureValue();
		}
	}

	protected float computeMosesExponentRepresentationValue(double value) {
		return (float) Math.exp(value);
	}

}
