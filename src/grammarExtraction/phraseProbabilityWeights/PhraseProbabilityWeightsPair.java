package grammarExtraction.phraseProbabilityWeights;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

public class PhraseProbabilityWeightsPair {

	private static final String BASIC_PHRASE_PROBAILITY_PAIR_PREFIX = "Basic";
	private final static String SOURCE_GIVEN_TARGET_PHRASE_WEIGHT_STRING = "Source_given_Target_PhraseWeight";
	private final static String TARGET_GIVEN_SOURCE_PHRASE_WEIGHT_STRING = "Target_given_Source_PhraseWeight";

	private final NamedProbabilityFeature phraseProbabilityWeightTargetGivenSource;
	private final NamedProbabilityFeature phraseProbabilityWeightSourceGivenTarget;

	protected PhraseProbabilityWeightsPair(NamedProbabilityFeature phraseProbabilityWeightTargetGivenSource,
			NamedProbabilityFeature phraseProbabilityWeightSourceGivenTarget) {
		this.phraseProbabilityWeightTargetGivenSource = phraseProbabilityWeightTargetGivenSource;
		this.phraseProbabilityWeightSourceGivenTarget = phraseProbabilityWeightSourceGivenTarget;
	}

	public static PhraseProbabilityWeightsPair createPhraseProbabilityWeightsPair(double phraseProbabilityTargetGivenSource,
			double phraseProbabilitySourceGivenTarget, String weightLabelPrefix) {
		return new PhraseProbabilityWeightsPair(createTargetGivenSourceFeature(weightLabelPrefix, phraseProbabilityTargetGivenSource),
				createSourceGivenTargetFeature(weightLabelPrefix, phraseProbabilitySourceGivenTarget));
	}

	public static PhraseProbabilityWeightsPair createPhraseProbabilityWeightsPair(util.Pair<Double> phraseProbabilities, String weightLabelPrefix) {
		return new PhraseProbabilityWeightsPair(createTargetGivenSourceFeature(weightLabelPrefix, phraseProbabilities.getFirst()),
				createSourceGivenTargetFeature(weightLabelPrefix, phraseProbabilities.getSecond()));
	}

	public static PhraseProbabilityWeightsPair createBasicPhraseProbabilityWeightsPair(double phraseProbabilityTargetGivenSource,
			double phraseProbabilitySourceGivenTarget) {
		return createPhraseProbabilityWeightsPair(phraseProbabilityTargetGivenSource, phraseProbabilitySourceGivenTarget, BASIC_PHRASE_PROBAILITY_PAIR_PREFIX);
	}

	public static PhraseProbabilityWeightsPair createBasicPhraseProbabilityWeightsPair(util.Pair<Double> phraseProbabilities) {
		return createPhraseProbabilityWeightsPair(phraseProbabilities, BASIC_PHRASE_PROBAILITY_PAIR_PREFIX);
	}

	public NamedProbabilityFeature getBasicPhraseProbabilityWeightTargetGivenSource() {
		return this.phraseProbabilityWeightTargetGivenSource;
	}

	public NamedProbabilityFeature getBasicPhraseProbabilityWeightSourceGivenTarget() {
		return this.phraseProbabilityWeightSourceGivenTarget;
	}

	public PhraseProbabilityWeightsPair createScaledCopy(double scalingFactor) {
		Assert.assertTrue(scalingFactor <= 1);
		NamedProbabilityFeature phraseProbabilityWeightTargetGivenSourceScaled = NamedProbabilityFeature.createNamedProbabilityFeature(
				this.phraseProbabilityWeightTargetGivenSource.getFeatureName(), this.phraseProbabilityWeightTargetGivenSource.getProbability() * scalingFactor);
		NamedProbabilityFeature phraseProbabilityWeightSourceGivenTargetScaled = NamedProbabilityFeature.createNamedProbabilityFeature(
				this.phraseProbabilityWeightSourceGivenTarget.getFeatureName(), this.phraseProbabilityWeightSourceGivenTarget.getProbability() * scalingFactor);
		Assert.assertTrue(phraseProbabilityWeightSourceGivenTargetScaled.getProbability() <= 1);
		Assert.assertTrue(phraseProbabilityWeightTargetGivenSource.getProbability() <= 1);
		return new PhraseProbabilityWeightsPair(phraseProbabilityWeightTargetGivenSourceScaled, phraseProbabilityWeightSourceGivenTargetScaled);
	}

	public List<NamedProbabilityFeature> getAllProbabilities() {
		List<NamedProbabilityFeature> result = new ArrayList<NamedProbabilityFeature>();
		result.add(this.phraseProbabilityWeightTargetGivenSource);
		result.add(phraseProbabilityWeightSourceGivenTarget);
		return result;
	}

	private static NamedProbabilityFeature createSourceGivenTargetFeature(String weightLabelPrefix, double probability) {
		return NamedProbabilityFeature.createNamedProbabilityFeature(getSourceGivenTargetPhraseWeightString(weightLabelPrefix), probability);
	}

	private static NamedProbabilityFeature createTargetGivenSourceFeature(String weightLabelPrefix, double probability) {
		return NamedProbabilityFeature.createNamedProbabilityFeature(getTargetGivenSourcePhraseWeightString(weightLabelPrefix), probability);
	}

	private static String getTargetGivenSourcePhraseWeightString(String weightLabelPrefix) {
		return weightLabelPrefix + "_" + TARGET_GIVEN_SOURCE_PHRASE_WEIGHT_STRING;
	}

	private static String getSourceGivenTargetPhraseWeightString(String weightLabelPrefix) {
		return weightLabelPrefix + "_" + SOURCE_GIVEN_TARGET_PHRASE_WEIGHT_STRING;
	}

	public static List<String> getPhraseLabelsForWeightLabelPrefix(String weightLabelPrefix) {
		List<String> result = new ArrayList<String>();
		result.add(getTargetGivenSourcePhraseWeightString(weightLabelPrefix));
		result.add(getSourceGivenTargetPhraseWeightString(weightLabelPrefix));
		return result;
	}

	public static List<String> getBasicPhraseWeightsLabels() {
		return getPhraseLabelsForWeightLabelPrefix(BASIC_PHRASE_PROBAILITY_PAIR_PREFIX);
	}

}
