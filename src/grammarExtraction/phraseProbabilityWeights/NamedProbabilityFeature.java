package grammarExtraction.phraseProbabilityWeights;

import junit.framework.Assert;
import grammarExtraction.translationRuleTrie.ProbabilityRepresentation;

public class NamedProbabilityFeature extends NamedFeature {
	private final double probability;

	protected NamedProbabilityFeature(String featureName, double probability) {
		super(featureName);
		this.probability = probability;
	}

	public static NamedProbabilityFeature createNamedProbabilityFeature(String featureName, double probability) {
		//Assert.assertTrue(probability <= 1);
	    	if(probability > 1){
	    	    throw new RuntimeException("Error: probability for " + featureName + "  : " + probability + " > 1");
	    	}
		return new NamedProbabilityFeature(featureName, probability);
	}

	private float getJoshuaLogProbability() {
		return ProbabilityRepresentation.getJoshuaLogProbabilityRepresentation(probability);
	}

	@Override
	protected float getUnlabeledJoshuaFeatureValue() {
		return getJoshuaLogProbability();
	}

	@Override
	protected String getLabeledJoshuaFeatureValue() {
		return NamedFeature.getLabeledFeaturRepresentation(featureName, new Float(getJoshuaLogProbability()));
	}

	@Override
	protected float getUnlabeledMosesFeatureValue() {
		return (float) probability;
	}

	@Override
	protected String getLabeledMosesFeatureValue() {
		return NamedFeature.getLabeledFeaturRepresentation(featureName, new Float(this.probability));
	}

	public double getProbability() {
		return this.probability;
	}

	@Override
	public String getLabeledOriginalFeatureValue() {
		return NamedFeature.getLabeledFeaturRepresentation(featureName, this.probability);
	}

	@Override
	public Object getUnlabeledOriginalFeatureValue() {
		return this.probability;
	}

	@Override
	public String getJoshuaQuantizationTypeString() {
		return NamedFloatingFeature.JOSHUA_FLOAT_TYPE_VALUE_STRING;
	}

	@Override
	protected boolean hasZeroValue() {
		return (this.probability == 1);
	}

}
