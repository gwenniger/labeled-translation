package grammarExtraction.phraseProbabilityWeights;

import java.util.Collections;
import java.util.List;

import util.Pair;

public class EmptyPhraseProbabilityWeights implements PhraseProbabilityWeights {

	@Override
	public List<NamedProbabilityFeature> getExtraSmoothingWeights() {
		return Collections.emptyList();
	}

	@Override
	public NamedProbabilityFeature getBasicPhraseProbabilityWeightTargetGivenSource() {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public NamedProbabilityFeature getBasicPhraseProbabilityWeightSourceGivenTarget() {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public List<NamedProbabilityFeature> getAllWeights() {
		return Collections.emptyList();
	}

	@Override
	public Pair<Double> getSourceAndTargetPhraseFrequency() {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public PhraseProbabilityWeights createScaledCopy(double scalingFactor) {
		return new EmptyPhraseProbabilityWeights();
	}
}
