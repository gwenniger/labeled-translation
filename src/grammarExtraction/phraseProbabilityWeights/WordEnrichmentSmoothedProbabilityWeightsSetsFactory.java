package grammarExtraction.phraseProbabilityWeights;

import java.util.ArrayList;
import java.util.List;

import mt_pipeline.preAndPostProcessing.EnrichedSourceFileCreator;

import grammarExtraction.IntegerKeyRuleRepresentation;
import grammarExtraction.translationRuleTrie.TranslationRuleProbabilityTable;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

public class WordEnrichmentSmoothedProbabilityWeightsSetsFactory extends BasicPhraseProbabilityWeightsSetsFactory {

	private final TranslationRuleProbabilityTable wordEnrichmentSmoothedProbabilityTable;

	private WordEnrichmentSmoothedProbabilityWeightsSetsFactory(TranslationRuleProbabilityTable mainRuleProbabilityTable, TranslationRuleProbabilityTable wordEnrichmentSmoothedProbabilityTable) {
		super(mainRuleProbabilityTable);
		this.wordEnrichmentSmoothedProbabilityTable = wordEnrichmentSmoothedProbabilityTable;
	}

	public static WordEnrichmentSmoothedProbabilityWeightsSetsFactory createWordEnrichmentSmoothedProbabilityWeightsSetsFactory(TranslationRuleProbabilityTable mainRuleProbabilityTable,
			TranslationRuleProbabilityTable wordEnrichmentSmoothedProbabilityTable) {
		return new WordEnrichmentSmoothedProbabilityWeightsSetsFactory(mainRuleProbabilityTable, wordEnrichmentSmoothedProbabilityTable);
	}

	public PhraseProbabilityWeightsSet createWordEnrichmentSmoothedProbabilityWeightsSet(TranslationRuleSignatureTriple translationRuleSignatureTriple) {
		// System.out.println("<<<<>>>> createWordEnrichmentSmoothedProbabilityWeightsSet called...");
		IntegerKeyRuleRepresentation wordEnrichmentStrippedSourceSide = createBareWordsRuleRepresentationFromWordEnrichedRuleRepresentation(translationRuleSignatureTriple
				.getSourceSideRepresentation());
		IntegerKeyRuleRepresentation wordEnrichmentStrippedTargetSide = createBareWordsRuleRepresentationFromWordEnrichedRuleRepresentation(translationRuleSignatureTriple
				.getTargetSideRepresentation());
		TranslationRuleSignatureTriple wordStrippedTranslationRuleSignatureTriple = TranslationRuleSignatureTriple.createTranslationRuleSignatureTriple(wordEnrichmentStrippedSourceSide,
				wordEnrichmentStrippedTargetSide, translationRuleSignatureTriple.getRuleLabel());
		return PhraseProbabilityWeightsSetJoshua.createPhraseProbabilityWeightsSetJoshua(
				this.mainRuleProbabilityTable.createPhraseProbabilityWeights(wordStrippedTranslationRuleSignatureTriple),
				this.wordEnrichmentSmoothedProbabilityTable.createPhraseProbabilityWeights(wordStrippedTranslationRuleSignatureTriple));
	}

	private String getBareWordFromEnrichedWordString(String enrichedWordString) {
		String[] parts = enrichedWordString.split(EnrichedSourceFileCreator.WORD_ENRICHMENT_SEPARATOR);
		String result = parts[0];
		return result;
	}

	private final IntegerKeyRuleRepresentation createBareWordsRuleRepresentationFromWordEnrichedRuleRepresentation(IntegerKeyRuleRepresentation wordEnrichedRuleSide) {
		List<String> rulePartStrings = wordEnrichedRuleSide.getStringListRepresentation(this.mainRuleProbabilityTable.getWordKeyMappingTable());

		List<String> bareWordRulePartStrings = new ArrayList<String>();
		for (String rulePartString : rulePartStrings) {
			if (WordKeyMappingTable.isLabel(rulePartString)) {
				bareWordRulePartStrings.add(rulePartString);
			} else {
				bareWordRulePartStrings.add(getBareWordFromEnrichedWordString(rulePartString));
			}
		}
		IntegerKeyRuleRepresentation result = IntegerKeyRuleRepresentation.createIntegerKeyRuleRepresentation(bareWordRulePartStrings,
				this.wordEnrichmentSmoothedProbabilityTable.getWordKeyMappingTable());
		return result;
	}

}
