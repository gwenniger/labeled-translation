package grammarExtraction.phraseProbabilityWeights;

public class NamedFloatingFeature extends NamedFeature {

	protected static final String JOSHUA_FLOAT_TYPE_VALUE_STRING = "float";
	private final double value;

	protected NamedFloatingFeature(String featureName, double value) {
		super(featureName);
		this.value = value;
	}

	public static NamedFloatingFeature createNamedFloatingFeature(String featureName, double value) {
		return new NamedFloatingFeature(featureName, value);
	}

	@Override
	public float getUnlabeledJoshuaFeatureValue() {
		return (float) value;
	}

	@Override
	protected String getLabeledJoshuaFeatureValue() {
		return NamedFeature.getLabeledFeaturRepresentation(featureName, (float) value);
	}

	@Override
	protected float getUnlabeledMosesFeatureValue() {
		return computeMosesExponentRepresentationValue(value);
	}

	@Override
	protected String getLabeledMosesFeatureValue() {
		return NamedFeature.getLabeledFeaturRepresentation(featureName, computeMosesExponentRepresentationValue(value));
	}

	@Override
	public String getLabeledOriginalFeatureValue() {
		return NamedFeature.getLabeledFeaturRepresentation(featureName, value);
	}

	@Override
	public Object getUnlabeledOriginalFeatureValue() {
		return this.value;
	}

	@Override
	public String getJoshuaQuantizationTypeString() {
		return JOSHUA_FLOAT_TYPE_VALUE_STRING;
	}

	@Override
	protected boolean hasZeroValue() {
		return (this.value == 0);
	}
}
