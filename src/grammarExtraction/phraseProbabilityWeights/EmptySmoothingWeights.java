package grammarExtraction.phraseProbabilityWeights;

import java.util.Collections;
import java.util.List;

public class EmptySmoothingWeights implements SmoothingWeights {

	public static EmptySmoothingWeights createEmptySmoothingWeights() {
		return new EmptySmoothingWeights();
	}

	@Override
	public List<NamedProbabilityFeature> getExtraSmoothingWeights() {
		return Collections.emptyList();
	}

	@Override
	public SmoothingWeights createScaledCopy(double scalingFactor) {
		return createEmptySmoothingWeights();
	}
}
