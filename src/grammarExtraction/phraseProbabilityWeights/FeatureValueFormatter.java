package grammarExtraction.phraseProbabilityWeights;

public interface FeatureValueFormatter {
	public float getUnlabeledFeatureValue(NamedFeature namedFeature);
	public String getLabeledFeatureValue(NamedFeature namedFeature);
}
