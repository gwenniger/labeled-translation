package grammarExtraction.phraseProbabilityWeights;

import grammarExtraction.translationRuleTrie.TranslationRuleProbabilityTable;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

public class BasicPhraseProbabilityWeightsSetsFactory {

	protected final TranslationRuleProbabilityTable mainRuleProbabilityTable;

	public BasicPhraseProbabilityWeightsSetsFactory(TranslationRuleProbabilityTable mainRuleProbabilityTable) {
		this.mainRuleProbabilityTable = mainRuleProbabilityTable;
	}

	public PhraseProbabilityWeightsSet createProbabilityWeightsSet(TranslationRuleSignatureTriple translationRuleSignatureTriple) {
		// System.out.println(">>>><<<< createBasicUnsmoothedProbabilityWeightsSet called...");
		return PhraseProbabilityWeightsSetJoshua.createPhraseProbabilityWeightsSetJoshua(
				this.mainRuleProbabilityTable.createPhraseProbabilityWeights(translationRuleSignatureTriple), new EmptyPhraseProbabilityWeights());
	}

}
