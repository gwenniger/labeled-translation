package grammarExtraction;

import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.RuleGap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;

public class IntegerKeyRuleRepresentation implements Serializable, Comparable<IntegerKeyRuleRepresentation> {
	private static final long serialVersionUID = 1L;
	protected static final GapFormatter MOSES_GAP_FORMATTER = new MosesGapFormatter();
	final List<Integer> partKeys;

	protected IntegerKeyRuleRepresentation(List<Integer> partKeys) {
		this.partKeys = partKeys;
	}

	public static IntegerKeyRuleRepresentation createIntegerKeyRuleRepresentation(List<String> ruleComponents, WordKeyMappingTable wordKeyMappingTable) {
		return new IntegerKeyRuleRepresentation(getKeyListForRuleComponentsList(ruleComponents, wordKeyMappingTable));
	}

	public static IntegerKeyRuleRepresentation createIntegerKeyRuleRepresentation(String ruleString, WordKeyMappingTable wordKeyMappingTable) {
		return new IntegerKeyRuleRepresentation(getKeyListForRuleString(ruleString, wordKeyMappingTable));
	}

	public static IntegerKeyRuleRepresentation createIntegerKeyRuleRepresentation(List<Integer> partKeys) {
		return new IntegerKeyRuleRepresentation(partKeys);
	}

	public static IntegerKeyRuleRepresentation createIntegerKeyRuleRepresentationRemovingLabel(IntegerKeyRuleRepresentationWithLabel integerKeyRuleRepresentationWithLabel) {
		return new IntegerKeyRuleRepresentation(integerKeyRuleRepresentationWithLabel.partKeys.subList(0, integerKeyRuleRepresentationWithLabel.partKeys.size() - 1));
	}

	public static List<Integer> getKeyListForRuleComponentsList(List<String> ruleComponents, WordKeyMappingTable wordKeyMappingTable) {
		List<Integer> result = new ArrayList<Integer>();
		for (String word : ruleComponents) {
			result.add(wordKeyMappingTable.getKeyForWord(word));
		}
		return result;
	}

	public static List<Integer> getKeyListForRuleString(String ruleString, WordKeyMappingTable wordKeyMappingTable) {
		return getKeyListForRuleComponentsList(Arrays.asList(ruleString.split(" ")), wordKeyMappingTable);
	}

	@Override
	public boolean equals(Object object2) {
		if (object2 instanceof IntegerKeyRuleRepresentation) {
			return ((IntegerKeyRuleRepresentation) object2).partKeys.equals(this.partKeys);
		}
		return false;
	}

	public int polynomialHashCode() {
		int result = 0;
		int base = 200003;

		if (partKeys.size() == 0) {
			return 0;
		}

		result = partKeys.get(0);

		int multiplier = base;
		for (int i = 1; i < partKeys.size(); i++) {
			result += partKeys.get(i) * multiplier;
			multiplier *= base;
		}
		return result;
	}

	public int cyclicShifHashCode() {
		int result = 0;

		for (int i = 0; i < partKeys.size(); i++) {
			// Best so far: 12 / 20 combined with shuffling of words in
			// WordKeyMappingTable
			result = (result << 12 | result >>> 20);
			result += partKeys.get(i);
		}
		return result;
	}

	@Override
	public int hashCode() {
		// return cyclicShifHashCode();
		return polynomialHashCode();
	}

	public int getKey(int index) {
		return this.partKeys.get(index);
	}

	public boolean isLastIndex(int index) {
		return (index == (this.partKeys.size() - 1));
	}

	protected static String getStringRepresentation(List<Integer> keys, WordKeyMappingTable wordKeyMappingTable, GapFormatter gapFormatter) {
		String result = "";

		int i;
		for (i = 0; i < keys.size() - 1; i++) {
			result += gapFormatter.formatGapOrNonGap(wordKeyMappingTable.getWordForKey(keys.get(i))) + " ";
		}

		// add the last key
		if (!keys.isEmpty()) {
			result += gapFormatter.formatGapOrNonGap(wordKeyMappingTable.getWordForKey(keys.get(i)));
		}

		return result;
	}

	protected static String getStringRepresentation(List<Integer> keys, WordKeyMappingTable wordKeyMappingTable) {
		String result = "";

		int i;
		for (i = 0; i < keys.size() - 1; i++) {
			result += wordKeyMappingTable.getWordForKey(keys.get(i)) + " ";
		}

		// add the last key
		if (!keys.isEmpty()) {
			result += wordKeyMappingTable.getWordForKey(keys.get(i));
		}

		return result;
	}

	public List<String> getStringListRepresentation(WordKeyMappingTable wordKeyMappingTable) {
		List<String> result = new ArrayList<String>();
		int i;
		for (i = 0; i < this.partKeys.size(); i++) {
			result.add(wordKeyMappingTable.getWordForKey(this.partKeys.get(i)));
		}
		return result;
	}

	public String getStringRepresentation(WordKeyMappingTable wordKeyMappingTable) {
		return getStringRepresentation(this.partKeys, wordKeyMappingTable);
	}

	public String toString() {
		String result = "<IKRR>";

		for (Integer key : this.partKeys) {
			result += key + " ";
		}
		result += "</IKRR>";
		result = result.trim();
		return result;
	}

	public List<Integer> getPartKeys() {
		return this.partKeys;
	}

	@Override
	public int compareTo(IntegerKeyRuleRepresentation integerKeyRepresentation2) {
		// By convention the objext with the longer partKeys list is "bigger"
		if (this.partKeys.size() > integerKeyRepresentation2.partKeys.size()) {
			return 1;
		} else if (this.partKeys.size() < integerKeyRepresentation2.partKeys.size()) {
			return -1;
		} else {
			for (int i = 0; i < this.partKeys.size(); i++) {
				int difference = this.partKeys.get(i) - integerKeyRepresentation2.partKeys.get(i);

				if (difference != 0) {
					return difference;
				}
			}
			// All keys are the same, they two objects are equal
			return 0;
		}
	}

	public boolean hasTerminals(WordKeyMappingTable wordKeyMappingTable) {
		for (Integer sourceKey : this.partKeys) {
			if (wordKeyMappingTable.isWordKey(sourceKey)) {
				return true;
			}
		}
		return false;
	}

	public IntegerKeyRuleRepresentation createIntegerKeyRuleRepresentationWithNewLastElement(Integer newLastElement) {
		List<Integer> newPartKeys = new ArrayList<Integer>(this.partKeys);
		newPartKeys.set(this.partKeys.size() - 1, newLastElement);
		return createIntegerKeyRuleRepresentation(newPartKeys);
	}

	private static abstract class GapFormatter {

		protected abstract String formatGap(String gapString);

		private boolean isGap(String string) {
			return (WordKeyMappingTable.isLabelOne(string) || WordKeyMappingTable.isLabelTwo(string));
		}

		public String formatGapOrNonGap(String string) {
			if (isGap(string)) {
				return formatGap(string);
			}
			return string;
		}

	}

	protected static class JoshuaGapFormatter extends GapFormatter {
		@Override
		protected String formatGap(String gapString) {
			return gapString;
		}
	}

	protected static class MosesGapFormatter extends GapFormatter {

		@Override
		protected String formatGap(String gapString) {
			String gapLabel = gapString.substring(1);
			Assert.assertTrue(WordKeyMappingTable.isLabel(gapString) || WordKeyMappingTable.isLabelTwo(gapString));
			// We want to remove the last three characters, i.e. ",1]" or ",2]"
			gapLabel = gapLabel.substring(0, gapLabel.length() - 3);
			String result = RuleGap.ruleGapStringMosesStringToTree(gapLabel);
			return result;
		}
	}

}
