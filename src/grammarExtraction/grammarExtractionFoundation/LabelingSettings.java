package grammarExtraction.grammarExtractionFoundation;

import java.io.Serializable;

import reorderingLabeling.FinePhraseCentricReorderingLabelBasic;
import mt_pipeline.MTPipelineHatsBoundaryTagLabeled;
import mt_pipeline.mt_config.MTConfigFile;
import mt_pipeline.mt_config.MTGrammarExtractionConfig;
import mt_pipeline.mt_config.SystemSpecificSupportObjectsFactory;
import grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagLabelingSettings;
import grammarExtraction.chiangGrammarExtraction.LabelSideSettings;
import grammarExtraction.chiangGrammarExtraction.ReorderLabelingSettings;
import grammarExtraction.grammarExtractionFoundation.LabelingSettings.CanonicalFormSettings;
import grammarExtraction.labelSmoothing.LabeledRulesSmoother;
import grammarExtraction.labelSmoothing.LabeledRulesSmoother.LABEL_SMOOTHING_TYPE;

public class LabelingSettings implements Serializable {

    private static final long serialVersionUID = 1L;
    private final BoundaryTagLabelingSettings boundaryTagLabelingSettings;
    private final ReorderLabelingSettings reorderLabelingSettings;
    private final boolean useNonHieroLabels;
    private final LabelSideSettings labelSideSettings;
    private final CanonicalFormSettings canonicalFormSettings;

    private LabelingSettings(BoundaryTagLabelingSettings boundaryTagLabelingSettings,
	    ReorderLabelingSettings reorderLabelingSettings, boolean useNonHieroLabels,
	    LabelSideSettings labelSideSettings, CanonicalFormSettings canonicalFormSettings) {
	this.boundaryTagLabelingSettings = boundaryTagLabelingSettings;
	this.reorderLabelingSettings = reorderLabelingSettings;
	this.useNonHieroLabels = useNonHieroLabels;
	this.labelSideSettings = labelSideSettings;
	this.canonicalFormSettings = canonicalFormSettings;
    }

    public static LabelingSettings createLabelingSettings(MTConfigFile configFile,
	    String systemName,
	    SystemSpecificSupportObjectsFactory systemSpecificSupportObjectsFactory) {
	boolean useNonHieroLabels = systemSpecificSupportObjectsFactory.useNonHieroLabels(
		configFile, systemName);

	if (systemName.equals(MTPipelineHatsBoundaryTagLabeled.SystemName)) {
	    BoundaryTagLabelingSettings boundaryTagLabelingSettings = BoundaryTagLabelingSettings
		    .createBoundaryTagLabelingSettingsFromConfig(configFile);
	    return new LabelingSettings(boundaryTagLabelingSettings,
		    MTGrammarExtractionConfig.getReorderinLabelSettings(configFile),
		    useNonHieroLabels,
		    LabelSideSettings.getLabelSideSettingsFromConfig(configFile),
		    CanonicalFormSettings.createCanonicalFormSettings(configFile));
	} else {
	    return new LabelingSettings(
		    BoundaryTagLabelingSettings.createAllEmptyBoundaryTagLabelingSettings(),
		    MTGrammarExtractionConfig.getReorderinLabelSettings(configFile),
		    useNonHieroLabels,
		    LabelSideSettings.getLabelSideSettingsFromConfig(configFile),
		    CanonicalFormSettings.createCanonicalFormSettings(configFile));
	}
    }

    public static LabelingSettings createStandardLabelingSettings(boolean useNonHieroLabels) {
	return new LabelingSettings(
		BoundaryTagLabelingSettings.createAllEmptyBoundaryTagLabelingSettings(),
		ReorderLabelingSettings.createNoReorderingLabelingSettings(), useNonHieroLabels,
		LabelSideSettings.createLabelSideSettingsBothSides(),
		CanonicalFormSettings.createCanonicalFormSettingsNonCanonicalFormUsed());
    }

    public static LabelingSettings createLabelingSettingsReorderingLabeled(
	    CanonicalFormSettings canonicalFormSettings) {
	ReorderLabelingSettings reorderingLabelingSettingsHatLabels = ReorderLabelingSettings
		.createReorderLabelingSettings(
			FinePhraseCentricReorderingLabelBasic.createReorderingLabelCreater(), true,
			true, true);
	return new LabelingSettings(
		BoundaryTagLabelingSettings.createAllEmptyBoundaryTagLabelingSettings(),
		reorderingLabelingSettingsHatLabels, true,
		LabelSideSettings.createLabelSideSettingsBothSides(), canonicalFormSettings);
    }

    public BoundaryTagLabelingSettings getBoundaryTagLabelingSettings() {
	return boundaryTagLabelingSettings;
    }

    public ReorderLabelingSettings getReorderLabelingSettings() {
	return reorderLabelingSettings;
    }

    public boolean useReorderingLabelExtension() {
	return reorderLabelingSettings.useReorderingLabelExtension();
    }

    public boolean useNonHieroLabels() {
	return this.useNonHieroLabels;
    }

    public LabelSideSettings getLabelSideSettings() {
	return labelSideSettings;
    }

    public boolean useCanonicalFormLabeledRules() {
	return canonicalFormSettings.useCanonicalFormLabeledRules();
    }

    public boolean useOnlyHieroWeightsForCanonicalFormLabeledRules() {
	return canonicalFormSettings.useOnlyHieroWeightsForCanonicalFormLabeledRules();
    }

    public boolean restrictLabelVariationForCanonicalFormLabeledRules() {
	return canonicalFormSettings.restrictLabelVariationForCanonicalFormLabeledRules();
    }

    public int numAllowedTypesPerLabelForSource() {
	return canonicalFormSettings.numAllowedTypesPerLabelForSource();
    }

    public boolean producesDoubleReorderingLabels() {
	return this.reorderLabelingSettings.producesDoubleReorderingLabels();
    }

    /**
     * This class stores the properties about whether or not to output canonical
     * forms of labeled rules rather than the labeled rules themselves, and
     * whether or not to output only the Hiero (i.e. fully label smoothed)
     * phrase weights rather than all the phrase weights. The latter option is
     * useful if you want to get a grammar that is completely identical to Hiero
     * except for the rule labels.
     * 
     * @author Gideon Maillette de Buy Wenniger
     * 
     */
    public static class CanonicalFormSettings implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Whether or not to use the canonical form of rules
	private final boolean useCanonicalFormLabeledRules;
	// Whether or not to use only the hiero weights as phrase weights for
	// the generated rules
	private final boolean useOnlyHieroWeightsForCanonicalFormLabeledRules;

	// Whether or not the number of label types per label given an unlabeled
	// source
	// identity shlould be restircted
	private final boolean restrictLabelVariationForCanonicalFormLabeledRules;

	private final int numAllowedTypesPerLabelForSource;

	private CanonicalFormSettings(boolean useCanonicalFormLabeledRules,
		boolean useOnlyHieroWeightsForCanonicalFormLabeledRules,
		boolean restrictLabelVariationForCanonicalFormLabeledRules,
		int numAllowedTypesPerLabelForSource) {
	    this.useCanonicalFormLabeledRules = useCanonicalFormLabeledRules;
	    this.useOnlyHieroWeightsForCanonicalFormLabeledRules = useOnlyHieroWeightsForCanonicalFormLabeledRules;
	    this.restrictLabelVariationForCanonicalFormLabeledRules = restrictLabelVariationForCanonicalFormLabeledRules;
	    this.numAllowedTypesPerLabelForSource = numAllowedTypesPerLabelForSource;
	}

	private static void consistencyCheck(boolean useCanonicalFormLabeledRules,
		boolean useOnlyHieroWeightsForCanonicalFormLabeledRules,
		LABEL_SMOOTHING_TYPE labelSmoothingType) {
	    if (useOnlyHieroWeightsForCanonicalFormLabeledRules) {

		if (!useCanonicalFormLabeledRules) {
		    throw new RuntimeException(
			    "Error: Inconsistent configuration settings - trying to use "
				    + MTGrammarExtractionConfig.USE_ONLY_HIERO_WEIGHTS_FOR_CANONICAL_FORM_LABELED_RULES_PROPERTY
				    + " = true, without using"
				    + MTGrammarExtractionConfig.USE_CANONICAL_FORM_LABELED_RULES_PROPERTY
				    + "= true set in the configuration file");
		}
		if (labelSmoothingType != LABEL_SMOOTHING_TYPE.NO_LABEL_SMOOTHING) {
		    throw new RuntimeException(
			    "Error: Inconsistent configuration settings - trying to use "
				    + MTGrammarExtractionConfig.USE_ONLY_HIERO_WEIGHTS_FOR_CANONICAL_FORM_LABELED_RULES_PROPERTY
				    + " = true, without using"
				    + LabeledRulesSmoother.LABEL_SMOOTHING_TYPE_PROPERTY + "= "
				    + LabeledRulesSmoother.NO_LABEL_SMOOTHING
				    + "  set in the configuration file");
		}
	    }
	}

	public static CanonicalFormSettings createCanonicalFormSettings(
		boolean useCanonicalFormLabeledRules) {
	    return new CanonicalFormSettings(useCanonicalFormLabeledRules, false, false, -1);
	}

	public static CanonicalFormSettings createCanonicalFormSettings(
		boolean useCanonicalFormLabeledRules,
		boolean useOnlyHieroWeightsForCanonicalFormLabeledRules,
		LABEL_SMOOTHING_TYPE labelSmoothingType,
		boolean restrictLabelVariationForCanonicalFormLabeledRules,
		int numAllowedTypesPerLabelForSource) {
	    consistencyCheck(useCanonicalFormLabeledRules,
		    useOnlyHieroWeightsForCanonicalFormLabeledRules, labelSmoothingType);
	    return new CanonicalFormSettings(useCanonicalFormLabeledRules,
		    useOnlyHieroWeightsForCanonicalFormLabeledRules,
		    restrictLabelVariationForCanonicalFormLabeledRules,
		    numAllowedTypesPerLabelForSource);
	}

	public static CanonicalFormSettings createCanonicalFormSettings(MTConfigFile configFile) {
	    MTGrammarExtractionConfig grammarExtractionConfig = MTGrammarExtractionConfig
		    .createMTGrammarExtractionConfig(configFile);
	    boolean useCanonicalFormLabeledRules = grammarExtractionConfig
		    .useCanonicalFormLabeledRules();
	    boolean useOnlyHieroWeightsForCanonicalFormLabeledRules = grammarExtractionConfig
		    .useOnlyHieroWeightsForCanonicalFormLabeledRules();
	    LABEL_SMOOTHING_TYPE labelSmoothingType = LabeledRulesSmoother
		    .getLabelSmoothingType(configFile);

	    boolean restrictLabelVariationForCanonicalFormLabeledRules = grammarExtractionConfig
		    .restrictLabelVariationForCanonicalFormLabeledRules();
	    int numAllowedTypesPerLabelForSource = grammarExtractionConfig
		    .numAllowedTypesPerLabelForSource();

	    return createCanonicalFormSettings(useCanonicalFormLabeledRules,
		    useOnlyHieroWeightsForCanonicalFormLabeledRules, labelSmoothingType,
		    restrictLabelVariationForCanonicalFormLabeledRules,
		    numAllowedTypesPerLabelForSource);
	}

	public static CanonicalFormSettings createCanonicalFormSettingsNonCanonicalFormUsed() {
	    return new CanonicalFormSettings(false, false, false, -1);
	}
	
	public static CanonicalFormSettings createCanonicalFormSettingsCannonicalFormLabelingDefault() {
	    return new CanonicalFormSettings(true, true, false, -1);
	}

	public boolean useCanonicalFormLabeledRules() {
	    return useCanonicalFormLabeledRules;
	}

	public boolean useOnlyHieroWeightsForCanonicalFormLabeledRules() {
	    return useOnlyHieroWeightsForCanonicalFormLabeledRules;
	}

	public boolean restrictLabelVariationForCanonicalFormLabeledRules() {
	    return restrictLabelVariationForCanonicalFormLabeledRules;
	}

	public int numAllowedTypesPerLabelForSource() {
	    return numAllowedTypesPerLabelForSource;
	}
    }

    public CanonicalFormSettings getCanonicalFormSettings() {
	return this.canonicalFormSettings;
    }

}
