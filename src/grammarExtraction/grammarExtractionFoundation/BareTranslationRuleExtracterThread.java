package grammarExtraction.grammarExtractionFoundation;

import java.io.IOException;
import java.util.List;

import util.Pair;
import alignment.SourceString;
import alignmentStatistics.EbitgCorpusComputationMultiThread;
import alignmentStatistics.InputStringsEnumeration;
import grammarExtraction.translationRuleTrie.TranslationRuleTrie;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

public abstract class BareTranslationRuleExtracterThread<T extends SourceString> extends EbitgCorpusComputationMultiThread<BareTranslationRuleExtracterThread<T>, T> {
	protected final WordKeyMappingTable wordKeyMappingTable;
	private final TranslationRuleTrie translationRuleTrie;
	protected double totalCountCreated = 0;
	protected double totalSentencesParsed = 0;
	protected final boolean useCaching;

	public BareTranslationRuleExtracterThread() {
		this.wordKeyMappingTable = null;
		this.translationRuleTrie = null;
		this.useCaching = false;

	};

	public BareTranslationRuleExtracterThread(int maxAllowedInferencesPerNode, Pair<Integer> minMaxSentencePairNum, InputStringsEnumeration<T> inputStringEnumeration, int maxSentenceLength,
			int outputLevel, TranslationRuleTrie translationRuleTrie, WordKeyMappingTable wordKeyMappingTable, boolean useJoshuaStyleSimplifiedCountComputing, boolean useCaching) {
		super(maxAllowedInferencesPerNode, minMaxSentencePairNum, inputStringEnumeration, maxSentenceLength, outputLevel);
		this.translationRuleTrie = translationRuleTrie;
		this.wordKeyMappingTable = wordKeyMappingTable;
		this.useCaching = useCaching;
	}

	@Override
	protected final void doBeforeLooping() throws IOException {
	}

	protected static final  int countTotalRules(List<LightWeightPhrasePairRuleSet> ruleSets) {
		int result = 0;
		for (LightWeightPhrasePairRuleSet ruleSet : ruleSets) {
			result += ruleSet.getNumRules();
		}
		return result;
	}

	protected abstract List<LightWeightPhrasePairRuleSet> extractRuleSets(T input);

	

	
	protected final void performCorpusComputation(T input, int sentencePairNum) {
		// long startTime = System.currentTimeMillis();

		// System.out.println("TranslationRuleExtractionThread: extractRuleSets");
		List<LightWeightPhrasePairRuleSet> ruleSets = extractRuleSets(input);
		// System.out.println("TranslationRuleExtractionThread: success");

		// long passedTime = System.currentTimeMillis() - startTime;
		// System.out.println("::::(A) TranslationRuleExtractorThread: extracted "
		// +countTotalRules(ruleSets) + " rules , rule extraction took " +
		// ((double)passedTime/1000) + " seconds");

		// startTime = System.currentTimeMillis();
		processPhrasePairRuleSets(ruleSets);

		// passedTime = System.currentTimeMillis() - startTime;
		// System.out.println("::::(B) TranslationRuleExtractorThread: Adding rules to trie took "
		// + ((double)passedTime/1000) + " seconds");

		this.setTotalSentencesParsed(this.getTotalSentencesParsed() + 1);
	}

	@Override
	protected void doAfterLooping() throws IOException {
	}

	@Override
	public BareTranslationRuleExtracterThread<T> call() {
		performLoopCorpusComputation();
		return this;
	}

	public final double getTotalCountCreated() {
		return this.totalCountCreated;
	}

	public final void setTotalSentencesParsed(double totalSentencesParsed) {
		this.totalSentencesParsed = totalSentencesParsed;
	}

	public final double getTotalSentencesParsed() {
		return totalSentencesParsed;
	}

	protected final void processPhrasePairRuleSets(List<LightWeightPhrasePairRuleSet> ruleSets) {
		// System.out.println("TranslationRuleExtractorThread.addPhrasePairRuleSetsToTable ...");
		// long timeStart = System.currentTimeMillis();
		// int numRules = 0;

		for (LightWeightPhrasePairRuleSet ruleSet : ruleSets) {
			processPhrasePairRuleSet(ruleSet);
			// numRules += ruleSet.getNumRules();
		}
		// long timeSpend = System.currentTimeMillis() - timeStart;
		// double millisPerRule = ((double)timeSpend) / numRules;
		// System.out.println("TranslationRuleExtractorThread >> Time spend per rule: "
		// + millisPerRule);

		this.totalCountCreated += ruleSets.size();
	}

	protected abstract void processPhrasePairRuleSet(LightWeightPhrasePairRuleSet ruleSet);

	public WordKeyMappingTable getWordKeyMappingTable() {
		return wordKeyMappingTable;
	}

	public TranslationRuleTrie getTranslationRuleTrie() {
		return translationRuleTrie;
	}

}
