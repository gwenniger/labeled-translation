package grammarExtraction.grammarExtractionFoundation;

import grammarExtraction.translationRuleTrie.LexicalProbabilityTables;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

import java.io.IOException;
import util.Pair;
import alignment.AlignmentStringTriple;
import alignment.AlignmentTriple;
import alignmentStatistics.EbitgCorpusComputationMultiThread;
import alignmentStatistics.InputStringsEnumeration;

public class LexicalProbabilitiesTableFillThread extends EbitgCorpusComputationMultiThread<LexicalProbabilitiesTableFillThread,AlignmentStringTriple>
{
	final  WordKeyMappingTable wordKeyMappingTable;
	final  LexicalProbabilityTables lexicalProbabilityTables;
	
	protected double totalCountCreated = 0;
	protected double totalSentencesParsed = 0;
	
	public  LexicalProbabilitiesTableFillThread(int maxAllowedInferencesPerNode,Pair<Integer>
			 minMaxSentencePairNum, 
			InputStringsEnumeration<AlignmentStringTriple> inputStringEnumeration,int maxSentenceLength,
			int outputLevel, WordKeyMappingTable wordKeyMappingTable,LexicalProbabilityTables lexicalProbabilityTables)
	{
		super(maxAllowedInferencesPerNode, minMaxSentencePairNum,inputStringEnumeration ,maxSentenceLength, outputLevel);
		
		this.wordKeyMappingTable = wordKeyMappingTable;
		this.lexicalProbabilityTables = lexicalProbabilityTables;
	}

	@Override
	protected void doBeforeLooping() throws IOException 
	{
	}

	@Override
	protected void performCorpusComputation(AlignmentStringTriple input, int sentencePairNum) 
	{
		//System.out.println("sourceString: " + sourceSentence + "targetString: " + targetSentence +" alignmentString: " + alignment);
		AlignmentTriple alignmentTriple = AlignmentTriple.createNullExtendedAlignmentTriple(input.getSourceString(),input.getTargetString(),input.getAlignmentString());
		this.lexicalProbabilityTables.getLexicalProbabilityTableSourceGivenTarget().addCountsForAlignmentTriple(alignmentTriple);
		
		AlignmentTriple invertedAlignmentTriple = AlignmentTriple.createNullExtendedAlignmentTriple(input.getTargetString(), input.getSourceString(), AlignmentTriple.computeInvertedAlignmentString(input.getAlignmentString()));
		this.lexicalProbabilityTables.getLexicalProbabilityTableTargetGivenSource().addCountsForAlignmentTriple(invertedAlignmentTriple);
		//System.out.println("TranslationRuleExtractionThread: Rule Table size: " + translationRuleTrie.getNumRuleTypes() + " rules");
		this.setTotalSentencesParsed(this.getTotalSentencesParsed() + 1);
	}


	
	
	@Override
	protected void doAfterLooping() throws IOException 
	{
	}
	
	@Override
	public LexicalProbabilitiesTableFillThread call() 
	{
		performLoopCorpusComputation();
		return this;
	}

	public double getTotalCountCreated()
	{
		return this.totalCountCreated;
	}

	public void setTotalSentencesParsed(double totalSentencesParsed) {
		this.totalSentencesParsed = totalSentencesParsed;
	}

	public double getTotalSentencesParsed() {
		return totalSentencesParsed;
	}
	
	
	
	
}
