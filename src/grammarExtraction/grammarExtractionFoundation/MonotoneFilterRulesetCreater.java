package grammarExtraction.grammarExtractionFoundation;

import grammarExtraction.IntegerKeyRuleRepresentation;
import util.Span;
import grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagFilterPair;
import grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagLabelingSettings;
import grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagSourceLabelingCreater;
import grammarExtraction.chiangGrammarExtraction.GapRangeComputing;
import grammarExtraction.chiangGrammarExtraction.GrammarExtractionConstraints;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.LightWeightTranslationRule;
import grammarExtraction.translationRules.RuleGap;
import grammarExtraction.translationRules.LightWeightRuleLabel;
import grammarExtraction.translationRules.RuleLabel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import junit.framework.Assert;
import util.ConfigFile;
import util.Pair;

public class MonotoneFilterRulesetCreater {
	private final List<String> sourceWords;
	private final WordKeyMappingTable wordKeyMappingTable;
	private final GapRangeComputing gapRangeComputing;
	private final List<SourceLabelingCreater> sourceLabelingCraters;
	private final static String WHITESPACE_STRING = "\\s";

	private MonotoneFilterRulesetCreater(List<String> sourceWords, WordKeyMappingTable wordKeyMappingTable, GapRangeComputing gapRangeComputing, List<SourceLabelingCreater> sourceLabelingCraters) {
		this.sourceWords = sourceWords;
		this.wordKeyMappingTable = wordKeyMappingTable;
		this.gapRangeComputing = gapRangeComputing;
		this.sourceLabelingCraters = sourceLabelingCraters;
	}

	public static MonotoneFilterRulesetCreater createMonotoneFilterRulesetCreater(List<String> sourceWords, WordKeyMappingTable wordKeyMappingTable,
			GrammarExtractionConstraints chiangGrammarExtractionConstraints) {
		List<SourceLabelingCreater> sourceLabelingCraters = new ArrayList<SourceLabelingCreater>();
		sourceLabelingCraters.add(new HieroSourceLabelingCreater());
		return new MonotoneFilterRulesetCreater(sourceWords, wordKeyMappingTable, GapRangeComputing.createGapRangeComputing(chiangGrammarExtractionConstraints), sourceLabelingCraters);
	}

	public static MonotoneFilterRulesetCreater createMonotoneFilterRulesetCreaterSourceBoundaryTagLabeled(BoundaryTagFilterPair input, WordKeyMappingTable wordKeyMappingTable,
			GrammarExtractionConstraints chiangGrammarExtractionConstraints, ConfigFile theConfig) {
		List<String> sourceWords = Arrays.asList(input.getSourceString().split(WHITESPACE_STRING));
		List<String> sourceTags = Arrays.asList(input.getSourceTagsString().split(WHITESPACE_STRING));
		List<SourceLabelingCreater> sourceLabelingCraters = new ArrayList<SourceLabelingCreater>();
		sourceLabelingCraters.add(new HieroSourceLabelingCreater());
		sourceLabelingCraters.add(BoundaryTagSourceLabelingCreater.createBoundaryTagSourceLabelingCreater(sourceTags, theConfig,
				BoundaryTagLabelingSettings.createBoundaryTagLabelingSettingsFromConfig(theConfig)));
		return new MonotoneFilterRulesetCreater(sourceWords, wordKeyMappingTable, GapRangeComputing.createGapRangeComputing(chiangGrammarExtractionConstraints), sourceLabelingCraters);
	}

	private int sentenceLength() {
		return sourceWords.size();
	}

	private List<LightWeightPhrasePairRuleSet> produceSoureSideConsistentRuleSets(int maxSpanLength, RuleSetCreater ruleSetCreater) {
		List<LightWeightPhrasePairRuleSet> result = new ArrayList<LightWeightPhrasePairRuleSet>();

		for (int spanLength = 1; spanLength <= maxSpanLength; spanLength++) {
			for (int i = 0; i < (sentenceLength() - spanLength + 1); i++) {
				Span sourceSpan = new Span(i, i + spanLength - 1);
				result.addAll(createRuleSetsForSpan(sourceSpan, ruleSetCreater));

			}
		}
		return result;
	}

	private List<LightWeightPhrasePairRuleSet> produceSoureSideConsistentNormalRuleSets() {
		int maxSpanLength = Math.min(gapRangeComputing.getMaxSourceAndTargetLength(), sentenceLength());
		return produceSoureSideConsistentRuleSets(maxSpanLength, new NormalRuleSetCreater());
	}

	private List<LightWeightPhrasePairRuleSet> produceSoureSideConsistentAbstractRuleSets() {
		int maxSpanLength = sentenceLength();
		return produceSoureSideConsistentRuleSets(maxSpanLength, new AbstractRuleSetCreater());
	}

	public List<LightWeightPhrasePairRuleSet> produceSoureSideConsistentRuleSets() {
		List<LightWeightPhrasePairRuleSet> result = new ArrayList<LightWeightPhrasePairRuleSet>();
		result.addAll(produceSoureSideConsistentNormalRuleSets());
		result.addAll(produceSoureSideConsistentAbstractRuleSets());
		return result;
	}

	private List<LightWeightPhrasePairRuleSet> createRuleSetsForSpan(Span sourceSpan, RuleSetCreater ruleSetCreater) {
		List<LightWeightPhrasePairRuleSet> result = new ArrayList<LightWeightPhrasePairRuleSet>();

		for (SourceLabelingCreater sourceLabelingCreater : this.sourceLabelingCraters) {
			result.add(ruleSetCreater.createRuleSetForSpan(this, sourceSpan, sourceLabelingCreater));
		}
		return result;
	}

	protected LightWeightPhrasePairRuleSet createRuleSetForSpan(Span sourceSpan, SourceLabelingCreater sourceLabelingCreater) {
		LightWeightPhrasePairRuleSet result = null;

		LightWeightTranslationRule baseTranslationRule = null;
		if (sourceSpan.getSpanLength() <= gapRangeComputing.getMaxTerminalsPlusNonTerminalsSourceSide()) {
			baseTranslationRule = createFilterRule(sourceWords, sourceSpan, new ArrayList<Span>(), sourceLabelingCreater);
			// System.out.println("Base translation rule: " +
			// baseTranslationRule);
			assert (baseTranslationRule.getSourceSideRepresentation().getPartKeys().size() <= (gapRangeComputing.getMaxTerminalsPlusNonTerminalsSourceSide() + 1));
			assert (baseTranslationRule.getTargetSideRepresentation().getPartKeys().size() < 10);
		}
		// Assert.assertNotNull(baseTranslationRule);
		Assert.assertNotNull(sourceLabelingCreater);

		List<LightWeightTranslationRule> oneGapRules = createOneGapRulesForSpan(sourceSpan, sourceLabelingCreater);
		List<LightWeightTranslationRule> twoGapRules = createTwoGapRulesForSpan(sourceSpan, sourceLabelingCreater);

		result = LightWeightPhrasePairRuleSet.createLightWeightPhrasePairRuleSet(baseTranslationRule, oneGapRules, twoGapRules,
				LightWeightPhrasePairRuleSet.getLeftHandSideRulesLabel(baseTranslationRule, oneGapRules, twoGapRules), 0);

		return result;
	}

	protected LightWeightPhrasePairRuleSet createFullyAbstractRuleSetForSpan(Span sourceSpan, SourceLabelingCreater sourceLabelingCreater) {
		LightWeightPhrasePairRuleSet result = null;
		LightWeightTranslationRule baseTranslationRule = null;
		baseTranslationRule = createFilterRule(sourceWords, sourceSpan, new ArrayList<Span>(), sourceLabelingCreater);
		Assert.assertNotNull(sourceLabelingCreater);

		List<LightWeightTranslationRule> oneGapRules = Collections.emptyList();
		List<LightWeightTranslationRule> twoGapRules = createFullyAbstractRules(sourceSpan, sourceLabelingCreater);

		result = LightWeightPhrasePairRuleSet.createLightWeightPhrasePairRuleSet(null, oneGapRules, twoGapRules,
				LightWeightPhrasePairRuleSet.getLeftHandSideRulesLabel(baseTranslationRule, oneGapRules, twoGapRules), 0);

		return result;
	}

	private List<LightWeightTranslationRule> createOneGapRulesForSpan(Span sourceSpan, SourceLabelingCreater sourceLabelingCreater) {
		List<LightWeightTranslationRule> result = new ArrayList<LightWeightTranslationRule>();

		List<Span> prospectiveOneGapRanges = gapRangeComputing.findPermissableOneGapSourceRanges(sourceSpan);

		for (Span gapSpan : prospectiveOneGapRanges) {
			result.add(createFilterRule(sourceWords, sourceSpan, Arrays.asList(gapSpan), sourceLabelingCreater));
		}
		return result;
	}

	private List<LightWeightTranslationRule> createTwoGapRulesForSpan(Span sourceSpan, SourceLabelingCreater sourceLabelingCreater) {
		List<LightWeightTranslationRule> result = new ArrayList<LightWeightTranslationRule>();

		List<Pair<Span>> prospectiveTwoGapRanges = gapRangeComputing.findPermissibleTwoGapSourceRanges(sourceSpan);

		for (Pair<Span> gapSpans : prospectiveTwoGapRanges) {
			result.add(createFilterRule(sourceWords, sourceSpan, Arrays.asList(gapSpans.getFirst(), gapSpans.getSecond()), sourceLabelingCreater));
		}
		return result;
	}

	private List<LightWeightTranslationRule> createFullyAbstractRules(Span sourceSpan, SourceLabelingCreater sourceLabelingCreater) {
		List<LightWeightTranslationRule> result = new ArrayList<LightWeightTranslationRule>();
		List<Pair<Span>> prospectiveTwoGapRanges = gapRangeComputing.findFullyAbstractRulesPermissibleSplitRanges(sourceSpan);
		for (Pair<Span> gapSpans : prospectiveTwoGapRanges) {
			result.add(createFilterRule(sourceWords, sourceSpan, Arrays.asList(gapSpans.getFirst(), gapSpans.getSecond()), sourceLabelingCreater));
		}
		return result;
	}

	private List<String> createSourceSideRuleComponentsList(List<String> sourceWords, Span ruleSpan, List<Span> gapSpans, SourceLabelingCreater sourceLabelingCreater) {
		List<String> result = new ArrayList<String>();

		int i = ruleSpan.getFirst();
		int gapNum = 0;
		for (Span gapSpan : gapSpans) {
			while (i < gapSpan.getFirst()) {
				result.add(this.sourceWords.get(i));
				;
				i++;
			}
			i = gapSpan.getSecond() + 1;
			String sourceGapLabeling = sourceLabelingCreater.createSourceGapLabelingForSourceSpan(gapSpan);
			result.add(RuleGap.ruleGapString(sourceGapLabeling, gapNum));
			gapNum++;
		}

		while (i <= ruleSpan.getSecond()) {
			result.add(this.sourceWords.get(i));
			i++;
		}

		return result;
	}

	private LightWeightTranslationRule createFilterRule(List<String> sourceWords, Span ruleSpan, List<Span> gapSpans, SourceLabelingCreater sourceLabelingCreater) {
		List<String> sourceSideRuleComponentsList = createSourceSideRuleComponentsList(sourceWords, ruleSpan, gapSpans, sourceLabelingCreater);

		IntegerKeyRuleRepresentation sourceSideRepresentation = IntegerKeyRuleRepresentation.createIntegerKeyRuleRepresentation(sourceSideRuleComponentsList, wordKeyMappingTable);
		LightWeightTranslationRule result = LightWeightTranslationRule.createLightWeightTranslationRuleWithoutLexicalWeights(sourceSideRepresentation, sourceSideRepresentation,
				LightWeightRuleLabel.createLightWeightRuleLabel(RuleLabel.createBothSidesSameRuleLabel(sourceLabelingCreater.createSourceRuleLabelingForSourceSpan(ruleSpan)), wordKeyMappingTable));

		return result;

	}

	private static interface RuleSetCreater {
		LightWeightPhrasePairRuleSet createRuleSetForSpan(MonotoneFilterRulesetCreater ruleSetCreater, Span sourceSpan, SourceLabelingCreater sourceLabelingCreater);
	}

	private static class NormalRuleSetCreater implements RuleSetCreater {

		@Override
		public LightWeightPhrasePairRuleSet createRuleSetForSpan(MonotoneFilterRulesetCreater ruleSetCreater, Span sourceSpan, SourceLabelingCreater sourceLabelingCreater) {
			return ruleSetCreater.createRuleSetForSpan(sourceSpan, sourceLabelingCreater);
		}
	}

	private static class AbstractRuleSetCreater implements RuleSetCreater {

		@Override
		public LightWeightPhrasePairRuleSet createRuleSetForSpan(MonotoneFilterRulesetCreater ruleSetCreater, Span sourceSpan, SourceLabelingCreater sourceLabelingCreater) {
			return ruleSetCreater.createFullyAbstractRuleSetForSpan(sourceSpan, sourceLabelingCreater);
		}
	}

}
