package grammarExtraction.grammarExtractionFoundation;

import extended_bitg.EbitgLexicalizedInference;
import grammarExtraction.translationRuleTrie.LexicalProbabilityTables;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

import java.util.ArrayList;
import java.util.List;

public class LightWeightRuleExtractor<T extends EbitgLexicalizedInference> 
{
	protected List<LightWeightPhrasePairRuleSet> createLightWeightTranslationRuleSetsWithLexicalProabilities(List<HeavyWeightPhrasePairRuleSet<T>> heavyWeightSets, WordKeyMappingTable wordKeyMappingTable,LexicalProbabilityTables lexicalProbabilityTables)
	{
		List<LightWeightPhrasePairRuleSet> result = new ArrayList<LightWeightPhrasePairRuleSet>();
		for(HeavyWeightPhrasePairRuleSet<T> heavyWeightRuleSet : heavyWeightSets)
		{
			result.add(LightWeightPhrasePairRuleSet.createLightWeightPhrasePairRuleSet(heavyWeightRuleSet, wordKeyMappingTable,lexicalProbabilityTables));
		}
		return result;
	}
}
