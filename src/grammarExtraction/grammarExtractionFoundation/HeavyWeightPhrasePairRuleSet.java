package grammarExtraction.grammarExtractionFoundation;

import extended_bitg.EbitgLexicalizedInference;
import util.Span;
import grammarExtraction.chiangGrammarExtraction.GapRangeComputing;
import grammarExtraction.chiangGrammarExtraction.HeavyWeightChiangRuleExtractor;
import grammarExtraction.chiangGrammarExtraction.ReorderLabelingSettings;
import grammarExtraction.minimalPhrasePairExtraction.HeavyWeightMinimalPhrasePairRuleExtractor;
import grammarExtraction.translationRules.RuleLabel;
import grammarExtraction.translationRules.TranslationRule;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import junit.framework.Assert;

public class HeavyWeightPhrasePairRuleSet<T extends EbitgLexicalizedInference> extends PhrasePairRuleSet<TranslationRule<T>> {
	private HeavyWeightPhrasePairRuleSet(TranslationRule<T> baseTranslationRule, List<TranslationRule<T>> oneGapTranslationRules, List<TranslationRule<T>> twoGapTranslationRules) {
		super(baseTranslationRule, oneGapTranslationRules, twoGapTranslationRules);
	}

	public static <T extends EbitgLexicalizedInference> HeavyWeightPhrasePairRuleSet<T> createPhrasePairRuleSet(TranslationRule<T> baseTranslationRule,
			List<TranslationRule<T>> oneGapTranslationRules, List<TranslationRule<T>> twoGapTranslationRules) {
		return new HeavyWeightPhrasePairRuleSet<T>(baseTranslationRule, oneGapTranslationRules, twoGapTranslationRules);
	}

	private static boolean useReorderingLabelsForEverythingButPurePhrasePairs(HeavyWeightChiangRuleExtractor<?, ?> ruleExtractor) {
		ReorderLabelingSettings reorderLabelingSettings = ruleExtractor.getReorderLabelingSettings();
		if (reorderLabelingSettings.useReorderingLabelExtension() && (reorderLabelingSettings.reorderLabelHieroRules()) && (!reorderLabelingSettings.reorderLabelPhrasePairs())) {
			return true;
		}
		return false;
	}

	public static <T extends EbitgLexicalizedInference> HeavyWeightPhrasePairRuleSet<T> createPhrasePairRuleSet(Span absoluteSourceSpan, HeavyWeightChiangRuleExtractor<T, ?> ruleExtractor,
			GapRangeComputing gapRangeComputing) {
		TranslationRule<T> baseTranslationRule = ruleExtractor.createChiangConstraintsSatisfyingBaseTranslationRule(new Span(absoluteSourceSpan));
		List<TranslationRule<T>> oneGapTranslationRules = Collections.emptyList();
		List<TranslationRule<T>> twoGapsTranslationRules = Collections.emptyList();

		if (baseTranslationRule != null) {
			// System.out.println(">>> PhrasePairRuleSet: baseTranlsationRule: "+ baseTranslationRule);
			oneGapTranslationRules = ruleExtractor.createOneGapRules(baseTranslationRule);
			twoGapsTranslationRules = ruleExtractor.createTwoGapNonAbstractRules(baseTranslationRule);
			//twoGapsTranslationRules = ruleExtractor.createTwoGapRulesAndAbstractRules(baseTranslationRule);

			if (useReorderingLabelsForEverythingButPurePhrasePairs(ruleExtractor)) {
				baseTranslationRule = ruleExtractor.createChiangConstraintsSatisfyingBaseTranslationRuleWithPhrasePairLabeling(new Span(absoluteSourceSpan));
			}

			Assert.assertNotNull(baseTranslationRule.getRuleLabel());
			// If the base translation rule itself breaks the constraint on the
			// number of elements, we remove it
			if (ruleBreaksMaxNumElementsConstraint(baseTranslationRule, gapRangeComputing)) {
				baseTranslationRule = null;
			}

		}

		return new HeavyWeightPhrasePairRuleSet<T>(baseTranslationRule, oneGapTranslationRules, twoGapsTranslationRules);
	}

	public static <T extends EbitgLexicalizedInference> HeavyWeightPhrasePairRuleSet<T> createAbstractPhrasePairRuleSet(Span absoluteSourceSpan, HeavyWeightChiangRuleExtractor<T, ?> ruleExtractor,
			GapRangeComputing gapRangeComputing) {
		
		TranslationRule<T> baseTranslationRule = ruleExtractor.createAbstractBaseTranslationRuleNotLengthConstrained(new Span(absoluteSourceSpan));
		List<TranslationRule<T>> oneGapTranslationRules = Collections.emptyList();
		List<TranslationRule<T>> twoGapsTranslationRules = Collections.emptyList();

		if (baseTranslationRule != null) {
			oneGapTranslationRules = Collections.emptyList();
			twoGapsTranslationRules = ruleExtractor.createOnlyFullyAbstractRules(baseTranslationRule);
		}
		return new HeavyWeightPhrasePairRuleSet<T>(null, oneGapTranslationRules, twoGapsTranslationRules);
	}

	public static <T extends EbitgLexicalizedInference> HeavyWeightPhrasePairRuleSet<T> createMinimalPhrasePairRuleSet(Span absoluteSourceSpan,
			HeavyWeightMinimalPhrasePairRuleExtractor<T, ?> ruleExtractor) {
		TranslationRule<T> baseTranslationRule = ruleExtractor.createBaseTranslationRule(new Span(absoluteSourceSpan));
		List<TranslationRule<T>> oneGapTranslationRules = new ArrayList<TranslationRule<T>>();
		List<TranslationRule<T>> twoGapsTranslationRules = new ArrayList<TranslationRule<T>>();
		return new HeavyWeightPhrasePairRuleSet<T>(baseTranslationRule, oneGapTranslationRules, twoGapsTranslationRules);
	}

	private static boolean ruleBreaksMaxNumElementsConstraint(TranslationRule<?> translationRule, GapRangeComputing gapRangeComputing) {
		return (translationRule.getSourceSideNumElements() > gapRangeComputing.getMaxTerminalsPlusNonTerminalsSourceSide());
	}

	protected RuleLabel getLeftHandSideLabel() {
		if (hasRules()) {
			TranslationRule<T> firstRule = this.getRules().get(0);
			return firstRule.getRuleLabel();
		} else {
			return null;
		}
	}

}