package grammarExtraction.grammarExtractionFoundation;

import grammarExtraction.chiangGrammarExtraction.GrammarExtractionConstraints;
import grammarExtraction.samtGrammarExtraction.UnknownWordRulesCreator;
import grammarExtraction.translationRuleTrie.GrammarWriter;
import grammarExtraction.translationRuleTrie.LabelProbabilityEstimator;
import grammarExtraction.translationRuleTrie.LexicalProbabilityTables;
import grammarExtraction.translationRuleTrie.TranslationRuleCountTable;
import grammarExtraction.translationRuleTrie.TranslationRuleProbabilityTable;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import util.Pair;
import alignmentStatistics.AlignmentTriplesEnumeration;
import alignmentStatistics.EbitgCorpusComputationMultiThread;
import alignmentStatistics.MultiThreadComputationParameters;
import alignmentStatistics.MultiThreadCorpusDataGenerator;
import alignmentStatistics.TimeObject;

public abstract class MultiThreadGrammarExtractor extends MultiThreadCorpusDataGenerator {

	public static final String UseCachingProperty = "useCaching";
	public static final boolean UseJoshuaStyleSimplifiedCountComputing = false; // TODO:
																				// remove
																				// this
																				// option
	private final TranslationRuleCountTable translationRuleCountTable;
	private final GrammarExtractionConstraints grammarExtractionConstraints;
	protected final UnknownWordRulesCreator unknownWordsRulesCreator;

	// Flags indicating what (extra) output should be shown and written to file
	private final boolean writeProbabilityTablesToFiles;
	private final boolean printProbabilityTables;
	private final String SMOOTHED_GRAMMAR_SUFFIX = ".SMOOTHED_GRAMMAR";
	private final String MAIN_GRAMMAR_SUFFIX = ".MAIN_GRAMMAR";
	private final SystemIdentity systemIdentity;

	protected MultiThreadGrammarExtractor(MultiThreadComputationParameters parameters, TranslationRuleCountTable translationRuleProbabilityTable,
			GrammarExtractionConstraints grammarExtractionConstraints, UnknownWordRulesCreator unknownWordsRulesCreator, boolean writeProbabilityTablesToFiles, boolean printProbabilityTables,
			SystemIdentity systemIdentity) {
		super(parameters);
		this.translationRuleCountTable = translationRuleProbabilityTable;
		this.grammarExtractionConstraints = grammarExtractionConstraints;
		this.unknownWordsRulesCreator = unknownWordsRulesCreator;
		this.writeProbabilityTablesToFiles = writeProbabilityTablesToFiles;
		this.printProbabilityTables = printProbabilityTables;
		this.systemIdentity = systemIdentity;
	}

	public void joinThread(Thread thread) {
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public double collectTotalCountCreatedByThreads(List<BareTranslationRuleExtracterThread<?>> results) {
		double result = 0;

		for (BareTranslationRuleExtracterThread<?> thread : results) {
			result += thread.getTotalCountCreated();
		}

		return result;
	}

	public double collectTotalSentencesParsed(List<BareTranslationRuleExtracterThread<?>> results) {
		double result = 0;

		for (BareTranslationRuleExtracterThread<?> thread : results) {
			result += thread.getTotalSentencesParsed();
		}

		return result;
	}

	private void computeLexicalProbabilitiesTable() {
		List<Pair<Integer>> minMaxSentencePairNumList = this.createMinMaxSentencePairNumsList();
		List<LexicalProbabilitiesTableFillThread> threads = new ArrayList<LexicalProbabilitiesTableFillThread>();

		for (Pair<Integer> minMaxSentencePairNum : minMaxSentencePairNumList) {
			AlignmentTriplesEnumeration alignmentTriplesEnumeration = AlignmentTriplesEnumeration.createAlignmentTriplesEnumeration(parameters.getSourceFileName(), parameters.getTargetFileName(),
					parameters.getAlignmentsFileName());
			LexicalProbabilitiesTableFillThread tret = new LexicalProbabilitiesTableFillThread(this.parameters.getMaxAllowedInferencesPerNode(), minMaxSentencePairNum, alignmentTriplesEnumeration,
					parameters.getMaxSentenceLength(), parameters.getOutputLevel(), getTranslationRuleCountTable().getWordKeyMappingTable(), getLexicalProbabilityTables());
			threads.add(tret);
		}
		performThreadComputation(threads);
		getLexicalProbabilityTables().computeProbabilitiesFromCounts();

		// getLexicalProbabilityTables().getLexicalProbabilityTableSourceGivenTarget().printTable();
		// getLexicalProbabilityTables().getLexicalProbabilityTableTargetGivenSource().printTable();
		System.out.println("\n\n>>> Compute Lexical probabilities table finished \n\n");
	}

	private LexicalProbabilityTables getLexicalProbabilityTables() {
		return this.getTranslationRuleCountTable().getLexicalProbabilityTables();
	}

	protected double getGrammarRuleExtractionResultsRapport(List<BareTranslationRuleExtracterThread<?>> results) {
		System.out.println("Total sentences parsed: " + this.collectTotalSentencesParsed(results));
		results.get(0).printMinMaxSentencePairNums();
		double totalCountCreatedByThreads = this.collectTotalCountCreatedByThreads(results);
		return totalCountCreatedByThreads;
	}

	public static <T extends Callable<T>> List<T> performThreadComputation(List<T> threads, int noThreads) {
		List<Future<T>> registeredTasks = new ArrayList<Future<T>>();
		ExecutorService executorService = Executors.newFixedThreadPool(noThreads);

		for (T tret : threads) {
			Future<T> executingTask = executorService.submit(tret);
			registeredTasks.add(executingTask);
		}
		// Wait until all extractor threads to finish
		List<T> results = MultiThreadCorpusDataGenerator.joinThreads(registeredTasks);
		System.out.println("\n\n Threads finished \n\n");

		return results;
	}

	protected <T extends Callable<T>> List<T> performThreadComputation(List<T> threads) {
		return performThreadComputation(threads, this.parameters.getNumThreads());
	}

	protected abstract void performTrieFilling();

	public TranslationRuleProbabilityTable createTranslationRuleProbabilityTable() {
		long startTime = System.currentTimeMillis();

		System.out.println("Started MultiThreadGrammarExtractorTestFiltered.extractGrammarMultiThread...");
		computeLexicalProbabilitiesTable();
		performTrieFilling();

		long millisSpend = System.currentTimeMillis() - startTime;
		System.out.println("Time spend for rule extraction : " + TimeObject.createTimeString(millisSpend));

		startTime = System.currentTimeMillis();

		TranslationRuleProbabilityTable translationRuleProbabilityTable = TranslationRuleProbabilityTable.createTranslationRuleProbabilityTable(getTranslationRuleCountTable(),
				this.grammarExtractionConstraints, systemIdentity);
		millisSpend = System.currentTimeMillis() - startTime;
		System.out.println("Time spend for probabilities computation in two direction : " + TimeObject.createTimeString(millisSpend));
		return translationRuleProbabilityTable;
	}

	protected TranslationRuleProbabilityTable createTranslationRuleProbabilityTable(String resultGrammarFilePath) {
		TranslationRuleProbabilityTable translationRuleProbabilityTable = createTranslationRuleProbabilityTable();
		return translationRuleProbabilityTable;
	}

	public TranslationRuleProbabilityTable extractGrammarMultiThreadAndWriteResultToFile(String resultGrammarFilePath,
		String gapLabelConditionalProbabilityTableFilePath, boolean writePlainHieroRules ) {
		TranslationRuleProbabilityTable translationRuleProbabilityTable = createTranslationRuleProbabilityTable();
		printAndWriteResults(translationRuleProbabilityTable, resultGrammarFilePath,
			writePlainHieroRules);
		writeGapLabelConditionalProbabilityTableToFile(translationRuleProbabilityTable, gapLabelConditionalProbabilityTableFilePath);
		return translationRuleProbabilityTable;
	}
	
	public TranslationRuleProbabilityTable extractGrammarMultiThreadAndWriteResultToFile(String resultGrammarFilePath,
		 boolean writePlainHieroRules ) {
		TranslationRuleProbabilityTable translationRuleProbabilityTable = createTranslationRuleProbabilityTable();
		printAndWriteResults(translationRuleProbabilityTable, resultGrammarFilePath,
			writePlainHieroRules);
		return translationRuleProbabilityTable;
	}

	protected void printProbabilityTables(TranslationRuleProbabilityTable translationRuleProbabilityTable) {
		translationRuleProbabilityTable.printTargetGivenSourceProbabilityTable();
		translationRuleProbabilityTable.printSourceGivenTargetProbabilityTable();
	}

	protected void writeProbabilityTables(TranslationRuleProbabilityTable translationRuleProbabilityTable) {
		long measurePointTime = System.currentTimeMillis();
		translationRuleProbabilityTable.writeTargetGivenSourceProbabilityTableToFile("TargetGivenSourceProbabilities.txt");
		translationRuleProbabilityTable.writeSourceGivenTargetProbabilityTableToFile("SourceGivenTargetProbabilities.txt");
		long millisSpend = System.currentTimeMillis() - measurePointTime;
		System.out.println("Time spend for writing probability tables : " + TimeObject.createTimeString(millisSpend));
		measurePointTime = System.currentTimeMillis();
	}

	protected void writeBasicGrammarToFile(TranslationRuleProbabilityTable translationRuleProbabilityTable, String resultGrammarFilePath, 
		boolean writePlainHieroRules) {
		GrammarWriter grammarWriter = GrammarWriter.createBasicGrammarWriter(translationRuleProbabilityTable, unknownWordsRulesCreator, writePlainHieroRules, systemIdentity);
		grammarWriter.writeHieroGrammarToFile(resultGrammarFilePath, true);
	}
	
	public void writeGapLabelConditionalProbabilityTableToFile(TranslationRuleProbabilityTable translationRuleProbabilityTable, 
		String gapLabelConditionalProbabilityTableFilePath)
	{
	    GrammarWriter grammarWriter = GrammarWriter.createBasicGrammarWriter(translationRuleProbabilityTable, unknownWordsRulesCreator, false, systemIdentity);
	    grammarWriter.writeGapLabeConditionalProbabilityTableToFile(gapLabelConditionalProbabilityTableFilePath);
	}

	protected abstract boolean useExtraSAMTFeatures();

	private void writeGrammarsToFileAndPrintTimeConcsumption(GrammarWriter grammarWriter, String resultGrammarFilePath) {
		long measurePointTime = System.currentTimeMillis();
		grammarWriter.writeHieroGrammarToFile(resultGrammarFilePath, true);
		long millisSpend = System.currentTimeMillis() - measurePointTime;
		System.out.println("Time spend for writing Tries to grammar file : " + TimeObject.createTimeString(millisSpend));
		measurePointTime = System.currentTimeMillis();
		System.out.println(EbitgCorpusComputationMultiThread.heapSize());
	}

	public void writeGrammaWithWordEnrichmentSmoothingFeatures(TranslationRuleProbabilityTable mainTranslationRuleProbabilityTable,
			TranslationRuleProbabilityTable smoothedTranslationRuleProbabilityTable, String resultGrammarFilePath, boolean writePartialResultGrammars, boolean writePlainHieroRules) {

		System.out.println("WriteGrammarEnrichedWithSmoothingFeatures...");

		if (printProbabilityTables) {
			printProbabilityTables(mainTranslationRuleProbabilityTable);
		}
		if (writeProbabilityTablesToFiles) {
			writeProbabilityTables(mainTranslationRuleProbabilityTable);
		}
		if (writePartialResultGrammars) {
			GrammarWriter mainGrammarWriter = GrammarWriter.createBasicGrammarWriter(mainTranslationRuleProbabilityTable, unknownWordsRulesCreator, writePlainHieroRules, systemIdentity);
			writeGrammarsToFileAndPrintTimeConcsumption(mainGrammarWriter, resultGrammarFilePath + MAIN_GRAMMAR_SUFFIX);
			GrammarWriter smoothedGrammarWriter = GrammarWriter.createBasicGrammarWriter(smoothedTranslationRuleProbabilityTable, unknownWordsRulesCreator, writePlainHieroRules, systemIdentity);
			writeGrammarsToFileAndPrintTimeConcsumption(smoothedGrammarWriter, resultGrammarFilePath + SMOOTHED_GRAMMAR_SUFFIX);
		}

		GrammarWriter combinedGrammarWriter = GrammarWriter.createWordEnrichmentSmoothedGrammarWriter(mainTranslationRuleProbabilityTable, smoothedTranslationRuleProbabilityTable,
				unknownWordsRulesCreator, writePlainHieroRules, systemIdentity);
		writeGrammarsToFileAndPrintTimeConcsumption(combinedGrammarWriter, resultGrammarFilePath);
		return;
	}

	private void printProbabilityTablesIfTurnedOn(TranslationRuleProbabilityTable translationRuleProbabilityTable)
	{
	    if (printProbabilityTables) {
		printProbabilityTables(translationRuleProbabilityTable);
	}
        	if (writeProbabilityTablesToFiles) {
        		writeProbabilityTables(translationRuleProbabilityTable);
        	}
	}
	protected void printAndWriteResults(TranslationRuleProbabilityTable translationRuleProbabilityTable, String resultGrammarFilePath,
		boolean writePlainHieroRules) {

		printProbabilityTablesIfTurnedOn(translationRuleProbabilityTable);

		long measurePointTime = System.currentTimeMillis();
		writeBasicGrammarToFile(translationRuleProbabilityTable, resultGrammarFilePath,
			writePlainHieroRules);

		long millisSpend = System.currentTimeMillis() - measurePointTime;
		System.out.println("Time spend for writing Tries to grammar file : " + TimeObject.createTimeString(millisSpend));
		measurePointTime = System.currentTimeMillis();
		System.out.println(EbitgCorpusComputationMultiThread.heapSize());

		return;
	}
	
	

	protected <T extends BareTranslationRuleExtracterThread<?>, T2 extends MultiThreadGrammarExtractor> double performTrieFilling(ExtractorThreadFactory<T, T2> factory,
			T2 mutliThreadGrammarExtractor, List<Pair<Integer>> minMaxSentencePairNumList) {
		List<BareTranslationRuleExtracterThread<?>> threads = new ArrayList<BareTranslationRuleExtracterThread<?>>();

		for (Pair<Integer> minMaxSentencePairNum : minMaxSentencePairNumList) {
			System.out.println("this: " + this);
			BareTranslationRuleExtracterThread<?> tret = factory.createExtractorThread(minMaxSentencePairNum, mutliThreadGrammarExtractor);
			// assert(tret != null);
			threads.add(tret);
		}
		List<BareTranslationRuleExtracterThread<?>> results = performThreadComputation(threads);
		System.out.println("\n\n>>Trie Filling finished \n\n");
		return getGrammarRuleExtractionResultsRapport(results);
	}

	protected <T extends TranslationRuleExtractorThread<?>, T2 extends MultiThreadGrammarExtractor> double performTrieFilling(ExtractorThreadFactory<T, T2> factory, T2 mutliThreadGrammarExtractor) {
		List<Pair<Integer>> minMaxSentencePairNumList = mutliThreadGrammarExtractor.createMinMaxSentencePairNumsList();
		return performTrieFilling(factory, mutliThreadGrammarExtractor, minMaxSentencePairNumList);
	}

	public int getLexicalCountSourceToTarget(String sourceWord, String targetWord) {
		return this.getTranslationRuleCountTable().getLexicalCountSourceToTarget(sourceWord, targetWord);
	}

	public int getLexicalCountTargetToSource(String targetWord, String sourceWord) {
		return this.getTranslationRuleCountTable().getLexicalCountTargetToSource(targetWord, sourceWord);
	}

	public double getLexicalProbabilitySourceToTarget(String sourceWord, String targetWord) {
		return this.getTranslationRuleCountTable().getLexicalProbabilitySourceToTarget(sourceWord, targetWord);
	}

	public double getLexicalProbabilityTargetToSource(String targetWord, String sourceWord) {
		return this.getTranslationRuleCountTable().getLexicalProbabilityTargetToSource(targetWord, sourceWord);
	}

	public double getLexicalWeightSourceToTarget(String sourceString, String targetString, String alignmentString) {
		return this.getTranslationRuleCountTable().getLexicalWeightSourceToTarget(sourceString, targetString, alignmentString);
	}

	public double getLexicalWeightTargetToSource(String sourceString, String targetString, String alignmentString) {
		return this.getTranslationRuleCountTable().getLexicalWeightTargetToSource(sourceString, targetString, alignmentString);
	}

	public void printSourceTrie() {
		this.getTranslationRuleCountTable().printSourceTrie();
	}

	public double getRuleConditionalPhraseProbabilitySourceToTarget(String sourceSideRuleString, String targetSideRuleString) {
		return this.getTranslationRuleCountTable().getRuleConditionalPhraseProbabilitySourceToTarget(sourceSideRuleString, targetSideRuleString);
	}

	public boolean testLexicalTables() {
		return this.getTranslationRuleCountTable().testLexicalTables();
	}

	public TranslationRuleCountTable getTranslationRuleCountTable() {
		return translationRuleCountTable;
	}

	public GrammarExtractionConstraints getChiangGrammarExtractionConstraints() {
		return grammarExtractionConstraints;
	}

	public WordKeyMappingTable getWordKeyMappingTable() {
		return this.translationRuleCountTable.getWordKeyMappingTable();
	}

	public LabelProbabilityEstimator getLabelProbabilityEstimator() {
		return this.getTranslationRuleCountTable().getLabelProbabilityEstimator();
	}

}
