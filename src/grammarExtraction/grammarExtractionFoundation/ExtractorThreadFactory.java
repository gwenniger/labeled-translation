package grammarExtraction.grammarExtractionFoundation;

import util.Pair;

public interface ExtractorThreadFactory<T extends BareTranslationRuleExtracterThread<?>, T2 extends MultiThreadGrammarExtractor> 
{
	T createExtractorThread(Pair<Integer> minMaxSentencePairNum,T2 multiThreadGrammarExtractor);
}