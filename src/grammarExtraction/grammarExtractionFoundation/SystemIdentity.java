package grammarExtraction.grammarExtractionFoundation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mt_pipeline.MTPipelineHATsSAMT;
import mt_pipeline.MTPipelineHatsBoundaryTagLabeled;
import mt_pipeline.MTPipelineHatsHiero;
import mt_pipeline.mt_config.JoshuaSupportObjectsFactory;
import mt_pipeline.mt_config.MTConfigFile;
import mt_pipeline.mt_config.SystemSpecificSupportObjectsFactory;
import util.ConfigFile;
import util.Serialization;
import grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagLabelingSettings;
import grammarExtraction.chiangGrammarExtraction.ReorderLabelingSettings;
import grammarExtraction.chiangGrammarExtraction.RuleFeaturesBasic;
import grammarExtraction.grammarExtractionFoundation.LabelingSettings.CanonicalFormSettings;
import grammarExtraction.labelSmoothing.LabeledRulesSmoother;
import grammarExtraction.labelSmoothing.LabeledRulesSmoother.LABEL_SMOOTHING_TYPE;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatter;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatterJoshua;
import grammarExtraction.phraseProbabilityWeights.NamedFeature;
import grammarExtraction.reorderingLabeling.ReorderingFeatureSetCreater;
import grammarExtraction.reorderingLabeling.RuleFeaturesReordering;
import grammarExtraction.samtGrammarExtraction.RuleFeaturesStandard;
import grammarExtraction.translationRuleTrie.TranslationRuleProbabilityTable;
import grammarExtraction.translationRules.JoshuaRuleRepresentationCreater;
import grammarExtraction.translationRules.MosesRuleRepresentationCreater;
import grammarExtraction.translationRules.TranslationRuleFeatures;
import grammarExtraction.translationRules.TranslationRuleRepresentationCreater;

public class SystemIdentity implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final int HATS_HIERO_NO_LEXICAL_WEIGHTS = 2;
	public static final int HATS_HIERO_BASIC_NUMBER_OF_PHRASE_PROBABILITY_WEIGHTS = 2;
	public static final String MAX_SOURCE_AND_TARGET_LENGTH_PROPERTY = "maxSourceAndTargetLength";
	public static final int STANDARD_MAX_SOURCE_AND_TARGET_LENGTH  = 10;
	
	private final String systemName;
	private final LabelingSettings labelingSettings;
	private final FeatureSettings featureSettings;
	private final TranslationRuleRepresentationCreater translationRuleRepresentationCreater;
	private final GrammarStructureSettings grammarStructureSettings;
	/**
	 * The maximum source and target length to be used for grammar extraction and decoding
	 */
	private final int maxSourceAndTargetLength;

	// private final boolean usePackedGramamars;
	// private final boolean useMosesHieroUnknownWordRuleVariants;
	// private final boolean writePlainHieroRulesForSmoothing;

	private SystemIdentity(String systemName, LabelingSettings labelingSettings, FeatureSettings featureSettings, TranslationRuleRepresentationCreater translationRuleRepresentationCreater,
			GrammarStructureSettings grammarStructureSettings,int maxSourceAndTargetLength) {
		this.systemName = systemName;
		this.labelingSettings = labelingSettings;
		this.featureSettings = featureSettings;
		this.translationRuleRepresentationCreater = translationRuleRepresentationCreater;
		this.grammarStructureSettings = grammarStructureSettings;
		this.maxSourceAndTargetLength = maxSourceAndTargetLength;
		sanityCheckUseMosesHieroUnknownWordRuleVariants();
	}

	private final void sanityCheckUseMosesHieroUnknownWordRuleVariants() {
		if (isJoshuaSystem() && grammarStructureSettings.useMosesHieroUnknownWordRuleVariants()) {
			throw new RuntimeException("Error:  useMosesHieroUnknownWordRuleVariants in Joshua System, which should not happen");
		}

		if ((!useNonHieroLabels()) && grammarStructureSettings.useMosesHieroUnknownWordRuleVariants()) {
			throw new RuntimeException("Error:  useMosesHieroUnknownWordRuleVariants in plain Hiero (X-labeled) system which should not happen");
		}
	}

	public static int getMaxSourceAndTargetLengthFromConfig(ConfigFile theConfig){
	    String valueString = theConfig.getValueWithPresenceCheck(MAX_SOURCE_AND_TARGET_LENGTH_PROPERTY);
	    return Integer.parseInt(valueString);
	}
	
	public static SystemIdentity createSystemIdentity(MTConfigFile theConfig) {
		SystemSpecificSupportObjectsFactory systemSpecificSupportObjectsFactory = MTConfigFile.createSystemSpecificSupportObjectsFactory(theConfig);
		LabelingSettings labelingSettings = LabelingSettings.createLabelingSettings(theConfig, theConfig.getSystemName(), systemSpecificSupportObjectsFactory);
		TranslationRuleRepresentationCreater translationRuleRepresentationCreater = systemSpecificSupportObjectsFactory
				.createTranslationRuleRepresentationCreater(theConfig, theConfig.getSystemName());
		GrammarStructureSettings grammarStructureSettings = GrammarStructureSettings.createGrammarStructureSettings(theConfig, translationRuleRepresentationCreater,
				labelingSettings.useNonHieroLabels());
		return new SystemIdentity(theConfig.getSystemName(), labelingSettings, FeatureSettings.createFeatureSettings(theConfig, labelingSettings.getReorderLabelingSettings(),
				systemSpecificSupportObjectsFactory.createFeatureValueFormatter(theConfig)), translationRuleRepresentationCreater, grammarStructureSettings,getMaxSourceAndTargetLengthFromConfig(theConfig));
	}

	public static SystemIdentity createSystemIdentityWithStandardLabelingSettings(ConfigFile theConfig, String systemName) {
		SystemSpecificSupportObjectsFactory systemSpecificSupportObjectsFactory = MTConfigFile.createSystemSpecificSupportObjectsFactory(theConfig);
		LabelingSettings labelingSettings = LabelingSettings.createStandardLabelingSettings(true);
		TranslationRuleRepresentationCreater translationRuleRepresentationCreater = systemSpecificSupportObjectsFactory.createTranslationRuleRepresentationCreater(theConfig, systemName);
		GrammarStructureSettings grammarStructureSettings = GrammarStructureSettings.createGrammarStructureSettings(theConfig, translationRuleRepresentationCreater,
				labelingSettings.useNonHieroLabels());
		return new SystemIdentity(systemName, labelingSettings, FeatureSettings.createFeatureSettings(theConfig, labelingSettings.getReorderLabelingSettings(),
				systemSpecificSupportObjectsFactory.createFeatureValueFormatter(theConfig)), translationRuleRepresentationCreater, grammarStructureSettings,getMaxSourceAndTargetLengthFromConfig(theConfig));
	}

	public static SystemIdentity createSystemIdentityJoshuaSystemWithStandardLabels(ConfigFile theConfig, String systemName) {
		SystemSpecificSupportObjectsFactory systemSpecificSupportObjectsFactory = new JoshuaSupportObjectsFactory();
		boolean useNonHieroLabels = systemSpecificSupportObjectsFactory.useNonHieroLabels(theConfig, systemName);
		LabelingSettings labelingSettings = LabelingSettings.createStandardLabelingSettings(useNonHieroLabels);
		TranslationRuleRepresentationCreater translationRuleRepresentationCreater = systemSpecificSupportObjectsFactory.createTranslationRuleRepresentationCreater(theConfig, systemName);
		GrammarStructureSettings grammarStructureSettings = GrammarStructureSettings.createGrammarStructureSettings(theConfig, translationRuleRepresentationCreater, useNonHieroLabels);
		return new SystemIdentity(systemName, labelingSettings, FeatureSettings.createFeatureSettings(theConfig, labelingSettings.getReorderLabelingSettings(), new FeatureValueFormatterJoshua()),
				translationRuleRepresentationCreater, grammarStructureSettings,getMaxSourceAndTargetLengthFromConfig(theConfig));
	}

	
	public static SystemIdentity createStandardReorderingLabeledSystemIdentity(CanonicalFormSettings canonicalFormSettings) {
		GrammarStructureSettings grammarStructureSettings = GrammarStructureSettings.createGrammarStructureSettings(false, false, false, false,false);
		return new SystemIdentity(MTPipelineHATsSAMT.SystemName, LabelingSettings.createLabelingSettingsReorderingLabeled(canonicalFormSettings), FeatureSettings.createFeatureSettingsStandard(),
				JoshuaRuleRepresentationCreater.createJoshuaRuleRepresentationCreaterFactoryComplexLabeled(), grammarStructureSettings,STANDARD_MAX_SOURCE_AND_TARGET_LENGTH);
	}
	
	public static SystemIdentity createStandardSAMTSystemIdentity() {
		GrammarStructureSettings grammarStructureSettings = GrammarStructureSettings.createGrammarStructureSettings(false, false, false, false,false);
		return new SystemIdentity(MTPipelineHATsSAMT.SystemName, LabelingSettings.createStandardLabelingSettings(true), FeatureSettings.createFeatureSettingsStandard(),
				JoshuaRuleRepresentationCreater.createJoshuaRuleRepresentationCreaterFactoryComplexLabeled(), grammarStructureSettings,STANDARD_MAX_SOURCE_AND_TARGET_LENGTH);
	}

	public static SystemIdentity createSAMTSystemIdentyWithoutSmoothing() {
		GrammarStructureSettings grammarStructureSettings = GrammarStructureSettings.createGrammarStructureSettings(false, false, false, false,false);
		return new SystemIdentity(MTPipelineHATsSAMT.SystemName, LabelingSettings.createStandardLabelingSettings(true), FeatureSettings.createFeatureSettingsWithoutSmoothing(),
				JoshuaRuleRepresentationCreater.createJoshuaRuleRepresentationCreaterFactoryComplexLabeled(), grammarStructureSettings,STANDARD_MAX_SOURCE_AND_TARGET_LENGTH);
	}

	public static SystemIdentity createHieroSystemIdenty() {
		GrammarStructureSettings grammarStructureSettings = GrammarStructureSettings.createGrammarStructureSettings(false, false, false, false,false);
		return new SystemIdentity(MTPipelineHatsHiero.SystemName, LabelingSettings.createStandardLabelingSettings(false), FeatureSettings.createFeatureSettingsWithoutSmoothing(),
				JoshuaRuleRepresentationCreater.createJoshuaRuleRepresentationCreaterFactoryHieroLabeled(), grammarStructureSettings,STANDARD_MAX_SOURCE_AND_TARGET_LENGTH);
	}

	public String getSystemName() {
		return this.systemName;
	}

	public boolean useExtraSamtFeatures() {
		return this.featureSettings.useExtraSamtFeatures();
	}

	public boolean useReorderingBinaryFeatures() {
		return this.featureSettings.useReorderingBinaryFeatures();
	}

	public ReorderingFeatureSetCreater getReorderingFeatureSetCreater() {
		return this.featureSettings.getReorderingFeatureSetCreater();
	}

	public boolean useWordEnrichmentSmoothingFeatures() {
		return this.featureSettings.useWordEnrichmentSmoothingFeatures();
	}

	public ReorderLabelingSettings getReorderLabelingSettings() {
		return this.labelingSettings.getReorderLabelingSettings();
	}

	public LABEL_SMOOTHING_TYPE getLabSmoothingType() {
		return this.featureSettings.getLabelSmoothingType();
	}

	public boolean useReorderingLabelExtension() {
		return this.labelingSettings.useReorderingLabelExtension();
	}

	public BoundaryTagLabelingSettings getBoundaryTagLabelingSettings() {
		return labelingSettings.getBoundaryTagLabelingSettings();
	}

	public List<String> getSmoothingPrefixesForSystem() {
		List<String> result = new ArrayList<String>();
		if (systemName.equals(MTPipelineHATsSAMT.SystemName)) {
			result.addAll(LabeledRulesSmoother.getSmootingPrefixesSAMTSystem(this));
		} else if (systemName.equals(MTPipelineHatsBoundaryTagLabeled.SystemName)) {
			result.addAll(LabeledRulesSmoother.getSmoothingPrefixesBoundaryTaggedSystem(this));
		} else if (systemName.equals(MTPipelineHatsHiero.SystemName)) {
			result.addAll(LabeledRulesSmoother.getSmoothingPrefixesHieroSystem(this));
		} else {
			return Collections.emptyList();
		}

		if (this.writePlainHieroRulesForSmoothing() && this.useSeparateFeatureForPlainHieroRulesForSmoothing()) {
			result.add(TranslationRuleProbabilityTable.PURE_HIERO_SMOOTHING_RULE_PREFIX_STRING);
		}

		return result;
	}

	public boolean stripSourcePartLabelsForSmoothingDoubleLabeldRules() {
		return this.featureSettings.stripSourcePartLabelsForSmoothingDoubleLabeldRules();
	}

	private int getNumExtraLabelSmoothingPhraseFeatures() {
		int result = getSmoothingPrefixesForSystem().size() * 2;
		return result;
	}

	private int getNumPhraseProbabilityFeatures() {
		int result = HATS_HIERO_BASIC_NUMBER_OF_PHRASE_PROBABILITY_WEIGHTS;
		result += getNumExtraLabelSmoothingPhraseFeatures();
		if (useWordEnrichmentSmoothingFeatures()) {
			result *= 2;
		}
		return result;
	}

	public int getNumLexicalPlusPhraseProbabilityFeatures() {
		int result = this.getNumPhraseProbabilityFeatures();
		result += HATS_HIERO_NO_LEXICAL_WEIGHTS;
		return result;
	}

	public static int getNumberOfBasicFeatureWeights() {
		return HATS_HIERO_BASIC_NUMBER_OF_PHRASE_PROBABILITY_WEIGHTS + HATS_HIERO_NO_LEXICAL_WEIGHTS;
	}

	public void serialize(String serializationFileName) {
		Serialization.serializeOBject(this, serializationFileName);
	}

	private TranslationRuleFeatures getOrderedRepresentativeTranslationRuleFeatures() {
		if (this.useExtraSamtFeatures()) {
			if (this.useReorderingBinaryFeatures()) {
				return RuleFeaturesReordering.createRuleFeaturesReorderingZeroValues(this);
			} else {
				return RuleFeaturesStandard.createRuleFeaturesStandardZeroValues(this);
			}
		} else {
			return RuleFeaturesBasic.createRuleFeaturesBasicZeroValues(this);
		}
	}

	private List<NamedFeature> getOrderedRepresentativeFeatures() {
		return getOrderedRepresentativeTranslationRuleFeatures().getNamedFeaturesList();
	}

	private List<NamedFeature> getOrderedRepresentativeDenseFeatures() {
		return getOrderedRepresentativeTranslationRuleFeatures().getDenseNamedFeaturesList();
	}

	private List<NamedFeature> getOrderedRepresentativeDiscriminativeFeatures() {
		return getOrderedRepresentativeTranslationRuleFeatures().getDiscriminativeNamedFeaturesList();
	}

	public List<String> getOrderedFeatureNames() {
		List<String> result = new ArrayList<String>();
		for (NamedFeature namedFeature : getOrderedRepresentativeFeatures()) {
			result.add(namedFeature.getFeatureName());
		}
		return result;
	}

	public List<String> getOrderedDiscriminativeFeatureNames() {
		List<String> result = new ArrayList<String>();
		for (NamedFeature namedFeature : getOrderedRepresentativeDiscriminativeFeatures()) {
			result.add(namedFeature.getFeatureName());
		}
		return result;
	}

	public List<String> getOrderedDenseFeatureNames() {
		List<String> result = new ArrayList<String>();
		for (NamedFeature namedFeature : getOrderedRepresentativeDenseFeatures()) {
			result.add(namedFeature.getFeatureName());
		}
		return result;
	}

	public boolean usePackedGrammars() {
		return this.grammarStructureSettings.usePackedGramamars();
	}

	public String getCorrectFormatFeaturesRepresentation(TranslationRuleFeatures translationRuleFeatures) {
		if (usePackedGrammars()) {
			return translationRuleFeatures.getSparseLabeledFeaturesRepresentation();
		} else {
			return translationRuleFeatures.getJoshuaOriginalFeaturesRepresentation();
		}
	}

	public TranslationRuleRepresentationCreater getTranslationRuleRepresentationCreater() {
		return this.translationRuleRepresentationCreater;
	}

	public FeatureValueFormatter getFeatureValueFormatter() {
		return this.translationRuleRepresentationCreater.getFeatureValueFormatter();
	}

	public boolean isJoshuaSystem() {
		return (this.translationRuleRepresentationCreater instanceof JoshuaRuleRepresentationCreater);
	}

	static boolean isMosesSystem(TranslationRuleRepresentationCreater translationRuleRepresentationCreater) {
		return (translationRuleRepresentationCreater instanceof MosesRuleRepresentationCreater);
	}

	public boolean isMosesSystem() {
		return isMosesSystem(this.translationRuleRepresentationCreater);
	}

	public String getUnknownWordsLabel() {
		return translationRuleRepresentationCreater.getUnknownWordsLabel();
	}

	public boolean useNonHieroLabels() {
		return this.labelingSettings.useNonHieroLabels();
	}

	public boolean useMosesHieroUnknownWordRuleVariants() {

		return this.grammarStructureSettings.useMosesHieroUnknownWordRuleVariants();
	}

	public boolean writePlainHieroRulesForSmoothing() {
		return this.grammarStructureSettings.writePlainHieroRulesForSmoothing();
	}

	public boolean useSeparateFeatureForPlainHieroRulesForSmoothing() {
		return this.grammarStructureSettings.useSeparateFeatureForPlainHieroRulesForSmoothing();
	}

	public boolean useAbstractRuleReorderingLabelingOnly() {
		return this.getReorderLabelingSettings().useAbstractRuleReorderingLabelingOnly();
	}

	public boolean labelSourceSide() {
		return this.labelingSettings.getLabelSideSettings().labelSourceSide();
	}

	public boolean labelTargetSide() {
		return this.labelingSettings.getLabelSideSettings().labelTargetSide();
	}

	public boolean labelBothSides() {
		return (this.labelSourceSide() && this.labelTargetSide());
	}

	public boolean useCanonicalFormLabeledRules() {
		return labelingSettings.useCanonicalFormLabeledRules();
	}
	
	public boolean useOnlyHieroWeightsForCanonicalFormLabeledRules(){
	    return labelingSettings.useOnlyHieroWeightsForCanonicalFormLabeledRules();
	}
	
	public boolean restrictLabelVariationForCanonicalFormLabeledRules() {
	    return labelingSettings.restrictLabelVariationForCanonicalFormLabeledRules();
	}

	public int numAllowedTypesPerLabelForSource() {
	    return labelingSettings.numAllowedTypesPerLabelForSource();
	}

	public int getMaxSourceAndTargetLength() {
	    return maxSourceAndTargetLength;
	}
	
	public boolean producesDoubleReorderingLabels() {
	    return this.labelingSettings.producesDoubleReorderingLabels();
	}
	
	public CanonicalFormSettings getCanonicalFormSettings(){
	    return this.labelingSettings.getCanonicalFormSettings();
	}
	
	public boolean useSingleUnknownWordPreterminalForEfficientFuzzyMatchingDecoding(){
	    return this.grammarStructureSettings.useSingleUnknownWordPreterminalForEfficientFuzzyMatchingDecoding();
	}
}
