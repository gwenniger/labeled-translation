package grammarExtraction.grammarExtractionFoundation;

import util.Span;

public interface SourceLabelingCreater {
	public String createSourceRuleLabelingForSourceSpan(Span sourceSpan);
	public String createSourceGapLabelingForSourceSpan(Span sourceSpan);
}
