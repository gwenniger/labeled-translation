package grammarExtraction.grammarExtractionFoundation;

import extended_bitg.EbitgLexicalizedInference;
import grammarExtraction.chiangGrammarExtraction.ChiangRuleExtracter;
import grammarExtraction.chiangGrammarExtraction.GrammarExtractionConstraints;
import grammarExtraction.chiangGrammarExtraction.HierarchicalPhraseBasedRuleExtractor;
import grammarExtraction.labelSmoothing.RuleLabelsSmoother;
import grammarExtraction.samtGrammarExtraction.SAMTGrammarExtractionConstraints;
import grammarExtraction.samtGrammarExtraction.SAMTQuadruple;
import grammarExtraction.samtGrammarExtraction.SAMTRuleExtracter;
import grammarExtraction.translationRuleTrie.TranslationRuleTrie;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import util.Pair;
import alignment.AlignmentStringTriple;
import alignmentStatistics.InputStringsEnumeration;
import alignmentStatistics.MultiThreadComputationParameters;

public abstract class TranslationRuleExtractorThread<InputType extends AlignmentStringTriple> extends BareTranslationRuleExtracterThread<InputType> {
	protected final RuleExtracter<? extends EbitgLexicalizedInference, InputType> ruleExtracter;
	protected final RuleLabelsSmoother ruleLabelsSmoother;

	public TranslationRuleExtractorThread(RuleExtracter<? extends EbitgLexicalizedInference, InputType> ruleExtracter, RuleLabelsSmoother ruleLabelsSmoother) {
		super();
		this.ruleExtracter = ruleExtracter;
		this.ruleLabelsSmoother = ruleLabelsSmoother;
	}

	public TranslationRuleExtractorThread(int maxAllowedInferencesPerNode, Pair<Integer> minMaxSentencePairNum, InputStringsEnumeration<InputType> inputStringEnumeration, int maxSentenceLength,
			int outputLevel, TranslationRuleTrie translationRuleTrie, WordKeyMappingTable wordKeyMappingTable, boolean useJoshuaStyleSimplifiedCountComputing, boolean useCaching,
			RuleExtracter<? extends EbitgLexicalizedInference, InputType> ruleExtracter, RuleLabelsSmoother ruleLabelsSmoother) {
		super(maxAllowedInferencesPerNode, minMaxSentencePairNum, inputStringEnumeration, maxSentenceLength, outputLevel, translationRuleTrie, wordKeyMappingTable,
				useJoshuaStyleSimplifiedCountComputing, useCaching);
		this.ruleExtracter = ruleExtracter;
		this.ruleLabelsSmoother = ruleLabelsSmoother;
	}

	protected HierarchicalPhraseBasedRuleExtractor<? extends EbitgLexicalizedInference> createTranslationRuleExtractor(InputType input) {
		return ruleExtracter.createTranslationRuleExtractor(input);

	}

	public static ChiangRuleExtracter createChiangRuleExtracter(MultiThreadComputationParameters parameters, GrammarExtractionConstraints chiangGrammarExtractionConstraints) {
		return ChiangRuleExtracter.createChiangRuleExtracter(chiangGrammarExtractionConstraints, parameters.getMaxAllowedInferencesPerNode(), true, parameters.getUseCaching());
	}

	public static RuleExtracter<EbitgLexicalizedInference, SAMTQuadruple> createSAMTRuleExtracter(MultiThreadComputationParameters parameters,
			SAMTGrammarExtractionConstraints samtGrammarExtractionConstraints) {
		return SAMTRuleExtracter.createSAMTRuleExtracter(samtGrammarExtractionConstraints, parameters.getMaxAllowedInferencesPerNode(), true, parameters.getUseCaching());
	}
}
