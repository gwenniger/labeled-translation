package grammarExtraction.grammarExtractionFoundation;

import tsg.TSNodeLabel;
import tsg.corpora.Wsj;
import ccgLabeling.CCGLabeler;
import alignment.SourceString;
import alignmentStatistics.InputStringsEnumeration;
import alignmentStatistics.MultiThreadComputationParameters;
import junit.framework.Assert;
import extended_bitg.EbitgChartBuilderBasic;
import extended_bitg.EbitgLexicalizedInference;
import grammarExtraction.chiangGrammarExtraction.GrammarExtractionConstraints;
import grammarExtraction.chiangGrammarExtraction.HierarchicalPhraseBasedRuleExtractor;

public abstract class RuleExtracter<T extends EbitgLexicalizedInference, InputType extends SourceString> {
	protected final GrammarExtractionConstraints grammarExtractionConstraints;
	protected final int maxAllowedInferencesPerNode;
	protected final boolean useCaching;

	protected RuleExtracter(GrammarExtractionConstraints grammarExtractionConstraints, int maxAllowedInferencesPerNode, 
			boolean useCaching) {
		this.grammarExtractionConstraints = grammarExtractionConstraints;
		this.maxAllowedInferencesPerNode = maxAllowedInferencesPerNode;
		this.useCaching = useCaching;
	}

	public abstract HierarchicalPhraseBasedRuleExtractor<T> createTranslationRuleExtractor(InputType input);

	public abstract InputStringsEnumeration<InputType> createInputStringEnumeration(MultiThreadComputationParameters parameters);

	protected EbitgChartBuilderBasic createChartBuilderBasic(String sourceString, String targetString, String alignmentString) {
		EbitgChartBuilderBasic ebitgChartBuilder = EbitgChartBuilderBasic.createCompactHATsEbitgChartBuilder(sourceString, targetString, alignmentString,
				maxAllowedInferencesPerNode,true, useCaching);

		assert (maxAllowedInferencesPerNode > 10000);

		boolean result = ebitgChartBuilder.findDerivationsAlignment();
		Assert.assertEquals(true, result);
		return ebitgChartBuilder;
	}

	protected CCGLabeler createTargetCCGLabeler(String targetParse) {
		CCGLabeler ccgLabeler;
		try {
			String parseString = Wsj.getPreprocessedParseString(targetParse);
			// System.out.println("parseString: " + parseString);
			ccgLabeler = CCGLabeler.createTargetCCGLabeler(new TSNodeLabel(parseString), grammarExtractionConstraints.getUnaryCategoryHandlerType(),
					grammarExtractionConstraints.allowDoublePlus(), grammarExtractionConstraints.allowPosTagLabelFallback());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return ccgLabeler;
	}



	public GrammarExtractionConstraints getGrammarExtractionConstraints() {
		return grammarExtractionConstraints;
	}
}
