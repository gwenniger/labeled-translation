package grammarExtraction.grammarExtractionFoundation;

import util.Span;
import grammarExtraction.chiangGrammarExtraction.ChiangRuleCreater;

public class HieroSourceLabelingCreater implements SourceLabelingCreater{

	@Override
	public String createSourceRuleLabelingForSourceSpan(Span sourceSpan) {
		return  ChiangRuleCreater.ChiangLabel;
	}

	@Override
	public String createSourceGapLabelingForSourceSpan(Span sourceSpan) {
		return ChiangRuleCreater.ChiangLabel;
	}

}
