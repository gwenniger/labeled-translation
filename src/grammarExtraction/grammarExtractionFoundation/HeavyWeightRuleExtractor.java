package grammarExtraction.grammarExtractionFoundation;
import util.Span;
import grammarExtraction.translationRules.TranslationRuleCreater;
import hat_lexicalization.Lexicalizer;
import extended_bitg.EbitgChartBuilder;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeState;

public class HeavyWeightRuleExtractor<T extends EbitgLexicalizedInference, T2 extends EbitgTreeState<T,T2>>
{
	protected static final boolean USE_GREEDY_NULL_BINDING = true;
	protected final EbitgChartBuilder<T,T2> chartBuilder;
	
	protected final TranslationRuleCreater<T> translationRuleCreater;
	
	
	protected HeavyWeightRuleExtractor(EbitgChartBuilder<T,T2> chartBuilder, TranslationRuleCreater<T> translationRuleCreater)
	{
		assureChartBuilderHasBeenFilled(chartBuilder);
		this.chartBuilder = chartBuilder;
		this.translationRuleCreater = translationRuleCreater;
	}

	
	private void assureChartBuilderHasBeenFilled(EbitgChartBuilder<T,T2> chartBuilder)
	{
		if(!chartBuilder.derivationsAlignmentHaveBeenComputed())
		{
			throw new RuntimeException("HeavyWeightRuleExtractor: trying to create HeavyWeightRuleExtractor with ChartBuilder for which derivations have not yet been found");
		}
	}
	
	
	public int getChartSourceLength()
	{
		return this.chartBuilder.getChartSourceLength();
	}
	
	public int getChartTargetLength()
	{
		return this.chartBuilder.getChartTargetLength();
	}
	
	
	protected T getNoNullsOnEdgesSatisfyingFirstCompleteInference(Span absoluteSourceSpan)
	{
		T tightPhrasePairInference = this.chartBuilder.getTightPhrasePair(absoluteSourceSpan);
		if(tightPhrasePairInference != null)
		{
			return tightPhrasePairInference;
		}
		return null;
	}
	
	protected Lexicalizer getLexicalizer()
	{
		return chartBuilder.getLexicalizer();
	}
	
	public TranslationRuleCreater<T> getTranslationRuleCreater()
	{
		return translationRuleCreater;
	}
	
}
