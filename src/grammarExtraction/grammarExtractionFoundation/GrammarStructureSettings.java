package grammarExtraction.grammarExtractionFoundation;

import java.io.Serializable;

import grammarExtraction.translationRuleTrie.FeatureEnrichedGrammarRuleCreator;
import grammarExtraction.translationRules.TranslationRuleRepresentationCreater;
import mt_pipeline.mt_config.MTDecoderConfig;
import mt_pipeline.mt_config.MTGrammarExtractionConfig;
import util.ConfigFile;

public class GrammarStructureSettings implements Serializable{
    	/**
         * 
         */
    	private static final long serialVersionUID = 1L;
	public static final String USE_PACKED_GRAMMARS_PROPERTY = "usePackedGrammars";
	public static final String WRITE_PLAIN_HIERO_RULES_FOR_SMOOTHING = "writePlainHieroRulesForSmoothing";
	public static final String USE_SEPARATE_FEATURE_FOR_PLAIN_HIERO_RULES_FOR_SMOOTHING = "useSeparateFeatureForPlainHieroRulesForSmoothing";
	private final boolean usePackedGramamars;
	private final boolean useMosesHieroUnknownWordRuleVariants;
	/**
	 *  When fuzzy decoding will be used, it is typically wise to use only a single 
	 *  nonterminal for producing unknown words, to avoid unnecessary complexity.
	 *  This boolean is set to true if this should be done.
	 */
	private final boolean useSingleUnknownWordPreterminalForEfficientFuzzyMatchingDecoding;
	private final boolean writePlainHieroRulesForSmoothing;
	private final boolean useSeparateFeatureForPlainHieroRulesForSmoothing;

	private GrammarStructureSettings(boolean usePackedGramamars, boolean useMosesHieroUnknownWordRuleVariants, boolean writePlainHieroRulesForSmoothing,
			boolean useSeparateFeatureForPlainHieroRulesForSmoothing,
			boolean useSingleUnknownWordPreterminalForEfficientFuzzyMatchingDecoding) {
		this.usePackedGramamars = usePackedGramamars;
		this.useMosesHieroUnknownWordRuleVariants = useMosesHieroUnknownWordRuleVariants;
		this.writePlainHieroRulesForSmoothing = writePlainHieroRulesForSmoothing;
		this.useSeparateFeatureForPlainHieroRulesForSmoothing = useSeparateFeatureForPlainHieroRulesForSmoothing;
		this.useSingleUnknownWordPreterminalForEfficientFuzzyMatchingDecoding = useSingleUnknownWordPreterminalForEfficientFuzzyMatchingDecoding;
	}

	public static GrammarStructureSettings createGrammarStructureSettings(boolean usePackedGramamars, boolean useMosesHieroUnknownWordRuleVariants, boolean writePlainHieroRulesForSmoothing,
			boolean useSeparateFeatureForPlainHieroRulesForSmoothing,boolean useSingleUnknownWordPreterminalForEfficientFuzzyMatchingDecoding) {
		return new GrammarStructureSettings(usePackedGramamars, useMosesHieroUnknownWordRuleVariants, writePlainHieroRulesForSmoothing, useSeparateFeatureForPlainHieroRulesForSmoothing,
			useSingleUnknownWordPreterminalForEfficientFuzzyMatchingDecoding);
	}

	public static GrammarStructureSettings createGrammarStructureSettings(ConfigFile theConfig, TranslationRuleRepresentationCreater translationRuleRepresentationCreater, boolean useNonHieroLabels) {
		return new GrammarStructureSettings(usePackedGrammars(theConfig), useMosesHieroUnknownWordRuleVariants(theConfig, translationRuleRepresentationCreater, useNonHieroLabels),
				writePlainHieroRulesForSmoothing(theConfig), useSeparateFeatureForPlainHieroRulesForSmoothing(theConfig),useSingleUnknownWordPreterminalForEfficientFuzzyMatchingDecoding(theConfig));
	}

	public boolean usePackedGramamars() {
		return usePackedGramamars;
	}

	public boolean useMosesHieroUnknownWordRuleVariants() {
		return useMosesHieroUnknownWordRuleVariants;
	}

	public boolean writePlainHieroRulesForSmoothing() {
		return writePlainHieroRulesForSmoothing;
	}

	public static boolean usePackedGrammars(ConfigFile theConfig) {
		boolean usePackedGrammars = theConfig.getBooleanIfPresentOrReturnDefault(USE_PACKED_GRAMMARS_PROPERTY, false);
		return usePackedGrammars;
	}

	public static boolean writePlainHieroRulesForSmoothing(ConfigFile theConfig) {
		return theConfig.getBooleanWithPresenceCheck(WRITE_PLAIN_HIERO_RULES_FOR_SMOOTHING);
	}
	
	
	private static boolean  useSingleUnknownWordPreterminalForEfficientFuzzyMatchingDecoding(ConfigFile theConfig){
	    return useFuzzyMatching(theConfig);
	}
	
	private static boolean useFuzzyMatching(ConfigFile theConfig){
	    return theConfig.getBooleanWithPresenceCheck(MTDecoderConfig.FUZZY_MATCHING_PROPERTY_NAME);
	}
	
	private static boolean useSeparateFeatureForPlainHieroRulesForSmoothing(ConfigFile theConfig) {
		return writePlainHieroRulesForSmoothing(theConfig) && theConfig.getBooleanWithPresenceCheck(USE_SEPARATE_FEATURE_FOR_PLAIN_HIERO_RULES_FOR_SMOOTHING);
	}

	private static boolean useMosesHieroUnknownWordRuleVariants(ConfigFile theConfig, TranslationRuleRepresentationCreater translationRuleRepresentationCreater, boolean useNonHieroLabels) {
		return SystemIdentity.isMosesSystem(translationRuleRepresentationCreater) && useNonHieroLabels && MTGrammarExtractionConfig.useMosesHieroUnknownWordRuleVariants(theConfig);
	}

	public boolean useSeparateFeatureForPlainHieroRulesForSmoothing() {
		return useSeparateFeatureForPlainHieroRulesForSmoothing;
	}

	public boolean useSingleUnknownWordPreterminalForEfficientFuzzyMatchingDecoding() {
	    return useSingleUnknownWordPreterminalForEfficientFuzzyMatchingDecoding;
	}

}
