package grammarExtraction.grammarExtractionFoundation;

import extended_bitg.EbitgLexicalizedInference;
import grammarExtraction.translationRuleTrie.LexicalProbabilityTables;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.LightWeightTranslationRule;
import grammarExtraction.translationRules.LightWeightRuleLabel;
import grammarExtraction.translationRules.RuleLabel;
import grammarExtraction.translationRules.TranslationRule;

import java.util.ArrayList;
import java.util.List;


public class LightWeightPhrasePairRuleSet extends PhrasePairRuleSet<LightWeightTranslationRule> {
	private final static double STANDARD_RULE_SET_WEIGHT = 1;
	private final double ruleSetWeight;

	private LightWeightPhrasePairRuleSet(LightWeightTranslationRule baseTranslationRule, List<LightWeightTranslationRule> oneGapTranslationRules,
			List<LightWeightTranslationRule> twoGapTranslationRules, LightWeightRuleLabel leftHandSideLabel, double ruleSetWeight) {
		super(baseTranslationRule, oneGapTranslationRules, twoGapTranslationRules);
		this.ruleSetWeight = ruleSetWeight;
	}
	
	public static LightWeightPhrasePairRuleSet createLightWeightPhrasePairRuleSet(LightWeightTranslationRule baseTranslationRule, List<LightWeightTranslationRule> oneGapTranslationRules,
			List<LightWeightTranslationRule> twoGapTranslationRules, LightWeightRuleLabel leftHandSideLabel) {
		return createLightWeightPhrasePairRuleSet(baseTranslationRule, oneGapTranslationRules, twoGapTranslationRules, leftHandSideLabel, STANDARD_RULE_SET_WEIGHT);
	}

	public static LightWeightPhrasePairRuleSet createLightWeightPhrasePairRuleSet(LightWeightTranslationRule baseTranslationRule, List<LightWeightTranslationRule> oneGapTranslationRules,
			List<LightWeightTranslationRule> twoGapTranslationRules, LightWeightRuleLabel leftHandSideLabel, double ruleSetWeight) {
		return new LightWeightPhrasePairRuleSet(baseTranslationRule, oneGapTranslationRules, twoGapTranslationRules, leftHandSideLabel, ruleSetWeight);
	}

	public static <T extends EbitgLexicalizedInference> LightWeightPhrasePairRuleSet createLightWeightPhrasePairRuleSet(HeavyWeightPhrasePairRuleSet<T> heavyWeightPhrasePairRuleSet,
			WordKeyMappingTable wordKeyMappingTable, LexicalProbabilityTables lexicalProbabilityTables) {

		LightWeightTranslationRule baseTranslationRule = null;
		if (heavyWeightPhrasePairRuleSet.baseTranslationRule != null) {
			baseTranslationRule = LightWeightTranslationRule.createLightWeightTranslationRule(heavyWeightPhrasePairRuleSet.baseTranslationRule, wordKeyMappingTable, lexicalProbabilityTables);
		}
		List<LightWeightTranslationRule> oneGapTranslationRules = createLeightWeightRuleListFromHeavyWeightRuleList(heavyWeightPhrasePairRuleSet.oneGapTranslationRules, wordKeyMappingTable,
				lexicalProbabilityTables);
		List<LightWeightTranslationRule> twoGapTranslationRules = createLeightWeightRuleListFromHeavyWeightRuleList(heavyWeightPhrasePairRuleSet.twoGapTranslationRules, wordKeyMappingTable,
				lexicalProbabilityTables);
		return new LightWeightPhrasePairRuleSet(baseTranslationRule, oneGapTranslationRules, twoGapTranslationRules, getLeftHandSideLabel(heavyWeightPhrasePairRuleSet, wordKeyMappingTable),
				STANDARD_RULE_SET_WEIGHT);
	}

	private static LightWeightRuleLabel getLeftHandSideLabel(HeavyWeightPhrasePairRuleSet<? extends EbitgLexicalizedInference> heavyWeightPhrasePairRuleSet, WordKeyMappingTable wordKeyMappingTable) {
		RuleLabel label = heavyWeightPhrasePairRuleSet.getLeftHandSideLabel();

		if (label != null) {
			return LightWeightRuleLabel.createLightWeightRuleLabel(label, wordKeyMappingTable);
		}

		return null;

	}

	public static LightWeightPhrasePairRuleSet createLightWeightPhrasePairRuleSet(HeavyWeightPhrasePairRuleSet<? extends EbitgLexicalizedInference> heavyWeightPhrasePairRuleSet,
			WordKeyMappingTable wordKeyMappingTable) {

		LightWeightTranslationRule baseTranslationRule = null;
		if (heavyWeightPhrasePairRuleSet.baseTranslationRule != null) {
			baseTranslationRule = LightWeightTranslationRule.createLightWeightTranslationRule(heavyWeightPhrasePairRuleSet.baseTranslationRule, wordKeyMappingTable);
		}
		List<LightWeightTranslationRule> oneGapTranslationRules = createLeightWeightRuleListFromHeavyWeightRuleList(heavyWeightPhrasePairRuleSet.oneGapTranslationRules, wordKeyMappingTable);
		List<LightWeightTranslationRule> twoGapTranslationRules = createLeightWeightRuleListFromHeavyWeightRuleList(heavyWeightPhrasePairRuleSet.twoGapTranslationRules, wordKeyMappingTable);
		return new LightWeightPhrasePairRuleSet(baseTranslationRule, oneGapTranslationRules, twoGapTranslationRules, getLeftHandSideLabel(heavyWeightPhrasePairRuleSet, wordKeyMappingTable),
				STANDARD_RULE_SET_WEIGHT);
	}

	public static LightWeightPhrasePairRuleSet createEmptyLightWeightPhrasePairRuleSet() {
		return new LightWeightPhrasePairRuleSet(null, null, null, null, 0);
	}

	public static <T extends EbitgLexicalizedInference> List<LightWeightTranslationRule> createLeightWeightRuleListFromHeavyWeightRuleList(List<TranslationRule<T>> heavyWeightRuleList,
			WordKeyMappingTable wordKeyMappingTable, LexicalProbabilityTables lexicalProbabilityTables) {
		List<LightWeightTranslationRule> result = new ArrayList<LightWeightTranslationRule>();

		for (TranslationRule<?> translationRule : heavyWeightRuleList) {
			result.add(LightWeightTranslationRule.createLightWeightTranslationRule(translationRule, wordKeyMappingTable, lexicalProbabilityTables));
		}
		return result;
	}

	public static <T extends EbitgLexicalizedInference> List<LightWeightTranslationRule> createLeightWeightRuleListFromHeavyWeightRuleList(List<TranslationRule<T>> heavyWeightRuleList,
			WordKeyMappingTable wordKeyMappingTable) {
		List<LightWeightTranslationRule> result = new ArrayList<LightWeightTranslationRule>();

		for (TranslationRule<?> translationRule : heavyWeightRuleList) {
			result.add(LightWeightTranslationRule.createLightWeightTranslationRule(translationRule, wordKeyMappingTable));
		}
		return result;
	}

	public LightWeightRuleLabel getLeftHandSideLabel() {
		return getLeftHandSideRulesLabel(this.baseTranslationRule, this.oneGapTranslationRules, this.twoGapTranslationRules);
	}

	public static LightWeightRuleLabel getLeftHandSideRulesLabel(LightWeightTranslationRule baseTranslationRule, List<LightWeightTranslationRule> oneGapTranslationRules,
			List<LightWeightTranslationRule> twoGapTranslationRules) {
		if (baseTranslationRule != null) {
			return baseTranslationRule.getLefthHandSideLabel();
		} else if (!oneGapTranslationRules.isEmpty()) {
			return oneGapTranslationRules.get(0).getLefthHandSideLabel();
		} else if (!twoGapTranslationRules.isEmpty()) {
			return twoGapTranslationRules.get(0).getLefthHandSideLabel();
		} else {
			throw new RuntimeException("Error:  getLeftHandSideRulesLabel all rules are null");
		}
	}

	public double getRuleSetWeight() {
		return this.ruleSetWeight;
	}
}
