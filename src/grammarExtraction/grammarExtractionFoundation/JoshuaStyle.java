package grammarExtraction.grammarExtractionFoundation;

import java.util.regex.Pattern;

import junit.framework.Assert;
import grammarExtraction.translationRules.RuleLabel;

public class JoshuaStyle {
    public static final String JOSHUA_STYLE_RULE_SEPERATOR = " ||| ";
    private static final String JOSHUA_STYLE_RULE_SEPARTOR_FOR_SPLIT = " \\|\\|\\| ";
    private static final Pattern JOSHUA_STYLE_RULE_SEPARTOR_SPLIT_PATTERN = Pattern
	    .compile(JOSHUA_STYLE_RULE_SEPARTOR_FOR_SPLIT);
    private static final Pattern SINGLE_WHITE_SPACE_SPLIT_PATTERN = Pattern.compile(" ");

    public static String createJoshuaStyleRuleLabel(String ruleLabel) {
	return RuleLabel.ruleLabelString(convertToJoshuaStyleLabel(ruleLabel));
    }

    public static String convertToJoshuaStyleLabel(String label) {
	return label.replaceAll(",", "_COMMA_").replaceAll("\\$", "_DOLLAR_");
    }

    public static String[] spitOnRuleSeparator(String inputString) {
	return JOSHUA_STYLE_RULE_SEPARTOR_SPLIT_PATTERN.split(inputString);
    }

    public static String[] spitOnSingleWhiteSpace(String inputString) {
	return SINGLE_WHITE_SPACE_SPLIT_PATTERN.split(inputString);
    }

    public static String getRuleIdentity(String ruleString) {
	String[] ruleParts = spitOnRuleSeparator(ruleString);
	/**
	 * A rule should consist of 4 parts, the first 3 parts forming the rule
	 * identity, the last one containing the weights
	 */
	Assert.assertEquals(4, ruleParts.length);

	String result = ruleParts[0] + JOSHUA_STYLE_RULE_SEPERATOR + ruleParts[1]
		+ JOSHUA_STYLE_RULE_SEPERATOR + ruleParts[2];
	return result;
    }

}
