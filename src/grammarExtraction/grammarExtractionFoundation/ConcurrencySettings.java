package grammarExtraction.grammarExtractionFoundation;

public class ConcurrencySettings {

	public static final float ConcurrentHashMapLoadFactor = (float) 0.75;
	public static final int ConcurrentHashMapInitialCapacity = 16;
	
	public static int getConcurrencyLevel(int noConcurrentThreads)
	{
		if(noConcurrentThreads <= 16)
		{
			return 16;
		}
		return noConcurrentThreads;
	}
}
