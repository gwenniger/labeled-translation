package grammarExtraction.grammarExtractionFoundation;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

public abstract class PhrasePairRuleSet<T> {
	final T baseTranslationRule;
	final List<T> oneGapTranslationRules;
	final List<T> twoGapTranslationRules;

	protected PhrasePairRuleSet(T basTranslationRule, List<T> oneGapTranslationRules, List<T> twoGapTranslationRules) {
		this.baseTranslationRule = basTranslationRule;
		this.oneGapTranslationRules = oneGapTranslationRules;
		this.twoGapTranslationRules = twoGapTranslationRules;
		//testConsistency();
	}

	public void testConsistency() {
		for (T rule : oneGapTranslationRules) {
			Assert.assertNotNull(rule);
		}
		for (T rule : twoGapTranslationRules) {
			Assert.assertNotNull(rule);
		}
	}

	public boolean hasRules() {
		return ((this.baseTranslationRule != null) || (this.getNumOneGapRules() > 0) || (this.getNumTwoGapRules() > 0));
	}

	private int getNumOneGapRules() {
		if (this.oneGapTranslationRules == null) {
			return 0;
		}
		return this.oneGapTranslationRules.size();
	}

	private int getNumTwoGapRules() {
		if (this.twoGapTranslationRules == null) {
			return 0;
		}
		return this.twoGapTranslationRules.size();
	}

	public int getNumRules() {
		int result = 0;
		if (hasRules()) {
			if (this.baseTranslationRule != null) {
				result += 1;
			}

			result += this.oneGapTranslationRules.size() + this.twoGapTranslationRules.size();
		}
		return result;
	}

	public List<T> getRules() {
		List<T> result = new ArrayList<T>();
		if (hasRules()) {
			if (this.baseTranslationRule != null) {
				result.add(baseTranslationRule);
			}
			result.addAll(oneGapTranslationRules);
			result.addAll(twoGapTranslationRules);
		}
		return result;
	}

	public List<T> getOneGapTranslationRules() {
		List<T> result = new ArrayList<T>();
		result.addAll(oneGapTranslationRules);
		return result;
	}

	public List<T> getTwoGapTranslationRules() {
		List<T> result = new ArrayList<T>();
		result.addAll(twoGapTranslationRules);
		return result;
	}

	public T getBaseTranslationRule() {
		return this.baseTranslationRule;
	}

	public boolean hasBaseTranslationRule() {
		return (this.baseTranslationRule != null);
	}

	public String toString() {
		String result = "<PhrasePairRuleSet>";
		result += "\n" + this.baseTranslationRule;
		result += "\n Number of one gap rules: " + this.oneGapTranslationRules.size();
		result += "\n Number of two gaps rules: " + this.twoGapTranslationRules.size();
		result += "\n</PhrasePairRuleSet>";
		return result;
	}

}