package grammarExtraction.grammarExtractionFoundation;

import grammarExtraction.chiangGrammarExtraction.GrammarExtractionConstraints;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

import java.util.List;

import alignment.SourceString;
import alignmentStatistics.InputStringsEnumeration;


public interface FilterRuleSetCreater<InputType extends SourceString> {

	public List<LightWeightPhrasePairRuleSet> produceSoureSideConsistentRuleSets(InputType input, WordKeyMappingTable wordKeyMappingTable,
			GrammarExtractionConstraints chiangGrammarExtractionConstraints);
	
	public InputStringsEnumeration<InputType> getFilterSentencesEnumeration();
}
