package grammarExtraction.grammarExtractionFoundation;

import java.io.Serializable;

import mt_pipeline.GrammarCreaterHats;
import mt_pipeline.mt_config.MTGrammarExtractionConfig;
import mt_pipeline.preAndPostProcessing.EnrichedSourceFileCreatorFactory;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatter;
import grammarExtraction.reorderingLabeling.ReorderingFeatureSetCreater;
import grammarExtraction.samtGrammarExtraction.SAMTGrammarExtractionConstraints;
import util.ConfigFile;
import grammarExtraction.chiangGrammarExtraction.ReorderLabelingSettings;
import grammarExtraction.labelSmoothing.LabeledRulesSmoother;
import grammarExtraction.labelSmoothing.LabeledRulesSmoother.LABEL_SMOOTHING_TYPE;

;
public class FeatureSettings implements Serializable {
	private static final long serialVersionUID = 1L;

	private final boolean userExtraSamtFeatures;

	private final boolean useWordEnrichmentSmoothingFeatures;
	private final LABEL_SMOOTHING_TYPE labelSmoothingType;
	private final boolean stripSourcePartLabelsForSmoothingDoubleLabeldRules;
	private final ReorderingFeatureSetCreater reorderingFeatureSetCreater;

	private FeatureSettings(boolean userExtraSamtFeatures, boolean useWordEnrichmentSmoothingFeatures, LABEL_SMOOTHING_TYPE labelSmoothingType,
			boolean stripSourcePartLabelsForSmoothingDoubleLabeldRules, ReorderingFeatureSetCreater reorderingFeatureSetCreater) {
		this.userExtraSamtFeatures = userExtraSamtFeatures;
		this.useWordEnrichmentSmoothingFeatures = useWordEnrichmentSmoothingFeatures;
		this.labelSmoothingType = labelSmoothingType;
		this.stripSourcePartLabelsForSmoothingDoubleLabeldRules = stripSourcePartLabelsForSmoothingDoubleLabeldRules;
		this.reorderingFeatureSetCreater = reorderingFeatureSetCreater;
	}

	public static boolean useWordEnrichmentSmoothingFeaturesWithConsistencyCheck(ConfigFile configFile) {
		boolean useWordEnrichmentSmoothingFeatures = GrammarCreaterHats.useWordEnrichmentSmoothingGrammarFeatures(configFile);
		boolean useWordEnrichment = EnrichedSourceFileCreatorFactory.useSourceEnrichment(configFile);

		if (useWordEnrichmentSmoothingFeatures) {
			if (useWordEnrichment) {
				return true;
			} else {
				throw new RuntimeException(
						"Error: Featuresetings.useWordEnrichmentSmoothingFeaturesWithConsistencyCheck - Inconsistent configuration file: trying to use wordEnrichmentSmoothingFeatures without actually using wordEnrichment");
			}
		}
		return false;
	}

	public static FeatureSettings createFeatureSettingsStandard() {
		return new FeatureSettings(true, false, LABEL_SMOOTHING_TYPE.FULL_LABEL_SMOOTHING, true, null);
	}

	public static FeatureSettings createFeatureSettingsWithoutSmoothing() {
		return new FeatureSettings(true, false, LABEL_SMOOTHING_TYPE.NO_LABEL_SMOOTHING, true, null);
	}

	public static FeatureSettings createFeatureSettings(ConfigFile configFile, ReorderLabelingSettings reorderLabelingSettings,FeatureValueFormatter featureValueFormatter) {
		boolean useExtraSamtFeatures = configFile.getBooleanIfPresentOrReturnDefault(SAMTGrammarExtractionConstraints.USE_EXTRA_SAMT_FEATURES_PROPERTY, false);
		boolean useReorderingLabelBinaryFeatures = MTGrammarExtractionConfig.useReorderingLabelBinaryFeaturesWithConsistencyCheck(configFile);
		boolean useWordEnrichmentSmoothingFeatures = useWordEnrichmentSmoothingFeaturesWithConsistencyCheck(configFile);
		LABEL_SMOOTHING_TYPE labelSmoothing_TYPE = LabeledRulesSmoother.getLabelSmoothingType(configFile);
		boolean stripSourcePartLabelsForSmoothingDoubleLabeldRules = LabeledRulesSmoother.stripSourcePartLabelsForSmoothingDoubleLabeldRules(configFile);
		ReorderingFeatureSetCreater reorderingFeatureSetCreater = ReorderingFeatureSetCreater.createReorderingFeatureSetCreater(
				useReorderingLabelBinaryFeatures, configFile, reorderLabelingSettings,featureValueFormatter);
		return new FeatureSettings(useExtraSamtFeatures, useWordEnrichmentSmoothingFeatures, labelSmoothing_TYPE,
				stripSourcePartLabelsForSmoothingDoubleLabeldRules, reorderingFeatureSetCreater);
	}

	public boolean useExtraSamtFeatures() {
		return this.userExtraSamtFeatures;
	}

	public boolean useReorderingBinaryFeatures() {
		return (this.reorderingFeatureSetCreater != null);
	}

	public boolean useWordEnrichmentSmoothingFeatures() {
		return this.useWordEnrichmentSmoothingFeatures;
	}

	public LABEL_SMOOTHING_TYPE getLabelSmoothingType() {
		return this.labelSmoothingType;
	}

	public boolean stripSourcePartLabelsForSmoothingDoubleLabeldRules() {
		return this.stripSourcePartLabelsForSmoothingDoubleLabeldRules;
	}

	public ReorderingFeatureSetCreater getReorderingFeatureSetCreater() {
		return this.reorderingFeatureSetCreater;
	}
}
