package grammarExtraction.sourceEnrichment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;
import mt_pipeline.mt_config.MTConfigFile;
import mt_pipeline.preAndPostProcessing.EnrichedSourceFileCreator;

public class TagTripleEnrichedSourceFileCreator extends TagEnrichedSourceFileCreator implements EnrichedSourceFileCreator {

	public static final String SourceEnrichmentSchemeName = "posTagTripleEnrichedSource";
	private static String BeginSentenceTag = "{BeginSentence}";
	private static String EndSentenceTag = "{EndSentence}";

	protected TagTripleEnrichedSourceFileCreator(TagEnrichmentIOTriple tagEnrichmentIOQuadruple, int numLinesToCopy) {
		super(tagEnrichmentIOQuadruple, numLinesToCopy);
	}

	private static TagTripleEnrichedSourceFileCreator createTagTripleEnrichedSourceFileCreator(MTConfigFile theConfig, String category) {
		int numLinesToCopy = theConfig.decoderConfig.getDataLength(category);
		TagEnrichmentIOTriple tagEnrichmentIOTriple = TagEnrichmentIOTriple.createTagEnrichmentIOTriple(theConfig, category);
		return new TagTripleEnrichedSourceFileCreator(tagEnrichmentIOTriple, numLinesToCopy);
	}

	public static TagTripleEnrichedSourceFileCreator createTagTripleEnrichedSourceFileCreator() {
		return new TagTripleEnrichedSourceFileCreator(null, -1);
	}

	@Override
	public void createEnrichedSourceFile(MTConfigFile theConfig, String category) {
		TagTripleEnrichedSourceFileCreator tagTripleEnrichedSourceSubFileCreator = createTagTripleEnrichedSourceFileCreator(theConfig, category);
		tagTripleEnrichedSourceSubFileCreator.createTagEnrichedSourceSubFile();
	}

	protected String createTagTripleRepresentation(String previousTag, String tag, String nextTag) {
		return previousTag + "_" + tag + "_" + nextTag;
	}

	public static List<String> createBeginAndEndTagExtendedTagList(List<String> tags) {
		List<String> beginEndTagExtendedTags = new ArrayList<String>();
		beginEndTagExtendedTags.add(BeginSentenceTag);
		beginEndTagExtendedTags.addAll(tags);
		beginEndTagExtendedTags.add(EndSentenceTag);
		return beginEndTagExtendedTags;
	}

	public static List<String> createDoubleBeginAndEndTagExtendedTagList(List<String> tags) {
		List<String> beginEndTagExtendedTags = new ArrayList<String>();
		beginEndTagExtendedTags.add(BeginSentenceTag);
		beginEndTagExtendedTags.add(BeginSentenceTag);
		beginEndTagExtendedTags.addAll(tags);
		beginEndTagExtendedTags.add(EndSentenceTag);
		beginEndTagExtendedTags.add(EndSentenceTag);
		return beginEndTagExtendedTags;
	}
	
	@Override
	protected String createResultsLine(String sentence, String tagsLine) {
		String resultLine = "";
		String[] words = sentence.split(WhiteSpaceRegularExpression);
		String[] tags = tagsLine.split(WhiteSpaceRegularExpression);
		List<String> beginEndTagExtendedTags = createBeginAndEndTagExtendedTagList(Arrays.asList(tags));

		if (words.length != tags.length) {
			throw new RuntimeException("Number of tags does not match number of words in sentence");
		}

		for (int i = 0; i < words.length; i++) {
			String word = words[i];

			int previousWordTagIndex = i;
			int wordTagIndex = i + 1;
			int nextWordTagIndex = i + 2;
			String previousTag = JoshuaStyle.convertToJoshuaStyleLabel(beginEndTagExtendedTags.get(previousWordTagIndex));
			String tag = JoshuaStyle.convertToJoshuaStyleLabel(beginEndTagExtendedTags.get(wordTagIndex));
			String nextTag = JoshuaStyle.convertToJoshuaStyleLabel(beginEndTagExtendedTags.get(nextWordTagIndex));
			resultLine += word + getWordEnrichmentSeparator() + createTagTripleRepresentation(previousTag, tag, nextTag) + " ";
		}

		return resultLine;
	}

}
