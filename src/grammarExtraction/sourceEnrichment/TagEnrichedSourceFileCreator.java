package grammarExtraction.sourceEnrichment;

import java.io.IOException;

import mt_pipeline.preAndPostProcessing.EnrichedSourceFileCreator;

public abstract class TagEnrichedSourceFileCreator {

	protected static String WhiteSpaceRegularExpression = "\\s+";
	protected final TagEnrichmentIOTriple tagEnrichmentIOQuadruple;
	protected final int numLinesToCopy;

	protected TagEnrichedSourceFileCreator(TagEnrichmentIOTriple tagEnrichmentIOQuadruple, int numLinesToCopy) {
		this.tagEnrichmentIOQuadruple = tagEnrichmentIOQuadruple;
		this.numLinesToCopy = numLinesToCopy;
	}

	protected abstract String createResultsLine(String sentence, String tagsLine);

	public static String getWordEnrichmentSeparator() {
		return EnrichedSourceFileCreator.WORD_ENRICHMENT_SEPARATOR;
	}

	protected void createTagEnrichedSourceSubFile() {
		String sentence, tagsLine;

		System.out.println("createTagEnrichedSourceSubFile - numLinesToCopy: " + numLinesToCopy);

		int i = 0;
		try {
			while ((i < numLinesToCopy) && ((sentence = tagEnrichmentIOQuadruple.sentenceReader.readLine()) != null) && ((tagsLine = tagEnrichmentIOQuadruple.tagReader.readLine()) != null)) {
				String line = createResultsLine(sentence, tagsLine) + "\n";
				tagEnrichmentIOQuadruple.outputWriter.write(line);
				i++;
			}
			tagEnrichmentIOQuadruple.outputWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

}
