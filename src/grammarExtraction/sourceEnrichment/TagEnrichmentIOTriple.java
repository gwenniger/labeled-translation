package grammarExtraction.sourceEnrichment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import mt_pipeline.mt_config.MTConfigFile;

public class TagEnrichmentIOTriple {

	protected final BufferedReader sentenceReader;
	protected final BufferedReader tagReader;
	protected final BufferedWriter outputWriter;
	;

	protected TagEnrichmentIOTriple(BufferedReader sentenceReader, BufferedReader tagReader, BufferedWriter outputWriter) {
		this.sentenceReader = sentenceReader;
		this.tagReader = tagReader;
		this.outputWriter = outputWriter;
	}

	protected static TagEnrichmentIOTriple createTagEnrichmentIOTriple(MTConfigFile theConfig, String category) {
		String sentencFileName = theConfig.taggerConfig.getEnrichmentPairerSourceInputFilePath(category);
		String tagFileName = theConfig.taggerConfig.getTaggerSourceOutputFileName(category);
		String outFileName = theConfig.filesConfig.getEnrichedSourceFilePath(category);
		try {
			BufferedReader sentenceReader = new BufferedReader(new FileReader(sentencFileName));
			BufferedReader tagReader = new BufferedReader(new FileReader(tagFileName));
			BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outFileName));

			return new TagEnrichmentIOTriple(sentenceReader, tagReader, outputWriter);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
}
