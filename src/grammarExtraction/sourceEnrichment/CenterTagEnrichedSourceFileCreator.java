package grammarExtraction.sourceEnrichment;

import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;

import mt_pipeline.mt_config.MTConfigFile;
import mt_pipeline.preAndPostProcessing.EnrichedSourceFileCreator;

public class CenterTagEnrichedSourceFileCreator extends TagEnrichedSourceFileCreator implements EnrichedSourceFileCreator {

	public static final String SourceEnrichmentSchemeName = "posTagEnrichedSource";

	protected CenterTagEnrichedSourceFileCreator(TagEnrichmentIOTriple tagEnrichmentIOTriple , int numLinesToCopy) {
		super(tagEnrichmentIOTriple, numLinesToCopy);
	}

	private static CenterTagEnrichedSourceFileCreator createCenterTagEnrichedSourceSubFileCreator(MTConfigFile theConfig, String category) {
		int numLinesToCopy = theConfig.decoderConfig.getDataLength(category);
		TagEnrichmentIOTriple tagEnrichmentIOTriple = TagEnrichmentIOTriple.createTagEnrichmentIOTriple(theConfig, category);
		return new CenterTagEnrichedSourceFileCreator(tagEnrichmentIOTriple, numLinesToCopy);
	}

	public static CenterTagEnrichedSourceFileCreator createCenterTagEnrichedSourceFileCreator() {
		return new CenterTagEnrichedSourceFileCreator(null, -1);
	}

	@Override
	protected String createResultsLine(String sentence, String tagsLine) {
		String resultLine = "";
		String[] words = sentence.split(WhiteSpaceRegularExpression);
		String[] tags = tagsLine.split(WhiteSpaceRegularExpression);

		if (words.length != tags.length) {
			throw new RuntimeException("Number of tags does not match number of words in sentence");
		}

		for (int i = 0; i < words.length; i++) {
			String word = words[i];
			String tag = JoshuaStyle.convertToJoshuaStyleLabel(tags[i]);
			resultLine += word + getWordEnrichmentSeparator() + tag + " ";
		}

		return resultLine;
	}

	@Override
	public void createEnrichedSourceFile(MTConfigFile theConfig, String category) {
		CenterTagEnrichedSourceFileCreator tagEnrichedSourceSubFileCreator = createCenterTagEnrichedSourceSubFileCreator(theConfig, category);
		tagEnrichedSourceSubFileCreator.createTagEnrichedSourceSubFile();

	}

}
