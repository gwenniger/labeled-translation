package grammarExtraction.coreContextLabelGrammarExtraction;

import junit.framework.Assert;
import alignmentStatistics.InputStringsEnumeration;
import alignmentStatistics.MultiThreadComputationParameters;
import coreContextLabeling.CoreContextLabelsTable;
import coreContextLabeling.EbitgChartBuilderCoreContextLabeled;
import coreContextLabeling.EbitgLexicalizedInferenceCoreContextLabeled;
import grammarExtraction.chiangGrammarExtraction.GrammarExtractionConstraints;
import grammarExtraction.chiangGrammarExtraction.HierarchicalPhraseBasedRuleExtractor;
import grammarExtraction.grammarExtractionFoundation.RuleExtracter;
import grammarExtraction.samtGrammarExtraction.SAMTGrammarExtractionConstraints;
import grammarExtraction.samtGrammarExtraction.SAMTQuadruple;
import grammarExtraction.samtGrammarExtraction.SAMTQuadrupleEnumeration;

public class CCLRuleExtracter extends RuleExtracter<EbitgLexicalizedInferenceCoreContextLabeled, SAMTQuadruple> {

	private final CoreContextLabelsTable coreContextLabelsTable;
	private final CCLRuleGapCreater cclRuleGapCreater;

	protected CCLRuleExtracter(GrammarExtractionConstraints grammarExtractionConstraints, int maxAllowedInferencesPerNode, boolean useGreedyNullBinding,
			boolean useCaching, CoreContextLabelsTable coreContextLabelsTable, CCLRuleGapCreater cclRuleGapCreater) {
		super(grammarExtractionConstraints, maxAllowedInferencesPerNode, useCaching);
		this.coreContextLabelsTable = coreContextLabelsTable;
		this.cclRuleGapCreater = cclRuleGapCreater;
	}

	public static CCLRuleExtracter createCCLRuleExtracter(SAMTGrammarExtractionConstraints chiangGrammarExtractionConstraints, int maxAllowedInferencesPerNode,
			boolean useGreedyNullBinding, boolean useCaching, CoreContextLabelsTable coreContextLabelsTable, CCLRuleGapCreater cclRuleGapCreater) {
		return new CCLRuleExtracter(chiangGrammarExtractionConstraints, maxAllowedInferencesPerNode, useGreedyNullBinding, useCaching, coreContextLabelsTable,
				cclRuleGapCreater);
	}

	public static RuleExtracter<EbitgLexicalizedInferenceCoreContextLabeled, SAMTQuadruple> createCCLRuleExtracter(MultiThreadComputationParameters parameters,
			SAMTGrammarExtractionConstraints samtGrammarExtractionConstraints, CoreContextLabelsTable coreContextLabelsTable,
			CCLRuleGapCreater cclRuleGapCreater) {
		return createCCLRuleExtracter(samtGrammarExtractionConstraints, parameters.getMaxAllowedInferencesPerNode(), true, parameters.getUseCaching(),
				coreContextLabelsTable, cclRuleGapCreater);
	}

	@Override
	public HierarchicalPhraseBasedRuleExtractor<EbitgLexicalizedInferenceCoreContextLabeled> createTranslationRuleExtractor(SAMTQuadruple inputType) {
		return HierarchicalPhraseBasedRuleExtractor.createHierarchicalPhraseBasedRulesExtractorChiangCoreContexLabeled(
				createChartBuilderCCL(inputType.getSourceString(), inputType.getTargetString(), inputType.getAlignmentString(),
						inputType.getTargetParseString()), grammarExtractionConstraints, new CCLRuleCreater(this.cclRuleGapCreater));
	}

	@Override
	public InputStringsEnumeration<SAMTQuadruple> createInputStringEnumeration(MultiThreadComputationParameters parameters) {
		return SAMTQuadrupleEnumeration.createSAMTQuadrupleEnumeration(parameters.getSourceFileName(), parameters.getTargetFileName(),
				parameters.getAlignmentsFileName(), parameters.getTargetParseFileNameChecked());
	}

	protected EbitgChartBuilderCoreContextLabeled createChartBuilderCCL(String sourceString, String targetString, String alignmentString, String targetParse) {

		EbitgChartBuilderCoreContextLabeled ebitgChartBuilder = EbitgChartBuilderCoreContextLabeled.createEbitgChartBuilderCoreContextLabeled(sourceString,
				targetString, alignmentString, maxAllowedInferencesPerNode, true, useCaching, coreContextLabelsTable, createTargetCCGLabeler(targetParse));
		assert (maxAllowedInferencesPerNode > 10000);

		boolean result = ebitgChartBuilder.findDerivationsAlignment();
		Assert.assertEquals(true, result);
		return ebitgChartBuilder;
	}

}
