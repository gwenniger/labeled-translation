package grammarExtraction.coreContextLabelGrammarExtraction;

import ccgLabeling.FailTagLabelGenerator;
import util.Pair;
import coreContextLabeling.CoreContextLabel;
import coreContextLabeling.CoreContextLabelsTable;
import coreContextLabeling.EbitgLexicalizedInferenceCoreContextLabeled;
import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;
import grammarExtraction.samtGrammarExtraction.SAMTRuleGapCreater;
import grammarExtraction.translationRules.RuleGap;
import grammarExtraction.translationRules.RuleGapCreater;
import grammarExtraction.translationRules.RuleLabel;

public class CCLRuleGapCreater implements RuleGapCreater<EbitgLexicalizedInferenceCoreContextLabeled> {

	public static final String MaxiumAllowedCoreContextLabelAmbituityRankProperty = "maxiumAllowedCoreContextLabelAmbituityRank";
	private final CoreContextLabelsTable coreContextLabelsTable;
	private final int maxiumAllowedCoreContextLabelAmbituityRank;

	protected CCLRuleGapCreater(CoreContextLabelsTable coreContextLabelsTable, int maxiumAllowedCoreContextLabelAmbituityRank) {
		this.coreContextLabelsTable = coreContextLabelsTable;
		this.maxiumAllowedCoreContextLabelAmbituityRank = maxiumAllowedCoreContextLabelAmbituityRank;
	}

	private String getCoreContextLabelIfAmbiguityRankBelowMaximumOrReturnFailureLabel(CoreContextLabel coreContextLabel) {
		if (coreContextLabel.getAmbiguityScoreRank() <= maxiumAllowedCoreContextLabelAmbituityRank) {
			return JoshuaStyle.convertToJoshuaStyleLabel(coreContextLabelsTable.getCoreContextLabelWordString(coreContextLabel));
		} else {
			return FailTagLabelGenerator.FailLabelString;
		}
	}

	@SuppressWarnings("unused")
	private String getSAMTLabelIfAmbiguityRankBelowMaximumOrReturnFailureLabel(CoreContextLabel coreContextLabel, EbitgLexicalizedInferenceCoreContextLabeled inference) {
		if (coreContextLabel.getAmbiguityScoreRank() <= maxiumAllowedCoreContextLabelAmbituityRank) {
			return SAMTRuleGapCreater.creatSAMTLabel(inference);
		} else {
			return FailTagLabelGenerator.FailLabelString;
		}
	}

	protected String createCCLLabel(EbitgLexicalizedInferenceCoreContextLabeled inference) {
		CoreContextLabel coreContextLabel = inference.getCoreContextLabel();
		return getCoreContextLabelIfAmbiguityRankBelowMaximumOrReturnFailureLabel(coreContextLabel);
		// Temporary hack to test SAMT labels only at best-rank CCL positions
		// return getSAMTLabelIfAmbiguityRankBelowMaximumOrReturnFailureLabel(coreContextLabel, inference);
	}

	private String createCCLGapLabel(EbitgLexicalizedInferenceCoreContextLabeled gapInference) {
		return createCCLLabel(gapInference);
	}

	public static CCLRuleGapCreater createCCLRuleGapCreater(CoreContextLabelsTable coreContextLabelsTable, int maxiumAllowedAmbituityRank) {
		return new CCLRuleGapCreater(coreContextLabelsTable, maxiumAllowedAmbituityRank);
	}


	@Override
	public RuleGap creatLexicalizedHieroRuleGap(Pair<Integer> relativeSourceSpan, Pair<Integer> relativeTargetSpan, int gapNumber,
			EbitgLexicalizedInferenceCoreContextLabeled gapInference) {
		return new RuleGap(relativeSourceSpan, relativeTargetSpan, gapNumber, RuleLabel.createBothSidesSameRuleLabel(createCCLGapLabel(gapInference)));
	}

	@Override
	public RuleGap createAbstractRuleGap(Pair<Integer> relativeSourceSpan, Pair<Integer> relativeTargetSpan, int gapNumber,
			EbitgLexicalizedInferenceCoreContextLabeled gapInference) {
		return new RuleGap(relativeSourceSpan, relativeTargetSpan, gapNumber, RuleLabel.createBothSidesSameRuleLabel(createCCLGapLabel(gapInference)));
	}

}
