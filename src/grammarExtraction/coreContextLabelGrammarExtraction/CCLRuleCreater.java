package grammarExtraction.coreContextLabelGrammarExtraction;

import java.util.ArrayList;

import coreContextLabeling.EbitgLexicalizedInferenceCoreContextLabeled;
import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;
import grammarExtraction.translationRules.RuleGap;
import grammarExtraction.translationRules.RuleGapCreater;
import grammarExtraction.translationRules.RuleLabel;
import grammarExtraction.translationRules.TranslationRule;
import grammarExtraction.translationRules.TranslationRuleCreater;
import grammarExtraction.translationRules.TranslationRuleLexicalProperties;
import hat_lexicalization.Lexicalizer;

public class CCLRuleCreater implements TranslationRuleCreater<EbitgLexicalizedInferenceCoreContextLabeled> {

	private final RuleGapCreater<EbitgLexicalizedInferenceCoreContextLabeled> cclRuleGapCreater;

	protected CCLRuleCreater(RuleGapCreater<EbitgLexicalizedInferenceCoreContextLabeled> cclRuleGapCreater) {
		this.cclRuleGapCreater = cclRuleGapCreater;
	}

	@Override
	public TranslationRule<EbitgLexicalizedInferenceCoreContextLabeled> createBaseTranslationRuleLexicalizedHieroRules(Lexicalizer lexicalizer, EbitgLexicalizedInferenceCoreContextLabeled ruleInference) {
		return createCCLTranslationRule(lexicalizer, ruleInference);
	}

	private String createCCLRuleLabel(EbitgLexicalizedInferenceCoreContextLabeled ruleInference) {
		assert (ruleInference.getCCGLabels().hasTargetLabel());
		return JoshuaStyle.createJoshuaStyleRuleLabel(((CCLRuleGapCreater) this.cclRuleGapCreater).createCCLLabel(ruleInference));
	}

	public TranslationRule<EbitgLexicalizedInferenceCoreContextLabeled> createCCLTranslationRule(Lexicalizer lexicalizer, EbitgLexicalizedInferenceCoreContextLabeled ruleInference) {
		return new TranslationRule<EbitgLexicalizedInferenceCoreContextLabeled>(RuleLabel.createBothSidesSameRuleLabel(createCCLRuleLabel(ruleInference)), TranslationRuleLexicalProperties.createTranslationRuleLexicalProperties(lexicalizer,
				ruleInference), new ArrayList<RuleGap>(), new ArrayList<RuleGap>(), this.cclRuleGapCreater);
	}

	@Override
	public TranslationRule<EbitgLexicalizedInferenceCoreContextLabeled> createBaseTranslationRuleWithPhrasePairLabeling(Lexicalizer lexicalizer,
			EbitgLexicalizedInferenceCoreContextLabeled ruleInference) {
		throw new RuntimeException("Error: un-implemented");
	}

	@Override
	public TranslationRule<EbitgLexicalizedInferenceCoreContextLabeled> createBaseTranslationRuleWithAbstractRuleLabeling(Lexicalizer lexicalizer,
			EbitgLexicalizedInferenceCoreContextLabeled ruleInference) {
		throw new RuntimeException("Error: un-implemented");
	}

}
