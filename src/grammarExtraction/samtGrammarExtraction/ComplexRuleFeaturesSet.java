package grammarExtraction.samtGrammarExtraction;

import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatter;


public abstract class ComplexRuleFeaturesSet {

	protected final FeatureValueFormatter featureValueFormatter;
	
	protected ComplexRuleFeaturesSet(FeatureValueFormatter featureValueFormatter) {
		this.featureValueFormatter = featureValueFormatter;
	}

}
