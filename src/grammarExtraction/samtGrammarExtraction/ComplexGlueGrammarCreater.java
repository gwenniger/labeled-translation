package grammarExtraction.samtGrammarExtraction;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import grammarExtraction.chiangGrammarExtraction.HieroGlueGrammarCreater;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.translationRuleTrie.GlueFeaturesCreater;
import grammarExtraction.translationRuleTrie.GrammarWriter;
import grammarExtraction.translationRuleTrie.LabelProbabilityEstimator;
import grammarExtraction.translationRuleTrie.TranslationRuleProbabilityTable;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

/**
 * This class creates a ComplexGlueGrammarCreater based on the nonterminal
 * symbols that are present in WordKeyMappingTable. Note that one tricky aspect
 * in dealing with Joshua is the Out of Vocabulary Symbols. JoshuaConfiguration
 * defines a parameter 'default_non_terminal' which is used for out of
 * Vocabulary words. If we do not change this parameter to correspond to a
 * symbol that actually \ occurs in the grammar, then things will not work
 * properly. There are at least three strategies that might work to deal with
 * this: 1. Also add the default Chiang rules, and keep the parameter to be
 * 'default_non_terminal=X' This option works most robustly, so we opt for this
 * one at the moment. 2. Look at specific symbol that actually occurs in the
 * grammar (e.g. NN+PP) and adapt the JoshuaConfiguration to use that symbol for
 * default_non_terminal. This strategy has the clear disadvantage that for SAMT
 * and other complex-label-grammars, it is a bit tricky to determine what labels
 * you will get beforehand. 3. Use default_non_terminal=S . For 'normal'
 * grammars that include the glue rule (that generates a new S on the right hand
 * side) this always works. On the other hand it is conceptually perhaps a bit
 * ugly to make S, which is also the start symbol, the default non terminal. We
 * tried this option which would be ideal, but it seems to not work properly for
 * the basic Hiero Grammar, so we avoid it, and option for option (1) instead.
 * 
 * @author gemaille
 * 
 */

public class ComplexGlueGrammarCreater extends GlueGrammarCreater {

	private ComplexGlueGrammarCreater(boolean writePlainHieroRules, SystemIdentity systemIdentity) {
		super(writePlainHieroRules, systemIdentity);
	}

	public static ComplexGlueGrammarCreater createComplexGlueGrammarCreater(boolean writePlainHieroRules, SystemIdentity systemIdentity) {
		return new ComplexGlueGrammarCreater(writePlainHieroRules,systemIdentity);
	}


	
	private List<Integer> getValidRuleLabels(WordKeyMappingTable wordKeyMappingTable) {
		List<Integer> result = new ArrayList<Integer>();
		result.addAll(wordKeyMappingTable.getComplexRuleLabels());
		if (writePlainHieroRules || systemIdentity.useAbstractRuleReorderingLabelingOnly()) {
			result.add(WordKeyMappingTable.BASIC_CHIANG_RULE_LABEL_KEY);
		}
		return result;
	}

	private List<String> createGlueRules(WordKeyMappingTable wordKeyMappingTable, LabelProbabilityEstimator labelProbabilityEstimator,
			GlueFeaturesCreater glueFeaturesCreater) {
		List<String> result = new ArrayList<String>();
		for (Integer complexRuleLabel : getValidRuleLabels(wordKeyMappingTable)) {
			double probability = labelProbabilityEstimator.getProbabilityForGlueRules(complexRuleLabel);
			double rarityPenalty = labelProbabilityEstimator.getRarityPenaltyLabel(complexRuleLabel);
			String complexRuleLabelWithoutBrackets = wordKeyMappingTable.getComplexRuleLabelWithoutEnclosingBrackets(complexRuleLabel);
			result.addAll(createGlueRulesForLabel(complexRuleLabelWithoutBrackets, probability, rarityPenalty, glueFeaturesCreater));
		}

		return result;
	}

	public List<String> createComplexGlueRules(WordKeyMappingTable wordKeyMappingTable, LabelProbabilityEstimator labelProbabilityEstimator,
			GlueFeaturesCreater glueFeaturesCreater) {
		List<String> result = new ArrayList<String>();
		result.addAll(createGlueRules(wordKeyMappingTable, labelProbabilityEstimator, glueFeaturesCreater));
		result.addAll(createStartAndEndRules(glueFeaturesCreater));
		return result;
	}

	@Override
	public void writeGlueGrammar(TranslationRuleProbabilityTable translationRuleProbabilityTable, String outputFileName, GlueFeaturesCreater glueFeaturesCreater) {
		List<String> complexGlueRules = createComplexGlueRules(translationRuleProbabilityTable.getWordKeyMappingTable(),
				translationRuleProbabilityTable.getLabelProbabilityEstimator(), glueFeaturesCreater);
		// (Add the Chiang glue rules, to deal with OOV (Out of Vocabulary)
		// words)
		// complexGlueRules.addAll(HieroGlueGrammarCreater.getChiangGlueRules());

		try {
			BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFileName));
			HieroGlueGrammarCreater.writeGlueRules(complexGlueRules, outputWriter);
			outputWriter.write("\n");
			GrammarWriter grammarWriter = GrammarWriter.createBasicGrammarWriter(translationRuleProbabilityTable, null, writePlainHieroRules, systemIdentity);
			if (translationRuleProbabilityTable.hasTries()) {
				grammarWriter.writeAbstractRulesToFile(outputWriter, writePlainHieroRules);
			}
			outputWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

}
