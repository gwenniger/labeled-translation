package grammarExtraction.samtGrammarExtraction;

import java.util.ArrayList;
import extended_bitg.EbitgLexicalizedInference;
import grammarExtraction.chiangGrammarExtraction.BasicRuleGapCreater;
import grammarExtraction.chiangGrammarExtraction.BasicTranslationRuleCreater;
import grammarExtraction.chiangGrammarExtraction.ReorderLabelingSettings;
import grammarExtraction.translationRules.RuleGap;
import grammarExtraction.translationRules.RuleLabelCreater;
import grammarExtraction.translationRules.TranslationRule;
import grammarExtraction.translationRules.TranslationRuleLexicalProperties;
import hat_lexicalization.Lexicalizer;

public class SAMTRuleCreater extends BasicTranslationRuleCreater<EbitgLexicalizedInference, BasicRuleGapCreater<EbitgLexicalizedInference>> {

	private SAMTRuleCreater(BasicRuleGapCreater<EbitgLexicalizedInference> basicRuleGapCreater) {
		super(basicRuleGapCreater);
	}

	public static SAMTRuleCreater createSAMTRuleCreater(ReorderLabelingSettings reorderLabelingSettings,RuleLabelCreater ruleLabelCreater) {
		return new SAMTRuleCreater((BasicRuleGapCreater<EbitgLexicalizedInference>) SAMTRuleGapCreater.createSAMTRuleGapCreater(reorderLabelingSettings,ruleLabelCreater));
	}

	public TranslationRule<EbitgLexicalizedInference> createSAMTTranslationRule(Lexicalizer lexicalizer, EbitgLexicalizedInference ruleInference) {
		return new TranslationRule<EbitgLexicalizedInference>(basicRuleGapCreater.createRuleLabelFromLabelString(SAMTRuleGapCreater.creatSAMTLabel(ruleInference)),
				TranslationRuleLexicalProperties.createTranslationRuleLexicalProperties(lexicalizer, ruleInference), new ArrayList<RuleGap>(), new ArrayList<RuleGap>(), this.basicRuleGapCreater);
	}

	@Override
	public TranslationRule<EbitgLexicalizedInference> createBaseTranslationRuleLexicalizedHieroRules(Lexicalizer lexicalizer, EbitgLexicalizedInference ruleInference) {
		assert (lexicalizer.hasNonEmptyCCGLabeler());
		return createSAMTTranslationRule(lexicalizer, ruleInference);
	}

}
