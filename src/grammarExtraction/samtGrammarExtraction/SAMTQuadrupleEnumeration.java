package grammarExtraction.samtGrammarExtraction;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;

import mt_pipeline.parserInterface.BerkeleyParseFormatToWSJFormatConverter;



import alignmentStatistics.InputStringsEnumeration;

public class SAMTQuadrupleEnumeration extends InputStringsEnumeration<SAMTQuadruple>
{

	protected SAMTQuadrupleEnumeration(List<BufferedReader> inputReaders) 
	{
		super(inputReaders);
	}

	public static SAMTQuadrupleEnumeration createSAMTQuadrupleEnumeration(String sourceFileName, String targetFileName, String alignmentsFileName, String targetParsesFileName)
	{
		try 
		{
			BufferedReader sourceReader = new BufferedReader(new FileReader(sourceFileName));
			BufferedReader targetReader = new BufferedReader(new FileReader(targetFileName));
			BufferedReader alignmentsReader = new BufferedReader(new FileReader(alignmentsFileName));
			BufferedReader targetParsesReader = new BufferedReader(new FileReader(targetParsesFileName));
			List<BufferedReader> inputReaders = Arrays.asList(sourceReader,targetReader,alignmentsReader,targetParsesReader);
			return new SAMTQuadrupleEnumeration(inputReaders);
		} catch (FileNotFoundException e) 
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
	}
	
	@Override
	protected SAMTQuadruple getNextT(List<String> nextInputStrings) 
	{
		assert(nextInputStrings.size() == 4);
		return SAMTQuadruple.createSAMTQuadruple(nextInputStrings.get(0), nextInputStrings.get(1), nextInputStrings.get(2),nextInputStrings.get(3));
	}

	@Override
	protected boolean inputStringsAreAcceptable(List<String> nextInputStrings) {
		return !(nextInputStrings.get(3).equals(BerkeleyParseFormatToWSJFormatConverter.ParseErrorString));
	}

}
