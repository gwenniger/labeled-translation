package grammarExtraction.samtGrammarExtraction;

import grammarExtraction.translationRuleTrie.AtomicDouble;
import grammarExtraction.translationRuleTrie.LabelProbabilityEstimator;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;

public class LabelProbabilityEstimatorSAMT extends LabelProbabilityTable implements
	LabelProbabilityEstimator, Serializable {

    private static final long serialVersionUID = 1L;
    private final boolean useUniformGlueRuleWeights;

    private LabelProbabilityEstimatorSAMT(
	    ConcurrentHashMap<Integer, AtomicDouble> labelCountsTable, AtomicDouble totalCounts,
	    boolean useUniformGlueRuleWeights) {
	super(labelCountsTable, totalCounts);
	this.useUniformGlueRuleWeights = useUniformGlueRuleWeights;
    }

    public static LabelProbabilityEstimatorSAMT createLabelProbabilityEstimatorSAMT(boolean useUniformGlueRuleWeights) {
	System.out.println(">>> createLabelProbabilityEstimatorSAMT");
	ConcurrentHashMap<Integer, AtomicDouble> labelCountsTable = new ConcurrentHashMap<Integer, AtomicDouble>();
	return new LabelProbabilityEstimatorSAMT(labelCountsTable, new AtomicDouble(0),useUniformGlueRuleWeights);
    }

    @Override
    public double getRarityPenaltyLabel(Integer labelKey) {
	if(useUniformGlueRuleWeights){
	    return 0;
	}
	
	double ruleCorpusCount;
	if (labelCountsTable.containsKey(labelKey)) {
	    ruleCorpusCount = labelCountsTable.get(labelKey).doubleValue();
	} else {
	    ruleCorpusCount = 0;
	}
	return LabeledRuleFeaturesComputer.computeRarityPenalty(ruleCorpusCount);
    }

    /**
     * We use a separate method for the probability for glue rules, so we can use the 
     * flag useUniformGlueRuleWeights to assign all probabilities uniformly to 1.
     * In this case the method getProbability(Integer labelKey) can still be used to get the
     * actual probabilities of labels, used for example for sorting labels by their probability
     * when producing rules for UnknownWords for only the most likely labels.
     */
    @Override
    public double getProbabilityForGlueRules(Integer labelKey) {
	if(useUniformGlueRuleWeights){
	    return 1;
	}
	return super.getProbability(labelKey);
    }
    
	@Override
	public void showContentsForDebugging(WordKeyMappingTable wordKeyMappingTable) {
	    String resultString ="<LabelPrbabilityEstimatorSAMT>\n";
	    resultString += super.getContentsStringWithCounts(wordKeyMappingTable);
	    resultString +="</LabelPrbabilityEstimatorSAMT>\n";
	    System.out.println(resultString);
	    
	}
    
}
