package grammarExtraction.samtGrammarExtraction;

import grammarExtraction.translationRuleTrie.FeatureEnrichedGrammarRuleCreator;

import java.util.List;

public interface UnknownWordRulesCreator {
	public List<String> createUnknownWordsRules(FeatureEnrichedGrammarRuleCreator featureEnrichedGrammarRuleCreator);
}
