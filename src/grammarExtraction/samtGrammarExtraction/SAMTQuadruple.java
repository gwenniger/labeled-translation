package grammarExtraction.samtGrammarExtraction;

import alignment.AlignmentStringTriple;

public class SAMTQuadruple extends AlignmentStringTriple 
{
	final String targetParseString;
	
	protected SAMTQuadruple(String sourceString, String targetString, String alignmentString, String targetParseString) 
	{
		super(sourceString, targetString, alignmentString);
		this.targetParseString = targetParseString;
	}

	public static SAMTQuadruple createSAMTQuadruple(String sourceString, String targetString, String alignmentString, String targetParseString)
	{
		sourceString = sourceString.trim();
		targetString = targetString.trim();
		alignmentString = alignmentString.trim();
		return new SAMTQuadruple(sourceString, targetString, alignmentString,targetParseString);
	}
	
	public String getTargetParseString()
	{
		return this.targetParseString;
	}
}
