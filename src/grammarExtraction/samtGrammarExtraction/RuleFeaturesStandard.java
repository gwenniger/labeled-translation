package grammarExtraction.samtGrammarExtraction;

import grammarExtraction.chiangGrammarExtraction.BasicRuleFeaturesSet;
import grammarExtraction.chiangGrammarExtraction.RuleFeaturesBasic;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatter;
import grammarExtraction.phraseProbabilityWeights.NamedFeature;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsSet;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.TranslationRuleFeatures;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.List;

import mt_pipeline.mt_config.MTConfigFile;
import util.ConfigFile;

public class RuleFeaturesStandard extends RuleFeaturesExtended {

	protected RuleFeaturesStandard(BasicRuleFeaturesSet basicRuleFeaturesSet, LabeledRuleFeaturesSet labeledRuleFeaturesSet) {
		super(basicRuleFeaturesSet, labeledRuleFeaturesSet);
	}

	public static RuleFeaturesStandard createRuleFeaturesStandard(double ruleGivenLabelGenerativeProbability,
			PhraseProbabilityWeightsSet phraseProbabilityWeightsSet, double lexicalWeightSourceToTarget, double lexicalWeightSourceGivenTarget,
			double ruleCorpusCount, boolean isHieroRule, boolean isHieroGlueRule, boolean isSyntacticGlueRule,
			TranslationRuleSignatureTriple translationRuleSignatureTriple, WordKeyMappingTable wordKeyMappingTable, FeatureValueFormatter featureValueFormatter) {
		BasicRuleFeaturesSet basicRuleFeaturesSet = BasicRuleFeaturesSet.createBasicRuleFeatures(phraseProbabilityWeightsSet, lexicalWeightSourceToTarget,
				lexicalWeightSourceGivenTarget, featureValueFormatter);
		LabeledRuleFeaturesSet labeledRuleFeaturesSet = LabeledRuleFeaturesSet.createLabeledRuleFeaturesSet(featureValueFormatter, ruleCorpusCount,
				isHieroRule, isHieroGlueRule, isSyntacticGlueRule, translationRuleSignatureTriple, ruleGivenLabelGenerativeProbability, wordKeyMappingTable);
		return new RuleFeaturesStandard(basicRuleFeaturesSet, labeledRuleFeaturesSet);
	}

	public static RuleFeaturesStandard createCorrectSizeUnknnownWordRuleRuleFeaturesStandard(SystemIdentity systemIdentity, boolean isHieroRule) {
		return new RuleFeaturesStandard(RuleFeaturesBasic.getCorrectSizeBasicRuleFeaturesSetUniformValues(systemIdentity,
				UnknownWordRulesCreatorPosTags.UNKNOWN_WORD_PROBABILITY), LabeledRuleFeaturesSet.getExtraLabeledRuleFeaturesUnknownWordRule(systemIdentity,
				isHieroRule));
	}

	public static RuleFeaturesStandard createCorrectSizeReorderingLabelToPhraseLabelSwitchRuleFeaturesStandard(SystemIdentity systemIdentity) {
		return new RuleFeaturesStandard(RuleFeaturesBasic.getCorrectSizeBasicRuleFeaturesSetUniformValues(systemIdentity, 1),
				LabeledRuleFeaturesSet.getExtraLabeledRuleFeaturesReorderingSwitchRuleRule(systemIdentity));
	}

	public static RuleFeaturesStandard createCorrectSizeGlueRuleFeaturesStandard(SystemIdentity systemIdentity, double generativeGlueLabelProbability,
			boolean isHieroRule, double rarityPenalty) {
		return new RuleFeaturesStandard(RuleFeaturesBasic.getCorrectSizeBasicRuleFeaturesSetUniformValues(systemIdentity, 1),
				LabeledRuleFeaturesSet.createGlueRuleLabeledRuleFeatureSet(systemIdentity.getFeatureValueFormatter(), generativeGlueLabelProbability,
						isHieroRule, rarityPenalty));
	}

	public static RuleFeaturesStandard createRuleFeaturesStandardZeroValues(SystemIdentity systemIdentity) {
		return new RuleFeaturesStandard(RuleFeaturesBasic.getCorrectSizeBasicRuleFeaturesSetUniformValues(systemIdentity, 1),
				LabeledRuleFeaturesSet.createGlueRuleLabeledRuleFeatureSet(systemIdentity.getFeatureValueFormatter(), 0, false, 0));
	}

	public static boolean useExtraSAMTFeatures(MTConfigFile theConfig) {

		return theConfig.getBooleanWithPresenceCheck(SAMTGrammarExtractionConstraints.USE_EXTRA_SAMT_FEATURES_PROPERTY);
	}

	@Override
	public List<Float> getDenseFeatureValues() {
		List<Float> result = basicRuleFeatures.getFeatureValues();
		result.addAll(this.labeledRuleFeatures.getFeatureValues());
		return result;
	}

	@Override
	public String getCompressedFeaturesRepresentation() {
		return NamedFeature.createLabeledFeatureStringListJoshuaFormat(getNamedFeaturesList());
	}

	@Override
	public List<NamedFeature> getNamedFeaturesList() {
		return getStandardFeaturesList();
	}

	/*
	 * public static List<NamedFeature>
	 * getOrderedRepresentativeNamedFeatures(SystemIdentity systemIdentity) {
	 * List<NamedFeature> result = new ArrayList<NamedFeature>();
	 * result.addAll(RuleFeaturesBasic
	 * .getRepresentativeBasicZeroNamedFeaturesForConfiguration
	 * (systemIdentity));
	 * result.addAll(LabeledRuleFeaturesSet.getRepresentativeZeroNamedFeatures
	 * ()); return result; }
	 */

	@Override
	public List<NamedFeature> getNamedFeaturesListSparse() {
		return getStandardFeaturesList();
	}

	@Override
	public List<NamedFeature> getDiscriminativeNamedFeaturesList() {
		return Collections.emptyList();
	}

	@Override
	public List<NamedFeature> getDenseNamedFeaturesList() {
		return getStandardFeaturesList();
	}

	@Override
	public TranslationRuleFeatures createCopyWithScaledProbabilities(double scalingFactor) {
		return new RuleFeaturesStandard(this.basicRuleFeatures.createScaledCopyBasicRuleFeatures(scalingFactor), this.labeledRuleFeatures);
	}
}
