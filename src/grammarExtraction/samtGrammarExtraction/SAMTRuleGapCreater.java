package grammarExtraction.samtGrammarExtraction;

import util.XMLTagging;
import extended_bitg.EbitgLexicalizedInference;
import grammarExtraction.chiangGrammarExtraction.BasicRuleGapCreater;
import grammarExtraction.chiangGrammarExtraction.ReorderLabelingSettings;
import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;
import grammarExtraction.translationRules.LightWeightTranslationRule;
import grammarExtraction.translationRules.RuleGapCreater;
import grammarExtraction.translationRules.RuleLabelCreater;

public class SAMTRuleGapCreater extends BasicRuleGapCreater<EbitgLexicalizedInference> {

	protected SAMTRuleGapCreater(ReorderLabelingSettings reorderLabelingSettings,RuleLabelCreater ruleLabelCreater) {
		super(reorderLabelingSettings,ruleLabelCreater);
	}

	public static String creatSAMTLabel(EbitgLexicalizedInference inference) {
		return JoshuaStyle.convertToJoshuaStyleLabel(inference.getCCGLabels().getCCGTargetLabelString());
	}

	public static RuleGapCreater<EbitgLexicalizedInference> createSAMTRuleGapCreater(ReorderLabelingSettings reorderLabelingSettings,RuleLabelCreater ruleLabelCreater) {
		return new SAMTRuleGapCreater(reorderLabelingSettings,ruleLabelCreater);
	}

	/*
	@Override
	protected String  getLabelGapLexicalizedHieroRule(EbitgLexicalizedInference inference) {
		if (reorderLabelLexicalizedHieroRules()) {
			return XMLTagging.getTagSandwidchedString(LightWeightTranslationRule.SOURCE_LABELS, getReorderingLabelExtension(inference))
					+ XMLTagging.getTagSandwidchedString(LightWeightTranslationRule.TARGET_LABELS, creatSAMTLabel(inference));
		} else {
			return creatSAMTLabel(inference);
		}
	}

	@Override
	protected String getLabelGapAbstractRule(EbitgLexicalizedInference inference) {
		if (getReorderLabelingSettings().useReorderingLabelExtension()) {
			return XMLTagging.getTagSandwidchedString(LightWeightTranslationRule.SOURCE_LABELS, getReorderingLabelExtension(inference))
					+ XMLTagging.getTagSandwidchedString(LightWeightTranslationRule.TARGET_LABELS, creatSAMTLabel(inference));
		} else {
			return creatSAMTLabel(inference);
		}
	}*/
	
	@Override
	protected String getReorderingLabelPhrasePairLabel(EbitgLexicalizedInference inference) {
		return XMLTagging.getTagSandwidchedString(LightWeightTranslationRule.SOURCE_LABELS, getPhrasePairReorderingLabelExtension())
				+ XMLTagging.getTagSandwidchedString(LightWeightTranslationRule.TARGET_LABELS, creatSAMTLabel(inference));
	}

	@Override
	protected String getBasicLabel(EbitgLexicalizedInference inference) {
		return creatSAMTLabel(inference);
	}



}
