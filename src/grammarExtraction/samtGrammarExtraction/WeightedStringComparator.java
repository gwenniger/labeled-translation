package grammarExtraction.samtGrammarExtraction;

import java.util.Comparator;

import bitg.Pair;

public class WeightedStringComparator implements Comparator<Pair<String, Double>> {

    @Override
    public int compare(Pair<String, Double> o1, Pair<String, Double> o2) {
	return Double.compare((Double) o1.last, (Double) o2.last);
    }
}
