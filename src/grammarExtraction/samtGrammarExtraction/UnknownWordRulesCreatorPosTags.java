package grammarExtraction.samtGrammarExtraction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import bitg.Pair;
import grammarExtraction.chiangGrammarExtraction.HieroGlueGrammarCreater;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.translationRuleTrie.FeatureEnrichedGrammarRuleCreator;
import grammarExtraction.translationRuleTrie.LabelProbabilityEstimator;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.TranslationRuleFeatures;

public class UnknownWordRulesCreatorPosTags implements UnknownWordRulesCreator {

	// This is an ad-hoc solution. Something like proper good-turing smoothing
	// would be
	// a better idea. On the other hand it does not seem too important what
	// value we choose
	// if we are not seriously trying to translate unknown words anyway (Joshua
	// just leaves
	// them as is by default or translates them with some special marker).
	// ONE THING IS IMPORTANT HOWEVER
	// We should not set the (Joshua) weights to 0 (= probability 1). Since if
	// we do that, after
	// normalization, the weights for the actual rules that have been estimated
	// using proper
	// maximum frequency estimation will be skewed. Simply using a very small
	// weight
	// uniformly for all unknown word productions should be a safe solution in
	// principle.

    	// The number of labels used for unknown word rules.
    	// For efficiency reasons we don't make unknown word rules for all labels
    	// but only for the most likely ones. Thanks to glue rules we can always 
    	// generate unknown words in any case, but having a unknown word rule 
    	// for each label becomes prohibitively expensive for big label sets, because 
    	// it means that any unknown word rule and thus also any glue rule (for each of the labels 
    	// in the label set) can be applied in any cell chart. This has a risk of 
    	// making the decoding very slow for e.g. SAMT which has such a huge label set, 
    	// but does not really add any advantage.
    	private static final int NUM_LABELS_USED_FOR_UNKNOWN_WORD_RULES = 3; // lowered from 10 to 3 to reduce complexity
    	
    	private static final String FUZZY_MATCHING_PRE_UNKNOWN_WORDN_NONTERMINAL_LABEL = "UnknownWordPreNonterminal";
    
	public static final double UNKNOWN_WORD_PROBABILITY = 1.0e-9;
	static final float UnknownWordPreTerminaWeightJoshua = -1.0f * (float) Math.log(UNKNOWN_WORD_PROBABILITY);

	private static final Pattern PosTagLabelPattern = Pattern.compile("([\\w_])*");
	private final WordKeyMappingTable wordKeyMappingTable;
	private final SystemIdentity systemIdentity;

	private UnknownWordRulesCreatorPosTags(WordKeyMappingTable wordKeyMappingTable, SystemIdentity systemIdentity) {
		this.wordKeyMappingTable = wordKeyMappingTable;
		this.systemIdentity = systemIdentity;
	}

	public static UnknownWordRulesCreatorPosTags createUnknownWordRulesCreatorPosTags(WordKeyMappingTable wordKeyMappingTable, SystemIdentity systemIdentity) {
		return new UnknownWordRulesCreatorPosTags(wordKeyMappingTable, systemIdentity);
	}

	private String getUnknownWordWeightsJoshua(boolean isHieroRule, FeatureEnrichedGrammarRuleCreator featureEnrichedGrammarRuleCreator) {

		TranslationRuleFeatures translationRuleFeatures = featureEnrichedGrammarRuleCreator.getUnknownWordsTranslationRuleFeatures(systemIdentity, isHieroRule);

		return systemIdentity.getCorrectFormatFeaturesRepresentation(translationRuleFeatures);
	}

	public static boolean isPosTag(String complexRuleLabel) {
		Matcher matcher = PosTagLabelPattern.matcher(complexRuleLabel);
		return matcher.matches();
	}

	private boolean systemIsMosesAndUsesPlainHieroRulesForSmoothing() {
		return systemIdentity.isMosesSystem() && systemIdentity.writePlainHieroRulesForSmoothing();
	}
	
	private List<String> getLeftHandSideLabelsSortedByLikelihood(FeatureEnrichedGrammarRuleCreator featureEnrichedGrammarRuleCreator){
	    	List<String> result = new ArrayList<String>();
	    	
	    	List<Pair<String,Double>> weightedLabelsList = new ArrayList<Pair<String,Double>>();
	    	LabelProbabilityEstimator labelProbabilityEstimator = featureEnrichedGrammarRuleCreator.getLabelProbabilityEstimator();
	    	
	    	for(String label : wordKeyMappingTable.getComplexRuleLabelsWithoutEnclosingBrackets()){
	    	    String labelWithBrackets = "[" + label + "]";
	    	    Integer labelKey = wordKeyMappingTable.getKeyForWord(labelWithBrackets);
	    	    Double labelProbability = labelProbabilityEstimator.getProbability(labelKey);
	    	    weightedLabelsList.add(new Pair<String,Double>(label,labelProbability));
	    	}
	    	
	    	// Sort the list of label-probability pairs 
	    	weightedLabelsList.sort(Collections.reverseOrder(new WeightedStringComparator()));
	    	int index = 0;
	    	System.out.println("Creating unknown word creation labels for the most frequent nonterminal labels...");
	    	for(Pair<String,Double> labelProbabilityPair : weightedLabelsList){
	    	    result.add(labelProbabilityPair.first);
	    	    System.out.println("Label: " + labelProbabilityPair.first + " Label index: " + index + " Weight(Relative Frequency): " + labelProbabilityPair.last);
	    	    index++;
	    	}
	    	return result;
	}

	@Override
	public List<String> createUnknownWordsRules(FeatureEnrichedGrammarRuleCreator featureEnrichedGrammarRuleCreator) {
		if (systemIsMosesAndUsesPlainHieroRulesForSmoothing() || systemIdentity.useAbstractRuleReorderingLabelingOnly()) {
			return Collections.emptyList();
		}
		
		/**
		 * Adding many unary rules that can create the unknown word symbol from different nonterminals
		 * is always more expensive than creating unknown words only with glue rules or only from 
		 * a single selected nonterminal.
		 * But allowing multiple nonterminals to generate unknown word rules is
		 * is really problematic and insensible in a soft matching soft matching setup.
		 * The reason is that these "artificial" nonterminals produced by the unknown word nonterminal
		 * producing rules are allowed again to softly match for all rules. This risks adding a 
		 * kind of unnecessary additional complexity to the decoder, which increases potentially 
		 * quadratically with the number of labels, if for every label a unknown word producing rule
		 * is added. 
		 * Instead of this madness, we simply add one "PreUnknownWordNonterminal" producing 
		 * unary rule. Then the PreUnknownWordNonterminal can match softly with the 
		 * nonterminal in any rule, while add the same time producing informative label
		 * substitution features that can help to learn what nonterminals should be relatively
		 * encouraged or discouraged to produce the unknown words of the sentence.  
		 */
		if(systemIdentity.useSingleUnknownWordPreterminalForEfficientFuzzyMatchingDecoding()){
		    return createUnknownWordsRulesForFuzzyMatching(featureEnrichedGrammarRuleCreator);
		}

		List<String> result = new ArrayList<String>();
		List<String> labelsSortedByLikelyhood = getLeftHandSideLabelsSortedByLikelihood(featureEnrichedGrammarRuleCreator); 
		// We use only the top most likely labels for unknown word rules
		int maxLabelIndex = Math.min(NUM_LABELS_USED_FOR_UNKNOWN_WORD_RULES, labelsSortedByLikelyhood.size());
		for (int i = 0; i < maxLabelIndex ; i++) {
			result.add(createUnknownWordPreTerminalRule(labelsSortedByLikelyhood.get(i), featureEnrichedGrammarRuleCreator));
		}
		return result;
	}

	public List<String> createUnknownWordsRulesForFuzzyMatching(FeatureEnrichedGrammarRuleCreator featureEnrichedGrammarRuleCreator){
	    return Collections.singletonList(createUnknownWordRuleForFuzzyMatching(featureEnrichedGrammarRuleCreator));
	}
	
	public String createUnknownWordRuleForFuzzyMatching(FeatureEnrichedGrammarRuleCreator featureEnrichedGrammarRuleCreator){
	    return createUnknownWordPreTerminalRule(FUZZY_MATCHING_PRE_UNKNOWN_WORDN_NONTERMINAL_LABEL, featureEnrichedGrammarRuleCreator);
	}
	
	
	
	public String createUnknownWordPreTerminalRule(String complexLabel, FeatureEnrichedGrammarRuleCreator featureEnrichedGrammarRuleCreator) {
		String result = systemIdentity.getTranslationRuleRepresentationCreater().getUnknownWordRuleForLabel(complexLabel,
				getUnknownWordWeightsJoshua(HieroGlueGrammarCreater.isHieroLabel(complexLabel), featureEnrichedGrammarRuleCreator));
		/*
		 * String nonterminalRightSide =
		 * RuleGap.ruleGapString(systemIdentity.getUnknownWordsLabel(), 0);
		 * String result = JoshuaStyle.createJoshuaStyleRuleLabel(complexLabel)
		 * + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR + nonterminalRightSide +
		 * JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR + nonterminalRightSide +
		 * JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR +
		 * getUnknownWordWeightsJoshua(
		 * HieroGlueGrammarCreater.isHieroLabel(complexLabel),
		 * featureEnrichedGrammarRuleCreator);
		 */
		return result;
	}

}
