package grammarExtraction.samtGrammarExtraction;

import grammarExtraction.phraseProbabilityWeights.NamedBooleanFeature;
import grammarExtraction.phraseProbabilityWeights.NamedFloatingFeature;
import grammarExtraction.phraseProbabilityWeights.NamedIntegerFeature;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.TranslationRuleGapInformation;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

public class LabeledRuleFeaturesComputer {

	private static final String RARITY_PENALTY_FEATURE_NAME = "rarityPenalty";
	private static final String RULE_HAS_SOURCE_TERMINALS_WITHOUT_TARGET_FEATURE_NAME = "ruleHasSourceTerminalsWithoutTarget";
	private static final String RULE_HAS_TARGET_TERMINALS_WITHOUT_SOURCE_FEATURE_NAME = "ruleHasTargetTerminalsWithoutSource";
	private static final String RULE_IS_PURELY_ABSTRACT_FEATURE_NAME = "ruleIsPurelyAbstract";
	private static final String RULE_IS_MONOTONIC_FEATURE_NAME = "ruleIsMonotonic";
	private static final String NUM_TERMINALS_TARGET_SIDE_FEATURE_NAME = "numTerminalsTargetSide";
	protected final static String RULE_IS_PUREL_YLEXICAL_FEATURE_NAME = "ruleIsPurelyLexical";

	private final double ruleCorpusCount;
	private final TranslationRuleSignatureTriple translationRuleSignatureTriple;
	private final WordKeyMappingTable wordKeyMappingTable;
	private final TranslationRuleGapInformation translationRuleGapInformation;

	private LabeledRuleFeaturesComputer(double ruleCorpusCount, TranslationRuleSignatureTriple translationRuleSignatureTriple, WordKeyMappingTable wordKeyMappingTable,
			TranslationRuleGapInformation translationRuleGapInformation) {
		this.ruleCorpusCount = ruleCorpusCount;
		this.translationRuleSignatureTriple = translationRuleSignatureTriple;
		this.wordKeyMappingTable = wordKeyMappingTable;
		this.translationRuleGapInformation = translationRuleGapInformation;
	}

	public static LabeledRuleFeaturesComputer createLabeledRuleFeaturesComputer(double ruleCorpusCount, TranslationRuleSignatureTriple translationRuleSignatureTriple,
			WordKeyMappingTable wordKeyMappingTable, TranslationRuleGapInformation translationRuleGapInformation) {
		return new LabeledRuleFeaturesComputer(ruleCorpusCount, translationRuleSignatureTriple, wordKeyMappingTable, translationRuleGapInformation);
	}

	protected static NamedBooleanFeature createRuleIsMonotonicFeatureValue(boolean featureValue) {
		return NamedBooleanFeature.createNamedBooleanFeature(RULE_IS_MONOTONIC_FEATURE_NAME, featureValue);
	}

	public NamedBooleanFeature ruleIsMonotonic() {
		boolean featureValue = this.translationRuleGapInformation.ruleIsMonotonic();
		return createRuleIsMonotonicFeatureValue(featureValue);
	}

	private boolean ruleHasSourceTerminals() {
		return this.translationRuleSignatureTriple.getSourceSideRepresentation().hasTerminals(wordKeyMappingTable);
	}

	private boolean ruleHasTargetTerminals() {
		return this.translationRuleSignatureTriple.getTargetSideRepresentation().hasTerminals(wordKeyMappingTable);
	}

	private boolean ruleHasTerminals() {

		return (ruleHasSourceTerminals() || ruleHasTargetTerminals());
	}

	protected static NamedBooleanFeature createRuleHasSourceTerminalsWithoutTarget(boolean featureValue) {
		return NamedBooleanFeature.createNamedBooleanFeature(RULE_HAS_SOURCE_TERMINALS_WITHOUT_TARGET_FEATURE_NAME, featureValue);
	}

	public NamedBooleanFeature ruleHasSourceTerminalsWithoutTarget() {
		boolean featureValue = (ruleHasSourceTerminals() && (!ruleHasTargetTerminals()));
		return createRuleHasSourceTerminalsWithoutTarget(featureValue);
	}

	protected static NamedBooleanFeature createRuleHasTargetTerminalsWithoutSourceFeatire(boolean featureValue) {
		return NamedBooleanFeature.createNamedBooleanFeature(RULE_HAS_TARGET_TERMINALS_WITHOUT_SOURCE_FEATURE_NAME, featureValue);
	}

	public NamedBooleanFeature ruleHasTargetTerminalsWithoutSource() {
		boolean featureValue = (ruleHasTargetTerminals() && (!ruleHasSourceTerminals()));
		return createRuleHasTargetTerminalsWithoutSourceFeatire(featureValue);

	}

	protected static NamedBooleanFeature createRuleIsPurelyAbstractFeature(boolean featureValue) {
		return NamedBooleanFeature.createNamedBooleanFeature(RULE_IS_PURELY_ABSTRACT_FEATURE_NAME, featureValue);
	}

	public NamedBooleanFeature ruleIsPurelyAbstract() {
		boolean featureValue = !ruleHasTerminals();
		return createRuleIsPurelyAbstractFeature(featureValue);

	}

	private int computeNumTerminalsTargetSide() {
		int result = 0;
		for (Integer targetKey : this.translationRuleSignatureTriple.getTargetKeys()) {
			if (wordKeyMappingTable.isWordKey(targetKey)) {
				result++;
			}
		}
		return result;
	}

	protected static NamedIntegerFeature createNumTerminalsTargetSideFeature(int numTerminalsTargetSide) {
		return NamedIntegerFeature.createNamedCountFeature(NUM_TERMINALS_TARGET_SIDE_FEATURE_NAME, numTerminalsTargetSide);
	}

	public NamedIntegerFeature getNumTerminalsTargetSide() {
		int numTerminalsTargetSide = computeNumTerminalsTargetSide();
		return createNumTerminalsTargetSideFeature(numTerminalsTargetSide);
	}

	public static double computeRarityPenalty(double ruleCorpusCount) {
		double featureValue = Math.exp(1 - ruleCorpusCount);
		return featureValue;
	}

	protected static NamedFloatingFeature createRarityPenaltyFeature(double rarityPenalty) {
		return NamedFloatingFeature.createNamedFloatingFeature(RARITY_PENALTY_FEATURE_NAME, rarityPenalty);
	}

	public NamedFloatingFeature getRarityPenalty() {
		double rarityPenalty = computeRarityPenalty(ruleCorpusCount);
		return createRarityPenaltyFeature(rarityPenalty);
	}
	
	protected static NamedBooleanFeature createRuleIsPurelyLexicalFeature(boolean ruleIsPurelyLexical) {
		return NamedBooleanFeature.createNamedBooleanFeature(RULE_IS_PUREL_YLEXICAL_FEATURE_NAME, ruleIsPurelyLexical);
	}

	private static boolean ruleHasGaps(TranslationRuleSignatureTriple translationRuleSignatureTriple, WordKeyMappingTable wordKeyMappingTable) {
		for (Integer sourceKey : translationRuleSignatureTriple.getSourceKeys()) {
			if (wordKeyMappingTable.isGapLabelKey(sourceKey)) {
				return true;
			}
		}
		return false;
	}
	
	protected static NamedBooleanFeature ruleIsPurelyLexical(TranslationRuleSignatureTriple translationRuleSignatureTriple, WordKeyMappingTable wordKeyMappingTable) {
		boolean featureValue = !ruleHasGaps(translationRuleSignatureTriple, wordKeyMappingTable);
		return createRuleIsPurelyLexicalFeature(featureValue);

	}
}
