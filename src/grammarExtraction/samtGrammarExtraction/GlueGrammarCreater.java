package grammarExtraction.samtGrammarExtraction;

import java.util.List;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.translationRuleTrie.GlueFeaturesCreater;
import grammarExtraction.translationRuleTrie.TranslationRuleProbabilityTable;
import grammarExtraction.translationRules.TranslationRuleFeatures;
import grammarExtraction.translationRules.TranslationRuleRepresentationCreater;

public abstract class GlueGrammarCreater {

	public static final String ChiangLabel = "X";
	protected final boolean writePlainHieroRules;
	protected final SystemIdentity systemIdentity;

	protected GlueGrammarCreater(boolean writePlainHieroRules, SystemIdentity systemIdentity) {
		this.writePlainHieroRules = writePlainHieroRules;
		this.systemIdentity = systemIdentity;
	}

	public abstract void writeGlueGrammar(TranslationRuleProbabilityTable translationRuleProbabilityTable, String outputFileName,
			GlueFeaturesCreater glueFeaturesCreater);

	private String createGlueRulesWeightsString(GlueFeaturesCreater glueFeaturesCreater, boolean isHieroRule, double generativeGlueLabelProbability,
			double rarityPenalty) {

		TranslationRuleFeatures translationRuleFeatures = glueFeaturesCreater.geGlueRuleFeatures(systemIdentity, generativeGlueLabelProbability, isHieroRule,
				rarityPenalty);
		return translationRuleFeatures.getJoshuaOriginalFeaturesRepresentation();
	}

	private TranslationRuleRepresentationCreater getTranslationRuleRepresentationCreater() {
		return systemIdentity.getTranslationRuleRepresentationCreater();
	}

	public List<String> createStartAndEndRules(GlueFeaturesCreater glueFeaturesCreater) {
		String weightsString = createGlueRulesWeightsString(glueFeaturesCreater, false, 1, 0);
		return getTranslationRuleRepresentationCreater().getStartAndEndRules(weightsString);
	}

	public List<String> createGlueRulesForLabel(String complexLabel, double generativeGlueLabelProbability, double rarityPenalty, GlueFeaturesCreater glueFeaturesCreater) {
		String weightsString = createGlueRulesWeightsString(glueFeaturesCreater, isHieroLabel(complexLabel), generativeGlueLabelProbability, rarityPenalty);
		return getTranslationRuleRepresentationCreater().getGlueRulesForLabel(complexLabel, weightsString);
	}
	
	public static boolean isHieroLabel(String label) {
		return label.equals(ChiangLabel);
	}

}
