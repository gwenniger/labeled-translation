package grammarExtraction.samtGrammarExtraction;

import grammarExtraction.translationRuleTrie.AtomicDouble;
import grammarExtraction.translationRuleTrie.EntropyComputer;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import util.XMLTagging;

public class LabelProbabilityTable implements Serializable {

    private static final long serialVersionUID = 1L;
    static final String NL = "\n";
    private static final String LABEL_PROBABILITY_TABEL_NAME = "LabelProbabilityTable";
    final ConcurrentHashMap<Integer, AtomicDouble> labelCountsTable;
    final AtomicDouble totalCounts;

    protected LabelProbabilityTable(ConcurrentHashMap<Integer, AtomicDouble> labelCountsTable,
	    AtomicDouble totalCounts) {
	this.labelCountsTable = labelCountsTable;
	this.totalCounts = totalCounts;
    }

    public static LabelProbabilityTable createLabelProbabilityTable() {
	ConcurrentHashMap<Integer, AtomicDouble> labelCountsTable = new ConcurrentHashMap<Integer, AtomicDouble>();
	return new LabelProbabilityTable(labelCountsTable, new AtomicDouble(0));
    }

    public void increaseCount(Integer labelKey, double delta) {
	AtomicDouble previousValue = labelCountsTable
		.putIfAbsent(labelKey, new AtomicDouble(delta));

	if (previousValue != null) {
	    previousValue.addAndGet(delta);
	}
	totalCounts.addAndGet(delta);
    }

    public double getProbability(Integer labelKey) {
	if (labelCountsTable.containsKey(labelKey)) {
	    double result = getLabelCount(labelKey) / this.totalCounts.doubleValue();
	    assert (result <= 1);
	    return result;
	} else {
	    return 0;
	}
    }

    public double getLabelCount(Integer labelKey) {
	return labelCountsTable.get(labelKey).doubleValue();
    }

    public synchronized double getEntropy() {
	List<Double> probabilities = new ArrayList<Double>();
	for (Integer key : this.labelCountsTable.keySet()) {
	    probabilities.add(this.getProbability(key));
	}
	return EntropyComputer.computeEntropy(probabilities);
    }

    public double getTotalCounts() {
	return this.totalCounts.get();
    }

    private String getLabelProbabilityString(WordKeyMappingTable wordKeyMappingTable,
	    Integer labelKey) {
	return "Label: " + wordKeyMappingTable.getWordForKey(labelKey) + " Prob: " + getProbability(labelKey);
    }
    
    private String getLabelProbabilityAndCountString(WordKeyMappingTable wordKeyMappingTable,
	    Integer labelKey) {
	return getLabelProbabilityString(wordKeyMappingTable, labelKey) + " Count: " + getLabelCount(labelKey);
    }


    public String getContentsString(WordKeyMappingTable wordKeyMappingTable) {
	String result = XMLTagging.getLeftXMLTag(LABEL_PROBABILITY_TABEL_NAME) + NL;

	for (Integer labelKey : this.labelCountsTable.keySet()) {
	    result += getLabelProbabilityString(wordKeyMappingTable, labelKey) + NL;
	}
	result += "Entropy: " + getEntropy() + NL;
	result += XMLTagging.getRightXMLTag(LABEL_PROBABILITY_TABEL_NAME) + NL;
	return result;
    }
    
    public String getContentsStringWithCounts(WordKeyMappingTable wordKeyMappingTable) {
	String result = XMLTagging.getLeftXMLTag(LABEL_PROBABILITY_TABEL_NAME) + NL;

	for (Integer labelKey : this.labelCountsTable.keySet()) {
	    result += getLabelProbabilityAndCountString(wordKeyMappingTable, labelKey) + NL;
	}
	result += "Entropy: " + getEntropy() + NL;
	result += XMLTagging.getRightXMLTag(LABEL_PROBABILITY_TABEL_NAME) + NL;
	return result;
    }


}
