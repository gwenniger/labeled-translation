package grammarExtraction.samtGrammarExtraction;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;

import java.io.FileNotFoundException;

import util.ConfigFile;
import ccgLabeling.LabelGenerator.UnaryCategoryHandlerType;
import mt_pipeline.mt_config.MTGrammarExtractionConfig;
import alignmentStatistics.MultiThreadComputationParameters;

public class ComplexLabelsGrammarExtractionParameters {
	public final MTGrammarExtractionConfig grammarExtractionConfig;
	public final MultiThreadComputationParameters parameters;
	public final SAMTGrammarExtractionConstraints samtGrammarExtractionConstraints;
	public final ConfigFile configFile;

	private ComplexLabelsGrammarExtractionParameters(MTGrammarExtractionConfig grammarExtractionConfig, MultiThreadComputationParameters parameters,
			SAMTGrammarExtractionConstraints samtGrammarExtractionConstraints, ConfigFile configFile) {
		this.grammarExtractionConfig = grammarExtractionConfig;
		this.parameters = parameters;
		this.samtGrammarExtractionConstraints = samtGrammarExtractionConstraints;
		this.configFile = configFile;
	}

	private static SAMTGrammarExtractionConstraints getSAMTGrammarExtractionConstraints(MTGrammarExtractionConfig grammarExtractionConfig,int maxSourceAndTargetLength) {
		SAMTGrammarExtractionConstraints samtGrammarExtractionConstraints = SAMTGrammarExtractionConstraints.createSAMTGrammarExtractionConstraints(
				maxSourceAndTargetLength,
				grammarExtractionConfig.allowDanglingSecondNonterminal(), grammarExtractionConfig.allowConsecutiveNonterminalsInNonAbstractRules(),
				grammarExtractionConfig.allowSourceAbstract(), grammarExtractionConfig.allowtargetWordsWithoutSource(),
				grammarExtractionConfig.allowDoublePlus(), grammarExtractionConfig.allowPosTagLabelFallback(),
				grammarExtractionConfig.getReorderinLabelSettings(),grammarExtractionConfig.getLabelSideSettings(), getUnaryCategoryHandlerType(grammarExtractionConfig));
		return samtGrammarExtractionConstraints;
	}

	public static UnaryCategoryHandlerType getUnaryCategoryHandlerType(MTGrammarExtractionConfig grammarExtractionConfig) {

		String unaryCategoryHandlerType = grammarExtractionConfig.getUnaryCategoryHandlerType();
		if (unaryCategoryHandlerType.equals("top")) {
			return UnaryCategoryHandlerType.TOP;
		} else if (unaryCategoryHandlerType.equals("bottom")) {
			return UnaryCategoryHandlerType.BOTTOM;
		} else if (unaryCategoryHandlerType.equals("all")) {
			return UnaryCategoryHandlerType.ALL;
		} else {
			throw new RuntimeException("Error : unknown value for unaryCategoryHandlerType - must specify one value of \"all,bottom,top\" ");
		}

	}


	public static ComplexLabelsGrammarExtractionParameters createComplexLabelsGrammarExtractionParameters(String configFileName, int numThreads) {

		System.out.println("ConfigFileName: " + configFileName);
		MTGrammarExtractionConfig grammarExtractionConfig = MTGrammarExtractionConfig.createMTGrammarExtractionConfig(configFileName);
		
		ConfigFile configFile;
		try {
		    	configFile = new ConfigFile(configFileName);
		    	MultiThreadComputationParameters parameters = MultiThreadComputationParameters.createMultiThreadComputationParameters(configFileName, numThreads);
			SAMTGrammarExtractionConstraints samtGrammarExtractionConstraints = getSAMTGrammarExtractionConstraints(grammarExtractionConfig,SystemIdentity.getMaxSourceAndTargetLengthFromConfig(configFile));
		    	return new ComplexLabelsGrammarExtractionParameters(grammarExtractionConfig, parameters, samtGrammarExtractionConstraints, configFile);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}

	}

	public MultiThreadComputationParameters getMultiThreadComputationParameters() {
		return this.parameters;
	}
}
