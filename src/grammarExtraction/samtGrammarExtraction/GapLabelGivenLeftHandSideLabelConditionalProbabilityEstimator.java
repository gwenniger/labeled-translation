package grammarExtraction.samtGrammarExtraction;

import grammarExtraction.translationRuleTrie.AtomicDouble;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

import java.util.concurrent.ConcurrentHashMap;

import junit.framework.Assert;
import util.Pair;
import util.XMLTagging;

public class GapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator {

    private static final String TABLE_NAME_STRING = "GapLabelGivenLeftHandSideLabelConditionalProbabilityTable";
    final ConcurrentHashMap<Integer, LabelProbabilityTable> labelCountsTable;
    final AtomicDouble totalCounts;

    private GapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator(
	    ConcurrentHashMap<Integer, LabelProbabilityTable> labelCountsTable,
	    AtomicDouble totalCounts) {
	this.labelCountsTable = labelCountsTable;
	this.totalCounts = totalCounts;
    }

    public static GapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator createGapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator() {
	return new GapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator(
		new ConcurrentHashMap<Integer, LabelProbabilityTable>(), new AtomicDouble());
    }

    public LabelProbabilityTable putOrGetSubtableAtomically(Integer firstKey) {
	LabelProbabilityTable subtable;
	if (!labelCountsTable.containsKey(firstKey)) {
	    subtable = LabelProbabilityTable.createLabelProbabilityTable();
	    Assert.assertNotNull(subtable);
	    LabelProbabilityTable previousSubtable = labelCountsTable.putIfAbsent(firstKey,
		    subtable);

	    if (previousSubtable != null) {
		subtable = previousSubtable;
	    }

	    labelCountsTable.put(firstKey, subtable);
	    Assert.assertNotNull(subtable);
	} else {
	    subtable = labelCountsTable.get(firstKey);
	    Assert.assertNotNull(subtable);
	}
	return subtable;
    }

    private Pair<Integer> computeGapLabelIndicesDroppedConvertedLabelKeyPair(
	    Pair<Integer> labelKeyPair, WordKeyMappingTable wordKeyMappingTable) {
	Integer firstKey = labelKeyPair.getFirst();
	Integer secondKey = labelKeyPair.getSecond();

	Integer firstKeyConverted = firstKey;
	Integer secondKeyConverted = secondKey;

	if (wordKeyMappingTable.isGapLabelKey(firstKey)) {
	    firstKeyConverted = wordKeyMappingTable
		    .convertGapLabelIndexToLeftHandSideLabelIndex(firstKey);
	}

	if (wordKeyMappingTable.isGapLabelKey(secondKey)) {
	    secondKeyConverted = wordKeyMappingTable
		    .convertGapLabelIndexToLeftHandSideLabelIndex(secondKey);
	}
	return new Pair<Integer>(firstKeyConverted, secondKeyConverted);
    }

    public void increaseCount(Pair<Integer> labelKeyPair, double delta,
	    WordKeyMappingTable wordKeyMappingTable) {

	Pair<Integer> gapLabelIndicesDroppedLabelKeyPair = computeGapLabelIndicesDroppedConvertedLabelKeyPair(labelKeyPair, wordKeyMappingTable);
	Integer firstKey = gapLabelIndicesDroppedLabelKeyPair.getFirst();
	Integer secondKey = gapLabelIndicesDroppedLabelKeyPair.getSecond();
	LabelProbabilityTable subtable = putOrGetSubtableAtomically(firstKey);
	Assert.assertNotNull(subtable);
	subtable.increaseCount(secondKey, delta);
	totalCounts.addAndGet(delta);
    }

    private LabelProbabilityTable getLabelProbabilityTable(Integer firstKey) {
	return this.labelCountsTable.get(firstKey);
    }

    public double getProbability(Pair<Integer> labelKeyPair) {
	Integer firstKey = labelKeyPair.getFirst();
	Integer secondKey = labelKeyPair.getSecond();
	LabelProbabilityTable labelProbabilityTable = getLabelProbabilityTable(firstKey);
	if (labelProbabilityTable != null) {
	    double result = labelProbabilityTable.getProbability(secondKey);
	    assert (result <= 1);
	    return result;
	} else {
	    return 0;
	}
    }

    public double getLabelCount(Pair<Integer> labelKeyPair) {
	Integer firstKey = labelKeyPair.getFirst();
	Integer secondKey = labelKeyPair.getSecond();
	LabelProbabilityTable labelProbabilityTable = getLabelProbabilityTable(firstKey);
	if (labelProbabilityTable != null) {
	    double result = labelProbabilityTable.getLabelCount(secondKey);
	    assert (result <= 1);
	    return result;
	} else {
	    return 0;
	}
    }

    public double getTotalLabelCount(Integer leftHandSideLabelKey) {
	return this.getLabelProbabilityTable(leftHandSideLabelKey).getTotalCounts();

    }

    public double getRelativeFrequency(Integer leftHandSideLabelKey) {
	return getTotalLabelCount(leftHandSideLabelKey) / this.totalCounts.get();
    }

    public double getWeightedMeanEntropy() {
	double result = 0;
	for (Integer leftHandSideLabel : this.labelCountsTable.keySet()) {
	    result += getRelativeFrequency(leftHandSideLabel)
		    * getLabelProbabilityTable(leftHandSideLabel).getEntropy();
	}
	return result;
    }

    public String getLeftHandSideRelativeWeightString(WordKeyMappingTable wordKeyMappingTable,
	    Integer leftHandSideLabelKey) {
	return "LeftHandSideLabel: " + wordKeyMappingTable.getWordForKey(leftHandSideLabelKey)
		+ " relative frequency: " + getRelativeFrequency(leftHandSideLabelKey);
    }

    public String getContentsString(WordKeyMappingTable wordKeyMappingTable) {
	String result = "";
	result += XMLTagging.getLeftXMLTag(TABLE_NAME_STRING) + LabelProbabilityTable.NL;
	for (Integer leftHandSideLabelKey : this.labelCountsTable.keySet()) {
	    result += getLeftHandSideRelativeWeightString(wordKeyMappingTable, leftHandSideLabelKey)
		    + LabelProbabilityTable.NL;
	    result += this.getLabelProbabilityTable(leftHandSideLabelKey).getContentsString(
		    wordKeyMappingTable)
		    + LabelProbabilityTable.NL;
	    ;
	}
	result += "Weighted Mean Entropy: " + getWeightedMeanEntropy() + LabelProbabilityTable.NL;
	result += XMLTagging.getRightXMLTag(TABLE_NAME_STRING);
	return result;
    }

}
