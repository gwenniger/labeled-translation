package grammarExtraction.samtGrammarExtraction;

import junit.framework.Assert;
import alignmentStatistics.InputStringsEnumeration;
import alignmentStatistics.MultiThreadComputationParameters;
import extended_bitg.EbitgChartBuilderBasic;
import extended_bitg.EbitgLexicalizedInference;
import grammarExtraction.chiangGrammarExtraction.GrammarExtractionConstraints;
import grammarExtraction.chiangGrammarExtraction.HierarchicalPhraseBasedRuleExtractor;
import grammarExtraction.grammarExtractionFoundation.RuleExtracter;

public class SAMTRuleExtracter extends RuleExtracter<EbitgLexicalizedInference, SAMTQuadruple> {

	private SAMTRuleExtracter(GrammarExtractionConstraints samtGrammarExtractionConstraints, int maxAllowedInferencesPerNode, boolean useCaching) {
		super(samtGrammarExtractionConstraints, maxAllowedInferencesPerNode, useCaching);
	}

	public static SAMTRuleExtracter createSAMTRuleExtracter(SAMTGrammarExtractionConstraints chiangGrammarExtractionConstraints, int maxAllowedInferencesPerNode, boolean useGreedyNullBinding,
			boolean useCaching) {

		return new SAMTRuleExtracter(chiangGrammarExtractionConstraints, maxAllowedInferencesPerNode, useCaching);
	}

	protected EbitgChartBuilderBasic createChartBuilderSAMT(String sourceString, String targetString, String alignmentString, String targetParse) {

		EbitgChartBuilderBasic ebitgChartBuilder = EbitgChartBuilderBasic.createCompactHATsEbitgChartBuilder(sourceString, targetString, alignmentString, maxAllowedInferencesPerNode, true,
				useCaching, createTargetCCGLabeler(targetParse));
		assert (maxAllowedInferencesPerNode > 10000);

		boolean result = ebitgChartBuilder.findDerivationsAlignment();
		Assert.assertEquals(true, result);
		return ebitgChartBuilder;
	}

	@Override
	public HierarchicalPhraseBasedRuleExtractor<EbitgLexicalizedInference> createTranslationRuleExtractor(SAMTQuadruple inputType) {
		return HierarchicalPhraseBasedRuleExtractor.createHierarchicalPhraseBasedRulesExtractorChiangPure(
				createChartBuilderSAMT(inputType.getSourceString(), inputType.getTargetString(), inputType.getAlignmentString(), inputType.getTargetParseString()), grammarExtractionConstraints,
				SAMTRuleCreater.createSAMTRuleCreater(grammarExtractionConstraints.getReorderLabelingSettings(),grammarExtractionConstraints.createRuleLabelCreater()));
	}

	@Override
	public InputStringsEnumeration<SAMTQuadruple> createInputStringEnumeration(MultiThreadComputationParameters parameters) {
		return SAMTQuadrupleEnumeration.createSAMTQuadrupleEnumeration(parameters.getSourceFileName(), parameters.getTargetFileName(), parameters.getAlignmentsFileName(),
				parameters.getTargetParseFileNameChecked());
	}

}
