package grammarExtraction.samtGrammarExtraction;

import java.util.ArrayList;
import java.util.List;

import util.Pair;
import grammarExtraction.chiangGrammarExtraction.BasicRuleFeaturesSet;
import grammarExtraction.chiangGrammarExtraction.RuleFeaturesBasic;
import grammarExtraction.phraseProbabilityWeights.NamedFeature;
import grammarExtraction.translationRules.TranslationRuleFeatures;

public abstract class RuleFeaturesExtended implements TranslationRuleFeatures {

	protected final BasicRuleFeaturesSet basicRuleFeatures;
	protected final LabeledRuleFeaturesSet labeledRuleFeatures;

	protected RuleFeaturesExtended(BasicRuleFeaturesSet basicRuleFeatures, LabeledRuleFeaturesSet labeledRuleFeatures) {
		this.basicRuleFeatures = basicRuleFeatures;
		this.labeledRuleFeatures = labeledRuleFeatures;
	}

	protected List<NamedFeature> getStandardFeaturesList() {
		List<NamedFeature> result = new ArrayList<NamedFeature>();
		result.addAll(this.basicRuleFeatures.getNamedFeatures());
		result.addAll(this.labeledRuleFeatures.getNamedFeatures());

		return result;
	}


	@Override
	public String getSparseLabeledFeaturesRepresentation() {
		//return NamedFeature.createLabeledFeatureStringListJoshuaFormat(this.getNamedFeaturesList());
		return NamedFeature.createLabeledFeatureStringListJoshuaFormat(this.getNamedFeaturesListSparse());
	}

	@Override
	public String getLabeledFeaturesRepresentation() {
		//return NamedFeature.createLabeledFeatureStringListJoshuaFormat(this.getNamedFeaturesList());
		return NamedFeature.createLabeledFeatureStringListJoshuaFormat(this.getNamedFeaturesList());
	}

	
	@Override
	public String getLabeledFeaturesRepresentationOriginalFeatureFormat() {
		String result = "";
		result += NamedFeature.createLabeledFeatureStringListOriginalFormat(getNamedFeaturesList());
		return result;
	}

	@Override
	public String getJoshuaOriginalFeaturesRepresentation() {
		return RuleFeaturesBasic.getJoshuaFeaturesRepresentation(getDenseFeatureValues());
	}
	
	@Override
	public Pair<Double> getSourceAndTargetPhraseFrequency() {
		return this.basicRuleFeatures.getSourceAndTargetPhraseFrequency();
	}
}
