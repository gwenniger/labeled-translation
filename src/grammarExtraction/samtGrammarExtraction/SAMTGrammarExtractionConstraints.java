package grammarExtraction.samtGrammarExtraction;

import ccgLabeling.LabelGenerator.UnaryCategoryHandlerType;
import grammarExtraction.chiangGrammarExtraction.GrammarExtractionConstraints;
import grammarExtraction.chiangGrammarExtraction.LabelSideSettings;
import grammarExtraction.chiangGrammarExtraction.ReorderLabelingSettings;

public class SAMTGrammarExtractionConstraints extends GrammarExtractionConstraints {

	private static final int MaxTerminalsPlusNonTerminalsSourceSide = 5;

	/**
	 * This was 1 before based on http://www.isi.edu/~chiang/papers/chiang-acl05.pdf (page 5, 3. // However, a limit is nowhere mentioned for Thrax, and
	 * arguably less sensible with rich labels.
	 */
	private static final int MinNonTerminalSpan = 1;

	private static final int MinNonTerminalSpanSourceFilter = 1;
	public static final String USE_NON_UNINIFORM_GLUE_RULE_WEIGHTS_PROPERTY = "useNonUniniformGlueRuleWeights";
	public static final String USE_EXTRA_SAMT_FEATURES_PROPERTY = "useExtraSAMTFeatures";

	protected SAMTGrammarExtractionConstraints(int maxTerminalsPlusNonTerminalsSourceSide, int maxSourceAndTargetLength, int minNonTerminalSpan, int minNonTerminalSpanSourceFilter,
			boolean allowDanglingNonTerminal, boolean allowConsecutiveNonterminalsInNonAbstractRules, boolean allowSourceAbstract,boolean allowTargetWordsWithoutSource, boolean allowDoublePlus, boolean allowPosTagLabelFallback,
			ReorderLabelingSettings reorderLabelingSettings, LabelSideSettings labelSideSettings, UnaryCategoryHandlerType unaryCategoryHandlerType) {
		super(maxTerminalsPlusNonTerminalsSourceSide, maxSourceAndTargetLength, minNonTerminalSpan, minNonTerminalSpanSourceFilter, allowDanglingNonTerminal,
				allowConsecutiveNonterminalsInNonAbstractRules, allowSourceAbstract, allowTargetWordsWithoutSource,allowDoublePlus, allowPosTagLabelFallback, reorderLabelingSettings, labelSideSettings,unaryCategoryHandlerType);
	}

	public static SAMTGrammarExtractionConstraints createSAMTGrammarExtractionConstraints(int maxSourceAndTargetLength, boolean allowDanglingNonTerminal, boolean allowConsecutiveNonterminalsInNonAbstractRules,
			boolean allowSourceAbstract,boolean allowTargetWordsWithoutSource, boolean allowDoublePlus, boolean allowPosTagLabelFallback, ReorderLabelingSettings reorderLabelingSettings,LabelSideSettings labelSideSettings, UnaryCategoryHandlerType unaryCategoryHandlerType) {
		return new SAMTGrammarExtractionConstraints(MaxTerminalsPlusNonTerminalsSourceSide, maxSourceAndTargetLength, MinNonTerminalSpan, MinNonTerminalSpanSourceFilter, allowDanglingNonTerminal,
				allowConsecutiveNonterminalsInNonAbstractRules, allowSourceAbstract,allowTargetWordsWithoutSource, allowDoublePlus, allowPosTagLabelFallback, reorderLabelingSettings, labelSideSettings,unaryCategoryHandlerType);
	}
}
