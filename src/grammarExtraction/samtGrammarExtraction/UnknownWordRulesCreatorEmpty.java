package grammarExtraction.samtGrammarExtraction;

import grammarExtraction.translationRuleTrie.FeatureEnrichedGrammarRuleCreator;

import java.util.Collections;
import java.util.List;

public class UnknownWordRulesCreatorEmpty implements UnknownWordRulesCreator{

	private UnknownWordRulesCreatorEmpty(){};
	
	public static UnknownWordRulesCreatorEmpty createUnknownWordRulesCreatorEmpty()
	{
		return new UnknownWordRulesCreatorEmpty();
	}
	
	@Override
	public List<String> createUnknownWordsRules(FeatureEnrichedGrammarRuleCreator featureEnrichedGrammarRuleCreator) {
		return Collections.emptyList();
	}

}
