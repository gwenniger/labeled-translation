package grammarExtraction.samtGrammarExtraction;

import java.util.ArrayList;
import java.util.List;
import grammarExtraction.chiangGrammarExtraction.OrderedLabeledFeatures;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatter;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatterJoshua;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatterMoses;
import grammarExtraction.phraseProbabilityWeights.NamedBooleanFeature;
import grammarExtraction.phraseProbabilityWeights.NamedFeature;
import grammarExtraction.phraseProbabilityWeights.NamedFloatingFeature;
import grammarExtraction.phraseProbabilityWeights.NamedIntegerFeature;
import grammarExtraction.phraseProbabilityWeights.NamedProbabilityFeature;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.TranslationRuleGapInformation;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

public class LabeledRuleFeaturesSet extends ComplexRuleFeaturesSet implements OrderedLabeledFeatures {

	private static final String IS_HIERO_RULE_FEATURE_NAME = "isHierorule";
	private static final String IS_HIERO_GLUE_RULE_FEATURE_NAME = "isHieroGlueRule";
	private static final String IS_SYNTACTIC_GLUE_RULE_FEATURE_NAME = "isSyntacticGlueRule";
	private static final String RULE_GIVEN_LABEL_GENERATIVE_PROBABILITY_FEATURE_NAME = "ruleGivenLabelGenerativeProbability";
	private static final String RULE_APPLICATION_COUNT_FEATURE_NAME = "ruleApplicationCountFeature";
	private static final NamedIntegerFeature RULE_APPLICATION_COUNT_FEATURE = NamedIntegerFeature.createNamedCountFeature(RULE_APPLICATION_COUNT_FEATURE_NAME,
			1);

	public static final int NO_EXTRA_LABELED_RULE_FEATURES = 12;

	private static LabeledRuleFeatureSetStaticJohsua LABELED_RULE_FEATURE_SET_STATIC_JOHSUA = new LabeledRuleFeatureSetStaticJohsua();
	private static LabeledRuleFeatureSetStaticMoses LABELED_RULE_FEATURE_SET_STATIC_MOSES = new LabeledRuleFeatureSetStaticMoses();

	private final NamedBooleanFeature isHieroRule;
	private final NamedBooleanFeature isHieroGlueRule;
	private final NamedBooleanFeature isSyntacticGlueRule;
	private NamedProbabilityFeature ruleGivenLabelGenerativeProbability;
	private final NamedBooleanFeature ruleIsPurelyLexical;
	private final NamedBooleanFeature ruleIsPurelyAbstract;
	private final NamedBooleanFeature ruleHasSourceTerminalsWithoutTarget;
	private final NamedBooleanFeature ruleHasTargetTerminalsWithoutSource;
	private final NamedBooleanFeature ruleIsMonotonic;
	private final NamedIntegerFeature numTerminalsTargetSide;
	private final NamedFloatingFeature rarityPenalty;

	protected LabeledRuleFeaturesSet(FeatureValueFormatter featureValueFormatter, NamedBooleanFeature isHieroRule, NamedBooleanFeature isHieroGlueRule,
			NamedBooleanFeature isSyntacticGlueRule, TranslationRuleSignatureTriple translationRuleSignatureTriple,
			NamedProbabilityFeature ruleGivenLabelGenerativeProbability, WordKeyMappingTable wordKeyMappingTable,
			TranslationRuleGapInformation translationRuleGapInformation, NamedBooleanFeature ruleIsPurelyLexical, NamedBooleanFeature ruleIsPurelyAbstract,
			NamedBooleanFeature ruleHasSourceTerminalsWithoutTarget, NamedBooleanFeature ruleHasTargetTerminalsWithoutSource,
			NamedBooleanFeature ruleIsMonotonic, NamedIntegerFeature numTerminalsTargetSide, NamedFloatingFeature rarityPenalty

	) {
		super(featureValueFormatter);
		this.isHieroRule = isHieroRule;
		this.isHieroGlueRule = isHieroGlueRule;
		this.isSyntacticGlueRule = isSyntacticGlueRule;
		this.ruleGivenLabelGenerativeProbability = ruleGivenLabelGenerativeProbability;

		this.ruleIsPurelyLexical = ruleIsPurelyLexical;
		this.ruleIsPurelyAbstract = ruleIsPurelyAbstract;
		this.ruleHasSourceTerminalsWithoutTarget = ruleHasSourceTerminalsWithoutTarget;
		this.ruleHasTargetTerminalsWithoutSource = ruleHasTargetTerminalsWithoutSource;
		this.ruleIsMonotonic = ruleIsMonotonic;
		this.numTerminalsTargetSide = numTerminalsTargetSide;
		this.rarityPenalty = rarityPenalty;
	}

	public static LabeledRuleFeaturesSet createLabeledRuleFeaturesSet(FeatureValueFormatter featureValueFormatter, double ruleCorpusCount, boolean isHieroRule,
			boolean isHieroGlueRule, boolean isSyntacticGlueRule, TranslationRuleSignatureTriple translationRuleSignatureTriple,
			double ruleGivenLabelGenerativeProbability, WordKeyMappingTable wordKeyMappingTable) {

		TranslationRuleGapInformation translationRuleGapInformation = TranslationRuleGapInformation.createTranslationRuleGapInformation(
				translationRuleSignatureTriple, wordKeyMappingTable);
		LabeledRuleFeaturesComputer labeledRuleFeaturesComputer = LabeledRuleFeaturesComputer.createLabeledRuleFeaturesComputer(ruleCorpusCount,
				translationRuleSignatureTriple, wordKeyMappingTable, translationRuleGapInformation);

		return new LabeledRuleFeaturesSet(featureValueFormatter, createIsHieroRuleFeature(isHieroRule), createIsHieroGlueRuleFeature(isHieroGlueRule),
				createIsSyntacticGlueRuleFeature(isSyntacticGlueRule), translationRuleSignatureTriple,
				createRuleGivenLabelGenerativeProbability(ruleGivenLabelGenerativeProbability), wordKeyMappingTable, translationRuleGapInformation,
				LabeledRuleFeaturesComputer.ruleIsPurelyLexical(translationRuleSignatureTriple, wordKeyMappingTable),
				labeledRuleFeaturesComputer.ruleIsPurelyAbstract(), labeledRuleFeaturesComputer.ruleHasSourceTerminalsWithoutTarget(),
				labeledRuleFeaturesComputer.ruleHasTargetTerminalsWithoutSource(), labeledRuleFeaturesComputer.ruleIsMonotonic(),
				labeledRuleFeaturesComputer.getNumTerminalsTargetSide(), labeledRuleFeaturesComputer.getRarityPenalty());
	}

	public static LabeledRuleFeaturesSet createLabeledRuleFeaturesSet(FeatureValueFormatter featureValueFormatter, double ruleGivenLabelGenerativeProbability,
			boolean isHieroRule, boolean isHieroGlueRule, boolean isSyntacticGlueRule, boolean ruleIsPurelyLexical, boolean ruleIsPurelyAbstract,
			boolean sourceTerminalsWithoutTarget, boolean targetTerminalsWithoutSource, boolean ruleIsMonotonic, int numTerminalsTargetSide,
			double rarityPenalty) {
		return new LabeledRuleFeaturesSet(featureValueFormatter, createIsHieroRuleFeature(isHieroRule), createIsHieroGlueRuleFeature(isHieroGlueRule),
				createIsSyntacticGlueRuleFeature(isSyntacticGlueRule), null, createRuleGivenLabelGenerativeProbability(ruleGivenLabelGenerativeProbability),
				null, null, LabeledRuleFeaturesComputer.createRuleIsPurelyLexicalFeature(ruleIsPurelyLexical),
				LabeledRuleFeaturesComputer.createRuleIsPurelyAbstractFeature(ruleIsPurelyAbstract),
				LabeledRuleFeaturesComputer.createRuleHasSourceTerminalsWithoutTarget(sourceTerminalsWithoutTarget),
				LabeledRuleFeaturesComputer.createRuleHasTargetTerminalsWithoutSourceFeatire(targetTerminalsWithoutSource),
				LabeledRuleFeaturesComputer.createRuleIsMonotonicFeatureValue(ruleIsMonotonic),
				LabeledRuleFeaturesComputer.createNumTerminalsTargetSideFeature(numTerminalsTargetSide),
				LabeledRuleFeaturesComputer.createRarityPenaltyFeature(rarityPenalty));
	}

	public static LabeledRuleFeaturesSet createGlueRuleLabeledRuleFeatureSet(FeatureValueFormatter featureValueFormatter,
			double generativeGlueLabelProbability, boolean isHieroRule, double rarityPenalty) {
		return createLabeledRuleFeaturesSet(featureValueFormatter, generativeGlueLabelProbability, isHieroRule, isHieroRule, (!isHieroRule), false, true,
				false, false, true, 0, rarityPenalty);
	}

	public static LabeledRuleFeaturesSet createLabeledFeatureSetFromOrderedValuesStringList(FeatureValueFormatter featureValueFormatter,
			List<String> valuesStringList) {
		if (valuesStringList.size() != 12) {
			throw new RuntimeException("Error: trying to create a LabeledRuleFeatureSet from a list with a number of elements not equal to 12");
		}

		double ruleGivenLabelGenerativeProbability = Double.parseDouble(valuesStringList.get(0));
		boolean isHieroRule = Boolean.parseBoolean(valuesStringList.get(1));
		boolean isHieroGlueRule = Boolean.parseBoolean(valuesStringList.get(2));
		boolean isSyntacticGlueRule = Boolean.parseBoolean(valuesStringList.get(3));
		// Index 4 contains the ruleApplication count feature, but is not
		// required (it is always 1)
		boolean ruleIsPurelyLexical = Boolean.parseBoolean(valuesStringList.get(5));
		boolean ruleIsPurelyAbstract = Boolean.parseBoolean(valuesStringList.get(6));
		boolean sourceTerminalsWithoutTarget = Boolean.parseBoolean(valuesStringList.get(7));
		boolean targetTerminalsWithoutSource = Boolean.parseBoolean(valuesStringList.get(8));
		boolean ruleIsMonotonic = Boolean.parseBoolean(valuesStringList.get(9));
		int numTerminalsTargetSide = Integer.parseInt(valuesStringList.get(10));
		double rarityPenalty = Double.parseDouble(valuesStringList.get(11));

		return LabeledRuleFeaturesSet.createLabeledRuleFeaturesSet(featureValueFormatter, ruleGivenLabelGenerativeProbability, isHieroRule, isHieroGlueRule,
				isSyntacticGlueRule, ruleIsPurelyLexical, ruleIsPurelyAbstract, sourceTerminalsWithoutTarget, targetTerminalsWithoutSource, ruleIsMonotonic,
				numTerminalsTargetSide, rarityPenalty);

	}

	private static NamedBooleanFeature createIsHieroRuleFeature(boolean isHieroRule) {
		return NamedBooleanFeature.createNamedBooleanFeature(IS_HIERO_RULE_FEATURE_NAME, isHieroRule);
	}

	private static NamedBooleanFeature createIsHieroGlueRuleFeature(boolean isHieroGlueRule) {
		return NamedBooleanFeature.createNamedBooleanFeature(IS_HIERO_GLUE_RULE_FEATURE_NAME, isHieroGlueRule);
	}

	private static NamedBooleanFeature createIsSyntacticGlueRuleFeature(boolean isSyntacticGlueRule) {
		return NamedBooleanFeature.createNamedBooleanFeature(IS_SYNTACTIC_GLUE_RULE_FEATURE_NAME, isSyntacticGlueRule);
	}

	private static NamedProbabilityFeature createRuleGivenLabelGenerativeProbability(double ruleGivenLabelGenerativeProbability) {
		return NamedProbabilityFeature.createNamedProbabilityFeature(RULE_GIVEN_LABEL_GENERATIVE_PROBABILITY_FEATURE_NAME, ruleGivenLabelGenerativeProbability);
	}

	public List<Float> getFeatureValues() {
		List<Float> result = new ArrayList<Float>();
		for (NamedFeature namedFeature : getNamedFeatures()) {
			result.add(namedFeature.getUnlabeledFeatureValue(featureValueFormatter));
		}
		return result;
	}

	public static int noExtraLabeledRuleFeatures() {
		return NO_EXTRA_LABELED_RULE_FEATURES;
	}

	public static LabeledRuleFeatureSetStatic getLabeledRuleFeatureSetStatic(SystemIdentity systemIdentity) {
		if (systemIdentity.isMosesSystem()) {
			return LABELED_RULE_FEATURE_SET_STATIC_MOSES;
		} else if (systemIdentity.isJoshuaSystem()) {
			return LABELED_RULE_FEATURE_SET_STATIC_JOHSUA;
		} else {
			throw new RuntimeException("Unknown system type");
		}

	}

	public static LabeledRuleFeaturesSet getExtraLabeledRuleFeaturesUnknownWordRule(SystemIdentity systemIdentity, boolean isHieroRule) {
		return getLabeledRuleFeatureSetStatic(systemIdentity).getExtraLabeledRuleFeaturesUnknownWordRule(isHieroRule);
	}

	public String getUnlabeledFeaturesStringRepresentation() {
		String result = "";
		List<NamedFeature> namedFeatures = this.getNamedFeatures();
		for (int i = 0; i < namedFeatures.size() - 1; i++) {
			result += namedFeatures.get(i).getUnlabeledFeatureValue(featureValueFormatter) + " ";
		}
		if (!namedFeatures.isEmpty()) {
			result += namedFeatures.get(namedFeatures.size() - 1).getUnlabeledFeatureValue(featureValueFormatter);

		}
		return result;
	}

	public static LabeledRuleFeaturesSet getExtraLabeledRuleFeaturesReorderingSwitchRuleRule(SystemIdentity systemIdentity) {
		return getLabeledRuleFeatureSetStatic(systemIdentity).getExtraLabeledRuleFeaturesReorderingSwitchRuleRule();
	}

	/*
	 * public List<String> getOrderedFeatureLabels() { List<String> result = new
	 * ArrayList<String>();
	 * 
	 * result.add("ruleGivenLabelGenerativeProbability");
	 * result.add("isBasicHieroRule"); result.add("isHieroGlueRule");
	 * result.add("isSyntacticGLueRule");
	 * result.add(RULE_APPLICATION_COUNT_FEATURE_NAME);
	 * result.add("ruleIsPurelyLexical"); result.add("ruleIsPurelyAbstract");
	 * result.add("sourceTerminalsWithoutTarget");
	 * result.add("targetTerminalsWithoutSource");
	 * result.add("ruleIsMonotonic"); result.add("numTerminalsTargetSide");
	 * result.add(RARITY_PENALTY_FEATURE_NAME); return result;
	 * 
	 * }
	 */

	@Override
	public List<NamedFeature> getNamedFeatures() {
		List<NamedFeature> result = new ArrayList<NamedFeature>();
		result.add(ruleGivenLabelGenerativeProbability);
		result.add(isHieroRule);
		result.add(isHieroGlueRule);
		result.add(isSyntacticGlueRule);
		result.add(RULE_APPLICATION_COUNT_FEATURE);
		result.add(ruleIsPurelyLexical);
		result.add(ruleIsPurelyAbstract);
		result.add(ruleHasSourceTerminalsWithoutTarget);
		result.add(ruleHasTargetTerminalsWithoutSource);
		result.add(ruleIsMonotonic);
		result.add(numTerminalsTargetSide);
		result.add(rarityPenalty);
		return result;
	}

	public List<NamedFeature> getNamedFeaturesSparse() {
		List<NamedFeature> result = new ArrayList<NamedFeature>();
		for (NamedFeature namedFeature : getNamedFeatures()) {
			// Only features with non zero values are added to the sparse set
			if (namedFeature.getUnlabeledFeatureValue(featureValueFormatter) != 0) {
				result.add(namedFeature);
			}
		}
		return result;
	}

	public static List<NamedFeature> getRepresentativeZeroNamedFeatures(SystemIdentity systemIdentity) {
		return getLabeledRuleFeatureSetStatic(systemIdentity).getRepresentativeZeroNamedFeatures();
	}

	private static abstract class LabeledRuleFeatureSetStatic {
		public abstract List<NamedFeature> getRepresentativeZeroNamedFeatures();

		public abstract LabeledRuleFeaturesSet getExtraLabeledRuleFeaturesReorderingSwitchRuleRule();

		public abstract LabeledRuleFeaturesSet getExtraLabeledRuleFeaturesUnknownWordRule(boolean isHieroRule);
	}

	private static class LabeledRuleFeatureSetStaticJohsua extends LabeledRuleFeatureSetStatic {

		private static FeatureValueFormatterJoshua FEATURE_VALUE_FORMATTER_JOSHUA = new FeatureValueFormatterJoshua();
		private static final LabeledRuleFeaturesSet LABELED_RULE_FEATURES_SET_UNKNOWN_WORD_RULE_HIERO = createLabeledRuleFeaturesSet(
				FEATURE_VALUE_FORMATTER_JOSHUA, 1, true, false, false, false, false, false, false, true, 0, LabeledRuleFeaturesComputer.computeRarityPenalty(0));
		private static final LabeledRuleFeaturesSet LABELED_RULE_FEATURES_SET_UNKNOWN_WORD_RULE_NON_HIERO = createLabeledRuleFeaturesSet(
				FEATURE_VALUE_FORMATTER_JOSHUA, 1, false, false, false, false, false, false, false, true, 0,
				LabeledRuleFeaturesComputer.computeRarityPenalty(0));
		private static final LabeledRuleFeaturesSet LABELED_RULE_FEATURES_SET_REORDERING_SWITCH_RULE = createLabeledRuleFeaturesSet(
				FEATURE_VALUE_FORMATTER_JOSHUA, 1, false, false, false, false, false, false, false, true, 0, 0);
		private static final LabeledRuleFeaturesSet LABELED_RULE_FEATURES_SET_ZERO = createLabeledRuleFeaturesSet(FEATURE_VALUE_FORMATTER_JOSHUA, 0, false,
				false, false, false, false, false, false, false, 0, 0);

		@Override
		public List<NamedFeature> getRepresentativeZeroNamedFeatures() {
			return LABELED_RULE_FEATURES_SET_ZERO.getNamedFeatures();
		}

		@Override
		public LabeledRuleFeaturesSet getExtraLabeledRuleFeaturesReorderingSwitchRuleRule() {
			return LABELED_RULE_FEATURES_SET_REORDERING_SWITCH_RULE;
		}

		@Override
		public LabeledRuleFeaturesSet getExtraLabeledRuleFeaturesUnknownWordRule(boolean isHieroRule) {
			if (isHieroRule) {
				return LABELED_RULE_FEATURES_SET_UNKNOWN_WORD_RULE_HIERO;
			} else {
				return LABELED_RULE_FEATURES_SET_UNKNOWN_WORD_RULE_NON_HIERO;
			}
		}
	}

	private static class LabeledRuleFeatureSetStaticMoses extends LabeledRuleFeatureSetStatic {

		private static FeatureValueFormatterMoses FEATURE_VALUE_FORMATTER_MOSES = new FeatureValueFormatterMoses();
		private static final LabeledRuleFeaturesSet LABELED_RULE_FEATURES_SET_UNKNOWN_WORD_RULE_HIERO = createLabeledRuleFeaturesSet(
				FEATURE_VALUE_FORMATTER_MOSES, 1, true, false, false, false, false, false, false, true, 0, LabeledRuleFeaturesComputer.computeRarityPenalty(0));
		private static final LabeledRuleFeaturesSet LABELED_RULE_FEATURES_SET_UNKNOWN_WORD_RULE_NON_HIERO = createLabeledRuleFeaturesSet(
				FEATURE_VALUE_FORMATTER_MOSES, 1, false, false, false, false, false, false, false, true, 0, LabeledRuleFeaturesComputer.computeRarityPenalty(0));
		private static final LabeledRuleFeaturesSet LABELED_RULE_FEATURES_SET_REORDERING_SWITCH_RULE = createLabeledRuleFeaturesSet(
				FEATURE_VALUE_FORMATTER_MOSES, 1, false, false, false, false, false, false, false, true, 0, 0);
		private static final LabeledRuleFeaturesSet LABELED_RULE_FEATURES_SET_ZERO = createLabeledRuleFeaturesSet(FEATURE_VALUE_FORMATTER_MOSES, 0, false,
				false, false, false, false, false, false, false, 0, 0);

		@Override
		public List<NamedFeature> getRepresentativeZeroNamedFeatures() {
			return LABELED_RULE_FEATURES_SET_ZERO.getNamedFeatures();
		}

		@Override
		public LabeledRuleFeaturesSet getExtraLabeledRuleFeaturesReorderingSwitchRuleRule() {
			return LABELED_RULE_FEATURES_SET_REORDERING_SWITCH_RULE;
		}

		@Override
		public LabeledRuleFeaturesSet getExtraLabeledRuleFeaturesUnknownWordRule(boolean isHieroRule) {
			if (isHieroRule) {
				return LABELED_RULE_FEATURES_SET_UNKNOWN_WORD_RULE_HIERO;
			} else {
				return LABELED_RULE_FEATURES_SET_UNKNOWN_WORD_RULE_NON_HIERO;
			}
		}

	}

}
