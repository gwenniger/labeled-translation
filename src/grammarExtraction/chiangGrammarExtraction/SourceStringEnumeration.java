package grammarExtraction.chiangGrammarExtraction;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;
import alignment.SourceString;
import alignmentStatistics.InputStringsEnumeration;

public class SourceStringEnumeration extends InputStringsEnumeration<SourceString> {

	protected  SourceStringEnumeration(List<BufferedReader> inputReaders) {
		super(inputReaders);
	}

	public static SourceStringEnumeration createSourceStringEnumeration(String sourceFileName) {
		try {
			BufferedReader sourceReader = new BufferedReader(new FileReader(sourceFileName));
			
			List<BufferedReader> inputReaders = Arrays.asList(sourceReader);
			return new SourceStringEnumeration(inputReaders);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}
	@Override
	protected SourceString getNextT(List<String> nextInputStrings) {
		return SourceString.createSourceString(nextInputStrings.get(0));
	}
	@Override
	protected boolean inputStringsAreAcceptable(List<String> nextInputStrings) {
		return true;
	}

}
