package grammarExtraction.chiangGrammarExtraction;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.samtGrammarExtraction.GlueGrammarCreater;
import grammarExtraction.translationRuleTrie.GlueFeaturesCreater;
import grammarExtraction.translationRuleTrie.TranslationRuleProbabilityTable;

public class HieroGlueGrammarCreater extends GlueGrammarCreater {

	private HieroGlueGrammarCreater(boolean writePlainHieroRules, SystemIdentity systemIdentity) {
		super(writePlainHieroRules, systemIdentity);
	}

	public static HieroGlueGrammarCreater createHieroGlueGrammarCreater(boolean writePlainHieroRules, SystemIdentity systemIdentity) {
		return new HieroGlueGrammarCreater(writePlainHieroRules, systemIdentity);
	}

	public static void writeGlueRules(List<String> glueRules, BufferedWriter outputWriter) throws IOException {
		int i = 0;
		for (i = 0; i < glueRules.size() - 1; i++) {
			String ruleLine = glueRules.get(i);
			assert (ruleLine.length() > 0);
			ruleLine += "\n";
			outputWriter.write(ruleLine);
		}
		if (!glueRules.isEmpty()) {
			outputWriter.write(glueRules.get(i));
		}
	}

	public static void writeGlueGrammar(List<String> glueRules, String outputFileName) {
		try {
			BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFileName));
			writeGlueRules(glueRules, outputWriter);
			outputWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public List<String> getChiangGlueRules(GlueFeaturesCreater glueFeaturesCreater) {
		List<String> result = new ArrayList<String>();
		result.addAll(createStartAndEndRules(glueFeaturesCreater));
		result.addAll(createGlueRulesForLabel(ChiangLabel, 1.0, 0.0, glueFeaturesCreater));
		return result;
	}

	@Override
	public void writeGlueGrammar(TranslationRuleProbabilityTable translationRuleProbabilityTable, String outputFileName, GlueFeaturesCreater glueFeaturesCreater) {
		writeGlueGrammar(getChiangGlueRules(glueFeaturesCreater), outputFileName);
	}

}
