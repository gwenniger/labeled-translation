package grammarExtraction.chiangGrammarExtraction;

import grammarExtraction.phraseProbabilityWeights.NamedFeature;

import java.util.List;

public interface OrderedLabeledFeatures {

	List<NamedFeature> getNamedFeatures();
}
