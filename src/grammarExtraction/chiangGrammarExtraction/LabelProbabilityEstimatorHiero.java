package grammarExtraction.chiangGrammarExtraction;

import java.io.Serializable;

import grammarExtraction.translationRuleTrie.AtomicDouble;
import grammarExtraction.translationRuleTrie.LabelProbabilityEstimator;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

public class LabelProbabilityEstimatorHiero implements LabelProbabilityEstimator, Serializable {

	private static final long serialVersionUID = 1L;
	private AtomicDouble totalSummedRulesCount;

	private LabelProbabilityEstimatorHiero(AtomicDouble totalSummedRulesCount) {
		this.totalSummedRulesCount = totalSummedRulesCount;
	}

	public static LabelProbabilityEstimatorHiero createLabelProbabilityEstimatorHiero() {
	    System.out.println(">>> createLabelProbabilityEstimatorHiero");
		return new LabelProbabilityEstimatorHiero(new AtomicDouble());
	}

	@Override
	public void increaseCount(Integer labelKey, double delta) {
		this.totalSummedRulesCount.addAndGet(delta);
	}

	@Override
	public double getProbability(Integer labelKey) {
		return 1;
	}

	@Override
	public double getRarityPenaltyLabel(Integer labelKey) {
		return 0;
	}

	@Override
	public double getLabelCount(Integer labelKey) {
		return this.totalSummedRulesCount.get();
	}

	@Override
	public double getProbabilityForGlueRules(Integer labelKey) {
	    return 1;
	}

	@Override
	public void showContentsForDebugging(WordKeyMappingTable wordKeyMappingTable) {
	    String resultString ="<LabelPrbabilityEstimatorHiero>\n";
	    resultString += "totalCount: " + totalSummedRulesCount.get() + "\n";
	    resultString += "</LabelPrbabilityEstimatorHiero>\n";
	    System.out.println(resultString);
	    
	}

}
