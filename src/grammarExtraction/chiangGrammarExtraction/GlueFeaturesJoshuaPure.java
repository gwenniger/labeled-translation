package grammarExtraction.chiangGrammarExtraction;

import java.util.List;

import util.Pair;

import grammarExtraction.phraseProbabilityWeights.NamedFeature;
import grammarExtraction.translationRules.TranslationRuleFeatures;

public class GlueFeaturesJoshuaPure implements TranslationRuleFeatures {

	@Override
	public List<Float> getDenseFeatureValues() {
		throw new RuntimeException("not implemented");
	}

	@Override
	public String getJoshuaOriginalFeaturesRepresentation() {
		return "1 1 1";
	}

	@Override
	public List<NamedFeature> getNamedFeaturesList() {
		throw new RuntimeException("not implemented");
	}

	@Override
	public String getSparseLabeledFeaturesRepresentation() {
		throw new RuntimeException("not implemented");
	}

	@Override
	public String getLabeledFeaturesRepresentationOriginalFeatureFormat() {
		throw new RuntimeException("not implemented");
	}

	@Override
	public String getCompressedFeaturesRepresentation() {
		throw new RuntimeException("not implemented");
	}

	@Override
	public List<NamedFeature> getNamedFeaturesListSparse() {
		throw new RuntimeException("not implemented");
	}

	@Override
	public String getLabeledFeaturesRepresentation() {
		throw new RuntimeException("not implemented");
	}

	@Override
	public List<NamedFeature> getDiscriminativeNamedFeaturesList() {
		throw new RuntimeException("not implemented");
	}

	@Override
	public List<NamedFeature> getDenseNamedFeaturesList() {
		throw new RuntimeException("not implemented");
	}

	@Override
	public Pair<Double> getSourceAndTargetPhraseFrequency() {
		throw new RuntimeException("not implemented");
	}

	@Override
	public TranslationRuleFeatures createCopyWithScaledProbabilities(double scalingFactor) {
		throw new RuntimeException("not implemented");
	}

}
