package grammarExtraction.chiangGrammarExtraction;

import java.io.Serializable;
import java.util.List;
import extended_bitg.EbitgLexicalizedInference;
import reorderingLabeling.ReorderingLabel;
import reorderingLabeling.ReorderingLabelCreater;

public class ReorderLabelingSettings implements Serializable {
    private static final long serialVersionUID = 1L;
    private final ReorderingLabelCreater reorderingLabelCreater;
    private final ReorderLabelingFlags reorderLabelingFlags;

    private ReorderLabelingSettings(ReorderingLabelCreater reorderingLabelCreater,
	    ReorderLabelingFlags reorderLabelingFlags) {
	this.reorderingLabelCreater = reorderingLabelCreater;
	this.reorderLabelingFlags = reorderLabelingFlags;
    }

    public static ReorderLabelingSettings createReorderLabelingSettings(
	    ReorderingLabelCreater reorderingLabelCreater, boolean useReorderingLabelExtension,
	    boolean reorderLabelHieroRules, boolean reorderLabelPhrasePairs) {
	ReorderLabelingFlags reorderLabelingFlags = ReorderLabelingFlags
		.createReorderLabelingFlags(useReorderingLabelExtension, reorderLabelHieroRules,
			reorderLabelPhrasePairs);
	return new ReorderLabelingSettings(reorderingLabelCreater, reorderLabelingFlags);
    }

    public static ReorderLabelingSettings createNoReorderingLabelingSettings() {
	return new ReorderLabelingSettings(null,
		ReorderLabelingFlags.createNoReorderLabelingFlags());
    }

    public boolean useReorderingLabelExtension() {
	return reorderLabelingFlags.useReorderingLabelExtension();
    }

    public boolean reorderLabelHieroRules() {
	return reorderLabelingFlags.reorderLabelHieroRules();
    }

    public boolean reorderLabelPhrasePairs() {
	return reorderLabelingFlags.reorderLabelPhrasePairs();
    }

    public ReorderingLabel getReorderingLabelForInference(EbitgLexicalizedInference inference) {
	return this.reorderingLabelCreater.getReorderingLabelForInference(inference);
    }

    public ReorderingLabel getReorderingLabelForReorderingLabelContainingString(
	    String reorderingLabelContainingString) {
	return this.reorderingLabelCreater
		.getReorderingLabelForReorderingLabelContainingString(reorderingLabelContainingString);
    }

    public List<ReorderingLabel> getReorderingLabelsForPhraseSwitchRules() {
	return this.reorderingLabelCreater.getReorderingLabelsForPhraseSwitchRules();
    }

    public int getNumberOfReorderingCategoriesUsedForFineHatReorderingFeatures() {
	return this.reorderingLabelCreater
		.getNumberOfReorderingLabelsUsedForFineHatReorderingFeatures();
    }

    public int getNumberOfReorderingCategoriesUsedForCoarseReorderingFeatures() {
	return this.reorderingLabelCreater
		.getNumberOfReorderingLabelsUsedForCoarseReorderingFeatures();
    }

    public boolean useAbstractRuleReorderingLabelingOnly() {
	return this.useReorderingLabelExtension() && (!reorderLabelHieroRules());
    }

    public boolean producesDoubleReorderingLabels() {
	// The reorderinglabelCreater might be null, in case no reordering labels are used, 
	// so we have to check this
	if(this.reorderingLabelCreater != null){
	    return this.reorderingLabelCreater.producesDoubleReorderingLabels();
	}
	return false;
    }

}
