package grammarExtraction.chiangGrammarExtraction;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;

import mt_pipeline.mt_config.MTConfigFile;
import util.ConfigFile;
import util.Pair;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.phraseProbabilityWeights.EmptyPhraseProbabilityWeights;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatter;
import grammarExtraction.phraseProbabilityWeights.LabelSmoothingWeights;
import grammarExtraction.phraseProbabilityWeights.NamedFeature;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsJoshua;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsPair;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsSet;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsSetJoshua;
import grammarExtraction.phraseProbabilityWeights.SmoothingWeights;
import grammarExtraction.samtGrammarExtraction.UnknownWordRulesCreatorPosTags;
import grammarExtraction.translationRuleTrie.ProbabilityRepresentation;
import grammarExtraction.translationRules.TranslationRuleFeatures;

public class RuleFeaturesBasic implements TranslationRuleFeatures {

	private static Pair<Double> DUMMY_PHRASE_FREQUENCIES_PAIR = new Pair<Double>(-1.0, -1.0);

	private final BasicRuleFeaturesSet basicRuleFeatures;

	protected RuleFeaturesBasic(BasicRuleFeaturesSet basicRuleFeatures) {
		this.basicRuleFeatures = basicRuleFeatures;
	}

	public static RuleFeaturesBasic createRuleFeaturesBasic(PhraseProbabilityWeightsSet phraseProbabilityWeightsSet, double lexicalWeightSourceToTarget,
			double lexicalWeightSourceGivenTarget,FeatureValueFormatter featureValueFormatter) {
	    
		Assert.assertTrue(lexicalWeightSourceGivenTarget <= 1);
		Assert.assertTrue(lexicalWeightSourceToTarget <= 1);		
	    
		return new RuleFeaturesBasic(

		BasicRuleFeaturesSet.createBasicRuleFeatures(phraseProbabilityWeightsSet,lexicalWeightSourceToTarget,lexicalWeightSourceGivenTarget,featureValueFormatter));
	}

	public static RuleFeaturesBasic createRuleFeaturesBasicUniformBasicRuleFeaturesWeights(SystemIdentity systemIdentity, double uniformProbabilityValue) {
		return new RuleFeaturesBasic(getCorrectSizeBasicRuleFeaturesSetUniformValues(systemIdentity, uniformProbabilityValue));
	}

	public static RuleFeaturesBasic createCorrectSizeUnknnownWordRuleRuleFeaturesBasic(SystemIdentity systemIdentity) {
		return createRuleFeaturesBasicUniformBasicRuleFeaturesWeights(systemIdentity, UnknownWordRulesCreatorPosTags.UNKNOWN_WORD_PROBABILITY);
	}

	public static RuleFeaturesBasic createCorrectSizeReorderingLabelToPhraseLabelSwitchRuleFeaturesBasic(SystemIdentity systemIdentity) {
		return createRuleFeaturesBasicUniformBasicRuleFeaturesWeights(systemIdentity, 1);
	}

	public static RuleFeaturesBasic createCorrectSizeGlueRuleFeaturesBasic(SystemIdentity systemIdentity) {
		return createRuleFeaturesBasicUniformBasicRuleFeaturesWeights(systemIdentity, 1);
	}

	public static RuleFeaturesBasic createRuleFeaturesBasicZeroValues(SystemIdentity systemIdentity) {
		return createRuleFeaturesBasicUniformBasicRuleFeaturesWeights(systemIdentity, 0);
	}

	@Override
	public String getJoshuaOriginalFeaturesRepresentation() {
		return getJoshuaFeaturesRepresentation(getDenseFeatureValues());
	}

	public static String getJoshuaFeaturesRepresentation(List<Float> featureValues) {
		String result = "";
		for (Float feature : featureValues) {
			result += feature + " ";
		}

		// throw new
		// RuntimeException("RuleFeaturesBasic.getJoshuaFeaturesRepresentation - result"
		// + result);
		return result;
	}

	@Override
	public List<Float> getDenseFeatureValues() {
		// throw new
		// RuntimeException("RuleFeaturesBasic.getDenseFeatureValues() called");
		return this.basicRuleFeatures.getFeatureValues();

	}

	@Override
	public String getCompressedFeaturesRepresentation() {
		return getSparseLabeledFeaturesRepresentation();
	}

	@Override
	public String getLabeledFeaturesRepresentation() {
		return NamedFeature.createLabeledFeatureStringListJoshuaFormat(getNamedFeaturesList());
	}

	@Override
	public String getSparseLabeledFeaturesRepresentation() {
		return NamedFeature.createLabeledFeatureStringListJoshuaFormat(getNamedFeaturesListSparse());
	}

	@Override
	public List<NamedFeature> getNamedFeaturesList() {
		return this.basicRuleFeatures.getNamedFeatures();
	}

	public static int getNumLexicalPlusPhraseProbabilityFeatures(MTConfigFile configFile, String systemName) {
		SystemIdentity systemIdentity = SystemIdentity.createSystemIdentity(configFile);
		return systemIdentity.getNumLexicalPlusPhraseProbabilityFeatures();
	}

	public static int getNumLexicalPlusPhraseProbabilityFeatures(String configFilePath, String systemName) {
		MTConfigFile configFile;
		configFile = MTConfigFile.createMtConfigFile(configFilePath, systemName);
		return getNumLexicalPlusPhraseProbabilityFeatures(configFile, systemName);
	}

	private static List<util.Pair<Double>> extractSmoothingPhraseProbabilityPairs(List<Double> translationProbabilityWeights) {

		List<util.Pair<Double>> result = new ArrayList<Pair<Double>>();
		if ((translationProbabilityWeights.size() % 2) != 0) {
			throw new RuntimeException(
					"Error : RuleFeaturesBasic.extractSmoothingPhraseProbabilityPairs - odd number of translationProbabilityWeighs, even number is required");
		}

		for (int i = 4; i < translationProbabilityWeights.size(); i += 2) {
			result.add(new Pair<Double>(translationProbabilityWeights.get(i), translationProbabilityWeights.get(i + 1)));
		}
		return result;
	}

	public static final BasicRuleFeaturesSet getCorrectSizeBasicRuleFeaturesSetUniformValues(SystemIdentity systemIdentity, double value) {
		int numLexicalPlusPhraseProbabilityFeatures = systemIdentity.getNumLexicalPlusPhraseProbabilityFeatures();
		List<Double> weightsList = new ArrayList<Double>();
		for (int i = 0; i < numLexicalPlusPhraseProbabilityFeatures; i++) {
			weightsList.add(value);
		}
		return createBasicRuleFeatureSetFromProbabilitiesList(systemIdentity, weightsList);
	}

	public static List<NamedFeature> getRepresentativeBasicZeroNamedFeaturesForConfiguration(SystemIdentity systemIdentity) {
		return getCorrectSizeBasicRuleFeaturesSetUniformValues(systemIdentity, 0).getNamedFeatures();
	}

	public static final BasicRuleFeaturesSet createBasicRuleFeatureSetFromProbabilitiesList(SystemIdentity systemIdentity,
			List<Double> translationProbabilityWeights) {
		if (translationProbabilityWeights.size() < 4) {
			throw new RuntimeException("Error : RuleFeaturesBasic.createBasicRuleFeatureSetFromProbabilitiesList- at least for probabilities needed, but only"
					+ translationProbabilityWeights.size() + " are provided");
		}

		double phraseProbabilityTargetGivenSource = translationProbabilityWeights.get(1);
		double phraseProbabilitySourceGivenTarget = translationProbabilityWeights.get(2);

		double lexicalWeightTargetGivenSource = translationProbabilityWeights.get(0);
		double lexicalWeightSourceGivenTarget = translationProbabilityWeights.get(3);

		PhraseProbabilityWeightsPair phraseProbabilityWeightsPairBasic = PhraseProbabilityWeightsPair.createBasicPhraseProbabilityWeightsPair(
				phraseProbabilityTargetGivenSource, phraseProbabilitySourceGivenTarget);

		List<util.Pair<Double>> smoothingPhraseProbabilityPairs = extractSmoothingPhraseProbabilityPairs(translationProbabilityWeights);
		SmoothingWeights labelSmoothingWeights = LabelSmoothingWeights.createLabelSmoothingWeights(smoothingPhraseProbabilityPairs,
				systemIdentity.getSmoothingPrefixesForSystem());

		PhraseProbabilityWeightsJoshua phraseProbabilityWeightsJoshua = PhraseProbabilityWeightsJoshua.createPhraseProbabilityWeightsJoshuaForTesting(
				phraseProbabilityWeightsPairBasic, labelSmoothingWeights,DUMMY_PHRASE_FREQUENCIES_PAIR);
		PhraseProbabilityWeightsSetJoshua phraseProbabilityWeightsSetJoshua = PhraseProbabilityWeightsSetJoshua.createPhraseProbabilityWeightsSetJoshua(
				phraseProbabilityWeightsJoshua, new EmptyPhraseProbabilityWeights());

		return BasicRuleFeaturesSet.createBasicRuleFeatures(phraseProbabilityWeightsSetJoshua, lexicalWeightTargetGivenSource, lexicalWeightSourceGivenTarget,systemIdentity.getFeatureValueFormatter());

	}

	@Override
	public String getLabeledFeaturesRepresentationOriginalFeatureFormat() {
		return NamedFeature.createLabeledFeatureStringListOriginalFormat(getNamedFeaturesList());
	}

	/*
	 * public static List<NamedFeature>
	 * getOrderedRepresentativeNamedFeatures(SystemIdentity systemIdentity) {
	 * List<NamedFeature> result = new ArrayList<NamedFeature>();
	 * result.addAll(RuleFeaturesBasic
	 * .getRepresentativeBasicZeroNamedFeaturesForConfiguration
	 * (systemIdentity)); return result; }
	 */

	@Override
	public List<NamedFeature> getNamedFeaturesListSparse() {
		return getNamedFeaturesList();
	}

	@Override
	public List<NamedFeature> getDiscriminativeNamedFeaturesList() {
		return Collections.emptyList();
	}

	@Override
	public List<NamedFeature> getDenseNamedFeaturesList() {
		return this.basicRuleFeatures.getNamedFeatures();
	}

	@Override
	public Pair<Double> getSourceAndTargetPhraseFrequency() {
		return basicRuleFeatures.getSourceAndTargetPhraseFrequency();
	}

	@Override
	public TranslationRuleFeatures createCopyWithScaledProbabilities(double scalingFactor) {
		return new RuleFeaturesBasic(this.basicRuleFeatures.createScaledCopyBasicRuleFeatures(scalingFactor));
	}
}