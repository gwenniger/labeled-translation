package grammarExtraction.chiangGrammarExtraction;

import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatter;
import grammarExtraction.phraseProbabilityWeights.NamedFeature;
import grammarExtraction.phraseProbabilityWeights.NamedProbabilityFeature;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import util.Pair;

public class BasicRuleFeaturesSet implements OrderedLabeledFeatures {

	private static final String LEXICAL_WEIGHT_TARGET_GIVEN_SOURCE_NAME = "LexicalWeight_basicTargetGivenSource";
	private static final String LEXICAL_WEIGHT_SOURCE_GIVEN_TARGET_NAME = "LexcialWeight_basicSourceGivenTarget";

	private final PhraseProbabilityWeightsSet phraseProbabilityWeightsSet;

	private final NamedProbabilityFeature lexicalWeightTargetGivenSource;
	private final NamedProbabilityFeature lexicalWeightSourceGivenTarget;
	private final FeatureValueFormatter featureValueFormatter;

	protected BasicRuleFeaturesSet(PhraseProbabilityWeightsSet phraseProbabilityWeightsSet, NamedProbabilityFeature lexicalWeightTargetGivenSource,
			NamedProbabilityFeature lexicalWeightSourceGivenTarget, FeatureValueFormatter featureValueFormatter) {
		this.phraseProbabilityWeightsSet = phraseProbabilityWeightsSet;
		this.lexicalWeightTargetGivenSource = lexicalWeightTargetGivenSource;
		this.lexicalWeightSourceGivenTarget = lexicalWeightSourceGivenTarget;
		this.featureValueFormatter = featureValueFormatter;
	}

	public BasicRuleFeaturesSet createScaledCopyBasicRuleFeatures(double scalingFactor) {

		return new BasicRuleFeaturesSet(phraseProbabilityWeightsSet.createScaledCopy(scalingFactor), createLexicalWeightTargetGivenSource(this.lexicalWeightTargetGivenSource.getProbability() * scalingFactor),
				createLexicalWeightSourceGivenTarget(this.lexicalWeightSourceGivenTarget.getProbability() * scalingFactor), this.featureValueFormatter);
	}

	public static BasicRuleFeaturesSet createBasicRuleFeatures(PhraseProbabilityWeightsSet phraseProbabilityWeightsSet, double lexicalWeightTargetGivenSource, double lexicalWeightSourceGivenTarget,
			FeatureValueFormatter featureValueFormatter) {
	   	   
		return new BasicRuleFeaturesSet(phraseProbabilityWeightsSet, createLexicalWeightTargetGivenSource(lexicalWeightTargetGivenSource),
				createLexicalWeightSourceGivenTarget(lexicalWeightSourceGivenTarget), featureValueFormatter);
	}

	private static NamedProbabilityFeature createLexicalWeightSourceGivenTarget(double lexicalWeightSourceGivenTarget) {
		return NamedProbabilityFeature.createNamedProbabilityFeature(LEXICAL_WEIGHT_SOURCE_GIVEN_TARGET_NAME, lexicalWeightSourceGivenTarget);
	}

	private static NamedProbabilityFeature createLexicalWeightTargetGivenSource(double lexicalWeightTargetGivenSource) {
		return NamedProbabilityFeature.createNamedProbabilityFeature(LEXICAL_WEIGHT_TARGET_GIVEN_SOURCE_NAME, lexicalWeightTargetGivenSource);
	}

	public static List<String> getLexicalWeightsNames() {
		return Arrays.asList(LEXICAL_WEIGHT_TARGET_GIVEN_SOURCE_NAME, LEXICAL_WEIGHT_SOURCE_GIVEN_TARGET_NAME);
	}

	public List<Float> getFeatureValues() {
		List<Float> result = new ArrayList<Float>();
		for (NamedFeature namedFeature : getNamedFeatures()) {
			result.add(namedFeature.getUnlabeledFeatureValue(featureValueFormatter));
		}
		return result;
	}

	@Override
	public List<NamedFeature> getNamedFeatures() {
		List<NamedFeature> result = new ArrayList<NamedFeature>();
		result.add(phraseProbabilityWeightsSet.getBasicPhraseProbabilityWeightTargetGivenSource());
		result.add(lexicalWeightTargetGivenSource);
		result.add(lexicalWeightSourceGivenTarget);
		result.add(phraseProbabilityWeightsSet.getBasicPhraseProbabilityWeightSourceGivenTarget());

		for (NamedProbabilityFeature namedProbabilityFeature : phraseProbabilityWeightsSet.getExtraSmoothingWeights()) {
			result.add(namedProbabilityFeature);
		}
		return result;
	}

	public static String getLexicalWeightTargetGivenSourceLabel() {
		return LEXICAL_WEIGHT_TARGET_GIVEN_SOURCE_NAME;
	}

	public static String getLexicalWeightSourceGivenTargetLabel() {
		return LEXICAL_WEIGHT_SOURCE_GIVEN_TARGET_NAME;
	}

	public Pair<Double> getSourceAndTargetPhraseFrequency() {
		return this.phraseProbabilityWeightsSet.getSourceAndTargetPhraseFrequency();
	}

}
