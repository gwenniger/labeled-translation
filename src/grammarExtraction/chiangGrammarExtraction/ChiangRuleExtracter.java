package grammarExtraction.chiangGrammarExtraction;

import alignment.AlignmentStringTriple;
import alignmentStatistics.AlignmentTriplesEnumeration;
import alignmentStatistics.InputStringsEnumeration;
import alignmentStatistics.MultiThreadComputationParameters;
import extended_bitg.EbitgLexicalizedInference;
import grammarExtraction.grammarExtractionFoundation.RuleExtracter;

public class ChiangRuleExtracter extends RuleExtracter<EbitgLexicalizedInference, AlignmentStringTriple> {
	private ChiangRuleExtracter(GrammarExtractionConstraints chiangGrammarExtractionConstraints, int maxAllowedInferencesPerNode, boolean useCaching) {
		super(chiangGrammarExtractionConstraints, maxAllowedInferencesPerNode, useCaching);
	}

	public static ChiangRuleExtracter createChiangRuleExtracter(GrammarExtractionConstraints chiangGrammarExtractionConstraints, int maxAllowedInferencesPerNode, boolean useGreedyNullBinding,
			boolean useCaching) {
		return new ChiangRuleExtracter(chiangGrammarExtractionConstraints, maxAllowedInferencesPerNode, useCaching);
	}

	@Override
	public HierarchicalPhraseBasedRuleExtractor<EbitgLexicalizedInference> createTranslationRuleExtractor(AlignmentStringTriple input) {
		return HierarchicalPhraseBasedRuleExtractor.createHierarchicalPhraseBasedRulesExtractorChiangPure(
				createChartBuilderBasic(input.getSourceString(), input.getTargetString(), input.getAlignmentString()), grammarExtractionConstraints, new ChiangRuleCreater(
						grammarExtractionConstraints.getReorderLabelingSettings(),grammarExtractionConstraints.createRuleLabelCreater()));
	}

	@Override
	public InputStringsEnumeration<AlignmentStringTriple> createInputStringEnumeration(MultiThreadComputationParameters parameters) {
		return AlignmentTriplesEnumeration.createAlignmentTriplesEnumeration(parameters.getSourceFileName(), parameters.getTargetFileName(), parameters.getAlignmentsFileName());
	}

}
