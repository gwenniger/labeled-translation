package grammarExtraction.chiangGrammarExtraction;

import extended_bitg.EbitgLexicalizedInference;
import grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagPentuple;
import grammarExtraction.grammarExtractionFoundation.ExtractorThreadFactory;
import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;
import grammarExtraction.grammarExtractionFoundation.MultiThreadGrammarExtractor;
import grammarExtraction.grammarExtractionFoundation.RuleExtracter;
import grammarExtraction.grammarExtractionFoundation.TranslationRuleExtractorThread;
import grammarExtraction.labelSmoothing.RuleLabelsSmoother;
import grammarExtraction.samtGrammarExtraction.GapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator;
import grammarExtraction.samtGrammarExtraction.SAMTQuadruple;
import grammarExtraction.translationRuleTrie.LabelProbabilityEstimator;
import grammarExtraction.translationRuleTrie.LexicalProbabilityTables;
import grammarExtraction.translationRuleTrie.TranslationRuleCountTable;

import java.util.List;

import boundaryTagging.EbitgLexicalizedInferenceBoundaryTagged;
import coreContextLabeling.EbitgLexicalizedInferenceCoreContextLabeled;
import alignment.AlignmentStringTriple;
import alignmentStatistics.InputStringsEnumeration;
import util.Pair;

public class TranslationRuleSourceFilteredExtractionThread<InputType extends AlignmentStringTriple> extends TranslationRuleExtractorThread<InputType> implements
		ExtractorThreadFactory<TranslationRuleSourceFilteredExtractionThread<InputType>, MultiThreadGrammarExtractorTestFiltered<? extends EbitgLexicalizedInference, InputType>> {

	final LexicalProbabilityTables lexicalProbabilityTables;
	final LabelProbabilityEstimator labelProbabilityEstimator;
	final GapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator gapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator;

	protected TranslationRuleSourceFilteredExtractionThread(RuleExtracter<? extends EbitgLexicalizedInference, InputType> ruleExtracter, RuleLabelsSmoother ruleLabelsSmoother) {
		super(ruleExtracter, ruleLabelsSmoother);
		lexicalProbabilityTables = null;
		labelProbabilityEstimator = null;
		gapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator = null;
	};

	public TranslationRuleSourceFilteredExtractionThread(int maxAllowedInferencesPerNode, Pair<Integer> minMaxSentencePairNum, InputStringsEnumeration<InputType> inputStringsEnumeration,
			int maxSentenceLength, int outputLevel, TranslationRuleCountTable translationRuleProbabilityTable, boolean useJoshuaStyleSimplifiedCountComputing, boolean useCaching,
			RuleExtracter<? extends EbitgLexicalizedInference, InputType> ruleExtracter, RuleLabelsSmoother ruleLabelsSmoother) {
		super(maxAllowedInferencesPerNode, minMaxSentencePairNum, inputStringsEnumeration, maxSentenceLength, outputLevel, translationRuleProbabilityTable.getTranslationRuleTrie(),
				translationRuleProbabilityTable.getWordKeyMappingTable(), useJoshuaStyleSimplifiedCountComputing, useCaching, ruleExtracter, ruleLabelsSmoother);

		assert (translationRuleProbabilityTable != null);
		// assert(ruleExtracter instanceof SAMTRuleExtracter);

		this.lexicalProbabilityTables = translationRuleProbabilityTable.getLexicalProbabilityTables();
		this.labelProbabilityEstimator = translationRuleProbabilityTable.getLabelProbabilityEstimator();
		this.gapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator = translationRuleProbabilityTable.getGapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator();
	}

	private void addLabelSmoothedruleSets(LightWeightPhrasePairRuleSet ruleSet) {
		for (LightWeightPhrasePairRuleSet smoothedRuleSet : this.ruleLabelsSmoother.getAllLabelSmoothedRuleSets(ruleSet)) {
			getTranslationRuleTrie().addRulesFromSetToTableIfSourceSidePresentInTrie(smoothedRuleSet);
		}
	}

	@Override
	protected void processPhrasePairRuleSet(LightWeightPhrasePairRuleSet originalRuleSet) {

		for (LightWeightPhrasePairRuleSet ruleSet : this.ruleLabelsSmoother.createListOfOriginalAndLabelSmoothedRuleSets(originalRuleSet)) {
			getTranslationRuleTrie().addRulesFromSetToTableIfSourceSidePresentInTrieAndIncreaseLabelCount(ruleSet, this.labelProbabilityEstimator, this.gapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator);
			if (ruleSet.hasRules()) {
				addLabelSmoothedruleSets(ruleSet);
			}
		}
	}

	@Override
	protected List<LightWeightPhrasePairRuleSet> extractRuleSets(InputType input) {
		HierarchicalPhraseBasedRuleExtractor<? extends EbitgLexicalizedInference> translationRuleExtractor = createTranslationRuleExtractor(input);
		List<LightWeightPhrasePairRuleSet> ruleSets = translationRuleExtractor.createLightWeightTranslationRuleSetsWithLexicalProabilities(getWordKeyMappingTable(), this.lexicalProbabilityTables);
		return ruleSets;
	}

	@Override
	public TranslationRuleSourceFilteredExtractionThread<InputType> createExtractorThread(Pair<Integer> minMaxSentencePairNum,
			MultiThreadGrammarExtractorTestFiltered<? extends EbitgLexicalizedInference, InputType> multiThreadGrammarExtractor) {
		InputStringsEnumeration<InputType> inputStringsenumeration = this.ruleExtracter.createInputStringEnumeration(multiThreadGrammarExtractor.parameters);
		TranslationRuleSourceFilteredExtractionThread<InputType> result = new TranslationRuleSourceFilteredExtractionThread<InputType>(
				multiThreadGrammarExtractor.parameters.getMaxAllowedInferencesPerNode(), minMaxSentencePairNum, inputStringsenumeration, multiThreadGrammarExtractor.parameters.getMaxSentenceLength(),
				multiThreadGrammarExtractor.parameters.getOutputLevel(), multiThreadGrammarExtractor.getTranslationRuleCountTable(),
				MultiThreadGrammarExtractor.UseJoshuaStyleSimplifiedCountComputing, multiThreadGrammarExtractor.parameters.getUseCaching(), this.ruleExtracter, this.ruleLabelsSmoother);
		return result;
	}

	public static ExtractorThreadFactory<TranslationRuleSourceFilteredExtractionThread<AlignmentStringTriple>, MultiThreadGrammarExtractorTestFiltered<? extends EbitgLexicalizedInference, AlignmentStringTriple>> createExtractorThreadFactory(
			RuleExtracter<EbitgLexicalizedInference, AlignmentStringTriple> ruleExtracter, RuleLabelsSmoother ruleLabelsSmoother) {
		return new TranslationRuleSourceFilteredExtractionThread<AlignmentStringTriple>(ruleExtracter, ruleLabelsSmoother);
	}

	public static TranslationRuleSourceFilteredExtractionThread<SAMTQuadruple> createExtractorThreadFactorySAMT(RuleExtracter<EbitgLexicalizedInference, SAMTQuadruple> ruleExtracter,
			RuleLabelsSmoother ruleLabelsSmoother) {
		return new TranslationRuleSourceFilteredExtractionThread<SAMTQuadruple>(ruleExtracter, ruleLabelsSmoother);
	}

	public static TranslationRuleSourceFilteredExtractionThread<SAMTQuadruple> createExtractorThreadFactoryCCL(RuleExtracter<EbitgLexicalizedInferenceCoreContextLabeled, SAMTQuadruple> ruleExtracter,
			RuleLabelsSmoother ruleLabelsSmoother) {
		return new TranslationRuleSourceFilteredExtractionThread<SAMTQuadruple>(ruleExtracter, ruleLabelsSmoother);
	}

	public static TranslationRuleSourceFilteredExtractionThread<BoundaryTagPentuple> createExtractorThreadFactoryBoundaryTagLabeled(
			RuleExtracter<EbitgLexicalizedInferenceBoundaryTagged, BoundaryTagPentuple> ruleExtracter, RuleLabelsSmoother ruleLabelsSmoother) {
		return new TranslationRuleSourceFilteredExtractionThread<BoundaryTagPentuple>(ruleExtracter, ruleLabelsSmoother);
	}

	public LabelProbabilityEstimator getLabelProbabilityEstimator() {
		return this.labelProbabilityEstimator;
	}
}
