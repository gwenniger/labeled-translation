package grammarExtraction.chiangGrammarExtraction;

import extended_bitg.EbitgChartBuilder;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeState;
import grammarExtraction.grammarExtractionFoundation.HeavyWeightPhrasePairRuleSet;
import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;
import grammarExtraction.grammarExtractionFoundation.LightWeightRuleExtractor;
import grammarExtraction.translationRuleTrie.LexicalProbabilityTables;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.TranslationRuleCreater;

import java.util.ArrayList;
import java.util.List;

import boundaryTagging.EbitgLexicalizedInferenceBoundaryTagged;

import coreContextLabeling.EbitgLexicalizedInferenceCoreContextLabeled;

public class HierarchicalPhraseBasedRuleExtractor<T extends EbitgLexicalizedInference> extends LightWeightRuleExtractor<T> {
	private final HeavyWeightChiangRuleExtractor<T, ?> heavyWeightChiangRuleExtractor;

	protected HierarchicalPhraseBasedRuleExtractor(HeavyWeightChiangRuleExtractor<T, ?> heavyWeightChiangRuleExtractor) {
		this.heavyWeightChiangRuleExtractor = heavyWeightChiangRuleExtractor;
	}

	public static <T extends EbitgLexicalizedInference, T2 extends EbitgTreeState<T, T2>> HierarchicalPhraseBasedRuleExtractor<T> createHierarchicalPhraseBasedRulesExtractorChiangPure(
			EbitgChartBuilder<T, T2> chartBuilder, GrammarExtractionConstraints grammarExtractionConstraints, TranslationRuleCreater<T> translationRuleCreater) {
		HeavyWeightChiangRuleExtractor<T, T2> heavyWeightChiangRuleExtgractor = HeavyWeightChiangRuleExtractor.createHeavyWeightChiangRuleExtractor(chartBuilder, grammarExtractionConstraints,
				translationRuleCreater);
		return new HierarchicalPhraseBasedRuleExtractor<T>(heavyWeightChiangRuleExtgractor);
	}

	public static <T extends EbitgLexicalizedInferenceCoreContextLabeled, T2 extends EbitgTreeState<T, T2>> HierarchicalPhraseBasedRuleExtractor<T> createHierarchicalPhraseBasedRulesExtractorChiangCoreContexLabeled(
			EbitgChartBuilder<T, T2> chartBuilder, GrammarExtractionConstraints grammarExtractionConstraints, TranslationRuleCreater<T> translationRuleCreater) {
		HeavyWeightChiangRuleExtractor<T, T2> heavyWeightChiangRuleExtgractor = HeavyWeightChiangRuleExtractor.createHeavyWeightChiangRuleExtractor(chartBuilder, grammarExtractionConstraints,
				translationRuleCreater);
		return new HierarchicalPhraseBasedRuleExtractor<T>(heavyWeightChiangRuleExtgractor);
	}

	public static <T extends EbitgLexicalizedInferenceBoundaryTagged, T2 extends EbitgTreeState<T, T2>> HierarchicalPhraseBasedRuleExtractor<T> createHierarchicalPhraseBasedRulesExtractorChiangBoundaryTagged(
			EbitgChartBuilder<T, T2> chartBuilder, GrammarExtractionConstraints grammarExtractionConstraints, TranslationRuleCreater<T> translationRuleCreater) {
		HeavyWeightChiangRuleExtractor<T, T2> heavyWeightChiangRuleExtgractor = HeavyWeightChiangRuleExtractor.createHeavyWeightChiangRuleExtractor(chartBuilder, grammarExtractionConstraints,
				translationRuleCreater);
		return new HierarchicalPhraseBasedRuleExtractor<T>(heavyWeightChiangRuleExtgractor);
	}

	public List<LightWeightPhrasePairRuleSet> createLightWeightTranslationRuleSetsWithLexicalProabilities(WordKeyMappingTable wordKeyMappingTable, LexicalProbabilityTables lexicalProbabilityTables) {
		List<HeavyWeightPhrasePairRuleSet<T>> heavyWeightSets = heavyWeightChiangRuleExtractor.createHeavyWeightTranslationRuleSets();
		return createLightWeightTranslationRuleSetsWithLexicalProabilities(heavyWeightSets, wordKeyMappingTable, lexicalProbabilityTables);
	}

	public List<LightWeightPhrasePairRuleSet> createLightWeightTranslationRuleSets(WordKeyMappingTable wordKeyMappingTable) {
		List<LightWeightPhrasePairRuleSet> result = new ArrayList<LightWeightPhrasePairRuleSet>();

		List<HeavyWeightPhrasePairRuleSet<T>> heavyWeightSets = heavyWeightChiangRuleExtractor.createHeavyWeightTranslationRuleSets();

		// System.out.println("HierarchicalPhraseBasedRuleExtractor : heavyWeightSets created");

		for (HeavyWeightPhrasePairRuleSet<T> heavyWeightRuleSet : heavyWeightSets) {
			result.add(LightWeightPhrasePairRuleSet.createLightWeightPhrasePairRuleSet(heavyWeightRuleSet, wordKeyMappingTable));
			// System.out.println("HierarchicalPhraseBasedRuleExtractor : light weight converted set added");
		}

		
		//printWordKeyMappingTableContentsForDebugging(wordKeyMappingTable);
		
		return result;
	}
	
	/**
	 * Show the WordKeyMappingTable contents. This was needed for debugging a case of disappearing grammar 
	 * rules, which later turned out to be caused by the external grammar sorting, not the grammar creation.
	 * @param wordKeyMappingTable
	 */
	public void printWordKeyMappingTableContentsForDebugging(WordKeyMappingTable wordKeyMappingTable){
	    System.out.println(">>>>> Inspect contents of WordKeyMappingTable...");		
	    wordKeyMappingTable.printWordToKeyMappingTable();
	    wordKeyMappingTable.printKeyToWordMappingTable();		
            System.out.println("<<<<< Inspect contents of WordKeyMappingTable...");

	}

	public HeavyWeightChiangRuleExtractor<T, ?> getHeavyWeightChiangRuleExtractor() {
		return this.heavyWeightChiangRuleExtractor;
	}
}
