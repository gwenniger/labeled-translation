package grammarExtraction.chiangGrammarExtraction;

import extended_bitg.EbitgLexicalizedInference;
import grammarExtraction.grammarExtractionFoundation.BareTranslationRuleExtracterThread;
import grammarExtraction.grammarExtractionFoundation.ExtractorThreadFactory;
import grammarExtraction.grammarExtractionFoundation.FilterRuleSetCreater;
import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;
import grammarExtraction.grammarExtractionFoundation.MultiThreadGrammarExtractor;
import grammarExtraction.translationRuleTrie.TranslationRuleTrie;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.LightWeightTranslationRule;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import junit.framework.Assert;
import alignment.SourceString;
import alignmentStatistics.InputStringsEnumeration;
import util.Pair;

public class TranslationRuleFilterPreparationThread<InputType extends SourceString> extends BareTranslationRuleExtracterThread<InputType> implements
		ExtractorThreadFactory<TranslationRuleFilterPreparationThread<InputType>, MultiThreadGrammarExtractorTestFiltered<? extends EbitgLexicalizedInference, ? extends SourceString>> {

	FilterRuleSetCreater<InputType> filterRuleSetCreater;
	protected final GrammarExtractionConstraints grammarExtractionConstraints;

	private TranslationRuleFilterPreparationThread(FilterRuleSetCreater<InputType> filterRuleSetCreater, GrammarExtractionConstraints grammarExtractionConstraints) {
		super();
		this.filterRuleSetCreater = filterRuleSetCreater;
		this.grammarExtractionConstraints = grammarExtractionConstraints;
	}

	public TranslationRuleFilterPreparationThread(int maxAllowedInferencesPerNode, Pair<Integer> minMaxSentencePairNum, InputStringsEnumeration<InputType> inputStringsEnumeration,
			String sourceLanguageFileName, int maxSentenceLength, int outputLevel, TranslationRuleTrie translationRuleTrie, WordKeyMappingTable wordKeyMappingTable,
			boolean useJoshuaStyleSimplifiedCountComputing, boolean useCaching, FilterRuleSetCreater<InputType> filterRuleSetCreater, GrammarExtractionConstraints grammarExtractionConstraints) {
		super(maxAllowedInferencesPerNode, minMaxSentencePairNum, inputStringsEnumeration, maxSentenceLength, outputLevel, translationRuleTrie, wordKeyMappingTable,
				useJoshuaStyleSimplifiedCountComputing, useCaching);
		this.filterRuleSetCreater = filterRuleSetCreater;
		this.grammarExtractionConstraints = grammarExtractionConstraints;
	}

	public void performLoopCorpusComputation() {
		try {
			// int numSentencePairs = FileStatistics.countLines(alignmentsFileName);
			int numSentencePairs = (this.maxSentencePairNum - this.minSentencePairNum) + 1;

			long timeStart = System.currentTimeMillis();

			doBeforeLooping();

			int sentencePairNum = 1;
			// While we manage to read a next source and target sentence and an alignment

			Assert.assertTrue(this.inputStringEnumeration.hasMoreElements());
			while (this.inputStringEnumeration.hasMoreElements()) {
				InputType nextElement = this.inputStringEnumeration.nextElement();

				if (sentencePairNum < minSentencePairNum) {
					System.out.println("TranslationRuleFilterPreparationThread - Skipping sentence pair num > too small");
					sentencePairNum++;
					continue;
				}

				if (sentencePairNum > maxSentencePairNum) {
					System.out.println("TranslationRuleFilterPreparationThread - Skipping sentence pair num > too big");
					break;
				}

				String sourceSentence = nextElement.getSourceString();
				System.out.println("TranslationRuleFilterPreparationThread. performLoopCorpusComputation");
				System.out.println("sourceSentence" + sentencePairNum + " " + sourceSentence);
				showOutputForSelectedSentencePair(sentencePairNum, "Thread ID: " + Thread.currentThread().getId());
				showOutputForSelectedSentencePair(sentencePairNum, "}}}Processing sentence pair " + sentencePairNum);
				showOutputForSelectedSentencePair(sentencePairNum, "sourceSentence: " + sourceSentence);

				performCorpusComputation(nextElement, sentencePairNum);

				int processedSentencePairNum = sentencePairNum - this.minSentencePairNum + 1;
				showOutputForSelectedSentencePair(processedSentencePairNum, progressTimeString(timeStart, processedSentencePairNum, numSentencePairs));

				sentencePairNum++;

				showOutputForSelectedSentencePair(sentencePairNum, "Heapsize end iteration:");
				showOutputForSelectedSentencePair(sentencePairNum, heapSize());

			}

			// writeResults(outputWriter);

			// close all the readers and writers
			doAfterLooping();

			showOutputForSelectedSentencePair(sentencePairNum, "Heapsize at termination");
			showOutputForSelectedSentencePair(sentencePairNum, heapSize());

			Thread.currentThread();
			// Allow other threads to execute
			// yield();
			Thread.sleep((long) 1);

			System.out.println("TranslationRuleFilterPreparationThread finished");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println(heapSize());
	}

	protected void processPhrasePairRuleSet(LightWeightPhrasePairRuleSet ruleSet) {
		getTranslationRuleTrie().addPhrasePairRuleSetToTableWithoutAddingCounts(ruleSet);
	}

	
	@SuppressWarnings("unused")
	private final void showFilterRules(List<LightWeightPhrasePairRuleSet> ruleSets)
	{
		System.out.println("TranslationRuleFilterPreparationThread.extractRuleSets()");
		for(LightWeightPhrasePairRuleSet ruleSet : ruleSets)
		{
			//System.out.println("TranslationRuleFilterPreparationThread.extractRuleSets() - ruleSet: " + ruleSet);
			for(LightWeightTranslationRule  rule: ruleSet.getRules())
			{
				System.out.println("Filter rule: "+ rule.getJoshuaRuleRepresentation(wordKeyMappingTable));
			}
		}
	}
	
	
	@Override
	protected List<LightWeightPhrasePairRuleSet> extractRuleSets(InputType input) {
		List<LightWeightPhrasePairRuleSet>  result =  filterRuleSetCreater.produceSoureSideConsistentRuleSets(input, getWordKeyMappingTable(), this.grammarExtractionConstraints);
		//showFilterRules(result);
		return result;
	}

	@Override
	public TranslationRuleFilterPreparationThread<InputType> createExtractorThread(Pair<Integer> minMaxSentencePairNum,
			MultiThreadGrammarExtractorTestFiltered<? extends EbitgLexicalizedInference, ? extends SourceString> multiThreadGrammarExtractor) {

		return new TranslationRuleFilterPreparationThread<InputType>(multiThreadGrammarExtractor.parameters.getMaxAllowedInferencesPerNode(), minMaxSentencePairNum,
				this.filterRuleSetCreater.getFilterSentencesEnumeration(), multiThreadGrammarExtractor.getGrammarSourceFilterFileName(), multiThreadGrammarExtractor.parameters.getMaxSentenceLength(),
				multiThreadGrammarExtractor.parameters.getOutputLevel(), multiThreadGrammarExtractor.getTranslationRuleCountTable().getTranslationRuleTrie(), multiThreadGrammarExtractor
						.getTranslationRuleCountTable().getWordKeyMappingTable(), MultiThreadGrammarExtractor.UseJoshuaStyleSimplifiedCountComputing,
				multiThreadGrammarExtractor.parameters.getUseCaching(), filterRuleSetCreater, multiThreadGrammarExtractor.getChiangGrammarExtractionConstraints());
	}

	public static <InputType extends SourceString> TranslationRuleFilterPreparationThread<InputType> createExtractorThreadFactoryBasic(
			GrammarExtractionConstraints grammarExtractionConstraints, FilterRuleSetCreater<InputType> filterRuleSetCreater) {
		return new TranslationRuleFilterPreparationThread<InputType>(filterRuleSetCreater, grammarExtractionConstraints);
	}

}
