package grammarExtraction.chiangGrammarExtraction;

import java.util.List;

import alignment.SourceString;
import alignmentStatistics.InputStringsEnumeration;

import util.InputReading;
import grammarExtraction.grammarExtractionFoundation.FilterRuleSetCreater;
import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;
import grammarExtraction.grammarExtractionFoundation.MonotoneFilterRulesetCreater;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

public class HieroFilterRuleSetCreater implements FilterRuleSetCreater<SourceString> {

	private final String sourceFileName;

	public HieroFilterRuleSetCreater(String sourceLanguageFileName) {
		this.sourceFileName = sourceLanguageFileName;
	}

	@Override
	public List<LightWeightPhrasePairRuleSet> produceSoureSideConsistentRuleSets(SourceString input, WordKeyMappingTable wordKeyMappingTable,
			GrammarExtractionConstraints chiangGrammarExtractionConstraints) {
		MonotoneFilterRulesetCreater ruleCreater = MonotoneFilterRulesetCreater.createMonotoneFilterRulesetCreater(InputReading.getWordsFromString(input.getSourceString()), wordKeyMappingTable,
				chiangGrammarExtractionConstraints);
		return ruleCreater.produceSoureSideConsistentRuleSets();
	}

	@Override
	public InputStringsEnumeration<SourceString> getFilterSentencesEnumeration() {
		return SourceStringEnumeration.createSourceStringEnumeration(sourceFileName);
	}

}
