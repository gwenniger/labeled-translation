package grammarExtraction.chiangGrammarExtraction;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import util.ConfigFile;
import grammarExtraction.translationRules.RuleLabelCreater;
import grammarExtraction.translationRules.RuleLabelCreaterFactory;

public abstract class LabelSideSettings implements Serializable {

    	/**
         * 
         */
    	private static final long serialVersionUID = 1L;
	public static String LABEL_SIDE_SETTING_PROPERTY = "labelSideSetting";
	public static String LABEL_BOTH_SIDES_STRING = "labelBothSides";
	public static String LABEL_SOURCE_SIDE_STRING = "labelSourceSide";
	public static String LABEL_TARGET_SIDE_STRING = "labelTargetSide";
	private static List<String> LABEL_SIDE_SETTING_OPTIONS_LIST = Arrays.asList(LABEL_SOURCE_SIDE_STRING, LABEL_TARGET_SIDE_STRING, LABEL_BOTH_SIDES_STRING);

	private LabelSideSettings() {
	}

	private static String getPermissibleLabelSideSettingsString() {
		String result = "Please select for the property " + LABEL_SIDE_SETTING_PROPERTY + " one of the following values:\n";
		for (String labelSideSetting : LABEL_SIDE_SETTING_OPTIONS_LIST) {
			result += labelSideSetting + "\n";
		}
		return result;
	}

	public static LabelSideSettings getLabelSideSettingsFromConfig(ConfigFile theConfig) {
		String labelSideSettingsString = theConfig.getValueWithPresenceCheck(LABEL_SIDE_SETTING_PROPERTY);

		if (labelSideSettingsString.equals(LABEL_BOTH_SIDES_STRING)) {
			return new LabelBothSides();
		} else if (labelSideSettingsString.equals(LABEL_SOURCE_SIDE_STRING)) {
			return new LabelsSourceSide();
		} else if (labelSideSettingsString.equals(LABEL_TARGET_SIDE_STRING)) {
			return new LabelsTargetSide();
		} else {
			throw new RuntimeException("Error: " + LABEL_SIDE_SETTING_PROPERTY + " - invalid labeling settings" + "\n" + getPermissibleLabelSideSettingsString());
		}
	}

	public static LabelSideSettings createLabelSideSettingsBothSides() {
		return new LabelBothSides();
	}

	public abstract RuleLabelCreater createRuleLabelCreater();

	public abstract boolean labelSourceSide();

	public abstract boolean labelTargetSide();

	public static class LabelBothSides extends LabelSideSettings {
		@Override
		public RuleLabelCreater createRuleLabelCreater() {
			return RuleLabelCreaterFactory.createBothSidesLabeler();
		}

		@Override
		public boolean labelSourceSide() {
			return true;
		}

		@Override
		public boolean labelTargetSide() {
			return true;
		}
	}

	public static class LabelsSourceSide extends LabelSideSettings {
		@Override
		public RuleLabelCreater createRuleLabelCreater() {
			return RuleLabelCreaterFactory.createSourceSideLabeler();
		}

		@Override
		public boolean labelSourceSide() {
			return true;
		}

		@Override
		public boolean labelTargetSide() {
			return false;
		}
	}

	public static class LabelsTargetSide extends LabelSideSettings {
		@Override
		public RuleLabelCreater createRuleLabelCreater() {
			return RuleLabelCreaterFactory.createTargetSideLabeler();
		}

		@Override
		public boolean labelSourceSide() {
			return false;
		}

		@Override
		public boolean labelTargetSide() {
			return true;
		}
	}

}
