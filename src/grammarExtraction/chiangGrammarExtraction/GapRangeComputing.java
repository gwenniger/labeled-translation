package grammarExtraction.chiangGrammarExtraction;

import util.Span;
import java.util.ArrayList;
import java.util.List;

import util.Pair;

public class GapRangeComputing {

	private final GrammarExtractionConstraints grammarExtractionConstraints;

	private GapRangeComputing(GrammarExtractionConstraints grammarExtractionConstraints) {
		this.grammarExtractionConstraints = grammarExtractionConstraints;
	}

	public static GapRangeComputing createGapRangeComputing(GrammarExtractionConstraints chiangGrammarExtractionConstraints) {
		return new GapRangeComputing(chiangGrammarExtractionConstraints);
	}

	public int getMaxTerminalsPlusNonTerminalsSourceSide() {
		return grammarExtractionConstraints.getMaxTerminalsPlusNonTerminalsSourceSide();
	}

	public int getMaxSourceAndTargetLength() {
		return grammarExtractionConstraints.getMaxSourceAndTargetLength();
	}

	public int getMinNonTerminalSpan() {
		return grammarExtractionConstraints.getMinNonTerminalSpan();
	}

	public int getMinNonTerminalSpanSourceFilter() {
		return grammarExtractionConstraints.getMinNonTerminalSpanSourceFilter();
	}

	public int minimalSpaceBetweenGaps() {
		if (grammarExtractionConstraints.allowConsecutiveNonterminalsInNonAbstractRules()) {
			return 0;
		}
		return 1;
	}

	public int minSummedGapsLength(int sourceLength, int noGaps) {
		return Math.max(noGaps * getMinNonTerminalSpan(), (sourceLength - getMaxTerminalsPlusNonTerminalsSourceSide()) + noGaps);
	}

	private static int maxSummedGapsLengthOneGap(int sourceLength) {
		// With only one gap, we always require one terminal to avoid unary
		// rules that don't converge
		return sourceLength - 1;
	}

	private int maxSummedGapsLengthMultipleGapsForNonAbstractRules(int sourceLength) {
		return sourceLength - 1;
	}

	private boolean gapRangesCorrespondToAbstractRule(Pair<Span> gapRanges, Span sourceSpan) {
		Span firstGap = gapRanges.getFirst();
		Span secondGap = gapRanges.getSecond();
		return ((sourceSpan.getFirst() == firstGap.getFirst()) && (sourceSpan.getSecond() == secondGap.getSecond()) && (secondGap.getFirst() == (firstGap
				.getSecond() + 1)));
	}

	private List<Pair<Span>> findPermissibleTwoGapSourceRanges(Span sourceSpan, int firstGapLength, int secondGapLength) {
		List<Pair<Span>> result = new ArrayList<Pair<Span>>();

		// System.out.println(" findPermissibleTwoGapSourceRanges" + sourceSpan
		// + " ," + firstGapLength + " ," + secondGapLength);

		for (int firstGapBegin = sourceSpan.getFirst(); firstGapBegin <= maxGapBegin(sourceSpan, firstGapLength, secondGapLength); firstGapBegin++) {
			Span firstGapRange = makeGapRange(firstGapBegin, firstGapLength);

			for (int secondGapBegin = firstGapBegin + firstGapLength + minimalSpaceBetweenGaps(); secondGapBegin <= (sourceSpan.getSecond() - secondGapLength) + 1; secondGapBegin++) {

				Span secondGapRange = makeGapRange(secondGapBegin, secondGapLength);
				Pair<Span> gapRanges = new Pair<Span>(firstGapRange, secondGapRange);
				if (!gapRangesCorrespondToAbstractRule(gapRanges, sourceSpan)) {
					result.add(gapRanges);
				}

			}
		}
		return result;
	}

	private static Span makeGapRange(int gapBegin, int gapLength) {
		return new Span(gapBegin, (gapBegin + gapLength) - 1);
	}

	private int maxGapBegin(Span sourceSpan, int firstGapLength, int secondGapLength) {
		int totalGapsLength = firstGapLength + secondGapLength;
		final int maxGapBegin = sourceSpan.getSecond() - totalGapsLength - minimalSpaceBetweenGaps() + 1;
		return maxGapBegin;
	}

	protected List<Pair<Span>> findPermissibleTwoGapNonAbstractRuleSourceRanges(Span sourceSpan) {
		List<Pair<Span>> result = new ArrayList<Pair<Span>>();

		for (int summedGapLengths = minSummedGapsLength(sourceSpan.getSpanLength(), 2); summedGapLengths <= maxSummedGapsLengthMultipleGapsForNonAbstractRules(sourceSpan
				.getSpanLength()); summedGapLengths++) {
			for (int firstGapLength = getMinNonTerminalSpan(); firstGapLength <= (summedGapLengths - getMinNonTerminalSpan()); firstGapLength++) {
				int secondGapLength = summedGapLengths - firstGapLength;

				result.addAll(findPermissibleTwoGapSourceRanges(sourceSpan, firstGapLength, secondGapLength));
			}
		}
		return result;
	}

	public List<Pair<Span>> findPermissibleTwoGapSourceRanges(Span sourceSpan) {
		List<Pair<Span>> result = findPermissibleTwoGapNonAbstractRuleSourceRanges(sourceSpan);

		// We also add the split points for purely abstract rules on purpose.
		// The reason is that we use this method to get all span pairs for all
		// Hiero rules
		// including abstract rules
		if (this.grammarExtractionConstraints.allowSourceAbstract()) {
			result.addAll(findFullyAbstractRulesPermissibleSplitRanges(sourceSpan));
		}

		return result;
	}

	private Pair<Span> createTwoSpanPartition(Span originalSpan, int firstPartLength) {
		Span firstPartSpan = makeGapRange(originalSpan.getFirst(), firstPartLength);
		Span secondPartSpan = makeGapRange(originalSpan.getFirst() + firstPartLength, originalSpan.getSpanLength() - firstPartLength);
		return new Pair<Span>(firstPartSpan, secondPartSpan);
	}

	public List<Pair<Span>> findFullyAbstractRulesPermissibleSplitRanges(Span sourceSpan) {
		List<Pair<Span>> result = new ArrayList<Pair<Span>>();

		for (int firstSpanLenth = 1; firstSpanLenth < sourceSpan.getSpanLength(); firstSpanLenth++) {
			Pair<Span> partition = createTwoSpanPartition(sourceSpan, firstSpanLenth);
			result.add(partition);
		}

		return result;
	}

	public List<Span> findPermissableOneGapSourceRanges(Span sourceSpan) {
		List<Span> result = new ArrayList<Span>();
		int sourceLength = sourceSpan.getSpanLength();

		int minimalGapLength = Math.max(minSummedGapsLength(sourceLength, 1), getMinNonTerminalSpan());

		for (int gapLength = minimalGapLength; gapLength <= GapRangeComputing.maxSummedGapsLengthOneGap(sourceLength); gapLength++) {
			for (int gapBegin = sourceSpan.getFirst(); gapBegin <= (sourceSpan.getSecond() - gapLength) + 1; gapBegin++) {
				Span gapSourceRange = new Span(gapBegin, (gapBegin + gapLength) - 1);
				result.add(gapSourceRange);
			}
		}
		return result;
	}

}
