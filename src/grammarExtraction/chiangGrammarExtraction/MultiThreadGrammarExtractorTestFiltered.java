package grammarExtraction.chiangGrammarExtraction;

import extended_bitg.EbitgLexicalizedInference;
import grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagFilterRuleSetCreater;
import grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagLabeledRuleExtracter;
import grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagLabeledRuleExtracterThreadFactoryCreater;
import grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagLabeledRuleGapCreater;
import grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagLabelingSettings;
import grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagPentuple;
import grammarExtraction.coreContextLabelGrammarExtraction.CCLExtracterThreadFactoryCreater;
import grammarExtraction.coreContextLabelGrammarExtraction.CCLRuleExtracter;
import grammarExtraction.coreContextLabelGrammarExtraction.CCLRuleGapCreater;
import grammarExtraction.grammarExtractionFoundation.ExtracterThreadFactoryCreater;
import grammarExtraction.grammarExtractionFoundation.FilterRuleSetCreater;
import grammarExtraction.grammarExtractionFoundation.MultiThreadGrammarExtractor;
import grammarExtraction.grammarExtractionFoundation.RuleExtracter;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.grammarExtractionFoundation.TranslationRuleExtractorThread;
import grammarExtraction.grammarExtractionFoundation.LabelingSettings.CanonicalFormSettings;
import grammarExtraction.labelSmoothing.EmptyAlternativelyLabeledRuleSetsListCreater;
import grammarExtraction.labelSmoothing.LabeledRulesSmoother;
import grammarExtraction.labelSmoothing.NoLabelSmoothingRulesSmoother;
import grammarExtraction.labelSmoothing.RuleLabelsSmoother;
import grammarExtraction.samtGrammarExtraction.ComplexLabelsGrammarExtractionParameters;
import grammarExtraction.samtGrammarExtraction.LabelProbabilityEstimatorSAMT;
import grammarExtraction.samtGrammarExtraction.SAMTExtracterThreadFactoryCreater;
import grammarExtraction.samtGrammarExtraction.SAMTGrammarExtractionConstraints;
import grammarExtraction.samtGrammarExtraction.SAMTQuadruple;
import grammarExtraction.samtGrammarExtraction.UnknownWordRulesCreator;
import grammarExtraction.samtGrammarExtraction.UnknownWordRulesCreatorEmpty;
import grammarExtraction.samtGrammarExtraction.UnknownWordRulesCreatorPosTags;
import grammarExtraction.translationRuleTrie.LabelProbabilityEstimator;
import grammarExtraction.translationRuleTrie.LexicalProbabilityTables;
import grammarExtraction.translationRuleTrie.TranslationRuleCountTable;
import grammarExtraction.translationRuleTrie.TranslationRuleTrie;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import junit.framework.Assert;
import boundaryTagging.EbitgLexicalizedInferenceBoundaryTagged;
import coreContextLabeling.CoreContextLabelsTable;
import coreContextLabeling.EbitgLexicalizedInferenceCoreContextLabeled;
import mt_pipeline.GrammarExtracterCreaterHatsHiero;
import mt_pipeline.MTPipelineHatsCCL;
import mt_pipeline.MTPipelineHatsHiero;
import mt_pipeline.mt_config.MTConfigFile;
import mt_pipeline.mt_config.MTDecoderConfig;
import mt_pipeline.mt_config.MTGrammarExtractionConfig;
import ccgLabeling.LabelGenerator.UnaryCategoryHandlerType;
import util.ConfigFile;
import util.FileStatistics;
import util.Pair;
import alignment.AlignmentStringTriple;
import alignmentStatistics.MultiThreadComputationParameters;

public class MultiThreadGrammarExtractorTestFiltered<T extends EbitgLexicalizedInference, InputType extends AlignmentStringTriple> extends
		MultiThreadGrammarExtractor {

	private final String grammarSourceFilterFileName;
	private final RuleExtracter<T, InputType> ruleExtracter;
	private final ExtracterThreadFactoryCreater<T, InputType> extracterThreadFactoryCreater;
	private final FilterRuleSetCreater<?> filterRuleSetCreater;

	// Whether extra SAMT features are to be used in the produced grammar
	private final boolean useExtraSAMTFeatures;

	protected MultiThreadGrammarExtractorTestFiltered(MultiThreadComputationParameters parameters, TranslationRuleCountTable translationRuleProbabilityTable,
			String grammarSourceFilterFileName, GrammarExtractionConstraints chiangGrammarExtractionConstraints, RuleExtracter<T, InputType> ruleExtracter,
			ExtracterThreadFactoryCreater<T, InputType> extracterThreadfactoryCreater2, UnknownWordRulesCreator unknownWordsRulesCreator,
			boolean writeProbabilityTablesToFiles, boolean printProbabilityTables, boolean useExtraSAMTFeatures, FilterRuleSetCreater<?> filterRuleSetCreater,
			SystemIdentity systemIdentity) {
		super(parameters, translationRuleProbabilityTable, chiangGrammarExtractionConstraints, unknownWordsRulesCreator, writeProbabilityTablesToFiles,
				printProbabilityTables, systemIdentity);

		this.grammarSourceFilterFileName = grammarSourceFilterFileName;
		this.ruleExtracter = ruleExtracter;
		this.extracterThreadFactoryCreater = extracterThreadfactoryCreater2;
		this.useExtraSAMTFeatures = useExtraSAMTFeatures;
		this.filterRuleSetCreater = filterRuleSetCreater;
	}

	private static TranslationRuleCountTable createTranslationRuleCountTable(MultiThreadComputationParameters parameters, String filterFileName,
			LabelProbabilityEstimator labelProbabilityEstimator, int noConcurrentThreads, RuleLabelsSmoother ruleLabelsSmoother,
			boolean filterUsingSourceSideRuleLabels,CanonicalFormSettings canonicalFormSettings) {
		// The word file names are the source file, the target file and the
		// testFilter file
		// List<String> wordFileNames =
		// Arrays.asList(parameters.getSourceFileName(),
		// parameters.getTargetFileName(), filterFileName);
		WordKeyMappingTable wordKeyMappingTable = ruleLabelsSmoother.getWordKeyMappingTable();

		Assert.assertNotNull(wordKeyMappingTable);
		TranslationRuleTrie translationRuleTrie = TranslationRuleTrie.createTranslationRuleTrie(wordKeyMappingTable, UseJoshuaStyleSimplifiedCountComputing,
				filterUsingSourceSideRuleLabels,canonicalFormSettings);
		LexicalProbabilityTables lexicalProbabilityTables = LexicalProbabilityTables.createLexicalProbabilityTables(wordKeyMappingTable,
				parameters.getNumThreads());
		TranslationRuleCountTable translationRuleCountTable = TranslationRuleCountTable.createTranslationRuleCountTable(translationRuleTrie,
				lexicalProbabilityTables, wordKeyMappingTable, labelProbabilityEstimator, ruleLabelsSmoother);

		return translationRuleCountTable;
	}

	

	private static LabelProbabilityEstimator createLabelProbabilityEstimator(GrammarExtractionConstraints grammarExtractionConstraints,ConfigFile configFile) {
		LabelProbabilityEstimator labelProbabilityEstimator;
		
		if((grammarExtractionConstraints.useReorderingLabelExtension())){
		    // Labeled rule, so refer to  createLabelProbabilityEstimatorForLabeledSystem
		   return createLabelProbabilityEstimatorForLabeledSystem(configFile); 
		}		
		else{
		    // Unlabeled Hiero rule, so use LabelProbabilityEstimatorHiero
		    labelProbabilityEstimator = LabelProbabilityEstimatorHiero.createLabelProbabilityEstimatorHiero();						
		} 
		return labelProbabilityEstimator;
	}

	private static void assertCanonicalFormLabeledRulesAreUsed(ConfigFile configFile){
	 // Sanity check: check that also canonical form labeled rules are used
	    if(!MTGrammarExtractionConfig.useCanonicalFormLabeledRules(configFile)){
		String errorMessage = "Error in: MultiThreadGrammarExtractorTestFiltered.createLabelProbabilityEstimatorForLabeledSystem"
			+ ": trying to use \"useOnlyHieroWeightsForCanonicalFormLabeledRules=true\", without using \"useCanonicalFormLabeledRules=true\"";
		throw new RuntimeException(errorMessage);
	    }	    
	}
	
	/**
	 * Create a LabelProbabilityEstimator for a labeled system.
	 * If fuzzy matching is used in combination with using only Hiero weights 
	 * for canonical form labeled rules
	 * 
	 * @param configFile
	 * @return
	 */
	private static LabelProbabilityEstimator createLabelProbabilityEstimatorForLabeledSystem(ConfigFile configFile) {
		LabelProbabilityEstimator labelProbabilityEstimator;
			
		if(MTDecoderConfig.useFuzzyMAtching(configFile) && MTGrammarExtractionConfig.useOnlyHieroWeightsForCanonicalFormLabeledRules(configFile)){
		    assertCanonicalFormLabeledRulesAreUsed(configFile);		    
		    // Use fuzzy matching with weights from Hiero rules for canonical form labeled rules 
		    labelProbabilityEstimator = LabelProbabilityEstimatorHiero.createLabelProbabilityEstimatorHiero();		    
		}
		else{
		    boolean useNonUniformWeightedGlueRules = configFile
				.getBooleanWithPresenceCheck(SAMTGrammarExtractionConstraints.USE_NON_UNINIFORM_GLUE_RULE_WEIGHTS_PROPERTY);
		    labelProbabilityEstimator = LabelProbabilityEstimatorSAMT.createLabelProbabilityEstimatorSAMT(useNonUniformWeightedGlueRules);
		}				
		return labelProbabilityEstimator;
	}
	
	private static UnknownWordRulesCreator createUnknownWordRulesCreator(GrammarExtractionConstraints grammarExtractionConstraints,
			WordKeyMappingTable wordKeyMappingTable, SystemIdentity systemIdentity) {
		{
			if (grammarExtractionConstraints.useReorderingLabelExtension()) {
				return UnknownWordRulesCreatorPosTags.createUnknownWordRulesCreatorPosTags(wordKeyMappingTable, systemIdentity);
			} else {
				return UnknownWordRulesCreatorEmpty.createUnknownWordRulesCreatorEmpty();
			}
		}
	}

	private static MultiThreadGrammarExtractor createMultiThreadGrammarExtractorTestFiltered(ConfigFile configFile, int numThreads,
			MultiThreadComputationParameters parameters, GrammarExtractionConstraints grammarExtractionConstraints,
			RuleExtracter<EbitgLexicalizedInference, AlignmentStringTriple> ruleExtracter, FilterRuleSetCreater<?> filterRuleSetCreater,
			SystemIdentity systemIdentity) {
		// The word file names are the source file, the target file and the
		// testFilter file
		String filterFileName = MTGrammarExtractionConfig.getGrammerSourceFilterPath(configFile);

		RuleLabelsSmoother ruleLabelsSmoother = LabeledRulesSmoother.createHieroRuleLabelsSmoother(configFile, parameters, grammarExtractionConstraints,systemIdentity);
		Assert.assertNotNull(ruleLabelsSmoother.getWordKeyMappingTable());
		// NoLabelSmoothingRulesSmoother ruleSmoother =
		// NoLabelSmoothingRulesSmoother.createNoLabelSmoothingRulesSmootherForFilteredGrammarExtraction(parameters,
		// getFilterFilePath(configFile));
		TranslationRuleCountTable translationRuleCountTable = createTranslationRuleCountTable(parameters, filterFileName,
				createLabelProbabilityEstimator(grammarExtractionConstraints,configFile), parameters.getNumThreads(), ruleLabelsSmoother,
				MTGrammarExtractionConfig.useSourceSideLabelsInRuleFiltering(configFile),systemIdentity.getCanonicalFormSettings());
		boolean useExtraSAMTFeatures = configFile.getBooleanWithPresenceCheck(SAMTGrammarExtractionConstraints.USE_EXTRA_SAMT_FEATURES_PROPERTY);
		return new MultiThreadGrammarExtractorTestFiltered<EbitgLexicalizedInference, AlignmentStringTriple>(parameters, translationRuleCountTable,
				filterFileName, grammarExtractionConstraints, ruleExtracter, new ChiangExtracterThreadFactoryCreater(ruleLabelsSmoother),
				createUnknownWordRulesCreator(grammarExtractionConstraints, translationRuleCountTable.getWordKeyMappingTable(), systemIdentity), false, false,
				useExtraSAMTFeatures, filterRuleSetCreater, systemIdentity);

	}

	

	public static MultiThreadGrammarExtractor createMultiThreadGrammarExtractorTestFilteredHiero(String configFileName, int numThreads,
			SystemIdentity systemIdentity) {

		ConfigFile configFile;
		try {
			configFile = new ConfigFile(configFileName);
			MTGrammarExtractionConfig grammarExtractionConfig = MTGrammarExtractionConfig.createMTGrammarExtractionConfig(configFileName);
			MultiThreadComputationParameters parameters = MultiThreadComputationParameters.createMultiThreadComputationParameters(configFileName, numThreads);

			SAMTGrammarExtractionConstraints samtGrammarExtractionConstraints;
			ComplexLabelsGrammarExtractionParameters complexLabelGrammarExtractionParameters;

			if (grammarExtractionConfig.useReorderingLabelExtension()) {
				complexLabelGrammarExtractionParameters = ComplexLabelsGrammarExtractionParameters.createComplexLabelsGrammarExtractionParameters(
						configFileName, numThreads);
				samtGrammarExtractionConstraints = complexLabelGrammarExtractionParameters.samtGrammarExtractionConstraints;

			} else {
				samtGrammarExtractionConstraints = SAMTGrammarExtractionConstraints.createSAMTGrammarExtractionConstraints(
						systemIdentity.getMaxSourceAndTargetLength(),
						grammarExtractionConfig.allowDanglingSecondNonterminal(), false, false, false, false, false,
						grammarExtractionConfig.getReorderinLabelSettings(),grammarExtractionConfig.getLabelSideSettings(), UnaryCategoryHandlerType.BOTTOM);
			}

			ChiangRuleExtracter chiangRuleExtracter = TranslationRuleExtractorThread.createChiangRuleExtracter(parameters, samtGrammarExtractionConstraints);
			return createMultiThreadGrammarExtractorTestFiltered(configFile, numThreads, parameters, samtGrammarExtractionConstraints, chiangRuleExtracter,
					new HieroFilterRuleSetCreater(MTGrammarExtractionConfig.getGrammerSourceFilterPath(configFile)), systemIdentity);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	public static MultiThreadGrammarExtractor createMultiThreadGrammarExtractorTestFilteredSAMT(String configFileName, int numThreads,
			SystemIdentity systemIdentity) {

		ConfigFile configFile;
		try {
			configFile = new ConfigFile(configFileName);
			System.out.println("ConfigFileName: " + configFileName);
			ComplexLabelsGrammarExtractionParameters complexLabelGrammarExtractionParameters = ComplexLabelsGrammarExtractionParameters
					.createComplexLabelsGrammarExtractionParameters(configFileName, numThreads);
			RuleExtracter<EbitgLexicalizedInference, SAMTQuadruple> samtRuleExtracter = TranslationRuleExtractorThread.createSAMTRuleExtracter(
					complexLabelGrammarExtractionParameters.parameters, complexLabelGrammarExtractionParameters.samtGrammarExtractionConstraints);

			RuleLabelsSmoother ruleLabelsSmoother = LabeledRulesSmoother.createSAMTRuleLabelsSmoother(configFile, complexLabelGrammarExtractionParameters,systemIdentity);
			Assert.assertNotNull(ruleLabelsSmoother.getWordKeyMappingTable());
			TranslationRuleCountTable translationRuleCountTable = createTranslationRuleCountsTable(configFile, complexLabelGrammarExtractionParameters,
					ruleLabelsSmoother,systemIdentity.getCanonicalFormSettings());
			return createMultiThreadGrammarExtractorTestFilteredComplexLabels(configFile, numThreads, samtRuleExtracter, new SAMTExtracterThreadFactoryCreater(
					ruleLabelsSmoother), complexLabelGrammarExtractionParameters, new HieroFilterRuleSetCreater(MTGrammarExtractionConfig.getGrammerSourceFilterPath(configFile)),
					translationRuleCountTable, systemIdentity);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	private static TranslationRuleCountTable createTranslationRuleCountsTable(ConfigFile configFile,
			ComplexLabelsGrammarExtractionParameters complexLabelGrammarExtractionParameters, RuleLabelsSmoother ruleLabelsSmoother, CanonicalFormSettings canonicalFormSettings) {
		LabelProbabilityEstimator labelProbabilityEstimator = createLabelProbabilityEstimatorForLabeledSystem(configFile);
		TranslationRuleCountTable translationRuleCountTable = createTranslationRuleCountTable(complexLabelGrammarExtractionParameters.parameters,
				MTGrammarExtractionConfig.getGrammerSourceFilterPath(configFile), labelProbabilityEstimator, complexLabelGrammarExtractionParameters.parameters.getNumThreads(),
				ruleLabelsSmoother, MTGrammarExtractionConfig.useSourceSideLabelsInRuleFiltering(configFile),canonicalFormSettings);
		return translationRuleCountTable;
	}

	public static MultiThreadGrammarExtractor createMultiThreadGrammarExtractorTestFilteredBoundaryTagLabeled(String configFileName, int numThreads,
			SystemIdentity systemIdentity) {

		ConfigFile configFile;
		try {
			configFile = new ConfigFile(configFileName);
			System.out.println("ConfigFileName: " + configFileName);
			ComplexLabelsGrammarExtractionParameters complexLabelGrammarExtractionParameters = ComplexLabelsGrammarExtractionParameters
					.createComplexLabelsGrammarExtractionParameters(configFileName, numThreads);

			RuleExtracter<EbitgLexicalizedInferenceBoundaryTagged, BoundaryTagPentuple> boundaryTaggedRuleExtracter;

			boundaryTaggedRuleExtracter = BoundaryTagLabeledRuleExtracter.createBoundaryTagLabeledRuleExtracter(complexLabelGrammarExtractionParameters,
					BoundaryTagLabeledRuleGapCreater.createBoundaryTagLabeledRuleGapCreaterFromConfig(new ConfigFile(configFileName),
							complexLabelGrammarExtractionParameters.samtGrammarExtractionConstraints.getReorderLabelingSettings(),complexLabelGrammarExtractionParameters.grammarExtractionConfig.getLabelSideSettings().createRuleLabelCreater()), BoundaryTagLabelingSettings
							.createBoundaryTagLabelingSettingsFromConfig(configFile));

			RuleLabelsSmoother ruleLabelsSmoother = LabeledRulesSmoother.createBoundaryTagRuleLabelsSmoother(configFile,
					complexLabelGrammarExtractionParameters,systemIdentity);
			Assert.assertNotNull(ruleLabelsSmoother.getWordKeyMappingTable());
			TranslationRuleCountTable translationRuleCountTable = createTranslationRuleCountsTable(configFile, complexLabelGrammarExtractionParameters,
					ruleLabelsSmoother,systemIdentity.getCanonicalFormSettings());
			return createMultiThreadGrammarExtractorTestFilteredComplexLabels(configFile, numThreads, boundaryTaggedRuleExtracter,
					new BoundaryTagLabeledRuleExtracterThreadFactoryCreater(ruleLabelsSmoother), complexLabelGrammarExtractionParameters,
					new BoundaryTagFilterRuleSetCreater(configFile, MTGrammarExtractionConfig.getGrammerSourceFilterPath(configFile), MTGrammarExtractionConfig.getSourceTagFilePath(configFile)),
					translationRuleCountTable, systemIdentity);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	private static int getMaxCoreContextLabelAmbiguityRank(ConfigFile configFile) {
		String maxCCLRankString = configFile.getValueWithPresenceCheck(CCLRuleGapCreater.MaxiumAllowedCoreContextLabelAmbituityRankProperty);
		int result = Integer.parseInt(maxCCLRankString);
		return result;
	}

	public static MultiThreadGrammarExtractor createMultiThreadGrammarExtractorTestFilteredCCL(String configFileName, int numThreads) {

		System.out.println("ConfigFileName: " + configFileName);

		ConfigFile configFile;
		try {
			configFile = new ConfigFile(configFileName);
			ComplexLabelsGrammarExtractionParameters complexLabelGrammarExtractionParameters = ComplexLabelsGrammarExtractionParameters
					.createComplexLabelsGrammarExtractionParameters(configFileName, numThreads);

			CoreContextLabelsTable coreContextLabelsTable = CoreContextLabelsTable.getCoreContextLabelsTableFromSerializedFile(CoreContextLabelsTable
					.getCoreContextLabelsTableFileName(complexLabelGrammarExtractionParameters.configFile));

			CCLRuleGapCreater cclRuleGapCreater = CCLRuleGapCreater.createCCLRuleGapCreater(coreContextLabelsTable,
					getMaxCoreContextLabelAmbiguityRank(complexLabelGrammarExtractionParameters.configFile));

			RuleExtracter<EbitgLexicalizedInferenceCoreContextLabeled, SAMTQuadruple> cclRuleExtracter = CCLRuleExtracter.createCCLRuleExtracter(
					complexLabelGrammarExtractionParameters.parameters, complexLabelGrammarExtractionParameters.samtGrammarExtractionConstraints,
					coreContextLabelsTable, cclRuleGapCreater);

			MultiThreadComputationParameters parameters = MultiThreadComputationParameters.createMultiThreadComputationParameters(configFile, numThreads);
			RuleLabelsSmoother ruleLabelsSmoother = NoLabelSmoothingRulesSmoother.createNoLabelSmoothingNoReorderingRulesSmoothingSmoother(LabeledRulesSmoother
					.createWordKeyMappingTable(parameters),EmptyAlternativelyLabeledRuleSetsListCreater.createEmptyAlternativelyLabeledRuleSetsListCreater(),EmptyAlternativelyLabeledRuleSetsListCreater.createEmptyAlternativelyLabeledRuleSetsListCreater(),false);
			Assert.assertNotNull(ruleLabelsSmoother.getWordKeyMappingTable());
			SystemIdentity systemIdentity = SystemIdentity.createSystemIdentityWithStandardLabelingSettings(configFile, MTPipelineHatsCCL.SystemName);
			//MTGrammarExtractionConfig grammarExtractionConfig = MTGrammarExtractionConfig.createMTGrammarExtractionConfig(configFile);
			return createMultiThreadGrammarExtractorTestFilteredComplexLabels(configFile, numThreads, cclRuleExtracter, new CCLExtracterThreadFactoryCreater(
					ruleLabelsSmoother), complexLabelGrammarExtractionParameters, new HieroFilterRuleSetCreater(MTGrammarExtractionConfig.getGrammerSourceFilterPath(configFile)),
					createTranslationRuleCountsTable(configFile, complexLabelGrammarExtractionParameters, ruleLabelsSmoother,systemIdentity.getCanonicalFormSettings()), systemIdentity);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	private static <InferenceType extends EbitgLexicalizedInference, InputType extends AlignmentStringTriple> MultiThreadGrammarExtractor createMultiThreadGrammarExtractorTestFilteredComplexLabels(
			ConfigFile configFile, int numThreads, RuleExtracter<InferenceType, InputType> ruleExtracter,
			ExtracterThreadFactoryCreater<InferenceType, InputType> extracterThreadfactoryCreater,
			ComplexLabelsGrammarExtractionParameters complexLabelGrammarExtractionParameters, FilterRuleSetCreater<?> filterRuleSetCreater,
			TranslationRuleCountTable translationRuleCountTable, SystemIdentity systemIdentity) {

		String filterFileName = MTGrammarExtractionConfig.getGrammerSourceFilterPath(configFile);

		UnknownWordRulesCreator unknownWordRulesCreator = UnknownWordRulesCreatorPosTags.createUnknownWordRulesCreatorPosTags(translationRuleCountTable.getWordKeyMappingTable(),
				systemIdentity);

		// The word file names are the source file, the target file and the
		// testFilter file
		boolean useExtraSAMTFeatures = configFile.getBooleanWithPresenceCheck(SAMTGrammarExtractionConstraints.USE_EXTRA_SAMT_FEATURES_PROPERTY);
		return new MultiThreadGrammarExtractorTestFiltered<InferenceType, InputType>(complexLabelGrammarExtractionParameters.parameters,
				translationRuleCountTable, filterFileName, complexLabelGrammarExtractionParameters.samtGrammarExtractionConstraints, ruleExtracter,
				extracterThreadfactoryCreater, unknownWordRulesCreator, false, false, useExtraSAMTFeatures, filterRuleSetCreater, systemIdentity);
	}

	

	public RuleExtracter<? extends EbitgLexicalizedInference, ?> getRuleExtracter() {
		return this.ruleExtracter;
	}

	private List<Pair<Integer>> createMinMaxFilterSentencesNumList() {
		List<Pair<Integer>> minMaxFilterSentencesNumList;
		try {
			minMaxFilterSentencesNumList = this.createMinMaxLineNumsList(FileStatistics.countLines(this.getGrammarSourceFilterFileName()));
			return minMaxFilterSentencesNumList;

		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	protected void performGrammarRuleFilterSetup() {
		ChiangRuleExtracter chiangRuleExtractor = TranslationRuleExtractorThread.createChiangRuleExtracter(parameters, getChiangGrammarExtractionConstraints());
		performTrieFilling(TranslationRuleFilterPreparationThread.createExtractorThreadFactoryBasic(chiangRuleExtractor.getGrammarExtractionConstraints(),
				this.filterRuleSetCreater), this, createMinMaxFilterSentencesNumList());
		return;
	}

	private static void extractGrammarMultiThreadAndWriteResultToFile(String configFilePath, int numThreads, String resultFilePath) {
		SystemIdentity systemIdentity = SystemIdentity.createHieroSystemIdenty();
		MultiThreadGrammarExtractor grammarExtractor = createMultiThreadGrammarExtractorTestFilteredHiero(configFilePath, numThreads, systemIdentity);
		grammarExtractor.extractGrammarMultiThreadAndWriteResultToFile(resultFilePath, GrammarExtracterCreaterHatsHiero
				.writePlainHieroRulesIfSwitchedOnOrIfThereIsNoReorderingLabelExtension(MTConfigFile.createMtConfigFile(configFilePath,
						MTPipelineHatsHiero.SystemName)));
		// Gideon: Show the label probability estimator contents
		grammarExtractor.getLabelProbabilityEstimator().showContentsForDebugging(grammarExtractor.getWordKeyMappingTable());;

	}

	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println("Wrong usage, uage: java -jar extractGrammarMultiThread configFileName numThreads");
			return;
		}

		String configFileName = args[0];
		int numThreads = Integer.parseInt(args[1]);
		extractGrammarMultiThreadAndWriteResultToFile(configFileName, numThreads, "translationGrammarSourceFiltered.txt");
	}

	@Override
	protected void performTrieFilling() {
		performGrammarRuleFilterSetup();
		double totalCountCreatedBySourceToTargetThreads = performSourceToTargetTrieFilling();
		double totalCountCreatedByTargetToSourceThreads = performTargetToSourceTrieFilling();
		System.out.println("generating statistics");
		getTranslationRuleCountTable().printExtractedRulesStatistics(totalCountCreatedBySourceToTargetThreads, totalCountCreatedByTargetToSourceThreads);
	}

	protected double performTargetToSourceTrieFilling() {
		return performTrieFilling(extracterThreadFactoryCreater.createTargetFilteredExtractorThreadFactory(ruleExtracter), this);
	}

	protected double performSourceToTargetTrieFilling() {
		// assert(ruleExtracter instanceof SAMTRuleExtracter);
		return performTrieFilling(extracterThreadFactoryCreater.createSourceFilteredExtractorThreadFactory(ruleExtracter), this);
	}

	public String getGrammarSourceFilterFileName() {
		return grammarSourceFilterFileName;
	}

	public LabelProbabilityEstimator getLabelProbabilityEstimator() {
		return this.getTranslationRuleCountTable().getLabelProbabilityEstimator();
	}

	@Override
	protected boolean useExtraSAMTFeatures() {
		return this.useExtraSAMTFeatures;
	}

}
