package grammarExtraction.chiangGrammarExtraction;

import extended_bitg.EbitgLexicalizedInference;
import grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagPentuple;
import grammarExtraction.grammarExtractionFoundation.ExtractorThreadFactory;
import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;
import grammarExtraction.grammarExtractionFoundation.MultiThreadGrammarExtractor;
import grammarExtraction.grammarExtractionFoundation.RuleExtracter;
import grammarExtraction.grammarExtractionFoundation.TranslationRuleExtractorThread;
import grammarExtraction.labelSmoothing.RuleLabelsSmoother;
import grammarExtraction.samtGrammarExtraction.SAMTQuadruple;
import grammarExtraction.translationRuleTrie.TranslationRuleCountTable;
import java.util.List;

import boundaryTagging.EbitgLexicalizedInferenceBoundaryTagged;

import coreContextLabeling.EbitgLexicalizedInferenceCoreContextLabeled;
import alignment.AlignmentStringTriple;
import alignmentStatistics.InputStringsEnumeration;

import util.Pair;

public class TranslationRuleTargetFilteredExtractionThread<InputType extends AlignmentStringTriple> extends TranslationRuleExtractorThread<InputType> implements
		ExtractorThreadFactory<TranslationRuleTargetFilteredExtractionThread<InputType>, MultiThreadGrammarExtractorTestFiltered<? extends EbitgLexicalizedInference, InputType>> {

	private TranslationRuleTargetFilteredExtractionThread(RuleExtracter<? extends EbitgLexicalizedInference, InputType> ruleExtracter, RuleLabelsSmoother ruleLabelsSmoother) {
		super(ruleExtracter, ruleLabelsSmoother);
	};

	public TranslationRuleTargetFilteredExtractionThread(int maxAllowedInferencesPerNode, Pair<Integer> minMaxSentencePairNum, InputStringsEnumeration<InputType> inputStringsenumeration,
			int maxSentenceLength, int outputLevel, TranslationRuleCountTable translatrionRuleProbabilityTable, boolean useJoshuaStyleSimplifiedCountComputing, boolean useCaching,
			RuleExtracter<? extends EbitgLexicalizedInference, InputType> ruleExtracter, RuleLabelsSmoother ruleLabelsSmoother) {
		super(maxAllowedInferencesPerNode, minMaxSentencePairNum, inputStringsenumeration, maxSentenceLength, outputLevel, translatrionRuleProbabilityTable.getTranslationRuleTrie(),
				translatrionRuleProbabilityTable.getWordKeyMappingTable(), useJoshuaStyleSimplifiedCountComputing, useCaching, ruleExtracter, ruleLabelsSmoother);
	}

	protected void processPhrasePairRuleSet(LightWeightPhrasePairRuleSet originalRuleSet) {
		for (LightWeightPhrasePairRuleSet ruleSet : this.ruleLabelsSmoother.createListOfOriginalAndLabelSmoothedRuleSets(originalRuleSet)) {
			
		    /**
		     * We optimize the grammar extraction, by not adding labeled forms of rules 
		     * in target--source direction when we use canonical form labeled rules with only 
		     * Hiero phrase weights. In this case the phrase weights for the labeled rules are not 
		     * required, so we only have to add the labeled rules in the source--target direction in 
		     * order to find the most frequent (canonical form) rule labeling for each Hiero rule type.
		     * 
		     * The goal of this optimization is to speed up the second stage of the grammar extraction, the 
		     * Trie filling in target--source direction, and more importantly reduce the memory consumption.   
		     */
		    if(!getTranslationRuleTrie().useCanonicalFormLabeledRulesWithOnlyHieroWeights()){
		    		getTranslationRuleTrie().addRulesFromSetToTableIfTargetSidePresentInTrie(ruleSet);
		    }
		    
		    addLabelSmoothedRuleSets(ruleSet);
		}
	}

	protected void addLabelSmoothedRuleSets(LightWeightPhrasePairRuleSet ruleSet) {
		for (LightWeightPhrasePairRuleSet smoothedRuleSet : this.ruleLabelsSmoother.getAllLabelSmoothedRuleSets(ruleSet)) {
			getTranslationRuleTrie().addRulesFromSetToTableIfTargetSidePresentInTrie(smoothedRuleSet);
		}
	}

	protected List<LightWeightPhrasePairRuleSet> extractRuleSets(InputType input) {
		HierarchicalPhraseBasedRuleExtractor<? extends EbitgLexicalizedInference> translationRuleExtractor = createTranslationRuleExtractor(input);
		List<LightWeightPhrasePairRuleSet> ruleSets = translationRuleExtractor.createLightWeightTranslationRuleSets(getWordKeyMappingTable());
		return ruleSets;
	}

	@Override
	public TranslationRuleTargetFilteredExtractionThread<InputType> createExtractorThread(Pair<Integer> minMaxSentencePairNum,
			MultiThreadGrammarExtractorTestFiltered<? extends EbitgLexicalizedInference, InputType> multiThreadGrammarExtractor) {

		InputStringsEnumeration<InputType> inputStringsenumeration = this.ruleExtracter.createInputStringEnumeration(multiThreadGrammarExtractor.parameters);
		TranslationRuleTargetFilteredExtractionThread<InputType> result = new TranslationRuleTargetFilteredExtractionThread<InputType>(
				multiThreadGrammarExtractor.parameters.getMaxAllowedInferencesPerNode(), minMaxSentencePairNum, inputStringsenumeration, multiThreadGrammarExtractor.parameters.getMaxSentenceLength(),
				multiThreadGrammarExtractor.parameters.getOutputLevel(), multiThreadGrammarExtractor.getTranslationRuleCountTable(),
				MultiThreadGrammarExtractor.UseJoshuaStyleSimplifiedCountComputing, multiThreadGrammarExtractor.parameters.getUseCaching(), this.ruleExtracter, this.ruleLabelsSmoother);
		return result;
	}

	public static <InputType extends EbitgLexicalizedInference> ExtractorThreadFactory<TranslationRuleTargetFilteredExtractionThread<AlignmentStringTriple>, MultiThreadGrammarExtractorTestFiltered<? extends EbitgLexicalizedInference, AlignmentStringTriple>> createExtractorThreadFactory(
			RuleExtracter<? extends EbitgLexicalizedInference, AlignmentStringTriple> ruleExtracter, RuleLabelsSmoother ruleLabelsSmoother) {
		return new TranslationRuleTargetFilteredExtractionThread<AlignmentStringTriple>(ruleExtracter, ruleLabelsSmoother);
	}

	public static <InputType extends EbitgLexicalizedInference> ExtractorThreadFactory<TranslationRuleTargetFilteredExtractionThread<SAMTQuadruple>, MultiThreadGrammarExtractorTestFiltered<? extends EbitgLexicalizedInference, SAMTQuadruple>> createExtractorThreadFactorySAMT(
			RuleExtracter<EbitgLexicalizedInference, SAMTQuadruple> ruleExtracter, RuleLabelsSmoother ruleLabelsSmoother) {
		return new TranslationRuleTargetFilteredExtractionThread<SAMTQuadruple>(ruleExtracter, ruleLabelsSmoother);
	}

	public static <InputType extends EbitgLexicalizedInferenceCoreContextLabeled> ExtractorThreadFactory<TranslationRuleTargetFilteredExtractionThread<SAMTQuadruple>, MultiThreadGrammarExtractorTestFiltered<? extends EbitgLexicalizedInference, SAMTQuadruple>> createExtractorThreadFactoryCCL(
			RuleExtracter<EbitgLexicalizedInferenceCoreContextLabeled, SAMTQuadruple> ruleExtracter, RuleLabelsSmoother ruleLabelsSmoother) {
		return new TranslationRuleTargetFilteredExtractionThread<SAMTQuadruple>(ruleExtracter, ruleLabelsSmoother);
	}

	public static <InputType extends EbitgLexicalizedInferenceBoundaryTagged> ExtractorThreadFactory<TranslationRuleTargetFilteredExtractionThread<BoundaryTagPentuple>, MultiThreadGrammarExtractorTestFiltered<? extends EbitgLexicalizedInference, BoundaryTagPentuple>> createExtractorThreadFactoryBoundaryTagLabeled(
			RuleExtracter<EbitgLexicalizedInferenceBoundaryTagged, BoundaryTagPentuple> ruleExtracter, RuleLabelsSmoother ruleLabelsSmoother) {
		return new TranslationRuleTargetFilteredExtractionThread<BoundaryTagPentuple>(ruleExtracter, ruleLabelsSmoother);
	}
}
