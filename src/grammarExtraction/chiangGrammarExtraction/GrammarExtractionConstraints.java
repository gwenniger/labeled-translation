package grammarExtraction.chiangGrammarExtraction;

import grammarExtraction.translationRules.RuleLabelCreater;
import ccgLabeling.LabelGenerator.UnaryCategoryHandlerType;

public abstract class GrammarExtractionConstraints {

	public static final String ALLOW_DANGLING_SECONDNOND_TERMINAL_PROPERTY = "allowDanglingSecondNonterminal";
	public static final String ALLOW_CONSECUTIVE_NONTERMINALS_IN_NON_ABSTRACTRULES_PROPERTY = "allowConsecutiveNonterminalsInNonAbstractRules";
	public static final String ALLOW_SOURCE_ABSTRACT_PROPERTY = "allowSourceAbstract";
	public static final String ALLOW_TARGET_WORDS_WITHOUT_SOURCE_PROPERTY = "allowTargetWordsWithoutSource";
	public static final String UNARY_CATEGORY_HANDLER_TYPE_PROPERTY = "unaryCategoryHandlerType";
	public static final String ALLOW_DOUBLE_PLUS_PROPERTY = "allowDoublePlus";
	public static final String ALLOW_POS_TAG_LABEL_FALLBACK_PROPERTY = "allowPosTagLabelFallback";
	public static final String USE_REORDERING_LABEL_EXTENSION_PROPERTY = "useReorderingLabelExtension";
	public static final String REORDER_LABEL_PHRASE_PAIRS_PROPERTY = "reorderLabelPhrasePairs";
	public static final String REORDER_LABEL_HIERO_RULES_PROPERTY = "reorderLabelHieroRules";
	public static final String USE_REORDERING_LABEL_BINARY_FEATURES_PROPERTY = "useReorderingLabelBinaryFeatures";
	public static final String REORDERING_FEATURE_SET_TYPE_PROPERTY = "reorderingLabelBinaryFeatureSetType";
	public static final String REORDER_LABEL_TYPE_PROPERTY = "reorderLabelType";
	public static final String USE_MOSES_HIERO_UNKNOWN_WORD_RULE_VARIANTS_PROPERTY = "useMosesHieroUnknownWordRuleVariants";

	private final int maxTerminalsPlusNonTerminalsSourceSide;
	private final int maxSourceAndTargetLength;
	private final int minNonTerminalSpan;
	private final int minNonTerminalSpanSourceFilter;
	private final boolean allowDanglingSecondNonterminal;
	// If set to false discards rules that have consecutive nonterminals on the
	// source side
	private final boolean allowConsecutiveNonterminalsInNonAbstractRules;
	// If set to false, discards rules that do not have any source nonterminals
	private final boolean allowSourceAbstract;
	private final boolean allowTargetWordsWithoutSource;
	private final boolean allowDoublePlus;
	private final boolean allowPosTagLabelFallback;

	private final ReorderLabelingSettings reorderLabelingSettings;
	private final LabelSideSettings labelSideSettings;

	protected final UnaryCategoryHandlerType unaryCategoryHandlerType;

	protected GrammarExtractionConstraints(int maxTerminalsPlusNonTerminalsSourceSide, int maxSourceAndTargetLength, int minNonTerminalSpan,
			int minNonTerminalSpanSourceFilter, boolean allowDanglingSecondNonterminal, boolean allowConsecutiveNonterminals, boolean allowSourceAbstract,
			boolean allowTargetWordsWithoutSource, boolean allowDoublePlus, boolean allowPosTagLabelFallback, ReorderLabelingSettings reorderLabelingSettings,
			LabelSideSettings labelSideSettings, UnaryCategoryHandlerType unaryCategoryHandlerType) {
		this.maxTerminalsPlusNonTerminalsSourceSide = maxTerminalsPlusNonTerminalsSourceSide;
		this.maxSourceAndTargetLength = maxSourceAndTargetLength;
		this.minNonTerminalSpan = minNonTerminalSpan;
		this.minNonTerminalSpanSourceFilter = minNonTerminalSpanSourceFilter;
		this.allowDanglingSecondNonterminal = allowDanglingSecondNonterminal;
		this.allowConsecutiveNonterminalsInNonAbstractRules = allowConsecutiveNonterminals;
		this.allowSourceAbstract = allowSourceAbstract;
		this.allowTargetWordsWithoutSource = allowTargetWordsWithoutSource;
		this.allowDoublePlus = allowDoublePlus;
		this.allowPosTagLabelFallback = allowPosTagLabelFallback;
		this.reorderLabelingSettings = reorderLabelingSettings;
		this.labelSideSettings = labelSideSettings;
		this.unaryCategoryHandlerType = unaryCategoryHandlerType;
	}

	public int getMaxTerminalsPlusNonTerminalsSourceSide() {
		return this.maxTerminalsPlusNonTerminalsSourceSide;
	}

	public int getMaxSourceAndTargetLength() {
		return this.maxSourceAndTargetLength;
	}

	public int getMinNonTerminalSpan() {
		return this.minNonTerminalSpan;
	}

	public int getMinNonTerminalSpanSourceFilter() {
		return minNonTerminalSpanSourceFilter;
	}

	public boolean allowDanglingSecondNonterminal() {
		return allowDanglingSecondNonterminal;
	}

	public boolean allowSourceAbstract() {
		return allowSourceAbstract;
	}

	public boolean allowTargetWordsWithoutSource() {
		return allowTargetWordsWithoutSource;
	}

	public boolean allowConsecutiveNonterminalsInNonAbstractRules() {
		return allowConsecutiveNonterminalsInNonAbstractRules;
	}

	public boolean allowDoublePlus() {
		return allowDoublePlus;
	}

	public boolean allowPosTagLabelFallback() {
		return allowPosTagLabelFallback;
	}

	public UnaryCategoryHandlerType getUnaryCategoryHandlerType() {
		return this.unaryCategoryHandlerType;
	}

	public boolean useReorderingLabelExtension() {
		return getReorderLabelingSettings().useReorderingLabelExtension();
	}

	public ReorderLabelingSettings getReorderLabelingSettings() {
		return reorderLabelingSettings;
	}

	public LabelSideSettings getLabelSideSettings() {
		return labelSideSettings;
	}

	public RuleLabelCreater createRuleLabelCreater() {
		return this.labelSideSettings.createRuleLabelCreater();
	}

}
