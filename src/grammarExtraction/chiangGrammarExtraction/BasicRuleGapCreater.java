package grammarExtraction.chiangGrammarExtraction;

import util.Pair;
import util.XMLTagging;
import extended_bitg.EbitgLexicalizedInference;
import reorderingLabeling.ReorderingLabel;
import grammarExtraction.translationRules.RuleGap;
import grammarExtraction.translationRules.RuleGapCreater;
import grammarExtraction.translationRules.RuleLabel;
import grammarExtraction.translationRules.RuleLabelCreater;

public abstract class BasicRuleGapCreater<T extends EbitgLexicalizedInference> implements RuleGapCreater<T> {

	private final ReorderLabelingSettings reorderLabelingSettings;
	private final RuleLabelCreater ruleLabelCreater;
	private static final String REORDERING_LABEL_WRAPPER_TAG = "ReorderingLabel";

	protected BasicRuleGapCreater(ReorderLabelingSettings reorderLabelingSettings, RuleLabelCreater ruleLabelCreater) {
		this.reorderLabelingSettings = reorderLabelingSettings;
		this.ruleLabelCreater = ruleLabelCreater;
	}

	protected String getReorderingLabelExtension(T inference) {
		if (reorderLabelingSettings.useReorderingLabelExtension()) {
			// return
			// XMLTagging.getTagSandwidchedString(REORDERING_LABEL_WRAPPER_TAG,
			// reorderLabelingSettings.getReorderingLabelForInference(inference).getLabel());
			return reorderLabelingSettings.getReorderingLabelForInference(inference).getLabel();
		} else {
			return "";
		}
	}

	protected String getPhrasePairReorderingLabelExtension() {
		// return
		// XMLTagging.getTagSandwidchedString(REORDERING_LABEL_WRAPPER_TAG,
		// ReorderingLabel.PHRASE_PAIR_LABEL_TAG);
		return ReorderingLabel.PHRASE_PAIR_LABEL_TAG;
	}

	public static String extractReorderingLabelFromString(String s) {
		return XMLTagging.extractNamedPropertyFromString(REORDERING_LABEL_WRAPPER_TAG, s);
	}

	// protected abstract String getLabelGapLexicalizedHieroRule(T inference);
	// protected abstract String getLabelGapAbstractRule(T inference);

	protected String getLabelWithReorderingExtension(T inference) {
		return getBasicLabel(inference) + "-" + getReorderingLabelExtension(inference);
	}

	protected String getLabelGapLexicalizedHieroRule(T inference) {
		if (reorderLabelLexicalizedHieroRules()) {
			// return
			// XMLTagging.getTagSandwidchedString(LightWeightTranslationRule.SOURCE_LABELS,
			// getBasicLabel(inference) +
			// getReorderingLabelExtension(inference));
			return getLabelWithReorderingExtension(inference);
		} else {
			return getBasicLabel(inference);
		}
	}

	private boolean inferenceHasNonBinaryReorderingLabel(T inference) {
		return !reorderLabelingSettings.getReorderingLabelForInference(inference).isBinaryReorderingLabel();
	}

	protected String getLabelGapAbstractRule(T inference) {

		if (this.getReorderLabelingSettings().useReorderingLabelExtension()) {
			{
				if ((!this.getReorderLabelingSettings().reorderLabelHieroRules()) && inferenceHasNonBinaryReorderingLabel(inference)) {
					return getBasicLabel(inference);
				}
				return getLabelWithReorderingExtension(inference);
			}

		} else {
			return getBasicLabel(inference);
		}
	}

	protected abstract String getBasicLabel(T inference);

	// protected abstract String getReorderingLabelExtendedLabel(T inference);

	public boolean reorderLabelLexicalizedHieroRules() {
		boolean result = this.getReorderLabelingSettings().useReorderingLabelExtension() && this.getReorderLabelingSettings().reorderLabelHieroRules();
		return result;
	}

	protected String getReorderingLabelPhrasePairLabel(T inference) {
		return getBasicLabel(inference) + getReorderingLabelPhrasePairExtension();
	}

	public String getReorderingLabelPhrasePairExtension() {
		if (reorderLabelLexicalizedHieroRules()) {
			return "-" + ReorderingLabel.PHRASE_PAIR_LABEL_TAG;
		}
		return "";
	}

	@Override
	public RuleGap creatLexicalizedHieroRuleGap(Pair<Integer> relativeSourceSpan, Pair<Integer> relativeTargetSpan, int gapNumber, T gapInference) {
		return new RuleGap(relativeSourceSpan, relativeTargetSpan, gapNumber, ruleLabelCreater.createRuleLabelFromLabelString(getLabelGapLexicalizedHieroRule(gapInference)));
	}

	@Override
	public RuleGap createAbstractRuleGap(Pair<Integer> relativeSourceSpan, Pair<Integer> relativeTargetSpan, int gapNumber, T gapInference) {
		return new RuleGap(relativeSourceSpan, relativeTargetSpan, gapNumber, ruleLabelCreater.createRuleLabelFromLabelString(getLabelGapAbstractRule(gapInference)));
	}

	public ReorderLabelingSettings getReorderLabelingSettings() {
		return this.reorderLabelingSettings;
	}
	
	public RuleLabel createRuleLabelFromLabelString(String labelString)
	{
		return ruleLabelCreater.createRuleLabelFromLabelString(labelString);
	}

}
