package grammarExtraction.chiangGrammarExtraction;

import java.util.ArrayList;

import extended_bitg.EbitgLexicalizedInference;
import grammarExtraction.translationRules.RuleGap;
import grammarExtraction.translationRules.RuleLabel;
import grammarExtraction.translationRules.TranslationRule;
import grammarExtraction.translationRules.TranslationRuleCreater;
import grammarExtraction.translationRules.TranslationRuleLexicalProperties;
import hat_lexicalization.Lexicalizer;

public abstract class BasicTranslationRuleCreater<T extends EbitgLexicalizedInference, T2 extends BasicRuleGapCreater<T>> implements TranslationRuleCreater<T> {

	protected final T2 basicRuleGapCreater;

	protected BasicTranslationRuleCreater(T2 basicRuleGapCreater) {
		this.basicRuleGapCreater = basicRuleGapCreater;
	}

	public RuleLabel createRuleLabelFromLabelString(String labelString)
	{
		return this.basicRuleGapCreater.createRuleLabelFromLabelString(labelString);
	}
	

	@Override
	public TranslationRule<T> createBaseTranslationRuleLexicalizedHieroRules(Lexicalizer lexicalizer, T ruleInference) {
		TranslationRule<T> result =  new TranslationRule<T>(createRuleLabelFromLabelString(basicRuleGapCreater.getLabelGapLexicalizedHieroRule(ruleInference)), TranslationRuleLexicalProperties.createTranslationRuleLexicalProperties(lexicalizer, ruleInference),
				new ArrayList<RuleGap>(), new ArrayList<RuleGap>(), basicRuleGapCreater);
	
		return result;
	}

	@Override
	public TranslationRule<T> createBaseTranslationRuleWithAbstractRuleLabeling(Lexicalizer lexicalizer, T ruleInference) {
		return new TranslationRule<T>(createRuleLabelFromLabelString(basicRuleGapCreater.getLabelGapAbstractRule(ruleInference)), TranslationRuleLexicalProperties.createTranslationRuleLexicalProperties(
				lexicalizer, ruleInference), new ArrayList<RuleGap>(), new ArrayList<RuleGap>(), basicRuleGapCreater);
	}

	
	@Override
	public TranslationRule<T> createBaseTranslationRuleWithPhrasePairLabeling(Lexicalizer lexicalizer, T ruleInference) {
		return new TranslationRule<T>(createRuleLabelFromLabelString(basicRuleGapCreater.getReorderingLabelPhrasePairLabel(ruleInference)), TranslationRuleLexicalProperties.createTranslationRuleLexicalProperties(
				lexicalizer, ruleInference), new ArrayList<RuleGap>(), new ArrayList<RuleGap>(), basicRuleGapCreater);
	}
	
	

}