package grammarExtraction.chiangGrammarExtraction;

import java.io.Serializable;

import ccgLabeling.LabelGenerator.UnaryCategoryHandlerType;

public class ChiangGrammarExtractionConstraints extends GrammarExtractionConstraints implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final int MaxTerminalsPlusNonTerminalsSourceSide = 5;
	private static final int MaxSourceAndTargetLength = 10;
	private static final int MinNonTerminalSpan = 2;
	private static final int MinNonTerminalSpanSourceFilter = 1;

	protected ChiangGrammarExtractionConstraints(int maxTerminalsPlusNonTerminalsSourceSide, int maxSourceAndTargetLength, int minNonTerminalSpan, int minNonTerminalSpanSourceFilter,
			boolean allowDanglingSecondNonterminal, ReorderLabelingSettings reorderLabelingSettings,LabelSideSettings labelSideSettings) {
		super(maxTerminalsPlusNonTerminalsSourceSide, maxSourceAndTargetLength, minNonTerminalSpan, minNonTerminalSpanSourceFilter, allowDanglingSecondNonterminal, false, false, false, false,false,
				reorderLabelingSettings,labelSideSettings, UnaryCategoryHandlerType.BOTTOM);
	}

	public static ChiangGrammarExtractionConstraints createStandardChiangGrammarExtractionConstraints(boolean allowDanglingNonTerminal) {
		return new ChiangGrammarExtractionConstraints(MaxTerminalsPlusNonTerminalsSourceSide, MaxSourceAndTargetLength, MinNonTerminalSpan, MinNonTerminalSpanSourceFilter, allowDanglingNonTerminal,
				ReorderLabelingSettings.createNoReorderingLabelingSettings(),LabelSideSettings.createLabelSideSettingsBothSides());
	}

}
