package grammarExtraction.chiangGrammarExtraction;

import extended_bitg.EbitgLexicalizedInference;
import grammarExtraction.translationRules.RuleLabel;
import grammarExtraction.translationRules.RuleLabelCreater;

public class ChiangRuleCreater extends BasicTranslationRuleCreater<EbitgLexicalizedInference, BasicRuleGapCreater<EbitgLexicalizedInference>> {
	public static final String ChiangLabel = "X";
	public static final String ChiangRuleLabel = RuleLabel.ruleLabelString(ChiangLabel);

	public ChiangRuleCreater(ReorderLabelingSettings reorderLabelingSettings,RuleLabelCreater ruleLabelCreater) {
		super(ChiangRuleGapCreater.createChiangRuleGapCreater(reorderLabelingSettings,ruleLabelCreater));
	}
}
