package grammarExtraction.chiangGrammarExtraction;

import reorderingLabeling.ReorderingLabel;
import extended_bitg.EbitgLexicalizedInference;
import grammarExtraction.translationRules.RuleLabelCreater;

public class ChiangRuleGapCreater extends BasicRuleGapCreater<EbitgLexicalizedInference> {
	private static final String GapLabel = ChiangRuleCreater.ChiangLabel;

	private ChiangRuleGapCreater(ReorderLabelingSettings reorderLabelingSettings,RuleLabelCreater ruleLabelCreater) {
		super(reorderLabelingSettings, ruleLabelCreater);
	}

	public static BasicRuleGapCreater<EbitgLexicalizedInference> createChiangRuleGapCreater(ReorderLabelingSettings reorderLabelingSettings,RuleLabelCreater ruleLabelCreater) {
		return new ChiangRuleGapCreater(reorderLabelingSettings,ruleLabelCreater);
	}

	public static String chiangRuleGapString(int gapNumber) {
		return "[" + GapLabel + "," + (gapNumber + 1) + "]";
	}

	protected String getBasicLabel(EbitgLexicalizedInference inference) {
		return ChiangRuleCreater.ChiangLabel;
	}


	public static String getReorderingLabelPhrasePairLabel() {
		return ChiangRuleCreater.ChiangLabel + "-" + ReorderingLabel.PHRASE_PAIR_LABEL_TAG;
	}

	@Override
	protected String getReorderingLabelPhrasePairLabel(EbitgLexicalizedInference inference) {
		// return XMLTagging.getTagSandwidchedString(LightWeightTranslationRule.SOURCE_LABELS, getBasicLabel(inference) +
		// getPhrasePairReorderingLabelExtension());
		return getReorderingLabelPhrasePairLabel();
	}


}
