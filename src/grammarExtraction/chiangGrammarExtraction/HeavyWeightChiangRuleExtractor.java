package grammarExtraction.chiangGrammarExtraction;

import util.Span;
import grammarExtraction.grammarExtractionFoundation.HeavyWeightPhrasePairRuleSet;
import grammarExtraction.grammarExtractionFoundation.HeavyWeightRuleExtractor;
import grammarExtraction.translationRules.TranslationRule;
import grammarExtraction.translationRules.TranslationRuleCreater;
import java.util.ArrayList;
import java.util.List;
import util.Pair;
import extended_bitg.EbitgChartBuilder;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeState;
import extended_bitg.InferenceLengthProperties;

public class HeavyWeightChiangRuleExtractor<T extends EbitgLexicalizedInference, T2 extends EbitgTreeState<T, T2>> extends HeavyWeightRuleExtractor<T, T2> {

	private final GapRangeComputing gapRangeComputing;
	private final GrammarExtractionConstraints chiangGrammarExtractionConstraints;

	protected HeavyWeightChiangRuleExtractor(EbitgChartBuilder<T, T2> chartBuilder, GapRangeComputing gapRangeComputing,
			GrammarExtractionConstraints chiangGrammarExtractionConstraints, TranslationRuleCreater<T> translationRuleCreater) {
		super(chartBuilder, translationRuleCreater);
		this.gapRangeComputing = gapRangeComputing;
		this.chiangGrammarExtractionConstraints = chiangGrammarExtractionConstraints;
	}

	public static <T extends EbitgLexicalizedInference, T2 extends EbitgTreeState<T, T2>> HeavyWeightChiangRuleExtractor<T, T2> createHeavyWeightChiangRuleExtractor(
			EbitgChartBuilder<T, T2> chartBuilder, GrammarExtractionConstraints grammarExtractionConstraints, TranslationRuleCreater<T> translationRuleCreater) {
		/*
		 * if (!chartBuilder.derivationsAlignmentHaveBeenComputed()) {
		 * chartBuilder.findDerivationsAlignment(); }
		 */

		return new HeavyWeightChiangRuleExtractor<T, T2>(chartBuilder, GapRangeComputing.createGapRangeComputing(grammarExtractionConstraints),
				grammarExtractionConstraints, translationRuleCreater);
	}

	public int getMaxRuleSourceLength() {
		return Math.min(this.getLexicalizer().getSourceLength(), chiangGrammarExtractionConstraints.getMaxSourceAndTargetLength());
	}

	public int getMaxRuleTargetLength() {
		return Math.min(this.getLexicalizer().getTargetLength(), chiangGrammarExtractionConstraints.getMaxSourceAndTargetLength());
	}

	private List<HeavyWeightPhrasePairRuleSet<T>> createHeavyWeightTranslationRuleSets(RuleSetCreater<T, T2> ruleSetCreater, int minSpanLength,
			int maxSpanLength) {
		List<HeavyWeightPhrasePairRuleSet<T>> result = new ArrayList<HeavyWeightPhrasePairRuleSet<T>>();

		for (int spanLength = minSpanLength; spanLength <= maxSpanLength; spanLength++) {

			for (int beginIndex = 0; beginIndex < (this.getLexicalizer().getSourceLength() - spanLength + 1); beginIndex++) {
				assert (beginIndex < this.getLexicalizer().getSourceLength());
				assert ((beginIndex + spanLength - 1) < this.getLexicalizer().getSourceLength());

				int endIndex = beginIndex + spanLength - 1;
				if (beginIndex >= this.getLexicalizer().getSourceLength() || ((beginIndex + spanLength - 1) >= this.getLexicalizer().getSourceLength())) {
					throw new RuntimeException();
				}

				Span absoluteSourceSpan = new Span(beginIndex, endIndex);
				HeavyWeightPhrasePairRuleSet<T> ruleSet = ruleSetCreater.createRuleSet(absoluteSourceSpan, this, gapRangeComputing);

				if (ruleSet.hasRules()) {
					result.add(ruleSet);
				}
				// System.out.println("Computation for this span finished");

			}
		}
		
		//for(HeavyWeightPhrasePairRuleSet<T> ruleSet : result){
		//    for(TranslationRule rule : ruleSet.getRules()){
		//	System.out.println(">>> Extracted heavy translation rule: " + rule);
		//    }
		//}
		

		return result;
	}

	/**
	 * This method creates all rule sets for spans up to the Hiero span length
	 * limit (e.g. 10) Not that this concerns both Hiero rules as well as
	 * possibly fully abstract rules
	 * 
	 * @return
	 */
	private List<HeavyWeightPhrasePairRuleSet<T>> createNormalHeavyWeightTranslationRuleSets() {

		RuleSetCreater<T, T2> ruleSetCreater = new NormalRuleSetCreater();
		int minSpanLength = 1;
		int maxSpanLength = Math.min(getMaxRuleSourceLength(), this.getLexicalizer().getSourceLength());
		return createHeavyWeightTranslationRuleSets(ruleSetCreater, minSpanLength, maxSpanLength);
	}

	/**
	 * This method generates rulesets for spans bigger than the maxSourceLength
	 * (i.e. were Normal Chiang Rule are still allowed)
	 * 
	 * @return
	 */
	private List<HeavyWeightPhrasePairRuleSet<T>> createAbstractOnlyHeavyWeightTranslationRuleSets() {

		RuleSetCreater<T, T2> ruleSetCreater = new AbstractRuleSetCreater();
		//int minSpanLengt = getMaxRuleSourceLength() + 1;
		int minSpanLengt = 2;
		int maxSpanLength = this.getLexicalizer().getSourceLength();
		List<HeavyWeightPhrasePairRuleSet<T>> result =  createHeavyWeightTranslationRuleSets(ruleSetCreater, minSpanLengt, maxSpanLength);
		
		/*
		int totalNumRules = 0;
		for(HeavyWeightPhrasePairRuleSet<T>  ruleSet: result)
		{
			totalNumRules += ruleSet.getNumRules();
		}
		System.out.println("Adding " + totalNumRules + " abstract rules");
	    */
		return result;
	}

	protected List<HeavyWeightPhrasePairRuleSet<T>> createHeavyWeightTranslationRuleSets() {

		List<HeavyWeightPhrasePairRuleSet<T>> result = new ArrayList<HeavyWeightPhrasePairRuleSet<T>>();
		result.addAll(createNormalHeavyWeightTranslationRuleSets());
		if (chiangGrammarExtractionConstraints.allowSourceAbstract()) {
			result.addAll(createAbstractOnlyHeavyWeightTranslationRuleSets());
		}
		return result;
	}

	public TranslationRule<T> createAbstractBaseTranslationRuleNotLengthConstrained(Span absoluteSourceSpan) {
		T firstLegalInference = getNoNullsOnEdgesSatisfyingFirstCompleteInference(absoluteSourceSpan);
		if (firstLegalInference != null) {
			return translationRuleCreater.createBaseTranslationRuleWithAbstractRuleLabeling(this.getLexicalizer(), firstLegalInference);
		}
		return null;
	}

	public TranslationRule<T> createChiangConstraintsSatisfyingBaseTranslationRule(Span absoluteSourceSpan) {
		T firstLegalInference = getNoNullsOnEdgesSatisfyingFirstCompleteInference(absoluteSourceSpan);
		
		 //System.out.println("firstLegalInference: " + firstLegalInference);
		if (firstLegalInference != null && (targetSpanLengthWithinAcceptedRange(firstLegalInference))) {
		    TranslationRule<T> result =  translationRuleCreater.createBaseTranslationRuleLexicalizedHieroRules(this.getLexicalizer(), firstLegalInference);
		    //System.out.println(">>>Rule source words: " + result.sourceSideRuleStringList());
		    //System.out.println(">>>Rule target words: " + result.targetSideRuleStringList());
		    return result;
		}
		return null;
	}

	public TranslationRule<T> createChiangConstraintsSatisfyingBaseTranslationRuleWithPhrasePairLabeling(Span absoluteSourceSpan) {
		T firstLegalInference = getNoNullsOnEdgesSatisfyingFirstCompleteInference(absoluteSourceSpan);
		if (firstLegalInference != null && (targetSpanLengthWithinAcceptedRange(firstLegalInference))) {
			return translationRuleCreater.createBaseTranslationRuleWithPhrasePairLabeling(this.getLexicalizer(), firstLegalInference);
		}
		return null;
	}

	public boolean targetSpanLengthWithinAcceptedRange(EbitgLexicalizedInference inference) {
		InferenceLengthProperties inferenceLengthProperties = InferenceLengthProperties.createInferenceLengthProperties(inference);
		return (inferenceLengthProperties.getTargetSpanLength() <= this.getMaxRuleTargetLength());
	}

	public boolean sourceWithinAcceptedRange(EbitgLexicalizedInference inference) {
		InferenceLengthProperties inferenceLengthProperties = InferenceLengthProperties.createInferenceLengthProperties(inference);
		boolean result = (inferenceLengthProperties.getSourceSpanLength() <= this.getMaxRuleSourceLength());
		// System.out.println(inference.getSourceSpanLength() + "<=" +
		// this.getMaxRuleSourceLength());
		// System.out.println("sourceWithinAcceptedRange called for " +
		// inference + "\n result: " + result);
		return result;
	}

	public TranslationRule<T> createOneGapRule(TranslationRule<T> baseTranslationRule, Span gapAbsoluteSourceSpan) {
		TranslationRule<T> result = null;
		T firstLegalGapInference = getNoNullsOnEdgesSatisfyingFirstCompleteInference(gapAbsoluteSourceSpan);

		if (firstLegalGapInference != null) {
			// copy the base translation rule to start
			result = TranslationRule.copyTranslationRule(baseTranslationRule);
			// add the rule gap
			result.addRuleGap(gapAbsoluteSourceSpan, firstLegalGapInference);

			return result;
		}

		return null;
	}

	public TranslationRule<T> createRuleWithTwoGaps(TranslationRule<T> baseTranslationRule, T gap1Inference, T gap2Inference, Span gap1AbsoluteSourceRange,
			Span gap2AbsoluteSourceRange) {
		TranslationRule<T> candidate;
		if (gapRangesImplyAbstractRule(baseTranslationRule, gap1AbsoluteSourceRange, gap2AbsoluteSourceRange)) {
			candidate = this.createAbstractRule(baseTranslationRule, gap1Inference, gap2Inference, gap1AbsoluteSourceRange, gap2AbsoluteSourceRange);
		} else {
			candidate = this.createTwoGapNonAbstractRule(baseTranslationRule, gap1Inference, gap2Inference, gap1AbsoluteSourceRange, gap2AbsoluteSourceRange);
		}
		return candidate;

	}

	private TranslationRule<T> createTwoGapNonAbstractRule(TranslationRule<T> baseTranslationRule, T gap1Inference, T gap2Inference,
			Span gap1AbsoluteSourceRange, Span gap2AbsoluteSourceRange) {
		// copy the base translation rule to start
		TranslationRule<T> result = TranslationRule.copyTranslationRule(baseTranslationRule);
		// add the rule gap
		result.addRuleGap(gap1AbsoluteSourceRange, gap1Inference);
		result.addRuleGap(gap2AbsoluteSourceRange, gap2Inference);

		return result;
	}

	private TranslationRule<T> createAbstractRule(TranslationRule<T> baseTranslationRule, T gap1Inference, T gap2Inference, Span gap1AbsoluteSourceRange,
			Span gap2AbsoluteSourceRange) {
		// copy the base translation rule to start
		TranslationRule<T> result = TranslationRule.copyTranslationRule(baseTranslationRule);
		// add the rule gap
		result.addRuleGapAbstractRule(gap1AbsoluteSourceRange, gap1Inference);
		result.addRuleGapAbstractRule(gap2AbsoluteSourceRange, gap2Inference);

		return result;
	}

	private boolean gapInferencePairLeavesNoLexicalizationOnTargetSide(T gap1Inference1, T gap1Inference2) {
		return util.Span.spansAreConsecutiveInSomeOrder(gap1Inference1.getTargetSpan(), gap1Inference2.getTargetSpan());
	}

	private boolean gapRangesImplyAbstractRule(TranslationRule<T> baseTranslationRule, Span gap1AbsoluteSourceRange, Span gap2AbsoluteSourceRange) {
		return baseTranslationRule.getSourceSpan().isPartitionedBy(gap1AbsoluteSourceRange, gap2AbsoluteSourceRange);
	}

	public List<TranslationRule<T>> createTwoGapRules(TranslationRule<T> baseTranslationRule, List<Pair<Span>> permissibleTwoGapSourceRanges,
			boolean allowLexicalItemsOnTargetSide) {
		List<TranslationRule<T>> result = new ArrayList<TranslationRule<T>>();

		// Find all pairs of permissible source ranges for the gaps
		assert (baseTranslationRule != null);

		for (Pair<Span> gapSourceRanges : permissibleTwoGapSourceRanges) {
			Span gap1AbsoluteSourceRange = gapSourceRanges.getFirst();
			Span gap2AbsoluteSourceRange = gapSourceRanges.getSecond();

			// System.out.println("Source length: " +
			// this.theChart.getSourceLength());
			//System.out.println("Gap 1: " + gap1AbsoluteSourceRange);
			// System.out.println("Gap 2: " + gap2AbsoluteSourceRange);

			// Find inferences for the gaps
			T gap1Inference = this.getNoNullsOnEdgesSatisfyingFirstCompleteInference(gap1AbsoluteSourceRange);
			T gap2Inference = this.getNoNullsOnEdgesSatisfyingFirstCompleteInference(gap2AbsoluteSourceRange);

	
			
			//Assert.assertTrue(this.chiangGrammarExtractionConstraints.allowDanglingSecondNonterminal());
			
			// Check that a valid inference could be found for both the gaps
			if ((gap1Inference != null) && (gap2Inference != null)) {
				
				//System.out.println("gap1Inference: " + gap1Inference.getSourceSpan() + "  gap2Inference: " + gap2Inference.getSourceSpan());
				
				// add a new two gap rule for the chosen gaps and associated
				// inferences based on the baseTranslationRule
				
				
				//System.out.println("gapInferencePairLeavesNoLexicalizationOnTargetSide(gap1Inference, gap2Inference) ? " + gapInferencePairLeavesNoLexicalizationOnTargetSide(gap1Inference, gap2Inference));
				
				if (allowLexicalItemsOnTargetSide || gapInferencePairLeavesNoLexicalizationOnTargetSide(gap1Inference, gap2Inference)) {

					TranslationRule<T> candidate = createRuleWithTwoGaps(baseTranslationRule, gap1Inference, gap2Inference, gap1AbsoluteSourceRange,
							gap2AbsoluteSourceRange);

					if ((this.chiangGrammarExtractionConstraints.allowDanglingSecondNonterminal()) || (!candidate.hasDanglingSecondNonterminal())) {
					//	System.out.println("candidate " + candidate + "  accepted");
						result.add(candidate);
					} else {
						//System.out.println("candidate rejected");
					}
				}
			}
		}
		return result;
	}

	public List<TranslationRule<T>> createTwoGapNonAbstractRules(TranslationRule<T> baseTranslationRule) {

		List<Pair<Span>> permissibleTwoGapNonAbstractSourceRanges = gapRangeComputing.findPermissibleTwoGapNonAbstractRuleSourceRanges(new Span(baseTranslationRule.getSourceSpan()));		
		return createTwoGapRules(baseTranslationRule, permissibleTwoGapNonAbstractSourceRanges, true);
	}
	
	
	public List<TranslationRule<T>> createTwoGapRulesAndAbstractRules(TranslationRule<T> baseTranslationRule) {

		List<Pair<Span>> permissibleTwoGapSourceRanges = gapRangeComputing.findPermissibleTwoGapSourceRanges(new Span(baseTranslationRule.getSourceSpan()));
		return createTwoGapRules(baseTranslationRule, permissibleTwoGapSourceRanges, true);
	}

	public List<TranslationRule<T>> createOnlyFullyAbstractRules(TranslationRule<T> baseTranslationRule) {

		List<Pair<Span>> permissibleTwoGapSourceRanges = gapRangeComputing.findFullyAbstractRulesPermissibleSplitRanges(new Span(baseTranslationRule
				.getSourceSpan()));
		return createTwoGapRules(baseTranslationRule, permissibleTwoGapSourceRanges, this.chiangGrammarExtractionConstraints.allowTargetWordsWithoutSource());
	}

	public List<TranslationRule<T>> createOneGapRules(TranslationRule<T> baseTranslationRule) {
		List<TranslationRule<T>> result = new ArrayList<TranslationRule<T>>();

		assert (gapRangeComputing != null);
		assert (baseTranslationRule != null);
		List<Span> prospectiveOneGapRanges = gapRangeComputing.findPermissableOneGapSourceRanges(new Span(baseTranslationRule.getSourceSpan()));

		for (Span gapSourceRange : prospectiveOneGapRanges) {
			TranslationRule<T> oneGapRule = createOneGapRule(baseTranslationRule, gapSourceRange);

			if (oneGapRule != null && oneGapRule.hasOutsideGapTargetPositions()) {
				result.add(oneGapRule);
			}
		}
		return result;
	}

	public ReorderLabelingSettings getReorderLabelingSettings() {
		return this.chiangGrammarExtractionConstraints.getReorderLabelingSettings();
	}

	private static interface RuleSetCreater<T extends EbitgLexicalizedInference, T2 extends EbitgTreeState<T, T2>> {

		HeavyWeightPhrasePairRuleSet<T> createRuleSet(Span absoluteSourceSpan, HeavyWeightChiangRuleExtractor<T, T2> heavyWeightChiangRuleExtractor,
				GapRangeComputing gapRangeComputing);
	}

	private class NormalRuleSetCreater implements RuleSetCreater<T, T2> {
		@Override
		public HeavyWeightPhrasePairRuleSet<T> createRuleSet(Span absoluteSourceSpan, HeavyWeightChiangRuleExtractor<T, T2> ruleExtracter,
				GapRangeComputing gapRangeComputing) {
			//System.out.println("NormalRuleSetCreater.createRuleSet...");
			return HeavyWeightPhrasePairRuleSet.createPhrasePairRuleSet(absoluteSourceSpan, ruleExtracter, gapRangeComputing);
		}

	}

	private class AbstractRuleSetCreater implements RuleSetCreater<T, T2> {
		@Override
		public HeavyWeightPhrasePairRuleSet<T> createRuleSet(Span absoluteSourceSpan, HeavyWeightChiangRuleExtractor<T, T2> ruleExtracter,
				GapRangeComputing gapRangeComputing) {
		//	System.out.println("AbstractRuleSetCreater.createRuleSet...");
			return HeavyWeightPhrasePairRuleSet.createAbstractPhrasePairRuleSet(absoluteSourceSpan, ruleExtracter, gapRangeComputing);
		}

	}

}
