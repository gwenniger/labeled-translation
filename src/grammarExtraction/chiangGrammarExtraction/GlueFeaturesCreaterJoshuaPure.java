package grammarExtraction.chiangGrammarExtraction;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.translationRuleTrie.GlueFeaturesCreater;
import grammarExtraction.translationRules.TranslationRuleFeatures;

public class GlueFeaturesCreaterJoshuaPure implements GlueFeaturesCreater {

	private GlueFeaturesCreaterJoshuaPure() {
	}

	public static GlueFeaturesCreaterJoshuaPure createFeaturesCreaterJoshuaPure(SystemIdentity systemIdentity) {
		return new GlueFeaturesCreaterJoshuaPure();
	}

	@Override
	public TranslationRuleFeatures geGlueRuleFeatures(SystemIdentity systemIdentity, double generativeGlueLabelProbability, boolean isHieroRule,
			double rarityPenalty) {
		return new GlueFeaturesJoshuaPure();
	}
}
