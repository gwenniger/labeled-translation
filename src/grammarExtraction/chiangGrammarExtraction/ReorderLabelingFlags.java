package grammarExtraction.chiangGrammarExtraction;

import java.io.Serializable;

public class ReorderLabelingFlags implements Serializable{

    	/**
         * 
         */
        private static final long serialVersionUID = 1L;
	private final boolean useReorderingLabelExtension;
	private final boolean reorderLabelHieroRules;
	private final boolean reorderLabelPhrasePairs;

	private ReorderLabelingFlags(boolean useReorderingLabelExtension, boolean reorderLabelHieroRules, boolean reorderLabelPhrasePairs) {
		checkReorderingLabelSettingsConsistency(useReorderingLabelExtension, reorderLabelHieroRules, reorderLabelPhrasePairs);
		this.useReorderingLabelExtension = useReorderingLabelExtension;
		this.reorderLabelHieroRules = reorderLabelHieroRules;
		this.reorderLabelPhrasePairs = reorderLabelPhrasePairs;
	}

	public static ReorderLabelingFlags createReorderLabelingFlags(boolean useReorderingLabelExtension, boolean reorderLabelHieroRules,
			boolean reorderLabelPhrasePairs) {
		return new ReorderLabelingFlags(useReorderingLabelExtension, reorderLabelHieroRules, reorderLabelPhrasePairs);
	}

	public static ReorderLabelingFlags createReorderLabelEverythingReorderLabelingFlags() {
		return new ReorderLabelingFlags(true,true,true);
	}
	
	public static ReorderLabelingFlags createReorderLabelAbstractRulesOnlyReorderLabelingFlags() {
		return new ReorderLabelingFlags(true,false,false);
	}
	
	public static ReorderLabelingFlags createNoReorderLabelingFlags() {
		return new ReorderLabelingFlags(false, false, false);
	}

	private static final void checkReorderingLabelSettingsConsistency(boolean useReorderingLabelExtension, boolean reorderLabelHieroRules,
			boolean reorderLabelPhrasePairs) {
		if ((!useReorderingLabelExtension) && ((reorderLabelPhrasePairs) || (reorderLabelHieroRules))) {
			throw new RuntimeException("Inconsistent reordering settings : useReorderingLabelExtension = false, but  reorderLabelHieroRules ="
					+ reorderLabelHieroRules + " and  reorderLabelPhrasePairs =" + reorderLabelPhrasePairs);
		}

		if ((useReorderingLabelExtension) && (reorderLabelPhrasePairs) && (!reorderLabelHieroRules)) {
			String errorMessage = "Inconsistent reordering settings : if you urse reorderLabelingExtension=true and reorderLabelPhrasePairs=true you must also use reorderLabelHieroRules=true";
			throw new RuntimeException(errorMessage);
		}
	}

	public boolean useReorderingLabelExtension() {
		return this.useReorderingLabelExtension;
	}

	public boolean reorderLabelHieroRules() {
		return this.reorderLabelHieroRules;
	}

	public boolean reorderLabelPhrasePairs() {
		return this.reorderLabelPhrasePairs;
	}

}
