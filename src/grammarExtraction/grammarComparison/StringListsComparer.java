package grammarExtraction.grammarComparison;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import sortParallel.MergeSortThreadedListOperation;
import util.FileUtil;
import junit.framework.Assert;

public class StringListsComparer {

    private static final String NL = "\n";
    private static final String FIRST_GRAMMAR_ONLY_SUFFIX = ".firstGrammarOnlyRules.txt";
    private static final String SECOND_GRAMMAR_ONLY_SUFFIX = ".secondGrammarOnlyRules.txt";
    private static final String BOTH_GRAMMARS_SUFFIX = ".bothGrammarsRules.txt";

    private final List<String> firstStringListSorted;
    private final List<String> secondStringListSorted;
    private List<Integer> firstListOnlyStringIndices;
    private List<Integer> secondListOnlyStringIndices;
    private List<Integer> bothListsStringIndicesForFirstList;

    protected StringListsComparer(List<String> firstStringListSorted,
	    List<String> secondStringListListSorted) {
	this.firstStringListSorted = firstStringListSorted;
	this.secondStringListSorted = secondStringListListSorted;
	this.firstListOnlyStringIndices = null;
	this.secondListOnlyStringIndices = null;
	this.bothListsStringIndicesForFirstList = null;
	Assert.assertNotNull(firstStringListSorted);
	Assert.assertNotNull(secondStringListSorted);
    }

    private static List<String> getStringListSorted(List<String> stringList) {

	ArrayList<String> list = new ArrayList<String>(stringList);
	List<String> listSorted = MergeSortThreadedListOperation.sortParallel(list);
	Assert.assertNotNull(listSorted);
	return listSorted;
    }

    public static StringListsComparer createStringListsComparer(List<String> stringList1,
	    List<String> stringList2) {
	return new StringListsComparer(getStringListSorted(stringList1),
		getStringListSorted(stringList2));
    }

    public void computeStringPresenceInformation() {
	int firstListIndex = 0;
	int secondListIndex = 0;

	List<Integer> firstListOnlyStringIndices = new ArrayList<Integer>();
	List<Integer> secondListOnlyStringIndices = new ArrayList<Integer>();
	List<Integer> bothListsStringIndicesForFirstList = new ArrayList<Integer>();

	for (firstListIndex = 0; firstListIndex < firstStringListSorted.size(); firstListIndex++) {
	    String firstListString = firstStringListSorted.get(firstListIndex);
	    // System.out.println("firstListString : " + firstListString);
	    String secondListString = null;

	    while ((secondListIndex < secondStringListSorted.size())
		    && (secondListString = secondStringListSorted.get(secondListIndex))
			    .compareTo(firstListString) < 0) {
		// System.out.println(secondListString +
		// " only present in second list");
		secondListOnlyStringIndices.add(secondListIndex);
		secondListIndex++;
	    }

	    // System.out.println("secondListString: " + secondListString);

	    if (firstListString.equals(secondListString)) {
		bothListsStringIndicesForFirstList.add(firstListIndex);
		// We "consume" as second list item here as well, so we must
		// increment the second list index
		secondListIndex++;
	    } else {
		firstListOnlyStringIndices.add(firstListIndex);
	    }
	}

	for (; secondListIndex < secondStringListSorted.size(); secondListIndex++) {
	    secondListOnlyStringIndices.add(secondListIndex);
	}

	this.firstListOnlyStringIndices = firstListOnlyStringIndices;
	this.secondListOnlyStringIndices = secondListOnlyStringIndices;
	this.bothListsStringIndicesForFirstList = bothListsStringIndicesForFirstList;
    }

    public List<String> getStringsOnlyPresentInFirstList() {
	if (this.firstListOnlyStringIndices == null) {
	    throw new RuntimeException(
		    "Error: string presence information has not been computed yet");
	}

	List<String> result = new ArrayList<String>();
	for (int firstListIndex : this.firstListOnlyStringIndices) {
	    result.add(firstStringListSorted.get(firstListIndex));
	}
	return result;
    }

    public List<String> getStringsOnlyPresentInSecondList() {
	if (this.secondListOnlyStringIndices == null) {
	    throw new RuntimeException(
		    "Error: string presence information has not been computed yet");
	}

	List<String> result = new ArrayList<String>();
	for (int secondListIndex : this.secondListOnlyStringIndices) {
	    result.add(secondStringListSorted.get(secondListIndex));
	}
	return result;
    }

    public List<String> getStringsPresentInBothLists() {
	if (this.firstListOnlyStringIndices == null) {
	    throw new RuntimeException(
		    "Error: string presence information has not been computed yet");
	}

	List<String> result = new ArrayList<String>();
	for (int firstListIndex : this.bothListsStringIndicesForFirstList) {
	    result.add(firstStringListSorted.get(firstListIndex));
	}
	return result;
    }

    public void computeAndPrintStringPresenceInformation(String itemsName, String itemsContainerName) {
	computeStringPresenceInformation();
	System.out.println(itemsName + " only present in first " + itemsContainerName + ":");
	for (String firstListOnlyString : getStringsOnlyPresentInFirstList()) {
	    System.out.println(firstListOnlyString);
	}
	System.out.println(itemsName + " only present in second " + itemsContainerName + ":");
	for (String secondListOnlyString : getStringsOnlyPresentInSecondList()) {
	    System.out.println(secondListOnlyString);
	}
	System.out.println(itemsName + " present in both " + itemsContainerName + "s" + ":");
	for (String bothListsString : getStringsPresentInBothLists()) {
	    System.out.println(bothListsString);
	}
    }

    private void writeFirstGrammarOnlyRules(String itemsName, String itemsContainerName,
	    BufferedWriter outputWriter) throws IOException {
	outputWriter.write(itemsName + " only present in first " + itemsContainerName + ":" + NL);
	for (String firstListOnlyString : getStringsOnlyPresentInFirstList()) {
	    outputWriter.write(firstListOnlyString + NL);
	}
    }

    private void writeSecondGrammarOnlyRules(String itemsName, String itemsContainerName,
	    BufferedWriter outputWriter) throws IOException {
	outputWriter.write(itemsName + " only present in second " + itemsContainerName + ":" + NL);
	for (String secondListOnlyString : getStringsOnlyPresentInSecondList()) {
	    outputWriter.write(secondListOnlyString + NL);
	}
    }

    private void writeBothGrammarRules(String itemsName, String itemsContainerName,
	    BufferedWriter outputWriter) throws IOException {
	outputWriter.write(itemsName + " present in both " + itemsContainerName + "s" + ":" + NL);
	for (String bothListsString : getStringsPresentInBothLists()) {
	    outputWriter.write(bothListsString + NL);
	}
    }

    public void computeAndWriteStringPresenceInformation(String itemsName,
	    String itemsContainerName, String outputFilePathPrefix,
	    boolean writePresentInBothFilesStrings) {
	computeStringPresenceInformation();

	BufferedWriter firstGrammarOnlyRulesOutputWriter = null;
	BufferedWriter secondGrammarOnlyRulesOutputWriter = null;
	BufferedWriter bothGrammarsRulesoutputWriter = null;
	try {
	    firstGrammarOnlyRulesOutputWriter = new BufferedWriter(new FileWriter(outputFilePathPrefix
		    + FIRST_GRAMMAR_ONLY_SUFFIX));
	    writeFirstGrammarOnlyRules(itemsName, itemsContainerName, firstGrammarOnlyRulesOutputWriter);

	    secondGrammarOnlyRulesOutputWriter = new BufferedWriter(new FileWriter(outputFilePathPrefix
		    + SECOND_GRAMMAR_ONLY_SUFFIX));
	    writeSecondGrammarOnlyRules(itemsName, itemsContainerName, secondGrammarOnlyRulesOutputWriter);

	    if (writePresentInBothFilesStrings) {
		bothGrammarsRulesoutputWriter = new BufferedWriter(new FileWriter(outputFilePathPrefix
			+ BOTH_GRAMMARS_SUFFIX));
		writeBothGrammarRules(itemsName, itemsContainerName, bothGrammarsRulesoutputWriter);

	    }
	} catch (IOException e) {
	    e.printStackTrace();
	} finally {
	    FileUtil.closeCloseableIfNotNull(firstGrammarOnlyRulesOutputWriter);
	    FileUtil.closeCloseableIfNotNull(secondGrammarOnlyRulesOutputWriter);
	    FileUtil.closeCloseableIfNotNull(bothGrammarsRulesoutputWriter);
	}

    }

    public void computeAndPrintStringPresenceInformation() {
	computeAndPrintStringPresenceInformation("Strings", "list");
    }
}
