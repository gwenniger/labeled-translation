package grammarExtraction.grammarComparison;

import java.util.ArrayList;
import java.util.List;

import sortParallel.MergeSortThreadedListOperation;

public class GrammarComparer extends StringListsComparer {

    private GrammarComparer(List<String> firstGrammarRuleIdentitiesListSorted,
	    List<String> secondGrammarRuleIdentitiesListSorted) {
	super(firstGrammarRuleIdentitiesListSorted, secondGrammarRuleIdentitiesListSorted);
    }

    private static List<String> getGrammarRulIdentitiesListSorted(String grammarFilePath) {
	UnlabeledRuleIdentitiesListCreater unlabeledRuleIdentitiesListCreaterFirstGrammar = UnlabeledRuleIdentitiesListCreater
		.createUnlabeledRuleIdentitiesListCreater(grammarFilePath);
	ArrayList<String> grammarRuleIdentitiesList = new ArrayList<String>(
		unlabeledRuleIdentitiesListCreaterFirstGrammar.computeUnlabeledRuleIdentitiesList());
	List<String> grammarRuleIdentitiesListSorted = MergeSortThreadedListOperation
		.sortParallel(grammarRuleIdentitiesList);
	return grammarRuleIdentitiesListSorted;

    }

    public static GrammarComparer createGrammarComparer(String firstGrammarFilePath,
	    String secondGrammarFilePath) {
	return new GrammarComparer(getGrammarRulIdentitiesListSorted(firstGrammarFilePath),
		getGrammarRulIdentitiesListSorted(secondGrammarFilePath));
    }

    public void computeAndWriteRulePresenceInformation(String resultFilePathPrefix,
	    boolean writePresentInBothFilesRules) {
	computeAndWriteStringPresenceInformation("Rules", "grammar", resultFilePathPrefix,
		writePresentInBothFilesRules);
    }

    public static void main(String[] args) {
	if (args.length != 4) {
	    System.out
		    .println("Usage : compareGrammars GRAMMAR1_PATH GRAMMAR2_PATH RESULT_FILE_PATH WRITE_PRESENT_IN_BOTH_FILES_RULES");
	    System.exit(1);
	}
	GrammarComparer grammarComparer = GrammarComparer.createGrammarComparer(args[0], args[1]);
	boolean writePresentInBothFilesRules = Boolean.parseBoolean(args[3]);
	grammarComparer.computeAndWriteRulePresenceInformation(args[2],
		writePresentInBothFilesRules);
    }

}
