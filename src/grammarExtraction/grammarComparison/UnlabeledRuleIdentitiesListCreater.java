package grammarExtraction.grammarComparison;

import grammarExtraction.translationRules.TranslationRuleStringLabelStripper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import util.FileStatistics;

public class UnlabeledRuleIdentitiesListCreater {
    private final String grammarFilePath;
    private final TranslationRuleStringLabelStripper TRANSLATION_RULE_STRING_LABEL_STRIPPER = TranslationRuleStringLabelStripper
	    .createTranslationRuleStringLabelStripper();

    private UnlabeledRuleIdentitiesListCreater(String grammarFilePath) {
	this.grammarFilePath = grammarFilePath;
    }

    public static UnlabeledRuleIdentitiesListCreater createUnlabeledRuleIdentitiesListCreater(
	    String grammarFilePath) {
	return new UnlabeledRuleIdentitiesListCreater(grammarFilePath);
    }

    public List<String> computeUnlabeledRuleIdentitiesList() {
	System.out.println("computeUnlabeledRuleIdentitiesList...");

	LineIterator it = null;
	try {
	    int numLines = FileStatistics.countLines(grammarFilePath);
	    List<String> result = new ArrayList<String>();
	    it = FileUtils.lineIterator(new File(grammarFilePath), "UTF-8");

	    int lineNum = 1;
	    int previousPercentageCompleted = 0;
	    while (it.hasNext()) {
		String line = it.nextLine();
		String unlabeledRuleIdentityString = TRANSLATION_RULE_STRING_LABEL_STRIPPER
			.getLabelStrippedRuleStringWithoutWeights(line);
		result.add(unlabeledRuleIdentityString);
		lineNum++;

		//System.out.println("lineNumber: " + lineNum);
		int percentageCompleted = (100 * lineNum) / numLines;
		if (percentageCompleted > previousPercentageCompleted) {
		    System.out.println(">>> " + percentageCompleted + "% completed");
		    previousPercentageCompleted = percentageCompleted;
		}
	    }
	    System.out.println("done");
	    return result;
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	} finally {
	    it.close();
	}

    }

}
