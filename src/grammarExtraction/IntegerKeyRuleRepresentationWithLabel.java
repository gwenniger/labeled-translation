package grammarExtraction;

import grammarExtraction.chiangGrammarExtraction.ChiangRuleCreater;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

import java.util.ArrayList;
import java.util.List;

public class IntegerKeyRuleRepresentationWithLabel extends IntegerKeyRuleRepresentation {

	protected IntegerKeyRuleRepresentationWithLabel(List<Integer> partKeys) {
		super(partKeys);
	}

	public static IntegerKeyRuleRepresentationWithLabel createIntegerKeyRuleRepresentationWithLabel(List<Integer> partKeys) {
		return new IntegerKeyRuleRepresentationWithLabel(partKeys);
	}

	public static IntegerKeyRuleRepresentationWithLabel createIntegerKeyRuleRepresentationWithLabel(String ruleString, WordKeyMappingTable wordKeyMappingTable) {
		return new IntegerKeyRuleRepresentationWithLabel(getKeyListForRuleString(ruleString, wordKeyMappingTable));
	}

	public static IntegerKeyRuleRepresentationWithLabel createIntegerKeyRuleRepresentationWithLabel(IntegerKeyRuleRepresentation integerKeyRuleRepresentation,
			Integer ruleSideLabel) {
		List<Integer> partKeysIncludingLabel = new ArrayList<Integer>(integerKeyRuleRepresentation.getPartKeys());
		partKeysIncludingLabel.add(ruleSideLabel);
		return new IntegerKeyRuleRepresentationWithLabel(partKeysIncludingLabel);
	}

	public static IntegerKeyRuleRepresentationWithLabel createIntegerKeyRuleRepresentation(List<String> ruleComponents, WordKeyMappingTable wordKeyMappingTable) {
		return new IntegerKeyRuleRepresentationWithLabel(getKeyListForRuleComponentsList(ruleComponents, wordKeyMappingTable));
	}

	public static IntegerKeyRuleRepresentationWithLabel createIntegerKeyRuleRepresentationWithLabelWithAddedChiangLabel(List<String> ruleComponents,
			WordKeyMappingTable wordKeyMappingTable) {
		List<String> chiangLabelExtendedComponents = new ArrayList<String>(ruleComponents);
		chiangLabelExtendedComponents.add(ChiangRuleCreater.ChiangRuleLabel);
		return new IntegerKeyRuleRepresentationWithLabel(getKeyListForRuleComponentsList(chiangLabelExtendedComponents, wordKeyMappingTable));
	}

	private static final long serialVersionUID = 1L;

	/**
	 * Get the left-hand-side label for the rule side
	 * @return
	 */
	public Integer getLeftHandSideLabel() {
		return this.partKeys.get(this.partKeys.size() - 1);
	}
	
	/**
	 * Get the gap Labels in their order occurring from left to right
	 * @return
	 */
	public List<Integer>  getGapLabels(WordKeyMappingTable wordKeyMappingTable) {
	    List<Integer> result = new ArrayList<Integer>();
	    
	    for(int i = 0; i < (this.partKeys.size() - 1); i++){
		Integer index = this.partKeys.get(i);
		if(wordKeyMappingTable.isLabelOneKey(index) || wordKeyMappingTable.isLabelTwoKey(index)){
		    result.add(index);
		}
	    }
	    return result;
	}
	
	
	/**
	 * Get a list of all labels for this rule side, starting with the left-hand-side 
	 * label and followed by the gap labels (if any) ordered as occurring from left 
	 * to right for this side of the rule
	 * @param wordKeyMappingTable
	 * @return
	 */
	public List<Integer>  getAllLabelsLeftToRight(WordKeyMappingTable wordKeyMappingTable) {
	    List<Integer> result = new ArrayList<Integer>();
	    result.add(getLeftHandSideLabel());
	    result.addAll(getGapLabels(wordKeyMappingTable));
	    return result;
	}
	    

	public boolean isSyntacticRuleSide() {
		return !(WordKeyMappingTable.isBasicChiangRuleLabelKey(getLeftHandSideLabel()));
	}

	public String getStringRepresentationExcludingLeftHandSideLabelJoshua(WordKeyMappingTable wordKeyMappingTable) {
		return getStringRepresentation(this.partKeys.subList(0, this.partKeys.size() - 1), wordKeyMappingTable);
	}

	public String getStringRepresentationExcludingLeftHandSideLabelMoses(WordKeyMappingTable wordKeyMappingTable) {
		return getStringRepresentation(this.partKeys.subList(0, this.partKeys.size() - 1), wordKeyMappingTable, MOSES_GAP_FORMATTER);
	}

	public String getLeftHandSideLabelStringRepresentation(WordKeyMappingTable wordKeyMappingTable) {
		List<Integer> subList = this.partKeys.subList(this.partKeys.size() - 1, this.partKeys.size());
		assert (subList.size() == 1);
		String result = getStringRepresentation(subList, wordKeyMappingTable);
		return result;
	}

	public List<String> getStringListRepresentationExcludingLeftHandSideLabel(WordKeyMappingTable wordKeyMappingTable) {
		List<String> fullStringList = getStringListRepresentation(wordKeyMappingTable);
		return fullStringList.subList(0, fullStringList.size() - 1);
	}

}
