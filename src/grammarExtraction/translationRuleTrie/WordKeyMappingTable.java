package grammarExtraction.translationRuleTrie;

import grammarExtraction.IntegerKeyRuleRepresentation;
import grammarExtraction.IntegerKeyRuleRepresentationWithLabel;
import grammarExtraction.chiangGrammarExtraction.ChiangRuleCreater;
import grammarExtraction.chiangGrammarExtraction.ChiangRuleGapCreater;
import grammarExtraction.grammarExtractionFoundation.ConcurrencySettings;
import grammarExtraction.translationRules.TranslationRuleRepresentationCreater;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.Assert;
import alignment.AlignmentTriple;
import alignmentStatistics.EbitgCorpusComputationMultiThread;
import util.InputReading;

public class WordKeyMappingTable implements Serializable {
	/*
	 * Note : It may be more efficient to implement the keyToWordMappingTable with a list datas tructure that is only synchronized for writing. However it is
	 * not completely clear if that would be really thread safe, and also no such data structure is present in the standard java libraries.
	 */
	// The number of labels that can occur
	private static final int MaxNumLabelTypes = 300000;

	// The maximum number of different words (for both languages together) that
	// can occur
	private static final int MaxNumWordTypes = 5000000;

	// no , allowed in RuleLabelPattern
	private final static Pattern LabelPattern = Pattern.compile("\\[.*\\]");
	// Square-Bracket-Left, (comma not followed by decimal OR not a comma) ,
	// Square-Bracket-Right
	private final static Pattern RuleLabelPattern = Pattern.compile("\\[(,(?!\\d)|[^,])*\\]");
	private final static Pattern LabelOnePattern = Pattern.compile("\\[.*,1\\]");
	private final static Pattern LabelTwoPattern = Pattern.compile("\\[.*,2\\]");

	private static final long serialVersionUID = 1L;

	public static final int BASIC_CHIANG_RULE_LABEL_KEY = 0;

	private final WordKeyMappingTableData wordKeyMappingTableData;
	private final WordKeyMappingTabbleItemHandler rulelabelItemHandler;
	private final WordKeyMappingTabbleItemHandler gapOneLabelItemHandler;
	private final WordKeyMappingTabbleItemHandler gapTwoLabelItemHandler;
	private final WordKeyMappingTabbleItemHandler wordItemHandler;

	private WordKeyMappingTable(WordKeyMappingTableData wordKeyMappingTableData, WordKeyMappingTabbleItemHandler rulelabelItemHandler, WordKeyMappingTabbleItemHandler gapOneLabelItemHandler,
			WordKeyMappingTabbleItemHandler gapTwoLabelItemHandler, WordKeyMappingTabbleItemHandler wordItemHandler) {
		this.wordKeyMappingTableData = wordKeyMappingTableData;
		this.rulelabelItemHandler = rulelabelItemHandler;
		this.gapOneLabelItemHandler = gapOneLabelItemHandler;
		this.gapTwoLabelItemHandler = gapTwoLabelItemHandler;
		this.wordItemHandler = wordItemHandler;
	}

	public static WordKeyMappingTable createWordKeyMappingTable(int noConcurrentThreads) {
		final ConcurrentHashMap<String, Integer> wordToKeyMappingTable = new ConcurrentHashMap<String, Integer>(ConcurrencySettings.ConcurrentHashMapInitialCapacity,
				ConcurrencySettings.ConcurrentHashMapLoadFactor, ConcurrencySettings.getConcurrencyLevel(noConcurrentThreads));
		final ConcurrentHashMap<Integer, String> keyToWordMappingTable = new ConcurrentHashMap<Integer, String>(ConcurrencySettings.ConcurrentHashMapInitialCapacity,
				ConcurrencySettings.ConcurrentHashMapLoadFactor, ConcurrencySettings.getConcurrencyLevel(noConcurrentThreads));

		int maxNumLabelTypes = MaxNumLabelTypes;
		WordKeyMappingTableData wordKeyMappingTableData = new WordKeyMappingTableData(wordToKeyMappingTable, keyToWordMappingTable, maxNumLabelTypes);
		WordKeyMappingTabbleItemHandler rulelabelItemHandler = new LabelItemHandler(wordKeyMappingTableData, 0, maxNumLabelTypes);
		WordKeyMappingTabbleItemHandler labelOneItemHandler = new LabelItemHandler(wordKeyMappingTableData, maxNumLabelTypes, maxNumLabelTypes);
		WordKeyMappingTabbleItemHandler labelTwoItemHandler = new LabelItemHandler(wordKeyMappingTableData, maxNumLabelTypes * 2, maxNumLabelTypes);
		WordKeyMappingTabbleItemHandler wordItemHandler = new WordItemHandler(wordKeyMappingTableData, maxNumLabelTypes * 3, MaxNumWordTypes);
		WordKeyMappingTable result = new WordKeyMappingTable(wordKeyMappingTableData, rulelabelItemHandler, labelOneItemHandler, labelTwoItemHandler, wordItemHandler);
		result.addBasicChiangLabels();
		return result;
	}

	private void addBasicChiangLabels() {
		this.rulelabelItemHandler.addEntryForString(ChiangRuleCreater.ChiangRuleLabel);
		this.gapOneLabelItemHandler.addEntryForString(ChiangRuleGapCreater.chiangRuleGapString(0));
		this.gapTwoLabelItemHandler.addEntryForString(ChiangRuleGapCreater.chiangRuleGapString(1));
	}

	public static WordKeyMappingTable createWordKeyMappingTable(List<String> wordFileNames, int noVariables, int noConcurrentThreads) {
		WordKeyMappingTable result = createWordKeyMappingTable(noConcurrentThreads);
		List<String> uniqeWords = new ArrayList<String>();
		uniqeWords.add(getNullWordString());

		int i = 1;
		for (String wordFileName : wordFileNames) {
			System.out.println("WordKeyMappingTable: adding words from wordFile " + i);
			uniqeWords.addAll(getUniqueWordsFromFile(wordFileName));
			i++;
		}
		result.addWordsToTableRandomOrder(uniqeWords);

		System.out.println("Number of items in table : " + result.getSize());
		System.out.println("Heap size after table creation : " + EbitgCorpusComputationMultiThread.heapSize());
		if (result.getSize() < Short.MAX_VALUE) {
			System.out.println(" WordKeyMappingTable: Warning: Could have fitted into Short!!!");
		}

		return result;
	}

	private static String getNullWordString() {
		return AlignmentTriple.NullWordString;
	}

	public static WordKeyMappingTable createWordKeyMappingTableFromAlignmentTriples(List<AlignmentTriple> alignmentTriples, int noVariables, int noConcurrentThreads) {
		WordKeyMappingTable result = createWordKeyMappingTable(noConcurrentThreads);
		List<String> uniqeWords = new ArrayList<String>();
		uniqeWords.add(getNullWordString());

		for (AlignmentTriple alignmentTriple : alignmentTriples) {
			System.out.println("WordKeyMappingTable: adding words from source sentence");
			uniqeWords.addAll(InputReading.getWordsFromString(alignmentTriple.getSourceSentenceAsString()));
			System.out.println(" WordKeyMappingTable: adding words from target sentence");
			uniqeWords.addAll(InputReading.getWordsFromString(alignmentTriple.getTargetSentenceAsString()));
			result.addWordsToTableRandomOrder(uniqeWords);
		}
		return result;
	}

	public static WordKeyMappingTable createWordKeyMappingTable(AlignmentTriple alignmentTriple, int noVariables, int noConcurrentThreads) {
		List<AlignmentTriple> alignmentTriples = new ArrayList<AlignmentTriple>();
		alignmentTriples.add(alignmentTriple);
		return createWordKeyMappingTableFromAlignmentTriples(alignmentTriples, noVariables, noConcurrentThreads);
	}

	public static boolean isRuleLabel(String string) {
		Matcher matcher = RuleLabelPattern.matcher(string);
		return matcher.matches();
	}

	public static boolean isLabelOne(String string) {
		Matcher matcher = LabelOnePattern.matcher(string);
		return matcher.matches();
	}

	public static boolean isLabelTwo(String string) {
		Matcher matcher = LabelTwoPattern.matcher(string);
		return matcher.matches();
	}

	public static boolean isLabel(String string) {
		Matcher matcher = LabelPattern.matcher(string);
		return matcher.matches();
	}

	public static boolean isBasicChiangRuleLabelKey(int key) {
		return (key == BASIC_CHIANG_RULE_LABEL_KEY);
	}

	public static boolean isLightWeightRuleLabelKeyStatic(int key) {
		return (key < MaxNumLabelTypes);
	}

	private boolean isRuleLabelKey(int key) {
		return (key < this.getMaxNumLabelTypes());
	}
	
	public boolean isGoalOrSentenceLeftHandSideLabelKey(Integer key){
	    if(this.isRuleLabelKey(key))
	    {
		String labelString = this.getWordForKey(key);
		String label = labelString.substring(1, labelString.length() - 1);
		return( label.equals(TranslationRuleRepresentationCreater.GOAL_LABEL)) || label.equals(TranslationRuleRepresentationCreater.SentenceLabel);
	    }
	    return false;
	}

	public boolean isLabelOneKey(int key) {
		return (key >= this.getMaxNumLabelTypes()) && (key < (2 * this.getMaxNumLabelTypes()));
	}

	public boolean isLabelTwoKey(int key) {
		return (key >= (2 * this.getMaxNumLabelTypes())) && (key < (3 * this.getMaxNumLabelTypes()));
	}

	public boolean isLabelKey(int key) {
		return (key < (3 * this.getMaxNumLabelTypes()));
	}

	public boolean isGapLabelKey(int key) {
		return isLabelOneKey(key) || isLabelTwoKey(key);
	}

	public boolean isWordKey(int key) {
		return key >= (3 * (this.getMaxNumLabelTypes()));
	}

	public int getXLabeledGapOneKey() {
		return this.gapOneLabelItemHandler.getBaseIndex();
	}

	public int getXLabeledGapTwoKey() {
		return this.gapTwoLabelItemHandler.getBaseIndex();
	}
	
	public boolean isXLabeledGapOneKey(int key) {
		return (key == this.gapOneLabelItemHandler.getBaseIndex());
	}

	public boolean isXLabeledGapTwoKey(int key) {
		return (key == this.gapTwoLabelItemHandler.getBaseIndex());
	}
	
	public boolean isXLabeledGapKey(int key) {
		return isXLabeledGapOneKey(key) || isLabelTwoKey(key);
	}
	
	private int getMaxNumLabelTypes() {
		return this.wordKeyMappingTableData.getMaxNumLabelTypes();
	}

	private ConcurrentMap<String, Integer> getWordToKeyMappingTable() {
		return this.wordKeyMappingTableData.getWordToKeyMappingTable();
	}

	private ConcurrentMap<Integer, String> getKeyToWordMappingTable() {
		return this.wordKeyMappingTableData.getKeyToWordMappingTable();
	}

	public Integer getKeyForWord(String word) {
		if (this.getWordToKeyMappingTable().containsKey(word)) {
			return this.getWordToKeyMappingTable().get(word);
		} else {
			WordKeyMappingTabbleItemHandler itemHandler = getItemHandler(word);
			if (itemHandler != this.wordItemHandler) {
				// System.out.println("WordKeyMappingTable : Adding entry for label at runtime");
				return itemHandler.addEntryForString(word);
			}
			throw new RuntimeException("WordKeyMappingTable: Error - trying to get Key for word \"" + word + "\" which is not present in the table");
		}
	}

	public String getWordForKey(int key) {
		return this.getKeyToWordMappingTable().get(key);
	}

	public static void printAddMessage(int lineNo) {
		if ((lineNo % 1000) == 0) {
			System.out.println("WordKeyMappingTable: Processing line: " + lineNo);
		}
	}

	private void addWordsToTableRandomOrder(List<String> words) {
		List<String> randomOrderWords = new ArrayList<String>(words);
		Collections.shuffle(randomOrderWords);

		for (String word : randomOrderWords) {

			this.wordItemHandler.addEntryForString(word);
			/*
			 * if(!(this.getWordToKeyMappingTable().containsKey(word))) { int newKey = this.getNextKeyValue(); this.getWordToKeyMappingTable().put(word,
			 * newKey); this.getKeyToWordMappingTable().put(newKey, word);
			 * 
			 * }
			 */
		}
	}

	public WordKeyMappingTabbleItemHandler getItemHandler(int key) {
		if (isRuleLabelKey(key)) {
			return this.rulelabelItemHandler;
		} else if (isLabelOneKey(key)) {
			return this.gapOneLabelItemHandler;
		} else if (isLabelTwoKey(key)) {
			return this.gapTwoLabelItemHandler;
		} else {
			return wordItemHandler;
		}
	}

	public WordKeyMappingTabbleItemHandler getWordItemHandler() {
		return this.wordItemHandler;
	}

	public WordKeyMappingTabbleItemHandler getLabelOneItemHandler() {
		return this.gapOneLabelItemHandler;
	}

	public WordKeyMappingTabbleItemHandler getItemHandler(String string) {
		if (isRuleLabel(string)) {
			return this.rulelabelItemHandler;
		} else if (isLabelOne(string)) {
			return this.gapOneLabelItemHandler;
		} else if (isLabelTwo(string)) {
			return this.gapTwoLabelItemHandler;
		} else {
			return wordItemHandler;
		}
	}

	public int getSize() {
		return this.getKeyToWordMappingTable().size();
	}

	public int getMaxNumItems() {
		return this.wordItemHandler.getNumberIntemsForType() + 3 * this.wordKeyMappingTableData.getMaxNumLabelTypes();
	}

	public IntegerKeyRuleRepresentationWithLabel getFullyDowngradedLabelsRuleRepresentation(IntegerKeyRuleRepresentation complexLabelsRuleRepresentation) {
		List<Integer> resultKeys = new ArrayList<Integer>();
		for (Integer key : complexLabelsRuleRepresentation.getPartKeys()) {
			resultKeys.add(getFullyDowngradedLabelKey(key));
		}
		return IntegerKeyRuleRepresentationWithLabel.createIntegerKeyRuleRepresentationWithLabel(resultKeys);
	}

	public IntegerKeyRuleRepresentationWithLabel getDowngradedToSourceSideRuleLabelOnlyRuleRepresentation(IntegerKeyRuleRepresentationWithLabel complexLabelsRuleRepresentation) {
		List<Integer> resultKeys = new ArrayList<Integer>();
		for (int i = 0; i < complexLabelsRuleRepresentation.getPartKeys().size() - 1; i++) {
			resultKeys.add(getFullyDowngradedLabelKey(complexLabelsRuleRepresentation.getPartKeys().get(i)));
		}
		// Only the source side labels information of the rule label is kept,
		// for the gaps the labels
		// are removed (i.e. replaced by the basic Chiang label)
		Integer ruleLabelKey = complexLabelsRuleRepresentation.getPartKeys().get(complexLabelsRuleRepresentation.getPartKeys().size() - 1);
		resultKeys.add(getSourceSideLabelsOnlyDowngradedLabelKey(ruleLabelKey));
		return IntegerKeyRuleRepresentationWithLabel.createIntegerKeyRuleRepresentationWithLabel(resultKeys);
	}

	public IntegerKeyRuleRepresentation getDowngradedToTargetSideLabelsOnlyRuleSideRepresentation(IntegerKeyRuleRepresentation complexLabelsRuleSideRepresentation) {
		List<Integer> resultKeys = new ArrayList<Integer>();
		for (int i = 0; i < complexLabelsRuleSideRepresentation.getPartKeys().size(); i++) {
			resultKeys.add(getTargetSideLabelsOnlyDowngradedLabelKey(complexLabelsRuleSideRepresentation.getPartKeys().get(i)));
		}
		return IntegerKeyRuleRepresentation.createIntegerKeyRuleRepresentation(resultKeys);
	}

	public int getFullyDowngradedLabelKey(Integer key) {
		return this.getItemHandler(key).getFullyDowngradedKeyRepresentation(key);
	}

	private int getSourceSideLabelsOnlyDowngradedLabelKey(Integer key) {
		WordKeyMappingTabbleItemHandler itemHandler = this.getItemHandler(key);
		Assert.assertNotNull(itemHandler);
		return itemHandler.getSourceSideLabelsOnlyDowngradedKeyRepresentation(key);
	}

	public int getTargetSideLabelsOnlyDowngradedLabelKey(Integer key) {
		WordKeyMappingTabbleItemHandler itemHandler = this.getItemHandler(key);
		Assert.assertNotNull(itemHandler);
		return itemHandler.getTargetSideLabelsOnlyDowngradedKeyRepresentation(key);
	}

	public int convertGapLabelIndexToLeftHandSideLabelIndex(Integer key){
	    	if(!isGapLabelKey(key)){
	    	    throw new RuntimeException("Error: trying to call convertGapLabelIndexToLeftHandSideLabelIndex for non-gap label");
	    	}
		WordKeyMappingTabbleItemHandler itemHandler = this.getItemHandler(key);
		Assert.assertNotNull(itemHandler);
		return this.getKeyForWord(itemHandler.getLeftHandSideLabelForAnyLabelIndex(key));
	}
	
	public static List<String> getUniqueWordsFromFile(String fileName) {
		List<String> result = new ArrayList<String>();

		try {
			BufferedReader fileReader = new BufferedReader(new FileReader(fileName));

			String line = null;

			int lineNo = 1;

			Set<String> uniqueWords = new HashSet<String>();
			while ((line = fileReader.readLine()) != null) {
				printAddMessage(lineNo);

				List<String> words = InputReading.getWordsFromString(line);

				uniqueWords.addAll(words);

				lineNo++;
			}

			result.addAll(uniqueWords);
			fileReader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getComplexRuleLabelWithoutEnclosingBrackets(int key) {
		ConcurrentMap<Integer, String> keyToWordMappingTable = this.wordKeyMappingTableData.getKeyToWordMappingTable();
		String ruleLabel = keyToWordMappingTable.get(key);
		assert (ruleLabel.substring(0, 1).equals("["));
		assert (ruleLabel.substring(ruleLabel.length() - 1, ruleLabel.length()).equals("]"));
		String ruleLabelWithoutEnclosingBrackets = keyToWordMappingTable.get(key).substring(1, ruleLabel.length() - 1);
		return ruleLabelWithoutEnclosingBrackets;
	}

	public List<String> getComplexRuleLabelsWithoutEnclosingBrackets() {
		List<String> result = new ArrayList<String>();
		for (int key : getComplexRuleLabels()) {
			result.add(getComplexRuleLabelWithoutEnclosingBrackets(key));
		}
		return result;
	}

	public List<Integer> getComplexRuleLabels() {
		List<Integer> result = new ArrayList<Integer>();
		ConcurrentMap<Integer, String> keyToWordMappingTable = this.wordKeyMappingTableData.getKeyToWordMappingTable();
		for (int key = 1; key < this.getMaxNumLabelTypes(); key++) {
			if (keyToWordMappingTable.containsKey(key)) {
				result.add(key);
			}
		}
		return result;
	}

	private List<Integer> getComplexGapLabelsForGapNumer(int gapNumber) {
		List<Integer> result = new ArrayList<Integer>();
		ConcurrentMap<Integer, String> keyToWordMappingTable = this.wordKeyMappingTableData.getKeyToWordMappingTable();
		for (int key = ((gapNumber) * this.getMaxNumLabelTypes()) + 1; key < ((gapNumber + 1) * this.getMaxNumLabelTypes()); key++) {
			if (keyToWordMappingTable.containsKey(key)) {
				result.add(key);
			}
		}
		return result;
	}

	private List<Integer> getComplexGapLabels() {
		List<Integer> result = new ArrayList<Integer>();
		result.addAll(getComplexGapLabelsForGapNumer(1));
		result.addAll(getComplexGapLabelsForGapNumer(2));
		return result;
	}

	private String getComplexGapLabelWithoutGapNumberAndWithoutEnclosingBrackets(int key) {
		ConcurrentMap<Integer, String> keyToWordMappingTable = this.wordKeyMappingTableData.getKeyToWordMappingTable();
		String gapLabel = keyToWordMappingTable.get(key);
		assert (gapLabel.substring(0, 1).equals("["));
		assert (gapLabel.substring(gapLabel.length() - 1, gapLabel.length()).equals("]"));
		String ruleLabelWithoutEnclosingBrackets = keyToWordMappingTable.get(key).substring(1, gapLabel.lastIndexOf(","));
		return ruleLabelWithoutEnclosingBrackets;
	}

	public List<String> getComplexGapLabelsWithoutGapNumbersAndWithoutEnclosingBrackets() {
		List<String> result = new ArrayList<String>();
		for (int key : getComplexGapLabels()) {
			result.add(getComplexGapLabelWithoutGapNumberAndWithoutEnclosingBrackets(key));
		}
		return result;
	}

	public List<String> getComplexRuleAndGapLabelsWithoutEnclosingBrackets() {
		List<String> result = new ArrayList<String>();
		result.addAll(getComplexRuleLabelsWithoutEnclosingBrackets());
		result.addAll(getComplexGapLabelsWithoutGapNumbersAndWithoutEnclosingBrackets());
		return result;
	}
	
	public void printWordToKeyMappingTable(){
	    wordKeyMappingTableData.printWordToKeyMappingTable();
	}
	
	public void printKeyToWordMappingTable(){
	    wordKeyMappingTableData.printKeyToWordMappingTable();
	}

}
