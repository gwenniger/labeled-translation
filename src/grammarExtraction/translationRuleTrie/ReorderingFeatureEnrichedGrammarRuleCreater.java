package grammarExtraction.translationRuleTrie;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsSet;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsSetCreater;
import grammarExtraction.reorderingLabeling.RuleFeaturesReordering;
import grammarExtraction.translationRules.LightWeightTranslationRuleWithFeatures;
import grammarExtraction.translationRules.TranslationRuleFeatures;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;
import util.Pair;

public class ReorderingFeatureEnrichedGrammarRuleCreater extends FeatureEnrichedGrammarRuleCreator {

	protected ReorderingFeatureEnrichedGrammarRuleCreater(TranslationRuleProbabilityTable mainRuleProbabilityTable, PhraseProbabilityWeightsSetCreater phraseProbabilityWeightsSetCreater,
			SystemIdentity systemIdentity) {
		super(mainRuleProbabilityTable, phraseProbabilityWeightsSetCreater, systemIdentity);
	}

	public static ReorderingFeatureEnrichedGrammarRuleCreater createReorderingFeatureEnrichedGrammarRuleCreater(TranslationRuleProbabilityTable mainRuleProbabilityTable,
			PhraseProbabilityWeightsSetCreater phraseProbabilityWeightsSetCreater, SystemIdentity systemIdentity) {
		return new ReorderingFeatureEnrichedGrammarRuleCreater(mainRuleProbabilityTable, phraseProbabilityWeightsSetCreater, systemIdentity);
	}

	@Override
	protected String getHieroJoshuaRuleRepresentation(TranslationRuleSignatureTriple translationRuleSignatureTriple, PhraseProbabilityWeightsSet phraseProbabilityWeightsSet,
			Pair<Double> lexicalProbabilities, double ruleCorpusCount, boolean useExtraSAMTFeatures, boolean writeLabeledFeaturesRepresentation,double ruleGivenLabelGenerativeProbability) {
		LightWeightTranslationRuleWithFeatures ruleWithFeatures;

		ruleWithFeatures = LightWeightTranslationRuleWithFeatures.createLightWeightTranslationRuleWithReorderingInformationFeatures(
				ruleGivenLabelGenerativeProbability, translationRuleSignatureTriple, phraseProbabilityWeightsSet, lexicalProbabilities.getSecond(),
				lexicalProbabilities.getFirst(), ruleCorpusCount, !translationRuleSignatureTriple.getTargetSideRepresentationWithLabel().isSyntacticRuleSide(), false, false,
				mainRuleProbabilityTable.wordKeyMappingTable, systemIdentity.getReorderingFeatureSetCreater());

		ruleWithFeatures = returnScaleProbabilitiesRuleIfMosesUnknownWordsRule(ruleWithFeatures);
		String result;

		if (writeLabeledFeaturesRepresentation) {
			result = ruleWithFeatures.getSparseFeaturesRuleRepresentation(mainRuleProbabilityTable.wordKeyMappingTable, systemIdentity.getTranslationRuleRepresentationCreater());
		} else {
			result = ruleWithFeatures.getNormalRuleRepresentation(mainRuleProbabilityTable.wordKeyMappingTable, systemIdentity.getTranslationRuleRepresentationCreater());
		}
		return result;
	}

	@Override
	public boolean useExtraSamtFeatures() {
		return true;
	}

	@Override
	public TranslationRuleFeatures getUnknownWordsTranslationRuleFeatures(SystemIdentity systemIdentity, boolean isHieroRule) {
		return RuleFeaturesReordering.createCorrectSizeUnknnownWordRuleRuleFeaturesReordering(systemIdentity, isHieroRule);
	}

	@Override
	public TranslationRuleFeatures getReorderingLabelToPhraseLabelSwitchRuleFeatures(SystemIdentity systemIdentity) {
		return RuleFeaturesReordering.createCorrectSizeReorderingLabelToPhraseLabelSwitchRuleFeaturesReordering(systemIdentity);
	}

	@Override
	public TranslationRuleFeatures geGlueRuleFeatures(SystemIdentity systemIdentity, double generativeGlueLabelProbability, boolean isHieroRule, double rarityPenalty) {
		return RuleFeaturesReordering.createCorrectSizeGlueRuleFeaturesReordering(systemIdentity, generativeGlueLabelProbability, isHieroRule, rarityPenalty);
	}
}
