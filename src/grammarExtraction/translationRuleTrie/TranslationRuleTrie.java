package grammarExtraction.translationRuleTrie;

import java.util.Enumeration;
import java.util.List;

import junit.framework.Assert;
import bitg.Pair;
import grammarExtraction.IntegerKeyRuleRepresentation;
import grammarExtraction.IntegerKeyRuleRepresentationWithLabel;
import grammarExtraction.grammarExtractionFoundation.LabelingSettings.CanonicalFormSettings;
import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;
import grammarExtraction.samtGrammarExtraction.GapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator;
import grammarExtraction.translationRules.LightWeightTranslationRule;

public class TranslationRuleTrie {
	private final MTRuleTrieNodeRoot sourceTrie;
	private final MTRuleTrieNodeRoot targetTrie;
	private final WordKeyMappingTable wordKeyMappingTable;
	private final boolean useJoshuaStyleSimplifiedCountComputing;
	private double totalSourceTotTargetCountsLostInFiltering = 0;
	private double totalTargetToSourceCountsLostInFiltering = 0;
	private final boolean filterUsingSourceSideRuleLabels;

	private TranslationRuleTrie(MTRuleTrieNodeRoot sourceTrie, MTRuleTrieNodeRoot targetTrie, WordKeyMappingTable wordKeyMappingTable, boolean useJoshuaStyleSimplifiedCountComputing,
			boolean filterUsingSourceSideRuleLabels) {
		this.sourceTrie = sourceTrie;
		this.targetTrie = targetTrie;
		this.wordKeyMappingTable = wordKeyMappingTable;
		this.useJoshuaStyleSimplifiedCountComputing = useJoshuaStyleSimplifiedCountComputing;
		this.filterUsingSourceSideRuleLabels = filterUsingSourceSideRuleLabels;
	}

	public static TranslationRuleTrie createTranslationRuleTrie(WordKeyMappingTable wordKeyMappingTable, boolean useJoshuaStyleSimplifiedCountComputing, boolean filterUsingSourceSideRuleLabels,
			CanonicalFormSettings canonicalFormSettings) {
		MTRuleTrieNodeRoot sourceTrie = MTRuleTrieNodeRoot.createMTRuleTrieNodeRoot(wordKeyMappingTable, createCountItemCreater(canonicalFormSettings.useCanonicalFormLabeledRules()),canonicalFormSettings);
		MTRuleTrieNodeRoot targetTrie = MTRuleTrieNodeRoot.createMTRuleTrieNodeRoot(wordKeyMappingTable, new BasicCountItem(),canonicalFormSettings);
		return new TranslationRuleTrie(sourceTrie, targetTrie, wordKeyMappingTable, useJoshuaStyleSimplifiedCountComputing, filterUsingSourceSideRuleLabels);
	}

	public static CountItemCreater createCountItemCreater(boolean outputOnlyCanonicalFormLabeledRules) {
		if (outputOnlyCanonicalFormLabeledRules) {
			return new LexicalWeightCountItemWithLabeledRuleInformation();
		} else {
			System.out.println("Create LexicalWeightCountItem CountItemCreater");
			return new LexicalWeightCountItem();
		}
	}

	public double getTotalSourceToTargetRuleCounts() {
		double result = 0;

		TrieElementsEnumeration tablesEnumaration = TrieElementsEnumeration.createTrieElementsEnumeration(getSourceTrie());

		// There should be at least one element, otherwise something is
		// seriously wrong
		assert (tablesEnumaration.hasMoreElements());

		while (tablesEnumaration.hasMoreElements()) {

			Pair<IntegerKeyRuleRepresentationWithLabel, RuleCountsTable> trieElement = tablesEnumaration.nextElement();
			result += trieElement.last.getTotalCountUnlabeldConditionedSideRules();
		}

		return result;
	}

	public double getTotalSourceToTargetFilteredCounts() {
		return this.totalSourceTotTargetCountsLostInFiltering;
	}

	public double getTotalTargetToSourceFilteredCounts() {
		return this.totalTargetToSourceCountsLostInFiltering;
	}

	public WordKeyMappingTable getWordKeyMappintTable() {
		return this.wordKeyMappingTable;
	}

	public double getTotalTargetToSourceRuleCounts() {
		double result = 0;

		TrieElementsEnumeration tablesEnumaration = TrieElementsEnumeration.createTrieElementsEnumeration(getTargetTrie());

		while (tablesEnumaration.hasMoreElements()) {
			Pair<IntegerKeyRuleRepresentationWithLabel, RuleCountsTable> trieElement = tablesEnumaration.nextElement();
			result += trieElement.last.getTotalCountUnlabeldConditionedSideRules();
		}

		return result;
	}

	private double computeNormalizedCountToAddPerRule(LightWeightPhrasePairRuleSet ruleSet) {
		if (useJoshuaStyleSimplifiedCountComputing) {
			return ruleSet.getRuleSetWeight();
		}
		return (((double) ruleSet.getRuleSetWeight()) / ruleSet.getNumRules());
	}

	
	
	
	private void addGapLabelGivenLeftHandSideLabelCounts(LightWeightTranslationRule rule,GapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator gapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator,
		double amountToAdd){
	   
	    Integer sourceLeftHandSideLabel = rule.getLefthHandSideLabel().getSourceSideLabel();
	    List<Integer> sourceGapLabelKeys =  rule.getSourceGapLabelKeys(wordKeyMappingTable);
	    int numGaps = sourceGapLabelKeys.size();
	    double amountToAddPerGap = amountToAdd / numGaps;
	    
	    for(Integer sourceSideGapLabelKey : sourceGapLabelKeys){
		util.Pair<Integer> labelKeyPair = new util.Pair<Integer>(sourceLeftHandSideLabel, sourceSideGapLabelKey);
		gapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator.increaseCount(labelKeyPair, amountToAddPerGap,wordKeyMappingTable);
	    }
	}
	
	public double addRulesFromSetToTableIfSourceSidePresentInTrieAndIncreaseLabelCount(LightWeightPhrasePairRuleSet ruleSet, LabelProbabilityEstimator labelProbabilityEstimator,
		GapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator gapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator) {
		// System.out.println("TranslationRuleTrie.addRulesFromSetToTableIfSourceSidePresentInTrie...");

		double totalAmountAdded = 0;

		double amountToAdd = computeNormalizedCountToAddPerRule(ruleSet);

		for (LightWeightTranslationRule rule : ruleSet.getRules()) {

			if (sourceRuleSideInTrie(rule)) {
				// System.out.println(rule);
				CountUpdateItem countUpdateItem = new CountUpdateItem(amountToAdd, rule.getLexicalWeightSourceToTarget(), rule.getLexicalWeightTargetToSource());
				increaseTargetGivenSourceCount(rule, countUpdateItem);
				totalAmountAdded += amountToAdd;

				// Increase the count of the label in the
				// LabelProbabilityEstimator with an equal amount
				labelProbabilityEstimator.increaseCount(rule.getLefthHandSideLabel().getSourceSideLabel(), amountToAdd);
				
				if(!rule.isGlueStartOrEndRule(wordKeyMappingTable) && (!rule.isPureHieroRule(wordKeyMappingTable))){
				    addGapLabelGivenLeftHandSideLabelCounts(rule, gapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator, amountToAdd);
				}    
				
			} else {
				totalSourceTotTargetCountsLostInFiltering += amountToAdd;
			}
		}

		return totalAmountAdded;
	}

	public double addRulesFromSetToTableIfSourceSidePresentInTrie(LightWeightPhrasePairRuleSet ruleSet) {
		// System.out.println("TranslationRuleTrie.addRulesFromSetToTableIfSourceSidePresentInTrie...");

		double totalAmountAdded = 0;

		double amountToAdd = computeNormalizedCountToAddPerRule(ruleSet);

		for (LightWeightTranslationRule rule : ruleSet.getRules()) {

			if (sourceRuleSideInTrie(rule)) {
				// System.out.println("addRulesFromSetToTableIfSourceSidePresentInTrie "
				// + rule.getJoshuaRuleRepresentation(wordKeyMappingTable));

				CountUpdateItem countUpdateItem = new CountUpdateItem(amountToAdd, rule.getLexicalWeightSourceToTarget(), rule.getLexicalWeightTargetToSource());
				Assert.assertTrue(countUpdateItem.getLexicalWeightSourceToTarget() >= 0);
				Assert.assertTrue(countUpdateItem.getLexicalWeightTargetToSource() >= 0);
				increaseTargetGivenSourceCount(rule, countUpdateItem);
				totalAmountAdded += amountToAdd;
			} else {
				totalSourceTotTargetCountsLostInFiltering += amountToAdd;
			}
		}

		return totalAmountAdded;
	}

	public double addRulesFromSetToTableIfTargetSidePresentInTrie(LightWeightPhrasePairRuleSet ruleSet) {
		// System.out.println("TranslationRuleTrie.addRulesFromSetToTableIfSourceSidePresentInTrie...");

		double totalAmountAdded = 0;

		double amountToAdd = computeNormalizedCountToAddPerRule(ruleSet);
		for (LightWeightTranslationRule rule : ruleSet.getRules()) {
			if (targetRuleSideInTrie(rule)) {
				// System.out.println(rule);
				CountUpdateItem countUpdateItem = new CountUpdateItem(amountToAdd, rule.getLexicalWeightSourceToTarget(), rule.getLexicalWeightTargetToSource());
				// increaseSourceGivenTargetCount(rule, countUpdateItem);
				increaseSourceGivenTargetCountAnomouslyIfConditionedRuleRepresentationNotYetPresent(rule, countUpdateItem);
				totalAmountAdded += amountToAdd;
			} else {
				totalTargetToSourceCountsLostInFiltering += amountToAdd;
			}
		}

		return totalAmountAdded;
	}

	public double addPhrasePairRuleSetToTable(LightWeightPhrasePairRuleSet ruleSet,LabelProbabilityEstimator labelProbabilityEstimator) {
		double totalAmountAdded = 0;

		double amountToAdd = computeNormalizedCountToAddPerRule(ruleSet);
		for (LightWeightTranslationRule rule : ruleSet.getRules()) {
			CountUpdateItem countUpdateItem = new CountUpdateItem(amountToAdd, rule.getLexicalWeightSourceToTarget(), rule.getLexicalWeightTargetToSource());
			increaseSourceGivenTargetCount(rule, countUpdateItem);
			increaseTargetGivenSourceCount(rule, countUpdateItem);
			totalAmountAdded += amountToAdd;
			
			// Increase the count of the label in the
			// LabelProbabilityEstimator with an equal amount
			labelProbabilityEstimator.increaseCount(rule.getLefthHandSideLabel().getSourceSideLabel(), amountToAdd);
		}

		return totalAmountAdded;
	}

	public void addPhrasePairRuleSetToTableWithoutAddingCounts(LightWeightPhrasePairRuleSet ruleSet) {
		// System.out.println("TranslationRuleTrie. addPhrasePairRuleSetToTableWithoutAddingCounts...");

		for (LightWeightTranslationRule rule : ruleSet.getRules()) {
			this.getSourceTrie().addTranslationRuleToTrie(rule.getSourceSideRepresentationWithLabel(), rule.getTargetSideRepresentationWithLabel(), getTargetTrie());
		}

	}

	private IntegerKeyRuleRepresentationWithLabel getDownGradedRuleSourceSideRepresentation(LightWeightTranslationRule rule) {

		if (this.filterUsingSourceSideRuleLabels && rule.isSourceSideLabeledRule(this.wordKeyMappingTable)) {
			return this.wordKeyMappingTable.getDowngradedToSourceSideRuleLabelOnlyRuleRepresentation(rule.getSourceSideRepresentationWithLabel());
		} else {
			Assert.assertNotNull(wordKeyMappingTable);
			Assert.assertNotNull(rule);
			Assert.assertNotNull(rule.getSourceSideRepresentationWithLabel());
			return this.wordKeyMappingTable.getFullyDowngradedLabelsRuleRepresentation(rule.getSourceSideRepresentationWithLabel());
		}
	}

	public boolean sourceRuleSideInTrie(LightWeightTranslationRule rule) {
		boolean sourceSideInTrie = this.getSourceTrie().containsRuleRepresentation(getDownGradedRuleSourceSideRepresentation(rule));
		return (sourceSideInTrie);
	}

	public boolean targetRuleSideInTrie(LightWeightTranslationRule rule) {
		IntegerKeyRuleRepresentationWithLabel integerKeyRuleRepresentationWithLabel = IntegerKeyRuleRepresentationWithLabel.createIntegerKeyRuleRepresentationWithLabel(
				rule.getTargetSideRepresentation(), rule.getLefthHandSideLabel().getTargetSideLabel());
		boolean targetSideInTrie = this.getTargetTrie().containsRuleRepresentation(integerKeyRuleRepresentationWithLabel);
		return (targetSideInTrie);
	}

	public void addPhrasePairRuleSetToTableAndUpdateCollisionCount(LightWeightPhrasePairRuleSet ruleSet, HashCollisionCounter<IntegerKeyRuleRepresentation> sourceCollisionCounter,
			HashCollisionCounter<IntegerKeyRuleRepresentation> targetCollisionCounter) {

		double amountToAdd = computeNormalizedCountToAddPerRule(ruleSet);
		for (LightWeightTranslationRule rule : ruleSet.getRules()) {
			CountUpdateItem countUpdateItem = new CountUpdateItem(amountToAdd, rule.getLexicalWeightSourceToTarget(), rule.getLexicalWeightTargetToSource());

			increaseTargetGivenSourceCount(rule, countUpdateItem);
			sourceCollisionCounter.addObject(rule.getSourceSideRepresentation());
			targetCollisionCounter.addObject(rule.getTargetSideRepresentation());
		}

	}

	private void increaseTargetGivenSourceCount(LightWeightTranslationRule rule, CountUpdateItem countUpdateItem) {
		this.getSourceTrie().addTranslationRuleCountToTrie(rule.getSourceSideRepresentationWithLabel(), rule.getTargetSideRepresentationWithLabel(), countUpdateItem, getTargetTrie());
	}

	private void increaseSourceGivenTargetCount(LightWeightTranslationRule rule, CountUpdateItem countUpdateItem) {
		this.getTargetTrie().addTranslationRuleCountToTrie(rule.getTargetSideRepresentationWithLabel(), rule.getSourceSideRepresentationWithLabel(), countUpdateItem, getSourceTrie());
	}

	private void increaseSourceGivenTargetCountAnomouslyIfConditionedRuleRepresentationNotYetPresent(LightWeightTranslationRule rule, CountUpdateItem countUpdateItem) {
		this.getTargetTrie().addTranslationRuleCountToTrieAnomouslyIfConditionedRuleRepresentationNotYetPresent(rule.getTargetSideRepresentationWithLabel(),
				rule.getSourceSideRepresentationWithLabel(), countUpdateItem, getSourceTrie());
	}

	public int getTotalNumSourceToTargetRules() {
		assert (this.getSourceTrie() != null);
		return this.getSourceTrie().getTotalNumRules();
	}

	public int getTotalNumTargetToSourceRules() {
		assert (this.getSourceTrie() != null);
		return this.getTargetTrie().getTotalNumRules();
	}

	public void printSourceRuleCountTables(WordKeyMappingTable wordKeyMappingTable) {
		Enumeration<Pair<IntegerKeyRuleRepresentationWithLabel, RuleCountsTable>> elementsEnumeration = TrieElementsEnumeration.createTrieElementsEnumeration(getSourceTrie());

		while (elementsEnumeration.hasMoreElements()) {
			Pair<IntegerKeyRuleRepresentationWithLabel, RuleCountsTable> pair = elementsEnumeration.nextElement();
			IntegerKeyRuleRepresentationWithLabel sourceSide = pair.first;
			RuleCountsTable ruleCountsTable = pair.last;

			System.out.println("<<< Source side: " + sourceSide.getStringRepresentation(wordKeyMappingTable));
			System.out.println(ruleCountsTable.getCountItemsString());
		}
	}

	public MTRuleTrieNodeRoot getSourceTrie() {
		return sourceTrie;
	}

	public MTRuleTrieNodeRoot getTargetTrie() {
		return targetTrie;
	}

	/**
	 * Convenience method to determine whether we canonical form rules 
	 * are used in combination with only Hiero weights for these rules
	 * @return
	 */
	public boolean useCanonicalFormLabeledRulesWithOnlyHieroWeights(){
	    return this.targetTrie.useCanonicalFormLabeledRulesWithOnlyHieroWeights();
	}
	
}
