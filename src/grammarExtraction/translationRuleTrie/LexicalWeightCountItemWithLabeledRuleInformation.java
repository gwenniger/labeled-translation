package grammarExtraction.translationRuleTrie;

import java.util.List;

import util.Pair;

public class LexicalWeightCountItemWithLabeledRuleInformation extends LexicalWeightCountItem {

	private static final long serialVersionUID = 1L;
	private final LabeledRuleVersionsCollection labeledRuleVersionsCollection;

	private LexicalWeightCountItemWithLabeledRuleInformation(MTRuleTrieNode label, double count, LabeledRuleVersionsCollection labeledRuleVersionsCollection) {
		super(label, count);
		this.labeledRuleVersionsCollection = labeledRuleVersionsCollection;
	}

	public LexicalWeightCountItemWithLabeledRuleInformation() {
		this(null, 0, null);
	}

	/**
	 * For the unlabeled source side we are interested in the labeled rule versions information.
	 * Here the extended class (this class)  LexicalWeightCountItemWithLabeledRuleInformation is required.
	 */
	@Override 
	public CountItem createCountItemUnlabeledRuleSourceSide(MTRuleTrieNode label)
	{
		return new LexicalWeightCountItemWithLabeledRuleInformation(label, 0, LabeledRuleVersionsCollection.createLabeledRuleVersionsCollection());
	}
	
	/**
	 * For the labeled source side we will not store labeled rule version information,
	 * so we return a 'plain' LexicalWeightCountItem. 
	 * This is an optimization to save memory, as  LexicalWeightCountItemWithLabeledRuleInformation
	 * requires an extra  LabeledRuleVersionsCollection, which contains quite some extra memory, so 
	 * we don't want to use it if not necessary.
	 */
	@Override
	public CountItem createCountItemLabeledRuleSourceSide(MTRuleTrieNode label) {
		return new LexicalWeightCountItem(label,0);
	}
	

	@Override
	public void addLabeledRule(LabeledRuleWithCount labeledRuleWithCount) {
		labeledRuleVersionsCollection.addLabeledRule(labeledRuleWithCount.getRuleSourceSideTrieNode(), labeledRuleWithCount.getRuleTargetSideCountItem());
	}

	@Override
	public bitg.Pair<MTRuleTrieNode, CountItem> getMostFrequentLabeledRuleVersion() {
		return labeledRuleVersionsCollection.getMostFrequentLabeledRuleVersion();
	}
	
	@Override
	public RuleLabeling getMostFrequentRuleLabelingWithinAllowedRuleLabelingsForSourceList(
		List<RuleLabeling> mostFrequentRuleLabelingsForSourceList,
		WordKeyMappingTable wordKeyMappingTable) {
	    return labeledRuleVersionsCollection.getMostFrequentRuleLabelingWithinAllowedRuleLabelingsForSourceList(mostFrequentRuleLabelingsForSourceList, wordKeyMappingTable);
	}

}
