package grammarExtraction.translationRuleTrie;

import java.io.Serializable;
import java.util.concurrent.ConcurrentMap;

public class WordKeyMappingTableData implements Serializable {
	private static final long serialVersionUID = 1L;
	private final ConcurrentMap<String, Integer> wordToKeyMappingTable;
	private final ConcurrentMap<Integer, String> keyToWordMappingTable;
	private final int maxNumLabelTypes;

	protected WordKeyMappingTableData(ConcurrentMap<String, Integer> wordToKeyMappingTable, ConcurrentMap<Integer, String> keyToWordMappingTable,
			int maxNumLabelTypes) {
		this.wordToKeyMappingTable = wordToKeyMappingTable;
		this.keyToWordMappingTable = keyToWordMappingTable;
		this.maxNumLabelTypes = maxNumLabelTypes;
	}

	public ConcurrentMap<String, Integer> getWordToKeyMappingTable() {
		return wordToKeyMappingTable;
	}

	public ConcurrentMap<Integer, String> getKeyToWordMappingTable() {
		return keyToWordMappingTable;
	}

	public int getMaxNumLabelTypes() {
		return this.maxNumLabelTypes;
	}
	
	public void printWordToKeyMappingTable(){
	    System.out.println("Word to Key Mapping table: " + wordToKeyMappingTable);
	}
	
	public void printKeyToWordMappingTable(){
	    System.out.println("Word to Key Mapping table: " + keyToWordMappingTable);
	}
}
