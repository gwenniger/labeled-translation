package grammarExtraction.translationRuleTrie;

import grammarExtraction.samtGrammarExtraction.LabeledRuleFeaturesComputer;
import org.junit.Assert;

public class ProbabilityRepresentation {

	/**
	 * Represents a very high cost, corresponding to a very unlikely probability.
	 */
	static final float VERY_UNLIKELY = -1.0f * (float) Math.log(1.0e-9);

	public static float getJoshuaLogProbabilityRepresentation(double probability) {

		// System.out.println(" getJoshuaLogProbabilityRepresentation - probability " + probability);

		if ((probability >= 0)) {
			if (probability <= 1) {
				float result = 0;

				Assert.assertTrue(((float) (Math.log10(probability))) <= 0);
				result = -1.0f * ((float) (Math.log10(probability)));
				if (Float.isInfinite(result)) {
					// System.out.println("very unlikely");
					result = VERY_UNLIKELY;
				}

				Assert.assertTrue(result >= 0);

				return result;
			}
			throw new RuntimeException("ProbabilityRepresentation.getJoshuaLogProbabilityRepresentation called for number > 1");
		}
		throw new RuntimeException("ProbabilityRepresentation.getJoshuaLogProbabilityRepresentation called for number < 0");
	}

	public static void main(String[] args) {
		System.out.println("Proabability zero representation: " +  getJoshuaLogProbabilityRepresentation(0));
		System.out.println("Proabability one representation: " +  getJoshuaLogProbabilityRepresentation(1));
		System.out.println("Very unlikely probability: " + VERY_UNLIKELY);
		System.out.println("rarity penalty 0 : " +  LabeledRuleFeaturesComputer.computeRarityPenalty(0));
		System.out.println("rarity penalty 1 : " +  LabeledRuleFeaturesComputer.computeRarityPenalty(1));
	}
}
