package grammarExtraction.translationRuleTrie;

import grammarExtraction.IntegerKeyRuleRepresentation;

import java.util.Enumeration;
import java.util.List;

public class RuleCountsTableConditionedSidesEnumeration implements Enumeration<IntegerKeyRuleRepresentation> {

	private final List<CountItem> countItems;
	private int currentIndex;

	private RuleCountsTableConditionedSidesEnumeration(List<CountItem> countItems) {
		this.countItems = countItems;
		this.currentIndex = -1;
	}

	public static RuleCountsTableConditionedSidesEnumeration createRuleCountsTableConditionedSidesEnumeration(List<CountItem> countItems) {
		return new RuleCountsTableConditionedSidesEnumeration(countItems);
	}

	@Override
	public boolean hasMoreElements() {
		return (this.currentIndex < (this.countItems.size() - 1));
	}

	@Override
	public IntegerKeyRuleRepresentation nextElement() {
		this.currentIndex++;
		return countItems.get(this.currentIndex).getLabel().restoreRuleRepresentation();
	}
}
