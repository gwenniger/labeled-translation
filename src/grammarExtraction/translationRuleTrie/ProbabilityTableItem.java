package grammarExtraction.translationRuleTrie;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;

public class ProbabilityTableItem implements Serializable
{
	private static final long serialVersionUID = 1L;
	private AtomicInteger count;
	private double probability;
	
	
	public ProbabilityTableItem()
	{
		this.count = new AtomicInteger();
		this.probability = -1;
	}
	
	public void incrementCount()
	{
		this.count.incrementAndGet();
	}
	
	public  synchronized void setProbability(double probability)
	{
		this.probability = probability;
	}
	
	public double getProbability()
	{
		return this.probability;
	}
	
	public int getCount()
	{
		return this.count.get();
	}
}
