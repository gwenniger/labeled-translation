package grammarExtraction.translationRuleTrie;

import java.io.Serializable;


public class LexicalProbabilityTables implements Serializable
{
	private static final long serialVersionUID = 1L;
	private final ConcurrentLexicalProbabilityTable lexicalProbabilityTableSourceGivenTarget;
	private final ConcurrentLexicalProbabilityTable lexicalProbabilityTableTargetGivenSource;
	
	private LexicalProbabilityTables(ConcurrentLexicalProbabilityTable lexicalProbabilityTableSourceToTarget, ConcurrentLexicalProbabilityTable lexicalProbabilityTableTargetToSource)
	{
		this.lexicalProbabilityTableSourceGivenTarget = lexicalProbabilityTableSourceToTarget;
		this.lexicalProbabilityTableTargetGivenSource = lexicalProbabilityTableTargetToSource;
	}
	
	public static LexicalProbabilityTables createLexicalProbabilityTables(WordKeyMappingTable wordKeyMappingTable,int noConcurrentThreads)
	{
		ConcurrentLexicalProbabilityTable lexicalProbabilityTableSourceToTarget = ConcurrentLexicalProbabilityTable.createConcurrentLexicalProbabilityTable(wordKeyMappingTable,noConcurrentThreads);
		ConcurrentLexicalProbabilityTable lexicalProbabilityTableTargetToSource = ConcurrentLexicalProbabilityTable.createConcurrentLexicalProbabilityTable(wordKeyMappingTable,noConcurrentThreads);
		return new LexicalProbabilityTables(lexicalProbabilityTableSourceToTarget, lexicalProbabilityTableTargetToSource);
	}

	public ConcurrentLexicalProbabilityTable getLexicalProbabilityTableSourceGivenTarget() {
		return lexicalProbabilityTableSourceGivenTarget;
	}

	public ConcurrentLexicalProbabilityTable getLexicalProbabilityTableTargetGivenSource() {
		return lexicalProbabilityTableTargetGivenSource;
	}
	
	private boolean testSourToTargetTable()
	{
		boolean result =  getLexicalProbabilityTableSourceGivenTarget().tableCountIsAddedCount();
		assert(result);
		return result;
	}
	
	private boolean testTargetToSourceTable()
	{
		boolean result = getLexicalProbabilityTableTargetGivenSource().tableCountIsAddedCount();
		assert(result);
		return result;
	}
	
	public boolean testTables()
	{
		return testSourToTargetTable() && testTargetToSourceTable();
	}
	
	
	public void computeProbabilitiesFromCounts()
	{
		getLexicalProbabilityTableSourceGivenTarget().computeProbabilitiesFromCounts();
		getLexicalProbabilityTableTargetGivenSource().computeProbabilitiesFromCounts();
	}	
	 
	
	
}
