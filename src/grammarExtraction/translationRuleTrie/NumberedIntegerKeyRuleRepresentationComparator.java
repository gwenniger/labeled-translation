package grammarExtraction.translationRuleTrie;

import grammarExtraction.IntegerKeyRuleRepresentationWithLabel;

import java.util.Comparator;

import bitg.Pair;

public class NumberedIntegerKeyRuleRepresentationComparator implements Comparator<Pair<IntegerKeyRuleRepresentationWithLabel, Double>>
{

	@Override
	public int compare(Pair<IntegerKeyRuleRepresentationWithLabel, Double> o1, Pair<IntegerKeyRuleRepresentationWithLabel, Double> o2) 
	{
		return Double.compare((Double)o1.last, (Double)o2.last);
	}



}
