package grammarExtraction.translationRuleTrie;

import java.util.regex.Matcher;
import grammarExtraction.translationRules.LightWeightTranslationRule;
import grammarExtraction.translationRules.RuleGap;
import grammarExtraction.translationRules.RuleLabel;
import junit.framework.Assert;

public class LabelItemHandler extends WordKeyMappingTabbleItemHandler {
	private static final long serialVersionUID = 1L;

	protected LabelItemHandler(WordKeyMappingTableData wordKeyMappingTableData, int writeIndex, int maxNumLabelTypes) {
		super(wordKeyMappingTableData, writeIndex, maxNumLabelTypes);
	}

	@Override
	protected Integer getFullyDowngradedKeyRepresentation(Integer key) {
		return this.getBaseIndex();
	}

	@Override
	protected String getItemHandlerName() {
		return "LabelItemHandler";
	}

	private String getStringForKey(int key) {
		return this.wordKeyMappingTableData.getKeyToWordMappingTable().get(key);
	}

	private Integer getKeyForStringAndAddEntryToWordKeyMappingTableIfNotYetPresent(String keyString) {
		if (!this.wordKeyMappingTableData.getWordToKeyMappingTable().containsKey(keyString)) {
			this.addEntryForString(keyString);
		}
		return this.wordKeyMappingTableData.getWordToKeyMappingTable().get(keyString);
	}

	private static String reconstructLabelString(String labelString, String labelPart) {
		// System.out.println(" reconstructLabelString - labelString: " +
		// labelString);

		if (WordKeyMappingTable.isRuleLabel(labelString)) {
			return RuleLabel.ruleLabelString(labelPart);
		} else if (WordKeyMappingTable.isLabelOne(labelString)) {
			return RuleGap.ruleGapString(labelPart, 1);
		} else if (WordKeyMappingTable.isLabelTwo(labelString)) {
			return RuleGap.ruleGapString(labelPart, 2);
		} else {
			throw new RuntimeException("Error : LabelItemHandler - reconstructLabelString : unrecognized label type");
		}
	}
	
	
	private static String getGapLabelLabelPart(String labelString){
	    return labelString.substring(1, labelString.lastIndexOf(","));
	}
	
	/**
	 * Returns the 
	 * @param labelString
	 * @return
	 */
	private static String convertGapLabelToLeftHandSideLabelString(String labelString){
	   if (WordKeyMappingTable.isLabelOne(labelString) || WordKeyMappingTable.isLabelTwo(labelString)) {
	 	    return RuleLabel.ruleLabelString(getGapLabelLabelPart(labelString));
	   }
	   else {
	    throw new RuntimeException(
		    "Error : LabelItemHandler - must be called with a gap label");
	   }
	}
	
	@Override
	protected  String  getLeftHandSideLabelForAnyLabelIndex(Integer key)
	{
	    String labelString = this.getStringForKey(key);
	    // Nothing needs to be changed for left-hand side labels themselves
	    if (WordKeyMappingTable.isRuleLabel(labelString)) {
		return labelString;
	    }
	    else{
		String leftHandSideLabelString = convertGapLabelToLeftHandSideLabelString(labelString);
		return leftHandSideLabelString;
	    }
	}
	

	private static String getOneSideLabelsOnlyDowngradedStringRepresentation(Matcher preservableLabelPartMatcher, String labelString) {
		Assert.assertTrue(labelString.contains(LightWeightTranslationRule.getSourceLabelsPropertyTag()));
		if (preservableLabelPartMatcher.find()) {
			String sourceLabelPart = labelString.substring(preservableLabelPartMatcher.start(), preservableLabelPartMatcher.end());
			String downgradedLabelString = reconstructLabelString(labelString, sourceLabelPart);
			return downgradedLabelString;
		}
		throw new RuntimeException("No source side label found");
	}

	private Integer getOneSideLabelsOnlyDowngradedKeyRepresentation(Matcher preservableLabelPartMatcher, String labelString) {
		return getKeyForStringAndAddEntryToWordKeyMappingTableIfNotYetPresent(getOneSideLabelsOnlyDowngradedStringRepresentation(preservableLabelPartMatcher,
				labelString));
	}

	private Matcher getSourceSideLabelMatcher(String labelString) {
		return LightWeightTranslationRule.SOURCE_LABEL_PATTERN.matcher(labelString);
	}

	private Matcher getTargetSideLabelMatcher(String labelString) {
		return LightWeightTranslationRule.TARGET_LABEL_PATTERN.matcher(labelString);
	}

	@Override
	protected Integer getSourceSideLabelsOnlyDowngradedKeyRepresentation(Integer key) {
		String labelString = getStringForKey(key);
		return getOneSideLabelsOnlyDowngradedKeyRepresentation(getSourceSideLabelMatcher(labelString), labelString);
	}

	@Override
	protected Integer getTargetSideLabelsOnlyDowngradedKeyRepresentation(Integer key) {
		String labelString = getStringForKey(key);
		// System.out.println("getTargetSideLabelsOnlyDowngradedKeyRepresentation - labelString: "+
		// labelString + " key: " + key);
		return getOneSideLabelsOnlyDowngradedKeyRepresentation(getTargetSideLabelMatcher(labelString), labelString);
	}
}
