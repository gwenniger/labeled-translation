package grammarExtraction.translationRuleTrie;

import grammarExtraction.IntegerKeyRuleRepresentation;

import java.io.Serializable;
import java.util.List;

import arraySkipList.ArraySkipList;

public abstract class TrieNode implements Serializable {
	private static final long serialVersionUID = 1L;
	protected static final IntLabelTrieNodeComparator LabelBasedChildComparator = new IntLabelTrieNodeComparator();
	protected ArraySkipList<TrieNode> childNodesSkipList;
	protected int label;

	protected TrieNode() {

	}

	protected TrieNode(ArraySkipList<TrieNode> childNodes, Integer label) {
		this.childNodesSkipList = childNodes;
		this.label = label;
	}

	protected abstract TrieNode createSearchNode(Integer childLabel);

	// Previously synchronized

	protected TrieNode addOrRetrieveRuleRepresentation(IntegerKeyRuleRepresentation sourceRuleRepresentation)
	{
		return addOrRetrieveRuleRepresentation(sourceRuleRepresentation, 0);
	}
	
	
	protected TrieNode addOrRetrieveRuleRepresentation(IntegerKeyRuleRepresentation sourceRuleRepresentation, int currentSourceIndex) {
		int sourceKey = sourceRuleRepresentation.getKey(currentSourceIndex);
		TrieNode child = getOrInsertAndGetChild(sourceKey);

		if (sourceRuleRepresentation.isLastIndex(currentSourceIndex)) {
			return child;
		} else {
			return child.addOrRetrieveRuleRepresentation(sourceRuleRepresentation, currentSourceIndex + 1);
		}
	}

	/**
	 * Retrieves the rule representation. If it is not present, return null.
	 * 
	 * @param conditionRuleRepresentation
	 * @return
	 */
	public TrieNode retrieveRuleRepresentation(IntegerKeyRuleRepresentation conditionRuleRepresentation) {
		return retrieveRuleRepresentation(conditionRuleRepresentation, 0);
	}

	/**
	 * Retrieves the rule representation. If it is not present, return null.
	 * 
	 * @param conditionRuleRepresentation
	 * @param currentSourceIndex
	 * @return
	 */
	// Previously synchronized
	private TrieNode retrieveRuleRepresentation(IntegerKeyRuleRepresentation conditionRuleRepresentation, int currentSourceIndex) {
		int sourceKey = conditionRuleRepresentation.getKey(currentSourceIndex);

		TrieNode child = getChildWithLabel(sourceKey);

		if ((child == null) || (conditionRuleRepresentation.isLastIndex(currentSourceIndex))) {
			return child;
		} else {
			return child.retrieveRuleRepresentation(conditionRuleRepresentation, currentSourceIndex + 1);
		}
	}

	// Previously synchronized
	public boolean containsRuleRepresentation(IntegerKeyRuleRepresentation sourceRuleRepresentation) {
		return (this.retrieveRuleRepresentation(sourceRuleRepresentation, 0) != null);
	}

	/**
	 * This method gets a child with a certain sourceKey if it is present in
	 * this node. If the source key is not present, the synchronized retrieval
	 * method getOrInsertAndGetChildSynchronized will be called. This will then
	 * try to get it again (in case another thread has already inserted it in
	 * the mean time) and if it is (Still) not present it will be inserted.
	 * 
	 * @param sourceKey
	 *            The source key
	 * @return
	 */
	protected TrieNode getOrInsertAndGetChild(Integer sourceKey) {
		/*
		 * // This strategy is not necessarily safe, but extra work is done to
		 * make it safe. // If you perform a binary search while another thread
		 * // can lock the TrieNode with
		 * getOrInsertAndGetChildSynchronized(sourceKey), // then that
		 * synchronized method can change the list and thus compromise the //
		 * integrity of your binary search. This can then compromise all the
		 * results. // However, it can be made safe if the method getChild is
		 * adapted to compensate for the // fact that another thread might
		 * change the child list during the binary search. // By simply checking
		 * the result of getChild before returning it, and if it is not correct
		 * // recursively calling getChild again, we save this strategy (which
		 * is much faster than // always using the synchronized method). The
		 * method getChild(Integer sourceKey) // in MTRuleTrieNodeRoot does not
		 * have the same issue, because in the root node // the children are
		 * stored in a ConcurrentMap instead of a list.
		 */
		MTRuleTrieNode result;

		// With the adapted code in MTTrieNode for getChild this should be safe
		// now
		result = (MTRuleTrieNode) getChildWithLabel(sourceKey);
		if (result != null) {
			return result;
		}
		return getOrInsertAndGetChildSynchronized(sourceKey);

	}

	protected abstract TrieNode getOrInsertAndGetChildSynchronized(Integer sourceKey);

	protected abstract TrieNode getChildWithLabel(Integer sourceKey);

	public Integer getLabel() {
		return label;
	}

	public int getNumChildren() {
		if (this.childNodesSkipList == null) {
			return 0;
		}
		return this.childNodesSkipList.size();
	}

	public String toString() {
		String result = "\n<TrieNode>";
		result += " Node Label: " + this.label + " ";
		result += "NumChildren: " + this.getNumChildren();

		if (this.childNodesSkipList != null) {
			String childLabels = "\t ChildLabels: ";
			for (TrieNode child : getChildNodes()) {
				childLabels += child.getLabel() + " ";
			}
			result += childLabels;
		}

		result += "</TrieNode>";
		return result;
	}

	public List<TrieNode> getChildNodes() {
		return (List<TrieNode>) this.childNodesSkipList.values();
	}

	public abstract TrieNode getChildWithIndex(int index);

}
