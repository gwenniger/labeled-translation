package grammarExtraction.translationRuleTrie;

import java.io.Serializable;
import java.util.concurrent.locks.ReentrantLock;

import util.Locking;

public abstract class WordKeyMappingTabbleItemHandler implements Serializable {
	private static final long serialVersionUID = 1L;
	protected final WordKeyMappingTableData wordKeyMappingTableData;
	private final int baseIndex;
	private int writeIndex;
	private int noItemsForType = 0;
	private final int maxNumLabelTypes;

	// The minimum wait time for a lock to report a warning
	private static final long MinWaitTimeLockWarning = 500;
	private static final boolean UseFairLockingStrategy = true;
	private final ReentrantLock lock = new ReentrantLock(UseFairLockingStrategy);

	protected WordKeyMappingTabbleItemHandler(WordKeyMappingTableData wordKeyMappingTableData, int writeIndex, int maxNumLabelTypes) {
		this.wordKeyMappingTableData = wordKeyMappingTableData;
		this.writeIndex = writeIndex;
		this.baseIndex = writeIndex;
		this.maxNumLabelTypes = maxNumLabelTypes;
	}

	/**
	 * @param string
	 * @return
	 */
	public int addEntryForString(String string) {
		acquireLockWitmMaxWaitTimeWarning();

		try {
			boolean wasAbsent = (this.wordKeyMappingTableData.getWordToKeyMappingTable().putIfAbsent(string, writeIndex) == null);
			if (wasAbsent) {
				if (writeIndexOutOfRange()) {
					throw new RuntimeException("Error : WorkKeyMappingTableItemHandler - trying to add more items than is "
							+ "allowed by the capacity of the table, as specified by MaxNumLabelTypes" + "Number of items allowed: " + this.maxNumLabelTypes
							+ " number of items present: " + this.noItemsForType + "\nItem tried to add: " + string + "\nItem handler name: "
							+ getItemHandlerName());
				}
				int result = writeIndex;
				this.wordKeyMappingTableData.getKeyToWordMappingTable().put(writeIndex, string);
				//System.out.println("WordKeyMappingTabbleItemHandler -  addEntryForString - added pair " + writeIndex  + "|||" + string);
				writeIndex++;
				noItemsForType++;
				return result;
			} else {
				/**
				 * * Remark that this method can still be entered multiple times
				 * for the same argument, despite being synchronized This means
				 * that we must ensure that we return the previously added entry
				 * when this occurs. The else case takes care of this.
				 */
				return this.wordKeyMappingTableData.getWordToKeyMappingTable().get(string);
			}
		} finally {
			releaseLock();
		}
	}

	private boolean writeIndexOutOfRange() {
		boolean result =  (this.writeIndex >= (this.baseIndex + this.maxNumLabelTypes));
		
		if(result == true)
		{
			System.out.println("WordKeyMappingTabbleItemHandler.writIndexOutOfRange() returned true");
			System.out.println("this.writeIndex: " + this.writeIndex + " this.maxNumLabelTypes: " + this.maxNumLabelTypes);
		}
		
		return result;
	}

	protected abstract Integer getFullyDowngradedKeyRepresentation(Integer key);
	protected abstract Integer getSourceSideLabelsOnlyDowngradedKeyRepresentation(Integer key);
	protected abstract Integer getTargetSideLabelsOnlyDowngradedKeyRepresentation(Integer key);
	protected abstract  String  getLeftHandSideLabelForAnyLabelIndex(Integer key);
	
	protected abstract String getItemHandlerName();

	public int getNumberIntemsForType() {
		acquireLockWitmMaxWaitTimeWarning();
		try {
			return this.noItemsForType;
		} finally {
			releaseLock();
		}

	}

	protected int getBaseIndex() {
		return this.baseIndex;
	}

	protected void acquireLockWitmMaxWaitTimeWarning() {
		Locking.acquireLockWitmMaxWaitTimeWarning(lock, MinWaitTimeLockWarning, this, "WordKeyMappingTabbleItemHandler");
	}

	protected void releaseLock() {
		lock.unlock();
	}
}
