package grammarExtraction.translationRuleTrie;

import grammarExtraction.labelSmoothing.RuleLabelsSmoother;
import grammarExtraction.samtGrammarExtraction.GapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator;
import util.InputReading;
import alignment.Alignment;

public class TranslationRuleCountTable {
	private TranslationRuleTrie translationRuleTrie;
	private final LexicalProbabilityTables lexicalProbabilityTables;
	private final WordKeyMappingTable wordKeyMappingTable;
	private final LabelProbabilityEstimator labelProbabilityEstimator;
	private final RuleLabelsSmoother ruleLabelsSmoother;
	private final GapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator gapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator;

	private TranslationRuleCountTable(TranslationRuleTrie translationRuleTrie, LexicalProbabilityTables lexicalProbabilityTables, WordKeyMappingTable wordKeyMappingTable,
			LabelProbabilityEstimator labelProbabilityEstimator, RuleLabelsSmoother ruleLabelsSmoother,GapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator gapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator) {
		this.translationRuleTrie = translationRuleTrie;
		this.lexicalProbabilityTables = lexicalProbabilityTables;
		this.wordKeyMappingTable = wordKeyMappingTable;
		this.labelProbabilityEstimator = labelProbabilityEstimator;
		this.ruleLabelsSmoother = ruleLabelsSmoother;
		this.gapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator = gapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator;
	}

	public static TranslationRuleCountTable createTranslationRuleCountTable(TranslationRuleTrie translationRuleTrie, LexicalProbabilityTables lexicalProbabilityTables,
			WordKeyMappingTable wordKeyMappingTable, LabelProbabilityEstimator labelProbabilityEstimator, RuleLabelsSmoother ruleLabelsSmoother) {
		return new TranslationRuleCountTable(translationRuleTrie, lexicalProbabilityTables, wordKeyMappingTable, labelProbabilityEstimator, ruleLabelsSmoother,GapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator.createGapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator());
	}

	public TranslationRuleTrie getTranslationRuleTrie() {
		return this.translationRuleTrie;
	}

	public WordKeyMappingTable getWordKeyMappingTable() {
		return this.wordKeyMappingTable;
	}

	public LexicalProbabilityTables getLexicalProbabilityTables() {
		return this.lexicalProbabilityTables;
	}

	public TargetGivenSourceProbabilityTrie createTargetGivenSourceProbabilityTrie() {
		return TargetGivenSourceProbabilityTrie.createTargetGivenSourceProbabilityTrie(translationRuleTrie, wordKeyMappingTable);
	}

	public SourceGivenTargetProbabilityTrie createSourceGivenTargetProbabilityTrie() {
		return SourceGivenTargetProbabilityTrie.createSourceGivenTargetProbabilityTrie(translationRuleTrie, wordKeyMappingTable);
	}

	public int getNumNodesRecursive() {
		return this.translationRuleTrie.getSourceTrie().getNumNodesRecursive();
	}

	public int getTotalNumRulesInBothDirections() {
		return this.translationRuleTrie.getTotalNumSourceToTargetRules() + this.translationRuleTrie.getTotalNumTargetToSourceRules();
	}

	public int getTotalNumSourceToTargetRules() {
		return this.translationRuleTrie.getTotalNumSourceToTargetRules();
	}

	public int getTotalNumTargetToSourceRules() {
		return this.translationRuleTrie.getTotalNumTargetToSourceRules();
	}

	public int getLexicalCountSourceToTarget(String sourceWord, String targetWord) {
		return this.lexicalProbabilityTables.getLexicalProbabilityTableSourceGivenTarget().getCount(sourceWord, targetWord);
	}

	public int getLexicalCountTargetToSource(String targetWord, String sourceWord) {
		return this.lexicalProbabilityTables.getLexicalProbabilityTableTargetGivenSource().getCount(targetWord, sourceWord);
	}

	public double getLexicalProbabilitySourceToTarget(String sourceWord, String targetWord) {
		return this.lexicalProbabilityTables.getLexicalProbabilityTableSourceGivenTarget().getProbability(sourceWord, targetWord);
	}

	public double getLexicalProbabilityTargetToSource(String targetWord, String sourceWord) {
		return this.lexicalProbabilityTables.getLexicalProbabilityTableTargetGivenSource().getProbability(targetWord, sourceWord);
	}

	public double getLexicalWeightSourceToTarget(String sourceString, String targetString, String alignmentString) {
		return this.lexicalProbabilityTables.getLexicalProbabilityTableSourceGivenTarget().computeAlignedPhrasePairProbability(InputReading.getWordsFromString(sourceString),
				InputReading.getWordsFromString(targetString), Alignment.createAlignment(alignmentString));
	}

	public double getLexicalWeightTargetToSource(String sourceString, String targetString, String alignmentString) {
		return this.lexicalProbabilityTables.getLexicalProbabilityTableTargetGivenSource().computeAlignedPhrasePairProbability(InputReading.getWordsFromString(sourceString),
				InputReading.getWordsFromString(targetString), Alignment.createAlignment(alignmentString));
	}

	public void printSourceTrie() {
		this.translationRuleTrie.printSourceRuleCountTables(wordKeyMappingTable);
	}

	public double getRuleConditionalPhraseProbabilitySourceToTarget(String sourceSideRuleString, String targetSideRuleString) {
		return this.translationRuleTrie.getSourceTrie().getRuleConditionalProbability(sourceSideRuleString, targetSideRuleString, this.translationRuleTrie.getTargetTrie(), wordKeyMappingTable);
	}

	public void printExtractedRulesStatistics(double totalCountCreatedBySourceToTargetThreads, double totalCountCreatedByTargetToSourceThreads) {
		double sourceToTargetFilteredCounts = getTotalFilteredSourceToTargetCounts();
		double targetToSourceFilteredCounts = getTotalFilteredTargetToSourceCounts();

		System.out.println("Total extracted rule types in both directions: " + getTotalNumRulesInBothDirections());
		System.out.println("Total extracted rule types in source to target direction: " + getTotalNumSourceToTargetRules());
		System.out.println("Total extracted rule types in target to source direction: " + getTotalNumTargetToSourceRules());
		System.out.println("\n\n");
		System.out.println("Num trie nodes computed recursively: " + this.getNumNodesRecursive());
		System.out.println("Total extracted rule counts source-target (in table): " + getTotalExtractedSourceToTargetCounts());
		System.out.println("Total extracted rule counts source-target (in table): " + getTotalExtractedTargetToSourceCounts());
		System.out.println("Total rule counts extracted by source to target threads: " + totalCountCreatedBySourceToTargetThreads);
		System.out.println("Total rule counts filtered by source to target threads: " + sourceToTargetFilteredCounts);
		System.out.println("Total rule counts extracted by target to source threads: " + totalCountCreatedByTargetToSourceThreads);
		System.out.println("Total rule counts filtered by target to  source threads: " + targetToSourceFilteredCounts);
	}

	private double getTotalExtractedSourceToTargetCounts() {
		return this.translationRuleTrie.getTotalSourceToTargetRuleCounts();
	}

	private double getTotalExtractedTargetToSourceCounts() {
		return this.translationRuleTrie.getTotalTargetToSourceRuleCounts();
	}

	private double getTotalFilteredSourceToTargetCounts() {
		return this.translationRuleTrie.getTotalSourceToTargetFilteredCounts();
	}

	private double getTotalFilteredTargetToSourceCounts() {
		return this.translationRuleTrie.getTotalTargetToSourceFilteredCounts();
	}

	public boolean testLexicalTables() {
		return this.lexicalProbabilityTables.testTables();
	}

	public LabelProbabilityEstimator getLabelProbabilityEstimator() {
		return this.labelProbabilityEstimator;
	}
	
	public  GapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator getGapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator()
	{
	    return this.gapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator;
	}
	

	public RuleLabelsSmoother getRuleLabelsSmoother() {
		return this.ruleLabelsSmoother;
	}

}
