package grammarExtraction.translationRuleTrie;

import grammarExtraction.IntegerKeyRuleRepresentation;

import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class HashCollisionCounter<T> 
{
	ConcurrentHashMap<Integer, HashSet<T>> collisionTable;
	AtomicInteger totalCollisions;
	AtomicInteger maxCollisions;
	
	private HashCollisionCounter(ConcurrentHashMap<Integer, HashSet<T>> collisionTable, AtomicInteger totalCollisions, AtomicInteger maxCollisions)
	{
		this.collisionTable = collisionTable;
		this.totalCollisions = totalCollisions;
		this.maxCollisions = maxCollisions;
	
	}
	
	public static HashCollisionCounter<IntegerKeyRuleRepresentation> createRuleSideKeyCollisionCounter()
	{
		return new HashCollisionCounter<IntegerKeyRuleRepresentation>(new ConcurrentHashMap<Integer, HashSet<IntegerKeyRuleRepresentation>>(), new AtomicInteger(0), new AtomicInteger(0));
	}
	
	public void addObject(T object)
	{
		int hashCode = object.hashCode();
		
		collisionTable.putIfAbsent(hashCode,new HashSet<T>());
		   
		 
	 	HashSet<T> newSet, oldSet;
	    do 
	    {
	      oldSet = collisionTable.get(hashCode);
	      newSet = new HashSet<T>(oldSet);
	      newSet.add(object);
	      
	    } while (!collisionTable.replace(hashCode, oldSet, newSet));	

	     
	    if((newSet.size() >  1) && (newSet.size() > oldSet.size()))
	    {
	    	this.totalCollisions.incrementAndGet();
	    	int numCollisionsHashCode = newSet.size();
	    	
	    	if(numCollisionsHashCode > this.maxCollisions.get())
	    	{	
	    		this.maxCollisions.getAndSet(numCollisionsHashCode);
	    	}	
	    }
	}

	public String getCollisionsReport()
	{
		String result = "Total collisions:" + this.totalCollisions + " Max Collisions " + this.maxCollisions;
		return result;
		
	}
	
}
