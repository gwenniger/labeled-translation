package grammarExtraction.translationRuleTrie;


public class TargetGivenSourceProbabilityTrie extends ConditionalProbabilityTrie
{
	private static final long serialVersionUID = 1L;

	protected TargetGivenSourceProbabilityTrie(
			MTRuleTrieNodeRoot conditionalProbabilityTrie,
			WordKeyMappingTable wordKeyMappingTable,
			MTRuleTrieNodeRoot conditionedElementsRepresentationTrie) 
	{
		super(conditionalProbabilityTrie, wordKeyMappingTable,conditionedElementsRepresentationTrie);
	}

	public static TargetGivenSourceProbabilityTrie createTargetGivenSourceProbabilityTrie(TranslationRuleTrie translationRuleTrie,
			WordKeyMappingTable wordKeyMappingTable)
	{
		TargetGivenSourceProbabilityTrie result = new TargetGivenSourceProbabilityTrie(translationRuleTrie.getSourceTrie(), wordKeyMappingTable, translationRuleTrie.getTargetTrie());
		result.computeTotalCounts();
		return result;
	}
	
}
