package grammarExtraction.translationRuleTrie;

import grammarExtraction.IntegerKeyRuleRepresentationWithLabel;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

import org.junit.Assert;

public class BasicRuleFeaturesProperties {
	protected final util.Pair<Double> lexicalProbailities;
	protected final double ruleCorpusCount;

	private BasicRuleFeaturesProperties(util.Pair<Double> lexicalProbailities, double ruleCorpusCount) {
		this.lexicalProbailities = lexicalProbailities;
		this.ruleCorpusCount = ruleCorpusCount;
	}

	public static BasicRuleFeaturesProperties createBasicRuleFeaturesProperties(TranslationRuleSignatureTriple translationRuleSignatureTriple, TranslationRuleProbabilityTable ruleProbabilityTable) {
		IntegerKeyRuleRepresentationWithLabel sourceSide = translationRuleSignatureTriple.getSourceSideRepresentationWithLabel();
		IntegerKeyRuleRepresentationWithLabel targetSide = translationRuleSignatureTriple.getTargetSideRepresentationWithLabel();
		util.Pair<Double> lexicalProbailities = ruleProbabilityTable.targetGivenSourceProbabilityTrie.getRuleLexicalProbabilities(sourceSide, targetSide);
		Assert.assertTrue(lexicalProbailities.getFirst() >= 0);
		Assert.assertTrue(lexicalProbailities.getSecond() >= 0);
		Assert.assertTrue(lexicalProbailities.getFirst() <= 1);
		Assert.assertTrue(lexicalProbailities.getSecond() <= 1);
		double ruleCorpusCount = ruleProbabilityTable.getRuleCount(sourceSide, targetSide);
		// System.out.println("BasicRuleFeaturesProperties - ruleCorpusCount" + ruleCorpusCount);
		return new BasicRuleFeaturesProperties(lexicalProbailities, ruleCorpusCount);
	}
}
