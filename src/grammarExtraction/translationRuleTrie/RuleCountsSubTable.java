package grammarExtraction.translationRuleTrie;

import grammarExtraction.IntegerKeyRuleRepresentation;
import grammarExtraction.IntegerKeyRuleRepresentationWithLabel;
import grammarExtraction.translationRuleTrie.CountItem.LabeledRuleWithCount;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import junit.framework.Assert;
import util.Locking;
import util.ObjectFactory;
import util.Pair;

public abstract class RuleCountsSubTable implements ObjectFactory, Serializable {

	private static final long serialVersionUID = 1L;

	private final List<CountItem> countItems;
	protected double totalCount = -1;
	protected double totalAnonymousRulesCount;

	private static final CountItemComparator countItemComparator = new CountItemComparator();

	// The minimum wait time for a lock to report a warning
	private static final long MinWaitTimeLockWarning = 500;
	private static final boolean UseFairLockingStrategy = true;
	private final ReentrantLock lock = new ReentrantLock(UseFairLockingStrategy);

	private RuleCountsSubTable(List<CountItem> countItems, double totalCount, double totalAnonymousRulesCount) {
		this.countItems = countItems;
		this.totalCount = totalCount;
		this.totalAnonymousRulesCount = totalAnonymousRulesCount;

	}

	public static RuleCountsSubTableLabeledSourceSide createRuleCountsSubTableLabeledSourceSide() {
		List<CountItem> countItems = new ArrayList<CountItem>(1);
		return new RuleCountsSubTableLabeledSourceSide(countItems, 0, 0);
	}
	
	public static RuleCountsSubTableUnlabeledSourceSide createRuleCountsSubTableUnlabeledSourceSide() {
		List<CountItem> countItems = new ArrayList<CountItem>(1);
		return new RuleCountsSubTableUnlabeledSourceSide(countItems, 0, 0);
	}
	
	/*
	 * public CountItem getCountItemLinearSearch(MTRuleTrieNode targetRuleLeafNode) { for (CountItem countItem : this.getCountItems()) { //
	 * System.out.println("RuleCountsTable.getCountItemLinearSearch.countItem: " // + countItem); if (countItem.getLabel().equals(targetRuleLeafNode)) { return
	 * countItem; } } return null; }
	 */

	public int getCountItemIndexBinarySearch(MTRuleTrieNode targetRuleLeafNode) {
		int index = Collections.binarySearch(this.getCountItems(), CountItem.createSearchNode(targetRuleLeafNode), countItemComparator);
		return index;
	}

	protected CountItem getCountItemForTargetRuleLeafNode(MTRuleTrieNode targetRuleLeafNode) {

		CountItem result;
		int index = getCountItemIndexBinarySearch(targetRuleLeafNode);

		if (index >= 0) {
			result = this.getCountItems().get(index);

			if (result.getLabel().equals(targetRuleLeafNode)) {
				return result;
			} else {
				// The list of children has been changed by another thread
				// in the mean time, so
				// we must perform the whole search again to get the right
				// result
				String id = Integer.toHexString(System.identityHashCode(this));
				System.out.println("Re-iterating getCountItemForTargetRuleLeafNode at RuleCountsTable with id: " + id + " ...");
				return getCountItemForTargetRuleLeafNode(targetRuleLeafNode);
			}
		}
		return null;
	}

	public abstract CountItem createCountItem(CountItemCreater countItemCreater, MTRuleTrieNode label);
	
	// We synchronize this method rather than its parent method in order to
	// reduce lock contention
	private CountItem getOraddEntryIfAbsentSyncrhonized(MTRuleTrieNode targetRuleLeafNode, CountItemCreater countItemCreater) {
		acquireLockWitmMaxWaitTimeWarning();
		try {
			CountItem result;
			int index = getCountItemIndexBinarySearch(targetRuleLeafNode);

			if (index < 0) {
				index = ~index;
				result = createCountItem(countItemCreater, targetRuleLeafNode);
				assert (result != null);
				this.getCountItems().add(index, result);
			} else {
				result = this.getCountItems().get(index);
			}
			return result;
		} finally {
			releaseLock();
		}
	}

	private MTRuleTrieNode getTargetLeafNodeAndInsertIfNotPresent(IntegerKeyRuleRepresentation targetRuleRepresentation, MTRuleTrieNode targetTrie) {
		return (MTRuleTrieNode) targetTrie.addOrRetrieveRuleRepresentation(targetRuleRepresentation);
	}

	protected MTRuleTrieNode getTargetLeafNodeAndReturnNullIfNotPresent(IntegerKeyRuleRepresentation targetRuleRepresentation, MTRuleTrieNode targetTrie) {
		return (MTRuleTrieNode) targetTrie.retrieveRuleRepresentation(targetRuleRepresentation);
	}

	private void performCountUpdateSynchronized(CountItem countItem, CountUpdateItem countUpdateItem) {
		acquireLockWitmMaxWaitTimeWarning();
		try {
			countItem.performCountUpdate(countUpdateItem);
		} finally {
			releaseLock();
		}
	}

	public void updateCountSynchronized(MTRuleTrieNode targetRuleLeafNode, CountUpdateItem countUpdateItem, CountItemCreater countItemCreater) {
		CountItem countItem = getCountItemForTargetRuleLeafNode(targetRuleLeafNode);

		if (countItem == null) {
			countItem = getOraddEntryIfAbsentSyncrhonized(targetRuleLeafNode, countItemCreater);
		}
		performCountUpdateSynchronized(countItem, countUpdateItem);
	}

	void addCount(IntegerKeyRuleRepresentation targetRuleRepresentation, CountUpdateItem countUpdateItem, MTRuleTrieNode targetTrie, CountItemCreater countItemCreater) {
		MTRuleTrieNode targetRuleLeafNode = getTargetLeafNodeAndInsertIfNotPresent(targetRuleRepresentation, targetTrie);
		updateCountSynchronized(targetRuleLeafNode, countUpdateItem, countItemCreater);
	}

	protected void increaseTotalAnononymousRulesCount(CountUpdateItem countUpdateItem) {
		acquireLockWitmMaxWaitTimeWarning();
		try {
			this.totalAnonymousRulesCount += countUpdateItem.getCount();
		} finally {
			releaseLock();
		}
	}

	void addCountWithoutIntroducingNewConditionedSide(IntegerKeyRuleRepresentation targetRuleRepresentation, CountUpdateItem countUpdateItem, MTRuleTrieNode targetTrie,
			CountItemCreater countItemCreater) {
		MTRuleTrieNode targetRuleLeafNode = getTargetLeafNodeAndReturnNullIfNotPresent(targetRuleRepresentation, targetTrie);
		if (targetRuleLeafNode == null) {
			increaseTotalAnononymousRulesCount(countUpdateItem);
		} else {
			updateCountSynchronized(targetRuleLeafNode, countUpdateItem, countItemCreater);
		}
	}

	private double computeTotalCount() {
		double result = 0;
		for (CountItem countItem : countItems) {
			result += countItem.getCount();
		}
		result += this.totalAnonymousRulesCount;
		return result;
	}

	public int getNumElements() {
		return this.countItems.size();
	}

	public double getCount(IntegerKeyRuleRepresentation targetRuleRepresentation, MTRuleTrieNode targetTrie) {
		return getCount(getTargetLeafNodeAndInsertIfNotPresent(targetRuleRepresentation, targetTrie));
	}

	private double getCount(MTRuleTrieNode targetLeafRepresentation) {
		CountItem countItem = this.getCountItemForTargetRuleLeafNode(targetLeafRepresentation);

		if (countItem != null) {
			return countItem.getCount();
		}

		throw new RuntimeException("Requested a count for a targetLeafRepresentation that is not present in RuleCountsTable");
	}

	public void addLabeledRule(IntegerKeyRuleRepresentationWithLabel unlabeledTargetRuleRepresentation,MTRuleTrieNode targetTrie, LabeledRuleWithCount labeledRuleWithCount) {
		MTRuleTrieNode targetRuleLeafNode = this.getTargetLeafNodeAndReturnNullIfNotPresent(unlabeledTargetRuleRepresentation, targetTrie);
		Assert.assertNotNull(targetRuleLeafNode);
		CountItem countItem = getCountItemForTargetRuleLeafNode(targetRuleLeafNode); 

		if (countItem != null) {
			countItem.addLabeledRule(labeledRuleWithCount);
		}
		else
		{	
			throw new RuntimeException("Executed RuleCountsSubTable.addLabeledRule for non-existing targete sdie " + labeledRuleWithCount.getRuleTargetSideCountItem().getLabel());
		}	
	}
	

	public double computeSaveAndGetTotalCounts() {
		this.totalCount = computeTotalCount();
		return totalCount;
	}

	public double getProbability(IntegerKeyRuleRepresentation targetRuleRepresentation, MTRuleTrieNode targetTrie) {
		if (this.totalCount <= 0) {
			throw new RuntimeException("The total count has not been computed are not greater than 0");
		}

		double count = this.getCount(targetRuleRepresentation, targetTrie);

		if (count >= 0) {
			return count / totalCount;
		}
		return -1;
	}

	public Pair<Double> getRuleLexicalProbabilities(IntegerKeyRuleRepresentation targetRuleRepresentation, MTRuleTrieNode targetTrie) {
		CountItem countItem = this.getCountItemForTargetRuleLeafNode(this.getTargetLeafNodeAndInsertIfNotPresent(targetRuleRepresentation, targetTrie));
		if (countItem instanceof LexicalWeightCountItem) {
			return new Pair<Double>(((LexicalWeightCountItem) countItem).getLexicalWeightTargetGivenSource(), ((LexicalWeightCountItem) countItem).getLexicalWeightSourceGivenTarget());
		}
		throw new RuntimeException("RuleCountsTable: Exception - try to get lexical weight for  IntegerKeyRuleRepresentation without associated LexicalWeightCountItem");
	}

	protected List<Double> getProbabilities() {
		if (totalCount < 0) {
			throw new RuntimeException("The total count has not been computed ");
		}
		List<Double> result = new ArrayList<Double>();
		for (CountItem countItem : countItems) {
			double probability = countItem.getCount() / totalCount;
			result.add(probability);
		}
		return result;
	}

	public double getTotalCount() {
		return this.totalCount;
	}

	private static double computeEntropy(Collection<Double> probabilities) {
		double result = 0;
		for (double probability : probabilities) {
			double term = probability * Math.log(probability);
			// System.out.println("probability: " + probability +
			// " Entropy term: " + term);
			result += term;
		}

		result = -result;

		// System.out.println("Entropy: " + result);
		return result;
	}

	public double getEntropy() {
		if (this.totalAnonymousRulesCount != 0) {
			throw new RuntimeException("Error trying to get entropy in a rule Counts table with non zero counts of anonymous rules");
		}
		return computeEntropy(getProbabilities());
	}

	public String getCountItemsString() {
		String result = "";

		for (CountItem countItem : this.getCountItems()) {
			result += countItem.toString() + "\n";
		}
		return result;
	}

	public List<CountItem> getCountItems() {
		return countItems;
	}

	public RuleCountsTableConditionedSidesEnumeration getRuleCountsTableConditionedSidesEnumeration() {
		return RuleCountsTableConditionedSidesEnumeration.createRuleCountsTableConditionedSidesEnumeration(countItems);
	}

	public String toString() {
		String result = "<RuleCountsTable>\n";
		result += "Number of Count Items: " + this.countItems.size();
		for (CountItem countItem : this.countItems) {
			System.out.println("CountItem" + countItem);
			// result += countItem.toString();
		}
		result += "\n</RuleCountsTable>";
		return result;
	}

	protected void acquireLockWitmMaxWaitTimeWarning() {
		Locking.acquireLockWitmMaxWaitTimeWarning(lock, MinWaitTimeLockWarning, this, "RuleCountsTable");
	}

	protected void releaseLock() {
		lock.unlock();
	}

	public static class RuleCountsSubTableUnlabeledSourceSide extends RuleCountsSubTable{

		private static final long serialVersionUID = 1L;

		private RuleCountsSubTableUnlabeledSourceSide(List<CountItem> countItems, 
				double totalCount, double totalAnonymousRulesCount) {
			super(countItems, totalCount, totalAnonymousRulesCount);
		}

		@Override
		public CountItem createCountItem(CountItemCreater countItemCreater,
				MTRuleTrieNode label) {
			return countItemCreater.createCountItemUnlabeledRuleSourceSide(label);
		}
		
		@Override
		public Object makeObject() {
			List<CountItem> countItems = new ArrayList<CountItem>(1);
			countItems.add(new BasicCountItem(null));
			return new RuleCountsSubTableUnlabeledSourceSide(countItems, 0, 0);
		}
	}
	
	
	
	public static class RuleCountsSubTableLabeledSourceSide extends RuleCountsSubTable{

		private static final long serialVersionUID = 1L;

		private RuleCountsSubTableLabeledSourceSide(List<CountItem> countItems, 
				double totalCount, double totalAnonymousRulesCount) {
			super(countItems, totalCount, totalAnonymousRulesCount);
		}

		@Override
		public CountItem createCountItem(CountItemCreater countItemCreater,
				MTRuleTrieNode label) {
			return countItemCreater.createCountItemLabeledRuleSourceSide(label);
		}
		
		@Override
		public Object makeObject() {
			List<CountItem> countItems = new ArrayList<CountItem>(1);
			countItems.add(new BasicCountItem(null));
			return new RuleCountsSubTableLabeledSourceSide(countItems, 0, 0);
		}
	}
	
}
