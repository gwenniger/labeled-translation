package grammarExtraction.translationRuleTrie;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import junit.framework.Assert;
import grammarExtraction.IntegerKeyRuleRepresentationWithLabel;
import grammarExtraction.translationRules.LightWeightRuleLabel;

/**
 * This Class stores a specific labeling occurring for a rule or set of rules.
 * It can be used for example to find the most frequent set of labeling patterns
 * given a certain source side.
 * 
 * @author Gideon Maillette de Buy Wenniger
 * 
 */
public class RuleLabeling implements Comparable<RuleLabeling> {
    /**
     * We use raw Integer arrays to store the indices, to decrease memory
     * consumption, at the price of more overhead in converting to List<Integer>
     * for the querying methods etc. Since there are many RuleLabeling instances
     * this seems still a reasonable tradeoff.
     */
    private final int[] sourceLabelIndices;
    private final int[] targetLabelIndices;
    // The labelingCount keeps track of how often this labeling has been
    // observed in the data
    private double labelingCount;

    private RuleLabeling(int[] sourceLabelIndices, int[] targetLabelIndices, double labelingCount) {
	this.sourceLabelIndices = sourceLabelIndices;
	this.targetLabelIndices = targetLabelIndices;
	Assert.assertTrue(this.sourceLabelIndices.length <= 3);
	Assert.assertTrue(this.targetLabelIndices.length <= 3);
	this.labelingCount = labelingCount;
    }

    private static int[] getLabelIndicesForRuleSideRepresentation(
	    IntegerKeyRuleRepresentationWithLabel ruleSideRepresentation,
	    WordKeyMappingTable wordKeyMappingTable) {
	List<Integer> allLabels = ruleSideRepresentation
		.getAllLabelsLeftToRight(wordKeyMappingTable);
	int[] result = new int[allLabels.size()];
	for (int i = 0; i < allLabels.size(); i++) {
	    result[i] = allLabels.get(i);
	}
	return result;
    }

    /**
     * Factory method that takes a labeled source (rule) side and labeled target
     * (rule) side and extracts lists of integers with the label indices for
     * these rule sides and returns the RuleLabeling for those
     * 
     * @param labeledSourceRepresentation
     * @param labeledTargetRepresentation
     * @param wordKeyMappingTable
     * @return
     */
    public static RuleLabeling createRuleLabeling(WordKeyMappingTable wordKeyMappingTable,
	    IntegerKeyRuleRepresentationWithLabel labeledSourceRepresentation,
	    IntegerKeyRuleRepresentationWithLabel labeledTargetRepresentation,
	    double labelingFrequency) {
	int[] sourceLabelIndices = getLabelIndicesForRuleSideRepresentation(
		labeledSourceRepresentation, wordKeyMappingTable);
	int[] targetLabelIndices = getLabelIndicesForRuleSideRepresentation(
		labeledTargetRepresentation, wordKeyMappingTable);
	return new RuleLabeling(sourceLabelIndices, targetLabelIndices, labelingFrequency);
    }

    public static RuleLabelingFrequencyComparator createRuleLabelingFrequencyComparator() {
	return new RuleLabelingFrequencyComparator();
    }

    private static List<Integer> getIntegerListForIntegerArray(int[] intArray) {
	List<Integer> result = new ArrayList<Integer>();
	for (Integer integer : intArray) {
	    result.add(integer);
	}
	return result;
    }

    public List<Integer> getSourceLabelIndices() {
	return getIntegerListForIntegerArray(sourceLabelIndices);
    }

    public List<Integer> getTargetLabelIndices() {
	return getIntegerListForIntegerArray(targetLabelIndices);
    }

    public LightWeightRuleLabel getRuleLabel() {
	return LightWeightRuleLabel.createLightWeightRuleLabel(sourceLabelIndices[0],
		targetLabelIndices[0]);
    }

    public List<Integer> getSourceGapLabelIndicesOrderedByGapNumber() {
	List<Integer> sourceIndices = getSourceLabelIndices();
	return sourceIndices.subList(1, sourceIndices.size());
    }

    public List<Integer> getTargetGapLabelIndicesOrderedByGapNumber(
	    WordKeyMappingTable wordKeyMappingTable) {
	List<Integer> targetIndices = getTargetLabelIndices();
	List<Integer> result = new ArrayList<Integer>();
	for (int i = 1; i < targetIndices.size(); i++) {
	    result.add(0);
	}
	for (int i = 1; i < targetIndices.size(); i++) {
	    int gapIndex = targetIndices.get(i);
	    if (wordKeyMappingTable.isLabelOneKey(gapIndex)) {
		result.set(0, gapIndex);
	    }
	    if (wordKeyMappingTable.isLabelTwoKey(gapIndex)) {
		result.set(1, gapIndex);
	    }
	}
	return result;
    }

    public static int compareInts(int int1, int int2) {
	return (new Integer(int1)).compareTo(new Integer(int2));
    }

    public static int compareDoubles(double double1, double double2) {
	return (new Double(double2)).compareTo(new Double(double2));
    }

    @Override
    public int compareTo(RuleLabeling ruleLabeling2) {

	/**
	 * If the lists of source label indices are not of equal length, we
	 * compare based on those lengths
	 */
	if (this.sourceLabelIndices.length != ruleLabeling2.sourceLabelIndices.length) {
	    return compareInts(this.sourceLabelIndices.length,
		    ruleLabeling2.sourceLabelIndices.length);
	}

	/**
	 * Otherwise, if the lists of target label indices are not of equal
	 * length, we compare based on those lengths
	 */
	else if (this.targetLabelIndices.length != ruleLabeling2.targetLabelIndices.length) {
	    return compareInts(this.targetLabelIndices.length,
		    ruleLabeling2.targetLabelIndices.length);
	}

	/**
	 * The lists of source and target label indices are of same length, we
	 * compare based on the first element that is different, starting from
	 * the source side labels
	 */
	else {
	    for (int i = 0; i < this.sourceLabelIndices.length; i++) {
		if (this.sourceLabelIndices[i] != ruleLabeling2.sourceLabelIndices[i]) {
		    return compareInts(this.sourceLabelIndices[i],
			    ruleLabeling2.sourceLabelIndices[i]);
		}
	    }
	    for (int i = 0; i < this.targetLabelIndices.length; i++) {
		if (this.targetLabelIndices[i] != ruleLabeling2.targetLabelIndices[i]) {
		    return compareInts(this.targetLabelIndices[i],
			    ruleLabeling2.targetLabelIndices[i]);
		}
	    }
	    /**
	     * All labels are the same so the two RuleLabeling objects are the
	     * same
	     */
	    return 0;
	}

    }

    public double getLabelingCount() {
	return labelingCount;
    }

    /**
     * 
     * @param labelingFrequency
     */
    public void addLabelingCount(double labelingCount) {
	this.labelingCount += labelingCount;
    }

    /**
     * To implement the hashCode function we use the ArrayList hashcode for the
     * list we get by concatenating the source- and target indices lists
     */
    public int hashCode() {
	List<Integer> allIndicesList = new ArrayList<Integer>();
	allIndicesList.addAll(getSourceLabelIndices());
	allIndicesList.addAll(getTargetLabelIndices());
	return allIndicesList.hashCode();
    }

    public boolean equals(Object ruleLabeling2) {
	if (ruleLabeling2 instanceof RuleLabeling) {
	    return (this.compareTo((RuleLabeling) ruleLabeling2) == 0);
	}
	return false;
    }

    public static class RuleLabelingFrequencyComparator implements Comparator<RuleLabeling> {

	@Override
	public int compare(RuleLabeling ruleLabeling1, RuleLabeling ruleLabeling2) {
	    return compareDoubles(ruleLabeling1.getLabelingCount(),
		    ruleLabeling2.getLabelingCount());
	}
    }
    
    public String toString()
    {
	String result = "<RuleLabeling>";
	result += getSourceLabelIndices() + " " + getTargetLabelIndices() + "</RuleLabeling>";
	return result;
    }

}
