package grammarExtraction.translationRuleTrie;


import grammarExtraction.grammarExtractionFoundation.ConcurrencySettings;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import util.Pair;
import util.Utility;

import alignment.Alignment;
import alignment.AlignmentPair;
import alignment.AlignmentTriple;


public class ConcurrentLexicalProbabilityTable extends ConcurrentConditionalProbabilityTable<Integer> implements Serializable
{
	private static final long serialVersionUID = 1L;
	private final WordKeyMappingTable wordKeyMappingTable;
	
	
	private ConcurrentLexicalProbabilityTable(
			ConcurrentMap<Integer, ConcurrentMap<Integer, ProbabilityTableItem>> probabilityTable, WordKeyMappingTable wordKeyMappingTable) 
	{
		super(probabilityTable);
		this.wordKeyMappingTable = wordKeyMappingTable;
	}

	public static ConcurrentLexicalProbabilityTable createConcurrentLexicalProbabilityTable(WordKeyMappingTable wordKeyMappingTable, int noConcurrentThreads)
	{
		ConcurrentMap<Integer,ConcurrentMap<Integer,ProbabilityTableItem>> probabilityTable = new ConcurrentHashMap<Integer, ConcurrentMap<Integer,ProbabilityTableItem>>(ConcurrencySettings.ConcurrentHashMapInitialCapacity,ConcurrencySettings.ConcurrentHashMapLoadFactor,ConcurrencySettings.getConcurrencyLevel(noConcurrentThreads));
		return new ConcurrentLexicalProbabilityTable(probabilityTable,wordKeyMappingTable);
	}
	
	public void addAlignmentTriples(List<AlignmentTriple> alignmentTriples)
	{
		for(AlignmentTriple alignmentTriple : alignmentTriples)
		{
			addCountsForAlignmentTriple(alignmentTriple);
		}
	}
	
	private Pair<Integer> getSourceTargetKeyPairForWordPair(String sourceWord, String targetWord)
	{

		
		Integer sourceKey = this.wordKeyMappingTable.getKeyForWord(sourceWord);
		Integer targetKey = this.wordKeyMappingTable.getKeyForWord(targetWord);
		
			
		return new Pair<Integer>(sourceKey,targetKey);
	}
	
	
	
	public void addCountsForAlignmentTriple(AlignmentTriple alignmentTriple)
	{
		for(AlignmentPair alignmentPair : alignmentTriple.getAlignment().getAlignmentPairs())
		{
			String sourceWord = alignmentTriple.getSourceSentence().get(alignmentPair.getSourcePos());
			String targetWord = alignmentTriple.getTargetSentence().get(alignmentPair.getTargetPos());
			this.incrementCount(getSourceTargetKeyPairForWordPair(sourceWord, targetWord));
		}
	}
	
	private Map<Integer, List<AlignmentPair>> computeSourcePosIndicedAlignmentMap(List<AlignmentPair> alignment)
	{
		 Map<Integer, List<AlignmentPair>> result = new HashMap<Integer, List<AlignmentPair>>();
		 
		 for(AlignmentPair alignmentPair : alignment)
		 {
			 if(!result.containsKey(alignmentPair.getSourcePos()))
			 {
				 result.put(alignmentPair.getSourcePos(),new ArrayList<AlignmentPair>());
			 }
			 result.get(alignmentPair.getSourcePos()).add(alignmentPair);
		 }
		 return result;
	}
	
	private List<Pair<Integer>> computeSourceTargetKeyPairsForAlignment(List<String> sourceSentence, List<String> targetSentence, List<AlignmentPair> alignment)
	{
		List<Pair<Integer>> result = new ArrayList<Pair<Integer>>();
		
		for(AlignmentPair alignmentPair : alignment)
		{
			int sourceWordKey = this.wordKeyMappingTable.getKeyForWord(sourceSentence.get(alignmentPair.getSourcePos()));
			int targetWordKey = this.wordKeyMappingTable.getKeyForWord(targetSentence.get(alignmentPair.getTargetPos()));
			result.add(new Pair<Integer>(sourceWordKey,targetWordKey));
		}
		return result;
	}
	
	private double computeSourceWordLexicalWeight(List<String> sourceSentence, List<String> targetSentence, List<AlignmentPair> sourceWordAlignments)
	{
		double result = 0;

		List<Pair<Integer>> sourceTargetKeyPairs = computeSourceTargetKeyPairsForAlignment(sourceSentence, targetSentence, sourceWordAlignments);
		
		for(Pair<Integer> sourceTargetPair : sourceTargetKeyPairs)
		{	
			double lexicalProbability = this.getProbability(sourceTargetPair);
			result += lexicalProbability;
		}
		
		result = result / sourceTargetKeyPairs.size();
		
		assert(result >= 0);
		return result;
	}
	
	
	@SuppressWarnings("unused")
	private void printArgumentsComputeAlignedPhrasePairProbability(List<String> sourceSentence, List<String> targetSentence, Alignment alignment)
	{
		System.out.println("ConcurrentLexicalProbabilityTable: computeAlignedPhrasePairProbability");
		System.out.println("sourceSentence: " + Utility.stringListStringWithoutBrackets(sourceSentence));
		System.out.println("targetSentence: " + Utility.stringListStringWithoutBrackets(targetSentence));
		System.out.println("Alignment: " + alignment);
	}
	
	
	public double computeAlignedPhrasePairProbability(List<String> sourceSentence, List<String> targetSentence, Alignment alignment)
	{

		//printArgumentsComputeAlignedPhrasePairProbability(sourceSentence, targetSentence, alignment);
		
		double result = 1;
		Map<Integer, List<AlignmentPair>> sourcePosIndexedAlignmentMap =  computeSourcePosIndicedAlignmentMap(alignment.getAlignmentPairs());
		
		for(Integer sourcePos : sourcePosIndexedAlignmentMap.keySet())
		{
			List<AlignmentPair> alignmentPairsForSourcePos = sourcePosIndexedAlignmentMap.get(sourcePos);
			result =  result * computeSourceWordLexicalWeight(sourceSentence, targetSentence,alignmentPairsForSourcePos);
		}
		assert(result >= 0);
		return result;
	}
	
	public double getProbability(String sourceWord, String targetWord)
	{
		Pair<Integer> sourceTargetKeyPair = getSourceTargetKeyPairForWordPair(sourceWord, targetWord);
		return getProbability(sourceTargetKeyPair);
	}
	
	public int getCount(String sourceWord, String targetWord)
	{
		Pair<Integer> sourceTargetKeyPair = getSourceTargetKeyPairForWordPair(sourceWord, targetWord);
		return getCount(sourceTargetKeyPair);
	}
	
	private String getTableContentsStringForCondition(Integer conditionKey)
	{
		String result = "";
		double totalProbability = 0;
		
		String sourceWord = this.wordKeyMappingTable.getWordForKey(conditionKey);
		result += "\n================================ \n P( ? | " + sourceWord + ") : " + "\n\n";
		for(Integer conditionedKey : this.getConditionTable(conditionKey).keySet())
		{
			
			double probability =  this.getProbability(new Pair<Integer>(conditionKey, conditionedKey));
			totalProbability += probability;
			String targetWord = this.wordKeyMappingTable.getWordForKey(conditionedKey);
			result += "P(" + targetWord + " | " + sourceWord + ") = " + probability + "\n";
		}
		result += "Total probability : " + totalProbability + "\n";
		result += "================================";		
		
		return result;
	}
	
	public void printTable()
	{
		System.out.println("######### <Lexical Probability Table> ####################");
		for(Integer conditionKey : this.probabilityTable.keySet())
		{
			System.out.println(getTableContentsStringForCondition(conditionKey));
		}
		System.out.println("######### </Lexical Probability Table> ####################");
	}
	
	
}
