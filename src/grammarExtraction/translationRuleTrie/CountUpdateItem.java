package grammarExtraction.translationRuleTrie;

public class CountUpdateItem 
{
	private double count, lexicalWeightSourceToTarget,  lexicalWeightTargetToSource;
	
	protected CountUpdateItem(double count, double lexicalWeightSourceToTarget, double lexicalWeightTargetToSource)
	{
		this.count = count;
		this.lexicalWeightSourceToTarget = lexicalWeightSourceToTarget;
		this.lexicalWeightTargetToSource = lexicalWeightTargetToSource;
	}
	
	public static CountUpdateItem createCountUpdateItem(double count, double lexicalWeightSourceToTarget, double lexicalWeightTargetToSource)
	{
		return new CountUpdateItem(count, lexicalWeightSourceToTarget, lexicalWeightTargetToSource);
	}
	

	public double getCount()
	{
		return count;
	}
	
	public double getLexicalWeightSourceToTarget()
	{
		return this.lexicalWeightSourceToTarget;
	}
	
	public double getLexicalWeightTargetToSource()
	{
		return this.lexicalWeightTargetToSource;
	}
	
	
}
