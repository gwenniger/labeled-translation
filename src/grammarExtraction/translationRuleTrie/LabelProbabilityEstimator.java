package grammarExtraction.translationRuleTrie;


/**
 * This interface defines various methods with respect to the frequency and probability of 
 * specific labels. 
 * This interface is used for the creation of glue rules, and also for the creation of 
 * unknown word rules for the most likely labels.
 *  
 * 
 * @author gemaille
 *
 */
public interface LabelProbabilityEstimator {

	public void increaseCount(Integer labelKey, double delta);
	// probability for labels, used for sorting unknown word rules by their probability
	public double getProbability(Integer labelKey);
	// Probability used for features of glue rules
	public double getProbabilityForGlueRules(Integer labelKey);
	public double getRarityPenaltyLabel(Integer labelKey);
	public double getLabelCount(Integer labelKey); 
	
	public void showContentsForDebugging(WordKeyMappingTable wordKeyMappingTable);
}
