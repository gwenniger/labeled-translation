package grammarExtraction.translationRuleTrie;

import grammarExtraction.IntegerKeyRuleRepresentation;

import java.util.Comparator;

public class CountItemComparator implements Comparator<CountItem> {

	@Override
	public int compare(CountItem o1, CountItem o2) {

		// One of the items can be null, which is a result of getting the elements from 
		// an List that is possibly simultaneously resized, so that the index of binary search 
		// may not correspond to not-null entries at all times
		if((o1 == null) && (o2 == null))
		{
			return 0;
		}
		else if (o1 == null)
		{
			return -1;
		}
		else if (o2 == null)
		{
			return 1;
		}
		else
		{	
			IntegerKeyRuleRepresentation integerKeyRepresentation1 = o1.getLabel().restoreRuleRepresentation();
			IntegerKeyRuleRepresentation integerKeyRepresentation2 = o2.getLabel().restoreRuleRepresentation();
			return integerKeyRepresentation1.compareTo(integerKeyRepresentation2);
		}	
	}

}
