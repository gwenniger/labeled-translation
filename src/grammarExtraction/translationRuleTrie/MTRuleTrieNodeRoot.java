package grammarExtraction.translationRuleTrie;

import grammarExtraction.IntegerKeyRuleRepresentationWithLabel;
import grammarExtraction.grammarExtractionFoundation.LabelingSettings.CanonicalFormSettings;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import junit.framework.Assert;
import arraySkipList.ArraySkipList;

/*
 * Warning : This class currently uses explicit locks. So do not use synchronized as well at the same 
 * time, as this can potentially result in deadlock!!!
 */

public class MTRuleTrieNodeRoot extends MTRuleTrieNode implements Serializable {

	private static final long serialVersionUID = 1L;
	private int numRealChildren = 0;
	final CountItemCreater countItemCreator;
	private final List<MTRuleTrieNode> childNodesList;
	private final CanonicalFormSettings canonicalFormSettings;

	private MTRuleTrieNodeRoot(ArraySkipList<? extends TrieNode> childNodes, Integer label, MTRuleTrieNode parentTrieNode, RuleCountsTable countsTable,
			CountItemCreater countItemCreater, List<MTRuleTrieNode> childNodesList,CanonicalFormSettings canonicalFormSettings) {
		super(childNodes, label, parentTrieNode, countsTable);
		this.countItemCreator = countItemCreater;
		this.childNodesList = childNodesList;
		this.canonicalFormSettings = canonicalFormSettings;
	}

	public static MTRuleTrieNodeRoot createMTRuleTrieNodeRoot(WordKeyMappingTable wordKeyMappingTable, CountItemCreater countItemCreater,CanonicalFormSettings canonicalFormSettings) {
		MTRuleTrieNodeRoot result = new MTRuleTrieNodeRoot(null, -1, null, null, countItemCreater, createRootChildNodes(wordKeyMappingTable),canonicalFormSettings);
		return result;
	}

	private static List<MTRuleTrieNode> createRootChildNodes(WordKeyMappingTable wordKeyMappingTable) {
		List<MTRuleTrieNode> childNodesList = new ArrayList<MTRuleTrieNode>();

		Assert.assertNotNull(wordKeyMappingTable);
		for (int i = 0; i < wordKeyMappingTable.getMaxNumItems(); i++) {
			// ((List<TrieNode>)childNodes).add(MTRuleTrieNode.createMTRuleTrieNode(i,
			// this));
			childNodesList.add(null);
		}
		return childNodesList;
	}

	public void addTranslationRuleCountToTrie(IntegerKeyRuleRepresentationWithLabel sourceRuleRepresentation, IntegerKeyRuleRepresentationWithLabel targetRuleRepresentation,
			CountUpdateItem countUpdateItem, MTRuleTrieNode targetTrie) {
		MTRuleTrieNode tableContainingSourceChild = (MTRuleTrieNode) this.addOrRetrieveRuleRepresentation(sourceRuleRepresentation, 0);
		tableContainingSourceChild.addRuleCountToTable(targetRuleRepresentation, countUpdateItem, targetTrie, this.countItemCreator,this.canonicalFormSettings);
	}

	public void addTranslationRuleCountToTrieAnomouslyIfConditionedRuleRepresentationNotYetPresent(IntegerKeyRuleRepresentationWithLabel sourceRuleRepresentation,
			IntegerKeyRuleRepresentationWithLabel targetRuleRepresentation, CountUpdateItem countUpdateItem, MTRuleTrieNode targetTrie) {
		MTRuleTrieNode tableContainingSourceChild = (MTRuleTrieNode) this.addOrRetrieveRuleRepresentation(sourceRuleRepresentation, 0);
		tableContainingSourceChild.addRuleCountToTableAnomouslyIfConditionedRuleRepresentationNotYetPresent(targetRuleRepresentation, countUpdateItem,
				targetTrie, this.countItemCreator,this.canonicalFormSettings);
	}

	// replaced synchronized with reentrance lock
	protected TrieNode getOrInsertAndGetChildSynchronized(Integer sourceKey) {
		acquireLockWitmMaxWaitTimeWarning();
		try {
			MTRuleTrieNode result;

			if (sourceKey < 0) {
				throw new RuntimeException(
						"MTRuleTrieNode.getOrInsertAndGetChild Error: trying to get a child for an invalid source key. Valid source keys are 0 or bigger");
			}

			TrieNode childNode = getChildWithLabel(sourceKey);

			if (childNode == null) {
				result = MTRuleTrieNode.createMTRuleTrieNode(sourceKey, this);
				childNodesList.set(sourceKey, result);
				numRealChildren++;
			} else {
				result = (MTRuleTrieNode) childNode;
			}
			return result;
		} finally {
			releaseLock();
		}
	}

	@Override
	/**
	 * This method gets a child with a certain sourceKey if it is present in this node.
	 * Otherwise null is returned.
	 * @param sourceKey The source key
	 * @return 
	 */
	protected TrieNode getChildWithLabel(Integer sourceKey) {
		return this.childNodesList.get(sourceKey);
	}

	@Override
	public int getNumRealChildren() {
		return this.numRealChildren;
	}

	public int getTotalNumRules() {
		int result = 0;
		Enumeration<RuleCountsTable> ruleCountsTableEnumeration = TrieRuleCountsTableEnumeration.createTrieRuleCountsTableEnumeration(this);

		while (ruleCountsTableEnumeration.hasMoreElements()) {
			result += ruleCountsTableEnumeration.nextElement().getNumElements();
		}
		return result;
	}

	@Override
	public List<TrieNode> getChildNodes() {
		List<TrieNode> result = new ArrayList<TrieNode>();
		for (TrieNode childNode : childNodesList) {
			if (childNode != null) {
				result.add(childNode);
			}
		}
		return result;
	}

	public String toString() {
		String result = "\n<TrieNodeRoot>";
		result += " Node Label: " + this.label + " ";
		result += "NumChildren: " + this.getNumChildren();

		if (this.childNodesList != null) {
			String childLabels = "\t ChildLabels: ";
			for (TrieNode child : getChildNodes()) {
				childLabels += child.getLabel() + " ";
			}
			result += childLabels;
		}

		result += "CountsTable: " + this.countsTable;
		result += "</TrieNodeRoot>";
		return result;
	}

	@Override
	public TrieNode getChildWithIndex(int index) {
		return this.childNodesList.get(index);
	}
	
	public boolean useCanonicalFormLabeledRulesWithOnlyHieroWeights(){
	    return canonicalFormSettings.useCanonicalFormLabeledRules() && canonicalFormSettings.useOnlyHieroWeightsForCanonicalFormLabeledRules();
	}

}
