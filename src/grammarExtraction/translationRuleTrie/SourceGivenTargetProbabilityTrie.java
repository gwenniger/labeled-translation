package grammarExtraction.translationRuleTrie;


public class SourceGivenTargetProbabilityTrie extends ConditionalProbabilityTrie
{

	private static final long serialVersionUID = 1L;

	protected SourceGivenTargetProbabilityTrie(
			MTRuleTrieNodeRoot conditionalProbabilityTrie,
			WordKeyMappingTable wordKeyMappingTable,
			MTRuleTrieNodeRoot conditionedElementsRepresentationTrie) 
	{
		super(conditionalProbabilityTrie, wordKeyMappingTable, conditionedElementsRepresentationTrie);
	}

	public static SourceGivenTargetProbabilityTrie createSourceGivenTargetProbabilityTrie(TranslationRuleTrie translationRuleTrie,
			WordKeyMappingTable wordKeyMappingTable)
	{
		SourceGivenTargetProbabilityTrie result = new SourceGivenTargetProbabilityTrie(translationRuleTrie.getTargetTrie(),wordKeyMappingTable, translationRuleTrie.getSourceTrie());
		result.computeTotalCounts();
		return result;
	}
}
