package grammarExtraction.translationRuleTrie;

import grammarExtraction.IntegerKeyRuleRepresentationWithLabel;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Stack;

import bitg.Pair;

public class TrieElementsEnumeration  implements Enumeration<Pair<IntegerKeyRuleRepresentationWithLabel,RuleCountsTable>>
{
	Stack<MTRuleTrieNode> nodeStack;
	Stack<Pair<Integer,Integer>> branchChoiceStack;
	private boolean returnedValueForLevel = false;
	
	private TrieElementsEnumeration(Stack<MTRuleTrieNode> nodeStack,Stack<Pair<Integer,Integer>> branchChoiceStack) 
	{
		this.nodeStack = nodeStack;
		this.branchChoiceStack = branchChoiceStack;
	}
	
	public static  TrieElementsEnumeration createTrieElementsEnumeration(MTRuleTrieNode rootNode)
	{
		Stack<MTRuleTrieNode> nodeStack = new Stack<MTRuleTrieNode>();
		Stack<Pair<Integer,Integer>> branchChoiceStack = new Stack<Pair<Integer,Integer>>();
		assert(rootNode != null);
		nodeStack.push(rootNode);
		branchChoiceStack.push(new Pair<Integer,Integer>(-1,0));
		TrieElementsEnumeration result = new TrieElementsEnumeration(nodeStack, branchChoiceStack);
		result.goToNextElement();
		return result;
		
	}

	private MTRuleTrieNode peekTopNode()
	{
		return this.nodeStack.peek();
	}
	
	/*
	private int peekChosenIndex()
	{
		return this.branchChoiceStack.peek().first;
	}
	*/
	
	private int peekChosenChildNumber()
	{
		return this.branchChoiceStack.peek().last;
	}

	private MTRuleTrieNode incrementTopChosenChild()
	{
		Pair<Integer,Integer> branchingChoice = this.branchChoiceStack.pop();
		int newIndex  = branchingChoice.first + 1;
		int newChildNumber = branchingChoice.last + 1;
		
		MTRuleTrieNode nextChild = (MTRuleTrieNode) peekTopNode().getChildWithIndex(newIndex); 
		
		while(nextChild == null)
		{
			 newIndex++;
			 nextChild = (MTRuleTrieNode) peekTopNode().getChildWithIndex(newIndex); 
		}
		
		
		this.branchChoiceStack.push(new Pair<Integer, Integer>(newIndex, newChildNumber));
		this.branchChoiceStack.push(new Pair<Integer, Integer>(-1,0));
		this.nodeStack.push(nextChild);
		return nextChild;
	}
	

	
	private boolean hasValueChoiceLeftAtCurrentLevel()
	{
		return (!this.returnedValueForLevel) && this.peekTopNode().hasValue();
	}
	
	
	private boolean moreChildrenChoicesLeftAtThisLevel()
	{
		return (peekTopNode().getNumRealChildren() > 0) && (peekChosenChildNumber() < (peekTopNode().getNumRealChildren()));
	}
	
	private void goToNextElement()
	{
		
		//System.out.println("Go to next element called");
		//System.out.println("Current Enumeration state: " + this);
	
		while(true)
		{	
		
			
			if(nodeStack.isEmpty())
			{
				return;
			}
			
			if(moreChildrenChoicesLeftAtThisLevel())
			{
				returnedValueForLevel = false;
				incrementTopChosenChild();
				continue;
			}
			
			else if(hasValueChoiceLeftAtCurrentLevel())
			{
				returnedValueForLevel = true;
				return;
			}
			else
			{
				returnedValueForLevel = false;
				this.nodeStack.pop();
				this.branchChoiceStack.pop();
			}
		}	
			
	}
	
	
	private IntegerKeyRuleRepresentationWithLabel getConditioningRepresentation()
	{
		List<Integer> partKeys = new ArrayList<Integer>();
		
		for(int i = 1; i < this.nodeStack.size(); i++)
		{
			partKeys.add(nodeStack.get(i).label);
		}
		return IntegerKeyRuleRepresentationWithLabel.createIntegerKeyRuleRepresentationWithLabel(partKeys);
	}
	
	
	@Override
	public boolean hasMoreElements() 
	{
		//System.out.println("Has more elements called");
		//System.out.println("Current state: " + this);
		return !(this.nodeStack.isEmpty());
		
	}

	@Override
	public Pair<IntegerKeyRuleRepresentationWithLabel,RuleCountsTable> nextElement() 
	{
		IntegerKeyRuleRepresentationWithLabel conditioningRepresentation = getConditioningRepresentation();
		assert(conditioningRepresentation != null);
		RuleCountsTable ruleCountsTable = (RuleCountsTable) peekTopNode().getcountsTable();
		goToNextElement();
		return new Pair<IntegerKeyRuleRepresentationWithLabel,RuleCountsTable>(conditioningRepresentation, ruleCountsTable);
	}
	
	
	public String toString()
	{
		String result = "";
		result += this.branchChoiceStack.toString() + this.nodeStack.toString();
		return result;
	}
	
}
