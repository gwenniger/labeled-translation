package grammarExtraction.translationRuleTrie;

import grammarExtraction.IntegerKeyRuleRepresentationWithLabel;

import java.io.Serializable;
import java.util.List;

import bitg.Pair;
import util.ObjectFactory;

public abstract class CountItem implements ObjectFactory, CountItemCreater, Serializable {
	private static final long serialVersionUID = 1L;
	protected double count;
	private final MTRuleTrieNode label;

	protected CountItem(MTRuleTrieNode label, double count) {
		this.count = count;
		this.label = label;
	}

	public CountItem(MTRuleTrieNode label) {
		this(label, 0);
	}

	public CountItem() {
		this(null, 0);
	}

	public static CountItem createSearchNode(MTRuleTrieNode label) {
		return new BasicCountItem(label, 0);
	}

	public double getCount() {
		return count;
	}

	public MTRuleTrieNode getLabel() {
		return label;
	}

	public abstract void performCountUpdate(CountUpdateItem countUpdateItem);

	// Methods used to get to determine the most frequent labeled rule variant of
	// a certain unlabeled rule type

	// Method to add a labeled rule (and its frequency information) with this Countitem
	public abstract void addLabeledRule(LabeledRuleWithCount labeledRuleWithCount);

	// Get the most frequent labeled version for this (unlabeled rule) CountItem
	// This method is only implemented in some extensions of this abstract class
	public abstract Pair<MTRuleTrieNode,CountItem> getMostFrequentLabeledRuleVersion();

	public abstract RuleLabeling getMostFrequentRuleLabelingWithinAllowedRuleLabelingsForSourceList(
		    List<RuleLabeling> mostFrequentRuleLabelingsForSourceList,
		    WordKeyMappingTable wordKeyMappingTable);
	
	public String toString() {
		return "\n<CountItem>: " + count + " label " + label.toStringWithoutCountTable() + " </CountItem>";
	}

	public static class LabeledRuleWithCount {
		private final MTRuleTrieNode ruleSourceSideTrieNode;
		private final CountItem ruleTargetSideCountItem;

		public LabeledRuleWithCount(MTRuleTrieNode ruleSourceSideTrieNode, CountItem ruleTargetSideCountItem) {
			this.ruleSourceSideTrieNode = ruleSourceSideTrieNode;
			this.ruleTargetSideCountItem = ruleTargetSideCountItem;
		}

		public CountItem getRuleTargetSideCountItem() {
			return ruleTargetSideCountItem;
		}

		public MTRuleTrieNode getRuleSourceSideTrieNode() {
			return ruleSourceSideTrieNode;
		}

		public IntegerKeyRuleRepresentationWithLabel getTargetSideRepresentation() {
			return this.ruleTargetSideCountItem.getLabel().restoreRuleRepresentation();
		}
	}

}
