package grammarExtraction.translationRuleTrie;

import org.junit.Assert;
import grammarExtraction.IntegerKeyRuleRepresentationWithLabel;
import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.phraseProbabilityWeights.NamedProbabilityFeature;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsSet;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsSetCreater;
import grammarExtraction.samtGrammarExtraction.UnknownWordRulesCreatorPosTags;
import grammarExtraction.translationRules.LightWeightTranslationRuleWithFeatures;
import grammarExtraction.translationRules.TranslationRuleFeatures;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

public abstract class FeatureEnrichedGrammarRuleCreator implements GlueFeaturesCreater {

	protected final TranslationRuleProbabilityTable mainRuleProbabilityTable;
	protected final PhraseProbabilityWeightsSetCreater phraseProbabilityWeightsSetCreater;
	protected final SystemIdentity systemIdentity;

	protected FeatureEnrichedGrammarRuleCreator(TranslationRuleProbabilityTable mainRuleProbabilityTable, PhraseProbabilityWeightsSetCreater phraseProbabilityWeightsSetCreater,
			SystemIdentity systemIdentity) {
		this.mainRuleProbabilityTable = mainRuleProbabilityTable;
		this.phraseProbabilityWeightsSetCreater = phraseProbabilityWeightsSetCreater;
		this.systemIdentity = systemIdentity;
	}

	protected String createBasicFeaturesGrammarRuleRepresentation(TranslationRuleSignatureTriple translationRuleSignatureTriple, boolean useExtraSAMTFeatures) {
		IntegerKeyRuleRepresentationWithLabel sourceRuleSide = translationRuleSignatureTriple.getSourceSideRepresentationWithLabel();
		IntegerKeyRuleRepresentationWithLabel targetRuleSide = translationRuleSignatureTriple.getTargetSideRepresentationWithLabel();
		String result;
		PhraseProbabilityWeightsSet phraseProbabilityWeightsSet = this.phraseProbabilityWeightsSetCreater.createProbabilityWeightsSet(translationRuleSignatureTriple);
		BasicRuleFeaturesProperties basicRuleFeatureProperties = BasicRuleFeaturesProperties.createBasicRuleFeaturesProperties(translationRuleSignatureTriple, mainRuleProbabilityTable);
		result = ""
				+ getHieroRuleRepresentation(targetRuleSide, sourceRuleSide, phraseProbabilityWeightsSet.getBasicPhraseProbabilityWeightTargetGivenSource(),
						phraseProbabilityWeightsSet.getBasicPhraseProbabilityWeightSourceGivenTarget(), basicRuleFeatureProperties.lexicalProbailities);
		return result;
	}

	public LabelProbabilityEstimator getLabelProbabilityEstimator() {
		return mainRuleProbabilityTable.getLabelProbabilityEstimator();
	}

	private String getHieroRuleRepresentation(IntegerKeyRuleRepresentationWithLabel conditioned, IntegerKeyRuleRepresentationWithLabel condition, NamedProbabilityFeature namedProbabilityFeature,
			NamedProbabilityFeature namedProbabilityFeature2, util.Pair<Double> lexicalProbabilities) {
		float sourceToTargetProbabilityRepresentation = (float) namedProbabilityFeature.getUnlabeledFeatureValue(systemIdentity.getFeatureValueFormatter());
		float targetToSourceProbabilityRepresentation = (float) namedProbabilityFeature2.getUnlabeledFeatureValue(systemIdentity.getFeatureValueFormatter());
		float sourceGivenTargetLexicalWeightRepresentation = lexicalProbabilities.getFirst().floatValue();
		float targetGivenSourceLexicalWeightRepresentation = lexicalProbabilities.getSecond().floatValue();
		String result = condition.getLeftHandSideLabelStringRepresentation(mainRuleProbabilityTable.wordKeyMappingTable) + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR
				+ condition.getStringRepresentationExcludingLeftHandSideLabelJoshua(mainRuleProbabilityTable.wordKeyMappingTable) + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR
				+ conditioned.getStringRepresentationExcludingLeftHandSideLabelJoshua(mainRuleProbabilityTable.wordKeyMappingTable) + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR
				+ sourceToTargetProbabilityRepresentation + " " + targetGivenSourceLexicalWeightRepresentation + " " + sourceGivenTargetLexicalWeightRepresentation + " "
				+ targetToSourceProbabilityRepresentation;

		return result;
	}

	protected boolean isAbstractRule(TranslationRuleSignatureTriple translationRuleSignatureTriple) {
		return translationRuleSignatureTriple.isAbstractRule(this.mainRuleProbabilityTable.getWordKeyMappingTable());
	}

	protected String createFeatureEnrichedGrammarRuleRepresentation(TranslationRuleSignatureTriple translationRuleSignatureTriple, boolean useExtraSAMTFeatures,
			boolean writeLabeledFeaturesRepresentation) {
		String result;

		PhraseProbabilityWeightsSet phraseProbabilityWeightsSet = this.phraseProbabilityWeightsSetCreater.createProbabilityWeightsSet(translationRuleSignatureTriple);
		BasicRuleFeaturesProperties basicRuleFeatureProperties = BasicRuleFeaturesProperties.createBasicRuleFeaturesProperties(translationRuleSignatureTriple, mainRuleProbabilityTable);
		result = ""
				+ getHieroJoshuaRuleRepresentationWithRelabelingIfNecessary(translationRuleSignatureTriple, phraseProbabilityWeightsSet, basicRuleFeatureProperties.lexicalProbailities,
						basicRuleFeatureProperties.ruleCorpusCount, useExtraSAMTFeatures, writeLabeledFeaturesRepresentation);
		return result;
	}
	
	protected String createFeatureEnrichedGrammarRuleRepresentationRelabeled(TranslationRuleSignatureTriple translationRuleSignatureTriple, boolean useExtraSAMTFeatures,
		boolean writeLabeledFeaturesRepresentation,RuleLabeling ruleLabeling) {
        	String result;
        
        	PhraseProbabilityWeightsSet phraseProbabilityWeightsSet = this.phraseProbabilityWeightsSetCreater.createProbabilityWeightsSet(translationRuleSignatureTriple);
        	BasicRuleFeaturesProperties basicRuleFeatureProperties = BasicRuleFeaturesProperties.createBasicRuleFeaturesProperties(translationRuleSignatureTriple, mainRuleProbabilityTable);
        	result = ""
        			+ getRelabeledHieroJoshuaRuleRepresentation(translationRuleSignatureTriple, phraseProbabilityWeightsSet, basicRuleFeatureProperties.lexicalProbailities,
        					basicRuleFeatureProperties.ruleCorpusCount, useExtraSAMTFeatures, writeLabeledFeaturesRepresentation,ruleLabeling);
        	return result;
        }

	protected LightWeightTranslationRuleWithFeatures returnScaleProbabilitiesRuleIfMosesUnknownWordsRule(LightWeightTranslationRuleWithFeatures lightWeightTranslationRuleWithFeatures) {
		if (systemIdentity.useMosesHieroUnknownWordRuleVariants()) {
			int numberOfXLabeledGaps = lightWeightTranslationRuleWithFeatures.countNumberOfXLabeledGaps(this.mainRuleProbabilityTable.getWordKeyMappingTable());

			if (numberOfXLabeledGaps > 0) {
				double scalingFactor = Math.pow(UnknownWordRulesCreatorPosTags.UNKNOWN_WORD_PROBABILITY, numberOfXLabeledGaps);
				return lightWeightTranslationRuleWithFeatures.createLightWeightTranslationRuleWithScaledProbabilities(scalingFactor);
			}
		}
		return lightWeightTranslationRuleWithFeatures;
	}

	public String getHieroJoshuaRuleRepresentationWithRelabelingIfNecessary(TranslationRuleSignatureTriple translationRuleSignatureTriple, PhraseProbabilityWeightsSet phraseProbabilityWeightsSet,
			util.Pair<Double> lexicalProbabilities, double ruleCorpusCount, boolean useExtraSAMTFeatures, boolean writeLabeledFeaturesRepresentation) {
		TranslationRuleSignatureTriple translationRuleSignatureTriple2 = translationRuleSignatureTriple;
		if ((systemIdentity.labelSourceSide() && (!systemIdentity.labelTargetSide())) || ((!systemIdentity.labelSourceSide()) && systemIdentity.labelTargetSide())) {
			translationRuleSignatureTriple2 = TranslationRuleSignatureTriple.createTranslationRuleSignatureTripleWithLabelsOnBothSides(translationRuleSignatureTriple,
					this.mainRuleProbabilityTable.getWordKeyMappingTable());
		}

		Integer labelKey = translationRuleSignatureTriple.getRuleLabel().getSourceSideLabel();
		double ruleGivenLabelGenerativeProbability = computeRuleGivenLabelGenerativeProbability(labelKey, translationRuleSignatureTriple);
		return getHieroJoshuaRuleRepresentation(translationRuleSignatureTriple2, phraseProbabilityWeightsSet, lexicalProbabilities, ruleCorpusCount, useExtraSAMTFeatures,
				writeLabeledFeaturesRepresentation,ruleGivenLabelGenerativeProbability);
	}
	
	public String getRelabeledHieroJoshuaRuleRepresentation(TranslationRuleSignatureTriple translationRuleSignatureTriple, PhraseProbabilityWeightsSet phraseProbabilityWeightsSet,
		util.Pair<Double> lexicalProbabilities, double ruleCorpusCount, boolean useExtraSAMTFeatures, boolean writeLabeledFeaturesRepresentation,RuleLabeling ruleLabeling) {
	TranslationRuleSignatureTriple translationRuleSignatureTriple2 = TranslationRuleSignatureTriple.createTranslationRuleSignatureTripleWithAdaptedLabels(translationRuleSignatureTriple, ruleLabeling, this.mainRuleProbabilityTable.getWordKeyMappingTable());

	Integer labelKey = translationRuleSignatureTriple.getRuleLabel().getSourceSideLabel();
	double ruleGivenLabelGenerativeProbability = computeRuleGivenLabelGenerativeProbability(labelKey, translationRuleSignatureTriple);
	return getHieroJoshuaRuleRepresentation(translationRuleSignatureTriple2, phraseProbabilityWeightsSet, lexicalProbabilities, ruleCorpusCount, useExtraSAMTFeatures,
			writeLabeledFeaturesRepresentation,ruleGivenLabelGenerativeProbability);
}

	protected abstract String getHieroJoshuaRuleRepresentation(TranslationRuleSignatureTriple translationRuleSignatureTriple, PhraseProbabilityWeightsSet phraseProbabilityWeightsSet,
			util.Pair<Double> lexicalProbabilities, double ruleCorpusCount, boolean useExtraSAMTFeatures, boolean writeLabeledFeaturesRepresentation, double ruleGivenLabelGenerativeProbability);

	protected double computeRuleGivenLabelGenerativeProbability(Integer labelKey, TranslationRuleSignatureTriple translationRuleSignatureTriple) {
		double ruleCount = this.mainRuleProbabilityTable.getRuleCount(translationRuleSignatureTriple.getSourceSideRepresentationWithLabel(),
				translationRuleSignatureTriple.getTargetSideRepresentationWithLabel());
		double rulesWithLabelCount = this.getLabelProbabilityEstimator().getLabelCount(labelKey);
		double result = ruleCount / rulesWithLabelCount;
		
		//System.out.println("ruleCount: " + ruleCount);
		//System.out.println("rulesWithLabelCount: " + rulesWithLabelCount);
		Assert.assertTrue(result > 0);
		Assert.assertTrue(result <= 1);
		return result;
	}

	public abstract boolean useExtraSamtFeatures();

	public abstract TranslationRuleFeatures getUnknownWordsTranslationRuleFeatures(SystemIdentity systemIdentity, boolean isHieroRule);

	public abstract TranslationRuleFeatures getReorderingLabelToPhraseLabelSwitchRuleFeatures(SystemIdentity systemIdentity);

	public abstract TranslationRuleFeatures geGlueRuleFeatures(SystemIdentity systemIdentity, double generativeGlueLabelProbability, boolean isHieroRule, double rarityPenalty);
	
}
