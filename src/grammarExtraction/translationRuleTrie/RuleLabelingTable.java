package grammarExtraction.translationRuleTrie;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RuleLabelingTable {

    private final List<RuleLabeling> ruleLabelingList;
    private List<RuleLabeling> nMostFrequentRuleLabelingsList;
    private int maxNumValuesPerLabelBestRuleLabelingsHasBeenComputedFor = -1;

    private RuleLabelingTable(List<RuleLabeling> ruleLabelingList) {
	this.ruleLabelingList = ruleLabelingList;
    }

    public static final RuleLabelingTable createRuleLabelingTable() {
	return new RuleLabelingTable(new ArrayList<RuleLabeling>());
    }

    private int getRuleLabelingIndex(RuleLabeling ruleLabeling) {
	int index = Collections.binarySearch(ruleLabelingList, ruleLabeling);
	return index;
    }

    public void addRuleLablingToTable(RuleLabeling ruleLabeling) {
	int index = getRuleLabelingIndex(ruleLabeling);

	if (index < 0) {
	    // We translate the index returned by binary search
	    // (-(insertion point) - 1) into the index were we have
	    // to insert using the unary bitwise complement operator
	    index = ~index;
	    this.ruleLabelingList.add(index, ruleLabeling);
	} else {
	    RuleLabeling existingRuleLabeling = this.ruleLabelingList.get(index);
	    existingRuleLabeling.addLabelingCount(ruleLabeling.getLabelingCount());
	}
    }

    private List<RuleLabeling> computeTopRuleLabelingsWithSpecifiedMaxAlteternativesPerLabel(
	    int maxNumberDifferentValuesPerLabel) {
	MostFrequentRuleLabelingListCompter mostFrequentRuleLabelingListCompter = MostFrequentRuleLabelingListCompter
		.createMostFrequentRuleLabelingListCompter(ruleLabelingList,
			maxNumberDifferentValuesPerLabel);
	List<RuleLabeling> result = mostFrequentRuleLabelingListCompter
		.getTopRuleLabelingsWithSpecifiedMaxAlteternativesPerLabel();
	return result;
    }

    private boolean hasCorretTopRuleLabelingsList(int maxNumDifferentValuesPerLabel) {
	if (this.nMostFrequentRuleLabelingsList != null) {
	    return (maxNumDifferentValuesPerLabel == maxNumValuesPerLabelBestRuleLabelingsHasBeenComputedFor);
	}
	return false;
    }

    public List<RuleLabeling> getOrComputeAndGetMostFrequentRuleLabelingsList(
	    int maxNumRuleLabelings) {
	if (hasCorretTopRuleLabelingsList(maxNumRuleLabelings)) {
	    return this.nMostFrequentRuleLabelingsList;
	} else {
	    this.nMostFrequentRuleLabelingsList = computeTopRuleLabelingsWithSpecifiedMaxAlteternativesPerLabel(maxNumRuleLabelings);
	    return this.nMostFrequentRuleLabelingsList;
	}
    }

}
