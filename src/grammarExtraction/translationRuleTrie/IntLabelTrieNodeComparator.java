package grammarExtraction.translationRuleTrie;


import java.util.Comparator;

public class IntLabelTrieNodeComparator implements Comparator<TrieNode>
{

	public  int compare(TrieNode o1,TrieNode o2)
	{
		return o1.getLabel().compareTo(o2.getLabel());
	}

}
	
