package grammarExtraction.translationRuleTrie;

import util.Pair;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsSet;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsSetCreater;
import grammarExtraction.samtGrammarExtraction.RuleFeaturesStandard;
import grammarExtraction.translationRules.LightWeightTranslationRuleWithFeatures;
import grammarExtraction.translationRules.TranslationRuleFeatures;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

public class StandardFeatureEnrichedGrammarRuleCreater extends FeatureEnrichedGrammarRuleCreator {

	protected StandardFeatureEnrichedGrammarRuleCreater(TranslationRuleProbabilityTable mainRuleProbabilityTable, PhraseProbabilityWeightsSetCreater phraseProbabilityWeightsSetCreater,
			SystemIdentity systemIdentity) {
		super(mainRuleProbabilityTable, phraseProbabilityWeightsSetCreater, systemIdentity);
	}

	public static StandardFeatureEnrichedGrammarRuleCreater createStandardFeatureEnrichedGrammarRuleCreater(TranslationRuleProbabilityTable mainRuleProbabilityTable,
			PhraseProbabilityWeightsSetCreater phraseProbabilityWeightsSetCreater, SystemIdentity systemIdentity) {
		return new StandardFeatureEnrichedGrammarRuleCreater(mainRuleProbabilityTable, phraseProbabilityWeightsSetCreater, systemIdentity);
	}

	@Override
	protected String getHieroJoshuaRuleRepresentation(TranslationRuleSignatureTriple translationRuleSignatureTriple, PhraseProbabilityWeightsSet phraseProbabilityWeightsSet,
			Pair<Double> lexicalProbabilities, double ruleCorpusCount, boolean useExtraSAMTFeatures, boolean writeLabeledFeaturesRepresentation,double ruleGivenLabelGenerativeProbability) {
		LightWeightTranslationRuleWithFeatures ruleWithFeatures;

		ruleWithFeatures = LightWeightTranslationRuleWithFeatures.createLightWeightTranslationRuleWithStandardFeatures(
				ruleGivenLabelGenerativeProbability, translationRuleSignatureTriple, phraseProbabilityWeightsSet, lexicalProbabilities.getSecond(),
				lexicalProbabilities.getFirst(), ruleCorpusCount, !translationRuleSignatureTriple.getTargetSideRepresentationWithLabel().isSyntacticRuleSide(), false, false,
				mainRuleProbabilityTable.wordKeyMappingTable,systemIdentity.getFeatureValueFormatter());

		ruleWithFeatures = returnScaleProbabilitiesRuleIfMosesUnknownWordsRule(ruleWithFeatures);
		String result = ruleWithFeatures.getNormalRuleRepresentation(mainRuleProbabilityTable.wordKeyMappingTable, systemIdentity.getTranslationRuleRepresentationCreater());

		return result;
	}

	@Override
	public boolean useExtraSamtFeatures() {
		return true;
	}

	@Override
	public TranslationRuleFeatures getUnknownWordsTranslationRuleFeatures(SystemIdentity systemIdentity, boolean isHieroRule) {
		return RuleFeaturesStandard.createCorrectSizeUnknnownWordRuleRuleFeaturesStandard(systemIdentity, isHieroRule);
	}

	@Override
	public TranslationRuleFeatures getReorderingLabelToPhraseLabelSwitchRuleFeatures(SystemIdentity systemIdentity) {
		return RuleFeaturesStandard.createCorrectSizeReorderingLabelToPhraseLabelSwitchRuleFeaturesStandard(systemIdentity);
	}

	@Override
	public TranslationRuleFeatures geGlueRuleFeatures(SystemIdentity systemIdentity, double generativeGlueLabelProbability, boolean isHieroRule, double rarityPenalty) {
		return RuleFeaturesStandard.createCorrectSizeGlueRuleFeaturesStandard(systemIdentity, generativeGlueLabelProbability, isHieroRule, rarityPenalty);
	}

}
