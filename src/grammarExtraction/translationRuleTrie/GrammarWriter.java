package grammarExtraction.translationRuleTrie;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Enumeration;

import junit.framework.Assert;
import bitg.Pair;
import grammarExtraction.IntegerKeyRuleRepresentationWithLabel;
import grammarExtraction.chiangGrammarExtraction.ReorderLabelingSettings;
import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.phraseProbabilityWeights.BasicProbabilityWeightsSetCreater;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsSetCreater;
import grammarExtraction.phraseProbabilityWeights.WordEnrichmentSmoothedProbabilityWeightsSetCreater;
import grammarExtraction.reorderingLabeling.ReorderingLabelToPhraseLabelSwitchRuleCreater;
import grammarExtraction.samtGrammarExtraction.GapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator;
import grammarExtraction.samtGrammarExtraction.UnknownWordRulesCreator;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

public class GrammarWriter {

    // private static final int NUM_lEXIXAL_PLUS_PHRASE_WEIGHTS_BASIC_GRAMMAR =
    // 4;
    // private static final int
    // NUM_lEXIXAL_PLUS_PHRASE_WEIGHTS_WORD_ENRICHMENT_SMOOTHED_GRAMMAR = 6;
    private final TranslationRuleProbabilityTable mainTranslationRuleProabilityTable;
    protected final UnknownWordRulesCreator unknownWordsRulesCreator;
    // private final int numLexicalPlusPhraseWeights;
    private final boolean writePlainHieroRules;
    private final FeatureEnrichedGrammarRuleCreator featureEnrichedGrammarRuleCreator;
    private final SystemIdentity systemIdentity;

    private GrammarWriter(TranslationRuleProbabilityTable mainTranslationRuleProabilityTable,
	    UnknownWordRulesCreator unknownWordsRulesCreator, boolean writePlainHieroRules,
	    FeatureEnrichedGrammarRuleCreator featureEnrichedGrammarRuleCreator,
	    SystemIdentity systemIdentity) {
	this.mainTranslationRuleProabilityTable = mainTranslationRuleProabilityTable;
	this.unknownWordsRulesCreator = unknownWordsRulesCreator;
	this.writePlainHieroRules = writePlainHieroRules;
	this.featureEnrichedGrammarRuleCreator = featureEnrichedGrammarRuleCreator;
	this.systemIdentity = systemIdentity;
    }

    public static GrammarWriter createBasicGrammarWriter(
	    TranslationRuleProbabilityTable mainTranslationRuleProbabilityTable,
	    UnknownWordRulesCreator unknownWordsRulesCreator, boolean writePlainHieroRules,
	    SystemIdentity systemIdentity) {
	// int noLexicalPlusPhraseFeatures =
	// mainTranslationRuleProbabilityTable.getNumLexicalPlusPhraseProbabilityFeatures();

	PhraseProbabilityWeightsSetCreater phraseProbabilityWeightsSetCreater = BasicProbabilityWeightsSetCreater
		.createBasicProbabilityWeightsSetCreater(mainTranslationRuleProbabilityTable);
	FeatureEnrichedGrammarRuleCreator featureEnrichedGrammarRuleCreator = createFeatureEnrichedGrammarRuleCreator(
		mainTranslationRuleProbabilityTable, phraseProbabilityWeightsSetCreater,
		systemIdentity);
	return new GrammarWriter(mainTranslationRuleProbabilityTable, unknownWordsRulesCreator,
		writePlainHieroRules, featureEnrichedGrammarRuleCreator, systemIdentity);
    }

    public static GrammarWriter createWordEnrichmentSmoothedGrammarWriter(
	    TranslationRuleProbabilityTable mainTranslationRuleProbabilityTable,
	    TranslationRuleProbabilityTable wordEnrichmentSmoothedProbabilityTable,
	    UnknownWordRulesCreator unknownWordsRulesCreator, boolean writePlainHieroRules,
	    SystemIdentity systemIdentity) {
	// int noLexicalPlusPhraseFeatures =
	// mainTranslationRuleProbabilityTable.getNumLexicalPlusPhraseProbabilityFeatures()
	// * 2;
	WordEnrichmentSmoothedProbabilityWeightsSetCreater phraseProbabilityWeightsSetCreater = WordEnrichmentSmoothedProbabilityWeightsSetCreater
		.createWordEnrichmentSmoothedProbabilityWeightsSetCreater(
			mainTranslationRuleProbabilityTable, wordEnrichmentSmoothedProbabilityTable);
	FeatureEnrichedGrammarRuleCreator featureEnrichedGrammarRuleCreator = createFeatureEnrichedGrammarRuleCreator(
		mainTranslationRuleProbabilityTable, phraseProbabilityWeightsSetCreater,
		systemIdentity);

	GrammarWriter result = new GrammarWriter(mainTranslationRuleProbabilityTable,
		unknownWordsRulesCreator, writePlainHieroRules, featureEnrichedGrammarRuleCreator,
		systemIdentity);
	return result;
    }

    private TranslationRuleSignatureTriple createTranslationRuleSignatureTriple(
	    IntegerKeyRuleRepresentationWithLabel sourceRuleSide, CountItem targetCountItem) {
	IntegerKeyRuleRepresentationWithLabel targetRuleSide = targetCountItem.getLabel()
		.restoreRuleRepresentation();
	return TranslationRuleSignatureTriple.createTranslationRuleSignatureTriple(sourceRuleSide,
		targetRuleSide);
    }

    public static FeatureEnrichedGrammarRuleCreator createFeatureEnrichedGrammarRuleCreator(
	    TranslationRuleProbabilityTable mainTranslationRuleProbabilityTable,
	    PhraseProbabilityWeightsSetCreater phraseProbabilityWeightsSetCreater,
	    SystemIdentity systemIdentity) {
	if (systemIdentity.useExtraSamtFeatures()) {
	    if (systemIdentity.useReorderingBinaryFeatures()) {
		return ReorderingFeatureEnrichedGrammarRuleCreater
			.createReorderingFeatureEnrichedGrammarRuleCreater(
				mainTranslationRuleProbabilityTable,
				phraseProbabilityWeightsSetCreater, systemIdentity);
	    } else {
		return StandardFeatureEnrichedGrammarRuleCreater
			.createStandardFeatureEnrichedGrammarRuleCreater(
				mainTranslationRuleProbabilityTable,
				phraseProbabilityWeightsSetCreater, systemIdentity);
	    }
	} else {
	    return BasicFeatureEnrichedGrammarRuleCreator
		    .createBasicFeatureEnrichedGrammarRuleCreator(
			    mainTranslationRuleProbabilityTable,
			    phraseProbabilityWeightsSetCreater, systemIdentity);
	}
    }

    private boolean isNonAbstractRule(IntegerKeyRuleRepresentationWithLabel sourceRuleSide) {
	return sourceRuleSide.hasTerminals(mainTranslationRuleProabilityTable
		.getWordKeyMappingTable());
    }

    private boolean nonAbstractRulesMayHaveXOnLeftHandSide() {
	ReorderLabelingSettings reorderLabelingSettings = this.systemIdentity
		.getReorderLabelingSettings();
	return reorderLabelingSettings.useReorderingLabelExtension()
		&& (!reorderLabelingSettings.reorderLabelHieroRules());
    }

    private boolean isRuleWithAllowedXOnLeftHandSide(boolean writePlainHieroRules,
	    IntegerKeyRuleRepresentationWithLabel sourceRuleSide) {
	return writePlainHieroRules
		|| (nonAbstractRulesMayHaveXOnLeftHandSide() && isNonAbstractRule(sourceRuleSide));
    }

    public boolean labelSourceSide() {
	return this.systemIdentity.labelSourceSide();
    }

    public boolean labelTargetSide() {
	return this.systemIdentity.labelTargetSide();
    }

    private void writeBothSidesLabeledRules(IntegerKeyRuleRepresentationWithLabel sourceRuleSide,
	    RuleCountsTable ruleCountsTable, boolean writeJoshuaRepresentation,
	    boolean writePlainHieroRules, BufferedWriter fileWriter, RuleWriter ruleWriter)
	    throws IOException {
	for (CountItem countItem : ruleCountsTable.getLabeledConditionedSideCountItems()) {
	    ruleWriter.writeRuleIfRightType(writeJoshuaRepresentation, useExtraSamtFeatures(),
		    fileWriter, createTranslationRuleSignatureTriple(sourceRuleSide, countItem),
		    this.featureEnrichedGrammarRuleCreator);
	}
    }

    private void writeSourceSideLabeledTargetSideUnlabeledRules(
	    IntegerKeyRuleRepresentationWithLabel sourceRuleSide, RuleCountsTable ruleCountsTable,
	    boolean writeJoshuaRepresentation, boolean writePlainHieroRules,
	    BufferedWriter fileWriter, RuleWriter ruleWriter) throws IOException {
	for (CountItem countItem : ruleCountsTable.getUnlabeledConditionedSideCountItems()) {
	    ruleWriter.writeRuleIfRightType(writeJoshuaRepresentation, useExtraSamtFeatures(),
		    fileWriter, createTranslationRuleSignatureTriple(sourceRuleSide, countItem),
		    this.featureEnrichedGrammarRuleCreator);
	}
    }

    private void writeLabeledSourceSideRules(IntegerKeyRuleRepresentationWithLabel sourceRuleSide,
	    RuleCountsTable ruleCountsTable, boolean writeJoshuaRepresentation,
	    boolean writePlainHieroRules, BufferedWriter fileWriter, RuleWriter ruleWriter)
	    throws IOException {
	// System.out.println("sourceRuleSide.isSyntacticRuleSide()");
	if (labelTargetSide()) {
	    writeBothSidesLabeledRules(sourceRuleSide, ruleCountsTable, writeJoshuaRepresentation,
		    writePlainHieroRules, fileWriter, ruleWriter);
	} else {
	    writeSourceSideLabeledTargetSideUnlabeledRules(sourceRuleSide, ruleCountsTable,
		    writeJoshuaRepresentation, writePlainHieroRules, fileWriter, ruleWriter);
	}
    }

    private void writeSourceSideUnlabeledTargetSideLabeledRules(
	    IntegerKeyRuleRepresentationWithLabel sourceRuleSide, RuleCountsTable ruleCountsTable,
	    boolean writeJoshuaRepresentation, boolean writePlainHieroRules,
	    BufferedWriter fileWriter, RuleWriter ruleWriter) throws IOException {
	for (CountItem countItem : ruleCountsTable.getLabeledConditionedSideCountItems()) {
	    ruleWriter.writeRuleIfRightType(writeJoshuaRepresentation, useExtraSamtFeatures(),
		    fileWriter, createTranslationRuleSignatureTriple(sourceRuleSide, countItem),
		    this.featureEnrichedGrammarRuleCreator);
	}
    }

    private void writeBothSidesUnlabeledRules(IntegerKeyRuleRepresentationWithLabel sourceRuleSide,
	    RuleCountsTable ruleCountsTable, boolean writeJoshuaRepresentation,
	    boolean writePlainHieroRules, BufferedWriter fileWriter, RuleWriter ruleWriter)
	    throws IOException {
	for (CountItem countItem : ruleCountsTable.getUnlabeledConditionedSideCountItems()) {
	    ruleWriter.writeRuleIfRightType(writeJoshuaRepresentation, useExtraSamtFeatures(),
		    fileWriter, createTranslationRuleSignatureTriple(sourceRuleSide, countItem),
		    this.featureEnrichedGrammarRuleCreator);
	}
    }

    private RuleLabeling getRuleLabelingCanonicalFormLabeledRuleWithHieroFeatures(
	    RuleCountsTable ruleCountsTable, CountItem countItem) {
	WordKeyMappingTable wordKeyMappingTable = mainTranslationRuleProabilityTable
		.getWordKeyMappingTable();
	if (systemIdentity.restrictLabelVariationForCanonicalFormLabeledRules()) {
	    return ruleCountsTable
		    .getMostFrequentRuleLabelingWithinAllowedRuleLabelingsForSourceList(countItem,
			    systemIdentity, wordKeyMappingTable);
	} else {
	    bitg.Pair<MTRuleTrieNode, CountItem> mostFrequentLabeledRule = countItem
		    .getMostFrequentLabeledRuleVersion();
	    return RuleLabeling.createRuleLabeling(wordKeyMappingTable,
		    mostFrequentLabeledRule.first.restoreRuleRepresentation(),
		    mostFrequentLabeledRule.last.getLabel().restoreRuleRepresentation(), -1);
	}
    }
    
    private void assertRestrictedAndUnRestictedRuleLabelingEqual(
	    RuleCountsTable ruleCountsTable, CountItem countItem) {
	WordKeyMappingTable wordKeyMappingTable = mainTranslationRuleProabilityTable
		.getWordKeyMappingTable();
	RuleLabeling ruleLabeling1 =  ruleCountsTable
		    .getMostFrequentRuleLabelingWithinAllowedRuleLabelingsForSourceList(countItem,
			    systemIdentity, wordKeyMappingTable);
	
	    bitg.Pair<MTRuleTrieNode, CountItem> mostFrequentLabeledRule = countItem
		    .getMostFrequentLabeledRuleVersion();
	    RuleLabeling ruleLabeling2 =  RuleLabeling.createRuleLabeling(wordKeyMappingTable,
		    mostFrequentLabeledRule.first.restoreRuleRepresentation(),
		    mostFrequentLabeledRule.last.getLabel().restoreRuleRepresentation(), -1);
	
	Assert.assertTrue(ruleLabeling1.equals(ruleLabeling2));
	System.out.println("ruleLAbeling1" + ruleLabeling1 + " ruleLabeling2 : "+ ruleLabeling2);
	//Assert.fail();
    }

    private void writeCanonicalFormLabeledRuleWithHieroFeatures(
	    IntegerKeyRuleRepresentationWithLabel sourceRuleSide, RuleCountsTable ruleCountsTable,
	    boolean writeJoshuaRepresentation, BufferedWriter fileWriter, RuleWriter ruleWriter,
	    CountItem countItem) throws IOException {

	RuleLabeling ruleLabeling = getRuleLabelingCanonicalFormLabeledRuleWithHieroFeatures(
		ruleCountsTable, countItem);
	
	//assertRestrictedAndUnRestictedRuleLabelingEqual(ruleCountsTable, countItem);

	// We take the unlabeled Hiero rule to provide the features
	TranslationRuleSignatureTriple featuresProvidingTranslationRuleSignatureTriple = createTranslationRuleSignatureTriple(
		sourceRuleSide, countItem);
	//System.out.println("About to write rule...");
	ruleWriter.writeRuleWithLabelsFromFirstAndFeaturesFromSecondRuleIfRightType(
		writeJoshuaRepresentation, useExtraSamtFeatures(), fileWriter, ruleLabeling,
		featuresProvidingTranslationRuleSignatureTriple,
		this.featureEnrichedGrammarRuleCreator);
    }

    private void writeCanonicalFormLabeledRules(
	    IntegerKeyRuleRepresentationWithLabel sourceRuleSide, RuleCountsTable ruleCountsTable,
	    boolean writeJoshuaRepresentation, boolean writePlainHieroRules,
	    BufferedWriter fileWriter, RuleWriter ruleWriter) throws IOException {
	if (!sourceRuleSide.isSyntacticRuleSide()) {
	    for (CountItem countItem : ruleCountsTable.getUnlabeledConditionedSideCountItems()) {

		/**
		 * We are sometimes interested in making a grammar that is
		 * exactly like Hiero except for the labels. By taking the
		 * features from the unlabeled rule and the labels from its
		 * canonical labeled form we achieve this
		 */
		if (systemIdentity.useOnlyHieroWeightsForCanonicalFormLabeledRules()) {
		    writeCanonicalFormLabeledRuleWithHieroFeatures(sourceRuleSide, ruleCountsTable,
			    writeJoshuaRepresentation, fileWriter, ruleWriter, countItem);
		} else {
		    bitg.Pair<MTRuleTrieNode, CountItem> mostFrequentLabeledRule = countItem
			    .getMostFrequentLabeledRuleVersion();

		    // The canonical form labeled rule provides both the labels
		    // and the features
		    // These typically will include the hiero phrase weight
		    // smoothing features
		    ruleWriter.writeRuleIfRightType(
			    writeJoshuaRepresentation,
			    useExtraSamtFeatures(),
			    fileWriter,
			    createTranslationRuleSignatureTriple(
				    mostFrequentLabeledRule.first.restoreRuleRepresentation(),
				    mostFrequentLabeledRule.last),
			    this.featureEnrichedGrammarRuleCreator);
		}
	    }
	}
    }

    private void writeUnlabeledSourceSideRules(
	    IntegerKeyRuleRepresentationWithLabel sourceRuleSide, RuleCountsTable ruleCountsTable,
	    boolean writeJoshuaRepresentation, boolean writePlainHieroRules,
	    BufferedWriter fileWriter, RuleWriter ruleWriter) throws IOException {
	// if ((!sourceRuleSide.isSyntacticRuleSide()) &&
	// (sourceRuleSide.hasTerminals(mainTranslationRuleProabilityTable.getWordKeyMappingTable())))
	// {
	// if ((!sourceRuleSide.isSyntacticRuleSide())) {

	if (labelTargetSide() && (!labelSourceSide())) {
	    writeSourceSideUnlabeledTargetSideLabeledRules(sourceRuleSide, ruleCountsTable,
		    writeJoshuaRepresentation, writePlainHieroRules, fileWriter, ruleWriter);
	}

	if (isRuleWithAllowedXOnLeftHandSide(writePlainHieroRules, sourceRuleSide)) {
	    writeBothSidesUnlabeledRules(sourceRuleSide, ruleCountsTable,
		    writeJoshuaRepresentation, writePlainHieroRules, fileWriter, ruleWriter);
	}
    }

    private void writeHieroGrammarToFile(boolean writeJoshuaRepresentation,
	    boolean writePlainHieroRules, BufferedWriter fileWriter, RuleWriter ruleWriter) {
	System.out.println("ConditionalProbabilityTrie.writeHieroGrammarToFile ...");
	try {
	    Enumeration<Pair<IntegerKeyRuleRepresentationWithLabel, RuleCountsTable>> tableEnumeration = mainTranslationRuleProabilityTable.targetGivenSourceProbabilityTrie
		    .getConditionsEnumeration();

	    while (tableEnumeration.hasMoreElements()) {
		Pair<IntegerKeyRuleRepresentationWithLabel, RuleCountsTable> trieElement = tableEnumeration
			.nextElement();
		IntegerKeyRuleRepresentationWithLabel sourceRuleSide = trieElement.first;
		RuleCountsTable ruleCountsTable = trieElement.last;

		// System.out.println("systemIdentity.useCanonicalFormLabeledRules(): "
		// + systemIdentity.useCanonicalFormLabeledRules());

		if (systemIdentity.useCanonicalFormLabeledRules()) {
		    writeCanonicalFormLabeledRules(sourceRuleSide, ruleCountsTable,
			    writeJoshuaRepresentation, writePlainHieroRules, fileWriter, ruleWriter);
		} else {
		    if (sourceRuleSide.isSyntacticRuleSide()) {
			writeLabeledSourceSideRules(sourceRuleSide, ruleCountsTable,
				writeJoshuaRepresentation, writePlainHieroRules, fileWriter,
				ruleWriter);
		    }

		    if (!sourceRuleSide.isSyntacticRuleSide()) {
			writeUnlabeledSourceSideRules(sourceRuleSide, ruleCountsTable,
				writeJoshuaRepresentation, writePlainHieroRules, fileWriter,
				ruleWriter);
		    }
		}

	    }

	} catch (IOException e) {
	    System.out.println(e);
	    e.printStackTrace();
	}
    }

    private void writeAllChiangRulesToFile(BufferedWriter fileWriter, boolean writePlainHieroRules) {

	RuleWriter ruleWriter = new AllExceptAbstractRuleWriter(this.systemIdentity);
	writeHieroGrammarToFile(true, writePlainHieroRules, fileWriter, ruleWriter);
    }

    public void writeAbstractRulesToFile(BufferedWriter fileWriter, boolean writePlainHieroRules) {

	RuleWriter ruleWriter = new AbstractRuleWriter(this.systemIdentity);
	writeHieroGrammarToFile(true, writePlainHieroRules, fileWriter, ruleWriter);
    }

    private void writeUnknownWordRules(BufferedWriter fileWriter,
	    UnknownWordRulesCreator unknownWordsRulesCreator) throws IOException {
	for (String unknowWordsRule : unknownWordsRulesCreator
		.createUnknownWordsRules(featureEnrichedGrammarRuleCreator)) {
	    fileWriter.write(unknowWordsRule + "\n");
	}
    }

    private void writeReorderingLabelSwitchRules(BufferedWriter fileWriter) throws IOException {
	ReorderingLabelToPhraseLabelSwitchRuleCreater reorderingLabelToPhraseLabelSwitchRuleCreater = ReorderingLabelToPhraseLabelSwitchRuleCreater
		.createReorderingLabelToPhraseLabelSwitchRuleCreater(
			this.mainTranslationRuleProabilityTable.getWordKeyMappingTable(),
			systemIdentity);
	for (String switchRuleString : reorderingLabelToPhraseLabelSwitchRuleCreater
		.createSwitchRules(featureEnrichedGrammarRuleCreator)) {
	    fileWriter.write(switchRuleString + "\n");
	}
    }

    private boolean useExtraSamtFeatures() {
	return this.featureEnrichedGrammarRuleCreator.useExtraSamtFeatures();
    }

    public void writeHieroGrammarToFile(String fileName, boolean writeJoshuaRepresentation) {

	try {
	    BufferedWriter fileWriter = new BufferedWriter(new FileWriter(fileName));
	    writeAllChiangRulesToFile(fileWriter, writePlainHieroRules);
	    writeUnknownWordRules(fileWriter, unknownWordsRulesCreator);

	    if (systemIdentity.isJoshuaSystem()) {
		writeReorderingLabelSwitchRules(fileWriter);
	    }
	    fileWriter.close();
	} catch (IOException e) {
	    System.out.println(e);
	    e.printStackTrace();
	}
    }
    
    public void writeGapLabeConditionalProbabilityTableToFile(String fileName)
    {
	try {
	    BufferedWriter fileWriter = new BufferedWriter(new FileWriter(fileName));
	    String tableString = mainTranslationRuleProabilityTable.getGapLabelConditionalProbabilityTableString();
	    fileWriter.write(tableString);
	    fileWriter.close();
	} catch (IOException e) {
	    System.out.println(e);
	    e.printStackTrace();
	}
    }
    

    private static interface RuleWriter {
	public void writeRuleIfRightType(boolean writeJoshuaRepresentation,
		boolean useExtraSAMTFeatures, BufferedWriter writer,
		TranslationRuleSignatureTriple translationRuleSignatureTriple,
		FeatureEnrichedGrammarRuleCreator featureEnrichedRuleGrammarCreater)
		throws IOException;

	public void writeRuleWithLabelsFromFirstAndFeaturesFromSecondRuleIfRightType(
		boolean writeJoshuaRepresentation, boolean useExtraSAMTFeatures,
		BufferedWriter writer, RuleLabeling ruleLabeling,
		TranslationRuleSignatureTriple featuresProvidingTranslationRuleSignatureTriple,
		FeatureEnrichedGrammarRuleCreator featureEnrichedRuleGrammarCreater)
		throws IOException;
    }

    private static abstract class BasicRuleWriter implements RuleWriter {
	protected final SystemIdentity systemIdentity;

	protected BasicRuleWriter(SystemIdentity systemIdentity) {
	    this.systemIdentity = systemIdentity;
	}

	private String createRuleString(boolean writeJoshuaRepresentation,
		boolean useExtraSAMTFeatures, BufferedWriter fileWriter,
		TranslationRuleSignatureTriple translationRuleSignatureTriple,
		FeatureEnrichedGrammarRuleCreator featureEnrichedRuleGrammarCreater,
		boolean writeLabeledFeaturesRepresentation) {
	    String ruleString = "";
	    if (writeJoshuaRepresentation) {
		// System.out.println(">>>> sourceRuleSide: " +
		// sourceRuleSide.getStringRepresentation(wordKeyMappingTable)
		// + " targetRuleSide: "
		// +
		// targetRuleSide.getStringRepresentation(wordKeyMappingTable));
		ruleString = featureEnrichedRuleGrammarCreater
			.createFeatureEnrichedGrammarRuleRepresentation(
				translationRuleSignatureTriple, useExtraSAMTFeatures,
				writeLabeledFeaturesRepresentation);
	    } else {
		ruleString = featureEnrichedRuleGrammarCreater
			.createBasicFeaturesGrammarRuleRepresentation(
				translationRuleSignatureTriple, useExtraSAMTFeatures);
	    }
	    return ruleString;
	}
	
	private String createRelabeledRuleString(boolean writeJoshuaRepresentation,
		boolean useExtraSAMTFeatures, BufferedWriter fileWriter,
		TranslationRuleSignatureTriple translationRuleSignatureTriple,
		FeatureEnrichedGrammarRuleCreator featureEnrichedRuleGrammarCreater,
		boolean writeLabeledFeaturesRepresentation,RuleLabeling ruleLabeling) {
	    String ruleString = "";
	    if (writeJoshuaRepresentation) {
		// System.out.println(">>>> sourceRuleSide: " +
		// sourceRuleSide.getStringRepresentation(wordKeyMappingTable)
		// + " targetRuleSide: "
		// +
		// targetRuleSide.getStringRepresentation(wordKeyMappingTable));
		ruleString = featureEnrichedRuleGrammarCreater
			.createFeatureEnrichedGrammarRuleRepresentationRelabeled(
				translationRuleSignatureTriple, useExtraSAMTFeatures,
				writeLabeledFeaturesRepresentation,ruleLabeling);
	    } else {
		throw new RuntimeException("Error: GrammarWriter.createRelabeledRuleString - not implemented for writeJoshuaRepresentation = " + writeJoshuaRepresentation);
	    }
	    return ruleString;
	}   
	
	private String getRuleWordsAndLabelsPart(boolean writeJoshuaRepresentation,
		boolean useExtraSAMTFeatures, BufferedWriter fileWriter, RuleLabeling ruleLabeling,
		TranslationRuleSignatureTriple featuresProvidingtranslationRuleSignatureTriple,
		FeatureEnrichedGrammarRuleCreator featureEnrichedRuleGrammarCreater,
		boolean writeLabeledFeaturesRepresentation) {
	    String ruleStringLabels = createRelabeledRuleString(writeJoshuaRepresentation,
		    useExtraSAMTFeatures, fileWriter, featuresProvidingtranslationRuleSignatureTriple,
		    featureEnrichedRuleGrammarCreater, writeLabeledFeaturesRepresentation,ruleLabeling);
	    String[] ruleForLabelsParts = JoshuaStyle.spitOnRuleSeparator(ruleStringLabels);

	    String result = ruleForLabelsParts[0] + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR
		    + ruleForLabelsParts[1] + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR
		    + ruleForLabelsParts[2];
	    return result;
	}

	private String getRuleFeaturePart(boolean writeJoshuaRepresentation,
		boolean useExtraSAMTFeatures, BufferedWriter fileWriter,
		TranslationRuleSignatureTriple featuresProvidingtranslationRuleSignatureTriple,
		FeatureEnrichedGrammarRuleCreator featureEnrichedRuleGrammarCreater,
		boolean writeLabeledFeaturesRepresentation) {
	    String ruleStringFeatures = createRuleString(writeJoshuaRepresentation,
		    useExtraSAMTFeatures, fileWriter,
		    featuresProvidingtranslationRuleSignatureTriple,
		    featureEnrichedRuleGrammarCreater, writeLabeledFeaturesRepresentation);

	    String[] ruleForFeaturesParts = JoshuaStyle.spitOnRuleSeparator(ruleStringFeatures);
	    String featuresString = ruleForFeaturesParts[ruleForFeaturesParts.length - 1];
	    return featuresString;
	}

	private String createRuleStringWithLabelsFirstAndFeaturesSecondRule(
		boolean writeJoshuaRepresentation, boolean useExtraSAMTFeatures,
		BufferedWriter fileWriter, RuleLabeling ruleLabeling,
		TranslationRuleSignatureTriple featuresProvidingtranslationRuleSignatureTriple,
		FeatureEnrichedGrammarRuleCreator featureEnrichedRuleGrammarCreater,
		boolean writeLabeledFeaturesRepresentation) {

	    String combinedRule = getRuleWordsAndLabelsPart(writeJoshuaRepresentation,
		    useExtraSAMTFeatures, fileWriter, ruleLabeling,
		    featuresProvidingtranslationRuleSignatureTriple,
		    featureEnrichedRuleGrammarCreater, writeLabeledFeaturesRepresentation)
		    + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR
		    + getRuleFeaturePart(writeJoshuaRepresentation, useExtraSAMTFeatures,
			    fileWriter, featuresProvidingtranslationRuleSignatureTriple,
			    featureEnrichedRuleGrammarCreater, writeLabeledFeaturesRepresentation);
	    return combinedRule;
	}

	public void writeRule(boolean writeJoshuaRepresentation, boolean useExtraSAMTFeatures,
		BufferedWriter fileWriter,
		TranslationRuleSignatureTriple translationRuleSignatureTriple,
		FeatureEnrichedGrammarRuleCreator featureEnrichedRuleGrammarCreater,
		boolean writeLabeledFeaturesRepresentation) throws IOException {
	    // System.out.println("write rule");
	    fileWriter.write(createRuleString(writeJoshuaRepresentation, useExtraSAMTFeatures,
		    fileWriter, translationRuleSignatureTriple, featureEnrichedRuleGrammarCreater,
		    writeLabeledFeaturesRepresentation)
		    + "\n");
	}

	public void writeRuleWithLabelsFromFirstAndFeaturesFromSecondRule(
		boolean writeJoshuaRepresentation, boolean useExtraSAMTFeatures,
		BufferedWriter fileWriter, RuleLabeling ruleLabeling,
		TranslationRuleSignatureTriple featuresProvidingTranslationRuleSignatureTriple,
		FeatureEnrichedGrammarRuleCreator featureEnrichedRuleGrammarCreater,
		boolean writeLabeledFeaturesRepresentation) throws IOException {
	    // System.out.println("write rule");
	    
	    String ruleRepresentation = createRuleStringWithLabelsFirstAndFeaturesSecondRule(
		    writeJoshuaRepresentation, useExtraSAMTFeatures, fileWriter, ruleLabeling,
		    featuresProvidingTranslationRuleSignatureTriple,
		    featureEnrichedRuleGrammarCreater, writeLabeledFeaturesRepresentation)
		    + "\n"; 
	    //System.out.println(">>>>>Writing rule: " + ruleRepresentation);
	    fileWriter.write(ruleRepresentation);
	}
    }

    static class AllRuleWriter extends BasicRuleWriter {

	protected AllRuleWriter(SystemIdentity systemIdentity) {
	    super(systemIdentity);
	}

	@Override
	public void writeRuleIfRightType(boolean writeJoshuaRepresentation,
		boolean useExtraSAMTFeatures, BufferedWriter fileWriter,
		TranslationRuleSignatureTriple translationRuleSignatureTriple,
		FeatureEnrichedGrammarRuleCreator featureEnrichedRuleGrammarCreater)
		throws IOException {
	    writeRule(writeJoshuaRepresentation, useExtraSAMTFeatures, fileWriter,
		    translationRuleSignatureTriple, featureEnrichedRuleGrammarCreater, true);
	}

	@Override
	public void writeRuleWithLabelsFromFirstAndFeaturesFromSecondRuleIfRightType(
		boolean writeJoshuaRepresentation, boolean useExtraSAMTFeatures,
		BufferedWriter writer, RuleLabeling ruleLabeling,
		TranslationRuleSignatureTriple featuresProvidingTranslationRuleSignatureTriple,
		FeatureEnrichedGrammarRuleCreator featureEnrichedRuleGrammarCreater)
		throws IOException {
	    writeRuleWithLabelsFromFirstAndFeaturesFromSecondRule(writeJoshuaRepresentation,
		    useExtraSAMTFeatures, writer, ruleLabeling,
		    featuresProvidingTranslationRuleSignatureTriple,
		    featureEnrichedRuleGrammarCreater, true);
	}
    }

    static class AbstractRuleWriter extends BasicRuleWriter {

	protected AbstractRuleWriter(SystemIdentity systemIdentity) {
	    super(systemIdentity);
	}

	@Override
	public void writeRuleIfRightType(boolean writeJoshuaRepresentation,
		boolean useExtraSAMTFeatures, BufferedWriter fileWriter,
		TranslationRuleSignatureTriple translationRuleSignatureTriple,
		FeatureEnrichedGrammarRuleCreator featureEnrichedRuleGrammarCreater)
		throws IOException {
	    if (featureEnrichedRuleGrammarCreater.isAbstractRule(translationRuleSignatureTriple)) {
		writeRule(writeJoshuaRepresentation, useExtraSAMTFeatures, fileWriter,
			translationRuleSignatureTriple, featureEnrichedRuleGrammarCreater, false);
	    } else {
		// System.out.println("Wrong type of rule");
	    }
	}

	@Override
	public void writeRuleWithLabelsFromFirstAndFeaturesFromSecondRuleIfRightType(
		boolean writeJoshuaRepresentation, boolean useExtraSAMTFeatures,
		BufferedWriter writer, RuleLabeling ruleLabeling,
		TranslationRuleSignatureTriple featuresProvidingTranslationRuleSignatureTriple,
		FeatureEnrichedGrammarRuleCreator featureEnrichedRuleGrammarCreater)
		throws IOException {
	    if (featureEnrichedRuleGrammarCreater
		    .isAbstractRule(featuresProvidingTranslationRuleSignatureTriple)) {
		writeRuleWithLabelsFromFirstAndFeaturesFromSecondRule(writeJoshuaRepresentation,
			useExtraSAMTFeatures, writer, ruleLabeling,
			featuresProvidingTranslationRuleSignatureTriple,
			featureEnrichedRuleGrammarCreater, false);
	    } else {
		// System.out.println("Wrong type of rule");
	    }

	}
    }

    static class AllExceptAbstractRuleWriter extends BasicRuleWriter {

	protected AllExceptAbstractRuleWriter(SystemIdentity systemIdentity) {
	    super(systemIdentity);
	}

	@Override
	public void writeRuleIfRightType(boolean writeJoshuaRepresentation,
		boolean useExtraSAMTFeatures, BufferedWriter fileWriter,
		TranslationRuleSignatureTriple translationRuleSignatureTriple,
		FeatureEnrichedGrammarRuleCreator featureEnrichedRuleGrammarCreater)
		throws IOException {
	    if (!featureEnrichedRuleGrammarCreater.isAbstractRule(translationRuleSignatureTriple)) {
		writeRule(writeJoshuaRepresentation, useExtraSAMTFeatures, fileWriter,
			translationRuleSignatureTriple, featureEnrichedRuleGrammarCreater,
			this.systemIdentity.usePackedGrammars());
	    } else {
		// System.out.println("Wrong type of rule");
	    }
	}

	@Override
	public void writeRuleWithLabelsFromFirstAndFeaturesFromSecondRuleIfRightType(
		boolean writeJoshuaRepresentation, boolean useExtraSAMTFeatures,
		BufferedWriter writer, RuleLabeling ruleLabeling,
		TranslationRuleSignatureTriple featuresProvidingTranslationRuleSignatureTriple,
		FeatureEnrichedGrammarRuleCreator featureEnrichedRuleGrammarCreater)
		throws IOException {
	    if (!featureEnrichedRuleGrammarCreater
		    .isAbstractRule(featuresProvidingTranslationRuleSignatureTriple)) {
		writeRuleWithLabelsFromFirstAndFeaturesFromSecondRule(writeJoshuaRepresentation,
			useExtraSAMTFeatures, writer, ruleLabeling,
			featuresProvidingTranslationRuleSignatureTriple,
			featureEnrichedRuleGrammarCreater, this.systemIdentity.usePackedGrammars());
	    } else {
		// System.out.println("Wrong type of rule");
	    }

	}
    }

}