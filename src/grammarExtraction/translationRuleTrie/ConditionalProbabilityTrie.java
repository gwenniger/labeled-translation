package grammarExtraction.translationRuleTrie;

import grammarExtraction.IntegerKeyRuleRepresentationWithLabel;
import grammarExtraction.translationRuleTrie.CountItem.LabeledRuleWithCount;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import bitg.Pair;

public abstract class ConditionalProbabilityTrie implements Serializable {
	private static final long serialVersionUID = 1L;
	WordKeyMappingTable wordKeyMappingTable;
	MTRuleTrieNodeRoot conditionalProbabilityTrie;
	MTRuleTrieNodeRoot conditionedElementsRepresentationTrie;
	private double totalCountAllRules;

	protected ConditionalProbabilityTrie(MTRuleTrieNodeRoot conditionalProbabilityTrie, WordKeyMappingTable wordKeyMappingTable, MTRuleTrieNodeRoot conditionedElementsRepresentationTrie) {
		this.conditionalProbabilityTrie = conditionalProbabilityTrie;
		this.wordKeyMappingTable = wordKeyMappingTable;
		this.conditionedElementsRepresentationTrie = conditionedElementsRepresentationTrie;
	}

	protected void computeTotalCounts() {
		Enumeration<RuleCountsTable> ruleCountsTableEnumeration = TrieRuleCountsTableEnumeration.createTrieRuleCountsTableEnumeration(this.conditionalProbabilityTrie);

		double totalCount = 0;
		while (ruleCountsTableEnumeration.hasMoreElements()) {
			RuleCountsTable ruleCountsTable = ruleCountsTableEnumeration.nextElement();
			double totalCountForCondition = ruleCountsTable.getTotalCountAllRules();
			totalCount += totalCountForCondition;
		}
		this.totalCountAllRules = totalCount;
	}

	public double getTotalCountAllRules() {
		return this.totalCountAllRules;
	}

	protected Enumeration<Pair<IntegerKeyRuleRepresentationWithLabel, RuleCountsTable>> getConditionsEnumeration() {
		return TrieElementsEnumeration.createTrieElementsEnumeration(this.conditionalProbabilityTrie);
	}

	protected List<IntegerKeyRuleRepresentationWithLabel> getConditionsSortedByLevelUncertaintyDescending() {
		List<IntegerKeyRuleRepresentationWithLabel> result = new ArrayList<IntegerKeyRuleRepresentationWithLabel>();

		List<Pair<IntegerKeyRuleRepresentationWithLabel, Double>> numberedIntegerKeyRuleRepresentationWithLabels = new ArrayList<Pair<IntegerKeyRuleRepresentationWithLabel, Double>>();

		Enumeration<Pair<IntegerKeyRuleRepresentationWithLabel, RuleCountsTable>> tableEnumeration = getConditionsEnumeration();

		while (tableEnumeration.hasMoreElements()) {
			Pair<IntegerKeyRuleRepresentationWithLabel, RuleCountsTable> trieElement = tableEnumeration.nextElement();

			// only consider tries that have a table with non-empty counts
			if (trieElement.last.getTotalCountAllRules() > 0) {
				double entropy = trieElement.last.getEntropy();
				// Take the negative entropy to sort descending
				numberedIntegerKeyRuleRepresentationWithLabels.add(new Pair<IntegerKeyRuleRepresentationWithLabel, Double>(trieElement.first, -entropy));
			}
		}

		numberedIntegerKeyRuleRepresentationWithLabels.sort(new NumberedIntegerKeyRuleRepresentationComparator());

		for (Pair<IntegerKeyRuleRepresentationWithLabel, Double> numberedIntegerKeyRuleRepresentation : numberedIntegerKeyRuleRepresentationWithLabels) {
			result.add(numberedIntegerKeyRuleRepresentation.first);
		}
		return result;
	}

	public String getStringRuleRepresentation(IntegerKeyRuleRepresentationWithLabel conditioned, IntegerKeyRuleRepresentationWithLabel condition, double probability) {
		String result = "P(" + conditioned.getStringRepresentation(wordKeyMappingTable) + "|" + condition.getStringRepresentation(wordKeyMappingTable) + ") = " + probability;
		return result;
	}

	public void printTable() {
		System.out.println("======================== \n<Conditional prbability table>");
		for (IntegerKeyRuleRepresentationWithLabel condition : getConditionsSortedByLevelUncertaintyDescending()) {
			System.out.println("------------------");
			System.out.println("Probabilities for P( ? | " + condition.getStringRepresentation(wordKeyMappingTable) + ") : ");
			System.out.println("Entropy for condition: " + this.conditionalProbabilityTrie.getEntropy(condition));

			RuleCountsTable ruleCountsTable = this.conditionalProbabilityTrie.getRuleCountsTable(condition);

			double totalProbability = 0;
			for (CountItem countItem : ruleCountsTable.getAllCountItems()) {
				IntegerKeyRuleRepresentationWithLabel conditioned = countItem.getLabel().restoreRuleRepresentation();
				double probability = ruleCountsTable.getProbability(conditioned, conditionedElementsRepresentationTrie);
				System.out.println(getStringRuleRepresentation(conditioned, condition, probability));

				if (countItem instanceof LexicalWeightCountItem) {
					double lw1 = ((LexicalWeightCountItem) countItem).getLexicalWeightSourceGivenTarget();
					double lw2 = ((LexicalWeightCountItem) countItem).getLexicalWeightTargetGivenSource();
					System.out.println("Lexical weight source-to-target: " + lw1 + " Lexical weight target-to-source:" + lw2);
				}

				totalProbability += probability;
			}
			// Assert.assertEquals(1, totalProbability, 0.0000001);
			System.out.println("Total: " + totalProbability);
			System.out.println("-----------------");
		}
		System.out.println("======================== \n</Conditional prbability table>");
	}

	public void writeTableToFile(String fileName) {
		try {
			BufferedWriter fileWriter = new BufferedWriter(new FileWriter(fileName));

			for (IntegerKeyRuleRepresentationWithLabel conditionRuleSide : getConditionsSortedByLevelUncertaintyDescending()) {
				RuleCountsTable ruleCountsTable = this.conditionalProbabilityTrie.getRuleCountsTable(conditionRuleSide);

				for (CountItem countItem : ruleCountsTable.getAllCountItems()) {
					IntegerKeyRuleRepresentationWithLabel conditionedRuleSide = countItem.getLabel().restoreRuleRepresentation();
					double conditionToConditionedProbability = this.conditionalProbabilityTrie.getRuleConditionalProbability(conditionRuleSide, conditionedRuleSide,
							this.conditionedElementsRepresentationTrie);
					fileWriter.write(getStringRuleRepresentation(conditionedRuleSide, conditionRuleSide, conditionToConditionedProbability) + "\n");
				}
			}
			fileWriter.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected double getConditionToConditionedProbability(IntegerKeyRuleRepresentationWithLabel conditionRuleSide, IntegerKeyRuleRepresentationWithLabel conditionedRuleSide) {
		return conditionalProbabilityTrie.getRuleConditionalProbability(conditionRuleSide, conditionedRuleSide, this.conditionedElementsRepresentationTrie);
	}

	protected double getConditionedToConditionProbability(IntegerKeyRuleRepresentationWithLabel conditionRuleSide, IntegerKeyRuleRepresentationWithLabel conditionedRuleSide) {
		return conditionedElementsRepresentationTrie.getRuleConditionalProbability(conditionedRuleSide, conditionRuleSide, this.conditionalProbabilityTrie);
	}

	protected util.Pair<Double> getRuleLexicalProbabilities(IntegerKeyRuleRepresentationWithLabel conditionRuleSide, IntegerKeyRuleRepresentationWithLabel conditionedRuleSide) {
		return conditionalProbabilityTrie.getRuleLexicalProbabilities(conditionRuleSide, conditionedRuleSide, this.conditionedElementsRepresentationTrie);
	}

	public double getRulePhraseEntropy(IntegerKeyRuleRepresentationWithLabel conditionRuleRepresentation) {
		return this.conditionalProbabilityTrie.getRuleCountsTable(conditionRuleRepresentation).getEntropy();
	}

	public double getConditionSideProbability(IntegerKeyRuleRepresentationWithLabel conditionRuleRepresentation) {
		return getRelativeGenerativeProbability(this.conditionalProbabilityTrie.getRuleCountsTable(conditionRuleRepresentation).getTotalCountUnlabeldConditionedSideRules());
	}

	public double getConditionConditionedPairProbability(IntegerKeyRuleRepresentationWithLabel condition, IntegerKeyRuleRepresentationWithLabel conditioned) {
		double conditionConditionedCount = this.conditionalProbabilityTrie.getRuleCount(condition, conditioned, this.conditionedElementsRepresentationTrie);
		return getRelativeGenerativeProbability(conditionConditionedCount);
	}

	protected double getConditionTotalCount(IntegerKeyRuleRepresentationWithLabel conditionRuleRepresentation, boolean useNonHieroLabels) {
		if (useNonHieroLabels) {
			return this.conditionalProbabilityTrie.getRuleCountsTable(conditionRuleRepresentation).getTotalCountLabelRefinedConditionedSideRules();

		} else {
			return this.conditionalProbabilityTrie.getRuleCountsTable(conditionRuleRepresentation).getTotalCountUnlabeldConditionedSideRules();
		}

	}

	public double getRuleCount(IntegerKeyRuleRepresentationWithLabel condition, IntegerKeyRuleRepresentationWithLabel conditioned) {
		double conditionConditionedCount = this.conditionalProbabilityTrie.getRuleCount(condition, conditioned, this.conditionedElementsRepresentationTrie);
		return conditionConditionedCount;
	}

	private double getRelativeGenerativeProbability(double conditionCount) {
		return conditionCount / this.getTotalCountAllRules();
	}

	public int getNumRules() {
		return this.conditionalProbabilityTrie.getTotalNumRules();
	}
	

	/**
	 * Add the labeled version to the list of labeled versions of the unlabeled rule
	 * Warning: this method is not thread safe and should be executed single-threaded at the 
	 * end of the tie filling
	 * @param unlabeledSourceRepresentation
	 * @param unlabeledTargetRepresentation
	 * @param labeledSourceRepresentation
	 * @param countItemLabeled
	 */
	public void addLabeledRuleToListLabeledVersionsUnlabeledRule(IntegerKeyRuleRepresentationWithLabel  unlabeledSourceRepresentation, IntegerKeyRuleRepresentationWithLabel  unlabeledTargetRepresentation,
			IntegerKeyRuleRepresentationWithLabel labeledSourceRepresentation, CountItem countItemLabeled) {
		MTRuleTrieNode ruleRepresentingLeafNode = this.conditionalProbabilityTrie.getRuleSideRepresentingLeafNode(labeledSourceRepresentation);
		LabeledRuleWithCount labeledRuleWithCount = new LabeledRuleWithCount(ruleRepresentingLeafNode, countItemLabeled);
		//System.out.println("unlabeledSourceRepresentation: " + unlabeledSourceRepresentation.getStringRepresentation(wordKeyMappingTable));
		this.conditionalProbabilityTrie.addLabeledRuleToListLabeledVersionsUnlabeledRule(unlabeledSourceRepresentation,unlabeledTargetRepresentation, this.conditionedElementsRepresentationTrie, labeledRuleWithCount, wordKeyMappingTable);
	}
	
	/**
	 * Add the rule labeling to the RuleCountsTable belonging to the unlabeled source side
	 * Warning: this method is not thread safe and should be executed single-threaded at the 
	 * end of the tie filling
	 * @param unlabeledSourceRepresentation
	 * @param unlabeledTargetRepresentation
	 * @param labeledSourceRepresentation
	 * @param countItemLabeled
	 */
	public void addRuleLabeling(IntegerKeyRuleRepresentationWithLabel  unlabeledSourceRepresentation, IntegerKeyRuleRepresentationWithLabel  unlabeledTargetRepresentation,
		IntegerKeyRuleRepresentationWithLabel labeledSourceRepresentation, CountItem countItemLabeled) {
	    RuleLabeling ruleLabeling = RuleLabeling.createRuleLabeling(wordKeyMappingTable, labeledSourceRepresentation, countItemLabeled.getLabel().restoreRuleRepresentation(), countItemLabeled.getCount());
	    RuleCountsTable unlabeledSourceRuleCountsTable = this.conditionalProbabilityTrie.getRuleCountsTable(unlabeledSourceRepresentation);
	    unlabeledSourceRuleCountsTable.addRuleLablingToTable(ruleLabeling);
	}

}
