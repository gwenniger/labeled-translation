package grammarExtraction.translationRuleTrie;

import grammarExtraction.IntegerKeyRuleRepresentationWithLabel;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.translationRuleTrie.CountItem.LabeledRuleWithCount;
import grammarExtraction.translationRuleTrie.RuleCountsSubTable.RuleCountsSubTableLabeledSourceSide;
import grammarExtraction.translationRuleTrie.RuleCountsSubTable.RuleCountsSubTableUnlabeledSourceSide;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import util.ObjectFactory;
import util.Pair;

public class RuleCountsTable implements ObjectFactory, Serializable {
	private static final long serialVersionUID = 1L;

	private final RuleCountsSubTableUnlabeledSourceSide unlabeledConditionedSideRulesTable;
	private final RuleCountsSubTableLabeledSourceSide labeledConditionedSideRulesTable;

	protected RuleCountsTable(RuleCountsSubTableUnlabeledSourceSide unlabeledConditionedSideRulesTable, RuleCountsSubTableLabeledSourceSide labeledConditionedSideRulesTable) {
		this.unlabeledConditionedSideRulesTable = unlabeledConditionedSideRulesTable;
		this.labeledConditionedSideRulesTable = labeledConditionedSideRulesTable;
	}

	public static RuleCountsTable createRuleCountsTable() {
		return new RuleCountsTable(RuleCountsSubTable.createRuleCountsSubTableUnlabeledSourceSide(), RuleCountsSubTable.createRuleCountsSubTableLabeledSourceSide());
	}
	
	public static RuleCountsTableWithRuleLabelingInfo createRuleCountsTableWithRuleLabelingInfo(){
	    return new RuleCountsTableWithRuleLabelingInfo(RuleCountsSubTable.createRuleCountsSubTableUnlabeledSourceSide(), RuleCountsSubTable.createRuleCountsSubTableLabeledSourceSide(),
		    RuleLabelingTable.createRuleLabelingTable());
	}

	/*
	 * public CountItem getCountItemLinearSearch(MTRuleTrieNode targetRuleLeafNode) { for (CountItem countItem : this.getCountItems()) { //
	 * System.out.println("RuleCountsTable.getCountItemLinearSearch.countItem: " // + countItem); if (countItem.getLabel().equals(targetRuleLeafNode)) { return
	 * countItem; } } return null; }
	 */

	private boolean isBasicChiangRule(IntegerKeyRuleRepresentationWithLabel targetRuleRepresentation) {
		return WordKeyMappingTable.isBasicChiangRuleLabelKey(targetRuleRepresentation.getLeftHandSideLabel());
	}

	void addCount(IntegerKeyRuleRepresentationWithLabel targetRuleRepresentation, CountUpdateItem countUpdateItem, MTRuleTrieNode targetTrie, CountItemCreater countItemCreater) {
		getRuleCountsSubTableForTargetRuleRepresentation(targetRuleRepresentation).addCount(targetRuleRepresentation, countUpdateItem, targetTrie, countItemCreater);
	}

	void addCountWithoutIntroducingNewConditionedSide(IntegerKeyRuleRepresentationWithLabel targetRuleRepresentation, CountUpdateItem countUpdateItem, MTRuleTrieNode targetTrie,
			CountItemCreater countItemCreater) {
		getRuleCountsSubTableForTargetRuleRepresentation(targetRuleRepresentation)
				.addCountWithoutIntroducingNewConditionedSide(targetRuleRepresentation, countUpdateItem, targetTrie, countItemCreater);
	}

	private double computeTotalCountLabelRefinededConditionedSideRules() {
		return this.labeledConditionedSideRulesTable.computeSaveAndGetTotalCounts();
	}

	private double computeTotalCountUnlabeldConditionedSideRules() {
		return this.unlabeledConditionedSideRulesTable.computeSaveAndGetTotalCounts();
	}

	public double getTotalCountUnlabeldConditionedSideRules() {
		double result = this.unlabeledConditionedSideRulesTable.getTotalCount();
		if (result <= 0) {
			return computeTotalCountUnlabeldConditionedSideRules();
		}
		return result;
	}

	public double getTotalCountLabelRefinedConditionedSideRules() {
		double result = this.labeledConditionedSideRulesTable.getTotalCount();
		if (result <= 0) {
			return computeTotalCountLabelRefinededConditionedSideRules();
		}
		return result;
	}

	public double getTotalCountAllRules() {
		double result = getTotalCountLabelRefinedConditionedSideRules() + getTotalCountUnlabeldConditionedSideRules();
		return result;
	}

	public double getCount(IntegerKeyRuleRepresentationWithLabel targetRuleRepresentation, MTRuleTrieNode targetTrie) {
		return this.getRuleCountsSubTableForTargetRuleRepresentation(targetRuleRepresentation).getCount(targetRuleRepresentation, targetTrie);
	}

	public int getNumElements() {
		return this.labeledConditionedSideRulesTable.getNumElements() + this.unlabeledConditionedSideRulesTable.getNumElements();
	}

	private RuleCountsSubTable getRuleCountsSubTableForTargetRuleRepresentation(IntegerKeyRuleRepresentationWithLabel targetRuleRepresentation) {
		if (isBasicChiangRule(targetRuleRepresentation)) {
			return this.unlabeledConditionedSideRulesTable;
		} else {
			return this.labeledConditionedSideRulesTable;
		}
	}

	public double getProbability(IntegerKeyRuleRepresentationWithLabel targetRuleRepresentation, MTRuleTrieNode targetTrie) {
		return getRuleCountsSubTableForTargetRuleRepresentation(targetRuleRepresentation).getProbability(targetRuleRepresentation, targetTrie);
	}

	public Pair<Double> getRuleLexicalProbabilities(IntegerKeyRuleRepresentationWithLabel targetRuleRepresentation, MTRuleTrieNode targetTrie) {
		return getRuleCountsSubTableForTargetRuleRepresentation(targetRuleRepresentation).getRuleLexicalProbabilities(targetRuleRepresentation, targetTrie);
	}

	public void addLabeledRuleToListLabeledVersionsUnlabeledRule(IntegerKeyRuleRepresentationWithLabel unlabeledTargetRuleRepresentation,MTRuleTrieNode targetTrie, LabeledRuleWithCount labeledRuleWithCount) {
		RuleCountsSubTable containingSubTable = getRuleCountsSubTableForTargetRuleRepresentation(unlabeledTargetRuleRepresentation);
		containingSubTable.addLabeledRule(unlabeledTargetRuleRepresentation,targetTrie,labeledRuleWithCount);
	}

	public double getEntropy() {
		if ((this.unlabeledConditionedSideRulesTable.totalAnonymousRulesCount != 0) || (this.labeledConditionedSideRulesTable.totalAnonymousRulesCount != 0)) {
			throw new RuntimeException("Error trying to get entropy in a rule Counts table with non zero counts of anonymous rules");
		}
		return EntropyComputer.computeEntropy(this.unlabeledConditionedSideRulesTable.getProbabilities()) + EntropyComputer.computeEntropy(this.labeledConditionedSideRulesTable.getProbabilities());
	}

	@Override
	public Object makeObject() {
		return new RuleCountsTable(RuleCountsSubTable.createRuleCountsSubTableUnlabeledSourceSide(), RuleCountsSubTable.createRuleCountsSubTableLabeledSourceSide());
	}

	public static RuleCountsTable makeObjectStatic() {
		return new RuleCountsTable(RuleCountsSubTable.createRuleCountsSubTableUnlabeledSourceSide(), RuleCountsSubTable.createRuleCountsSubTableLabeledSourceSide());
	}

	public RuleCountsTableConditionedSidesEnumeration getUnlabeledConditionedSideRuleCountsTableConditionedSidesEnumeration() {
		return this.unlabeledConditionedSideRulesTable.getRuleCountsTableConditionedSidesEnumeration();
	}

	public String toString() {
		String result = this.unlabeledConditionedSideRulesTable.toString() + this.labeledConditionedSideRulesTable.toString();
		return result;
	}

	public String getCountItemsString() {
		return this.unlabeledConditionedSideRulesTable.getCountItemsString() + this.labeledConditionedSideRulesTable.getCountItemsString();
	}

	public List<CountItem> getUnlabeledConditionedSideCountItems() {
		return this.unlabeledConditionedSideRulesTable.getCountItems();
	}

	public List<CountItem> getLabeledConditionedSideCountItems() {
		return this.labeledConditionedSideRulesTable.getCountItems();
	}

	public List<CountItem> getAllCountItems() {
		List<CountItem> result = new ArrayList<CountItem>();
		result.addAll(this.unlabeledConditionedSideRulesTable.getCountItems());
		result.addAll(this.labeledConditionedSideRulesTable.getCountItems());
		return result;
	}
	
	public void addRuleLablingToTable(RuleLabeling ruleLabeling) {
	    throw new RuntimeException("Not implemented");
	}
	
	public RuleLabeling getMostFrequentRuleLabelingWithinAllowedRuleLabelingsForSourceList(CountItem countItem,SystemIdentity systemIdentity,WordKeyMappingTable wordKeyMappingTable){
	    throw new RuntimeException("Not implemented");
	}
	
	public static class RuleCountsTableWithRuleLabelingInfo extends RuleCountsTable{

		private static final long serialVersionUID = 1L;
		private final RuleLabelingTable ruleLabelingTable;

		protected RuleCountsTableWithRuleLabelingInfo(RuleCountsSubTableUnlabeledSourceSide unlabeledConditionedSideRulesTable, 
			RuleCountsSubTableLabeledSourceSide labeledConditionedSideRulesTable,RuleLabelingTable ruleLabelingTable) {
			super(unlabeledConditionedSideRulesTable,labeledConditionedSideRulesTable);
			this.ruleLabelingTable = ruleLabelingTable;
		}


		@Override
		public void addRuleLablingToTable(RuleLabeling ruleLabeling) {
		    this.ruleLabelingTable.addRuleLablingToTable(ruleLabeling);
		}
		
		public RuleLabeling getMostFrequentRuleLabelingWithinAllowedRuleLabelingsForSourceList(CountItem countItem,SystemIdentity systemIdentity,WordKeyMappingTable wordKeyMappingTable){
		    return countItem.getMostFrequentRuleLabelingWithinAllowedRuleLabelingsForSourceList(this.ruleLabelingTable.getOrComputeAndGetMostFrequentRuleLabelingsList(systemIdentity. numAllowedTypesPerLabelForSource()),wordKeyMappingTable);
		}
		
		@Override
		public Object makeObject() {
			RuleLabelingTable ruleLabelingTable = RuleLabelingTable.createRuleLabelingTable();
			return new  RuleCountsTableWithRuleLabelingInfo(RuleCountsSubTable.createRuleCountsSubTableUnlabeledSourceSide(), RuleCountsSubTable.createRuleCountsSubTableLabeledSourceSide(),ruleLabelingTable);
		}
	}
}
