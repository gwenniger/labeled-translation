package grammarExtraction.translationRuleTrie;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.translationRules.TranslationRuleFeatures;

public interface GlueFeaturesCreater {

	public TranslationRuleFeatures geGlueRuleFeatures(SystemIdentity systemIdentity, double generativeGlueLabelProbability, boolean isHieroRule,
			double rarityPenalty);

}
