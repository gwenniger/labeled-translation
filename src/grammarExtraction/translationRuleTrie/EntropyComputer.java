package grammarExtraction.translationRuleTrie;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class EntropyComputer {
    public static double computeEntropy(Collection<Double> probabilities) {
	double result = 0;
	for (double probability : probabilities) {
	    double term = probability * Math.log(probability);
	    // System.out.println("probability: " + probability +
	    // " Entropy term: " + term);
	    result += term;
	}

	result = -result;

	// System.out.println("Entropy: " + result);
	return result;
    }

    private static Collection<Double> createUniformProbabilitiesCollection(
	    int numPossibilitiesToChooseFrom) {
	Collection<Double> probabilities = new ArrayList<Double>();

	double uniformProbabilityEachChoice = 1.0 / numPossibilitiesToChooseFrom;
	for (int i = 0; i < numPossibilitiesToChooseFrom; i++) {
	    probabilities.add(uniformProbabilityEachChoice);
	}
	return probabilities;
    }

    public static double computeMaximumEntropyForUniformDistribution(
	    int numPossibilitiesToChooseFrom) {
	return computeEntropy(createUniformProbabilitiesCollection(numPossibilitiesToChooseFrom));
    }

    public static void main(String[] args) {
	System.out.println("Maximal entropy with five choices: "
		+ computeMaximumEntropyForUniformDistribution(5));
	
	System.out.println("Maximal entropy with seven choices: "
		+ computeMaximumEntropyForUniformDistribution(7));
    }
}
