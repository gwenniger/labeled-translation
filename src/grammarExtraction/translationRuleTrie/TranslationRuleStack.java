package grammarExtraction.translationRuleTrie;

import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TranslationRuleStack 
{
	private List<LightWeightPhrasePairRuleSet> translationRuleSetList;
	
	private TranslationRuleStack(List<LightWeightPhrasePairRuleSet> translationRuleSetList)
	{
		this.translationRuleSetList = translationRuleSetList;
	}

	public static TranslationRuleStack createTranslationRuleStack()
	{
		List<LightWeightPhrasePairRuleSet> translationRuleList =  Collections.synchronizedList(new ArrayList<LightWeightPhrasePairRuleSet>());
		return new TranslationRuleStack(translationRuleList); 
	}
	
	public synchronized void addToStack(List<LightWeightPhrasePairRuleSet> translationRuleSetList)
	{
		this.translationRuleSetList.addAll(translationRuleSetList);
	}
	
	public synchronized List<LightWeightPhrasePairRuleSet> popEntireStack()
	{
		List<LightWeightPhrasePairRuleSet> result = this.translationRuleSetList;
		this.translationRuleSetList = Collections.synchronizedList(new ArrayList<LightWeightPhrasePairRuleSet>());
		return result;
	}

}


