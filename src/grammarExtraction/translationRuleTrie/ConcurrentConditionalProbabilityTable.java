
package grammarExtraction.translationRuleTrie;


import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

import util.Pair;

public class ConcurrentConditionalProbabilityTable<t>  implements Serializable
{
	private static final long serialVersionUID = 1L;
	protected ConcurrentMap<t,ConcurrentMap<t,ProbabilityTableItem>> probabilityTable; 
	private final AtomicInteger totalAddedCount;
	
	protected ConcurrentConditionalProbabilityTable(ConcurrentMap<t,ConcurrentMap<t,ProbabilityTableItem>> probabilityTable)
	{
		this.probabilityTable = probabilityTable;
		this.totalAddedCount = new AtomicInteger();
	}
	
	private void addEntryTableIfNotExisting(t conditionKey)
	{
		this.probabilityTable.putIfAbsent(conditionKey, new ConcurrentHashMap<t, ProbabilityTableItem>());
		assert(this.getConditionTable(conditionKey) != null);
	}
	
	
	private void addEntryIfNotExisting(Pair<t> conditionConditionedPair)
	{
		ConcurrentMap<t,ProbabilityTableItem> entryTable = this.probabilityTable.get(conditionConditionedPair.getFirst());
		
		assert(this.getConditionTable(conditionConditionedPair.getFirst()) != null);
				
		entryTable.putIfAbsent(conditionConditionedPair.getSecond(), new ProbabilityTableItem());
	}
	
	
	public Map<t,ProbabilityTableItem> getConditionTable(t conditionKey)
	{
		return this.probabilityTable.get(conditionKey);
	}
	
	private ProbabilityTableItem getProbabilityTableItem(Pair<t> conditionConditionedPair)
	{
		if((conditionConditionedPair.getFirst() == null) || ((conditionConditionedPair.getSecond()) == null))
		{
			return null;
		}
		
		ConcurrentMap<t,ProbabilityTableItem> map; 
		if((map = this.probabilityTable.get(conditionConditionedPair.getFirst())) != null)
		{
			return map.get(conditionConditionedPair.getSecond());
		}
		return null;
		
		
	}
	
	public double getProbability(Pair<t> conditionConditionedPair)
	{
		ProbabilityTableItem item; 
		
		if((item = getProbabilityTableItem(conditionConditionedPair)) != null)
		{
			return item.getProbability();	
		}	
		return (double)0;
	}
	
	public int getCount(Pair<t> conditionConditionedPair)
	{
		ProbabilityTableItem item;
		
		if((item = getProbabilityTableItem(conditionConditionedPair)) != null)
		{
			return item.getCount();	
		}
		
		return 0;
	}
	
	public void incrementCount(Pair<t> conditionConditionedPair)
	{
		addEntryTableIfNotExisting(conditionConditionedPair.getFirst());
		addEntryIfNotExisting(conditionConditionedPair);
		this.getProbabilityTableItem(conditionConditionedPair).incrementCount();
		this.totalAddedCount.incrementAndGet();
	}
	
	
	private int computeTotalCountForCondition(t conditionKey)
	{
		int result = 0;
		for(ProbabilityTableItem item : this.getConditionTable(conditionKey).values())
		{
			result += item.getCount();
		}
		return result;
	}
	
	private void computeProbabilitiesForCondition(t conditionKey)
	{
		int totalCount = computeTotalCountForCondition(conditionKey);
		
		for(t conditionedKey : this.getConditionTable(conditionKey).keySet())
		{
			ProbabilityTableItem conditionConditionedPairItem = this.getProbabilityTableItem(new Pair<t>(conditionKey,conditionedKey)); 
			int pairCount = conditionConditionedPairItem.getCount();
			double pairProbability = ((double)pairCount) / totalCount;
			conditionConditionedPairItem.setProbability(pairProbability);
		}
	}
	
	private double getTotalPobabilityForCondition(t conditionKey)
	{
		double totalProbability = 0;
		
		for(t conditionedKey : this.getConditionTable(conditionKey).keySet())
		{
			totalProbability +=  this.getProbability(new Pair<t>(conditionKey, conditionedKey));
		}
		return totalProbability;
	}
	
	private boolean totalProbabilityForConditionIsOne(t conditionKey)
	{
		return (Math.abs((getTotalPobabilityForCondition(conditionKey) - 1)) < 0.00001);
	}
	
	
	public void computeProbabilitiesFromCounts()
	{
		for(t conditionKey : this.probabilityTable.keySet())
		{
			computeProbabilitiesForCondition(conditionKey);
		}
		
		if(!allProbabilitiesOverSameConditionAddToOne())
		{
			System.out.println("Error: he probabilities that were computed do not sum up to one");
			System.exit(0);
		}
		else
		{
			System.out.println("OK, computed probabilities sum up to one");
		}
	}
	
	public boolean allProbabilitiesOverSameConditionAddToOne()
	{
		for(t conditionKey : this.probabilityTable.keySet())
		{
			if(!totalProbabilityForConditionIsOne(conditionKey))
			{
				return false;
			}
		}
		return true;
	}
	
	
	
	private int computeTotalTableCount()
	{
		int result = 0;
		
		for(ConcurrentMap<t,ProbabilityTableItem> table : this.probabilityTable.values())
		{
			for(ProbabilityTableItem item: table.values())
			{
				result += item.getCount();
			}
		}
		return result;
	}
	
	public boolean tableCountIsAddedCount()
	{
		int tableCount = computeTotalTableCount();
		int addedCount = this.totalAddedCount.intValue();
		System.out.println("ConcurrentConditionalProbabilityTable tableCount: " + tableCount + " addedCount: " + addedCount);
		return (tableCount == addedCount );
	}	
	
}
