package grammarExtraction.translationRuleTrie;

import grammarExtraction.IntegerKeyRuleRepresentationWithLabel;
import grammarExtraction.grammarExtractionFoundation.LabelingSettings.CanonicalFormSettings;
import grammarExtraction.translationRuleTrie.CountItem.LabeledRuleWithCount;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import arraySkipList.ArraySkipList;
import util.Locking;
import util.ObjectFactory;
import util.Pair;

/**
 * Warning : This class currently uses explicit locks. So do not use synchronized as well at the same time, as this can potentially result in deadlock!!!
 * 
 * Why not just use synchronized: The site http://sourceforge.net/apps/trac/lilith/wiki/SynchronizedVsFairLock shows that with multi-core computers may lead to
 * serious starvation of threads. Indeed I ran the program and can confirm its results. It seems then, despite the overhead involved, a good idea to use
 * Reentrance locks with fair policy instead.
 */

public class MTRuleTrieNode extends TrieNode implements ObjectFactory, Serializable {
	private static final long serialVersionUID = 1L;
	private static final boolean UseFairLockingStrategy = false; // Without this
																	// threads seem
																	// to get
																	// starved
	protected static final IntLabelTrieNodeComparator LabelBasedChildComparator = new IntLabelTrieNodeComparator();
	private MTRuleTrieNode parentTrieNode;
	protected RuleCountsTable countsTable;

	private final ReentrantLock lock = new ReentrantLock(UseFairLockingStrategy);

	@SuppressWarnings("unchecked")
	protected MTRuleTrieNode(ArraySkipList<? extends TrieNode> childNodes, Integer label, MTRuleTrieNode parentTrieNode, RuleCountsTable countsTable) {
		super((ArraySkipList<TrieNode>) childNodes, label);
		this.parentTrieNode = parentTrieNode;
		this.countsTable = countsTable;

	}

	public static MTRuleTrieNode createMTRuleTrieNode(Integer label, MTRuleTrieNode parentTrieNode) {
		assert (label >= 0);
		return new MTRuleTrieNode(null, label, parentTrieNode, null);
	}

	protected MTRuleTrieNode createSearchNode(Integer label) {
		return new MTRuleTrieNode(null, label, null, null);
	}

	public void addTranslationRuleToTrie(IntegerKeyRuleRepresentationWithLabel sourceRuleRepresentation, IntegerKeyRuleRepresentationWithLabel targetRuleRepresentation, MTRuleTrieNode targetTrie) {
		assert (sourceRuleRepresentation.getPartKeys().size() <= 10);
		this.addOrRetrieveRuleRepresentation(sourceRuleRepresentation, 0);
	}

	@SuppressWarnings("unchecked")
	private void createChildNodesListIfNotPresent() {
		if (this.childNodesSkipList == null) {
			ArraySkipList<? extends TrieNode> childNodesSkipList = ArraySkipList.createArraySkipList();
			this.childNodesSkipList = (ArraySkipList<TrieNode>) childNodesSkipList;
		}
	}

	@Override
	/**
	 * This method gets a child with a certain sourceKey if it is present in this node.
	 * If the source key is not present it will be inserted.
	 * @param sourceKey The source key
	 * @return 
	 */
	// replaced synchronized with reentrance lock
	// protected synchronized TrieNode
	// getOrInsertAndGetChildSynchronized(Integer sourceKey) {
	protected TrieNode getOrInsertAndGetChildSynchronized(Integer sourceKey) {

		acquireLockWitmMaxWaitTimeWarning();
		try {
			MTRuleTrieNode result;
			createChildNodesListIfNotPresent();
			if (sourceKey < 0) {
				throw new RuntimeException("MTRuleTrieNode.getOrInsertAndGetChild Error: trying to get a child for an invalid source key. Valid source keys are 0 or bigger");
			}

			result = MTRuleTrieNode.createMTRuleTrieNode(sourceKey, this);

			MTRuleTrieNode oldNode = (MTRuleTrieNode) this.childNodesSkipList.putIfAbsent(sourceKey, result);

			if (oldNode != null) {
				result = oldNode;
				return result;
			}
			assert (this.childNodesSkipList.putIfAbsent(sourceKey, result) != null);
			return result;
		} finally {
			// This code will always be called at the last moment, even if
			// return is called
			releaseLock();
		}

	}

	protected void acquireLockWitmMaxWaitTimeWarning() {
		Locking.acquireLockWitmMaxWaitTimeWarning(lock, Locking.MinWaitTimeLockWarning, this, "MTRuleTrieNode");
	}

	protected void releaseLock() {
		lock.unlock();
	}

	@Override
	/**
	 * This method gets a child with a certain sourceKey if it is present in this node.
	 * Otherwise null is returned.
	 * @param sourceKey The source key
	 * @return 
	 */
	protected TrieNode getChildWithLabel(Integer sourceKey) {
		if (childNodesSkipList != null) {
			return childNodesSkipList.get(sourceKey);
		}
		// System.out.println("childNodesSkipList == null)");
		return null;
	}

	/**
	 * 
	 * @param sourceRuleRepresentation
	 * @param currentSourceIndex
	 * @return
	 */
	// Replaced synchronized with reentrance lock
	private RuleCountsTable getRuleCountsTable(IntegerKeyRuleRepresentationWithLabel sourceRuleRepresentation, int currentSourceIndex) {

		acquireLockWitmMaxWaitTimeWarning();
		try {
			int sourceKey = sourceRuleRepresentation.getKey(currentSourceIndex);
			MTRuleTrieNode child = (MTRuleTrieNode) getChildWithLabel(sourceKey);

			if (child == null) {
				return null;
			}

			if (sourceRuleRepresentation.isLastIndex(currentSourceIndex)) {
				return child.countsTable;
			} else {
				return child.getRuleCountsTable(sourceRuleRepresentation, currentSourceIndex + 1);
			}
		} finally {
			releaseLock();
		}
	}

	public RuleCountsTable getRuleCountsTable(IntegerKeyRuleRepresentationWithLabel sourceRuleRepresentation) {
		return getRuleCountsTable(sourceRuleRepresentation, 0);
	}
	
	
	/**
	 * 	Method that returns the leaf node that represents an
	 *  IntegerKeypRuleRepresentationLabeled in the Trie 
	 *  
	 * @param sourceRuleRepresentation
	 * @param currentSourceIndex
	 * @return
	 */
	private MTRuleTrieNode getRuleSideRepresentingLeafNode(IntegerKeyRuleRepresentationWithLabel sourceRuleRepresentation, int currentSourceIndex) {

		acquireLockWitmMaxWaitTimeWarning();
		try {
			int sourceKey = sourceRuleRepresentation.getKey(currentSourceIndex);
			MTRuleTrieNode child = (MTRuleTrieNode) getChildWithLabel(sourceKey);

			if (child == null) {
				return null;
			}

			if (sourceRuleRepresentation.isLastIndex(currentSourceIndex)) {
				return child;
			} else {
				return child.getRuleSideRepresentingLeafNode(sourceRuleRepresentation, currentSourceIndex + 1);
			}
		} finally {
			releaseLock();
		}
	}

	public MTRuleTrieNode getRuleSideRepresentingLeafNode(IntegerKeyRuleRepresentationWithLabel sourceRuleRepresentation) {
		return getRuleSideRepresentingLeafNode(sourceRuleRepresentation, 0);
	}

	public double getRuleCount(IntegerKeyRuleRepresentationWithLabel sourceRuleRepresentation, IntegerKeyRuleRepresentationWithLabel targetRuleRepresentation, MTRuleTrieNode targetTrie) {
		RuleCountsTable ruleCountsTable = getRuleCountsTable(sourceRuleRepresentation, 0);

		if (ruleCountsTable != null) {
			return ruleCountsTable.getCount(targetRuleRepresentation, targetTrie);
		}
		throw new RuntimeException("sourceRuleRepresentation not present in Trie");
	}

	public double getRuleConditionalProbability(IntegerKeyRuleRepresentationWithLabel sourceRuleRepresentation, IntegerKeyRuleRepresentationWithLabel targetRuleRepresentation,
			MTRuleTrieNode targetTrie) {
		RuleCountsTable ruleCountsTable = getRuleCountsTable(sourceRuleRepresentation, 0);

		assert (ruleCountsTable != null);

		if (ruleCountsTable != null) {
			return ruleCountsTable.getProbability(targetRuleRepresentation, targetTrie);
		}
		throw new RuntimeException("sourceRuleRepresentation not present in Trie");
	}

	public double getRuleConditionalProbability(String sourceSideRuleString, String targetSideRuleString, MTRuleTrieNode targetTrie, WordKeyMappingTable wordKeyMappingTable) {
		IntegerKeyRuleRepresentationWithLabel sourceRuleRepresentation = IntegerKeyRuleRepresentationWithLabel.createIntegerKeyRuleRepresentationWithLabel(sourceSideRuleString, wordKeyMappingTable);
		IntegerKeyRuleRepresentationWithLabel targetRuleRepresentation = IntegerKeyRuleRepresentationWithLabel.createIntegerKeyRuleRepresentationWithLabel(targetSideRuleString, wordKeyMappingTable);
		;
		return getRuleConditionalProbability(sourceRuleRepresentation, targetRuleRepresentation, targetTrie);
	}

	public Pair<Double> getRuleLexicalProbabilities(IntegerKeyRuleRepresentationWithLabel sourceRuleRepresentation, IntegerKeyRuleRepresentationWithLabel targetRuleRepresentation,
			MTRuleTrieNode targetTrie) {
		RuleCountsTable ruleCountsTable = getRuleCountsTable(sourceRuleRepresentation, 0);

		if (ruleCountsTable != null) {
			return ruleCountsTable.getRuleLexicalProbabilities(targetRuleRepresentation, targetTrie);
		}
		throw new RuntimeException("sourceRuleRepresentation not present in Trie");
	}

	public double getEntropy(IntegerKeyRuleRepresentationWithLabel sourceRuleRepresentation) {
		RuleCountsTable ruleCountsTable = getRuleCountsTable(sourceRuleRepresentation, 0);

		if (ruleCountsTable != null) {
			return ruleCountsTable.getEntropy();
		}

		throw new RuntimeException("sourceRuleRepresentation not present in Trie");
	}

	public void addLabeledRuleToListLabeledVersionsUnlabeledRule(IntegerKeyRuleRepresentationWithLabel sourceRuleRepresentation, IntegerKeyRuleRepresentationWithLabel targetRuleRepresentation, MTRuleTrieNode targetTrie,
			LabeledRuleWithCount labeledRuleWithCount,WordKeyMappingTable wordKeyMappingTable) {
		RuleCountsTable ruleCountsTable = getRuleCountsTable(sourceRuleRepresentation);

		assert (ruleCountsTable != null);

		if (ruleCountsTable != null) {
			ruleCountsTable.addLabeledRuleToListLabeledVersionsUnlabeledRule(targetRuleRepresentation,targetTrie,labeledRuleWithCount);
		}
		else
		{	
			throw new RuntimeException("Errror MTRuleTrieNode.addLabeledRuleToListLabeledVersionsUnlabeledRule :  sourceRuleRepresentation - " + sourceRuleRepresentation.getStringRepresentation(wordKeyMappingTable) + " - not present in Trie");
		}	
	}

	/**
	 * Add a rule labeling to the RuleCountsTable of the unlabeled source rule side
	 * @param unlabeledSourceRuleRepresentation
	 * @param ruleLabeling
	 */
	public void addRuleLabeling(IntegerKeyRuleRepresentationWithLabel unlabeledSourceRuleRepresentation,RuleLabeling ruleLabeling) {
	    RuleCountsTable ruleCountsTable = getRuleCountsTable(unlabeledSourceRuleRepresentation);
	    ruleCountsTable.addRuleLablingToTable(ruleLabeling);
	}
	
	private void createTableIfNullSynchronized(CanonicalFormSettings canonicalFormSettings) {
		acquireLockWitmMaxWaitTimeWarning();
		try {
			if (this.countsTable == null) {
			    	if(canonicalFormSettings.restrictLabelVariationForCanonicalFormLabeledRules()){    
			    	    this.countsTable = RuleCountsTable.createRuleCountsTableWithRuleLabelingInfo();
			    	}
			    	else{
			    	    this.countsTable = RuleCountsTable.createRuleCountsTable();
			    	}
			}
		} finally {
			releaseLock();
		}
	}

	// This is no longer synchronized as countsTable.addCount is already
	// synchronized, so this is not necessary
	// I.e. we can continue "using" the node "for other purposes" while the
	// countsTable is being updated independently
	protected void addRuleCountToTable(IntegerKeyRuleRepresentationWithLabel conditionedRuleRepresentation, CountUpdateItem countUpdateItem,

	MTRuleTrieNode targetTrie, CountItemCreater countItemCreater, CanonicalFormSettings canonicalFormSettings) {

		createTableIfNullSynchronized(canonicalFormSettings);
		countsTable.addCount(conditionedRuleRepresentation, countUpdateItem, targetTrie, countItemCreater);
	}

	protected void addRuleCountToTableAnomouslyIfConditionedRuleRepresentationNotYetPresent(IntegerKeyRuleRepresentationWithLabel conditionedRuleRepresentation, CountUpdateItem countUpdateItem,

	MTRuleTrieNode targetTrie, CountItemCreater countItemCreater,CanonicalFormSettings canonicalFormSettings) {

		createTableIfNullSynchronized(canonicalFormSettings);
		countsTable.addCountWithoutIntroducingNewConditionedSide(conditionedRuleRepresentation, countUpdateItem, targetTrie, countItemCreater);
	}

	public RuleCountsTable getcountsTable() {
		return countsTable;
	}

	public boolean hasValue() {
		return (countsTable != null);
	}

	public TrieElementsEnumeration getTrieElementsEnumeration() {
		return TrieElementsEnumeration.createTrieElementsEnumeration(this);
	}

	public int getNumNodesRecursive() {
		int result = 1;

		if (this.childNodesSkipList != null) {
			for (TrieNode child : this.childNodesSkipList.values()) {
				if (child != null) {
					result += ((MTRuleTrieNode) child).getNumNodesRecursive();
				}
			}
		}

		return result;
	}

	public IntegerKeyRuleRepresentationWithLabel restoreRuleRepresentation() {
		List<Integer> sourceKeys = new ArrayList<Integer>();

		MTRuleTrieNode trieNode = this;

		// Incrementally collect a list of trieNode Labels until arrived at the
		// root
		while (trieNode.label != -1) {
			sourceKeys.add(trieNode.label);
			trieNode = trieNode.parentTrieNode;
		}

		// Reverse the sourceKeys that have been collected in reverse order
		Collections.reverse(sourceKeys);
		return IntegerKeyRuleRepresentationWithLabel.createIntegerKeyRuleRepresentationWithLabel(sourceKeys);
	}

	@Override
	public Object makeObject() {
		RuleCountsTable ruleCountsTable = RuleCountsTable.makeObjectStatic();
		ArraySkipList<? extends TrieNode> childNodes = ArraySkipList.createArraySkipList();
		return new MTRuleTrieNode(childNodes, -1, null, ruleCountsTable);
	}

	public String toString() {
		String result = "\n<TrieNode>";
		result += " Node Label: " + this.label + " ";
		result += "NumChildren: " + this.getNumChildren();

		if (this.childNodesSkipList != null) {
			String childLabels = "\t ChildLabels: ";
			for (TrieNode child : getChildNodes()) {
				childLabels += child.getLabel() + " ";
			}
			result += childLabels;
		}

		result += "CountsTable: " + this.countsTable;
		result += "</TrieNode>";
		return result;
	}

	public String toStringWithoutCountTable() {
		String result = "\n<TrieNode>";
		result += " Node Label: " + this.label + " ";
		result += "NumChildren: " + this.getNumChildren();

		if (this.childNodesSkipList != null) {
			String childLabels = "\t ChildLabels: ";
			for (TrieNode child : getChildNodes()) {
				childLabels += child.getLabel() + " ";
			}
			result += childLabels;
		}
		result += "</TrieNode>";
		return result;
	}

	public int getNumRealChildren() {
		return this.getNumChildren();
	}

	@Override
	public TrieNode getChildWithIndex(int index) {
		List<MTRuleTrieNode> childrenList = new ArrayList<MTRuleTrieNode>();
		for (TrieNode t : this.childNodesSkipList.values()) {
			childrenList.add((MTRuleTrieNode) t);
		}
		return childrenList.get(index);
	}

}
