package grammarExtraction.translationRuleTrie;


/**
 * This interface takes care of CountItemCreaterion.
 * We define two methods :  createCountItemUnlabeledRuleSourceSide
 * and createCountItemLabeledRuleSourceSide.
 * The motivation is that in case of the unlabeled rule source side we need possibly different
 * additional information (in the case of working with canonical rule representations)
 * but this information is in any case not needed for CountItems corresponding to a 
 * unlabeled rule source side. 
 * This adds some extra complexity but can save a lot of memory, by not creating more complex 
 * CountItem instances than required 
 * (i.e. we only create LexicalWeightCountItemWithLabeledRuleInformation were the 
 *  capacity to store the labeled rule information is needed, that is in CountItems 
 *  belonging to an unlabeled rule source side).
 * 
 * 
 * @author Gideon Maillette de Buy Wenniger 
 *
 */
public interface CountItemCreater 
{
	public CountItem createCountItemUnlabeledRuleSourceSide(MTRuleTrieNode label);
	public CountItem createCountItemLabeledRuleSourceSide(MTRuleTrieNode label);	
}
