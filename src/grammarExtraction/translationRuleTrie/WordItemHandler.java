package grammarExtraction.translationRuleTrie;

import java.io.Serializable;

public class WordItemHandler extends WordKeyMappingTabbleItemHandler implements Serializable {
	private static final long serialVersionUID = 1L;

	protected WordItemHandler(WordKeyMappingTableData wordKeyMappingTableData, int writeIndex, int maxNumLabelTypes) {
		super(wordKeyMappingTableData, writeIndex, maxNumLabelTypes);
	}

	@Override
	protected Integer getFullyDowngradedKeyRepresentation(Integer key) {
		return key;
	}

	@Override
	protected String getItemHandlerName() {
		return "WordItemHandler";
	}

	@Override
	protected Integer getSourceSideLabelsOnlyDowngradedKeyRepresentation(Integer key) {
		return key;
	}

	@Override
	protected Integer getTargetSideLabelsOnlyDowngradedKeyRepresentation(Integer key) {
		return key;
	}

	@Override
	public String getLeftHandSideLabelForAnyLabelIndex(Integer key){
	    throw new RuntimeException("Not implemented for Word ItemHandler: getLeftHandSideLabelIndexForAnyLabelIndex");
	}
}
