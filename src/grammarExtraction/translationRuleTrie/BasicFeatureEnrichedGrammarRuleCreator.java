package grammarExtraction.translationRuleTrie;

import grammarExtraction.chiangGrammarExtraction.RuleFeaturesBasic;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsSet;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsSetCreater;
import grammarExtraction.translationRules.LightWeightTranslationRuleWithFeatures;
import grammarExtraction.translationRules.TranslationRuleFeatures;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

public class BasicFeatureEnrichedGrammarRuleCreator extends FeatureEnrichedGrammarRuleCreator {

	private BasicFeatureEnrichedGrammarRuleCreator(TranslationRuleProbabilityTable mainRuleProbabilityTable,
			PhraseProbabilityWeightsSetCreater phraseProbabilityWeightsSetCreater, SystemIdentity systemIdentity) {
		super(mainRuleProbabilityTable, phraseProbabilityWeightsSetCreater, systemIdentity);
	}

	public static BasicFeatureEnrichedGrammarRuleCreator createBasicFeatureEnrichedGrammarRuleCreator(TranslationRuleProbabilityTable mainRuleProbabilityTable,
			PhraseProbabilityWeightsSetCreater phraseProbabilityWeightsSetCreater, SystemIdentity systemIdentity) {
		return new BasicFeatureEnrichedGrammarRuleCreator(mainRuleProbabilityTable, phraseProbabilityWeightsSetCreater, systemIdentity);
	}

	@Override
	protected String getHieroJoshuaRuleRepresentation(TranslationRuleSignatureTriple translationRuleSignatureTriple,
			PhraseProbabilityWeightsSet phraseProbabilityWeightsSet, util.Pair<Double> lexicalProbabilities, double ruleCorpusCount,
			boolean useExtraSAMTFeatures, boolean writeLabeledFeaturesRepresentation,double ruleGivenLabelGenerativeProbability) {

		LightWeightTranslationRuleWithFeatures ruleWithFeatures;
		boolean isAbstractRule = translationRuleSignatureTriple.isAbstractRule(mainRuleProbabilityTable.getWordKeyMappingTable());
		ruleWithFeatures = LightWeightTranslationRuleWithFeatures.createLightWeightTranslationRuleWithBasicFeatures(translationRuleSignatureTriple,
				phraseProbabilityWeightsSet, lexicalProbabilities.getSecond(), lexicalProbabilities.getFirst(), isAbstractRule,systemIdentity.getFeatureValueFormatter());
		ruleWithFeatures = returnScaleProbabilitiesRuleIfMosesUnknownWordsRule(ruleWithFeatures);
		String result = ruleWithFeatures.getNormalRuleRepresentation(mainRuleProbabilityTable.wordKeyMappingTable,systemIdentity.getTranslationRuleRepresentationCreater());

		return result;
	}

	@Override
	public boolean useExtraSamtFeatures() {
		return false;
	}

	@Override
	public TranslationRuleFeatures getUnknownWordsTranslationRuleFeatures(SystemIdentity systemIdentity, boolean isHieroRule) {
		return RuleFeaturesBasic.createCorrectSizeUnknnownWordRuleRuleFeaturesBasic(systemIdentity);
	}

	@Override
	public TranslationRuleFeatures getReorderingLabelToPhraseLabelSwitchRuleFeatures(SystemIdentity systemIdentity) {
		return RuleFeaturesBasic.createCorrectSizeReorderingLabelToPhraseLabelSwitchRuleFeaturesBasic(systemIdentity);
	}

	@Override
	public TranslationRuleFeatures geGlueRuleFeatures(SystemIdentity systemIdentity, double generativeGlueLabelProbability, boolean isHieroRule,
			double rarityPenalty) {
		return RuleFeaturesBasic.createCorrectSizeGlueRuleFeaturesBasic(systemIdentity);
	}
}
