package grammarExtraction.translationRuleTrie;

import java.util.List;


public class BasicCountItem extends CountItem {

	
	private static final long serialVersionUID = 1L;

	public BasicCountItem() {
		this(null, 0);
	}

	protected BasicCountItem(MTRuleTrieNode label, double count) {
		super(label, count);
	}

	public BasicCountItem(MTRuleTrieNode label) {
		super(label, 0);
	}

	@Override
	public Object makeObject() {
		return new BasicCountItem(null);
	}
	
	@Override 
	public CountItem createCountItemUnlabeledRuleSourceSide(MTRuleTrieNode label)
	{
		return new BasicCountItem(label,0);
	}
	
	
	@Override
	public CountItem createCountItemLabeledRuleSourceSide(MTRuleTrieNode label) {
		return new BasicCountItem(label,0);
	}
	

	@Override
	public void performCountUpdate(CountUpdateItem countUpdateItem) {
		// add the count to add to compute the new count
		count += countUpdateItem.getCount();
	}

	@Override
	public bitg.Pair<MTRuleTrieNode, CountItem> getMostFrequentLabeledRuleVersion() {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public void addLabeledRule(LabeledRuleWithCount labeledRuleWithCount) {
		throw new RuntimeException("Not implemented");
	}

	@Override
	public RuleLabeling getMostFrequentRuleLabelingWithinAllowedRuleLabelingsForSourceList(
		List<RuleLabeling> mostFrequentRuleLabelingsForSourceList,
		WordKeyMappingTable wordKeyMappingTable) {
	    throw new RuntimeException("Not implemented");
	}

}
