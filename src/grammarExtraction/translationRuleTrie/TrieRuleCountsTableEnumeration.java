package grammarExtraction.translationRuleTrie;


import java.util.Enumeration;

public class TrieRuleCountsTableEnumeration implements Enumeration<RuleCountsTable>
{

	TrieElementsEnumeration backboneEnumeration;
	
	
	private TrieRuleCountsTableEnumeration(TrieElementsEnumeration backboneEnumeration)
	{
		this.backboneEnumeration = backboneEnumeration;
	}
	
	public static  TrieRuleCountsTableEnumeration createTrieRuleCountsTableEnumeration(MTRuleTrieNode rootNode)
	{
		TrieElementsEnumeration backboneEnumeration = TrieElementsEnumeration.createTrieElementsEnumeration(rootNode);
		return new TrieRuleCountsTableEnumeration(backboneEnumeration);
	}

	public boolean hasMoreElements() 
	{
		return backboneEnumeration.hasMoreElements();
	}

	@Override
	public RuleCountsTable nextElement() 
	{
		return backboneEnumeration.nextElement().last;
	}
	
	
}
