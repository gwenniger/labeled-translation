package grammarExtraction.translationRuleTrie;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MostFrequentRuleLabelingListCompter {

    private final List<RuleLabeling> frequencySortedRuleLabelingList;
    private List<Set<Integer>> sourceLabelSetList;
    private List<Set<Integer>> targetLabelSetList;
    private final int maxNumberDifferentValuesPerLabel;

    private MostFrequentRuleLabelingListCompter(List<RuleLabeling> frequencySortedRuleLabelingList,
	    int maxNumberDifferentValuesPerLabel) {
	this.frequencySortedRuleLabelingList = frequencySortedRuleLabelingList;
	this.maxNumberDifferentValuesPerLabel = maxNumberDifferentValuesPerLabel;
    }

    public static MostFrequentRuleLabelingListCompter createMostFrequentRuleLabelingListCompter(
	    List<RuleLabeling> ruleLabelingList, int maxNumberDifferentValuesPerLabel) {
	return new MostFrequentRuleLabelingListCompter(
		computeFrequencySortedRuleLabelingList(ruleLabelingList),
		maxNumberDifferentValuesPerLabel);
    }

    private static List<RuleLabeling> computeFrequencySortedRuleLabelingList(
	    List<RuleLabeling> ruleLabelingList) {
	List<RuleLabeling> frequencySortedRuleLabelingList = new ArrayList<RuleLabeling>(
		ruleLabelingList);
	frequencySortedRuleLabelingList.sort(Collections.reverseOrder(RuleLabeling.createRuleLabelingFrequencyComparator()));
	return frequencySortedRuleLabelingList;
    }

    /**
     * Create a List of Set<Integer> elements, in order to keep track of the
     * number of different rule label alternatives that has occurred for each
     * label There can be maximally 3 rule labels: LHS + 2 Gaps
     * 
     * @return
     */
    private static List<Set<Integer>> createEmptyRuleLabelingSetList() {
	List<Set<Integer>> result = new ArrayList<Set<Integer>>();
	for (int i = 0; i < 3; i++) {
	    result.add(new HashSet<Integer>());
	}
	return result;
    }

    private void addLabelIndicesToLabelSetList(List<Integer> labelIndices,
	    List<Set<Integer>> labelSetList) {
	for (int i = 0; i < labelIndices.size(); i++) {
	    labelSetList.get(i).add(labelIndices.get(i));
	}
    }

    private void addRuleLabelingToLabelSetLists(RuleLabeling ruleLabeling) {
	List<Integer> sourceLabelIndices = ruleLabeling.getSourceLabelIndices();
	addLabelIndicesToLabelSetList(sourceLabelIndices, sourceLabelSetList);
	List<Integer> targetLabelIndices = ruleLabeling.getTargetLabelIndices();
	addLabelIndicesToLabelSetList(targetLabelIndices, targetLabelSetList);
    }

    private boolean maxNumberDifferentValuesPerLabelExceeded(List<Set<Integer>> labelSetList) {
	for (Set<Integer> set : labelSetList) {
	    if (set.size() > this.maxNumberDifferentValuesPerLabel) {
		return true;
	    }
	}
	return false;
    }

    private boolean maxNumberDifferentValuesPerLabelExceeded() {
	return maxNumberDifferentValuesPerLabelExceeded(sourceLabelSetList)
		|| maxNumberDifferentValuesPerLabelExceeded(targetLabelSetList);
    }

    private void initialilizeSourceAndTargetLabelSetList() {
	this.sourceLabelSetList = createEmptyRuleLabelingSetList();
	this.targetLabelSetList = createEmptyRuleLabelingSetList();
    }

    /**
     * This methods forms a list of ruleLabelings, sorted by frequency, until
     * for any label the number of different values exceeds the maximum number
     * of different values per label, as defined when creating the
     * MostFrequentRuleLabelingListCompter instance
     * 
     * This method should return the correct result, even when called multiple
     * times. Nevertheless, for efficiency, it should be typically only used
     * once (and the result is then stored).
     * 
     * @param frequencySortedRuleLabelingListDescending
     * @return
     */
    public List<RuleLabeling> getTopRuleLabelingsWithSpecifiedMaxAlteternativesPerLabel() {
	initialilizeSourceAndTargetLabelSetList();
	List<RuleLabeling> result = new ArrayList<RuleLabeling>();
	for (RuleLabeling ruleLabeling : frequencySortedRuleLabelingList) {
	    addRuleLabelingToLabelSetLists(ruleLabeling);
	    if (maxNumberDifferentValuesPerLabelExceeded()) {
		return result;
	    }
	    result.add(ruleLabeling);
	}

	return result;
    }

}
