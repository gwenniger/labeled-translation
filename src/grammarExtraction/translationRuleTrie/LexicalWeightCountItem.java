package grammarExtraction.translationRuleTrie;

import java.io.Serializable;
import java.util.List;

public class LexicalWeightCountItem extends CountItem implements CountItemCreater, Serializable
{
	private static final long serialVersionUID = 1L;
	private double lexicalWeightSourceGivenTarget;
	private double lexicalWeightTargetGivenSource;

	protected LexicalWeightCountItem(MTRuleTrieNode label,double count)
	{
		super(label,count);
	}
	
	public LexicalWeightCountItem()
	{
		this(null,0);
	}
	
	@Override 
	public CountItem createCountItemUnlabeledRuleSourceSide(MTRuleTrieNode label)
	{
		return new LexicalWeightCountItem(label,0);
	}
	
	
	@Override
	public CountItem createCountItemLabeledRuleSourceSide(MTRuleTrieNode label) {
		return new LexicalWeightCountItem(label,0);
	}
	
	
	
	public synchronized void performCountUpdate(CountUpdateItem countUpdateItem)
	{
		
		
		// add the count to add to compute the new count
		count += countUpdateItem.getCount();
		
		// compute the multiplier as the fraction of the count added 
		// from the total new count
		double multiplierNewWeight = countUpdateItem.getCount() / count;
		double multiplierOldWeight = 1 - multiplierNewWeight;
		
		assert(multiplierNewWeight >= 0 );
		assert(multiplierNewWeight <= 1);
		
		// Compute the new lexical weights, this itype filter texts effectively the method of 
		// computing incremental means.
		//System.out.println("countUpdateItem.getLexicalWeightSourceToTarget(): " + countUpdateItem.getLexicalWeightSourceToTarget());
		assert(countUpdateItem.getLexicalWeightSourceToTarget() >= 0);
		assert(countUpdateItem.getLexicalWeightTargetToSource() >= 0);
		this.setLexicalWeightSourceGivenTarget(multiplierNewWeight * countUpdateItem.getLexicalWeightSourceToTarget() + multiplierOldWeight * this.getLexicalWeightSourceGivenTarget());
		this.setLexicalWeightTargetGivenSource(multiplierNewWeight * countUpdateItem.getLexicalWeightTargetToSource() + multiplierOldWeight * this.getLexicalWeightTargetGivenSource());
		
		assert(this.getLexicalWeightSourceGivenTarget() >= 0);
		assert(this.getLexicalWeightTargetGivenSource() >= 0);
		assert(this.getLexicalWeightSourceGivenTarget() <= 1);
		assert(this.getLexicalWeightTargetGivenSource() <= 1);
	}

	public double getLexicalWeightSourceGivenTarget() {
		return lexicalWeightSourceGivenTarget;
	}

	public void setLexicalWeightSourceGivenTarget(
			double lexicalWeightSourceGivenTarget) {
		this.lexicalWeightSourceGivenTarget = lexicalWeightSourceGivenTarget;
	}

	public double getLexicalWeightTargetGivenSource() {
		return lexicalWeightTargetGivenSource;
	}

	public void setLexicalWeightTargetGivenSource(
			double lexicalWeightTargetGivenSource) {
		this.lexicalWeightTargetGivenSource = lexicalWeightTargetGivenSource;
	}

	@Override
	public Object makeObject() {
		return new LexicalWeightCountItem();
	}

	@Override
	public bitg.Pair<MTRuleTrieNode, CountItem> getMostFrequentLabeledRuleVersion() {
		throw new RuntimeException("Not Implemented");
	}

	@Override
	public void addLabeledRule(LabeledRuleWithCount labeledRuleWithCount) {
		throw new RuntimeException("Not implemented");
		
	}

	@Override
	public RuleLabeling getMostFrequentRuleLabelingWithinAllowedRuleLabelingsForSourceList(
		List<RuleLabeling> mostFrequentRuleLabelingsForSourceList,
		WordKeyMappingTable wordKeyMappingTable) {
	    throw new RuntimeException("Not implemented");
	}

}
