package grammarExtraction.translationRuleTrie;

import grammarExtraction.IntegerKeyRuleRepresentationWithLabel;
import grammarExtraction.chiangGrammarExtraction.GrammarExtractionConstraints;
import grammarExtraction.chiangGrammarExtraction.ReorderLabelingSettings;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.labelSmoothing.RuleLabelsSmoother;
import grammarExtraction.phraseProbabilityWeights.LabelSmoothingWeights;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeights;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsJoshua;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsPair;
import grammarExtraction.samtGrammarExtraction.GapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import junit.framework.Assert;
import bitg.Pair;

public class TranslationRuleProbabilityTable implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String PURE_HIERO_SMOOTHING_RULE_PREFIX_STRING = "PureHieroSmoothingRuleWeight";
	final SourceGivenTargetProbabilityTrie sourceGivenTargetProbabilityTrie;
	final TargetGivenSourceProbabilityTrie targetGivenSourceProbabilityTrie;
	private final LexicalProbabilityTables lexicalProbabilityTables;
	protected final WordKeyMappingTable wordKeyMappingTable;
	private final LabelProbabilityEstimator labelProbabilityEstimator;
	private final GapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator gapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator;
	private final RuleLabelsSmoother ruleLabelsSmoother;
	private final GrammarExtractionConstraints grammarExtractionConstraints;
	private final SystemIdentity systemIdentity;

	public TranslationRuleProbabilityTable(SourceGivenTargetProbabilityTrie sourceGivenTargetProbabilityTrie, TargetGivenSourceProbabilityTrie targetGivenSourceProbabilityTrie,
			LexicalProbabilityTables lexicalProbabilityTables, WordKeyMappingTable wordKeyMappingTable, LabelProbabilityEstimator labelProbabilityEstimator,
			GapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator gapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator,RuleLabelsSmoother ruleLabelsSmoother,
			GrammarExtractionConstraints grammarExtractionConstraints, SystemIdentity systemIdentity) {
		this.sourceGivenTargetProbabilityTrie = sourceGivenTargetProbabilityTrie;
		this.targetGivenSourceProbabilityTrie = targetGivenSourceProbabilityTrie;
		this.lexicalProbabilityTables = lexicalProbabilityTables;
		this.wordKeyMappingTable = wordKeyMappingTable;
		this.labelProbabilityEstimator = labelProbabilityEstimator;
		this.gapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator = gapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator;
		this.ruleLabelsSmoother = ruleLabelsSmoother;
		this.grammarExtractionConstraints = grammarExtractionConstraints;
		this.systemIdentity = systemIdentity;
	}

	public static TranslationRuleProbabilityTable createTranslationRuleProbabilityTable(TranslationRuleCountTable translationRuleCountsTable,
			GrammarExtractionConstraints grammarExtractionConstraints, SystemIdentity systemIdentity) {
		SourceGivenTargetProbabilityTrie sourceGivenTargetProbabilityTrie = SourceGivenTargetProbabilityTrie.createSourceGivenTargetProbabilityTrie(
				translationRuleCountsTable.getTranslationRuleTrie(), translationRuleCountsTable.getTranslationRuleTrie().getWordKeyMappintTable());
		TargetGivenSourceProbabilityTrie targetGivenSourceProbabilityTrie = TargetGivenSourceProbabilityTrie.createTargetGivenSourceProbabilityTrie(
				translationRuleCountsTable.getTranslationRuleTrie(), translationRuleCountsTable.getTranslationRuleTrie().getWordKeyMappintTable());
		TranslationRuleProbabilityTable result = new TranslationRuleProbabilityTable(sourceGivenTargetProbabilityTrie, targetGivenSourceProbabilityTrie,
				translationRuleCountsTable.getLexicalProbabilityTables(), translationRuleCountsTable.getWordKeyMappingTable(), translationRuleCountsTable.getLabelProbabilityEstimator(),
				translationRuleCountsTable.getGapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator(),
				translationRuleCountsTable.getRuleLabelsSmoother(), grammarExtractionConstraints, systemIdentity);
		result.computeCanonicalFormRuleExtractionInformationIfRequired();
		return result;
	}

	public WordKeyMappingTable getWordKeyMappingTable() {
		return this.wordKeyMappingTable;
	}

	public LexicalProbabilityTables getLexicalProbabilityTables() {
		return this.lexicalProbabilityTables;
	}

	public void printTargetGivenSourceProbabilityTable() {
		targetGivenSourceProbabilityTrie.printTable();
	}

	public void printSourceGivenTargetProbabilityTable() {
		sourceGivenTargetProbabilityTrie.printTable();
	}

	public void writeSourceGivenTargetProbabilityTableToFile(String fileName) {
		sourceGivenTargetProbabilityTrie.writeTableToFile(fileName);
	}

	public void writeTargetGivenSourceProbabilityTableToFile(String fileName) {
		targetGivenSourceProbabilityTrie.writeTableToFile(fileName);
	}

	public double getTargetGivenSourcePhrasalProbability(IntegerKeyRuleRepresentationWithLabel sourceRuleSide, IntegerKeyRuleRepresentationWithLabel targetRuleSide) {
		return targetGivenSourceProbabilityTrie.getConditionToConditionedProbability(sourceRuleSide, targetRuleSide);
	}

	public double getRuleCount(IntegerKeyRuleRepresentationWithLabel condition, IntegerKeyRuleRepresentationWithLabel conditioned) {
		return this.targetGivenSourceProbabilityTrie.getRuleCount(condition, conditioned);
	}

	public double getSourceGivenTargetPhrasalProbability(IntegerKeyRuleRepresentationWithLabel sourceRuleSide, IntegerKeyRuleRepresentationWithLabel targetRuleSide) {
		return sourceGivenTargetProbabilityTrie.getConditionToConditionedProbability(targetRuleSide, sourceRuleSide);
	}

	public double getSourceToTargetRulePhraseEntropy(IntegerKeyRuleRepresentationWithLabel sourceRuleRepresentation) {
		return targetGivenSourceProbabilityTrie.getRulePhraseEntropy(sourceRuleRepresentation);
	}

	public double getSourceSideRuleProbability(IntegerKeyRuleRepresentationWithLabel sourceRuleRepresentation) {
		return targetGivenSourceProbabilityTrie.getConditionSideProbability(sourceRuleRepresentation);
	}

	public double getSourceTargetPairProbability(IntegerKeyRuleRepresentationWithLabel condition, IntegerKeyRuleRepresentationWithLabel conditioned) {
		return targetGivenSourceProbabilityTrie.getConditionConditionedPairProbability(condition, conditioned);
	}

	private double getSourcePhraseFrequency(TranslationRuleSignatureTriple translationRuleSignatureTriple) {
		return targetGivenSourceProbabilityTrie.getConditionTotalCount(translationRuleSignatureTriple.getSourceSideRepresentationWithLabel(), this.systemIdentity.useNonHieroLabels());
	}

	private double getTargetPhraseFrequency(TranslationRuleSignatureTriple translationRuleSignatureTriple) {
		return sourceGivenTargetProbabilityTrie.getConditionTotalCount(translationRuleSignatureTriple.getTargetSideRepresentationWithLabel(), this.systemIdentity.useNonHieroLabels());
	}

	public int getNumRulesSourceToTarget() {
		return this.targetGivenSourceProbabilityTrie.getNumRules();
	}

	public int getNumRules() {
		return this.targetGivenSourceProbabilityTrie.getNumRules();
	}

	public LabelProbabilityEstimator getLabelProbabilityEstimator() {
		return this.labelProbabilityEstimator;
	}
	
	public String getGapLabelConditionalProbabilityTableString(){
	    return this.gapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator.getContentsString(wordKeyMappingTable);
	}

	public Enumeration<Pair<IntegerKeyRuleRepresentationWithLabel, RuleCountsTable>> getTargetGivenSourceRuleCountTableEnumeration() {
		return targetGivenSourceProbabilityTrie.getConditionsEnumeration();
	}

	public boolean hasTries() {
		return (this.targetGivenSourceProbabilityTrie != null) && (this.sourceGivenTargetProbabilityTrie != null);
	}

	public util.Pair<Double> getPhraseProbabililitiesPair(TranslationRuleSignatureTriple translationEquivalenceItem) {
		double targetGivenSourceProbability = this.getTargetGivenSourcePhrasalProbability(translationEquivalenceItem.getSourceSideRepresentationWithLabel(),
				translationEquivalenceItem.getTargetSideRepresentationWithLabel());
		double sourceGivenTargetProbability = this.getSourceGivenTargetPhrasalProbability(translationEquivalenceItem.getSourceSideRepresentationWithLabel(),
				translationEquivalenceItem.getTargetSideRepresentationWithLabel());
		return new util.Pair<Double>(targetGivenSourceProbability, sourceGivenTargetProbability);
	}

	private PhraseProbabilityWeightsPair getPureHieroRuleLabelSmoothingPhraseProbabilitiesPair(String label) {
		return PhraseProbabilityWeightsPair.createPhraseProbabilityWeightsPair(1, 1, label);
	}

	/**
	 * Add the Pure Hiero rule weights as a separate (smoothing) feature
	 * 
	 * @param originalTranslationRuleSignatureTriple
	 * @param smoothingProbabilityWeightPairs
	 */
	public void addPlainHieroRulesWeightsSeparatelyIfTurndedOn(TranslationRuleSignatureTriple originalTranslationRuleSignatureTriple, List<PhraseProbabilityWeightsPair> smoothingProbabilityWeightPairs) {
		if (writePlainHieroSmoothingRulesWithSeparateFeature()) {
			PhraseProbabilityWeightsPair phraseProbabilityWeightsPair;

			if (originalTranslationRuleSignatureTriple.isPureHieroRule(wordKeyMappingTable)) {
				// Add actual weights for pure Hiero rules
				util.Pair<Double> phraseProbabilities = this.getPhraseProbabililitiesPair(originalTranslationRuleSignatureTriple);
				phraseProbabilityWeightsPair = PhraseProbabilityWeightsPair.createPhraseProbabilityWeightsPair(phraseProbabilities.getFirst(), phraseProbabilities.getSecond(),
						PURE_HIERO_SMOOTHING_RULE_PREFIX_STRING);
			} else {
				// Add weights pair with probability 1 for non-pure Hiero rules
				phraseProbabilityWeightsPair = PhraseProbabilityWeightsPair.createPhraseProbabilityWeightsPair(1, 1, PURE_HIERO_SMOOTHING_RULE_PREFIX_STRING);
			}
			smoothingProbabilityWeightPairs.add(phraseProbabilityWeightsPair);
		}
	}

	public boolean writePlainHieroSmoothingRulesWithSeparateFeature() {
		return systemIdentity.writePlainHieroRulesForSmoothing() && systemIdentity.useSeparateFeatureForPlainHieroRulesForSmoothing();
	}

	public PhraseProbabilityWeightsPair createBasicPhraseProbabilityWeights(TranslationRuleSignatureTriple translationRuleSignatureTriple) {
		if (writePlainHieroSmoothingRulesWithSeparateFeature() && translationRuleSignatureTriple.isPureHieroRule(wordKeyMappingTable)) {
			return PhraseProbabilityWeightsPair.createBasicPhraseProbabilityWeightsPair(new util.Pair<Double>(1.0, 1.0));
		} else {
			return PhraseProbabilityWeightsPair.createBasicPhraseProbabilityWeightsPair(this.getPhraseProbabililitiesPair(translationRuleSignatureTriple));
		}
	}

	public PhraseProbabilityWeights createPhraseProbabilityWeights(TranslationRuleSignatureTriple translationRuleSignatureTriple) {
		// System.out.println(">>>><<<< createBasicUnsmoothedProbabilityWeightsSet called...");
		return PhraseProbabilityWeightsJoshua.createPhraseProbabilityWeightsJoshua(createBasicPhraseProbabilityWeights(translationRuleSignatureTriple),
				this.createLabelSmoothingWeights(translationRuleSignatureTriple), this.getSourceAndTargetPhrasePairFrequencies(translationRuleSignatureTriple));
	}

	public LabelSmoothingWeights createLabelSmoothingWeights(TranslationRuleSignatureTriple originalTranslationRuleSignatureTriple) {
		List<PhraseProbabilityWeightsPair> smoothingProbabilityWeightPairs = new ArrayList<PhraseProbabilityWeightsPair>();

		List<String> smoothingLabelPrefixStrings = ruleLabelsSmoother.getSmoothingLabelPrefixStrings();
		List<TranslationRuleSignatureTriple> translationRuleSignatureTriples = ruleLabelsSmoother.createSmoothingTranslationRuleSignatureTriples(originalTranslationRuleSignatureTriple);
		Assert.assertEquals(translationRuleSignatureTriples.size(), smoothingLabelPrefixStrings.size());
		for (int i = 0; i < translationRuleSignatureTriples.size(); i++) {
			TranslationRuleSignatureTriple translationRuleSignatureTriple = translationRuleSignatureTriples.get(i);
			if (originalTranslationRuleSignatureTriple.isPureHieroRule(wordKeyMappingTable)) {
				smoothingProbabilityWeightPairs.add(getPureHieroRuleLabelSmoothingPhraseProbabilitiesPair(smoothingLabelPrefixStrings.get(i)));
			} else {
				util.Pair<Double> phraseProbabilities = this.getPhraseProbabililitiesPair(translationRuleSignatureTriple);
				PhraseProbabilityWeightsPair probabilitiesPair = PhraseProbabilityWeightsPair.createPhraseProbabilityWeightsPair(phraseProbabilities.getFirst(), phraseProbabilities.getSecond(),
						smoothingLabelPrefixStrings.get(i));
				smoothingProbabilityWeightPairs.add(probabilitiesPair);
			}
		}
		addPlainHieroRulesWeightsSeparatelyIfTurndedOn(originalTranslationRuleSignatureTriple, smoothingProbabilityWeightPairs);

		return LabelSmoothingWeights.createLabelSmoothingWeights(smoothingProbabilityWeightPairs);
	}


	/**
	 * 
	 * Add the labeling information to the unlabeled version of the labeled rule
	 * This is then used to find the most frequent labeled version of an unlabeled rule 
	 * (given by the combination of unlabeled source and unlabeled target side)
	 * @param unlabeledSourceRepresentation
	 * @param unlabeledTargetRepresentation
	 * @param labeledSourceRepresentation
	 * @param countItemLabeled
	 */
	public void addLabeledRuleToListLabeledVersionsUnlabeledRule(IntegerKeyRuleRepresentationWithLabel unlabeledSourceRepresentation,
			IntegerKeyRuleRepresentationWithLabel unlabeledTargetRepresentation, IntegerKeyRuleRepresentationWithLabel labeledSourceRepresentation, CountItem countItemLabeled) {
		//System.out.println("TranslationRuleProbabilityTable.addLabeledRuleToListLabeledVersionsUnlabeledRule");
		this.targetGivenSourceProbabilityTrie.addLabeledRuleToListLabeledVersionsUnlabeledRule(unlabeledSourceRepresentation, unlabeledTargetRepresentation, labeledSourceRepresentation,
				countItemLabeled);
	}
	
	/**
	 * Add labeling information to the RuleCountsTable of the unlabeled source side of the 
	 * labeled rule. This is used to find the most frequent labeling given the u
	 * @param unlabeledSourceRepresentation
	 * @param unlabeledTargetRepresentation
	 * @param labeledSourceRepresentation
	 * @param countItemLabeled
	 */
	public void addRuleLabeling(IntegerKeyRuleRepresentationWithLabel  unlabeledSourceRepresentation, IntegerKeyRuleRepresentationWithLabel  unlabeledTargetRepresentation,
		IntegerKeyRuleRepresentationWithLabel labeledSourceRepresentation, CountItem countItemLabeled){
	    this.targetGivenSourceProbabilityTrie.addRuleLabeling(unlabeledSourceRepresentation, unlabeledTargetRepresentation, labeledSourceRepresentation, countItemLabeled);
	}
	
	private void computeCanonicalFormRuleExtractionInformationIfRequired() {
		if (systemIdentity.useCanonicalFormLabeledRules()) {
			computeCanonicalFormRuleExtractionInformation();
		}
	}
	
	/**
	 * This method will be used to compute the label information for canonical form rules
	 */
	private void computeCanonicalFormRuleExtractionInformation() {
		System.out.println("TranslationRuleProbabilityTable.computeCanonicalFormRuleExtractionInformation ...");
		long startTime = System.currentTimeMillis();
		Enumeration<Pair<IntegerKeyRuleRepresentationWithLabel, RuleCountsTable>> tableEnumeration = this.targetGivenSourceProbabilityTrie.getConditionsEnumeration();

		while (tableEnumeration.hasMoreElements()) {
			Pair<IntegerKeyRuleRepresentationWithLabel, RuleCountsTable> trieElement = tableEnumeration.nextElement();
			IntegerKeyRuleRepresentationWithLabel sourceRuleSide = trieElement.first;
			RuleCountsTable ruleCountsTable = trieElement.last;

			
			//System.out.println("RuleCountsTable: " + ruleCountsTable);
			if (sourceRuleSide.isSyntacticRuleSide()) {
				for (CountItem countItemLabeled : ruleCountsTable.getLabeledConditionedSideCountItems()) {
					IntegerKeyRuleRepresentationWithLabel unlabeledSourceRepresentation = wordKeyMappingTable.getFullyDowngradedLabelsRuleRepresentation(sourceRuleSide);
					IntegerKeyRuleRepresentationWithLabel unlabeledTargetRepresentation = wordKeyMappingTable.getFullyDowngradedLabelsRuleRepresentation(countItemLabeled.getLabel()
							.restoreRuleRepresentation());
					//System.out.println("original source rule side: " + sourceRuleSide.getStringRepresentation(wordKeyMappingTable));
					//System.out.println("unlabeledSourceRepresentation: " + unlabeledSourceRepresentation.getStringRepresentation(wordKeyMappingTable));
					//System.out.println("unlabeledTargetRepresentation: " + unlabeledTargetRepresentation.getStringRepresentation(wordKeyMappingTable));
					
					addLabeledRuleToListLabeledVersionsUnlabeledRule(unlabeledSourceRepresentation, unlabeledTargetRepresentation, sourceRuleSide, countItemLabeled);
					
					/**
					 * Add the rule labeling information to the rule count table
					 */
					if(systemIdentity.restrictLabelVariationForCanonicalFormLabeledRules()){    
					    addRuleLabeling(unlabeledSourceRepresentation, unlabeledTargetRepresentation, sourceRuleSide, countItemLabeled);
					}    
					
				}
			}
		}
		long secondsPassed = (System.currentTimeMillis() - startTime) / 1000;
		System.out.println("\t>>>Done, took: " + secondsPassed + " seconds");
	}

	public util.Pair<Double> getSourceAndTargetPhrasePairFrequencies(TranslationRuleSignatureTriple translationRuleSignatureTriple) {
		Double sourcePhraseFrequency = getSourcePhraseFrequency(translationRuleSignatureTriple);
		Double targetPhraseFrequency = getTargetPhraseFrequency(translationRuleSignatureTriple);
		return new util.Pair<Double>(sourcePhraseFrequency, targetPhraseFrequency);
	}

	public RuleLabelsSmoother getRuleLabelsSmoother() {
		return this.ruleLabelsSmoother;
	}

	public int getNumLexicalPlusPhraseProbabilityFeatures() {
		return SystemIdentity.getNumberOfBasicFeatureWeights() + this.ruleLabelsSmoother.getNoLabelSmoothingFeatures();
	}

	public ReorderLabelingSettings getReorderingLabelingSettings() {
		return this.grammarExtractionConstraints.getReorderLabelingSettings();
	}

}
