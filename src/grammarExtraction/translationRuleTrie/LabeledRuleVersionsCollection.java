package grammarExtraction.translationRuleTrie;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import junit.framework.Assert;

public class LabeledRuleVersionsCollection {
    /**
     * Two lists, one with the source side leaf Trie nodes and one with the
     * target side CountItems is sufficient to retrieve the set of labeled rules
     * with counts stored in this collection. Note that we deliberately use two
     * separate lists rather than one list of pairs, as this safes a lot of
     * memory.
     */
    private final List<MTRuleTrieNode> sourceSideLeafTrieNodes;
    private final List<CountItem> targetSideCountItems;

    private LabeledRuleVersionsCollection(List<MTRuleTrieNode> sourceSideLeafTrieNodes,
	    List<CountItem> targetSideCountItems) {
	this.sourceSideLeafTrieNodes = sourceSideLeafTrieNodes;
	this.targetSideCountItems = targetSideCountItems;
    }

    public static LabeledRuleVersionsCollection createLabeledRuleVersionsCollection() {
	List<MTRuleTrieNode> sourceSideLeafTrieNodes = new ArrayList<MTRuleTrieNode>();
	List<CountItem> targetSideCountItems = new ArrayList<CountItem>();
	return new LabeledRuleVersionsCollection(sourceSideLeafTrieNodes, targetSideCountItems);
    }

    /**
     * Add a labeled rule to the collection
     * 
     * @param ruleSourceSideTrieNode
     *            : The source Trie node identifying the source side
     * @param ruleTargetSideCountItem
     *            : The target side count item, identifying the target side and
     *            its count
     */
    public void addLabeledRule(MTRuleTrieNode ruleSourceSideTrieNode,
	    CountItem ruleTargetSideCountItem) {
	this.sourceSideLeafTrieNodes.add(ruleSourceSideTrieNode);
	this.targetSideCountItems.add(ruleTargetSideCountItem);
    }

    /**
     * This method return the most frequent labeled rule version, simply by
     * looking over all the CountItems and finding the one with highest
     * frequency. The the pair of the corresponding source and target
     * MTRuleTrieNode for that count item is returned
     * 
     * @return
     */
    public bitg.Pair<MTRuleTrieNode, CountItem> getMostFrequentLabeledRuleVersion() {
	Assert.assertTrue(!this.sourceSideLeafTrieNodes.isEmpty());

	CountItem firstTargeCountItem = this.targetSideCountItems.get(0);
	int currentBestIndex = 0;
	double currentHighestFrequency = firstTargeCountItem.getCount();

	//System.out.println("\n<getMostFrequentLabeledRuleVersion>");
	
	//System.out.println(" firstTargeCountItem.getCount(): " +  firstTargeCountItem.getCount() + " " + firstTargeCountItem);
	for (int i = 1; i < this.sourceSideLeafTrieNodes.size(); i++) {
	    CountItem targeCountItem = this.targetSideCountItems.get(i);
	    //System.out.println("targeCountItem.getCount(): " + targeCountItem.getCount() + " " + targeCountItem);
	    if (targeCountItem.getCount() > currentHighestFrequency) {
		// This important line was missing before!!!
		currentHighestFrequency = targeCountItem.getCount();
		currentBestIndex = i;
		//System.out.println("currentHighestFrequency: " + currentHighestFrequency);
	    }
	}
	//System.out.println("</getMostFrequentLabeledRuleVersion>\n");
	return new bitg.Pair<MTRuleTrieNode, CountItem>(
		sourceSideLeafTrieNodes.get(currentBestIndex),
		targetSideCountItems.get(currentBestIndex));
    }

    private RuleLabeling getRuleLabelingForElement(int index,
	    WordKeyMappingTable wordKeyMappingTable) {
	MTRuleTrieNode sourceTrieNode = this.sourceSideLeafTrieNodes.get(index);
	CountItem targetCountItem = this.targetSideCountItems.get(index);
	RuleLabeling ruleLabeling = RuleLabeling.createRuleLabeling(wordKeyMappingTable,
		sourceTrieNode.restoreRuleRepresentation(), targetCountItem.getLabel()
			.restoreRuleRepresentation(), 0);
	return ruleLabeling;
    }

    /**
     * This method finds the most frequent rule labeling that also is present in the 
     * mostFrequentRuleLabelingForSourceList argument.
     * If non of the rule labeling instances for the source-target combination is present in 
     * mostFrequentRuleLabelingForSourceList, then the best (i.e. first) RuleLabeling
     * from the  RuleLabelings for the source side is returned (i.e. as a form of back-off)
     * @param mostFrequentRuleLabelingsForSourceList
     * @param wordKeyMappingTable
     * @return
     */
    public RuleLabeling getMostFrequentRuleLabelingWithinAllowedRuleLabelingsForSourceList(
	    List<RuleLabeling> mostFrequentRuleLabelingsForSourceList,
	    WordKeyMappingTable wordKeyMappingTable) {
	Set<RuleLabeling> mostFrequentRuleLabelingsForSourceSet = new HashSet<RuleLabeling>(
		mostFrequentRuleLabelingsForSourceList);
	Assert.assertTrue(!this.sourceSideLeafTrieNodes.isEmpty());

	RuleLabeling currentBestRuleLabeling = null;
	double currentHighestFrequency = -1;

	for (int i = 0; i < this.sourceSideLeafTrieNodes.size(); i++) {
	    CountItem targeCountItem = this.targetSideCountItems.get(i);
	    RuleLabeling ruleLabeling = getRuleLabelingForElement(i, wordKeyMappingTable);
	    if (mostFrequentRuleLabelingsForSourceSet.contains(ruleLabeling)) {
		if (targeCountItem.getCount() > currentHighestFrequency) {
		    currentHighestFrequency = targeCountItem.getCount();
		    currentBestRuleLabeling = getRuleLabelingForElement(i, wordKeyMappingTable);
		}
	    }
	}
	if (currentBestRuleLabeling != null) {
	    return currentBestRuleLabeling;
	}
	
	return mostFrequentRuleLabelingsForSourceList.get(0);
    }

}
