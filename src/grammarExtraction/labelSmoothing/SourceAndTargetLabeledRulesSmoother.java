package grammarExtraction.labelSmoothing;

import grammarExtraction.IntegerKeyRuleRepresentation;
import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SourceAndTargetLabeledRulesSmoother extends LabeledRulesSmoother {

	private SourceAndTargetLabeledRulesSmoother(WordKeyMappingTable wordKeyMappingTable, AlternativelyLabeledRuleSetsListCreater reorderingLabelSmoothedRuleSetsCreater,AlternativelyLabeledRuleSetsListCreater mosesUnknownWordsRuleVariantsCreater,AlternativelyLabeledRuleSetsListCreater mosesPhrasePairRuleVariantsCreater) {
		super(wordKeyMappingTable, reorderingLabelSmoothedRuleSetsCreater,mosesUnknownWordsRuleVariantsCreater, mosesPhrasePairRuleVariantsCreater);
	}

	public static SourceAndTargetLabeledRulesSmoother crateSourceAndTargetLabeledRulesSmoother(WordKeyMappingTable wordKeyMappingTable,
			AlternativelyLabeledRuleSetsListCreater reorderingLabelSmoothedRuleSetsCreater,AlternativelyLabeledRuleSetsListCreater mosesUnknownWordsRuleVariantsCreater,AlternativelyLabeledRuleSetsListCreater mosesPhrasePairRuleVariantsCreater) {
		return new SourceAndTargetLabeledRulesSmoother(wordKeyMappingTable, reorderingLabelSmoothedRuleSetsCreater,mosesUnknownWordsRuleVariantsCreater,mosesPhrasePairRuleVariantsCreater);
	}

	@Override
	public List<LightWeightPhrasePairRuleSet> getAllLabelSmoothedRuleSets(LightWeightPhrasePairRuleSet originalRuleSet) {
		List<LightWeightPhrasePairRuleSet> result = getAllButFullySmoothedLabelSmoothedRuleSets(originalRuleSet);
		result.add(getFullSmoothedRulesSet(originalRuleSet));
		return result;
	}

	@Override
	protected IntegerKeyRuleRepresentation createBasicSmoothedRuleSideRepresentation(IntegerKeyRuleRepresentation originalRuleRepresentation) {
		return wordKeyMappingTable.getDowngradedToTargetSideLabelsOnlyRuleSideRepresentation(originalRuleRepresentation);
	}

	@Override
	protected Integer createBasicSmoothedLabelSideRepresentation(Integer labelSide) {
		return wordKeyMappingTable.getTargetSideLabelsOnlyDowngradedLabelKey(labelSide);
	}

	@Override
	public List<LightWeightPhrasePairRuleSet> getAllButFullySmoothedLabelSmoothedRuleSets(LightWeightPhrasePairRuleSet originalRuleSet) {
		List<LightWeightPhrasePairRuleSet> result = new ArrayList<LightWeightPhrasePairRuleSet>();
		result.add(getBasicSmoothedRulesSet(originalRuleSet));
		result.add(getSourceSmoothedRulesSet(originalRuleSet));
		result.add(getTargetSmoothedRulesSet(originalRuleSet));
		return result;
	}

	@Override
	public LightWeightPhrasePairRuleSet getFullySmoothedLabelSmoothedRuleSet(LightWeightPhrasePairRuleSet originalRuleSet) {
		return getFullSmoothedRulesSet(originalRuleSet);
	}

	@Override
	public int getNoLabelSmoothingFeatures() {
		return 8;
	}

	@Override
	public List<TranslationRuleSignatureTriple> createSmoothingTranslationRuleSignatureTriples(TranslationRuleSignatureTriple originalTranslationRuleSignatureTriple) {
		List<TranslationRuleSignatureTriple> result = new ArrayList<TranslationRuleSignatureTriple>();
		result.add(LABELED_RULE_BASIC_SMOOTHER.createSmoothedTranslationRuleSignatureTriple(originalTranslationRuleSignatureTriple, this));
		result.add(LABELED_RULE_SOURCE_SMOOHTER.createSmoothedTranslationRuleSignatureTriple(originalTranslationRuleSignatureTriple, this));
		result.add(LABELED_RULE_TARGET_SMOOTHER.createSmoothedTranslationRuleSignatureTriple(originalTranslationRuleSignatureTriple, this));
		result.add(LABELD_RUlE_FULL_SMOOTHER.createSmoothedTranslationRuleSignatureTriple(originalTranslationRuleSignatureTriple, this));
		return result;
	}

	public static List<String> getSmoothingLabelPrefixStringsSourceAndTarget() {
		return Arrays.asList(BASIC_LABEL_SMOOTHED_STRING, SOURCE_LABEL_SMOOTHED_STRING, TARGET_LABEL_SMOOTHED_STRING, FULL_LABEL_SMOOTHED_STRING);
	}

	@Override
	public List<String> getSmoothingLabelPrefixStrings() {
		return getSmoothingLabelPrefixStringsSourceAndTarget();
	}
}
