package grammarExtraction.labelSmoothing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

/**
 * A HieroSmoothingOnlyRulesSmoother is a RulesSmoother that takes care that, besides the original labeled 
 * version of the rules, only a fully label-stripped (i.e. Hiero) version of the rules is added to the Trie, 
 * and used to compute the smoothing weights P(Unlabeled(source)|Unlabeled(target))
 * and P(Unlabeled(target)|Unlabeled(source)).
 *
 */
public class HieroSmoothingOnlyRulesSmoother extends HieroRuleSmoother {

	private HieroSmoothingOnlyRulesSmoother(WordKeyMappingTable wordKeyMappingTable,
			AlternativelyLabeledRuleSetsListCreater reorderingLabelSmoothedRuleSetsCreater,AlternativelyLabeledRuleSetsListCreater mosesUnknownWordsRuleVariantsCreater,AlternativelyLabeledRuleSetsListCreater mosesPhrasePairRuleVariantsCreater) {
		super(wordKeyMappingTable, reorderingLabelSmoothedRuleSetsCreater,mosesUnknownWordsRuleVariantsCreater,mosesPhrasePairRuleVariantsCreater);
	}

	public static HieroSmoothingOnlyRulesSmoother createHieroSmoothingOnlyRulesSmoother(WordKeyMappingTable wordKeyMappingTable,
			AlternativelyLabeledRuleSetsListCreater reorderingLabelSmoothedRuleSetsCreater,AlternativelyLabeledRuleSetsListCreater mosesUnknownWordsRuleVariantsCreater,AlternativelyLabeledRuleSetsListCreater mosesPhrasePairRuleVariantsCreater) {
		return new HieroSmoothingOnlyRulesSmoother(wordKeyMappingTable, reorderingLabelSmoothedRuleSetsCreater,mosesUnknownWordsRuleVariantsCreater,mosesPhrasePairRuleVariantsCreater);
	}

	@Override
	public int getNoLabelSmoothingFeatures() {
		return 2;
	}

	@Override
	public List<TranslationRuleSignatureTriple> createSmoothingTranslationRuleSignatureTriples(
			TranslationRuleSignatureTriple originalTranslationRuleSignatureTriple) {
		List<TranslationRuleSignatureTriple> result = new ArrayList<TranslationRuleSignatureTriple>();
		result.add(LABELD_RUlE_FULL_SMOOTHER.createSmoothedTranslationRuleSignatureTriple(originalTranslationRuleSignatureTriple, this));
		return result;
	}

	public static List<String> getSmoothingLabelPrefixStringsHieroOnly() {
		return Collections.singletonList(FULL_LABEL_SMOOTHED_STRING);
	}

	@Override
	public List<String> getSmoothingLabelPrefixStrings() {
		return getSmoothingLabelPrefixStringsHieroOnly();
	}


}	


