package grammarExtraction.labelSmoothing;

import grammarExtraction.translationRules.LightWeightTranslationRule;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

public abstract class LabeledRuleSmoother {

	public LightWeightTranslationRule createSourceSmoothedRule(LightWeightTranslationRule originalRule, LabeledRulesSmoother labeledRulesSmoother) {
		TranslationRuleSignatureTriple smoothedTranslationRuleSignatureTriple = createSmoothedTranslationRuleSignatureTriple(originalRule.getTranslationRuleSignatureTriple(), labeledRulesSmoother);
		return LightWeightTranslationRule.createLightWeightTranslationRule(smoothedTranslationRuleSignatureTriple, originalRule.getLexicalWeightSourceToTarget(),originalRule.getLexicalWeightTargetToSource());
	}

	public abstract TranslationRuleSignatureTriple createSmoothedTranslationRuleSignatureTriple(TranslationRuleSignatureTriple translationRuleSignatureTriple, LabeledRulesSmoother labeledRulesSmoother);
}
