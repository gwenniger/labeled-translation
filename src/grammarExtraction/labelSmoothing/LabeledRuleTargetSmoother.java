package grammarExtraction.labelSmoothing;

import grammarExtraction.translationRules.LightWeightRuleLabel;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

public class LabeledRuleTargetSmoother extends LabeledRuleSmoother {

	private LightWeightRuleLabel createTargetSmoothedRuleLabel(TranslationRuleSignatureTriple translationRuleSignatureTriple, LabeledRulesSmoother labeledRulesSmoother) {
		LightWeightRuleLabel originalLabel = translationRuleSignatureTriple.getRuleLabel();
		return LightWeightRuleLabel.createLightWeightRuleLabel(labeledRulesSmoother.createBasicSmoothedLabelSideRepresentation(originalLabel.getSourceSideLabel()),
				labeledRulesSmoother.wordKeyMappingTable.getFullyDowngradedLabelKey(originalLabel.getTargetSideLabel()));
	}

	@Override
	public TranslationRuleSignatureTriple createSmoothedTranslationRuleSignatureTriple(TranslationRuleSignatureTriple translationRuleSignatureTriple, LabeledRulesSmoother labeledRulesSmoother) {
		return TranslationRuleSignatureTriple.createTranslationRuleSignatureTriple(
				labeledRulesSmoother.createBasicSmoothedRuleSideRepresentation(translationRuleSignatureTriple.getSourceSideRepresentation()),
				labeledRulesSmoother.wordKeyMappingTable.getFullyDowngradedLabelsRuleRepresentation(translationRuleSignatureTriple.getTargetSideRepresentation()),
				createTargetSmoothedRuleLabel(translationRuleSignatureTriple, labeledRulesSmoother));
	}

	/*
	 * @Override public TranslationRuleSignatureTriple createSmoothedTranslationEquivalenceItem(TranslationEquivalenceItem translationEquivalenceItem,
	 * LabeledRulesSmoother labeledRulesSmoother) { return
	 * TranslationEquivalenceItem.createTranslationEquivalenceItem(labeledRulesSmoother.createBasicSmoothedRuleSideRepresentation
	 * (translationEquivalenceItem.getSourceSideRepresentation()),
	 * labeledRulesSmoother.wordKeyMappingTable.getFullyDowngradedLabelsRuleRepresentation(translationEquivalenceItem.getTargetSideRepresentation())); }
	 */

}
