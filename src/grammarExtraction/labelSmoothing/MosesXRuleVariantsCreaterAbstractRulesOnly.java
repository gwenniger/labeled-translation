package grammarExtraction.labelSmoothing;

import grammarExtraction.chiangGrammarExtraction.ChiangRuleCreater;
import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.LightWeightTranslationRule;

import java.util.List;
import java.util.Map;

import util.Pair;

public class MosesXRuleVariantsCreaterAbstractRulesOnly extends MosesRelabeledGapsRuleVariantsCreater {

	private static final double X_RULE_SETS_WEIGTH = 1;
	private static final String X_LABEL_TAG = ChiangRuleCreater.ChiangLabel;

	protected MosesXRuleVariantsCreaterAbstractRulesOnly(WordKeyMappingTable wordKeyMappingTable, Map<Integer, Pair<Integer>> firstGapLabelReplacedByNewLabelMap,
			Map<Integer, Pair<Integer>> secondGapLabelReplacedByNewLabelMap, Map<Integer, Pair<Integer>> bothGapLabelsReplacedByNewLabelMap) {
		super(wordKeyMappingTable, firstGapLabelReplacedByNewLabelMap, secondGapLabelReplacedByNewLabelMap, bothGapLabelsReplacedByNewLabelMap);
	}

	public static MosesXPhrasePairRuleVariantsCreater createMosesXPhrasePairRuleVariantsCreaterAbstractRulesOnly(WordKeyMappingTable wordKeyMappingTable) {
		List<Map<Integer, Pair<Integer>>> replacedGapMapsList = createReplacedGapMapsList(wordKeyMappingTable, X_LABEL_TAG);
		return new MosesXPhrasePairRuleVariantsCreater(wordKeyMappingTable, replacedGapMapsList.get(0), replacedGapMapsList.get(1), replacedGapMapsList.get(2));
	}

	@Override
	protected double getRelabeledRuleSetWeight() {
		return X_RULE_SETS_WEIGTH;
	}

	private boolean containsOnlyTwoGapRules(LightWeightPhrasePairRuleSet originalRuleSet) {
		return (originalRuleSet.getBaseTranslationRule() == null) && (originalRuleSet.getOneGapTranslationRules().isEmpty());
	}

	private boolean allRulesInListAreAbstract(List<LightWeightTranslationRule> rulesList) {
		for (LightWeightTranslationRule rule : rulesList) {
			if (!rule.getTranslationRuleSignatureTriple().isAbstractRule(wordKeyMappingTable)) {
				return false;
			}
		}
		return true;
	}

	private boolean isAbstractRulesOnlyRuleSet(LightWeightPhrasePairRuleSet originalRuleSet) {
		if (containsOnlyTwoGapRules(originalRuleSet)) {
			return allRulesInListAreAbstract(originalRuleSet.getTwoGapTranslationRules());
		}
		return false;
	}

	@Override
	public boolean ruleSetIsApplicableForRelabeling(LightWeightPhrasePairRuleSet originalRuleSet) {
		return isAbstractRulesOnlyRuleSet(originalRuleSet);
	}
}
