package grammarExtraction.labelSmoothing;

import grammarExtraction.chiangGrammarExtraction.ChiangRuleCreater;
import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import java.util.List;
import java.util.Map;

import util.Pair;

public class MosesUnknownWordsRuleVariantsCreater extends MosesRelabeledGapsRuleVariantsCreater {

	private static final double UNKNOWN_WORDS_VARIANTS_RELATIVE_GENERATIVE_WEIGHT = 1E-6;

	protected MosesUnknownWordsRuleVariantsCreater(WordKeyMappingTable wordKeyMappingTable, Map<Integer, Pair<Integer>> firstGapLabelReplacedByXMap,
			Map<Integer, Pair<Integer>> secondGapLabelReplacedByXMap, Map<Integer, Pair<Integer>> bothGapLabelsReplacedByXMap) {
		super(wordKeyMappingTable, firstGapLabelReplacedByXMap, secondGapLabelReplacedByXMap, bothGapLabelsReplacedByXMap);
	}

	public static MosesUnknownWordsRuleVariantsCreater createMosesUnknownWordsRuleVariantsCreater(WordKeyMappingTable wordKeyMappingTable) {
		List<Map<Integer, Pair<Integer>>> replacedGapMapsList = createReplacedGapMapsList(wordKeyMappingTable, ChiangRuleCreater.ChiangLabel);
		return new MosesUnknownWordsRuleVariantsCreater(wordKeyMappingTable, replacedGapMapsList.get(0), replacedGapMapsList.get(1), replacedGapMapsList.get(2));
	}
	

	@Override
	protected double getRelabeledRuleSetWeight() {
		return UNKNOWN_WORDS_VARIANTS_RELATIVE_GENERATIVE_WEIGHT;
	}

	@Override
	public boolean ruleSetIsApplicableForRelabeling(LightWeightPhrasePairRuleSet originalRuleSet) {
		return true;
	}
}