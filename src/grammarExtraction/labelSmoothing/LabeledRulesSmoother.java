package grammarExtraction.labelSmoothing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mt_pipeline.MTPipelineHatsHiero;
import mt_pipeline.mt_config.MTGrammarExtractionConfig;
import alignmentStatistics.MultiThreadComputationParameters;
import util.ConfigFile;
import grammarExtraction.IntegerKeyRuleRepresentation;
import grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagLabelingSettings;
import grammarExtraction.chiangGrammarExtraction.GrammarExtractionConstraints;
import grammarExtraction.chiangGrammarExtraction.LabelSideSettings;
import grammarExtraction.chiangGrammarExtraction.ReorderLabelingSettings;
import grammarExtraction.grammarExtractionFoundation.GrammarStructureSettings;
import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import reorderingLabeling.FineParentRelativeReorderingLabel;
import grammarExtraction.reorderingLabeling.ReorderingLabelSmoothedRuleSetsCreater;
import grammarExtraction.samtGrammarExtraction.ComplexLabelsGrammarExtractionParameters;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.LightWeightTranslationRule;
import grammarExtraction.translationRules.LightWeightRuleLabel;
/**
 * The primary idea behind label smoothing is that we need 
 * to generate alternatively labeled versions of rules, in which some of the labels are stripped,
 * in order to be able to compute smoothed phrase weights. But how much labeled variants need to 
 * be generated, depends on what labels are present in the original fully labeled version.
 * Somewhat counter intuitive to what is common in general syntactic MT, such as "String-to-Tree"
 * and "Tree-to-String" models, we follow the way of dealing with labeling introduced by 
 * Syntax Augmented Machine Translation. This means that labels are in principal shared on the 
 * source and target side. This is also in line with the Joshua decoder, which currently only supports 
 * labels that are shared across sides, not one type of labels for the source and another type of labels
 * on the other side. The non-abstract classes that are derived from this class, must be understood 
 * from this initial assumption.
 * But the side were labels are put in the grammar differs from the origin of the labels, and it is possible
 * in principle to take syntactic labels from source and/or target side and also other labels such as 
 * boundary tags (formed by taking the information of POS-tags on the boundary of phrases) from source and/or 
 * target side. This means that in principle labels are shared across both sides of the grammar, but 
 * can have a source and/or a target component.
 * 
 *  This explains the classes "SourceAndTargetLabeledRulesSmoother" and "SourceOrTargetLabeledRulesSmoother".
 *  The first class is used in the scenario that the labels - shared across source and target side like in SAMT -
 *  have a source and a target component. In the second case the labels - still shared across the source 
 *  and target side - have only a source or a target component, not both.
 *  
 *
 * This is the basic historic context, and typical usage.
 * However, it is possible to put the labels only on one side. The class  
 * (grammarExtraction.chiangGrammarExtraction.)LabelSideSettings the configuration to set labeling
 * on both sides or, only on the source or target side. This information is stored inside SystemIdentity,
 * the high level configuration storing object used in grammar extraction amongst other things. 
 * Depending on what sides are labeled, certain methods that determine what smoothing is necessary, such 
 * as the method getSmoothingPrefixesBoundaryTaggedSystem in this class, will also give different outputs.
 * If rules are not labeled on both sides for example, this method knows that only full label smoothing:
 * stripping the labels on both sides (effectively only one side in this case!) is required.   
 * In other words, labeling on only one side is fully supported, however, in case of working with Joshua
 * this will only affect the probabilities, but not the actual rules - because unlike Moses, 
 * Joshua requires labels to be the same on both sides currently.
 *  
 */
public abstract class LabeledRulesSmoother implements RuleLabelsSmoother {

	protected final WordKeyMappingTable wordKeyMappingTable;
	protected static final LabeledRuleBasicSmoother LABELED_RULE_BASIC_SMOOTHER = new LabeledRuleBasicSmoother();
	protected static final LabeledRuleSourceSmoother LABELED_RULE_SOURCE_SMOOHTER = new LabeledRuleSourceSmoother();
	protected static final LabeledRuleTargetSmoother LABELED_RULE_TARGET_SMOOTHER = new LabeledRuleTargetSmoother();
	protected static final LabeledRuleFullSmoother LABELD_RUlE_FULL_SMOOTHER = new LabeledRuleFullSmoother();
	public static final String LABEL_SMOOTHING_TYPE_PROPERTY = "labelSmoothingType";
	public static final String NO_LABEL_SMOOTHING = "noLabelSmoothing";
	public static final String HIERO_ONLY_LABEL_SMOOTHING = "hieroOnlyLabelSmoothing";
	public static final String FULL_LABEL_SMOOTHING = "fullLabelSmoothing";
	public static final String STRIP_SOURCE_PART_LABELS_FOR_SMOOTHING_DOUBLE_LABELD_RULES = "stripSourcePartLabelsForSmoothingDoubleLabeledRules";

	public static final String REORDERING_LABEL_RULE_SMOOTHING_PROPERTY = "useReorderingLabelRuleSmoothing";
	public static final String REORDERING_LABEL_RULE_SMOOTHING_RULE_WEIGHT_PROPERTY = "reorderingLabelRuleSmoothingRuleWeight";

	protected final AlternativelyLabeledRuleSetsListCreater reorderingLabelSmoothedRuleSetsCreater;
	protected final AlternativelyLabeledRuleSetsListCreater mosesUnknownWordsRuleVariantsCreater;
	protected final AlternativelyLabeledRuleSetsListCreater mosesPhrasePairRuleVariantsCreater;

	protected static final String BASIC_LABEL_SMOOTHED_STRING = "BasicLabelSmoothed";
	protected static final String SOURCE_LABEL_SMOOTHED_STRING = "SourceLabelSmoothed";
	protected static final String TARGET_LABEL_SMOOTHED_STRING = "TargetLabelSmoothed";
	protected static final String FULL_LABEL_SMOOTHED_STRING = "FullLabelSmoothed";

	/**
	 * Label smoothing types: 
	 *   NO_LABEL_SMOOTHING - no smoothing features will be added
	 *   
	 *   HIERO_ONLY_LABEL_SMOOTHING - Beside the phrase weights for the fully labeled version of 
	 *   the rules, only phrase weights for the fully unlabeled (fully label stripped = Hiero)
	 *   version of the rules will be added.
	 *   
	 *   Full_LABEL_SMOOTHING: Details of the implementation depend on what labels are present
	 *   in the maximum case, when labels are present on both sides, with a source and target 
	 *   component in the label. There are smoothing features added for:
	 *   1. Stripping only the source component of the labels, on both sides => 2 features
	 *   	2. Stripping the source component  and additionally stripping the 
	 *         target component on the source side. => 2 features
	 *      3. Stripping the source component  and additionally stripping 
	 *         the target component on the target side. => 2 features
	 *        4. Stripping the source component and the target component 
	 *           on both sides (fully label stripped). => 2 features
	 *    So in the maximum case there are in total 8 smoothing features added.
	 *    In the case that only the source or the target component of the label is used (on both sides),
	 *    the first smoothing step is not necessary, and only 6 smoothing features are added.   
	 *             
	 *
	 */
	public static enum LABEL_SMOOTHING_TYPE {
		NO_LABEL_SMOOTHING, HIERO_ONLY_LABEL_SMOOTHING, FULL_LABEL_SMOOTHING
	};

	protected LabeledRulesSmoother(WordKeyMappingTable wordKeyMappingTable, AlternativelyLabeledRuleSetsListCreater reorderingLabelSmoothedRuleSetsCreater,
			AlternativelyLabeledRuleSetsListCreater mosesUnknownWordsRuleVariantsCreater, AlternativelyLabeledRuleSetsListCreater mosesPhrasePairRuleVariantsCreater) {
		this.wordKeyMappingTable = wordKeyMappingTable;
		this.reorderingLabelSmoothedRuleSetsCreater = reorderingLabelSmoothedRuleSetsCreater;
		this.mosesUnknownWordsRuleVariantsCreater = mosesUnknownWordsRuleVariantsCreater;
		this.mosesPhrasePairRuleVariantsCreater = mosesPhrasePairRuleVariantsCreater;
	}

	protected abstract IntegerKeyRuleRepresentation createBasicSmoothedRuleSideRepresentation(IntegerKeyRuleRepresentation originalRuleRepresentation);

	protected abstract Integer createBasicSmoothedLabelSideRepresentation(Integer labelSide);

	protected LightWeightTranslationRule createSmoothedRule(LightWeightTranslationRule unsmoothedRule, LabeledRuleSmoother labeledRuleSmoother) {
		if (unsmoothedRule != null) {
			return labeledRuleSmoother.createSourceSmoothedRule(unsmoothedRule, this);
		}
		return null;
	}

	protected List<LightWeightTranslationRule> createSmoothedRuleList(List<LightWeightTranslationRule> unsmoothedRules, LabeledRuleSmoother labeledRuleSmoother) {
		List<LightWeightTranslationRule> result = new ArrayList<LightWeightTranslationRule>();
		for (LightWeightTranslationRule rule : unsmoothedRules) {
			result.add(labeledRuleSmoother.createSourceSmoothedRule(rule, this));
		}
		return result;
	}

	protected LightWeightPhrasePairRuleSet getSmoothedRulesSet(LightWeightPhrasePairRuleSet originalRuleSet, LabeledRuleSmoother labeledRuleSmoother) {
		LightWeightTranslationRule basicRuleSmoothed = createSmoothedRule(originalRuleSet.getBaseTranslationRule(), labeledRuleSmoother);
		List<LightWeightTranslationRule> oneGapRulesSmoothed = createSmoothedRuleList(originalRuleSet.getOneGapTranslationRules(), labeledRuleSmoother);
		List<LightWeightTranslationRule> twoGapRulesSmoothed = createSmoothedRuleList(originalRuleSet.getTwoGapTranslationRules(), labeledRuleSmoother);
		LightWeightRuleLabel smoothedRuleLabel = LightWeightPhrasePairRuleSet.getLeftHandSideRulesLabel(basicRuleSmoothed, oneGapRulesSmoothed, twoGapRulesSmoothed);
		return LightWeightPhrasePairRuleSet.createLightWeightPhrasePairRuleSet(basicRuleSmoothed, oneGapRulesSmoothed, twoGapRulesSmoothed, smoothedRuleLabel, originalRuleSet.getRuleSetWeight());
	}

	private static List<String> labelSmoothingTypes() {
		return Arrays.asList(NO_LABEL_SMOOTHING, HIERO_ONLY_LABEL_SMOOTHING, FULL_LABEL_SMOOTHING);
	}

	public static LABEL_SMOOTHING_TYPE getLabelSmoothingType(ConfigFile theConfig) {

		if (!theConfig.hasValue(LABEL_SMOOTHING_TYPE_PROPERTY)) {
			return LABEL_SMOOTHING_TYPE.NO_LABEL_SMOOTHING;
		}

		String labelSmoothingTypeString = theConfig.getValueWithPresenceCheck(LABEL_SMOOTHING_TYPE_PROPERTY);

		System.out.println("labelSmoothingTypeString: " + labelSmoothingTypeString);

		if (labelSmoothingTypeString.equals(NO_LABEL_SMOOTHING)) {
			return LABEL_SMOOTHING_TYPE.NO_LABEL_SMOOTHING;
		} else if (labelSmoothingTypeString.equals(HIERO_ONLY_LABEL_SMOOTHING)) {
			return LABEL_SMOOTHING_TYPE.HIERO_ONLY_LABEL_SMOOTHING;
		} else if (labelSmoothingTypeString.equals(FULL_LABEL_SMOOTHING)) {
			return LABEL_SMOOTHING_TYPE.FULL_LABEL_SMOOTHING;
		} else {
			String exceptionString = "Unrecognized label smoothing type. Please choose one from :";
			for (String smoothingType : labelSmoothingTypes()) {
				exceptionString += "\n" + smoothingType;
			}
			throw new RuntimeException(exceptionString);
		}

	}

	private static boolean useReorderingLabelRuleSmoothing(ConfigFile theConfig) {
		return theConfig.getBooleanWithPresenceCheck(REORDERING_LABEL_RULE_SMOOTHING_PROPERTY);
	}

	private static double getReorderingLabelRuleSmoothingSmoothingRuleWeight(ConfigFile theConfig) {
		String valueString = theConfig.getValueWithPresenceCheck(REORDERING_LABEL_RULE_SMOOTHING_RULE_WEIGHT_PROPERTY);
		return Double.parseDouble(valueString);
	}

	private static AlternativelyLabeledRuleSetsListCreater createReorderingLabelSmoothedRuleSetsCreater(ConfigFile theConfig, WordKeyMappingTable wordKeyMappingTable) {
		if (useReorderingLabelRuleSmoothing(theConfig)) {
			return ReorderingLabelSmoothedRuleSetsCreater.createReorderingLabelSmoothedRuleSetsCreater(wordKeyMappingTable, getReorderingLabelRuleSmoothingSmoothingRuleWeight(theConfig),
					FineParentRelativeReorderingLabel.createReorderingLabelCreater(), LabelSideSettings.getLabelSideSettingsFromConfig(theConfig).createRuleLabelCreater());
		} else {
			return EmptyAlternativelyLabeledRuleSetsListCreater.createEmptyAlternativelyLabeledRuleSetsListCreater();
		}
	}

	public static boolean stripSourcePartLabelsForSmoothingDoubleLabeldRules(ConfigFile configFile) {
		return configFile.getBooleanIfPresentOrReturnDefault(LabeledRulesSmoother.STRIP_SOURCE_PART_LABELS_FOR_SMOOTHING_DOUBLE_LABELD_RULES, true);
	}

	public static List<String> getSmoothingPrefixesBoundaryTaggedSystem(SystemIdentity systemIdentity) {
		LABEL_SMOOTHING_TYPE labelSmoothingType = systemIdentity.getLabSmoothingType();
		BoundaryTagLabelingSettings boundaryTagLabelSettings = systemIdentity.getBoundaryTagLabelingSettings();
		boolean useReorderingLabelExtension = systemIdentity.useReorderingLabelExtension();
		if (labelSmoothingType.equals(LABEL_SMOOTHING_TYPE.NO_LABEL_SMOOTHING) || (!boundaryTagLabelSettings.useAnyTagLabels())) {
			return NoLabelSmoothingRulesSmoother.getSmoothingLabelPrefixStringsNoSmoothing();
		} else if (labelSmoothingType.equals(LABEL_SMOOTHING_TYPE.HIERO_ONLY_LABEL_SMOOTHING) || (!systemIdentity.labelBothSides()) ) {
			return HieroSmoothingOnlyRulesSmoother.getSmoothingLabelPrefixStringsHieroOnly();
		} else if (labelSmoothingType.equals(LABEL_SMOOTHING_TYPE.FULL_LABEL_SMOOTHING)) {
			if (systemIdentity.stripSourcePartLabelsForSmoothingDoubleLabeldRules() && (boundaryTagLabelSettings.useSourceTagLabels() || useReorderingLabelExtension)
					&& boundaryTagLabelSettings.useTargetTagLabels()) {
				return SourceAndTargetLabeledRulesSmoother.getSmoothingLabelPrefixStringsSourceAndTarget();
			} else {
				return SourceOrTargetLabeledRulesSmoother.getSmoothingLabelPrefixStringsSourceOrTarget();
			}
		}
		throw new RuntimeException("Error : createBoundaryTagRuleLabelsSmoother: Unrecognized label smoothing type");
	}

	public static AlternativelyLabeledRuleSetsListCreater createMosesUnknownWordsRuleVariantsCreater(SystemIdentity systemIdentity, WordKeyMappingTable wordKeyMappingTable, ConfigFile configFile) {
		if (systemIdentity.isMosesSystem() && MTGrammarExtractionConfig.useMosesHieroUnknownWordRuleVariants(configFile)) {
			return MosesUnknownWordsRuleVariantsCreater.createMosesUnknownWordsRuleVariantsCreater(wordKeyMappingTable);
		} else {
			return EmptyAlternativelyLabeledRuleSetsListCreater.createEmptyAlternativelyLabeledRuleSetsListCreater();
		}
	}

	public static AlternativelyLabeledRuleSetsListCreater createMosesPhrasePairRuleVariantsCreater(SystemIdentity systemIdentity, WordKeyMappingTable wordKeyMappingTable, ConfigFile configFile) {
		ReorderLabelingSettings reorderLabelingSettings = systemIdentity.getReorderLabelingSettings();
		if ((reorderLabelingSettings.useReorderingLabelExtension()) && (!reorderLabelingSettings.reorderLabelPhrasePairs()) && systemIdentity.isMosesSystem()) {

			if (systemIdentity.getSystemName().equals(MTPipelineHatsHiero.SystemName)) {
				if (reorderLabelingSettings.reorderLabelHieroRules()) {
					return MosesXPhrasePairRuleVariantsCreater.createMosesXPhrasePairRuleVariantsCreater(wordKeyMappingTable);
				} else {
					return MosesXRuleVariantsCreaterAbstractRulesOnly.createMosesXPhrasePairRuleVariantsCreaterAbstractRulesOnly(wordKeyMappingTable);
				}
			} else {
				throw new RuntimeException("Error: MosesXPhrasePairRuleVariantsCreater - createMosesPhrasePairRuleVariantsCreater with PhrasePair (reordering) labeled phrase pairs is currently only"
						+ " supported for ReorderingLabeled Hiero grammars, not for general (e.g. SAMT) reordering labeled grammars");
			}
		} else {
			return EmptyAlternativelyLabeledRuleSetsListCreater.createEmptyAlternativelyLabeledRuleSetsListCreater();
		}
	}

	public static boolean writePlainHieroRulesForSmoothing(ConfigFile theConfig) {
		return GrammarStructureSettings.writePlainHieroRulesForSmoothing(theConfig);
	}

	private static  boolean useCanonicalFormLabeledRulesAndUseLabeling(SystemIdentity systemIdentity)
	{
		return systemIdentity.useCanonicalFormLabeledRules() && systemIdentity.useNonHieroLabels();
	}
	
	
	/** This method determines if we actually need to add (still) hiero versions of the rules, when 
	 * not working with label smoothing
	 * @param useComplexLabels
	 * @param systemIdentity
	 * @return
	 */
	private static boolean needToAddHieroVersionRules(boolean useComplexLabels,SystemIdentity systemIdentity)
	{
		/** IF we use only hiero weights for canonical form labeled rules, then we  
		* need the PureHieroRulesSmoothingOnlyRulesSmoother, which basically adds 
		* the smoothed hiero rules which are necessary for computing the canonical form 
		* of rules and adding the weights, but does not add extra features that are in Hiero
		* The phrase weights that are used for the rules are then in fact the phrase weights 
		* for the Hiero rules. The rule labels are the rule labels of the cannoical labeled forms
		* of the unlabeled rules*/
		if(systemIdentity.useOnlyHieroWeightsForCanonicalFormLabeledRules()){
		   return true;
		}
		else{ 
		    /**
		     *  If there are no complex labels the unsmoothed rules themselves are 
		     *  hiero rules, so no need to add hiero versions in any case
		     */
		    if ((!useComplexLabels)){ 
			return false;
		    }
		    /**
		     * If no plain hiero rules (separately written to grammar) for smoothing are
		     * desired (and we are dealing with a complex grammar), then no 
		     * separate hiero rules are needed
		     */
    		    else if (!systemIdentity.writePlainHieroRulesForSmoothing()){
    			return false;
    		    }
    		    /**
    		     * We have complex labels and desire plain hiero rules for smoothing,
    		     * so we need to write the hiero rules
    		     */
    		    else
    		    {
    			return true;
    		    }
		}  	    
	}
	
	/**
	 * This method creates a RuleLabelsSmoother for the case that no smoothing is desired in principle.
	 * However, it may still be that we want to to write an unlabeled (Hiero) version of the grammar 
	 * as a sort of crude top level smoothing. Or alternatively, the Hiero version of the rules might
	 * be needed to compute the weights for the canonical version of rules.
	 * In either of these two cases the method  will return a PurehHieroRulesSmoothingOnlyRulesSmoother,
	 * that still will take care a Hiero version of the rules is still added to the Trie, besides the 
	 * labeled version. 
	 * 
	 * If a Hiero version of the rules is not needed a NoLabelSmoothingRulesSmoother is returned.
	 * 
	 * @param configFile
	 * @param labelSmoothingType
	 * @param wordKeyMappingTable
	 * @param systemIdentity
	 * @param useComplexLabels
	 * @return
	 */
	public static RuleLabelsSmoother createNoLabelSmoothingRuleSmoother(ConfigFile configFile, LABEL_SMOOTHING_TYPE labelSmoothingType, WordKeyMappingTable wordKeyMappingTable,
			SystemIdentity systemIdentity, boolean useComplexLabels) {
		AlternativelyLabeledRuleSetsListCreater reorderingLabelSmoothedRuleSetsCreater = createReorderingLabelSmoothedRuleSetsCreater(configFile, wordKeyMappingTable);
		AlternativelyLabeledRuleSetsListCreater mosesUnknownWordsRuleVariantsCreater = createMosesUnknownWordsRuleVariantsCreater(systemIdentity, wordKeyMappingTable, configFile);
		AlternativelyLabeledRuleSetsListCreater mosesPhrasePairRuleVariantsCreater = createMosesPhrasePairRuleVariantsCreater(systemIdentity, wordKeyMappingTable, configFile);
		
    		if(needToAddHieroVersionRules(useComplexLabels, systemIdentity)){    
		    return PureHieroRulesSmoothingOnlyRulesSmoother.createPureHieroRulesSmoothingOnlyRulesSmoother(wordKeyMappingTable, reorderingLabelSmoothedRuleSetsCreater,
				mosesUnknownWordsRuleVariantsCreater, mosesPhrasePairRuleVariantsCreater);
		}
    		else{    
    		    return NoLabelSmoothingRulesSmoother.createNoLabelSmoothingRulesSmootherForFilteredGrammarExtraction(reorderingLabelSmoothedRuleSetsCreater, wordKeyMappingTable,
					mosesUnknownWordsRuleVariantsCreater, mosesPhrasePairRuleVariantsCreater,useCanonicalFormLabeledRulesAndUseLabeling(systemIdentity));
		} 
	}

	public static RuleLabelsSmoother createHieroRuleLabelsSmoother(ConfigFile configFile, MultiThreadComputationParameters multiThreadComputationParameters,
			GrammarExtractionConstraints grammarExtractionConstraints, SystemIdentity systemIdentity) {
		LABEL_SMOOTHING_TYPE labelSmoothingType = LabeledRulesSmoother.getLabelSmoothingType(configFile);
		boolean useReorderingExtension = grammarExtractionConstraints.useReorderingLabelExtension();

		WordKeyMappingTable wordKeyMappingTable = createWordKeyMappingTable(multiThreadComputationParameters, MTGrammarExtractionConfig.getGrammerSourceFilterPath(configFile));
		AlternativelyLabeledRuleSetsListCreater reorderingLabelSmoothedRuleSetsCreater = createReorderingLabelSmoothedRuleSetsCreater(configFile, wordKeyMappingTable);
		AlternativelyLabeledRuleSetsListCreater mosesUnknownWordsRuleVariantsCreater = createMosesUnknownWordsRuleVariantsCreater(systemIdentity, wordKeyMappingTable, configFile);
		AlternativelyLabeledRuleSetsListCreater mosesPhrasePairRuleVariantsCreater = createMosesPhrasePairRuleVariantsCreater(systemIdentity, wordKeyMappingTable, configFile);

		if (labelSmoothingType.equals(LABEL_SMOOTHING_TYPE.NO_LABEL_SMOOTHING) || (!useReorderingExtension)) {
			return createNoLabelSmoothingRuleSmoother(configFile, labelSmoothingType, wordKeyMappingTable, systemIdentity, useReorderingExtension);
		} else if (labelSmoothingType.equals(LABEL_SMOOTHING_TYPE.HIERO_ONLY_LABEL_SMOOTHING) || (!systemIdentity.labelBothSides())) {
			return HieroSmoothingOnlyRulesSmoother.createHieroSmoothingOnlyRulesSmoother(wordKeyMappingTable, reorderingLabelSmoothedRuleSetsCreater, mosesUnknownWordsRuleVariantsCreater,
					mosesPhrasePairRuleVariantsCreater);
		} else if (labelSmoothingType.equals(LABEL_SMOOTHING_TYPE.FULL_LABEL_SMOOTHING)) {
			return SourceOrTargetLabeledRulesSmoother.crateSourceOrTargetLabeledRulesSmoother(wordKeyMappingTable, reorderingLabelSmoothedRuleSetsCreater, mosesUnknownWordsRuleVariantsCreater,
					mosesPhrasePairRuleVariantsCreater);
		}
		throw new RuntimeException("Error : createBoundaryTagRuleLabelsSmoother: Unrecognized label smoothing type");
	}

	public static RuleLabelsSmoother createBoundaryTagRuleLabelsSmoother(ConfigFile configFile, ComplexLabelsGrammarExtractionParameters complexLabelGrammarExtractionParameters,
			SystemIdentity systemIdentity) {
		LABEL_SMOOTHING_TYPE labelSmoothingType = LabeledRulesSmoother.getLabelSmoothingType(configFile);
		BoundaryTagLabelingSettings boundaryTagLabelSettings = BoundaryTagLabelingSettings.createBoundaryTagLabelingSettingsFromConfig(configFile);

		WordKeyMappingTable wordKeyMappingTable = createWordKeyMappingTable(complexLabelGrammarExtractionParameters.getMultiThreadComputationParameters(),
				MTGrammarExtractionConfig.getGrammerSourceFilterPath(configFile));
		AlternativelyLabeledRuleSetsListCreater reorderingLabelSmoothedRuleSetsCreater = createReorderingLabelSmoothedRuleSetsCreater(configFile, wordKeyMappingTable);
		AlternativelyLabeledRuleSetsListCreater mosesUnknownWordsRuleVariantsCreater = createMosesUnknownWordsRuleVariantsCreater(systemIdentity, wordKeyMappingTable, configFile);
		AlternativelyLabeledRuleSetsListCreater mosesPhrasePairRuleVariantsCreater = createMosesPhrasePairRuleVariantsCreater(systemIdentity, wordKeyMappingTable, configFile);

		if (labelSmoothingType.equals(LABEL_SMOOTHING_TYPE.NO_LABEL_SMOOTHING) || (!boundaryTagLabelSettings.useAnyTagLabels())) {
			// return
			// NoLabelSmoothingRulesSmoother.createNoLabelSmoothingRulesSmootherForFilteredGrammarExtraction(reorderingLabelSmoothedRuleSetsCreater,
			// wordKeyMappingTable, mosesUnknownWordsRuleVariantsCreater,
			// mosesPhrasePairRuleVariantsCreater);
			return createNoLabelSmoothingRuleSmoother(configFile, labelSmoothingType, wordKeyMappingTable, systemIdentity, boundaryTagLabelSettings.useAnyTagLabels());
		} else if (labelSmoothingType.equals(LABEL_SMOOTHING_TYPE.HIERO_ONLY_LABEL_SMOOTHING) || (!systemIdentity.labelBothSides())) {
			return HieroSmoothingOnlyRulesSmoother.createHieroSmoothingOnlyRulesSmoother(wordKeyMappingTable, reorderingLabelSmoothedRuleSetsCreater, mosesUnknownWordsRuleVariantsCreater,
					mosesPhrasePairRuleVariantsCreater);
		} else if (labelSmoothingType.equals(LABEL_SMOOTHING_TYPE.FULL_LABEL_SMOOTHING)) {

			return createFullLabelSmoother(useSourceLabels(complexLabelGrammarExtractionParameters, boundaryTagLabelSettings) && boundaryTagLabelSettings.useTargetTagLabels(), configFile,
					wordKeyMappingTable, reorderingLabelSmoothedRuleSetsCreater, mosesUnknownWordsRuleVariantsCreater, mosesPhrasePairRuleVariantsCreater);

		}
		throw new RuntimeException("Error : createBoundaryTagRuleLabelsSmoother: Unrecognized label smoothing type");
	}

	private static boolean useSourceLabels(ComplexLabelsGrammarExtractionParameters complexLabelGrammarExtractionParameters, BoundaryTagLabelingSettings boundaryTagLabelSettings) {
		return boundaryTagLabelSettings.useSourceTagLabels() || complexLabelGrammarExtractionParameters.samtGrammarExtractionConstraints.useReorderingLabelExtension();
	}

	private static RuleLabelsSmoother createFullLabelSmoother(boolean useSourceAndTargetLabels, ConfigFile configFile, WordKeyMappingTable wordKeyMappingTable,
			AlternativelyLabeledRuleSetsListCreater reorderingLabelSmoothedRuleSetsCreater, AlternativelyLabeledRuleSetsListCreater mosesUnknownWordsRuleVariantsCreater,
			AlternativelyLabeledRuleSetsListCreater mosesPhrasePairRuleVariantsCreater) {
		if (useSourceAndTargetLabels && stripSourcePartLabelsForSmoothingDoubleLabeldRules(configFile)) {
			return SourceAndTargetLabeledRulesSmoother.crateSourceAndTargetLabeledRulesSmoother(wordKeyMappingTable, reorderingLabelSmoothedRuleSetsCreater, mosesUnknownWordsRuleVariantsCreater,
					mosesPhrasePairRuleVariantsCreater);
		} else {
			return SourceOrTargetLabeledRulesSmoother.crateSourceOrTargetLabeledRulesSmoother(wordKeyMappingTable, reorderingLabelSmoothedRuleSetsCreater, mosesUnknownWordsRuleVariantsCreater,
					mosesPhrasePairRuleVariantsCreater);
		}
	}

	public static RuleLabelsSmoother createSAMTRuleLabelsSmoother(ConfigFile configFile, ComplexLabelsGrammarExtractionParameters complexLabelGrammarExtractionParameters, SystemIdentity systemIdentity) {
		LABEL_SMOOTHING_TYPE labelSmoothingType = LabeledRulesSmoother.getLabelSmoothingType(configFile);

		WordKeyMappingTable wordKeyMappingTable = createWordKeyMappingTable(complexLabelGrammarExtractionParameters.getMultiThreadComputationParameters(),
				MTGrammarExtractionConfig.getGrammerSourceFilterPath(configFile));
		AlternativelyLabeledRuleSetsListCreater reorderingLabelSmoothedRuleSetsCreater = createReorderingLabelSmoothedRuleSetsCreater(configFile, wordKeyMappingTable);
		AlternativelyLabeledRuleSetsListCreater mosesUnknownWordsRuleVariantsCreater = createMosesUnknownWordsRuleVariantsCreater(systemIdentity, wordKeyMappingTable, configFile);
		AlternativelyLabeledRuleSetsListCreater mosesPhrasePairRuleVariantsCreater = createMosesPhrasePairRuleVariantsCreater(systemIdentity, wordKeyMappingTable, configFile);
		if (labelSmoothingType.equals(LABEL_SMOOTHING_TYPE.NO_LABEL_SMOOTHING)) {
			// return
			// NoLabelSmoothingRulesSmoother.createNoLabelSmoothingRulesSmootherForFilteredGrammarExtraction(reorderingLabelSmoothedRuleSetsCreater,
			// wordKeyMappingTable, mosesUnknownWordsRuleVariantsCreater,
			// mosesPhrasePairRuleVariantsCreater);
			return createNoLabelSmoothingRuleSmoother(configFile, labelSmoothingType, wordKeyMappingTable, systemIdentity, true);
		} else if (labelSmoothingType.equals(LABEL_SMOOTHING_TYPE.HIERO_ONLY_LABEL_SMOOTHING) || (!systemIdentity.labelBothSides())) {
			return HieroSmoothingOnlyRulesSmoother.createHieroSmoothingOnlyRulesSmoother(wordKeyMappingTable, reorderingLabelSmoothedRuleSetsCreater, mosesUnknownWordsRuleVariantsCreater,
					mosesPhrasePairRuleVariantsCreater);
		} else if (labelSmoothingType.equals(LABEL_SMOOTHING_TYPE.FULL_LABEL_SMOOTHING)) {
			return createFullLabelSmoother(useSourceLabels(complexLabelGrammarExtractionParameters), configFile, wordKeyMappingTable, reorderingLabelSmoothedRuleSetsCreater,
					mosesUnknownWordsRuleVariantsCreater, mosesPhrasePairRuleVariantsCreater);
		}
		throw new RuntimeException("Error : createSAMTRuleLabelsSmoother: Unrecognized label smoothing type: " + labelSmoothingType);
	}

	private static boolean useSourceLabels(ComplexLabelsGrammarExtractionParameters complexLabelGrammarExtractionParameters) {
		return complexLabelGrammarExtractionParameters.samtGrammarExtractionConstraints.useReorderingLabelExtension();
	}

	public static List<String> getSmootingPrefixesSAMTSystem(SystemIdentity systemIdentity) {
		LABEL_SMOOTHING_TYPE labelSmoothingType = systemIdentity.getLabSmoothingType();
		boolean useReorderingLabelExtension = systemIdentity.useReorderingLabelExtension();
		if (labelSmoothingType.equals(LABEL_SMOOTHING_TYPE.NO_LABEL_SMOOTHING)) {
			return NoLabelSmoothingRulesSmoother.getSmoothingLabelPrefixStringsNoSmoothing();
		} else if (labelSmoothingType.equals(LABEL_SMOOTHING_TYPE.HIERO_ONLY_LABEL_SMOOTHING) || (!systemIdentity.labelBothSides())) {
			return HieroSmoothingOnlyRulesSmoother.getSmoothingLabelPrefixStringsHieroOnly();
		} else if (labelSmoothingType.equals(LABEL_SMOOTHING_TYPE.FULL_LABEL_SMOOTHING)) {
			if (useReorderingLabelExtension) {
				return SourceAndTargetLabeledRulesSmoother.getSmoothingLabelPrefixStringsSourceAndTarget();
			} else {
				return SourceOrTargetLabeledRulesSmoother.getSmoothingLabelPrefixStringsSourceOrTarget();
			}
		}
		throw new RuntimeException("Error : createSAMTRuleLabelsSmoother: Unrecognized label smoothing type");
	}

	protected LightWeightPhrasePairRuleSet getBasicSmoothedRulesSet(LightWeightPhrasePairRuleSet originalRuleSet) {
		return getSmoothedRulesSet(originalRuleSet, LABELED_RULE_BASIC_SMOOTHER);
	}

	protected LightWeightPhrasePairRuleSet getSourceSmoothedRulesSet(LightWeightPhrasePairRuleSet originalRuleSet) {
		return getSmoothedRulesSet(originalRuleSet, LABELED_RULE_SOURCE_SMOOHTER);
	}

	protected LightWeightPhrasePairRuleSet getTargetSmoothedRulesSet(LightWeightPhrasePairRuleSet originalRuleSet) {
		return getSmoothedRulesSet(originalRuleSet, LABELED_RULE_TARGET_SMOOTHER);
	}

	protected LightWeightPhrasePairRuleSet getFullSmoothedRulesSet(LightWeightPhrasePairRuleSet originalRuleSet) {
		return getSmoothedRulesSet(originalRuleSet, LABELD_RUlE_FULL_SMOOTHER);
	}

	public WordKeyMappingTable getWordKeyMappingTable() {
		return this.wordKeyMappingTable;
	}

	protected static WordKeyMappingTable createWordKeyMappingTable(MultiThreadComputationParameters parameters, String filterName) {
		// The word file names are the source file, the target file and the
		// testFilter file
		List<String> wordFileNames = Arrays.asList(parameters.getSourceFileName(), parameters.getTargetFileName(), filterName);
		WordKeyMappingTable wordKeyMappingTable = WordKeyMappingTable.createWordKeyMappingTable(wordFileNames, 2, parameters.getNumThreads());
		return wordKeyMappingTable;
	}

	public static WordKeyMappingTable createWordKeyMappingTable(MultiThreadComputationParameters parameters) {
		List<String> wordFileNames = Arrays.asList(parameters.getSourceFileName(), parameters.getTargetFileName());
		WordKeyMappingTable wordKeyMappingTable = WordKeyMappingTable.createWordKeyMappingTable(wordFileNames, 2, parameters.getNumThreads());
		return wordKeyMappingTable;
	}

	public static List<String> getSmoothingPrefixesHieroSystem(SystemIdentity systemIdentity) {
		LABEL_SMOOTHING_TYPE labelSmoothingType = systemIdentity.getLabSmoothingType();
		boolean useReorderingLabelExtension = systemIdentity.useReorderingLabelExtension();
		if (labelSmoothingType.equals(LABEL_SMOOTHING_TYPE.NO_LABEL_SMOOTHING) || (!useReorderingLabelExtension)) {
			return NoLabelSmoothingRulesSmoother.getSmoothingLabelPrefixStringsNoSmoothing();
		} else if (labelSmoothingType.equals(LABEL_SMOOTHING_TYPE.HIERO_ONLY_LABEL_SMOOTHING) || (!systemIdentity.labelBothSides())) {
			return HieroSmoothingOnlyRulesSmoother.getSmoothingLabelPrefixStringsHieroOnly();
		} else if (labelSmoothingType.equals(LABEL_SMOOTHING_TYPE.FULL_LABEL_SMOOTHING)) {
			return SourceOrTargetLabeledRulesSmoother.getSmoothingLabelPrefixStringsSourceOrTarget();
		}
		throw new RuntimeException("Error : createSAMTRuleLabelsSmoother: Unrecognized label smoothing type");
	}

	@Override
	public List<LightWeightPhrasePairRuleSet> createListOfOriginalAndLabelSmoothedRuleSets(LightWeightPhrasePairRuleSet originalRuleSet) {
		List<LightWeightPhrasePairRuleSet> result = new ArrayList<LightWeightPhrasePairRuleSet>();
		result.add(originalRuleSet);
		result.addAll(this.reorderingLabelSmoothedRuleSetsCreater.createAlternativelyLabeledLightWeightRuleSets(originalRuleSet));
		result.addAll(this.mosesUnknownWordsRuleVariantsCreater.createAlternativelyLabeledLightWeightRuleSets(originalRuleSet));
		result.addAll(this.mosesPhrasePairRuleVariantsCreater.createAlternativelyLabeledLightWeightRuleSets(originalRuleSet));
		return result;
	}
}
