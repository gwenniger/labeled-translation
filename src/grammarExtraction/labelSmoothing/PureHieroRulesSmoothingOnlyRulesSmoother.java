package grammarExtraction.labelSmoothing;

import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

import java.util.Collections;
import java.util.List;

/**
 * This smoother makes sure that Hiero versions of rules (unlabeled) are added to the Trie, 
 * but without adding them as extra smoothing weights to the produced grammar. 
 * This is needed in the scenario that we want to add a Hiero version of 
 * the rules in addition to the labeled version, or alternatively we wan to use the phrase weights 
 * for the Hiero version of the rules in place of the phrase weights version using the labels, 
 * in case we work with canonically labeled rules for example.
 *
 */
public class PureHieroRulesSmoothingOnlyRulesSmoother extends HieroRuleSmoother {

	protected PureHieroRulesSmoothingOnlyRulesSmoother(
			WordKeyMappingTable wordKeyMappingTable,
			AlternativelyLabeledRuleSetsListCreater reorderingLabelSmoothedRuleSetsCreater,
			AlternativelyLabeledRuleSetsListCreater mosesUnknownWordsRuleVariantsCreater,
			AlternativelyLabeledRuleSetsListCreater mosesPhrasePairRuleVariantsCreater) {
		super(wordKeyMappingTable, reorderingLabelSmoothedRuleSetsCreater,
				mosesUnknownWordsRuleVariantsCreater,
				mosesPhrasePairRuleVariantsCreater);
	}

	public static PureHieroRulesSmoothingOnlyRulesSmoother createPureHieroRulesSmoothingOnlyRulesSmoother(
			WordKeyMappingTable wordKeyMappingTable,
			AlternativelyLabeledRuleSetsListCreater reorderingLabelSmoothedRuleSetsCreater,
			AlternativelyLabeledRuleSetsListCreater mosesUnknownWordsRuleVariantsCreater,
			AlternativelyLabeledRuleSetsListCreater mosesPhrasePairRuleVariantsCreater) {
		return new PureHieroRulesSmoothingOnlyRulesSmoother(
				wordKeyMappingTable, reorderingLabelSmoothedRuleSetsCreater,
				mosesUnknownWordsRuleVariantsCreater,
				mosesPhrasePairRuleVariantsCreater);
	}

	@Override
	public List<TranslationRuleSignatureTriple> createSmoothingTranslationRuleSignatureTriples(
			TranslationRuleSignatureTriple originalTranslationRuleSignatureTriple) {
		return Collections.emptyList();
	}

	@Override
	public List<String> getSmoothingLabelPrefixStrings() {
		return Collections.emptyList();
	}

	@Override
	public int getNoLabelSmoothingFeatures() {
		return 0;
	}

}
