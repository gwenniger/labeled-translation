package grammarExtraction.labelSmoothing;

import grammarExtraction.translationRules.LightWeightRuleLabel;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

public class LabeledRuleBasicSmoother extends LabeledRuleSmoother {

	private LightWeightRuleLabel createBasicSmoothedRuleLabel(TranslationRuleSignatureTriple translationRuleSignatureTriple, LabeledRulesSmoother labeledRulesSmoother) {
		LightWeightRuleLabel originalLabel = translationRuleSignatureTriple.getRuleLabel();
		return LightWeightRuleLabel.createLightWeightRuleLabel(labeledRulesSmoother.createBasicSmoothedLabelSideRepresentation(originalLabel.getSourceSideLabel()),
				labeledRulesSmoother.createBasicSmoothedLabelSideRepresentation(originalLabel.getTargetSideLabel()));
	}

	@Override
	public TranslationRuleSignatureTriple createSmoothedTranslationRuleSignatureTriple(TranslationRuleSignatureTriple translationRuleSignatureTriple, LabeledRulesSmoother labeledRulesSmoother) {
		return TranslationRuleSignatureTriple.createTranslationRuleSignatureTriple(
				labeledRulesSmoother.createBasicSmoothedRuleSideRepresentation(translationRuleSignatureTriple.getSourceSideRepresentation()),
				labeledRulesSmoother.createBasicSmoothedRuleSideRepresentation(translationRuleSignatureTriple.getTargetSideRepresentation()),
				createBasicSmoothedRuleLabel(translationRuleSignatureTriple, labeledRulesSmoother));
	}
	/*
	 * @Override public TranslationEquivalenceItem createSmoothedTranslationEquivalenceItem(TranslationEquivalenceItem
	 * translationEquivalenceItem,LabeledRulesSmoother labeledRulesSmoother) {
	 * 
	 * }
	 */

}
