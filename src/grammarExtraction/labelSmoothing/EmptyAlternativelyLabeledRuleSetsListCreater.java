package grammarExtraction.labelSmoothing;

import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;

import java.util.Collections;
import java.util.List;

public class EmptyAlternativelyLabeledRuleSetsListCreater implements AlternativelyLabeledRuleSetsListCreater {

	public static EmptyAlternativelyLabeledRuleSetsListCreater createEmptyAlternativelyLabeledRuleSetsListCreater() {
		return new EmptyAlternativelyLabeledRuleSetsListCreater();
	}

	@Override
	public List<LightWeightPhrasePairRuleSet> createAlternativelyLabeledLightWeightRuleSets(LightWeightPhrasePairRuleSet originalRuleSet) {
		return Collections.emptyList();
	}

}
