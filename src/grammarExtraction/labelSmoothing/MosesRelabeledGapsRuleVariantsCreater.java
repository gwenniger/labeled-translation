package grammarExtraction.labelSmoothing;

import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;
import grammarExtraction.reorderingLabeling.ReorderingLabelSmoothedRuleSetsCreater;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.LightWeightTranslationRule;
import grammarExtraction.translationRules.RuleGap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import junit.framework.Assert;
import util.Pair;

public abstract class MosesRelabeledGapsRuleVariantsCreater implements AlternativelyLabeledRuleSetsListCreater {

	protected final WordKeyMappingTable wordKeyMappingTable;
	private final Map<Integer, Pair<Integer>> firstGapLabelReplacedByNewLabelMap;
	private final Map<Integer, Pair<Integer>> secondGapLabelReplacedByNewLabelMap;
	private final Map<Integer, Pair<Integer>> bothGapLabelsReplacedByNewLabelMap;

	protected MosesRelabeledGapsRuleVariantsCreater(WordKeyMappingTable wordKeyMappingTable, Map<Integer, Pair<Integer>> firstGapLabelReplacedByNewLabelMap,
			Map<Integer, Pair<Integer>> secondGapLabelReplacedByNewLabelMap, Map<Integer, Pair<Integer>> bothGapLabelsReplacedByNewLabelMap) {
		this.wordKeyMappingTable = wordKeyMappingTable;
		this.firstGapLabelReplacedByNewLabelMap = firstGapLabelReplacedByNewLabelMap;
		this.secondGapLabelReplacedByNewLabelMap = secondGapLabelReplacedByNewLabelMap;
		this.bothGapLabelsReplacedByNewLabelMap = bothGapLabelsReplacedByNewLabelMap;
	}

	private LightWeightTranslationRule createRelabeledTranslationRule(LightWeightTranslationRule translationRule, Map<Integer, Pair<Integer>> gapIndexToNewLabelKeysPairMap) {
		if (translationRule != null) {
			return translationRule.createLightWeightTranslationRuleWithAdaptedGapLabels(gapIndexToNewLabelKeysPairMap, wordKeyMappingTable);
		}
		return null;
	}

	private List<LightWeightTranslationRule> createTranslationRuleListWithAdaptedGapLabels(List<LightWeightTranslationRule> originalRules, Map<Integer, Pair<Integer>> gapIndexToNewLabelKeysPairMap) {
		List<LightWeightTranslationRule> result = new ArrayList<LightWeightTranslationRule>();
		for (LightWeightTranslationRule originalRule : originalRules) {
			result.add(createRelabeledTranslationRule(originalRule, gapIndexToNewLabelKeysPairMap));
		}
		return result;
	}

	private LightWeightPhrasePairRuleSet createLightWeightPhrasePairRuleSetWithNewGapLabeling(LightWeightPhrasePairRuleSet originalRuleSet, Map<Integer, Pair<Integer>> gapIndexToNewLabelKeysPairMap) {

		LightWeightTranslationRule newBaseRule = createRelabeledTranslationRule(originalRuleSet.getBaseTranslationRule(), gapIndexToNewLabelKeysPairMap);
		List<LightWeightTranslationRule> newOneGapRules = createTranslationRuleListWithAdaptedGapLabels(originalRuleSet.getOneGapTranslationRules(), gapIndexToNewLabelKeysPairMap);
		List<LightWeightTranslationRule> newTwoGapRules = createTranslationRuleListWithAdaptedGapLabels(originalRuleSet.getTwoGapTranslationRules(), gapIndexToNewLabelKeysPairMap);
		ReorderingLabelSmoothedRuleSetsCreater.assertNoNullElements(newOneGapRules);
		ReorderingLabelSmoothedRuleSetsCreater.assertNoNullElements(newTwoGapRules);
		LightWeightPhrasePairRuleSet result = LightWeightPhrasePairRuleSet.createLightWeightPhrasePairRuleSet(newBaseRule, newOneGapRules, newTwoGapRules, originalRuleSet.getLeftHandSideLabel(),
				getRelabeledRuleSetWeight());
		Assert.assertTrue(result.getRuleSetWeight() <= 1);
		return result;
	}

	protected abstract double getRelabeledRuleSetWeight();
	public abstract boolean ruleSetIsApplicableForRelabeling(LightWeightPhrasePairRuleSet originalRuleSet);

	private List<LightWeightPhrasePairRuleSet> createMosesUnknownWordsRuleVariantsLightWeightRuleSets(LightWeightPhrasePairRuleSet originalRuleSet) {
		List<LightWeightPhrasePairRuleSet> result = new ArrayList<LightWeightPhrasePairRuleSet>();
		result.add(createLightWeightPhrasePairRuleSetWithNewGapLabeling(originalRuleSet, this.firstGapLabelReplacedByNewLabelMap));
		result.add(createLightWeightPhrasePairRuleSetWithNewGapLabeling(originalRuleSet, this.secondGapLabelReplacedByNewLabelMap));
		result.add(createLightWeightPhrasePairRuleSetWithNewGapLabeling(originalRuleSet, this.bothGapLabelsReplacedByNewLabelMap));
		return result;
	}

	@Override
	public List<LightWeightPhrasePairRuleSet> createAlternativelyLabeledLightWeightRuleSets(LightWeightPhrasePairRuleSet originalRuleSet) {
		return createMosesUnknownWordsRuleVariantsLightWeightRuleSets(originalRuleSet);
	}

	public static List<Map<Integer, Pair<Integer>>> createReplacedGapMapsList(WordKeyMappingTable wordKeyMappingTable, String gapLabel) {

		String relabeledGapOneString = RuleGap.ruleGapString(gapLabel, 0);
		String relabeledGapTwoString = RuleGap.ruleGapString(gapLabel, 1);
		Integer relabeledGapOneKey = wordKeyMappingTable.getKeyForWord(relabeledGapOneString);
		Integer relabeledGapTwoKey = wordKeyMappingTable.getKeyForWord(relabeledGapTwoString);

		Pair<Integer> chiangGapOneLabelPair = new Pair<Integer>(relabeledGapOneKey, relabeledGapOneKey);
		Pair<Integer> chiangGapTwoLabelPair = new Pair<Integer>(relabeledGapTwoKey, relabeledGapTwoKey);

		Map<Integer, Pair<Integer>> firstGapLabelReplacedByXMap = new HashMap<Integer, Pair<Integer>>();
		firstGapLabelReplacedByXMap.put(1, chiangGapOneLabelPair);
		Map<Integer, Pair<Integer>> secondGapLabelReplacedByXMap = new HashMap<Integer, Pair<Integer>>();
		secondGapLabelReplacedByXMap.put(2, chiangGapTwoLabelPair);
		Map<Integer, Pair<Integer>> bothGapLabelsReplacedByXMap = new HashMap<Integer, Pair<Integer>>();
		bothGapLabelsReplacedByXMap.put(1, chiangGapOneLabelPair);
		bothGapLabelsReplacedByXMap.put(2, chiangGapTwoLabelPair);

		return Arrays.asList(firstGapLabelReplacedByXMap, secondGapLabelReplacedByXMap, bothGapLabelsReplacedByXMap);
	}

}
