package grammarExtraction.labelSmoothing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import grammarExtraction.IntegerKeyRuleRepresentation;
import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

public abstract class HieroRuleSmoother extends LabeledRulesSmoother{

	protected HieroRuleSmoother(
			WordKeyMappingTable wordKeyMappingTable,
			AlternativelyLabeledRuleSetsListCreater reorderingLabelSmoothedRuleSetsCreater,
			AlternativelyLabeledRuleSetsListCreater mosesUnknownWordsRuleVariantsCreater,
			AlternativelyLabeledRuleSetsListCreater mosesPhrasePairRuleVariantsCreater) {
		super(wordKeyMappingTable, reorderingLabelSmoothedRuleSetsCreater,
				mosesUnknownWordsRuleVariantsCreater,
				mosesPhrasePairRuleVariantsCreater);
	}

	@Override
	public List<LightWeightPhrasePairRuleSet> getAllLabelSmoothedRuleSets(LightWeightPhrasePairRuleSet originalRuleSet) {
		List<LightWeightPhrasePairRuleSet> result = new ArrayList<LightWeightPhrasePairRuleSet>();
		result.add(getFullSmoothedRulesSet(originalRuleSet));
		return result;
	}

	@Override
	protected IntegerKeyRuleRepresentation createBasicSmoothedRuleSideRepresentation(IntegerKeyRuleRepresentation originalRuleRepresentation) {
		return originalRuleRepresentation;
	}

	@Override
	protected Integer createBasicSmoothedLabelSideRepresentation(Integer labelSide) {
		return labelSide;
	}

	@Override
	public List<LightWeightPhrasePairRuleSet> getAllButFullySmoothedLabelSmoothedRuleSets(LightWeightPhrasePairRuleSet originalRuleSet) {
		return Collections.emptyList();
	}

	@Override
	public LightWeightPhrasePairRuleSet getFullySmoothedLabelSmoothedRuleSet(LightWeightPhrasePairRuleSet originalRuleSet) {
		return getFullSmoothedRulesSet(originalRuleSet);
	}
}
