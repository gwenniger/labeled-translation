package grammarExtraction.labelSmoothing;

import grammarExtraction.chiangGrammarExtraction.ChiangRuleGapCreater;
import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import java.util.List;
import java.util.Map;
import util.Pair;

public class MosesXPhrasePairRuleVariantsCreater extends MosesRelabeledGapsRuleVariantsCreater {

	private static final double X_PHRASE_PAIR_RULE_SETS_WEIGTH = 1;
	private static final String X_PHRASE_PAIR_LABEL_TAG = ChiangRuleGapCreater.getReorderingLabelPhrasePairLabel();

	protected MosesXPhrasePairRuleVariantsCreater(WordKeyMappingTable wordKeyMappingTable, Map<Integer, Pair<Integer>> firstGapLabelReplacedByNewLabelMap,
			Map<Integer, Pair<Integer>> secondGapLabelReplacedByNewLabelMap, Map<Integer, Pair<Integer>> bothGapLabelsReplacedByNewLabelMap) {
		super(wordKeyMappingTable, firstGapLabelReplacedByNewLabelMap, secondGapLabelReplacedByNewLabelMap, bothGapLabelsReplacedByNewLabelMap);
	}

	public static MosesXPhrasePairRuleVariantsCreater createMosesXPhrasePairRuleVariantsCreater(WordKeyMappingTable wordKeyMappingTable) {
		List<Map<Integer, Pair<Integer>>> replacedGapMapsList = createReplacedGapMapsList(wordKeyMappingTable, X_PHRASE_PAIR_LABEL_TAG);
		return new MosesXPhrasePairRuleVariantsCreater(wordKeyMappingTable, replacedGapMapsList.get(0), replacedGapMapsList.get(1), replacedGapMapsList.get(2));
	}

	@Override
	protected double getRelabeledRuleSetWeight() {
		return X_PHRASE_PAIR_RULE_SETS_WEIGTH;
	}

	@Override
	public boolean ruleSetIsApplicableForRelabeling(LightWeightPhrasePairRuleSet originalRuleSet) {
		return true;
	}
}
