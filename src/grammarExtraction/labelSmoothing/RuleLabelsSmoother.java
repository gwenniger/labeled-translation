package grammarExtraction.labelSmoothing;

import java.util.List;

import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

public interface RuleLabelsSmoother {

	public List<LightWeightPhrasePairRuleSet> getAllLabelSmoothedRuleSets(LightWeightPhrasePairRuleSet originalRuleSet);

	public List<LightWeightPhrasePairRuleSet> getAllButFullySmoothedLabelSmoothedRuleSets(LightWeightPhrasePairRuleSet originalRuleSet);

	public LightWeightPhrasePairRuleSet getFullySmoothedLabelSmoothedRuleSet(LightWeightPhrasePairRuleSet originalRuleSet);
	
	public WordKeyMappingTable getWordKeyMappingTable();
	
	public List<TranslationRuleSignatureTriple> createSmoothingTranslationRuleSignatureTriples(TranslationRuleSignatureTriple originalTranslationRuleSignatureTriple);
	
	public List<String> getSmoothingLabelPrefixStrings();
	
	public int getNoLabelSmoothingFeatures();
	
	public List<LightWeightPhrasePairRuleSet> createListOfOriginalAndLabelSmoothedRuleSets(LightWeightPhrasePairRuleSet originalRuleSet);
}
