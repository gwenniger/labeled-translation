package grammarExtraction.labelSmoothing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import grammarExtraction.IntegerKeyRuleRepresentation;
import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

public class SourceOrTargetLabeledRulesSmoother extends LabeledRulesSmoother {

	private SourceOrTargetLabeledRulesSmoother(WordKeyMappingTable wordKeyMappingTable, AlternativelyLabeledRuleSetsListCreater reorderingLabelSmoothedRuleSetsCreater,AlternativelyLabeledRuleSetsListCreater mosesUnknownWordsRuleVariantsCreater,AlternativelyLabeledRuleSetsListCreater mosesPhrasePairRuleVariantsCreater) {
		super(wordKeyMappingTable, reorderingLabelSmoothedRuleSetsCreater,mosesUnknownWordsRuleVariantsCreater,mosesPhrasePairRuleVariantsCreater);
	}

	public static SourceOrTargetLabeledRulesSmoother crateSourceOrTargetLabeledRulesSmoother(WordKeyMappingTable wordKeyMappingTable,
			AlternativelyLabeledRuleSetsListCreater reorderingLabelSmoothedRuleSetsCreater,AlternativelyLabeledRuleSetsListCreater mosesUnknownWordsRuleVariantsCreater,AlternativelyLabeledRuleSetsListCreater mosesPhrasePairRuleVariantsCreater) {
		return new SourceOrTargetLabeledRulesSmoother(wordKeyMappingTable, reorderingLabelSmoothedRuleSetsCreater,mosesUnknownWordsRuleVariantsCreater,mosesPhrasePairRuleVariantsCreater);
	}

	@Override
	public List<LightWeightPhrasePairRuleSet> getAllLabelSmoothedRuleSets(LightWeightPhrasePairRuleSet originalRuleSet) {
		List<LightWeightPhrasePairRuleSet> result = getAllButFullySmoothedLabelSmoothedRuleSets(originalRuleSet);
		result.add(getFullSmoothedRulesSet(originalRuleSet));
		return result;
	}

	@Override
	protected IntegerKeyRuleRepresentation createBasicSmoothedRuleSideRepresentation(IntegerKeyRuleRepresentation originalRuleRepresentation) {
		return originalRuleRepresentation;
	}

	@Override
	protected Integer createBasicSmoothedLabelSideRepresentation(Integer labelSide) {
		return labelSide;
	}

	@Override
	public List<LightWeightPhrasePairRuleSet> getAllButFullySmoothedLabelSmoothedRuleSets(LightWeightPhrasePairRuleSet originalRuleSet) {
		List<LightWeightPhrasePairRuleSet> result = new ArrayList<LightWeightPhrasePairRuleSet>();
		result.add(getSourceSmoothedRulesSet(originalRuleSet));
		result.add(getTargetSmoothedRulesSet(originalRuleSet));
		return result;
	}

	@Override
	public LightWeightPhrasePairRuleSet getFullySmoothedLabelSmoothedRuleSet(LightWeightPhrasePairRuleSet originalRuleSet) {
		return getFullSmoothedRulesSet(originalRuleSet);
	}

	@Override
	public int getNoLabelSmoothingFeatures() {
		return 6;
	}

	@Override
	public List<TranslationRuleSignatureTriple> createSmoothingTranslationRuleSignatureTriples(TranslationRuleSignatureTriple originalTranslationRuleSignatureTriple) {
		List<TranslationRuleSignatureTriple> result = new ArrayList<TranslationRuleSignatureTriple>();
		result.add(LABELED_RULE_SOURCE_SMOOHTER.createSmoothedTranslationRuleSignatureTriple(originalTranslationRuleSignatureTriple, this));
		result.add(LABELED_RULE_TARGET_SMOOTHER.createSmoothedTranslationRuleSignatureTriple(originalTranslationRuleSignatureTriple, this));
		result.add(LABELD_RUlE_FULL_SMOOTHER.createSmoothedTranslationRuleSignatureTriple(originalTranslationRuleSignatureTriple, this));
		return result;
	}

	public static List<String> getSmoothingLabelPrefixStringsSourceOrTarget() {
		return Arrays.asList(SOURCE_LABEL_SMOOTHED_STRING, TARGET_LABEL_SMOOTHED_STRING, FULL_LABEL_SMOOTHED_STRING);
	}
	
	@Override
	public List<String> getSmoothingLabelPrefixStrings() {
		return getSmoothingLabelPrefixStringsSourceOrTarget();
	}
}
