package grammarExtraction.labelSmoothing;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import alignmentStatistics.MultiThreadComputationParameters;

import grammarExtraction.IntegerKeyRuleRepresentation;
import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;
import grammarExtraction.reorderingLabeling.ReorderingLabelSmoothedRuleSetsCreater;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

public class NoLabelSmoothingRulesSmoother extends LabeledRulesSmoother implements Serializable {

	private static final long serialVersionUID = 1L;
	// In certain cases we want this class to write a fully smoothed (Hiero) version of rules,
	// even if we do not want to add smoothing features to the extracted rules. This is the case 
	// when working with canonical labeled rules, which rely on the presence of unlabeled versions of 
	// the rules to work. This boolean flag serves to deal with this case
	private final boolean writeFullSmoothedRuleSetForNonSmoothingPurposes;

	public NoLabelSmoothingRulesSmoother(WordKeyMappingTable wordKeyMappingTable, AlternativelyLabeledRuleSetsListCreater reorderingLabelSmoothedRuleSetsCreater,AlternativelyLabeledRuleSetsListCreater mosesUnknownWordsRuleVariantsCreater,AlternativelyLabeledRuleSetsListCreater mosesPhrasePairRuleVariantsCreater, boolean  writeFullSmoothedRuleSetForNonSmoothingPurposes) {
		super(wordKeyMappingTable, reorderingLabelSmoothedRuleSetsCreater,mosesUnknownWordsRuleVariantsCreater, mosesPhrasePairRuleVariantsCreater);
		this.writeFullSmoothedRuleSetForNonSmoothingPurposes =  writeFullSmoothedRuleSetForNonSmoothingPurposes;
	}

	public static NoLabelSmoothingRulesSmoother createNoLabelSmoothingRulesSmoother(MultiThreadComputationParameters parameters,
			ReorderingLabelSmoothedRuleSetsCreater reorderingLabelSmoothedRuleSetsCreater,AlternativelyLabeledRuleSetsListCreater mosesUnknownWordsRuleVariantsCreater,AlternativelyLabeledRuleSetsListCreater mosesPhrasePairRuleVariantsCreater, boolean  writeFullSmoothedRuleSetForNonSmoothingPurposes) {
		return new NoLabelSmoothingRulesSmoother(createWordKeyMappingTable(parameters), reorderingLabelSmoothedRuleSetsCreater,mosesUnknownWordsRuleVariantsCreater,mosesPhrasePairRuleVariantsCreater, writeFullSmoothedRuleSetForNonSmoothingPurposes);
	}

	public static NoLabelSmoothingRulesSmoother createNoLabelSmoothingRulesSmootherForFilteredGrammarExtraction(AlternativelyLabeledRuleSetsListCreater reorderingLabelSmoothedRuleSetsCreater,
			WordKeyMappingTable wordKeyMappingTable,AlternativelyLabeledRuleSetsListCreater mosesUnknownWordsRuleVariantsCreater,AlternativelyLabeledRuleSetsListCreater mosesPhrasePairRuleVariantsCreater, boolean useCanonicalFormLabeledRulesAndUseLabeling) {
		return new NoLabelSmoothingRulesSmoother(wordKeyMappingTable, reorderingLabelSmoothedRuleSetsCreater,mosesUnknownWordsRuleVariantsCreater,mosesPhrasePairRuleVariantsCreater,useCanonicalFormLabeledRulesAndUseLabeling);
	}

	public static NoLabelSmoothingRulesSmoother createNoLabelSmoothingNoReorderingRulesSmoothingSmoother(WordKeyMappingTable wordKeyMappingTable,AlternativelyLabeledRuleSetsListCreater mosesUnknownWordsRuleVariantsCreater,AlternativelyLabeledRuleSetsListCreater mosesPhrasePairRuleVariantsCreater, boolean  writeFullSmoothedRuleSetForNonSmoothingPurposes) {
		return new NoLabelSmoothingRulesSmoother(wordKeyMappingTable, EmptyAlternativelyLabeledRuleSetsListCreater.createEmptyAlternativelyLabeledRuleSetsListCreater(),mosesUnknownWordsRuleVariantsCreater,mosesPhrasePairRuleVariantsCreater, writeFullSmoothedRuleSetForNonSmoothingPurposes);
	}

	@Override
	public List<LightWeightPhrasePairRuleSet> getAllLabelSmoothedRuleSets(LightWeightPhrasePairRuleSet originalRuleSet) {
		return Collections.emptyList();
	}

	@Override
	protected IntegerKeyRuleRepresentation createBasicSmoothedRuleSideRepresentation(IntegerKeyRuleRepresentation originalRuleRepresentation) {
		return null;
	}

	@Override
	protected Integer createBasicSmoothedLabelSideRepresentation(Integer labelSide) {
		return null;
	}

	@Override
	public List<LightWeightPhrasePairRuleSet> getAllButFullySmoothedLabelSmoothedRuleSets(LightWeightPhrasePairRuleSet originalRuleSet) {
		return Collections.emptyList();
	}

	@Override
	public LightWeightPhrasePairRuleSet getFullySmoothedLabelSmoothedRuleSet(LightWeightPhrasePairRuleSet originalRuleSet) {
		if(writeFullSmoothedRuleSetForNonSmoothingPurposes){
			return super.getFullSmoothedRulesSet(originalRuleSet);
		}
		return null;
	}

	@Override
	public int getNoLabelSmoothingFeatures() {
		return 0;
	}

	@Override
	public List<TranslationRuleSignatureTriple> createSmoothingTranslationRuleSignatureTriples(TranslationRuleSignatureTriple originalTranslationRuleSignatureTriple) {
		return Collections.emptyList();
	}
	
	public static List<String> getSmoothingLabelPrefixStringsNoSmoothing() {
		return Collections.emptyList();
	}

	@Override
	public List<String> getSmoothingLabelPrefixStrings() {
		return getSmoothingLabelPrefixStringsNoSmoothing();
	}

}
