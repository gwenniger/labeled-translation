package grammarExtraction.labelSmoothing;

import grammarExtraction.translationRules.LightWeightRuleLabel;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;

public class LabeledRuleSourceSmoother extends LabeledRuleSmoother {

	private LightWeightRuleLabel createSourceSmoothedRuleLabel(TranslationRuleSignatureTriple translationRuleSignatureTriple, LabeledRulesSmoother labeledRulesSmoother) {
		LightWeightRuleLabel originalLabel = translationRuleSignatureTriple.getRuleLabel();
		return LightWeightRuleLabel.createLightWeightRuleLabel(labeledRulesSmoother.wordKeyMappingTable.getFullyDowngradedLabelKey(originalLabel.getSourceSideLabel()),
				labeledRulesSmoother.createBasicSmoothedLabelSideRepresentation(originalLabel.getTargetSideLabel()));
	}

	@Override
	public TranslationRuleSignatureTriple createSmoothedTranslationRuleSignatureTriple(TranslationRuleSignatureTriple translationRuleSignatureTriple, LabeledRulesSmoother labeledRulesSmoother) {
		return TranslationRuleSignatureTriple.createTranslationRuleSignatureTriple(
				labeledRulesSmoother.wordKeyMappingTable.getFullyDowngradedLabelsRuleRepresentation(translationRuleSignatureTriple.getSourceSideRepresentation()),
				labeledRulesSmoother.createBasicSmoothedRuleSideRepresentation(translationRuleSignatureTriple.getTargetSideRepresentation()),
				createSourceSmoothedRuleLabel(translationRuleSignatureTriple, labeledRulesSmoother));
	}

	/*
	 * @Override public TranslationEquivalenceItem createSmoothedTranslationEquivalenceItem(TranslationEquivalenceItem translationEquivalenceItem,
	 * LabeledRulesSmoother labeledRulesSmoother) { return TranslationEquivalenceItem.createTranslationEquivalenceItem( ); }
	 */

}
