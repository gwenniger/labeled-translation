package datapreparation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import junit.framework.Assert;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import util.ConfigFile;
import util.FileUtil;
import util.LinuxInteractor;

public class WordAlignerInterface {

    private static final String ARGUMENT_SEPARATOR = " ";
    private static final String ALIGNER_SCRIPT_FILE_NAME = "train-model.perl";
    private static final String ROOT_DIR_SPECIFICATION_FLAG = "-root-dir";
    private static final String CORPUS_SPECIFICATION_FLAG = "-corpus";
    private static final String EXTERNAL_BIN_DIR_SPECIFICATION_FLAG = "-external-bin-dir";
    private static final String SOURCE_ABBREVIATION_SPECIFICATION_FLAG = "-f";
    private static final String TARGET_ABBREVIATION_SPECIFICATION_FLAG = "-e";
    private static final String FIRST_STEP_SPECIFICATION_FLAG = "--first-step";
    private static final String LAST_STEP_SPECIFICATION_FLAG = "--last-step";
    private static final String MGIZA_SPECIFICATION_FLAG = "-mgiza";
    private static final String MGIZA_NUM_THREADS_SPECIFICATION_FLAG = "-mgiza-cpus";
    private static final String MODEL_DIR_NAME = "model";
    private static final String ALIGNMENT_FILE_NAME_PREFIX = "aligned.";
    private static final String GROW_DIAG_FINAL_FILE_SEPARATOR_PATTERN = "\\{##\\}";
    private static final String NL = "\n";
    private static final String ALIGNMENT_HEURISTICS_STRING_PROPERTY = "alignmentHeuristicsString";
    private static final String ALIGNMENT_MERGE_HEURISTIC_SPECIFICATION = "-alignment";
    // bin/moses-scripts/scripts-YYYYMMDD-HHMM/training/train-model.perl
    // -scripts-root-dir bin/moses-scripts/scripts-YYYYMMDD-HHMM -root-dir
    // working-dir -corpus working-dir/corpus/europarl.lowercased -f fr -e en
    // -alignment grow-diag-final-and -reordering msd-bidirectional-fe -lm
    // 0:5:working-dir/lm/europarl.lm:0

    private final String mosesScriptsDir;
    private final String mGizaBinDir;
    private final String rootOutputDir;
    private final String inputFilePathsPrefix;
    private final LanguageAbbreviationPair languageAbbreviationPair;
    private final int numMGizaThreads;
    private final String alignmentHeuristicString;

    private WordAlignerInterface(String mosesScriptsDir, String mGizaBinDir, String rootOutputDir,
	    String inputFilePathsPrefix, LanguageAbbreviationPair languageAbbreviationPair,
	    int numMGizaThreads, String alignmentHeuristicString) {
	this.mosesScriptsDir = mosesScriptsDir;
	this.mGizaBinDir = mGizaBinDir;
	this.rootOutputDir = rootOutputDir;
	this.inputFilePathsPrefix = inputFilePathsPrefix;
	this.languageAbbreviationPair = languageAbbreviationPair;
	this.numMGizaThreads = numMGizaThreads;
	this.alignmentHeuristicString = alignmentHeuristicString;
    }

    public static WordAlignerInterface createWordAlignerInterface(
	    AlignedCorpusPreparationConfig alignedCorpusPreparationConfig) {
	return new WordAlignerInterface(alignedCorpusPreparationConfig.getMosesScriptsDir(),
		alignedCorpusPreparationConfig.getMGizaBinDir(),
		alignedCorpusPreparationConfig.getWorkingDirPath(),
		alignedCorpusPreparationConfig.getWordAlignerInputFilesPrefix(),
		alignedCorpusPreparationConfig.getLanguageAbbreviationPair(),
		alignedCorpusPreparationConfig.getNumWordAlignerThreads(),
		alignedCorpusPreparationConfig.getAlignmentHeuristicString());
    }

    private String inputSpecificationString() {
	return CORPUS_SPECIFICATION_FLAG + ARGUMENT_SEPARATOR + inputFilePathsPrefix
		+ ARGUMENT_SEPARATOR + SOURCE_ABBREVIATION_SPECIFICATION_FLAG + ARGUMENT_SEPARATOR
		+ languageAbbreviationPair.getSourceAbbreviation() + ARGUMENT_SEPARATOR
		+ TARGET_ABBREVIATION_SPECIFICATION_FLAG + ARGUMENT_SEPARATOR
		+ languageAbbreviationPair.getTargetAbbreviation();
    }

    private String wordAlignmentStepsSpecificationString() {
	return FIRST_STEP_SPECIFICATION_FLAG + ARGUMENT_SEPARATOR + "1" + ARGUMENT_SEPARATOR
		+ LAST_STEP_SPECIFICATION_FLAG + ARGUMENT_SEPARATOR + "3";
    }

    private String alignmentCommandString() {
	String result = mosesScriptsDir + ALIGNER_SCRIPT_FILE_NAME;
	result += ARGUMENT_SEPARATOR + MGIZA_SPECIFICATION_FLAG + ARGUMENT_SEPARATOR
		+ ALIGNMENT_MERGE_HEURISTIC_SPECIFICATION + ARGUMENT_SEPARATOR
		+ alignmentHeuristicString + ARGUMENT_SEPARATOR
		+ MGIZA_NUM_THREADS_SPECIFICATION_FLAG + ARGUMENT_SEPARATOR + numMGizaThreads
		+ ARGUMENT_SEPARATOR + EXTERNAL_BIN_DIR_SPECIFICATION_FLAG + ARGUMENT_SEPARATOR
		+ mGizaBinDir + ARGUMENT_SEPARATOR + ROOT_DIR_SPECIFICATION_FLAG
		+ ARGUMENT_SEPARATOR + rootOutputDir + ARGUMENT_SEPARATOR
		+ inputSpecificationString() + ARGUMENT_SEPARATOR
		+ wordAlignmentStepsSpecificationString();
	return result;
    }

    public void performWordAlignment() {
	LinuxInteractor.executeExtendedCommandDisplayOutput(alignmentCommandString());
	writeFinalSeparateOutputFiles();
    }

    private String modelDirPath() {
	return rootOutputDir + MODEL_DIR_NAME + "/";
    }

    private String getAlignmentOutputFileName() {
	return ALIGNMENT_FILE_NAME_PREFIX + alignmentHeuristicString;
    }

    private String alignmentOutputFilePath() {
	return modelDirPath() + getAlignmentOutputFileName();
    }

    private String alignmentsFinalOutputFilePath() {
	return modelDirPath() + languageAbbreviationPair.getSourceAbbreviation() + "-"
		+ languageAbbreviationPair.getTargetAbbreviation() + "."
		+ getAlignmentOutputFileName();
    }

    private String sourceFinalOutputFilePath() {
	return modelDirPath() + "source" + "." + languageAbbreviationPair.getSourceAbbreviation();
    }

    private String targetFinalOutputFilePath() {
	return modelDirPath() + "target" + "." + languageAbbreviationPair.getTargetAbbreviation();
    }

    private void writeFinalSeparateOutputFiles() {
	BufferedWriter alignmentOutputWriter, sourceOutputWriter, targetOutputWriter;
	alignmentOutputWriter = sourceOutputWriter = targetOutputWriter = null;

	LineIterator it = null;
	try {
	    sourceOutputWriter = new BufferedWriter(new FileWriter(sourceFinalOutputFilePath()));
	    targetOutputWriter = new BufferedWriter(new FileWriter(targetFinalOutputFilePath()));
	    alignmentOutputWriter = new BufferedWriter(new FileWriter(
		    alignmentsFinalOutputFilePath()));
	    it = FileUtils.lineIterator(new File(alignmentOutputFilePath()), "UTF-8");
	    while (it.hasNext()) {
		String line = it.nextLine();
		String[] parts = line.split(GROW_DIAG_FINAL_FILE_SEPARATOR_PATTERN);
		Assert.assertEquals(3, parts.length);
		sourceOutputWriter.write((parts[0]).trim() + NL);
		targetOutputWriter.write((parts[1]).trim() + NL);
		alignmentOutputWriter.write((parts[2]).trim() + NL);
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	}

	finally {
	    it.close();
	    FileUtil.closeCloseableIfNotNull(sourceOutputWriter);
	    FileUtil.closeCloseableIfNotNull(targetOutputWriter);
	    FileUtil.closeCloseableIfNotNull(alignmentOutputWriter);
	}
    }

    public static String getAlignmentHeuristicString(ConfigFile theConfig) {
	if (!theConfig.hasValue(ALIGNMENT_HEURISTICS_STRING_PROPERTY)) {
	    throw new RuntimeException(
		    "Please specify "
			    + ALIGNMENT_HEURISTICS_STRING_PROPERTY
			    + " in the configuration file, e.g. \"grow-diag-final\", \"grow-diag-final-and\" or \"intersection\"");
	}
	return theConfig.getValueWithPresenceCheck(ALIGNMENT_HEURISTICS_STRING_PROPERTY);

    }
}
