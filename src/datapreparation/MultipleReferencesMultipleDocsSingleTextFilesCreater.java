package datapreparation;

import java.io.File;

import util.FileUtil;

/**
 * Many test/reference resources consist of multiple source documents and
 * multiple folders with corresponding reference documents for multiple
 * translators This is in particular the case for the Multiple-Translation
 * Chinese (MTC) copora form the Linguistic Data Consortium (LDC). However,
 * translation systems, including our pipeline, expect plain text
 * sentence-aligned test and dev files. This class takes care of extracting
 * these text files from the document XML files
 * 
 * @author Gideon Maillette de Buy Wenniger
 * 
 */
public class MultipleReferencesMultipleDocsSingleTextFilesCreater {
    private final String sourceDocsFolderPath;
    private final String referenceFoldersFolderPath;
    private final String outputFolderPath;
    private final String sourceFilesEncodingString;
    private final String refFilesEncodingString;

    private MultipleReferencesMultipleDocsSingleTextFilesCreater(String sourceDocsFolderPath,
	    String referenceFoldersFolderPath, String outputFolderPath,
	    String sourceFilesEncodingString, String refFilesEncodingString) {
	this.sourceDocsFolderPath = sourceDocsFolderPath;
	this.referenceFoldersFolderPath = referenceFoldersFolderPath;
	this.outputFolderPath = outputFolderPath;
	this.sourceFilesEncodingString = sourceFilesEncodingString;
	this.refFilesEncodingString = refFilesEncodingString;
    }

    public static MultipleReferencesMultipleDocsSingleTextFilesCreater createMultipleReferencesMultipleDocsSingleTextFilesCreater(
	    String sourceDocsFolderPath, String referenceFoldersFolderPath,
	    String outputFolderPath, String sourceFilesEncodingString, String refFilesEncodingString) {
	return new MultipleReferencesMultipleDocsSingleTextFilesCreater(sourceDocsFolderPath,
		referenceFoldersFolderPath, outputFolderPath, sourceFilesEncodingString,
		refFilesEncodingString);
    }

    private String sourceOutputFolderPath() {
	return outputFolderPath + "source.txt";
    }

    public String referenceOutputFolderPath(String referenceFolderName) {
	return outputFolderPath + referenceFolderName + ".ref.txt";
    }

    private void writeSourceOutputFile() {
	PlainTextFromXMLTextDocsExtracter.createPlainTextFromXMLTextDocsExtracter(
		sourceDocsFolderPath, sourceOutputFolderPath(), sourceFilesEncodingString)
		.writePlainTextOutputFile();
    }

    private void writeReferenceOutputFile(File referenceFolder) {
	PlainTextFromXMLTextDocsExtracter.createPlainTextFromXMLTextDocsExtracter(
		referenceFolder.getAbsolutePath() + "/",
		referenceOutputFolderPath(referenceFolder.getName()), refFilesEncodingString)
		.writePlainTextOutputFile();
    }

    public void writePlainTextOutputFiles() {
	writeSourceOutputFile();
	for (File referenceFolder : FileUtil.getAllFoldersInFolder(new File(
		referenceFoldersFolderPath))) {
	    writeReferenceOutputFile(referenceFolder);
	}
    }

    public static void main(String[] args) {
	if (args.length != 3) {
	    System.out
		    .println("Usage: createSingleTextFiles SOURCE_DOCS_FOLDER_PATH REFS_FOLDERS_PATH OUTPUT_FOLDER_PATH");
	    System.exit(1);
	}
	String sourceDocsFolderPath = args[0];
	String referenceFoldersFolderPath = args[1];
	String outputFolderPath = args[2];

	System.out.println("outputFolderPath: " + outputFolderPath);
	FileUtil.createFolderIfNotExisting(outputFolderPath);

	MultipleReferencesMultipleDocsSingleTextFilesCreater multipleDocsSingleTextFilesCreater = MultipleReferencesMultipleDocsSingleTextFilesCreater
		.createMultipleReferencesMultipleDocsSingleTextFilesCreater(sourceDocsFolderPath,
			referenceFoldersFolderPath, outputFolderPath,
			FileEncodingConverter.GB2312_ENCODING, FileEncodingConverter.UTF8_ENCODING);
	multipleDocsSingleTextFilesCreater.writePlainTextOutputFiles();

    }
}
