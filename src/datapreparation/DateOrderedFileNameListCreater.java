package datapreparation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import util.FileUtil;

/**
 * It is important to combine the corpus subfiles in the right order. This class
 * serves to help assuring this, by providing a method that returns a list of
 * the file names ordered by date
 * 
 * @author Gideon Maillette de Buy Wenniger
 * 
 */
public class DateOrderedFileNameListCreater {

    private final List<String> fileNameList;

    private DateOrderedFileNameListCreater(List<String> fileNameList) {
	this.fileNameList = fileNameList;
    }

    public static DateOrderedFileNameListCreater createDateOrderedFileNameListCreater(
	    String fileFolderPath) {
	List<String> fileNames = FileUtil.getFileNamesInDirectory(fileFolderPath);
	if (fileNames.isEmpty()) {
	    throw new RuntimeException(
		    "Error : DateOrderedFileNameListCreater.createDateOrderedFileNameListCreater - folder "
			    + fileFolderPath + " contains no files");
	}
	return new DateOrderedFileNameListCreater(fileNames);
    }

    private List<String> getFileNamesSortedAlphanumerically() {
	List<String> result = new ArrayList<String>(fileNameList);
	Collections.sort(result);
	return result;
    }

    /**
     * File names are of the form HKH19851030.sgm i.e. PREFIXyyyymmdd.SUFFIX
     * (with yyyymmdd = year, month, day). In other words, simply sorting
     * alphanumerically will sort also by date.
     * 
     * @return
     */
    public List<String> getFileNamesSortedByDate() {
	return getFileNamesSortedAlphanumerically();
    }

}
