package datapreparation;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import util.Pair;

public class DevTestDuplicatesChecker {

    DuplicatesChecker devDuplicatesChecker;
    DuplicatesChecker testDuplicatesChecker;

    private DevTestDuplicatesChecker(DuplicatesChecker devDuplicatesChecker,
	    DuplicatesChecker testDuplicatesChecker) {
	this.devDuplicatesChecker = devDuplicatesChecker;
	this.testDuplicatesChecker = testDuplicatesChecker;
    }

    public static DevTestDuplicatesChecker createDuplicatesChecker(String devSourceFilePath,
	    String testSourceFilePath) {
	return new DevTestDuplicatesChecker(
		DuplicatesChecker.createDuplicatesChecker(devSourceFilePath),
		DuplicatesChecker.createDuplicatesChecker(testSourceFilePath));
    }

    public Map<String, Pair<Integer>> computeDevTestDuplicatesMap() {
	Map<String, Pair<Integer>> result = new HashMap<String, Pair<Integer>>();
	Map<String, Integer> devFrequencyMap = devDuplicatesChecker.computeStringFrequencyMap();
	Map<String, Integer> testFrequencyMap = testDuplicatesChecker.computeStringFrequencyMap();
	for (Entry<String, Integer> entry : testFrequencyMap.entrySet()) {
	    if (devFrequencyMap.containsKey(entry.getKey())) {
		Integer devFrequency = devFrequencyMap.get(entry.getKey());
		Integer testFrequency = entry.getValue();
		result.put(entry.getKey(), new Pair<Integer>(devFrequency, testFrequency));
	    }
	}
	return result;
    }

    public void showDevTestSetCrossDuplicates() {
	Map<String, Pair<Integer>> devTestSetDuplicatesMap = computeDevTestDuplicatesMap();

	if (devTestSetDuplicatesMap.isEmpty()) {
	    System.out.println("No duplicates found");
	} else {
	    int numDuplicates = devTestSetDuplicatesMap.size();
	    System.out.println(numDuplicates
		    + " strings with duplicates across dev and test were found:");
	    for (Entry<String, Pair<Integer>> entry : devTestSetDuplicatesMap.entrySet()) {
		System.out.println("\"" + entry.getKey() + "\"");
		System.out.println("number of occurences in dev set: "
			+ entry.getValue().getFirst());
		System.out.println("number of occurences in test set: "
			+ entry.getValue().getSecond());
		System.out.println("\n");
	    }
	}
    }

    public void showDuplicates() {
	System.out.println("Duplicates inside dev set:");
	devDuplicatesChecker.showDuplicates();
	System.out.println("Duplicates inside test set:");
	testDuplicatesChecker.showDuplicates();
	System.out.println("Duplicates across dev and test set:");
	showDevTestSetCrossDuplicates();
    }

    public static void main(String[] args) {
	if (args.length != 2) {
	    System.out
		    .println("usage : devTestDuplicatesChecker DEV_SOURCE_FILE_PATH TEST_SOURCE_FILE_PATH");
	    System.exit(1);
	}
	DevTestDuplicatesChecker devTestDuplicatesChecker = DevTestDuplicatesChecker
		.createDuplicatesChecker(args[0], args[1]);
	devTestDuplicatesChecker.showDuplicates();
    }

}
