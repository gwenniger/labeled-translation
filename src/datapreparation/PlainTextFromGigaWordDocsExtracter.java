package datapreparation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import mt_external_scripts.Lowercase;
import util.FileUtil;

public class PlainTextFromGigaWordDocsExtracter {

    private final static String TOKENIZED_SUFFIX = ".tok";
    private final static String LOWERCASED_SUFFIX = ".lowercased";

    private final String docsFolderPath;
    private final BufferedWriter outputWriter;
    private final boolean includeHeadLines;
    private final boolean includeDateLines;

    private PlainTextFromGigaWordDocsExtracter(String docsFolderPath, BufferedWriter outputWriter,
	    boolean includeHeadLines, boolean includeDateLines) {
	this.docsFolderPath = docsFolderPath;
	this.outputWriter = outputWriter;
	this.includeHeadLines = includeHeadLines;
	this.includeDateLines = includeDateLines;
    }

    public static PlainTextFromGigaWordDocsExtracter createPlainTextFromGigaWordDocsExtracter(
	    String docsFolderPath, String outputFilePath, boolean includeHeadLines,
	    boolean includeDateLines) {
	try {
	    BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFilePath));
	    return new PlainTextFromGigaWordDocsExtracter(docsFolderPath, outputWriter,
		    includeHeadLines, includeDateLines);
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    public void writeOutput() {
	for (File inputFile : FileUtil.getAllFilesInFolder(new File(docsFolderPath), false)) {
	    if (inputFile.isFile()) {
		PlainTextFromGigaWordDocExtracter plainTextFromGigaWordDocExtracter = PlainTextFromGigaWordDocExtracter
			.createPlainTextFromGigaWordDocExtracter(inputFile.getAbsolutePath(),
				outputWriter, includeHeadLines, includeDateLines);
		plainTextFromGigaWordDocExtracter.writeContentLinesToOutputWriter();
	    }
	}
	try {
	    outputWriter.close();
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private static String getTokenizedOutputFilePath(String inputFilePath,
	    String languageAbbreviation) {
	return inputFilePath + TOKENIZED_SUFFIX + "." + languageAbbreviation;
    }

    private static String getLowercasedOutputFilePath(String inputFilePath,
	    String languageAbbreviation) {
	return inputFilePath + LOWERCASED_SUFFIX + "." + languageAbbreviation;
    }

    private static void createTokenizedOutput(String scriptsDir, String inputFilePath,
	    String languageAbbreviation) {
	TokenizerInterface tokenizerInterface = TokenizerInterface
		.createTokenizerInterface(scriptsDir);
	tokenizerInterface.createTokenizedFile(inputFilePath,
		getTokenizedOutputFilePath(inputFilePath, languageAbbreviation),
		languageAbbreviation);
    }

    private static void createLowercasedOutput(String scriptsDir, String inputFilePath,
	    String languageAbbreviation) {
	try {
	    Lowercase lowercase = Lowercase.createLowercase(
		    getTokenizedOutputFilePath(inputFilePath, languageAbbreviation),
		    getLowercasedOutputFilePath(inputFilePath, languageAbbreviation));
	    lowercase.performLowercaseConversion();
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}

    }

    public static void main(String[] args) {
	if (args.length != 3) {
	    for (String arg : args) {
		System.out.println("arg: " + arg);
	    }
	    System.out
		    .println("Usage: convertGigaWordFileToPlainText DOCS_FOLDER_PAHT OUTPUT_FILE_PATH SCRIPTS_DIR");
	    System.exit(1);
	}
	String inputFolderPath = args[0];
	String outputFilePath = args[1];
	String scriptsDir = args[2];
	boolean includeHeadLines = true;
	boolean includeDateLines = true;

	String languageAbbreviation = "en";
	PlainTextFromGigaWordDocsExtracter plainTextFromGigaWordDocsExtracter = createPlainTextFromGigaWordDocsExtracter(
		inputFolderPath, outputFilePath, includeHeadLines, includeDateLines);
	plainTextFromGigaWordDocsExtracter.writeOutput();
	createTokenizedOutput(scriptsDir, outputFilePath, languageAbbreviation);
	createLowercasedOutput(scriptsDir, outputFilePath, languageAbbreviation);
    }

}
