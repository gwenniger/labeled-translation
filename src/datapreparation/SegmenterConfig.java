package datapreparation;

import java.io.FileNotFoundException;

import mt_pipeline.mt_config.MTConfigFile;
import util.ConfigFile;

public class SegmenterConfig {

    public static final String STANFORD_SEGMENTER_ROOT_DIR_LOCATION = "stanfordSegmenterRootDirLocation";
    private static final String SEGMENTER_RESULT_FILE_SUFFIX = ".segmented";
    public static final String SEGMENTER_INPUT_FILE_PATH_PROPERTY = "segmenterInputFilePath";
    public static final String NUM_SEGMENTER_THREADS_PROPERTY = "numSegmenterThreads";

    // There are two choices for the Segmentation model:
    // "ctb" model was trained with Chinese treebank (CTB) segmentation,
    // and the "pku" model was trained with Beijing University's (PKU)
    // segmentation.
    // "Unsupervised Tokenization for Machine Translation" (Chun and
    // Gildea,2009)
    // http://www.cs.rochester.edu/~gildea/pubs/chung-gildea-emnlp09.pdf
    // suggests that ctb performs better for translation, so we mostly go with
    // that
    public static final String CHINESE_TREEBANK_SEGMENTATION_MODEL_STRING = "ctb";
    public static final String PEKING_UNIVERSITY_SEGMENTATION_MODEL_STRING = "pku";
    public static final String SEGMENTATION_MODEL_SPECIFICATION_STRING_POPERTY = "segmentationModelSpecificationString";

    private final ConfigFile theConfig;

    protected SegmenterConfig(ConfigFile theConfig) {
	this.theConfig = theConfig;
    }

    public static SegmenterConfig createMtSegmenterConfig(String configFilePath) {
	try {
	    return new SegmenterConfig(new ConfigFile(configFilePath));
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    public static SegmenterConfig createMTMtSegmenterConfig(MTConfigFile theConfig) {
	return new SegmenterConfig(theConfig);
    }

    private static void checkValidSegmentationModelChosen(String segmenationModelSpecificationString) {

	boolean specifiedChineseTreebankModel = segmenationModelSpecificationString
		.equals(CHINESE_TREEBANK_SEGMENTATION_MODEL_STRING);
	boolean specifiedPekingUniversityModel = segmenationModelSpecificationString
		.equals(PEKING_UNIVERSITY_SEGMENTATION_MODEL_STRING);

	if (!(specifiedChineseTreebankModel || specifiedPekingUniversityModel)) {
	    String errorMessage = "Error: Please choose a value of \"ctb\" for \"chinese tree bank\" or \"pku\" for \"peking university segmentation\" for the segmentation model property specified with "
		    + SEGMENTATION_MODEL_SPECIFICATION_STRING_POPERTY + " in the config file";
	    errorMessage += "\n (value chosen: " + segmenationModelSpecificationString + ")";
	    throw new RuntimeException(errorMessage);
	}
    }

    public static String getSegmentationModelSpecificationString(ConfigFile theConfig) {
	String result = theConfig
		.getValueWithPresenceCheck(SEGMENTATION_MODEL_SPECIFICATION_STRING_POPERTY);
	checkValidSegmentationModelChosen(result);
	return result;
    }

    public String getSegmentationModelSpecificationString() {
	return getSegmentationModelSpecificationString(theConfig);
    }

    public String getStandfordSegmenterRootLocation() {
	return this.theConfig.getValueWithPresenceCheck(STANFORD_SEGMENTER_ROOT_DIR_LOCATION);
    }

    public String getSegmenterInputFilePath() {
	return this.theConfig.getValueWithPresenceCheck(SEGMENTER_INPUT_FILE_PATH_PROPERTY);
    }

    public int getNumSegmenterThreads() {
	String valueString = this.theConfig
		.getValueWithPresenceCheck(NUM_SEGMENTER_THREADS_PROPERTY);
	return Integer.parseInt(valueString);
    }

    public static String addSuffixBeforeLastSuffixIfAny(String stringToAddTo, String suffixToAdd) {
	String result = "";
	if (stringToAddTo.contains(".")) {
	    String[] subParts = stringToAddTo.split("\\.");
	    result = subParts[0] + SEGMENTER_RESULT_FILE_SUFFIX + "." + subParts[1];
	} else {
	    result = stringToAddTo + SEGMENTER_RESULT_FILE_SUFFIX;
	}
	return result;
    }

    public static String getSegmenterOutputFileName(String inputFileName) {
	System.out.println("inputFileName : " + inputFileName);
	// If there is a file extension we add the suffix just before that
	String result = addSuffixBeforeLastSuffixIfAny(inputFileName, SEGMENTER_RESULT_FILE_SUFFIX);

	System.out.println("getSegmenterPartOutputFileName - result " + result);
	return result;
    }

    public String getSegmenterOutputFilePath() {
	// If there is a file extension we add the suffix just before that
	String result = addSuffixBeforeLastSuffixIfAny(getSegmenterInputFilePath(),
		SEGMENTER_RESULT_FILE_SUFFIX);

	System.out.println("getSegmenterPartOutputFileName - result " + result);
	return result;
    }

    public static String getSegmenterPartOutputFileName(String inputFileName) {
	System.out.println("inputFileName : " + inputFileName);
	String[] parts = inputFileName.split("Part");

	String firstPartResult = parts[0];
	firstPartResult = addSuffixBeforeLastSuffixIfAny(firstPartResult,
		SEGMENTER_RESULT_FILE_SUFFIX);

	return firstPartResult + "Part" + parts[1];
    }

}
