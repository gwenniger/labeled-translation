package datapreparation;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;

public class SegmentListExtracter {

    private static final String ID_STRING = "id=";
    private static final String DOC_ID_PREFIX = "<DOC docid=";
    private static final String SEGMENT_END_TAG = "</seg>";
    private final BufferedReader segnmentFileReader;
    private final String segmentFilePath;

    private SegmentListExtracter(BufferedReader segnmentFileReader, String segmentFilePath) {
	this.segnmentFileReader = segnmentFileReader;
	this.segmentFilePath = segmentFilePath;
    }

    public static SegmentListExtracter createSegmentListExtracter(String segmentFilePath) {
	BufferedReader segmentFileReader;
	try {
	    segmentFileReader = new BufferedReader(new FileReader(segmentFilePath));
	    return new SegmentListExtracter(segmentFileReader, segmentFilePath);
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private boolean isSegmentLine(String line) {
	return line.startsWith("<seg id") && line.endsWith("</seg>");
    }

    private boolean isNewDocLine(String line) {
	return line.startsWith(DOC_ID_PREFIX);
    }

    public static String getFirstStringBeforeWhitespace(String string) {
	return string.split(" ")[0];
    }

    private String getDocID(String line) {
	int docIDBeginIndex = line.indexOf(DOC_ID_PREFIX) + DOC_ID_PREFIX.length();
	String result = getFirstStringBeforeWhitespace(line.substring(docIDBeginIndex));
	return result;
    }

    private boolean isEndDocLine(String line) {
	return line.endsWith("</DOC>");
    }

    public List<DocSegments> extractDocSegmentList() {
	try {
	    List<DocSegments> result = new ArrayList<DocSegments>();

	    List<String> resultList = new ArrayList<String>();
	    String line = null;
	    String docID = null;
	    int nextExpectedSegNum = 1;
	    while ((line = segnmentFileReader.readLine()) != null) {
		//System.out.println("line: " + line);
		if (isSegmentLine(line)) {
		    //System.out.println("segment line found: " + line);
		    int segmentPrefixEndIndex = line.indexOf(">");
		    int segmentSuffixBeginIndex = line.indexOf(SEGMENT_END_TAG);
		    String segmentPrefix = line.substring(0, segmentPrefixEndIndex + 1);
		    String segmentNumString = segmentPrefix.substring(
			    segmentPrefix.indexOf(ID_STRING) + ID_STRING.length(),
			    segmentPrefix.length() - 1);
		    //System.out.println("segmentNumString: " + segmentNumString);
		    int segNum = Integer.parseInt(segmentNumString);
		    if (!(nextExpectedSegNum == segNum)) {
			String errorMessage = "Error - expected segNum = " + nextExpectedSegNum
				+ " but got " + segNum + " in file " + this.segmentFilePath;
			errorMessage += "\n last processed line: " + line;
			throw new RuntimeException(errorMessage);
		    }
		    // System.out.println("segmentPrefixEndIndex: " +
		    // segmentPrefixEndIndex);
		    // System.out.println("segmentSuffixBeginIndex: " +
		    // segmentSuffixBeginIndex);
		    String segment = line.substring(segmentPrefixEndIndex + 1,
			    segmentSuffixBeginIndex);
		    nextExpectedSegNum++;
		    // We trim the segments before adding them, removing the
		    // leading and trailing whitespace
		    resultList.add(segment.trim());
		} else if (isNewDocLine(line)) {
		    docID = getDocID(line);
		}
		// end of document reached
		else if (isEndDocLine(line)) {
		    //System.out.println("Found end doc line");
		    DocSegments docSegments = new DocSegments(resultList, docID);
		    Assert.assertEquals(nextExpectedSegNum - 1, docSegments.size());
		    result.add(docSegments);
		    // Reset docID and resultList for the next document
		    docID = null;
		    resultList = new ArrayList<String>();
		    nextExpectedSegNum = 1;
		}
	    }
	    return result;
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    public static class DocSegments {
	private final List<String> segmentList;
	private final String docID;

	private DocSegments(List<String> segmentList, String docID) {
	    this.segmentList = segmentList;
	    this.docID = docID;
	}

	/**
	 * This method gets the segment with index i . This is stored at index
	 * i-1 in the file because segments always start at index 1.
	 * 
	 * @param index
	 * @return
	 */
	public String getSegment(int index) {
	    return segmentList.get(index - 1);
	}

	public int size() {
	    return segmentList.size();
	}

	public void printSegments() {
	    for (int i = 1; i <= segmentList.size(); i++) {
		System.out.println("Segment " + i + ": " + getSegment(i));
	    }
	}

	public String getDocID() {
	    return this.docID;
	}

	public String toString() {
	    String result = "<DocSegments>";
	    for (int i = 1; i <= this.segmentList.size(); i++) {
		result += "\n" + "segment " + i + " " + getSegment(i);
	    }
	    result += "\n" + "</DocSegments>";
	    return result;
	}
    }

}
