package datapreparation;

import util.LinuxInteractor;
import util.FileUtil;

public class CorpusCleanerInterface {
    private static final String CLEAN_CORPUS_COMMAND_NAME = "clean-corpus-n.perl";

    private static final String ARGUMENT_SEPARATOR = " ";

    private final CleanerAndLowercaserConfig corpusCleanerConfig;

    private CorpusCleanerInterface(CleanerAndLowercaserConfig corpusCleanerConfig) {
	this.corpusCleanerConfig = corpusCleanerConfig;
    }

    public static CorpusCleanerInterface createCleanerInterfaceDefaultSettings(String scriptsDir,
	    String sourceAbbreviation, String targetAbbreviation, String sourceFilePath,
	    String targetFilePath, String corpusNameString, String workingDir) {
	return new CorpusCleanerInterface(
		CleanerAndLowercaserConfig.createCorpusCleanerAndLowercaserConfigDefaultSettings(scriptsDir,
			sourceAbbreviation, targetAbbreviation, sourceFilePath, targetFilePath,
			corpusNameString, workingDir));
    }

    private void createTempInputFiles() {
	FileUtil.copyFileNative(corpusCleanerConfig.getSourceFilePath(),
		corpusCleanerConfig.getTempSourceFilePath(), true);
	FileUtil.copyFileNative(corpusCleanerConfig.getTargetFilePath(),
		corpusCleanerConfig.getTempTargetFilePath(), true);
    }

    private final String sourceAndTargetLanguageAbbreviations() {
	return corpusCleanerConfig.getSourceAbbreviation() + ARGUMENT_SEPARATOR
		+ corpusCleanerConfig.getTargetAbbreviation();
    }

    private final String lengthArguments() {
	return corpusCleanerConfig.getLengthSetttings().getMinLength() + " "
		+ corpusCleanerConfig.getLengthSetttings().getMaxLength();
    }

    private final String cleanCorpusCommand() {
	// bin/moses-scripts/scripts-YYYYMMDD-HHMM/training/clean-corpus-n.perl
	// working-dir/corpus/europarl.tok fr en
	// working-dir/corpus/europarl.clean 1 40
	return corpusCleanerConfig.getScriptsDir() + CLEAN_CORPUS_COMMAND_NAME + ARGUMENT_SEPARATOR
		+ corpusCleanerConfig.getTempTokenizedFilesPathPrefix() + ARGUMENT_SEPARATOR
		+ sourceAndTargetLanguageAbbreviations() + ARGUMENT_SEPARATOR
		+ corpusCleanerConfig.getCleanedFilesPathPrefix() + ARGUMENT_SEPARATOR + lengthArguments();
    }

    public void createCleanedCorpusFiles() {
	createTempInputFiles();
	String command = cleanCorpusCommand();
	System.out.println("Excecuting command: \n" + command);
	LinuxInteractor.executeExtendedCommandDisplayOutput(command);
    }

}
