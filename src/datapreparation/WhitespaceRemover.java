package datapreparation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import util.FileUtil;
import mt_pipeline.parserInterface.InputFilePreprocessor;

/**
 * This class performs the pre-processing step of removing whitespace from
 * sentences. This is done as a pre-processing step on the Chinese sentences
 * before feeding them to the (Standford) segmenter
 * 
 * @author Gideon Maillette de Buy Wenniger
 * 
 */
public class WhitespaceRemover extends InputFilePreprocessor {

    private static String NO_WITESPACE_FILE_SUFFIX = ".NO_WHITESPACE_TEMP_FILE";
    
    @Override
    public String createPreprocessedLine(String line) {
	return removeSpacesThorough(line);
    }
        
    private static boolean isNewlineChacacter(char c) {
	return (c == ("\n".charAt(0)));
    }

    private static boolean isNonNewlineSpaceCharacter(char c) {
	return (Character.isSpaceChar(c) && (!isNewlineChacacter(c)));
    }

    public static String removeSpacesThorough(String line) {
	String result = "";
	for (int i = 0; i < line.length(); i++) {
	    char c = line.charAt(i);
	    if (!isNonNewlineSpaceCharacter(c)) {
		result += c;
	    }
	}
	return result;
    }

    public static String removeAllNonCharactersOrNumerals(String line) {
	String result = "";
	for (int i = 0; i < line.length(); i++) {
	    char c = line.charAt(i);
	    if (Character.isLetter(c) || Character.isDigit(c)) {
		result += c;
	    }
	}
	return result;
    }

    /*
     * private final BufferedReader inputReader; private final BufferedWriter
     * outputWriter;
     * 
     * private WhitespaceRemover(BufferedReader inputReader, BufferedWriter
     * outputWriter) { this.inputReader = inputReader; this.outputWriter =
     * outputWriter; }
     * 
     * public static WhitespaceRemover createWhitespaceRemover(String
     * inputFilePath, String outputFilePath) {
     * 
     * try { BufferedReader inputReader = new BufferedReader(new
     * FileReader(inputFilePath)); BufferedWriter outputWriter = new
     * BufferedWriter(new FileWriter(outputFilePath)); return new
     * WhitespaceRemover(inputReader, outputWriter); } catch
     * (FileNotFoundException e) { e.printStackTrace(); throw new
     * RuntimeException(e); } catch (IOException e) { e.printStackTrace(); throw
     * new RuntimeException(e); }
     * 
     * }
     * 
     * public void writeOutputWithoutWhiteSpace() { try {
     * System.out.println("Writing file without whitespace within lines");
     * String line; boolean writeNewLine = false; while ((line =
     * inputReader.readLine()) != null) {
     * 
     * if (writeNewLine) { line = "\n" + line; }
     * 
     * outputWriter.write(Utility.removeWhitespace(line)); writeNewLine = true;
     * } } catch (IOException e) { e.printStackTrace(); throw new
     * RuntimeException(e); } finally {
     * FileUtil.closeCloseableIfNotNull(outputWriter); } }
     */

    public static void removeWhiteSpaceInPlace(String fileName) {
	try 
        {
	    String tempFileName = fileName + NO_WITESPACE_FILE_SUFFIX;
	    BufferedWriter tempFileWriter = new BufferedWriter(new FileWriter(tempFileName));
	    LineIterator lineIterator = FileUtils.lineIterator(new File(fileName));
	    while (lineIterator.hasNext()) {
		String line = lineIterator.nextLine();
		String lineWithoutSpaces = WhitespaceRemover.removeSpacesThorough(line);
		tempFileWriter.write(lineWithoutSpaces + "\n");
	    }
	    tempFileWriter.close();
	    FileUtil.copyFileNative(tempFileName, fileName, true);
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    public static void main(String[] args) {
	String chineseString = "主席先生，正如　閣下在上週發表施政報告時所說一樣，政府已展開計劃，在五至十年內清拆所有市區寮屋和重新安置所有市區寮屋居民";
	WhitespaceRemover whitespaceRemover = new WhitespaceRemover();
	String chineseStringNoSpaces = whitespaceRemover.createPreprocessedLine(chineseString);
	String chineseStringNoSpaces2 = removeSpacesThorough(chineseString);
	System.out.println("Chinese String original:  \n" + "\"" + chineseString + "\"");
	System.out.println("Chinese String no spaces:  \n" + "\"" + chineseStringNoSpaces + "\"");
	System.out.println("Chinese String no spaces2:  \n" + "\"" + chineseStringNoSpaces2 + "\"");

    }

}
