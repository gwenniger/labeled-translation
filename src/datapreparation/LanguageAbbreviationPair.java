package datapreparation;

public class LanguageAbbreviationPair {
    private final String sourceAbbreviation;
    private final String targetAbbreviation;

    private LanguageAbbreviationPair(String sourceAbbreviation, String targetAbbreviation) {
	this.sourceAbbreviation = sourceAbbreviation;
	this.targetAbbreviation = targetAbbreviation;
    }

    public static LanguageAbbreviationPair createLanguageAbbreviationPair(
	    String sourceAbbreviation, String targetAbbreviation) {
	return new LanguageAbbreviationPair(sourceAbbreviation, targetAbbreviation);
    }

    public String getSourceAbbreviation() {
	return sourceAbbreviation;
    }

    public String getTargetAbbreviation() {
	return targetAbbreviation;
    }

}
