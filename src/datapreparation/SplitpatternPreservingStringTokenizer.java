package datapreparation;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SplitpatternPreservingStringTokenizer {

    private final Pattern regularExpressionPattern;

    private SplitpatternPreservingStringTokenizer(Pattern regularExpressionPattern) {
	this.regularExpressionPattern = regularExpressionPattern;
    }

    public static SplitpatternPreservingStringTokenizer createSplitPatternPreservingStringTokenizer(
	    String regularExpressionString) {
	return new SplitpatternPreservingStringTokenizer(Pattern.compile(regularExpressionString));
    }

    public List<SmartToken> tokenize(String inputLine) {

	List<SmartToken> result = new ArrayList<>();
	Matcher matcher = regularExpressionPattern.matcher(inputLine);

	String noMatchString, matchString;
	int currentIndex = 0;
	while (matcher.find()) {
	    System.out.print("Start index: " + matcher.start());
	    System.out.print(" End index: " + matcher.end() + " ");
	    System.out.println("matched string: \"" + matcher.group() + "\"");
	    noMatchString = inputLine.substring(currentIndex, matcher.start());
	    matchString = matcher.group();

	    if (noMatchString.length() > 0) {
		result.add(new SmartToken(noMatchString, false));
	    }
	    result.add(new SmartToken(matchString, true));
	    currentIndex = matcher.end();
	}
	noMatchString = inputLine.substring(currentIndex, inputLine.length());

	if (noMatchString.length() > 0) {
	    result.add(new SmartToken(noMatchString, false));
	}

	return result;
    }

    public List<String> tokenizeReturnSimpleStringList(String inputLine) {
	List<String> result = new ArrayList<>();
	for (SmartToken token : tokenize(inputLine)) {
	    result.add(token.getContents());
	}
	return result;
    }

    public static class SmartToken {
	private final String contents;
	private final boolean isSplitPatternMatchToken;

	public SmartToken(String contents, boolean isSplitPatternMatchToken) {
	    this.contents = contents;
	    this.isSplitPatternMatchToken = isSplitPatternMatchToken;
	}

	public String getContents() {
	    return this.contents;
	}

	public boolean isSplitPatternToken() {
	    return isSplitPatternMatchToken;
	}

    }

    public static void main(String[] args) {
	SplitpatternPreservingStringTokenizer regularExpressionMatchRangeListComputer = SplitpatternPreservingStringTokenizer
		.createSplitPatternPreservingStringTokenizer(WhitespaceReformatter.ALL_NUMBERS_MATCHING_REGULAR_EXPRESSION_STRING);
	for (SmartToken token : regularExpressionMatchRangeListComputer.tokenize("的25.5的")) {
	    System.out.println("token: " + token.getContents() + "  is split pattern token: "
		    + token.isSplitPatternToken());
	}
    }
}
