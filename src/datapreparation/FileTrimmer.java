package datapreparation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

public class FileTrimmer {

    public static void trimFile(String inputFilePath, String outputFilePath) {
	try {
	    LineIterator lineIterator = FileUtils.lineIterator(new File(inputFilePath), "UTF-8");
	    BufferedWriter fw = new BufferedWriter(new FileWriter(outputFilePath));
	    String line;

	    while (lineIterator.hasNext()) {
		line = lineIterator.nextLine().trim();
		if (lineIterator.hasNext()) {
		    line += "\n";
		}
		fw.write(line);
	    }
	    fw.close();
	    lineIterator.close();
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    public static void main(String[] args) {
	if (args.length != 2) {
	    System.out.println("trimFile INPUT_FILE OUTPUT_FILE");
	    System.exit(1);
	}
	trimFile(args[0], args[1]);
    }

}
