package datapreparation;

import util.FileUtil;
import util.LinuxInteractor;
import mt_pipeline.parserInterface.MultiThreadedComputationProcess;

public class StanfordSegmenterProcess extends
	MultiThreadedComputationProcess<StanfordSegmenterProcess> {
    public static final String SEGMENTER_SCRIPT_NAME = "segment.sh";

    private static final String SIZE_SPECIFICATION_STRING = "0";

    private final String encoding;
    private final String segmentationModelSpecificationString;
    private final String standfordSegmenterRootDir;

    protected StanfordSegmenterProcess(String partFileName, String encoding,
	    String standfordSegmenterRootDir, String segmentationModelSpecificationString) {
	super(partFileName);
	this.encoding = encoding;
	this.segmentationModelSpecificationString = segmentationModelSpecificationString;
	this.standfordSegmenterRootDir = standfordSegmenterRootDir;
    }

    @Override
    public StanfordSegmenterProcess call() throws Exception {
	String segmentationCommand = getSegmentationCommand(partFileName);
	System.out.println("segmentation command: " + segmentationCommand);
	LinuxInteractor.executeExtendedCommandDisplayOutput(segmentationCommand);
	// System.out.println("result: " + result);
	return this;
    }

    private final String getSegmentBaseCommand() {
	String result = standfordSegmenterRootDir + SEGMENTER_SCRIPT_NAME;
	if (!FileUtil.fileExists(result)) {
	    throw new RuntimeException(
		    "Error StanfordSegmenterProcess:  stanford segmenter program " + result
			    + " does not exist." + "\nPlease check that you specified " + "\""
			    + SegmenterConfig.STANFORD_SEGMENTER_ROOT_DIR_LOCATION + "\""
			    + " correcly in the configuration file");
	}
	return result;
    }

    public String getSegmentationCommand(String partFileName) {
	// segment.sh [-k] [ctb|pku] <filename> <encoding> <size>
	String result = getSegmentBaseCommand() + " " + segmentationModelSpecificationString + " "
		+ partFileName + " " + encoding + " " + SIZE_SPECIFICATION_STRING;
	result += " > " + SegmenterConfig.getSegmenterPartOutputFileName(partFileName);
	return result;
    }
}
