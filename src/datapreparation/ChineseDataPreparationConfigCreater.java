package datapreparation;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChineseDataPreparationConfigCreater {

    private static String NL = "\n";

    private static final String SOURCE_SEGMENT_FOLDER_SUB_PATH = "/English/";
    private static final String TARGET_SEGMENT_FOLDER_SUB_PATH = "/Chinese/";
    private static final String ALIGNMENT_FOLDER_SUB_PATH = "/alignment/";

    private static final String HK_HANSARDS_NAME = "HK_Hansards";
    private static final String HK_LAWS_NAME = "HK_Laws";
    private static final String HK_NEWS_NAME = "HK_News";

    private final String dataDirectory;
    private final List<String> subcorperaNamesList;

    private ChineseDataPreparationConfigCreater(String dataDirectory,
	    List<String> subcorperaNamesList) {
	this.dataDirectory = dataDirectory;
	this.subcorperaNamesList = subcorperaNamesList;
    }

    private static List<String> getFullDataSubCorperaList() {
	return Arrays.asList(HK_HANSARDS_NAME, HK_LAWS_NAME, HK_NEWS_NAME);
    }

    private static List<String> getTestDataSubCorperaList() {
	return Arrays.asList(HK_HANSARDS_NAME, HK_LAWS_NAME);
    }

    public static ChineseDataPreparationConfigCreater createFullChineseDataConfigCreater(
	    String dataDirectory) {
	return new ChineseDataPreparationConfigCreater(dataDirectory, getFullDataSubCorperaList());
    }

    public static ChineseDataPreparationConfigCreater createTestChineseDataConfigCreater(
	    String dataDirectory) {
	return new ChineseDataPreparationConfigCreater(dataDirectory, getTestDataSubCorperaList());
    }

    private static final String sourceSegmentFolderPath(String dataDirectory, String subcorpusName) {
	return dataDirectory + "/" + subcorpusName + SOURCE_SEGMENT_FOLDER_SUB_PATH;
    }

    private static final String targetSegmentFolderPath(String dataDirectory, String subcorpusName) {
	return dataDirectory + "/" + subcorpusName + TARGET_SEGMENT_FOLDER_SUB_PATH;
    }

    private static final String alignmentFolderPath(String dataDirectory, String subcorpusName) {
	return dataDirectory + "/" + subcorpusName + ALIGNMENT_FOLDER_SUB_PATH;
    }

    private String getSourceSegmentFolderPaths() {
	List<String> paths = new ArrayList<String>();
	for (String subcorpusName : subcorperaNamesList) {
	    paths.add(sourceSegmentFolderPath(dataDirectory, subcorpusName));
	}
	return util.Utility.stringListStringWithoutBracketsCommaSeparated(paths);
    }

    private String getTargetSegmentFolderPaths() {
	List<String> paths = new ArrayList<String>();
	for (String subcorpusName : subcorperaNamesList) {
	    paths.add(targetSegmentFolderPath(dataDirectory, subcorpusName));
	}
	return util.Utility.stringListStringWithoutBracketsCommaSeparated(paths);
    }

    private String getAlignmentFolderPaths() {
	List<String> paths = new ArrayList<String>();
	for (String subcorpusName : subcorperaNamesList) {
	    paths.add(alignmentFolderPath(dataDirectory, subcorpusName));
	}
	return util.Utility.stringListStringWithoutBracketsCommaSeparated(paths);
    }

    private static String propertyValueStringWithNewline(String propertyName, String valueString) {
	return propertyName + " = " + valueString + NL;
    }

    private void writeSegmenterConfigValues(BufferedWriter fileWriter,
	    String fullCorpusTargetOutputFilePath, String stanfordSegmenterRootDirLocation,
	    String segmentationModelSpecificationString,int numSegmenterThreads) throws IOException {
	fileWriter.write("% Segmenter configuration" + NL);
	fileWriter.write(propertyValueStringWithNewline(
		SegmenterConfig.STANFORD_SEGMENTER_ROOT_DIR_LOCATION,
		stanfordSegmenterRootDirLocation));
	fileWriter.write(propertyValueStringWithNewline(
		SegmenterConfig.SEGMENTATION_MODEL_SPECIFICATION_STRING_POPERTY,
		segmentationModelSpecificationString));

	fileWriter
		.write(propertyValueStringWithNewline(
			SegmenterConfig.SEGMENTER_INPUT_FILE_PATH_PROPERTY,
			fullCorpusTargetOutputFilePath));
	fileWriter.write(propertyValueStringWithNewline(
		SegmenterConfig.NUM_SEGMENTER_THREADS_PROPERTY, "" + numSegmenterThreads));
    }

    public void writeFullCorpusExtractionConfig(String outputConfigFilePath,
	    String fullCorpusSourceOutputFilePath, String fullCorpusTargetOutputFilePath,
	    String stanfordSegmenterRootDirLocation,String segmentationModelSpecificationString, int numSegmenterThreads) {
	try {
	    BufferedWriter fileWriter = new BufferedWriter(new FileWriter(outputConfigFilePath));

	    fileWriter.write(propertyValueStringWithNewline(
		    SentenceAlignedFilesCreaterFullCorpus.SOURCE_SEGMENT_FOLDER_PATHS_PROPERTY,
		    getSourceSegmentFolderPaths()));
	    fileWriter.write(propertyValueStringWithNewline(
		    SentenceAlignedFilesCreaterFullCorpus.TARGET_SEGMENT_FOLDER_PATHS_PROPERTY,
		    getTargetSegmentFolderPaths()));
	    fileWriter.write(propertyValueStringWithNewline(
		    SentenceAlignedFilesCreaterFullCorpus.ALIGNMENT_FOLDER_PATHS_PROPERTY,
		    getAlignmentFolderPaths()));
	    fileWriter.write(propertyValueStringWithNewline(
		    SentenceAlignedFilesCreaterFullCorpus.SOURCE_OUTPUT_FILE_PATH_PROPERTY,
		    fullCorpusSourceOutputFilePath));
	    fileWriter.write(propertyValueStringWithNewline(
		    SentenceAlignedFilesCreaterFullCorpus.TARGET_OUTPUT_FILE_PATH_PROPERTY,
		    fullCorpusTargetOutputFilePath));

	    writeSegmenterConfigValues(fileWriter, fullCorpusTargetOutputFilePath,
		    stanfordSegmenterRootDirLocation,segmentationModelSpecificationString, numSegmenterThreads);
	    fileWriter.close();
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }
}
