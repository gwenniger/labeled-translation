package datapreparation;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

public class SegmenterOutputChecker {
    private final String inputFilePath;
    private final String segmentedFilePath;

    private SegmenterOutputChecker(String inputFilePath, String segmentedFilePath) {
	this.inputFilePath = inputFilePath;
	this.segmentedFilePath = segmentedFilePath;
    }

    public static SegmenterOutputChecker createSegmenterOutputChecker(String inputFilePath,
	    String segmentedFilePath) {
	return new SegmenterOutputChecker(inputFilePath, segmentedFilePath);
    }

    public void checkSegmentedFile() {
	try {
	    LineIterator inputIterator = FileUtils.lineIterator(new File(inputFilePath), "UTF-8");
	    LineIterator resultIterator = FileUtils.lineIterator(new File(segmentedFilePath),
		    "UTF-8");

	    int lineNumber = 1;
	    while (inputIterator.hasNext()) {
		if (!resultIterator.hasNext()) {
		    throw new RuntimeException("Input file " + inputFilePath + "  has line "
			    + lineNumber + " but output file " + segmentedFilePath + "  does not");
		}
		String inputLine = inputIterator.nextLine();
		String segmentedLine = resultIterator.nextLine();
		String inputLineWithoutWhitespace = WhitespaceRemover
			.removeAllNonCharactersOrNumerals(inputLine);
		String segmentedLineWithoutWhitespace = WhitespaceRemover
			.removeAllNonCharactersOrNumerals(segmentedLine);

		if (!inputLineWithoutWhitespace.equals(segmentedLineWithoutWhitespace)) {
		    String errorString = "Input line without white space :\""
			    + inputLineWithoutWhitespace
			    + "\" and segmented line without white space\""
			    + segmentedLineWithoutWhitespace
			    + "\" are not the same for input line " + lineNumber + "\n"
			    + "inputLineWithoutWhitespace.length() "
			    + inputLineWithoutWhitespace.length()
			    + "\n segmentedLineWithoutWhitespace.length(): "
			    + segmentedLineWithoutWhitespace.length() + "\ninput line: \""
			    + inputLine + "\"" + "\nsegmented line: \"" + segmentedLine;

		    throw new RuntimeException(errorString);

		}
		lineNumber++;
	    }

	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }
}
