package datapreparation;

import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import util.ConfigFile;
import util.FileUtil;

/**
 * The class is responsible for writing sentence aligned source and target files
 * for the full corpus. The corpus itself is split into multiple subcorpora.
 * These are stored as List<SubCorpusPaths> subcorpusPathsList: every
 * SubCorpusPaths entry keeps track of the source,target and alignment file of
 * one subcorpus.
 * 
 * @author Gideon Maillette de Buy Wenniger
 * 
 */
public class SentenceAlignedFilesCreaterFullCorpus {
    public static final String SOURCE_SEGMENT_FOLDER_PATHS_PROPERTY = "sourceSegmentFolderPaths";
    public static final String TARGET_SEGMENT_FOLDER_PATHS_PROPERTY = "targetSegmentFolderPaths";
    public static final String ALIGNMENT_FOLDER_PATHS_PROPERTY = "alignmentFolderPaths";
    public static final String SOURCE_OUTPUT_FILE_PATH_PROPERTY = "sourceOutputFilePath";
    public static final String TARGET_OUTPUT_FILE_PATH_PROPERTY = "targetOutputFilePath";

    private final List<SubCorpusPaths> subcorpusPathsList;
    private final String sourceOutputFilePath;
    private final String targetOutputFilePath;

    private SentenceAlignedFilesCreaterFullCorpus(List<SubCorpusPaths> subcorpusPathsList,
	    String sourceOutputFilePath, String targetOutputFilePath) {
	this.subcorpusPathsList = subcorpusPathsList;
	this.sourceOutputFilePath = sourceOutputFilePath;
	this.targetOutputFilePath = targetOutputFilePath;
    }

    private static List<SubCorpusPaths> getSubCorpusPathsFromConfigFile(ConfigFile configFile) {
	List<SubCorpusPaths> result = new ArrayList<SubCorpusPaths>();
	List<String> sourcePaths = configFile
		.getMultiValueParmeterAsListWithPresenceCheck(SOURCE_SEGMENT_FOLDER_PATHS_PROPERTY);
	List<String> targetPaths = configFile
		.getMultiValueParmeterAsListWithPresenceCheck(TARGET_SEGMENT_FOLDER_PATHS_PROPERTY);
	List<String> alignmentPaths = configFile
		.getMultiValueParmeterAsListWithPresenceCheck(ALIGNMENT_FOLDER_PATHS_PROPERTY);
	Assert.assertEquals(sourcePaths.size(), targetPaths.size());
	Assert.assertEquals(targetPaths.size(), alignmentPaths.size());
	for (int i = 0; i < sourcePaths.size(); i++) {
	    result.add(new SubCorpusPaths(sourcePaths.get(i), targetPaths.get(i), alignmentPaths
		    .get(i)));
	}
	return result;
    }

    public static String getSourceOutputPathFromConfigFile(ConfigFile configFile) {
	return configFile.getValueWithPresenceCheck(SOURCE_OUTPUT_FILE_PATH_PROPERTY);
    }

    private static String getTargetOutputPathFromConfigFile(ConfigFile configFile) {
	return configFile.getValueWithPresenceCheck(TARGET_OUTPUT_FILE_PATH_PROPERTY);
    }

    public static SentenceAlignedFilesCreaterFullCorpus createSentenceAlignedFilesCreateFullCorpus(
	    ConfigFile configFile) {
	List<SubCorpusPaths> subcorpusPathsList = getSubCorpusPathsFromConfigFile(configFile);

	return new SentenceAlignedFilesCreaterFullCorpus(subcorpusPathsList,
		getSourceOutputPathFromConfigFile(configFile),
		getTargetOutputPathFromConfigFile(configFile));
    }

    private String getTempSourceOutputFilePath(int index) {
	return sourceOutputFilePath + ".subcorpus-" + index;
    }

    private String getTempTargetOutputFilePath(int index) {
	return targetOutputFilePath + ".subcorpus-" + index;
    }

    private void createCorpusSourceAndTargetFileByAppendingSubCorpusFiles() {

	FileUtil.copyFileNative(getTempSourceOutputFilePath(0), sourceOutputFilePath, true);
	FileUtil.copyFileNative(getTempTargetOutputFilePath(0), targetOutputFilePath, true);

	for (int index = 1; index < subcorpusPathsList.size(); index++) {
	    String sourceFilePath = getTempSourceOutputFilePath(index);
	    String targetFilePath = getTempTargetOutputFilePath(index);
	    FileUtil.appendFile(sourceOutputFilePath, sourceFilePath);
	    FileUtil.appendFile(targetOutputFilePath, targetFilePath);
	}
    }

    public void extractSentenceAlignedSourceAndTargetFullCorpus() {
	System.out.println("subcorpusPathsList.size(): " + subcorpusPathsList.size());

	for (int index = 0; index < subcorpusPathsList.size(); index++) {
	    System.out.println(">>>Writing sentence aligned files subcorpus " + index);
	    SubCorpusPaths subCorpusPaths = subcorpusPathsList.get(index);
	    BigSentenceAlignedFilesCreater bigSentenceAlignedFilesCreater = BigSentenceAlignedFilesCreater
		    .createBigSentenceAlignedFilesCreater(
			    subCorpusPaths.getSourceSegmentsFolderPath(),
			    subCorpusPaths.getTargetSegmentsFolderPath(),
			    subCorpusPaths.getAlignmentsFolderPath(),
			    getTempSourceOutputFilePath(index), getTempTargetOutputFilePath(index));
	    bigSentenceAlignedFilesCreater.writeBigSentenceAlignedFiles();
	}
	createCorpusSourceAndTargetFileByAppendingSubCorpusFiles();
    }

    private static class SubCorpusPaths {
	private final String sourceSegmentsFolderPath;
	private final String targetSegmentsFolderPath;
	private final String alignmentsFolderPath;

	private SubCorpusPaths(String sourceSegmentsFolderPath, String targetSegmentsFolderPath,
		String alignmentsFolderPath) {
	    this.sourceSegmentsFolderPath = sourceSegmentsFolderPath;
	    this.targetSegmentsFolderPath = targetSegmentsFolderPath;
	    this.alignmentsFolderPath = alignmentsFolderPath;
	}

	public String getAlignmentsFolderPath() {
	    return alignmentsFolderPath;
	}

	public String getTargetSegmentsFolderPath() {
	    return targetSegmentsFolderPath;
	}

	public String getSourceSegmentsFolderPath() {
	    return sourceSegmentsFolderPath;
	}
    }

}
