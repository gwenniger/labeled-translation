package datapreparation;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import util.FilePartMerger;
import mt_pipeline.parserInterface.InputFilePreprocessor;
import mt_pipeline.parserInterface.MultiThreadedExecutionInterface;

public class StanfordSegmenterInterface extends
	MultiThreadedExecutionInterface<StanfordSegmenterProcess> {
    protected final String encoding;
    private final String standfordSegmenterRootDir;
    private final String segmentationModelSpecificationString;
    private final int numSegmenterThreads;

    protected StanfordSegmenterInterface(InputFilePreprocessor inputFilePreprocessor,
	    String baseInputFileName, String encoding, String standfordSegmenterRootDir,
	    String segmentationModelSpecificationString, int numSegmenterThreads) {
	super(inputFilePreprocessor, baseInputFileName);
	this.encoding = encoding;
	this.standfordSegmenterRootDir = standfordSegmenterRootDir;
	this.segmentationModelSpecificationString = segmentationModelSpecificationString;
	this.numSegmenterThreads = numSegmenterThreads;
    }

    public static StanfordSegmenterInterface createStanfordSegmenterInterface(
	    String baseInputFileName, String encoding, String standfordSegmenterRootDir,
	    String segmentationModelSpecificationString, int numSegmenterThreads) {
	return new StanfordSegmenterInterface(new WhitespaceRemover(), baseInputFileName, "utf8",
		standfordSegmenterRootDir, segmentationModelSpecificationString,
		numSegmenterThreads);
    }

    public static StanfordSegmenterInterface createStanfordSegmenterInterface(
	    SegmenterConfig segmenterConfig) {
	String baseInputFileName = segmenterConfig.getSegmenterInputFilePath();
	return StanfordSegmenterInterface.createStanfordSegmenterInterface(baseInputFileName,
		"utf8", segmenterConfig.getSegmentationModelSpecificationString(),
		segmenterConfig.getStandfordSegmenterRootLocation(),
		segmenterConfig.getNumSegmenterThreads());
    }

    public static StanfordSegmenterInterface createStanfordSegmenterInterface(String configFilePath) {
	// theConfig.taggerConfig.getEnrichmentProducerSourceInputFilePath(category),
	// theConfig.filesConfig.getSourceTagSubFilePath(category),
	// theConfig.decoderConfig.getDataLength(category),
	// theConfig.taggerConfig.useUniversalTagSetForTaggingSource(), new
	// SourceTaggerProcessCreater()
	SegmenterConfig segmenterConfig = SegmenterConfig.createMtSegmenterConfig(configFilePath);
	return StanfordSegmenterInterface.createStanfordSegmenterInterface(segmenterConfig);
    }

    private void testInputFileContainsNoWhitespace() {
	try {
	    LineIterator lineIterator = FileUtils.lineIterator(new File(baseInputFileName));
	    int lineNumber = 1;
	    while (lineIterator.hasNext()) {
		String line = lineIterator.nextLine();
		String lineWithoutSpaces = WhitespaceRemover.removeSpacesThorough(line);
		int lineLenth = line.length();
		int lineWithoutSpacesLenth = lineWithoutSpaces.length();
		if (lineLenth != lineWithoutSpacesLenth) {
		    throw new RuntimeException("Error : input file contains white space in line "
			    + lineNumber + " line:\n" + line);
		}
		lineNumber++;
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private void segmentFilesMultipleProcesses() {
	System.out.println("baseInputFileName: " + baseInputFileName);
	// WhitespaceRemover.removeWhiteSpaceInPlace(baseInputFileName);
	// testInputFileContainsNoWhitespace();

	createMetaDataFilesMultipleProcesses();
	SegmenterOutputChecker segmenterOutputChecker = SegmenterOutputChecker
		.createSegmenterOutputChecker(baseInputFileName,
			SegmenterConfig.getSegmenterOutputFileName(baseInputFileName));
	segmenterOutputChecker.checkSegmentedFile();
    }

    protected void createMetaDataAnnotationFilesFromPreprocessedInputFiles() {
	segmentFilesMultipleProcesses();
    }

    public void tagFiles() {
	System.out.println("StandfordSegmenterInterface.tagFiles called...");
	createMetaDataAnnotationBaseFiles();
    }

    @Override
    protected void createMetaDataAnnotationBaseFiles() {
	createMetaDataAnnotationFilesFromPreprocessedInputFiles();
    }

    @Override
    protected StanfordSegmenterProcess createMetaDataCreatorProcess(String partFileName) {
	return new StanfordSegmenterProcess(partFileName, encoding,
		segmentationModelSpecificationString, standfordSegmenterRootDir);
    }

    public String getSegmenterOutputFileName() {
	return SegmenterConfig.getSegmenterOutputFileName(baseInputFileName);
    }

    @Override
    protected void mergePartResultsAndProduceFinalResult(String baseInputFileName, int numParts) {
	FilePartMerger.produceMergedPartsFile(getSegmenterOutputFileName(), numParts);
	FilePartMerger.deletePartFiles(baseInputFileName, numParts);
    }

    @Override
    protected int getNumberOfThreads() {
	return numSegmenterThreads;
    }

    @Override
    public void createResultFilesMutltiThreaded() {
	createMetaDataAnnotationBaseFiles();
    }

    public static void main(String[] args) {
	if (args.length != 1) {
	    System.out.println("Usage: >>> StandfordSegmenterInterface CONFIG_FILE_PATH");
	    System.exit(1);
	}

	String configFilePath = args[0];
	StanfordSegmenterInterface segmenterInterface = StanfordSegmenterInterface
		.createStanfordSegmenterInterface(configFilePath);
	segmenterInterface.createResultFilesMutltiThreaded();
    }

}
