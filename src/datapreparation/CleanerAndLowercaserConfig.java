package datapreparation;

public class CleanerAndLowercaserConfig {
    private static final String TOKENIZED_PREFIX = ".tok";
    private static final String CLEANED_PREFIX = ".clean";
    private static final String LOWERCASED_PREFIX = ".lowercased";

    private final LengthSetttings lengthSetttings;
    private final String scriptsDir;
    private final LanguageAbbreviationPair languageAbbreviationPair;
    private final String sourceFilePath;
    private final String targetFilePath;
    private final String corpusNameString;
    private final String workingDir;

    private CleanerAndLowercaserConfig(LengthSetttings lengthSetttings, String scriptsDir,
	    LanguageAbbreviationPair languageAbbreviationPair, String sourceFilePath,
	    String targetFilePath, String corpusNameString, String workingDir) {
	this.lengthSetttings = lengthSetttings;
	this.scriptsDir = scriptsDir;
	this.languageAbbreviationPair = languageAbbreviationPair;
	this.sourceFilePath = sourceFilePath;
	this.targetFilePath = targetFilePath;
	this.corpusNameString = corpusNameString;
	this.workingDir = workingDir;
    }

    public static CleanerAndLowercaserConfig createCorpusCleanerAndLowercaserConfigDefaultSettings(
	    String scriptsDir, String sourceAbbreviation, String targetAbbreviation,
	    String sourceFilePath, String targetFilePath, String corpusNameString, String workingDir) {
	return new CleanerAndLowercaserConfig(LengthSetttings.createDefaultLengthSettings(),
		scriptsDir, LanguageAbbreviationPair.createLanguageAbbreviationPair(
			sourceAbbreviation, targetAbbreviation), sourceFilePath, targetFilePath,
		corpusNameString, workingDir);
    }

    public String getTempSourceFilePath() {
	return getTempTokenizedFilesPathPrefix() + "." + getSourceAbbreviation();
    }

    public String getTempTargetFilePath() {
	return getTempTokenizedFilesPathPrefix() + "." + getTargetAbbreviation();
    }

    public String getBasicFileNamePathPrefix() {
	return workingDir + corpusNameString;
    }

    public String getTempTokenizedFilesPathPrefix() {
	return getBasicFileNamePathPrefix() + TOKENIZED_PREFIX;
    }

    public String getCleanedFilesPathPrefix() {
	return getBasicFileNamePathPrefix() + CLEANED_PREFIX;
    }

    public String getLowercasedFilesPathPrefix() {
	return getBasicFileNamePathPrefix() + LOWERCASED_PREFIX;
    }

    public String getCleanedSourceFilePath() {
	return getCleanedFilesPathPrefix() + "." + getSourceAbbreviation();
    }

    public String getCleanedTargetFilePath() {
	return getCleanedFilesPathPrefix() + "." + getTargetAbbreviation();
    }

    public String getCleanedAndLowercasedSourceFilePath() {
	return getLowercasedFilesPathPrefix() + "." + getSourceAbbreviation();
    }

    public String getCleanedAndLowercasedTargetFilePath() {
	return getLowercasedFilesPathPrefix() + "." + getTargetAbbreviation();
    }

    public String getSourceFilePath() {
	return sourceFilePath;
    }

    public String getTargetFilePath() {
	return targetFilePath;
    }

    public String getSourceAbbreviation() {
	return languageAbbreviationPair.getSourceAbbreviation();
    }

    public LengthSetttings getLengthSetttings() {
	return lengthSetttings;
    }

    public String getTargetAbbreviation() {
	return languageAbbreviationPair.getTargetAbbreviation();
    }

    public String getScriptsDir() {
	return scriptsDir;
    }

    public LanguageAbbreviationPair getLanguageAbbreviationPair() {
	return this.languageAbbreviationPair;
    }

    public static class LengthSetttings {
	private static final int DEFAULT_MIN_LENGTH = 1;
	private static final int DEFAULT_MAX_LENGTH = 40;

	private final int minLength;
	private final int maxLength;

	private LengthSetttings(int minLength, int maxLength) {
	    this.minLength = minLength;
	    this.maxLength = maxLength;
	}

	public static LengthSetttings createDefaultLengthSettings() {
	    return new LengthSetttings(DEFAULT_MIN_LENGTH, DEFAULT_MAX_LENGTH);
	}

	public int getMaxLength() {
	    return maxLength;
	}

	public int getMinLength() {
	    return minLength;
	}
    }
}
