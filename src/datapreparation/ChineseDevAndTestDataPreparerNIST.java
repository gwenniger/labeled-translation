package datapreparation;

import java.io.File;
import java.io.FileNotFoundException;

import util.ConfigFile;
import junit.framework.Assert;

public class ChineseDevAndTestDataPreparerNIST {
    private static final String XML_EXTENSION = ".xml";
    private static final String SOURCE_FILE_NAME = "source";
    private static final String ENGLISH_ABBREVIATION = "en";
    private static final String CHINESE_ABBREVIATION = "zh";

    private static final String SOURCE_FILE_PATH_PROPERTY = "sourceFilePath";
    private static final String TARGET_FILE_PATH_PROPERTY = "targetFilePath";
    private static final String SOURCE_ABBREVIATION_PROPERTY = "sourceAbbreviation";
    private static final String TARGET_ABBREVIATION_PROPERTY = "targetAbbreviation";

    private final String sourceAbbreviation;
    private final String targetAbbreviation;

    private ChineseDevAndTestDataPreparerNIST(String sourceAbbreviation, String targetAbbreviation) {
	this.sourceAbbreviation = sourceAbbreviation;
	this.targetAbbreviation = targetAbbreviation;
    }

    public static ChineseDevAndTestDataPreparerNIST createChineseDevAndTestDataPreparerNIST(
	    String sourceAbbreviation, String targetAbbreviation) {
	return new ChineseDevAndTestDataPreparerNIST(sourceAbbreviation, targetAbbreviation);
    }

    private String sourceExtension() {
	return "." + sourceAbbreviation;
    }

    private String refExtension() {
	return "." + targetAbbreviation;
    }

    private String getSourceFilePathFromXMLFilePath(String xmlFilePath) {
	Assert.assertTrue(xmlFilePath.endsWith(XML_EXTENSION));
	String prefix = new File(xmlFilePath).getParent() + "/";
	return prefix + SOURCE_FILE_NAME + sourceExtension();
    }

    private void createTextOnlyFileForSourceFile(String inputXMLFilePath, String fileEncodingString) {
	String outputFile = getSourceFilePathFromXMLFilePath(inputXMLFilePath);
	System.out.println("Creating text only file \"" + outputFile + "\" from :\""
		+ inputXMLFilePath + "\"");
	PlainTextFromXMLTextDocExtracter.createPlainTextFromXMLTextDocExtracterSourceFile(
		inputXMLFilePath, outputFile, fileEncodingString).writePlainTextToOutputFile(true);
    }

    private void createTextOnlyFilesForReferenceFile(String inputXMLFilePath, String outputFolder,
	    String fileEncodingString) {
	System.out.println("Creating text only  output files from :\"" + inputXMLFilePath + "\"");
	PlainTextFromXMLTextDocExtracter
		.createPlainTextFromXMLTextDocExtracterMultipleReferenceFile(inputXMLFilePath,
			outputFolder, fileEncodingString, refExtension())
		.writePlainTextToOutputFile(true);
    }

    public void extractTextOnlySourceAndTargetFiles(String sourceXMLFilePath,
	    String refXMLFilePath, String fileEncodingString) {
	createTextOnlyFileForSourceFile(sourceXMLFilePath, fileEncodingString);
	String outputFolder = new File(refXMLFilePath).getParentFile().getAbsolutePath() + "/";
	createTextOnlyFilesForReferenceFile(refXMLFilePath, outputFolder, fileEncodingString);
    }

    public static void extractData(String sourceXMLFilePath, String refXMLFilePath,
	    String sourceAbbrevaion, String targetAbbreviation) {

	ChineseDevAndTestDataPreparerNIST chineseDevAndTestDataPreparerNIST = createChineseDevAndTestDataPreparerNIST(
		sourceAbbrevaion, targetAbbreviation);
	chineseDevAndTestDataPreparerNIST.extractTextOnlySourceAndTargetFiles(sourceXMLFilePath,
		refXMLFilePath, FileEncodingConverter.UTF8_ENCODING);
    }

    public static void extractChineseEnglishData(String sourceXMLFilePath, String refXMLFilePath) {
	extractData(sourceXMLFilePath, refXMLFilePath, CHINESE_ABBREVIATION, ENGLISH_ABBREVIATION);
    }

    public static void extractEnglishChineseData(String sourceXMLFilePath, String refXMLFilePath) {
	extractData(sourceXMLFilePath, refXMLFilePath, ENGLISH_ABBREVIATION, CHINESE_ABBREVIATION);
    }

    private static String getSourceFilePath(ConfigFile theConfig) {
	return theConfig.getValueWithPresenceCheck(SOURCE_FILE_PATH_PROPERTY);
    }

    private static String getTargetFilePath(ConfigFile theConfig) {
	return theConfig.getValueWithPresenceCheck(TARGET_FILE_PATH_PROPERTY);
    }

    private static String getSourceAbbreviation(ConfigFile theConfig) {
	return theConfig.getValueWithPresenceCheck(SOURCE_ABBREVIATION_PROPERTY);
    }

    private static String getTargetAbbreviation(ConfigFile theConfig) {
	return theConfig.getValueWithPresenceCheck(TARGET_ABBREVIATION_PROPERTY);
    }

    public static void extractDataUsingConfig(String configFilePath) {
	try {
	    ConfigFile theConfig = new ConfigFile(configFilePath);
	    extractData(getSourceFilePath(theConfig), getTargetFilePath(theConfig),
		    getSourceAbbreviation(theConfig), getTargetAbbreviation(theConfig));
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}

    }

    public static void main(String[] args) {
	if (args.length != 1) {
	    System.out.println("Usage: createPlainTextFiles CONFIG_FILE_PATH");
	    System.exit(1);
	}
	String configFilePath = args[0];
	extractDataUsingConfig(configFilePath);

    }
}
