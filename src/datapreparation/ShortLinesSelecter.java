package datapreparation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import util.FileUtil;

public class ShortLinesSelecter {
    private final BufferedReader inputReader;
    private final BufferedWriter outputWriter;
    private final int maxLineLength;

    private ShortLinesSelecter(BufferedReader inputReader, BufferedWriter outputWriter,
	    int maxLineLength) {
	this.inputReader = inputReader;
	this.outputWriter = outputWriter;
	this.maxLineLength = maxLineLength;
    }

    public static ShortLinesSelecter createShortLinesSelecter(String inputFilePath,
	    String outputFilePath, int maxLineLength) {

	try {
	    BufferedReader inputReader = new BufferedReader(new FileReader(inputFilePath));
	    BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFilePath));
	    return new ShortLinesSelecter(inputReader, outputWriter, maxLineLength);
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}

    }

    public void writeShortLines() {
	try {
	    System.out.println("Writing file with lines of length up to " + maxLineLength);
	    String line;
	    while ((line = inputReader.readLine()) != null) {
		if (line.length() <= maxLineLength)
		    ;
		{
		    outputWriter.write(line + "\n");
		}
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	} finally {
	    FileUtil.closeCloseableIfNotNull(outputWriter);
	}
    }

    public static void main(String[] args) {
	if (args.length != 3) {
	    System.out
		    .println("Usage : selectShortLines INPUT_FILE_PATH OUTPUT_FILE_PATH MAX_LINE_LENGTH");
	    System.exit(1);
	}
	String inputFilePath = args[0];
	String outputFilePath = args[1];
	int maxLineLength = Integer.parseInt(args[2]);
	ShortLinesSelecter shortLinesSelecter = ShortLinesSelecter.createShortLinesSelecter(
		inputFilePath, outputFilePath, maxLineLength);
	shortLinesSelecter.writeShortLines();
    }

}
