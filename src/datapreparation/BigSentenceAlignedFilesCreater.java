package datapreparation;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Assert;

import util.FileUtil;

/**
 * This class serves to produce two big files of sentence aligned source and
 * target. This is done by using SentenceAlignedFilesCreater to produce the
 * sentence aligned source and target for the different segment files in the
 * source, target and alignment folder.
 * 
 * A boolean "filterLinePairsIfOneIsEmpty" is used to to exactly that: we don't
 * want sentence pairs if one is empty. This is what we will normally use.
 * 
 * @author Gideon Maillette de Buy Wenniger
 * 
 */
public class BigSentenceAlignedFilesCreater {
    private final String sourceFilesFolderPath;
    private final String targetFilesFolderPath;
    private final String alignmentFilesFolderPath;
    private final BufferedWriter bigSourceFileOutputWriter;
    private final BufferedWriter bigTargetFileOutputWriter;

    private BigSentenceAlignedFilesCreater(String sourceFilesFolderPath,
	    String targetFilesFolderPath, String alignmentFilesFolderPath,
	    BufferedWriter bigSourceFileOutputWriter, BufferedWriter bigTargetFileOutputWriter) {
	this.sourceFilesFolderPath = sourceFilesFolderPath;
	this.targetFilesFolderPath = targetFilesFolderPath;
	this.alignmentFilesFolderPath = alignmentFilesFolderPath;
	this.bigSourceFileOutputWriter = bigSourceFileOutputWriter;
	this.bigTargetFileOutputWriter = bigTargetFileOutputWriter;
    }

    public static BigSentenceAlignedFilesCreater createBigSentenceAlignedFilesCreater(
	    String sourceFilesFolderPath, String targetFilesFolderPath,
	    String alignmentFilesFolderPath, String bigSourceFileOutputPath,
	    String bigTargetFileOutputPath) {
	try {
	    BufferedWriter bigSourceFileOutputWriter = new BufferedWriter(new FileWriter(
		    bigSourceFileOutputPath));
	    BufferedWriter bigTargetFileOutputWriter = new BufferedWriter(new FileWriter(
		    bigTargetFileOutputPath));
	    return new BigSentenceAlignedFilesCreater(sourceFilesFolderPath, targetFilesFolderPath,
		    alignmentFilesFolderPath, bigSourceFileOutputWriter, bigTargetFileOutputWriter);
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private String sourceFilePath(String fileName) {
	return this.sourceFilesFolderPath + fileName;
    }

    private String targetFilePath(String fileName) {
	return this.targetFilesFolderPath + fileName;
    }

    private String alignmentFilePath(String fileName) {
	return this.alignmentFilesFolderPath + fileName;
    }

    private void closeOutputWriters() {
	FileUtil.closeCloseableIfNotNull(bigSourceFileOutputWriter);
	FileUtil.closeCloseableIfNotNull(bigTargetFileOutputWriter);
    }

    public void writeBigSentenceAlignedFiles() {
	DateOrderedFileNameListCreater dateOrderedFileNameListCreater = DateOrderedFileNameListCreater
		.createDateOrderedFileNameListCreater(alignmentFilesFolderPath);
	int totalNumLinesWritten = 0;
	boolean writeNewLineForFirstLine = false;
	for (String fileName : dateOrderedFileNameListCreater.getFileNamesSortedByDate()) {
	    SentenceAlignedFilesCreater sentenceAlignedFilesCreater = SentenceAlignedFilesCreater
		    .createSentenceAlignedFilesCreater(sourceFilePath(fileName),
			    targetFilePath(fileName), alignmentFilePath(fileName),
			    bigSourceFileOutputWriter, bigTargetFileOutputWriter,
			    writeNewLineForFirstLine);
	    totalNumLinesWritten += sentenceAlignedFilesCreater
		    .writeSourceAndTargetOutputToWriters();
	    // For every source/target file appended after the first one we
	    // write a new line
	    // for the first line
	    writeNewLineForFirstLine = true;
	}
	Assert.assertTrue(totalNumLinesWritten > 0);
	System.out.println("Total num lines written: " + totalNumLinesWritten);
	closeOutputWriters();
    }
}
