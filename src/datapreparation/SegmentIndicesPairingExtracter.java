package datapreparation;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;

public class SegmentIndicesPairingExtracter {

    private static final String SEGMENT_ID_LIST_TAG_SUFFIX = "SegId=";

    private static final String DOC_ID_PREFIX = "<ALIGNMENT docid=";

    private final BufferedReader segnmentFileReader;

    private SegmentIndicesPairingExtracter(BufferedReader segnmentFileReader) {
	this.segnmentFileReader = segnmentFileReader;
    }

    public static SegmentIndicesPairingExtracter createSegmentIndicesPairingExtracter(
	    String segmentFilePath) {
	BufferedReader segmentFileReader;
	try {
	    segmentFileReader = new BufferedReader(new FileReader(segmentFilePath));
	    return new SegmentIndicesPairingExtracter(segmentFileReader);
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private boolean isSegmentPairLine(String line) {
	return line.startsWith("<SentPair ") && line.endsWith("</SentPair>");
    }

    private List<Integer> getSegmentIndicesFromSegmentIndicesExtractionString(
	    String segmentIndicesExtractionString) {
	List<Integer> result = new ArrayList<Integer>();
	String segmentListString = SegmentListExtracter
		.getFirstStringBeforeWhitespace(segmentIndicesExtractionString);
	// System.out.println("SegmentListString: " + segmentListString);
	String segmentListStringWithoutParenthesis = segmentListString.substring(1,
		segmentListString.length() - 1);

	if (segmentListStringWithoutParenthesis.length() > 0) {
	    String[] integerStringsList = segmentListStringWithoutParenthesis.split(",");
	    for (String intString : integerStringsList) {
		result.add(Integer.parseInt(intString));
	    }
	}
	return result;
    }

    private boolean isNewDocLine(String line) {
	Assert.assertNotNull(line);
	//System.out.println("line: " + line);
	return line.startsWith(DOC_ID_PREFIX);
    }

    private String getDocID(String line) {
	int docIDBeginIndex = line.indexOf(DOC_ID_PREFIX) + DOC_ID_PREFIX.length();
	String result = SegmentListExtracter.getFirstStringBeforeWhitespace(line
		.substring(docIDBeginIndex));
	return result;
    }

    private boolean isEndDocLine(String line) {
	return line.endsWith("</ALIGNMENT>");
    }

    public List<DocSegmentPairing> extractSegmentIndicesPairingList() {
	try {

	    List<DocSegmentPairing> result = new ArrayList<DocSegmentPairing>();

	    List<SegmentPairing> resultList = new ArrayList<SegmentPairing>();

	    String line = null;
	    String docID = null;
	    while ((line = segnmentFileReader.readLine()) != null) {
		//System.out.println(" extractSegmentIndicesPairingList() - line" + line);
		if (isSegmentPairLine(line)) {
		    int beforeFirstSegmentListIndex = line.indexOf(SEGMENT_ID_LIST_TAG_SUFFIX)
			    + SEGMENT_ID_LIST_TAG_SUFFIX.length();
		    int beforeSecondSegmentListIndex = line.lastIndexOf(SEGMENT_ID_LIST_TAG_SUFFIX)
			    + SEGMENT_ID_LIST_TAG_SUFFIX.length();

		    String firstListExtractionString = line.substring(beforeFirstSegmentListIndex,
			    beforeSecondSegmentListIndex).trim();
		    String secondListExtractionString = line
			    .substring(beforeSecondSegmentListIndex).trim();

		    List<Integer> sourceSegmentIndices = getSegmentIndicesFromSegmentIndicesExtractionString(firstListExtractionString);
		    List<Integer> targetSegmentIndices = getSegmentIndicesFromSegmentIndicesExtractionString(secondListExtractionString);
		    SegmentPairing segmentPairing = new SegmentPairing(sourceSegmentIndices,
			    targetSegmentIndices);
		    //System.out.println("SegmentPairing: " + segmentPairing);
		    resultList.add(segmentPairing);
		} else if (isNewDocLine(line)) {
		    Assert.assertNotNull(line);
		    docID = getDocID(line);
		}
		// end of document reached
		else if (isEndDocLine(line)) {
		    DocSegmentPairing docSegmentPairing = new DocSegmentPairing(resultList, docID);
		    result.add(docSegmentPairing);
		    // Reset docID and resultList for the next document
		    docID = null;
		    resultList = new ArrayList<SegmentPairing>();
		}
	    }
	    return result;
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    public static class SegmentPairing {
	private final List<Integer> sourceSegmentIndices;
	private final List<Integer> targetSegmentIndices;

	public SegmentPairing(List<Integer> sourceSegmentIndices, List<Integer> targetSegmentIndices) {
	    this.sourceSegmentIndices = sourceSegmentIndices;
	    this.targetSegmentIndices = targetSegmentIndices;
	}

	public static SegmentPairing createSegmentPairing(List<Integer> sourceSegmentIndices,
		List<Integer> targetSegmentIndices) {
	    return new SegmentPairing(sourceSegmentIndices, targetSegmentIndices);
	}

	public List<Integer> getSourceSegmentIndices() {
	    return sourceSegmentIndices;
	}

	public List<Integer> getTargetSegmentIndices() {
	    return targetSegmentIndices;
	}

	public String toString() {
	    String result = "<SegmentPairing>";
	    result += " sourceIndices: " + sourceSegmentIndices;
	    result += " targetIndices: " + targetSegmentIndices;
	    result += " </SegmentPairing>";
	    return result;
	}

	private <E> boolean listsEqualOrBothNull(List<E> list1, List<E> list2) {
	    boolean result = false;
	    if (list1 == null) {
		result = (list2 == null);
	    } else {
		result = (list1.equals(list2));
	    }
	    return result;
	}

	public boolean equals(Object segmentPairObject) {
	    if (segmentPairObject instanceof SegmentPairing) {
		SegmentPairing segmentPairing2 = (SegmentPairing) segmentPairObject;

		boolean sourceSementIndicesEqual = listsEqualOrBothNull(this.sourceSegmentIndices,
			segmentPairing2.sourceSegmentIndices);
		boolean targetSementIndicesEqual = listsEqualOrBothNull(this.targetSegmentIndices,
			segmentPairing2.targetSegmentIndices);
		return sourceSementIndicesEqual && targetSementIndicesEqual;
	    }
	    return false;
	}

	public int hashCode() {
	    final int prime = 31;
	    int result = sourceSegmentIndices.hashCode() + prime * targetSegmentIndices.hashCode();
	    return result;
	}

	public boolean oneSideIsEmpty() {
	    return sourceSegmentIndices.isEmpty() || targetSegmentIndices.isEmpty();
	}
    }

    public static class DocSegmentPairing {
	private final List<SegmentPairing> segmentIndicesPairingList;
	private final String docID;

	private DocSegmentPairing(List<SegmentPairing> segmentIndicesPairingList, String docID) {
	    this.segmentIndicesPairingList = segmentIndicesPairingList;
	    this.docID = docID;
	}

	public String getDocID() {
	    return docID;
	}

	public List<SegmentPairing> getSegmentIndicesPairingList() {
	    return segmentIndicesPairingList;
	}

	public String toString() {
	    String result = "<DocSegmentPairing>";
	    result += "\ndocid: " + docID;
	    for (SegmentPairing segmentPairing : segmentIndicesPairingList) {
		result += "\n" + segmentPairing;
	    }
	    result += "<DocSegmentPairing>";
	    return result;
	}

    }

}
