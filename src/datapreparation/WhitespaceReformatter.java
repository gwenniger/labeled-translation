package datapreparation;

import datapreparation.SplitpatternPreservingStringTokenizer.SmartToken;
import java.util.List;
import java.util.regex.Pattern;

public class WhitespaceReformatter {

    public static String PERCENTAGE_MATCHING_REGULAR_EXPRESSION_STRING = "%";
    public static String DOUBLE_QUOTES_MATCHING_REGULAR_EXPRESSION_STRING = "\"";
    public static String SINGLE_QUOTES_MATCHING_REGULAR_EXPRESSION_STRING = "'";
    public static String ALL_NUMBERS_MATCHING_REGULAR_EXPRESSION_STRING = "\\d+(,\\d\\d\\d)*(\\.\\d+)?";
    Pattern ALL_NUMBERS_MATCHING_REGULAR_EXPRESSION_PATTERN = Pattern
	    .compile(ALL_NUMBERS_MATCHING_REGULAR_EXPRESSION_STRING);

    final SplitpatternPreservingStringTokenizer numberSeparatedTokenizer = SplitpatternPreservingStringTokenizer
	    .createSplitPatternPreservingStringTokenizer(ALL_NUMBERS_MATCHING_REGULAR_EXPRESSION_STRING);

    final SplitpatternPreservingStringTokenizer percentageSeparatedTokenizer = SplitpatternPreservingStringTokenizer
	    .createSplitPatternPreservingStringTokenizer(PERCENTAGE_MATCHING_REGULAR_EXPRESSION_STRING);

    final SplitpatternPreservingStringTokenizer singleQuotesSeparatedTokenizer = SplitpatternPreservingStringTokenizer
	    .createSplitPatternPreservingStringTokenizer(SINGLE_QUOTES_MATCHING_REGULAR_EXPRESSION_STRING);

    final SplitpatternPreservingStringTokenizer doubleQuotesSeparatedTokenizer = SplitpatternPreservingStringTokenizer
	    .createSplitPatternPreservingStringTokenizer(DOUBLE_QUOTES_MATCHING_REGULAR_EXPRESSION_STRING);

    private boolean isFirstIndex(int index) {
	return index == 0;
    }

    private boolean isLastIndex(int index, int totalNumElements) {
	return index == (totalNumElements - 1);
    }

    private String reformatStringWhitespaceOnBothSides(String inputString) {
	return " " + inputString.trim() + " ";
    }

    private String reformatStringWhitespaceOnLeftSide(String inputString) {
	return " " + inputString.trim();
    }

    private String reformatStringWhitespaceOnRightSide(String inputString) {
	return inputString.trim() + " ";
    }

    public String reformatWhiteSpaceWithTokenizer(SplitpatternPreservingStringTokenizer tokenizer,
	    String inputLine) {

	String result = "";

	List<SmartToken> tokensList = tokenizer.tokenize(inputLine);
	int currentIndex = 0;

	if (tokensList.size() == 1) {
	    return tokensList.get(0).getContents();
	}

	for (SmartToken token : tokensList) {
	    System.out.println("token: " + token.getContents() + "  is split pattern token: "
		    + token.isSplitPatternToken());

	    if (token.isSplitPatternToken()) {
		// Split pattern tokens are added without reformatting
		result += token.getContents();
	    } else {
		if (isFirstIndex(currentIndex)) {
		    result += reformatStringWhitespaceOnRightSide(token.getContents());
		} else if (isLastIndex(currentIndex, tokensList.size())) {
		    result += reformatStringWhitespaceOnLeftSide(token.getContents());
		} else {
		    result += reformatStringWhitespaceOnBothSides(token.getContents());
		}
	    }
	    System.out.println("current result String: \"" + result + "\"");
	    currentIndex++;
	}
	return result;
    }

    public String reformatWhiteSpace(String inputLine) {
	String result = reformatWhiteSpaceWithTokenizer(numberSeparatedTokenizer, inputLine);
	result = reformatWhiteSpaceWithTokenizer(percentageSeparatedTokenizer, result);
	result = reformatWhiteSpaceWithTokenizer(singleQuotesSeparatedTokenizer, result);
	result = reformatWhiteSpaceWithTokenizer(doubleQuotesSeparatedTokenizer, result);
	return result;
    }

}
