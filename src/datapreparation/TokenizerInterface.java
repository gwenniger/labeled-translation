package datapreparation;

import util.LinuxInteractor;

public class TokenizerInterface {
    private final String scriptsDir;

    private TokenizerInterface(String scriptsDir) {
	this.scriptsDir = scriptsDir;
    }

    public static TokenizerInterface createTokenizerInterface(String scriptsDir) {
	return new TokenizerInterface(scriptsDir);
    }

    public void createTokenizedFile(String inputFileName, String tokenizedOutputFileName,
	    String language) {
	// See:
	// http://stackoverflow.com/questions/525212/how-to-run-unix-shell-script-from-java-code
	// http://docs.oracle.com/javase/6/docs/api/java/lang/ProcessBuilder.html

	// LinuxInteractor.executeCommand(new String[]{getScriptsDir() +
	// "preprocess_data.sh", inputFileName, outputFileName, language},true);
	String tokenizationCommand = scriptsDir + "tokenizer.perl -l " + language + " < "
		+ inputFileName + " > " + tokenizedOutputFileName;
	LinuxInteractor.executeCommand(tokenizationCommand, true);
    }

}
