package datapreparation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import util.ConfigFile;
import util.FileStatistics;
import util.FileUtil;

public class DuplicatesRemover {

    private static final String DUPLICATE_DEFINING_INPUT_FILE_PATHS_PROPERTY_STRING = "duplicateDefiningInputFilePaths";
    private static final String ASSOCIATED_REMAINING_INPUT_FILE_PATHS_PROPERTY_STRING = "associatedRemainingInputFilePaths";
    private static final String OUTPUT_FOLDER_PATH_PROPERTY_STRING = "outputFolderPath";

    private final Set<List<String>> observedSynchronousLineTuples;
    private final List<FileRewriter> duplicateDefiningFileRewriters;
    private final List<FileRewriter> associatedRemainingFileRewriters;

    private DuplicatesRemover(List<FileRewriter> duplicateDefiningFileRewriters,
	    List<FileRewriter> associatedRemainingFileRewriters,
	    Set<List<String>> observedSynchronousLineTuples) {

	this.duplicateDefiningFileRewriters = duplicateDefiningFileRewriters;
	this.associatedRemainingFileRewriters = associatedRemainingFileRewriters;
	this.observedSynchronousLineTuples = observedSynchronousLineTuples;
    }

    public static List<FileRewriter> createFileRewriters(List<String> inputFilePaths,
	    String outputFolderPath) {
	List<FileRewriter> result = new ArrayList<FileRewriter>();
	for (String inputFilePath : inputFilePaths) {
	    result.add(FileRewriter.createFileRewriter(inputFilePath, outputFolderPath));
	}
	return result;
    }

    public static boolean inputFilesAllHaveSameNumberOfLines(List<String> inputFilePaths) {
	try {
	    int firstFileNumLines = FileStatistics.countLines(inputFilePaths.get(0));
	    for (int i = 1; i < inputFilePaths.size(); i++) {
		int fileNumLines = FileStatistics.countLines(inputFilePaths.get(i));
		if (firstFileNumLines != fileNumLines) {
		    return false;
		}
	    }
	    return true;
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private static List<String> getDuplicateDefiningInputFilePathsFromConfig(ConfigFile theConfig) {
	return theConfig
		.getMultiValueParmeterAsListWithPresenceCheck(DUPLICATE_DEFINING_INPUT_FILE_PATHS_PROPERTY_STRING);
    }

    private static List<String> getAssociatedRemainingInputFilePathsFromConfig(ConfigFile theConfig) {
	return theConfig
		.getMultiValueParmeterAsListWithPresenceCheck(ASSOCIATED_REMAINING_INPUT_FILE_PATHS_PROPERTY_STRING);
    }

    private static String getOutputFilePathFromConfig(ConfigFile theConfig) {
	return theConfig.getValueWithPresenceCheck(OUTPUT_FOLDER_PATH_PROPERTY_STRING);
    }

    public static DuplicatesRemover createDuplicatesRemover(
	    List<String> duplicateDefinningInputFilePaths,
	    List<String> associatedRemainingInputFilePaths, String outputFolderPath) {

	if (!inputFilesAllHaveSameNumberOfLines(duplicateDefinningInputFilePaths)) {
	    System.out
		    .println("Error: not all input files have the same number of lines. Aborting");
	    System.exit(1);
	}

	List<FileRewriter> duplicateDefiningFileRewriters = createFileRewriters(
		duplicateDefinningInputFilePaths, outputFolderPath);
	List<FileRewriter> associatedRemainingFileRewriters = createFileRewriters(
		associatedRemainingInputFilePaths, outputFolderPath);
	Set<List<String>> observedSynchronousLineTuples = new HashSet<List<String>>();
	return new DuplicatesRemover(duplicateDefiningFileRewriters,
		associatedRemainingFileRewriters, observedSynchronousLineTuples);
    }

    public static DuplicatesRemover createDuplicatesRemover(String configFilePath) {
	ConfigFile theConfig;
	try {
	    theConfig = new ConfigFile(configFilePath);
	    return DuplicatesRemover.createDuplicatesRemover(
		    getDuplicateDefiningInputFilePathsFromConfig(theConfig),
		    getAssociatedRemainingInputFilePathsFromConfig(theConfig),
		    getOutputFilePathFromConfig(theConfig));
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private List<String> getNextDuplicatesDefiningLinesTuple() {
	List<String> result = new ArrayList<String>();
	for (FileRewriter fileRewriter : this.duplicateDefiningFileRewriters) {
	    result.add(fileRewriter.nextLine());
	}
	return result;
    }

    private List<String> getNextAssociatedRemainingLinesTuple() {
	List<String> result = new ArrayList<String>();
	for (FileRewriter fileRewriter : this.associatedRemainingFileRewriters) {
	    result.add(fileRewriter.nextLine());
	}
	return result;
    }

    private boolean observedDuplicatesDefiningNextLinesTupleBefore(List<String> nextLinesTuple) {
	return this.observedSynchronousLineTuples.contains(nextLinesTuple);
    }

    private boolean addObservedNextLinesTupleToObservedTuplesSet(List<String> nextLinesTuple) {
	return this.observedSynchronousLineTuples.add(nextLinesTuple);
    }

    private static void writeNextLinesTupleToRewrittenOutputFiles(List<String> nextLinesTuple,
	    List<FileRewriter> fileRewriters) {
	for (int i = 0; i < nextLinesTuple.size(); i++) {
	    String line = nextLinesTuple.get(i);
	    fileRewriters.get(i).writeLine(line);
	}
    }

    private void writeNextDuplicateDefiningLinesToRewrittenOutputLines(List<String> nextLinesTuple) {
	writeNextLinesTupleToRewrittenOutputFiles(nextLinesTuple, duplicateDefiningFileRewriters);
    }

    private void writeNextRemainingAssociatedLinesToRewrittenOutputLines(List<String> nextLinesTuple) {
	writeNextLinesTupleToRewrittenOutputFiles(nextLinesTuple, associatedRemainingFileRewriters);
    }

    private boolean hasNextLines() {
	boolean result = true;
	for (FileRewriter fileRewriter : this.duplicateDefiningFileRewriters) {
	    if (!fileRewriter.hasNextLine()) {
		result = false;
	    }
	    break;
	}
	return result;
    }

    public void produceRewrittenFilesWithoutDuplicates() {
	while (hasNextLines()) {
	    List<String> nextDuplicatesDefiningLinesTuple = getNextDuplicatesDefiningLinesTuple();
	    List<String> nextAssociatedRemainingLinesTuple = getNextAssociatedRemainingLinesTuple();

	    if (!observedDuplicatesDefiningNextLinesTupleBefore(nextDuplicatesDefiningLinesTuple)) {
		/**
		 * If the duplicates defining tuple is not observed before, we
		 * write both the lines in that tuple ass well as the associated
		 * remaining line tuples to their respective output writers
		 */
		writeNextDuplicateDefiningLinesToRewrittenOutputLines(nextDuplicatesDefiningLinesTuple);
		writeNextRemainingAssociatedLinesToRewrittenOutputLines(nextAssociatedRemainingLinesTuple);
	    }

	    // Only the duplicates defining lines tuple is added to the observed
	    // tuples set
	    addObservedNextLinesTupleToObservedTuplesSet(nextDuplicatesDefiningLinesTuple);
	}
	closeOutputWriters();
    }

    private void closeOutputWriters() {
	for (FileRewriter fileRewriter : this.duplicateDefiningFileRewriters) {
	    fileRewriter.closeWriter();
	}
	for (FileRewriter fileRewriter : this.associatedRemainingFileRewriters) {
	    fileRewriter.closeWriter();
	}

    }

    public static class FileRewriter {
	private final LineIterator inputLineIterator;
	private final BufferedWriter outputWriter;

	private FileRewriter(LineIterator inputLineIterator, BufferedWriter outputWriter) {
	    this.inputLineIterator = inputLineIterator;
	    this.outputWriter = outputWriter;
	}

	public static FileRewriter createFileRewriter(String inputFilePath, String outputFolderPath) {
	    String inputFileName = (new File(inputFilePath)).getName();
	    String outputFilePath = outputFolderPath + inputFileName;

	    try {
		LineIterator inputLineIterator = FileUtils.lineIterator(new File(inputFilePath),
			"UTF8");
		BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFilePath));
		return new FileRewriter(inputLineIterator, outputWriter);

	    } catch (IOException e) {
		e.printStackTrace();
		throw new RuntimeException(e);
	    }
	}

	public String nextLine() {
	    return inputLineIterator.nextLine();
	}

	public void writeLine(String line) {
	    try {
		outputWriter.write(line + "\n");
	    } catch (IOException e) {
		e.printStackTrace();
		throw new RuntimeException(e);
	    }
	}

	public boolean hasNextLine() {
	    return this.inputLineIterator.hasNext();
	}

	public void closeWriter() {
	    FileUtil.closeCloseableIfNotNull(outputWriter);
	}
    }

    public static void main(String[] args) {
	if (args.length != 1) {
	    System.out.println("usage: duplicatesRemover CONFIG_FILE_PATH");
	    System.exit(1);
	}
	DuplicatesRemover duplicatesRemover = DuplicatesRemover.createDuplicatesRemover(args[0]);
	duplicatesRemover.produceRewrittenFilesWithoutDuplicates();
    }

}
