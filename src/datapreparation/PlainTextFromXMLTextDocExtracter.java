package datapreparation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import bitg.Pair;
import junit.framework.Assert;
import util.FileUtil;

public class PlainTextFromXMLTextDocExtracter {
    private static final String SEGMENT_LINE_PREFIX = "<seg";
    private static final String DOC_START_PREFIX = "<doc";
    private static final String SYS_ID_TAG = "sysid=\"";
    private final String inputFilePath;
    private final OutputWriterSelecter outputWriterSelecter;
    private final String fileEncodingString;

    private PlainTextFromXMLTextDocExtracter(String inputFilePath,
	    OutputWriterSelecter outputWriterSelecter, String fileEncodingString) {
	this.inputFilePath = inputFilePath;
	this.outputWriterSelecter = outputWriterSelecter;
	this.fileEncodingString = fileEncodingString;
    }

    public static PlainTextFromXMLTextDocExtracter createPlainTextFromXMLTextDocExtracter(
	    String inputFilePath, BufferedWriter outputWriter, String fileEncodingString) {
	return new PlainTextFromXMLTextDocExtracter(inputFilePath,
		SingleReferenceOutputWriterSelecter
			.createSingleReferenceOutputWriterSelecter(outputWriter),
		fileEncodingString);
    }

    public static PlainTextFromXMLTextDocExtracter createPlainTextFromXMLTextDocExtracterSourceFile(
	    String inputFilePath, String outputFilePath, String fileEncodingString) {
	BufferedWriter outputWriter;
	try {
	    outputWriter = FileUtil.createBufferedWriterWithSpecificEncoding(outputFilePath,
		    FileEncodingConverter.UTF8_ENCODING);
	    return new PlainTextFromXMLTextDocExtracter(inputFilePath,
		    SingleReferenceOutputWriterSelecter
			    .createSingleReferenceOutputWriterSelecter(outputWriter),
		    fileEncodingString);
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    public static PlainTextFromXMLTextDocExtracter createPlainTextFromXMLTextDocExtracterMultipleReferenceFile(
	    String inputFilePath, String outputFolderPath, String fileEncodingString,
	    String referenceLanguageExtension) {
	return new PlainTextFromXMLTextDocExtracter(inputFilePath,
		MultipleReferenceOutputWriterSelecter.createMultipleReferenceOutputWriterSelecter(
			inputFilePath, fileEncodingString, outputFolderPath,
			referenceLanguageExtension), fileEncodingString);
    }

    public boolean isTextLine(String line) {
	return line.startsWith(SEGMENT_LINE_PREFIX);
    }

    private static boolean isDocLine(String line) {
	return line.startsWith(DOC_START_PREFIX);
    }

    /**
     * Get the reference String from a doc line like <doc
     * docid="AFP_CMN_20070704.0016" genre="text" sysid="reference_1"> (which
     * would be 'reference_1' in this example)
     * 
     * @param docLine
     * @return
     */
    private static String getReferenceStringFromDocLine(String docLine) {
	int referenceNameSubstringStartIndex = docLine.indexOf(SYS_ID_TAG) + SYS_ID_TAG.length();
	int referenceNameSubstringEndIndex = docLine.lastIndexOf("\">") - 1;
	String referenceNameString = docLine.substring(referenceNameSubstringStartIndex,
		referenceNameSubstringEndIndex + 1);
	return referenceNameString;
    }

    private int textBeginIndex(String line) {
	return line.indexOf(">") + 1;
    }

    private int textEndIndex(String line) {
	return line.lastIndexOf("<") - 1;
    }

    private String getContentsPartSegmentLine(String line) {
	return line.substring(textBeginIndex(line), textEndIndex(line) + 1);
    }

    /**
     * This method cleans up some of the formatting used in the NIST (2008)
     * corpus, replacing the apostrophe and quote codes with "the real thing" 
     * @param line
     * @return
     */
    private String replaceAposAndQuoteCodesWithActualAposAndQute(String line) {
	// Replace &apos; : => "'"
	String result = line.replace("&apos;", "'");
	result = result.replace("&apos", "'");
	// Replace "&quot" with or without semicolon : => "'"
	result = result.replace("&quot;", "\"");
	result = result.replace("&quot", "\"");
	return result;
    }

    public void writePlainTextToOutputFile(boolean closeWriterAfterProcessingFile) {

	try {
	    LineIterator it = FileUtils.lineIterator(new File(this.inputFilePath),
		    fileEncodingString);
	    List<Pair<String, BufferedWriter>> linesList = new ArrayList<Pair<String, BufferedWriter>>();

	    BufferedWriter outputWriter = outputWriterSelecter.getDefaultOutputWriter();
	    while (it.hasNext()) {
		String line = it.nextLine();
		// System.out.println("line: " + line);

		if (isDocLine(line)) {
		    outputWriter = this.outputWriterSelecter.getOutputWriterForDocLine(line);
		    System.out.println("Selected Writer: " + outputWriter);
		}

		if (isTextLine(line)) {
		    Assert.assertNotNull(outputWriter);
		    String contentPart = getContentsPartSegmentLine(line);
		    // The NIST corpus contains some weird apos and quote codes that we want to replace 
		    // with the real thing as used in the rest of our formatting
		    contentPart = replaceAposAndQuoteCodesWithActualAposAndQute(contentPart);
		    Pair<String, BufferedWriter> pair = new Pair<String, BufferedWriter>(
			    contentPart, outputWriter);
		    linesList.add(pair);
		}
	    }
	    for (int i = 0; i < linesList.size(); i++) {
		String lineToWrite = linesList.get(i).first;
		outputWriter = linesList.get(i).last;
		if (i < linesList.size() - 1) {
		    lineToWrite += "\n";
		}
		outputWriter.write(lineToWrite);
	    }
	    it.close();
	} catch (IOException e) {
	    e.printStackTrace();
	}

	finally {
	    if (closeWriterAfterProcessingFile) {
		outputWriterSelecter.closeAllOutputWriters();
	    }
	}
    }

    public interface OutputWriterSelecter {
	public BufferedWriter getOutputWriterForDocLine(String docString);

	public BufferedWriter getDefaultOutputWriter();

	public void closeAllOutputWriters();
    }

    public static class SingleReferenceOutputWriterSelecter implements OutputWriterSelecter {
	private final BufferedWriter outputWriter;

	private SingleReferenceOutputWriterSelecter(BufferedWriter outputWriter) {
	    this.outputWriter = outputWriter;
	}

	public static SingleReferenceOutputWriterSelecter createSingleReferenceOutputWriterSelecter(
		BufferedWriter outputWriter) {
	    return new SingleReferenceOutputWriterSelecter(outputWriter);
	}

	@Override
	public BufferedWriter getOutputWriterForDocLine(String docString) {
	    return outputWriter;
	}

	@Override
	public void closeAllOutputWriters() {
	    FileUtil.closeCloseableIfNotNull(outputWriter);
	}

	@Override
	public BufferedWriter getDefaultOutputWriter() {
	    return outputWriter;
	}

    }

    public static class MultipleReferenceOutputWriterSelecter implements OutputWriterSelecter {
	private final Map<String, BufferedWriter> outputWriterSelectionMap;

	private MultipleReferenceOutputWriterSelecter(
		Map<String, BufferedWriter> outputWriterSelectionMap) {
	    this.outputWriterSelectionMap = outputWriterSelectionMap;
	}

	public static MultipleReferenceOutputWriterSelecter createMultipleReferenceOutputWriterSelecter(
		String inputFilePath, String fileEncodingString, String outputFolderPath,
		String referenceLanguageExtension) {
	    Map<String, BufferedWriter> outputWriterSelectionMap = createOutputWriterSelectionMapForInputFileAndOutputFolderSpecification(
		    inputFilePath, fileEncodingString, outputFolderPath, referenceLanguageExtension);
	    return new MultipleReferenceOutputWriterSelecter(outputWriterSelectionMap);
	}

	private static Set<String> getReferenceNamesFromInputFile(String inputFilePath,
		String fileEncodingString) {
	    try {
		Set<String> result = new HashSet<String>();
		LineIterator it = FileUtils.lineIterator(new File(inputFilePath),
			fileEncodingString);

		while (it.hasNext()) {
		    String line = it.nextLine();
		    if (isDocLine(line)) {
			result.add(getReferenceStringFromDocLine(line));
		    }
		}
		it.close();
		return result;
	    } catch (IOException e) {
		e.printStackTrace();
		throw new RuntimeException(e);
	    }
	}

	private static String getReferenceOutputFilePath(String outputFolderPath,
		String referenceName, String referenceLanguageExtension) {
	    return outputFolderPath + referenceName + referenceLanguageExtension;
	}

	private static Map<String, BufferedWriter> createOutputWriterSelectionMapForInputFileAndOutputFolderSpecification(
		String inputFilePath, String fileEncodingString, String outputFolderPath,
		String referenceLanguageExtension) {

	    try {
		Set<String> referenceNames = getReferenceNamesFromInputFile(inputFilePath,
			fileEncodingString);
		Map<String, BufferedWriter> result = new HashMap<String, BufferedWriter>();

		for (String referenceName : referenceNames) {
		    String referenceOutputFilePath = getReferenceOutputFilePath(outputFolderPath,
			    referenceName, referenceLanguageExtension);
		    System.out
			    .println("Creating output writer writing to " + referenceOutputFilePath
				    + " for reference: \"" + referenceName + "\"");
		    BufferedWriter outputWriter = new BufferedWriter(new FileWriter(
			    referenceOutputFilePath));
		    result.put(referenceName, outputWriter);
		}
		System.out.println("outputWriterSelectionMap.keySet(): " + result.keySet());
		return result;
	    } catch (IOException e) {
		e.printStackTrace();
		throw new RuntimeException(e);
	    }
	}

	@Override
	public BufferedWriter getOutputWriterForDocLine(String docLine) {
	    return outputWriterSelectionMap.get(getReferenceStringFromDocLine(docLine));
	}

	@Override
	public void closeAllOutputWriters() {
	    for (BufferedWriter outputWriter : this.outputWriterSelectionMap.values()) {
		FileUtil.closeCloseableIfNotNull(outputWriter);
	    }
	}

	@Override
	public BufferedWriter getDefaultOutputWriter() {
	    return null;
	}
    }

}