package datapreparation;

import util.ConfigFile;

public class AlignedCorpusPreparationConfig {

    private static final String FULL_SOURCE_SENTENCES_FILE_NAME = "source.txt";
    private static final String FULL_TARGET_SENTENCES_FILE_NAME = "target.txt";
    private static final String FULL_TARGET_SENTENCES_FILE_NAME_NO_WHITEPSACE = "targetNoWhitespace.txt";
    private static final String EXTRACTION_CONFIG_FILE_NAME = "extractionConfig.txt";
    private final ConfigFile theConfig;
    private final String hkDataFolderPath;
    private final String workingDirPath;
    private final String defaultInputEncoding;
    private final String scriptsDir;
    protected final SegmenterConfig segmenterConfig;
    protected final CleanerAndLowercaserConfig corpusCleanerAndLowercaserConfig;
    private final String mosesScriptsDir;
    private final String mGizaBinDir;
    private final int numWordAlignerThreads;
    private final String alignmentHeuristicString;

    private AlignedCorpusPreparationConfig(ConfigFile theConfig, String hkDataFolderPath,
	    String workingDirPath, String defaultInputEncoding, String scriptsDir,
	    SegmenterConfig segmenterConfig,
	    CleanerAndLowercaserConfig corpusCleanerAndLowercaserConfig, String mosesScriptsDir,
	    String mGizaBinDir, int numWordAlignerThreads, String alignmentHeuristicString) {
	this.theConfig = theConfig;
	this.hkDataFolderPath = hkDataFolderPath;
	this.workingDirPath = workingDirPath;
	this.defaultInputEncoding = defaultInputEncoding;
	this.scriptsDir = scriptsDir;
	this.segmenterConfig = segmenterConfig;
	this.corpusCleanerAndLowercaserConfig = corpusCleanerAndLowercaserConfig;
	this.mosesScriptsDir = mosesScriptsDir;
	this.mGizaBinDir = mGizaBinDir;
	this.numWordAlignerThreads = numWordAlignerThreads;
	this.alignmentHeuristicString = alignmentHeuristicString;
    }

    public static AlignedCorpusPreparationConfig createAlignedCorpusPreparationConfig(
	    ConfigFile theConfig, String hkDataFolderPath, String workingDirPath,
	    String defaultInputEncoding, String scriptsDir, SegmenterConfig segmenterConfig,
	    CleanerAndLowercaserConfig corpusCleanerConfig, String mosesScriptsDir,
	    String mGizaBinDir, int numWordAlignerThreads,String alignmentHeuristicString) {
	return new AlignedCorpusPreparationConfig(theConfig, hkDataFolderPath, workingDirPath,
		defaultInputEncoding, scriptsDir, segmenterConfig, corpusCleanerConfig,
		mosesScriptsDir, mGizaBinDir, numWordAlignerThreads,alignmentHeuristicString);
    }

    public ConfigFile getConfig() {
	return theConfig;
    }

    public String getHkDataFolderPath() {
	return hkDataFolderPath;
    }

    public String getWorkingDirPath() {
	return workingDirPath;
    }

    public String getDefaultInputEncoding() {
	return defaultInputEncoding;
    }

    public String getScriptsDir() {
	return scriptsDir;
    }

    public static String fullCorpusSourceOutputFilePath(String workingDirPath) {
	return workingDirPath + FULL_SOURCE_SENTENCES_FILE_NAME;
    }

    public static String fullCorpusTargetOutputFilePath(String workingDirPath) {
	return workingDirPath + FULL_TARGET_SENTENCES_FILE_NAME;
    }

    public String fullCorpusTargetNoWhitepaceOutputFilePath() {
	return workingDirPath + FULL_TARGET_SENTENCES_FILE_NAME_NO_WHITEPSACE;
    }

    public static String extractionConfigPath(String workingDirPath) {
	return workingDirPath + EXTRACTION_CONFIG_FILE_NAME;
    }

    public String extractionConfigPath() {
	return workingDirPath + EXTRACTION_CONFIG_FILE_NAME;
    }

    public String getMosesScriptsDir() {
	return mosesScriptsDir;
    }

    public String getWordAlignerInputFilesPrefix() {
	return this.corpusCleanerAndLowercaserConfig.getLowercasedFilesPathPrefix();
    }

    public LanguageAbbreviationPair getLanguageAbbreviationPair() {
	return this.corpusCleanerAndLowercaserConfig.getLanguageAbbreviationPair();
    }

    public String getMGizaBinDir() {
	return mGizaBinDir;
    }

    public int getNumWordAlignerThreads() {
	return numWordAlignerThreads;
    }

    public String getAlignmentHeuristicString() {
	return alignmentHeuristicString;
    }

}
