package datapreparation;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;

import util.FileStatistics;
import util.FileUtil;
import util.LinuxInteractor;

public class FileEncodingConverter {

    public static String UTF8_ENCODING = "UTF8";
    public static String GB2312_ENCODING = "GB2312";
    public static String ISO_88591_ENCODING = "ISO-8859-1";
    
    private static List<String> RELIABLE_DETECTED_INPUT_ENCODINGS = Arrays.asList(UTF8_ENCODING);
    // private static final String DEFAULT_ENCODING = "big5" ;// "big5hkscs";
    // //"big5-hkscs"; // "big5"

    // See
    // http://www.linuxquestions.org/questions/linux-software-2/how-to-make-iconv-to-skip-incorrect-symbols-or-iconv-alternative-893435/
    private static String OMIT_INVALID_CHARACTERS_FROM_OUTPUT_ICONV_SWITCH = "-sc";

    // See also: http://kjetilvalle.com/posts/text-file-encodings.html
    private static final String FIND_COMMAND_STIRNG = "file -b --mime-encoding";
    private final String conversionInputFolderPath;
    private final String conversionOutputFolderPath;
    private final String convertToEncodingFormat;
    private final String defaultInputEncoding;

    private FileEncodingConverter(String conversionInputFolderPath,
	    String conversionOutputFolderPath, String convertToEncodingFormat,
	    String defaultInputEncoding) {
	this.conversionInputFolderPath = conversionInputFolderPath;
	this.conversionOutputFolderPath = conversionOutputFolderPath;
	this.convertToEncodingFormat = convertToEncodingFormat;
	this.defaultInputEncoding = defaultInputEncoding;
    }

    public static FileEncodingConverter createFileEncodingConverter(
	    String conversionInputFolderPath, String conversionOutputFolderPath,
	    String convertToEncodingFormat, String defaultInputEncoding) {
	return new FileEncodingConverter(conversionInputFolderPath, conversionOutputFolderPath,
		convertToEncodingFormat, defaultInputEncoding);
    }

    public static String getEncodingStringForFile(String filePath) {
	String command = FIND_COMMAND_STIRNG + " " + filePath;
	String encodingInfoString = LinuxInteractor.executeCommand(command, true);
	return encodingInfoString.trim();
    }

    private static String conversionCommand(String inputFilePath, String outputFilePath,
	    String inputEncoding, String outputEncoding) {
	String result = "iconv " + OMIT_INVALID_CHARACTERS_FROM_OUTPUT_ICONV_SWITCH + " -f "
		+ inputEncoding + " -t " + outputEncoding + " -o " + outputFilePath + " < "
		+ inputFilePath;
	return result;
    }

    public static void performConversionInPlace(String inputFilePath, String inputEncoding,
	    String outputEncoding) {
	String tempFilePath = inputFilePath + ".temp";
	String conversionCommand = conversionCommand(inputFilePath, tempFilePath, inputEncoding,
		outputEncoding);
	LinuxInteractor.executeCommand(conversionCommand, true);
	FileUtil.copyFileNative(tempFilePath, inputFilePath, true);
	new File(tempFilePath).delete();
    }

    private static void performConversionWithSeparateOutputFile(String inputFilePath,
	    String outputFilePath, String inputEncoding, String outputEncoding) {
	String conversionCommand = conversionCommand(inputFilePath, outputFilePath, inputEncoding,
		outputEncoding);
	LinuxInteractor.executeCommand(conversionCommand, true);
    }

    private static void testNumLinesRemainedUnchanged(int linesBeforeConversion,
	    int linesAfterConversion) {
	if (linesBeforeConversion != linesAfterConversion) {
	    throw new RuntimeException(
		    "Error: something went wrong during encoding conversion - num lines input file: "
			    + linesBeforeConversion + " num lines after conversion: "
			    + linesAfterConversion);
	}
    }

    public static void performConversion(String inputFilePath, String outputFilePath,
	    String outputEncoding, String defaultInputEncoding) throws IOException {
	String inputEncoding = getEncodingStringForFile(inputFilePath);

	System.out.println("automatically dectected encoding: " + inputEncoding);

	if (!RELIABLE_DETECTED_INPUT_ENCODINGS.contains(inputEncoding)) {
	    // Everything that looks weird we assume is big5 (or big5-hkscs)
	    // encoded
	    inputEncoding = defaultInputEncoding;
	}

	if (inputEncoding != outputEncoding) {
	    int linesBeforeConversion = FileStatistics.countLines(inputFilePath);
	    if (inputFilePath == outputFilePath) {
		performConversionInPlace(inputFilePath, inputEncoding, outputEncoding);
	    } else {
		performConversionWithSeparateOutputFile(inputFilePath, outputFilePath,
			inputEncoding, outputEncoding);
	    }
	    int linesAfterConversion = FileStatistics.countLines(outputFilePath);
	    testNumLinesRemainedUnchanged(linesBeforeConversion, linesAfterConversion);
	} else {
	    throw new RuntimeException("Errror : inputEncoding and outputEncoding are same");
	}
    }

    private void copyDirectory() {
	try {
	    FileUtil.removeDirectory(new File(conversionOutputFolderPath));
	    FileUtils.copyDirectory(new File(conversionInputFolderPath), new File(
		    conversionOutputFolderPath));
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    public void performConversion() {
	try {
	    copyDirectory();
	    List<File> allFiles = FileUtil.getAllFilesInFolder(
		    new File(conversionOutputFolderPath), true);
	    for (File file : allFiles) {
		if (file.isFile()) {

		    performConversion(file.getCanonicalPath(), file.getCanonicalPath(),
			    convertToEncodingFormat, defaultInputEncoding);
		}
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }
}
