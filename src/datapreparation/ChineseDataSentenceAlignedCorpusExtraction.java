package datapreparation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import mt_pipeline.preAndPostProcessing.Lowercase;
import util.ConfigFile;

public class ChineseDataSentenceAlignedCorpusExtraction {
    private static final String ENCODING_CONERTED_SUFFIX = "-utf8-converted";

    private static final String SOURCE_LANGUAGE_STRING = "en";
    private static final String TARGET_LANGUAGE_STRING = "zh";
    private static final String CORPUS_NAME_STRING = "HongKongParallelText";
    private static final String PERFORM_ENCODING_CONVERSION = "performEncodingConversion";
    private static final String EXTRACT_SENTENCE_ALIGNED_DATA_FILES = "extractSentenceAlignedDataFiles";
    private static final String DEFAULT_INPUT_ENCODING_PROPERTY = "defaultInputEncoding";
    private static final String PERFORM_SEGMENTATION_CHINESE_SIDE_DATA = "performSegmentationChineseSideData";
    private static final String PERFORM_TOKENIZATION_ENGLISH_SIDE_DATA = "performTokenizationEnglishSideData";
    private static final String MOSES_SCRIPTS_DIR_PROPERTY = "mosesScriptsDir";
    private static final String CREATE_CLEANED_DATA = "createCleandeData";

    private static final String CREATE_LOWER_CASED_CLEANED_DATA = "createLowercasedCleanedData";

    private static final String MGIZA_SCRIPTS_DIR_PROPERTY = "mGizaScriptsDir";
    private static String HK_DATA_FOLDER_PATH_PROPERTY = "hkDataFolderPath";
    private static String WORKING_DIR_PATH = "workingDirPath";
    private static String STANFORD_SEGMENTER_ROOT_DIR = "stanfordSegmenterRootDir";
    private static String NUM_SEGMENTER_THREADS_PROPERTY = "numSegmenterThreads";
    private static String SCRIPTS_DIR_PROPERTY = "scriptsDir";
    public static final String NUM_WORDALIGNER_THREADS_PROPERTY = "numWordAlignerThreads";
    private final AlignedCorpusPreparationConfig alignedCorpusPreparationConfig;
    private final TaskSwitches taskSwitches;

    private ChineseDataSentenceAlignedCorpusExtraction(
	    AlignedCorpusPreparationConfig alignedCorpusPreparationConfig, TaskSwitches taskSwitches) {
	this.alignedCorpusPreparationConfig = alignedCorpusPreparationConfig;
	this.taskSwitches = taskSwitches;
    }

    public static ChineseDataSentenceAlignedCorpusExtraction createChineseDataSentenceAlignedCorpusExtraction(
	    String hkDataFolderPath, String workingDirPath, String stanfordSegmenterRootDir,
	    String segmentationModelSpecificationString, String defaultInputEncoding,
	    int numSegmenterThreads, String scriptsDir, String mosesScriptsDir,
	    String mGizaScriptsDir, int numWordAlignerThreads, String alignmentHeuristicString,
	    boolean performEncodingConversion, boolean extractSentenceAlignedDataFiles,
	    boolean performSegmentationChineseSideData, boolean performTokenizationEnglishSideData,
	    boolean createCleanedData, boolean createLowercasedCleanedData) {

	ConfigFile theConfig = createWorkConfig(workingDirPath, hkDataFolderPath,
		stanfordSegmenterRootDir, segmentationModelSpecificationString, numSegmenterThreads);

	TaskSwitches taskSwitches = new TaskSwitches(performEncodingConversion,
		extractSentenceAlignedDataFiles, performSegmentationChineseSideData,
		performTokenizationEnglishSideData, createCleanedData, createLowercasedCleanedData);

	SegmenterConfig segmenterConfig = SegmenterConfig
		.createMtSegmenterConfig(AlignedCorpusPreparationConfig
			.extractionConfigPath(workingDirPath));

	CleanerAndLowercaserConfig corpusCleanerConfig = CleanerAndLowercaserConfig
		.createCorpusCleanerAndLowercaserConfigDefaultSettings(scriptsDir,
			SOURCE_LANGUAGE_STRING, TARGET_LANGUAGE_STRING,
			getSourceTokenizedOutputFilePath(theConfig),
			segmenterConfig.getSegmenterOutputFilePath(), CORPUS_NAME_STRING,
			workingDirPath);

	AlignedCorpusPreparationConfig alignedCorpusPreparationConfig = AlignedCorpusPreparationConfig
		.createAlignedCorpusPreparationConfig(theConfig, hkDataFolderPath, workingDirPath,
			defaultInputEncoding, scriptsDir, segmenterConfig, corpusCleanerConfig,
			mosesScriptsDir, mGizaScriptsDir, numWordAlignerThreads,
			alignmentHeuristicString);

	return new ChineseDataSentenceAlignedCorpusExtraction(alignedCorpusPreparationConfig,
		taskSwitches);
    }

    public static ChineseDataSentenceAlignedCorpusExtraction createChineseDataSentenceAlignedCorpusExtraction(
	    String configFilePath) {
	ConfigFile configFile;
	try {
	    configFile = new ConfigFile(configFilePath);
	    String hkDataFolderPath = configFile
		    .getValueWithPresenceCheck(HK_DATA_FOLDER_PATH_PROPERTY);
	    String workingDirPath = configFile.getValueWithPresenceCheck(WORKING_DIR_PATH);

	    String stanfordSegmenterRootDir = configFile
		    .getValueWithPresenceCheck(STANFORD_SEGMENTER_ROOT_DIR);
	    String scriptsDir = configFile.getValueWithPresenceCheck(SCRIPTS_DIR_PROPERTY);

	    String segmentationModelSpecificationString = SegmenterConfig
		    .getSegmentationModelSpecificationString(configFile);

	    String defaultInputEncoding = configFile
		    .getValueWithPresenceCheck(DEFAULT_INPUT_ENCODING_PROPERTY);

	    int numSegmenterThreads = Integer.parseInt(configFile
		    .getValueWithPresenceCheck(NUM_SEGMENTER_THREADS_PROPERTY));

	    boolean performEncodingConversion = configFile
		    .getBooleanWithPresenceCheck(PERFORM_ENCODING_CONVERSION);

	    boolean extractSentenceAlignedDataFiles = configFile
		    .getBooleanWithPresenceCheck(EXTRACT_SENTENCE_ALIGNED_DATA_FILES);

	    boolean performSegmentationChineseSideData = configFile
		    .getBooleanWithPresenceCheck(PERFORM_SEGMENTATION_CHINESE_SIDE_DATA);
	    boolean performTokenizationEnglishSideData = configFile
		    .getBooleanWithPresenceCheck(PERFORM_TOKENIZATION_ENGLISH_SIDE_DATA);

	    boolean createCleanedData = configFile.getBooleanWithPresenceCheck(CREATE_CLEANED_DATA);

	    boolean createLowercasedCleanedData = configFile
		    .getBooleanWithPresenceCheck(CREATE_LOWER_CASED_CLEANED_DATA);

	    String mosesScriptsDir = configFile
		    .getValueWithPresenceCheck(MOSES_SCRIPTS_DIR_PROPERTY);

	    String mGizaScriptsDir = configFile
		    .getValueWithPresenceCheck(MGIZA_SCRIPTS_DIR_PROPERTY);

	    String valueString = configFile
		    .getValueWithPresenceCheck(NUM_WORDALIGNER_THREADS_PROPERTY);
	    int numWordAlignerThreads = Integer.parseInt(valueString);

	    return ChineseDataSentenceAlignedCorpusExtraction
		    .createChineseDataSentenceAlignedCorpusExtraction(hkDataFolderPath,
			    workingDirPath, stanfordSegmenterRootDir,
			    segmentationModelSpecificationString, defaultInputEncoding,
			    numSegmenterThreads, scriptsDir, mosesScriptsDir, mGizaScriptsDir,
			    numWordAlignerThreads,
			    WordAlignerInterface.getAlignmentHeuristicString(configFile),
			    performEncodingConversion, extractSentenceAlignedDataFiles,
			    performSegmentationChineseSideData, performTokenizationEnglishSideData,
			    createCleanedData, createLowercasedCleanedData);
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}

    }

    private static String hkDataFolderName(String hkDataFolderPath) {
	return new File(hkDataFolderPath).getName();
    }

    private static String convertedDataFolderPath(String workingDirPath, String hkDataFolderPath) {
	return workingDirPath + hkDataFolderName(hkDataFolderPath) + ENCODING_CONERTED_SUFFIX;
    }

    private String convertedDataFolderPath() {
	return alignedCorpusPreparationConfig.getWorkingDirPath()
		+ hkDataFolderName(alignedCorpusPreparationConfig.getHkDataFolderPath())
		+ ENCODING_CONERTED_SUFFIX;
    }

    private void performDataConversion() {
	System.out.println("perform data encoding conversion...");
	FileEncodingConverter fileEncodingConverter = FileEncodingConverter
		.createFileEncodingConverter(alignedCorpusPreparationConfig.getHkDataFolderPath(),
			convertedDataFolderPath(), "utf8",
			alignedCorpusPreparationConfig.getDefaultInputEncoding());
	fileEncodingConverter.performConversion();
	System.out.println("done");
    }

    private static ConfigFile createWorkConfig(String workingDirPath, String hkDataFolderPath,
	    String stanfordSegmenterRootDir, String segmentationModelSpecificationString,
	    int numSegmenterThreads) {
	ChineseDataPreparationConfigCreater chineseDataPreparationConfigCreater = ChineseDataPreparationConfigCreater
		.createFullChineseDataConfigCreater(convertedDataFolderPath(workingDirPath,
			hkDataFolderPath));
	chineseDataPreparationConfigCreater.writeFullCorpusExtractionConfig(
		AlignedCorpusPreparationConfig.extractionConfigPath(workingDirPath),
		AlignedCorpusPreparationConfig.fullCorpusSourceOutputFilePath(workingDirPath),
		AlignedCorpusPreparationConfig.fullCorpusTargetOutputFilePath(workingDirPath),
		stanfordSegmenterRootDir,segmentationModelSpecificationString, numSegmenterThreads);
	try {
	    return new ConfigFile(
		    AlignedCorpusPreparationConfig.extractionConfigPath(workingDirPath));
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private void extractSentenceAlignedDataFiles() {
	SentenceAlignedFilesCreaterFullCorpus sentenceAlignedFilesCreaterFullCorpus = SentenceAlignedFilesCreaterFullCorpus
		.createSentenceAlignedFilesCreateFullCorpus(alignedCorpusPreparationConfig
			.getConfig());
	sentenceAlignedFilesCreaterFullCorpus.extractSentenceAlignedSourceAndTargetFullCorpus();
    }

    private void performSegmentationChineseSideData() {
	StanfordSegmenterInterface stanfordSegmenterInterface = StanfordSegmenterInterface
		.createStanfordSegmenterInterface(alignedCorpusPreparationConfig.segmenterConfig);
	stanfordSegmenterInterface.createResultFilesMutltiThreaded();
    }

    private String getSourceOutputFilePath(ConfigFile configFile) {
	return SentenceAlignedFilesCreaterFullCorpus.getSourceOutputPathFromConfigFile(configFile);
    }

    private static String getSourceTokenizedOutputFilePath(ConfigFile configFile) {
	String outputFilePath = SentenceAlignedFilesCreaterFullCorpus
		.getSourceOutputPathFromConfigFile(configFile);
	String result = SegmenterConfig
		.addSuffixBeforeLastSuffixIfAny(outputFilePath, ".tokenized");
	return result;
    }

    private void performTokenizationEnglishSideData() {

	TokenizerInterface tokenizerInterface = TokenizerInterface
		.createTokenizerInterface(alignedCorpusPreparationConfig.getScriptsDir());
	tokenizerInterface.createTokenizedFile(
		getSourceOutputFilePath(alignedCorpusPreparationConfig.getConfig()),
		getSourceTokenizedOutputFilePath(alignedCorpusPreparationConfig.getConfig()),
		SOURCE_LANGUAGE_STRING);
    }

    private void createCleanedData() {
	CorpusCleanerInterface corpusCleanerInterface = CorpusCleanerInterface
		.createCleanerInterfaceDefaultSettings(
			alignedCorpusPreparationConfig.getScriptsDir(),
			SOURCE_LANGUAGE_STRING,
			TARGET_LANGUAGE_STRING,
			getSourceTokenizedOutputFilePath(alignedCorpusPreparationConfig.getConfig()),
			alignedCorpusPreparationConfig.segmenterConfig.getSegmenterOutputFilePath(),
			CORPUS_NAME_STRING, alignedCorpusPreparationConfig.getWorkingDirPath());
	corpusCleanerInterface.createCleanedCorpusFiles();
    }

    private void createLowercasedCleanedData() {
	try {
	    Lowercase lowercaseSource = Lowercase.createLowercase(
		    alignedCorpusPreparationConfig.corpusCleanerAndLowercaserConfig
			    .getCleanedSourceFilePath(),
		    alignedCorpusPreparationConfig.corpusCleanerAndLowercaserConfig
			    .getCleanedAndLowercasedSourceFilePath());
	    lowercaseSource.performConversion();
	    Lowercase lowercaseTarget = Lowercase.createLowercase(
		    alignedCorpusPreparationConfig.corpusCleanerAndLowercaserConfig
			    .getCleanedTargetFilePath(),
		    alignedCorpusPreparationConfig.corpusCleanerAndLowercaserConfig
			    .getCleanedAndLowercasedTargetFilePath());
	    lowercaseTarget.performConversion();

	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private void computeWordAlignments() {
	WordAlignerInterface alignerInterface = WordAlignerInterface
		.createWordAlignerInterface(alignedCorpusPreparationConfig);
	alignerInterface.performWordAlignment();
    }

    public void createPerparedCorpus() {
	if (taskSwitches.performEncodingConversion) {
	    performDataConversion();
	}
	if (taskSwitches.extractSentenceAlignedDataFiles) {
	    extractSentenceAlignedDataFiles();
	}
	if (taskSwitches.performSegmentationChineseSideData) {
	    performSegmentationChineseSideData();
	}
	if (taskSwitches.performTokenizationEnglishSideData) {
	    performTokenizationEnglishSideData();
	}
	if (taskSwitches.createCleanedData) {
	    createCleanedData();
	}
	if (taskSwitches.createLowewercasedCleanedData) {
	    createLowercasedCleanedData();
	}
	computeWordAlignments();
    }

    private static class TaskSwitches {
	private final boolean performEncodingConversion;
	private final boolean extractSentenceAlignedDataFiles;
	private final boolean performSegmentationChineseSideData;
	private final boolean performTokenizationEnglishSideData;
	private final boolean createCleanedData;
	private final boolean createLowewercasedCleanedData;

	private TaskSwitches(boolean performEncodingConversion,
		boolean extractSentenceAlignedDataFiles,
		boolean performSegmentationChineseSideData,
		boolean performTokenizationEnglishSideData, boolean createCleanedData,
		boolean createLowewercasedCleanedData) {
	    this.performEncodingConversion = performEncodingConversion;
	    this.extractSentenceAlignedDataFiles = extractSentenceAlignedDataFiles;
	    this.performSegmentationChineseSideData = performSegmentationChineseSideData;
	    this.performTokenizationEnglishSideData = performTokenizationEnglishSideData;
	    this.createCleanedData = createCleanedData;
	    this.createLowewercasedCleanedData = createCleanedData;
	}
    }

    public static void main(String[] args) {
	if (args.length != 1) {
	    System.out.println("Error - incorrect number of arguments.");
	    System.out.println("usage :\ncreateChineseData CONFIG_FILE_PATH");
	    System.exit(1);
	}
	String configFilePath = args[0];
	ChineseDataSentenceAlignedCorpusExtraction chineseDataSentenceAlignedCorpusExtraction = ChineseDataSentenceAlignedCorpusExtraction
		.createChineseDataSentenceAlignedCorpusExtraction(configFilePath);
	chineseDataSentenceAlignedCorpusExtraction.createPerparedCorpus();
    }

}
