package datapreparation;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

import util.FileUtil;

public class PlainTextFromXMLTextDocsExtracter {
    private final String docFolderPath;
    private final DateOrderedFileNameListCreater dateOrderedFileNameListCreater;
    private final BufferedWriter outputWriter;
    private final String fileEncodingString;

    private PlainTextFromXMLTextDocsExtracter(String docFolderPath,
	    DateOrderedFileNameListCreater dateOrderedFileNameListCreater,
	    BufferedWriter outputWriter, String fileEncodingString) {
	this.docFolderPath = docFolderPath;
	this.dateOrderedFileNameListCreater = dateOrderedFileNameListCreater;
	this.outputWriter = outputWriter;
	this.fileEncodingString = fileEncodingString;
    }

    public static PlainTextFromXMLTextDocsExtracter createPlainTextFromXMLTextDocsExtracter(
	    String docFolderPath, String outputFilePath, String fileEncodingString) {
	DateOrderedFileNameListCreater dateOrderedFileNameListCreater = DateOrderedFileNameListCreater
		.createDateOrderedFileNameListCreater(docFolderPath);
	BufferedWriter outputWriter;
	try {
	    outputWriter = FileUtil.createBufferedWriterWithSpecificEncoding(outputFilePath,
		    FileEncodingConverter.UTF8_ENCODING);
	    return new PlainTextFromXMLTextDocsExtracter(docFolderPath,
		    dateOrderedFileNameListCreater, outputWriter, fileEncodingString);
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    public void writePlainTextOutputFile() {
	List<String> fileNamesSorted = dateOrderedFileNameListCreater.getFileNamesSortedByDate();
	for (int i = 0; i < fileNamesSorted.size(); i++) {
	    String fileName = fileNamesSorted.get(i);
	    // System.out.println("fileName: " + fileName);
	    String filePath = docFolderPath + fileName;
	    // System.out.println("filePath: " + filePath);
	    PlainTextFromXMLTextDocExtracter.createPlainTextFromXMLTextDocExtracter(filePath,
		    outputWriter, fileEncodingString).writePlainTextToOutputFile(false);
	    if (i < fileNamesSorted.size() - 1) {
		try {
		    outputWriter.write("\n");
		} catch (IOException e) {
		    e.printStackTrace();
		    throw new RuntimeException(e);
		}
	    }
	}
	FileUtil.closeCloseableIfNotNull(outputWriter);
    }
}
