package datapreparation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import util.FileUtil;

public class DuplicatesChecker {
    private final List<String> linesList;

    private DuplicatesChecker(List<String> linesList) {
	this.linesList = linesList;
    }

    public static DuplicatesChecker createDuplicatesChecker(String filePath) {
	List<String> linesList = FileUtil.getLinesInFile(filePath, false);

	return new DuplicatesChecker(linesList);
    }

    public Map<String, Integer> computeStringFrequencyMap() {
	Map<String, Integer> result = new HashMap<String, Integer>();
	for (String line : linesList) {
	    if (result.containsKey(line)) {
		Integer newValue = result.get(line) + 1;
		result.put(line, newValue);
	    } else {
		result.put(line, 1);
	    }
	}
	return result;
    }

    public Map<String, Integer> computeDuplicatesFrequencyMap() {
	Map<String, Integer> result = new HashMap<String, Integer>();
	for (Entry<String, Integer> entry : computeStringFrequencyMap().entrySet()) {
	    if (entry.getValue() > 1) {
		result.put(entry.getKey(), entry.getValue());
	    }
	}
	return result;
    }

    public void showDuplicates() {
	Map<String, Integer> duplicatesFrequencyMap = computeDuplicatesFrequencyMap();
	if (duplicatesFrequencyMap.isEmpty()) {
	    System.out.println("No duplicates found");
	} else {
	    int numDuplicates = duplicatesFrequencyMap.size();
	    System.out.println(numDuplicates + " strings with duplicates were found:");
	    for (Entry<String, Integer> entry : duplicatesFrequencyMap.entrySet()) {
		System.out.println("\"" + entry.getKey() + "\"");
		System.out.println("number of occurences: " + entry.getValue());
	    }
	}
    }
}