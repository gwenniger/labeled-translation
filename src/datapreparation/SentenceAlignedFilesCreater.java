package datapreparation;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

import junit.framework.Assert;
import datapreparation.SegmentIndicesPairingExtracter.DocSegmentPairing;
import datapreparation.SegmentIndicesPairingExtracter.SegmentPairing;
import datapreparation.SegmentListExtracter.DocSegments;

/**
 * This class works with source target and alignment files. These files
 * correspond to a part of the bigger aligned corpus and are themselves further
 * subdivided into documents.
 * 
 * The class is responsible for writing sentence aligned source and target files
 * based on List<DocSegments> sourceDocSegmentsList : A list of DocSegments,
 * each containing the segments for one document containing in the source
 * List<DocSegments> targetDocSegmentsList : A list of DocSegments, each
 * containing the segments for one document containing in the target
 * List<DocSegmentPairing> segmentDocAlignments : A list of DocSegmentPairing
 * elements, each containing a list of sentence alignments for one document
 * within the alignment file
 * 
 * @author Gideon Maillette de Buy Wenniger
 * 
 */
public class SentenceAlignedFilesCreater {

    private final List<DocSegments> sourceDocSegmentsList;
    private final List<DocSegments> targetDocSegmentsList;
    private final List<DocSegmentPairing> segmentDocAlignments;

    private final BufferedWriter sourceOutputWriter;
    private final BufferedWriter targetOutputWriter;
    private final boolean writeNewlineForFirstLine;

    private int numLinesWritten;

    private SentenceAlignedFilesCreater(List<DocSegments> sourceDocSegmentsList,
	    List<DocSegments> targetDocSegmentsList, List<DocSegmentPairing> segmentDocAlignments,
	    BufferedWriter sourceOutputWriter, BufferedWriter targetOutputWriter,
	    boolean writeNewlineForFirstLine) {
	this.sourceDocSegmentsList = sourceDocSegmentsList;
	this.targetDocSegmentsList = targetDocSegmentsList;
	this.segmentDocAlignments = segmentDocAlignments;
	this.sourceOutputWriter = sourceOutputWriter;
	this.targetOutputWriter = targetOutputWriter;
	this.writeNewlineForFirstLine = writeNewlineForFirstLine;

    }

    public static SentenceAlignedFilesCreater createSentenceAlignedFilesCreater(
	    String sourceSegmentFilePath, String targetSegmentFilePath,
	    String segmentAlignmentFilePath, BufferedWriter sourceOutputWriter,
	    BufferedWriter targetOutputWriter, boolean writeNewlineForFirstLine) {

	List<DocSegments> sourceDocSegmentsList = SegmentListExtracter.createSegmentListExtracter(
		sourceSegmentFilePath).extractDocSegmentList();
	List<DocSegments> targetDocSegmentsList = SegmentListExtracter.createSegmentListExtracter(
		targetSegmentFilePath).extractDocSegmentList();
	List<DocSegmentPairing> segmentAlignment = SegmentIndicesPairingExtracter
		.createSegmentIndicesPairingExtracter(segmentAlignmentFilePath)
		.extractSegmentIndicesPairingList();

	return new SentenceAlignedFilesCreater(sourceDocSegmentsList, targetDocSegmentsList,
		segmentAlignment, sourceOutputWriter, targetOutputWriter, writeNewlineForFirstLine);
    }

    /**
     * This method writes the output of one reconstructed aligned sentence to
     * the output (source or target) file. This is done by gluing together all
     * the right segments from segmentList based on the indices found in
     * segmentIndices
     * 
     * @param outputWriter
     * @param segmentIndices
     * @param segmentList
     * @param writeNewline
     */
    private static String reconstructedAlignedSentence(List<Integer> segmentIndices,
	    DocSegments segmentList) {

	// System.out.println("SegmentIndices: " + segmentIndices);

	// System.out.println("docid: " + segmentList.getDocID());
	// System.out.println("segment indices: " + segmentIndices);
	// System.out.println("segmentList: " + segmentList);

	String outputString = "";

	if (!segmentIndices.isEmpty()) {
	    outputString = segmentList.getSegment(segmentIndices.get(0));
	}
	for (int i = 1; i < segmentIndices.size(); i++) {
	    int segmentIndex = segmentIndices.get(i);
	    outputString += " " + segmentList.getSegment(segmentIndex);
	}
	return outputString;

    }

    private String reconstructSourceOutputSentence(List<Integer> segmentIndices,
	    DocSegments sourceDocSegments) {
	// System.out.println("Write source output to file");
	return reconstructedAlignedSentence(segmentIndices, sourceDocSegments);
    }

    private String reconstructTargetOutputSentence(List<Integer> segmentIndices,
	    DocSegments targetDocSegments) {
	// System.out.println("Write target output to file");
	return reconstructedAlignedSentence(segmentIndices, targetDocSegments);
    }

    private boolean bothStringsContainNonWhitespaceCharacters(String sourceOutputSentence, String targetOutputSentence) {
	return (WhitespaceRemover.removeSpacesThorough(sourceOutputSentence).length() > 0)
		&& (WhitespaceRemover.removeSpacesThorough(targetOutputSentence).length() > 0);
    }

    private String createLineToWrite(String line, boolean writeNewline) {
	String result = line;
	if (writeNewline) {
	    result = "\n" + result;
	}
	return result;
    }

    private void writeSourceAndTargetLineIfNeitherEmptyNorContainingOnlyWhiteSpace(
	    String sourceOutputSentence, String targetOutputSentence, boolean writeNewLine) {

	try {
	    if (bothStringsContainNonWhitespaceCharacters(sourceOutputSentence, targetOutputSentence)) {
		sourceOutputWriter.write(createLineToWrite(sourceOutputSentence, writeNewLine));
		targetOutputWriter.write(createLineToWrite(targetOutputSentence, writeNewLine));
		numLinesWritten++;
	    }
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

    }

    private void writeLinesForFirsSegmentPairingIfExisting(DocSegmentPairing docSegmentPairing,
	    boolean writeNewLine, DocSegments sourceDocSegments, DocSegments targetDocSegments) {
	if (docSegmentPairing.getSegmentIndicesPairingList().size() > 0) {
	    SegmentPairing firstSegmentPairing = docSegmentPairing.getSegmentIndicesPairingList()
		    .get(0);

	    // System.out.println("firstSegmentPairing: " +
	    // firstSegmentPairing);

	    String sourceOutputSentence = reconstructSourceOutputSentence(
		    firstSegmentPairing.getSourceSegmentIndices(), sourceDocSegments);
	    String targetOutputSentence = reconstructTargetOutputSentence(
		    firstSegmentPairing.getTargetSegmentIndices(), targetDocSegments);

	    writeSourceAndTargetLineIfNeitherEmptyNorContainingOnlyWhiteSpace(sourceOutputSentence,
		    targetOutputSentence, writeNewLine);

	}
    }

    private void writeLinesForSegmentPairingsBeyondFirstIfExisting(
	    DocSegmentPairing docSegmentPairing, DocSegments sourceDocSegments,
	    DocSegments targetDocSegments) {
	for (int segmentIndex = 1; segmentIndex < docSegmentPairing.getSegmentIndicesPairingList()
		.size(); segmentIndex++) {
	    SegmentPairing segmentPairing = docSegmentPairing.getSegmentIndicesPairingList().get(
		    segmentIndex);

	    String sourceOutputSentence = reconstructSourceOutputSentence(
		    segmentPairing.getSourceSegmentIndices(), sourceDocSegments);
	    String targetOutputSentence = reconstructTargetOutputSentence(
		    segmentPairing.getTargetSegmentIndices(), targetDocSegments);

	    writeSourceAndTargetLineIfNeitherEmptyNorContainingOnlyWhiteSpace(sourceOutputSentence,
		    targetOutputSentence, true);
	}
    }

    /**
     * Boolean that determines whether we should write a newline befor the
     * document
     * 
     * @param docNum
     * @return
     */
    private boolean writeNewLine(int docNum) {
	// For every doc beyond the first we have to anyway write the
	// newline
	return writeNewlineForFirstLine || (docNum > 0);
    }

    /**
     * This method tests that the doc ids are the same in all three sources.
     * This is a safety check/test to see make sure that we are not combining
     * the wrong things somehow
     * 
     * @param docSegmentPairing
     * @param sourceDocSegments
     * @param targetDocSegments
     */
    private void checkDocIDsAreSameInAllThreeSources(DocSegmentPairing docSegmentPairing,
	    DocSegments sourceDocSegments, DocSegments targetDocSegments) {
	// Sanity check that the doc ids are the same in all three sources
	Assert.assertEquals(docSegmentPairing.getDocID(), sourceDocSegments.getDocID());
	Assert.assertEquals(docSegmentPairing.getDocID(), targetDocSegments.getDocID());
    }

    /**
     * This is the main method for the class, writing the reconstructed sentence
     * aligned source and target to their corresponding output writers. The
     * reason to work with output writers as opposed to files is that we want to
     * possibly run this class multiple times for different triples of files
     * with the same source- and target output writer, in order to generate the
     * sentence aligned version of the segment files for an entire (sub)corpus
     * at once.
     * 
     * @return
     */
    public int writeSourceAndTargetOutputToWriters() {

	numLinesWritten = 0;
	for (int docNum = 0; docNum < this.segmentDocAlignments.size(); docNum++) {
	    DocSegmentPairing docSegmentPairing = this.segmentDocAlignments.get(docNum);
	    // System.out.println("writeSourceAndTargetOutputToWriters() - num segment pairs doc: "
	    // + docSegmentPairing.getSegmentIndicesPairingList().size());
	    // System.out.println("docsegmentpairing: " + docSegmentPairing);
	    DocSegments sourceDocSegments = sourceDocSegmentsList.get(docNum);
	    DocSegments targetDocSegments = targetDocSegmentsList.get(docNum);

	    checkDocIDsAreSameInAllThreeSources(docSegmentPairing, sourceDocSegments,
		    targetDocSegments);

	    writeLinesForFirsSegmentPairingIfExisting(docSegmentPairing, writeNewLine(docNum),
		    sourceDocSegments, targetDocSegments);

	    writeLinesForSegmentPairingsBeyondFirstIfExisting(docSegmentPairing, sourceDocSegments,
		    targetDocSegments);
	}

	Assert.assertTrue(numLinesWritten > 0);
	return numLinesWritten;
    }

}
