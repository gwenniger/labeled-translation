package datapreparation;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;
import util.ConfigFile;
import util.FileStatistics;
import util.FileUtil;

/**
 * This class does data pre-processing for the Multi Translation Chinese Corpera
 * of the Linguistic Data Consortium (LDC)
 * 
 * Remark: Please not that the way text files are displayed depends on the
 * extension!!! A ".txt" file will be incorrectly rendered by kate, kile etc,
 * even if UTF8 is selected for the encoding. This is extremely confusing, but a
 * good reason to use ".en" and ".zh" as extensions to the result files rather
 * than ".txt"
 * 
 * @author Gideon Maillette de Buy Wenniger
 * 
 */
public class ChineseDevAndTestDataPreparerMultiTranslationChinese {

    private static final String SOURCE_FOLDER_NAME = "source";
    private static final String TRANSLATION_FOLDER_NAME = "translation";
    private static String WORKING_DIR_PROPERTY = "workingDir";
    private static String MULTI_TRANSLATION_CHINESE_PART1_FOLDER_PROPERTY = "multiTranslationChinesPart1Folder";
    private static String MULTI_TRANSLATION_CHINESE_PART2_FOLDER_PROPERTY = "multiTranslationChinesPart2Folder";
    private static String MULTI_TRANSLATION_CHINESE_PART3_FOLDER_PROPERTY = "multiTranslationChinesPart3Folder";
    private static String MULTI_TRANSLATION_CHINESE_PART4_FOLDER_PROPERTY = "multiTranslationChinesPart4Folder";
    private static String SELECTED_TRANSLATIONS_LIST_PROPERTY = "selectedTranslationsList";
    private static String DEV_MTA_PART_INDICES_LIST_PROPERTY = "partIndicesForDevList";
    private static String TEST_MTA_PART_INDICES_LIST_PROPERTY = "partIndicesForTestList";

    private final String workingDirPath;
    private final String mtaPart1FolderPath;
    private final String mtaPart2FolderPath;
    private final String mtaPart3FolderPath;
    private final String mtaPart4FolderPath;
    private final List<String> selectedTranslationsList;
    private final List<Integer> devPartIndices;
    private final List<Integer> testPartIndices;

    private ChineseDevAndTestDataPreparerMultiTranslationChinese(String workingDirPath,
	    String mtaPart1FolderPath, String mtaPart2FolderPath, String mtaPart3FolderPath,
	    String mtaPart4FolderPath, List<String> selectedTranslationsList,
	    List<Integer> devPartIndices, List<Integer> testPartIndices) {
	this.workingDirPath = workingDirPath;
	this.mtaPart1FolderPath = mtaPart1FolderPath;
	this.mtaPart2FolderPath = mtaPart2FolderPath;
	this.mtaPart3FolderPath = mtaPart3FolderPath;
	this.mtaPart4FolderPath = mtaPart4FolderPath;
	this.selectedTranslationsList = selectedTranslationsList;
	this.devPartIndices = devPartIndices;
	this.testPartIndices = testPartIndices;
    }

    private static String getWorkingDirPath(ConfigFile theConfig) {
	return theConfig.getValueWithPresenceCheck(WORKING_DIR_PROPERTY);
    }

    private static String getMTAPart1FolderPath(ConfigFile theConfig) {
	return theConfig.getValueWithPresenceCheck(MULTI_TRANSLATION_CHINESE_PART1_FOLDER_PROPERTY);
    }

    private static String getMTAPart2FolderPath(ConfigFile theConfig) {
	return theConfig.getValueWithPresenceCheck(MULTI_TRANSLATION_CHINESE_PART2_FOLDER_PROPERTY);
    }

    private static String getMTAPart3FolderPath(ConfigFile theConfig) {
	return theConfig.getValueWithPresenceCheck(MULTI_TRANSLATION_CHINESE_PART3_FOLDER_PROPERTY);
    }

    private static String getMTAPart4FolderPath(ConfigFile theConfig) {
	return theConfig.getValueWithPresenceCheck(MULTI_TRANSLATION_CHINESE_PART4_FOLDER_PROPERTY);
    }

    private static List<String> getSelectedTranslationList(ConfigFile theConfig) {
	if (!theConfig.hasValue(SELECTED_TRANSLATIONS_LIST_PROPERTY)) {
	    throw new RuntimeException(
		    "Error: "
			    + SELECTED_TRANSLATIONS_LIST_PROPERTY
			    + "\n - A comma separated list of the chosen (best) translation for each of the mta-part corpera 1-4"
			    + "\n Must be specified in the configuration file");
	}
	return theConfig
		.getMultiValueParmeterAsListWithPresenceCheck(SELECTED_TRANSLATIONS_LIST_PROPERTY);
    }

    private static List<Integer> getIntegerListFromIntegerStringList(List<String> integerStringList) {
	List<Integer> result = new ArrayList<Integer>();
	for (String intString : integerStringList) {
	    result.add(Integer.parseInt(intString));
	}
	return result;
    }

    private static List<Integer> getDevPartIndices(ConfigFile theConfig) {
	List<String> devPartIndicesStrings = theConfig
		.getMultiValueParmeterAsListWithPresenceCheck(DEV_MTA_PART_INDICES_LIST_PROPERTY);
	return getIntegerListFromIntegerStringList(devPartIndicesStrings);
    }

    private static List<Integer> getTestPartIndices(ConfigFile theConfig) {
	List<String> devPartIndicesStrings = theConfig
		.getMultiValueParmeterAsListWithPresenceCheck(TEST_MTA_PART_INDICES_LIST_PROPERTY);
	return getIntegerListFromIntegerStringList(devPartIndicesStrings);
    }

    public static ChineseDevAndTestDataPreparerMultiTranslationChinese createChineseDevAndTestDataPreparer(
	    String configFilePath) {
	try {
	    ConfigFile theConfig = new ConfigFile(configFilePath);
	    return new ChineseDevAndTestDataPreparerMultiTranslationChinese(
		    getWorkingDirPath(theConfig), getMTAPart1FolderPath(theConfig),
		    getMTAPart2FolderPath(theConfig), getMTAPart3FolderPath(theConfig),
		    getMTAPart4FolderPath(theConfig), getSelectedTranslationList(theConfig),
		    getDevPartIndices(theConfig), getTestPartIndices(theConfig));

	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private String getSourceDocsFolderPath(String corpusFolderPath) {
	return corpusFolderPath + SOURCE_FOLDER_NAME + "/";
    }

    private String getTranslationDocsFoldersFolderPath(String corpusFolderPath) {
	return corpusFolderPath + TRANSLATION_FOLDER_NAME + "/";
    }

    private String getOutputFolderPath(int partIndex) {
	return workingDirPath + "mta_part_" + partIndex + "/";
    }

    private String getOutputSourcePath(int partIndex) {
	return getOutputFolderPath(partIndex) + "source.txt";
    }

    private String getOutputBestReferencePath(int partIndex) {
	return getOutputFolderPath(partIndex) + "selected-reference.ref.txt";
    }

    private String getMergedFilePrefix(String category) {
	return workingDirPath + category;
    }

    private String getMergedSourcePath(String category) {
	return getMergedFilePrefix(category) + ".zh";
    }

    /**
     * Create the ref merged output file path.
     * 
     * @param category
     * @return
     */
    private String getMergedRefPath(String category) {
	return getMergedFilePrefix(category) + ".en";
    }

    private void createMergedSourceFile(String category, List<Integer> partIndices) {
	String resultFileName = getMergedSourcePath(category);
	List<String> appendFilePathList = new ArrayList<String>();
	for (Integer partIndex : partIndices) {
	    appendFilePathList.add(getOutputSourcePath(partIndex));
	}
	FileUtil.createAppendFilesFile(resultFileName, appendFilePathList,
		FileEncodingConverter.UTF8_ENCODING);
    }

    private void createMergedReferenceFile(String category, List<Integer> partIndices) {
	String resultFileName = getMergedRefPath(category);
	List<String> appendFilePathList = new ArrayList<String>();
	for (Integer partIndex : partIndices) {
	    appendFilePathList.add(getOutputBestReferencePath(partIndex));
	}
	FileUtil.createAppendFilesFile(resultFileName, appendFilePathList,
		FileEncodingConverter.UTF8_ENCODING);
    }

    private void testResultFilesHaveEqualLengths(String category) {
	try {
	    Assert.assertEquals(FileStatistics.countLines(getMergedSourcePath(category)),
		    FileStatistics.countLines(getMergedRefPath(category)));
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private void createMergedDevFiles() {
	createMergedSourceFile("dev", this.devPartIndices);
	createMergedReferenceFile("dev", this.devPartIndices);
	testResultFilesHaveEqualLengths("dev");
    }

    private void createMergedTestFiles() {
	createMergedSourceFile("test", this.testPartIndices);
	createMergedReferenceFile("test", this.testPartIndices);
	testResultFilesHaveEqualLengths("test");
    }

    private String getBestReferenceFile(
	    MultipleReferencesMultipleDocsSingleTextFilesCreater multipleDocsSingleTextFilesCreater,
	    String corpusFolderPath, int partIndex) {
	String selectedReferenceFolderName = this.selectedTranslationsList.get(partIndex - 1);
	return multipleDocsSingleTextFilesCreater
		.referenceOutputFolderPath(selectedReferenceFolderName);
    }

    private void copyBestReferenceFile(
	    MultipleReferencesMultipleDocsSingleTextFilesCreater multipleDocsSingleTextFilesCreater,
	    String corpusFolderPath, int partIndex) {
	FileUtil.copyFileNative(
		getBestReferenceFile(multipleDocsSingleTextFilesCreater, corpusFolderPath,
			partIndex), getOutputBestReferencePath(partIndex), true);

    }

    private void createTextFilesForCorpus(String corpusFolderPath, int partIndex) {
	FileUtil.createFolderIfNotExisting(getOutputFolderPath(partIndex));
	MultipleReferencesMultipleDocsSingleTextFilesCreater multipleDocsSingleTextFilesCreater = MultipleReferencesMultipleDocsSingleTextFilesCreater
		.createMultipleReferencesMultipleDocsSingleTextFilesCreater(
			getSourceDocsFolderPath(corpusFolderPath),
			getTranslationDocsFoldersFolderPath(corpusFolderPath),
			getOutputFolderPath(partIndex), FileEncodingConverter.GB2312_ENCODING,
			FileEncodingConverter.ISO_88591_ENCODING);
	multipleDocsSingleTextFilesCreater.writePlainTextOutputFiles();
	// multipleDocsSingleTextFilesCreater.convertFilesToUTF8();
	copyBestReferenceFile(multipleDocsSingleTextFilesCreater, corpusFolderPath, partIndex);
    }

    private void createTextFilesForCorpera() {
	FileUtil.createFolderIfNotExisting(workingDirPath);
	createTextFilesForCorpus(mtaPart1FolderPath, 1);
	createTextFilesForCorpus(mtaPart2FolderPath, 2);
	createTextFilesForCorpus(mtaPart3FolderPath, 3);
	createTextFilesForCorpus(mtaPart4FolderPath, 4);
	createMergedDevFiles();
	createMergedTestFiles();
    }

    public static void main(String[] args) {
	if (args.length != 1) {
	    System.out.println("Usage: createChinesDevAndTestFiles CONFIG_FILE_PATH");
	    System.exit(1);
	}
	ChineseDevAndTestDataPreparerMultiTranslationChinese chineseDevAndTestDataPreparer = ChineseDevAndTestDataPreparerMultiTranslationChinese
		.createChineseDevAndTestDataPreparer(args[0]);
	chineseDevAndTestDataPreparer.createTextFilesForCorpera();
    }
}
