package datapreparation.wmt;

public class EmptyLinesRemover {

    public static void main(String[] args) {
	if (args.length != 2) {
	    System.out.println("Usage: removeEmptyLines INPUT_FILE_PATH RESULT_FILE_PATH");
	    System.exit(1);
	}
	String inputFilePath = args[0];
	String resultFilePath = args[1];
	System.out.println("Removing empty lines in " + inputFilePath + " and writing result to "
		+ resultFilePath);
	util.EmptyLineRemover.removeEmptyLines(inputFilePath, resultFilePath);
	System.out.println("done");
    }
}
