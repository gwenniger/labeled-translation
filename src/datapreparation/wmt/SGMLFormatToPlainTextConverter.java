package datapreparation.wmt;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * A class that converts an SGML-format translation file (source/target)
 * into a plain text file, that contains only the source/target 
 * lines, without all the segment and doc XML tags
 * 
 * @author gemaille
 *
 */

public class SGMLFormatToPlainTextConverter {
    private static final String CONTENT_CONTAINING_LINES_PREFIX = "<seg id=";
    private static final String CONTENT_STRING_END_TAG = "</seg>";
    private static final String START_TAG_PATTERN_STRING = "<seg id=\"[0-9]*\">";
    // private static final String START_TAG_PATTERN_STRING = "<seg id.*>";
    private static final Pattern pattern = Pattern.compile(START_TAG_PATTERN_STRING);

    private static int firstIndexOfEndTag(String line) {
	return line.indexOf(CONTENT_STRING_END_TAG);
    }

    /**
     * A more safe way to find the last position of the start tag, using a
     * regular expression
     * 
     * See:
     * http://stackoverflow.com/questions/4194310/can-java-string-indexof-handle
     * -a-regular-expression-as-a-parameter
     * 
     * @param line
     * @return
     */
    private static int lastIndexOfStartTagMoreSafe(String line) {
	Matcher matcher = pattern.matcher(line);
	if (matcher.find()) {

	    // this will get the index of the last matched element
	    return matcher.end() - 1;
	}
	return -1;

    }

    /**
     * This simply exploits the fact that the start tag ends with ">";
     * 
     * @param line
     * @return
     */
    private static int lastIndexOfStartTag(String line) {
	return line.indexOf(">");
    }

    private static final java.util.function.UnaryOperator<String> CONTEXT_EXTRACTING_FUNCTION = line -> line
	    .substring(lastIndexOfStartTagMoreSafe(line) + 1, firstIndexOfEndTag(line));

    private final String inputFilePath;
    private final String resultFilePath;

    private SGMLFormatToPlainTextConverter(String inputFilePath, String resultFilePath) {
	this.inputFilePath = inputFilePath;
	this.resultFilePath = resultFilePath;
    }

    public static SGMLFormatToPlainTextConverter createSGMLFormatToPlainTextConverter(
	    String inputFilePath, String resultFilePath) {
	return new SGMLFormatToPlainTextConverter(inputFilePath, resultFilePath);
    }

    private Stream<String> getPlainTextStream() {
	Stream<String> result = null;
	try {
	    result = Files.lines(Paths.get(inputFilePath));
	    Stream<String> contentLines = result
		    .filter(line -> line.startsWith(CONTENT_CONTAINING_LINES_PREFIX));
	    Stream<String> contentLinesWithoutTags = contentLines.map(CONTEXT_EXTRACTING_FUNCTION);
	    return contentLinesWithoutTags;
	} catch (IOException e) {
	    e.printStackTrace();
	}
	return result;
    }

    public void convertFile() {
	util.StreamUtil.writeStreamToFileWhileAddingNewlineCharacters(this.getPlainTextStream(),
		resultFilePath);
    }

    public static void main(String[] args) {
	if (args.length != 2) {
	    System.out.println(
		    "Usage: sgmlFormatToPlainTextConverter INPUT_FILE_PATH RESULT_FILE_PATH");
	    System.exit(1);
	}
	String inputFilePath = args[0];
	String resultFilePath = args[1];
	System.out.println("Converting SGML format lines in " + inputFilePath
		+ " and writing result to " + resultFilePath);
	SGMLFormatToPlainTextConverter sgmlFormatToPlainTextConverter = createSGMLFormatToPlainTextConverter(
		inputFilePath, resultFilePath);
	sgmlFormatToPlainTextConverter.convertFile();
	System.out.println("done");
    }

}
