package datapreparation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import util.FileUtil;

/**
 * This class will process files from the (English) GigaWord corpus that have a
 * sequence of tags to mark the beginning and end of content lines. Normal
 * sentences may be spread out over multiple sentences, their beginning is
 * marked by
 * <P>
 * and their end by
 * </P>
 * Apart from normal sentences there are headlines and date lines. Including
 * them is optional, but recommended, since they often correspond to names and
 * dates that are also frequently occurring in some of the the NIST test sets.
 * 
 * @author gemaille
 * 
 */
public class PlainTextFromGigaWordDocExtracter {

    private static String HEAD_LINE_TAG = "HEADLINE";
    private static String DATA_LINE_TAG = "DATELINE";
    private static String P_TAG = "P";
    private final String inputFilePath;
    private final BufferedWriter outputWriter;
    private List<String> includeContentBeginTags;
    private List<String> includeContentEndTags;
    private boolean readingContentLines = false;

    private PlainTextFromGigaWordDocExtracter(String inputFilePath, BufferedWriter outputWriter,
	    List<String> includeContentBeginTags, List<String> includeContentEndTags) {
	this.inputFilePath = inputFilePath;
	this.outputWriter = outputWriter;
	this.includeContentBeginTags = includeContentBeginTags;
	this.includeContentEndTags = includeContentEndTags;
    }

    public static PlainTextFromGigaWordDocExtracter createPlainTextFromGigaWordDocExtracter(
	    String inputFilePath, BufferedWriter outputWriter, boolean includeHeadLines,
	    boolean includeDateLines) {
	return new PlainTextFromGigaWordDocExtracter(inputFilePath, outputWriter,
		getIncludeContentBeginTags(includeHeadLines, includeDateLines),
		getIncludeContentEndTags(includeHeadLines, includeDateLines));
    }

    public static PlainTextFromGigaWordDocExtracter createPlainTextFromGigaWordDocExtracter(
	    String inputFilePath, String outputFilePath, boolean includeHeadLines,
	    boolean includeDateLines) {
	try {
	    BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFilePath));
	    return PlainTextFromGigaWordDocExtracter.createPlainTextFromGigaWordDocExtracter(
		    inputFilePath, outputWriter, includeHeadLines, includeDateLines);
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private static List<String> getIncludeContentTagWords(boolean includeHeadLines,
	    boolean includeDateLines) {
	List<String> result = new ArrayList<String>();
	result.add(P_TAG);
	if (includeHeadLines) {
	    result.add(HEAD_LINE_TAG);
	}
	if (includeDateLines) {
	    result.add(DATA_LINE_TAG);
	}
	return result;
    }

    private static String getBeginTag(String tagString) {
	return "<" + tagString + ">";
    }

    private static String getEndTag(String tagString) {
	return "</" + tagString + ">";
    }

    private static List<String> getIncludeContentBeginTags(boolean includeHeadLines,
	    boolean includeDateLines) {
	List<String> result = new ArrayList<String>();
	for (String tagWord : getIncludeContentTagWords(includeHeadLines, includeDateLines)) {
	    result.add(getBeginTag(tagWord));
	}
	return result;
    }

    private static List<String> getIncludeContentEndTags(boolean includeHeadLines,
	    boolean includeDateLines) {
	List<String> result = new ArrayList<String>();
	for (String tagWord : getIncludeContentTagWords(includeHeadLines, includeDateLines)) {
	    result.add(getEndTag(tagWord));
	}
	return result;
    }

    private boolean isTagString(String line) {
	return line.startsWith("<") && line.endsWith(">");
    }

    private boolean isIncludeContentBeginTag(String tagString) {
	return this.includeContentBeginTags.contains(tagString);
    }

    private boolean isIncludeContentEndTag(String tagString) {
	return this.includeContentEndTags.contains(tagString);
    }

    public void writeContentLinesToOutputWriter() {

	try {
	    LineIterator it = FileUtils.lineIterator(new File(this.inputFilePath),
		    FileEncodingConverter.UTF8_ENCODING);
	    boolean isFirstLineNewContentBlock = true;
	    String collectedLine = "";
	    int lineNumber = 1;
	    int contentLinesRead = 0;
	    int nonTagLinesRead = 0;
	    int tagLinesRead = 0;
	    int fullLinesWritten = 0;
	    while (it.hasNext()) {
		String line = it.nextLine();

		if ((lineNumber % 1000) == 0) {
		    System.out.println("Processing line: " + lineNumber);
		}

		// System.out.println("line: " + line);
		if (isTagString(line)) {
		    tagLinesRead++;
		    // System.out.println("istTagString...");
		    if (isIncludeContentBeginTag(line)) {
			// System.out.println("isIncludeContentBeginTag...");
			readingContentLines = true;
			isFirstLineNewContentBlock = true;
		    } else if (isIncludeContentEndTag(line)) {
			// System.out.println("isIncludeContentEndTag...");
			readingContentLines = false;
			// System.out.println("Writing line...");
			Assert.assertTrue(collectedLine.length() >= 1);
			outputWriter.write(collectedLine + "\n");
			collectedLine = "";
			fullLinesWritten++;
		    }
		} else {
		    nonTagLinesRead++;
		    if (readingContentLines) {
			if (isFirstLineNewContentBlock) {
			    collectedLine += " ";
			}
			collectedLine += line;
			contentLinesRead++;
		    }
		}
		lineNumber++;
	    }
	    System.out.println("Total lines read: " + lineNumber);
	    System.out.println("tag lines read: " + tagLinesRead);
	    System.out.println("non-tag lines read: " + nonTagLinesRead);
	    System.out.println("content lines read: " + contentLinesRead);
	    System.out.println("full lines written: " + fullLinesWritten);
	    it.close();
	} catch (IOException e) {
	    e.printStackTrace();
	}

    }

    public void closeOutputWriter() {
	FileUtil.closeCloseableIfNotNull(outputWriter);
    }

    public static void main(String[] args) {
	if (args.length != 2) {
	    for (String arg : args) {
		System.out.println("arg: " + arg);
	    }
	    System.out
		    .println("Usage: convertGigaWordFileToPlainText INPUT_FILE_PAHT OUTPUT_FILE_PATH");
	    System.exit(1);
	}
	String inputFilePath = args[0];
	String outputFilePath = args[1];
	boolean includeHeadLines = true;
	boolean includeDateLines = true;

	PlainTextFromGigaWordDocExtracter plainTextFromGigaWordFileExtracter = createPlainTextFromGigaWordDocExtracter(
		inputFilePath, outputFilePath, includeHeadLines, includeDateLines);
	plainTextFromGigaWordFileExtracter.writeContentLinesToOutputWriter();
	plainTextFromGigaWordFileExtracter.closeOutputWriter();

    }
}
