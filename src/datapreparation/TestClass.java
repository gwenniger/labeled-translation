package datapreparation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import util.FileUtil;

public class TestClass {

    private static String inputFilePath = "/home/gemaille/AI/AlignedCorpera/LDC-Corpora-Chinese/Corpora-Unpacked/mtc_p3/data/translation/E01/AFC20020520.0003.sgm";
    private static String outputFilePath = "testFileOutput.txt";

    public static void main(String[] args) {

	BufferedWriter fileWriter = null;
	BufferedReader fileReader = null;
	try {
	    fileWriter = FileUtil.createBufferedWriterWithSpecificEncoding(outputFilePath,
		    FileEncodingConverter.UTF8_ENCODING);
	    fileReader = FileUtil.createBufferedReaderWithSpecificEncoding(inputFilePath,
		    FileEncodingConverter.ISO_88591_ENCODING);
	    String line = null;

	    while ((line = fileReader.readLine()) != null) {
		System.out.println("line: " + line);
		fileWriter.write(line + "\n");
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	}

	finally {
	    FileUtil.closeCloseableIfNotNull(fileWriter);
	    FileUtil.closeCloseableIfNotNull(fileReader);
	}

    }

}
