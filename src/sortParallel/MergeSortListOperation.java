package sortParallel;

import java.util.List;
import java.util.RandomAccess;

//Source : http://antimatroid.wordpress.com/2012/12/01/parallel-merge-sort-in-java/
public class MergeSortListOperation<T extends Comparable<T>, L extends List<T> & RandomAccess>
	implements IListOperation<T, L> {

    private final L a;

    public MergeSortListOperation(L a) {
	if (a == null)
	    throw new IllegalArgumentException("a must not be null.");

	this.a = a;
    }

    @Override
    public L execute() {
	if (a.size() <= 1)
	    return a;

	CopyListOperation<T, L> leftPartition = new CopyListOperation<T, L>(a, (a.size() / 2)
		+ a.size() % 2, 0);
	CopyListOperation<T, L> rightPartition = new CopyListOperation<T, L>(a, (a.size() / 2),
		(a.size() / 2) + a.size() % 2);

	MergeSortListOperation<T, L> leftSort = new MergeSortListOperation<T, L>(
		leftPartition.execute());
	MergeSortListOperation<T, L> rightSort = new MergeSortListOperation<T, L>(
		rightPartition.execute());

	MergeListOperation<T, L> merge = new MergeListOperation<T, L>(leftSort.execute(),
		rightSort.execute());

	return merge.execute();
    }
}