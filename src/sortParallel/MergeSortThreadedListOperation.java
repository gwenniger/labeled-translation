package sortParallel;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.RandomAccess;

import util.FileUtil;

// Source : http://antimatroid.wordpress.com/2012/12/01/parallel-merge-sort-in-java/
public class MergeSortThreadedListOperation<T extends Comparable<T>, L extends List<T> & RandomAccess>
	extends IThreadedListOperation<T, L> {

    private final L listA;
    private L listB;

    // The threshold is the maximum number of lines that will be sorted atomically,
    // withoug further splitting into mergesort sub-threads.
    private final int threshold;
    
    // Max threads can be used to determine a reasonable value for threshold,
    // that avoids too many threads to be created for sorting
    private static int MAX_THREADS = 32;

    public MergeSortThreadedListOperation(L a, int threshold) {
	if (a == null)
	    throw new IllegalArgumentException("a must be non-null.");

	if (threshold <= 0)
	    throw new IllegalArgumentException("threshold must be greater than zero.");

	this.listA = a;
	this.threshold = threshold;
    }
    
    public MergeSortThreadedListOperation(L a) {
	this(a, 1024);
    }

    
    public static <T extends Comparable<T>, L extends List<T> & RandomAccess>  int determineInitialThreshold(L listA){	
	if(listA.size() > MAX_THREADS){
	    return listA.size() / MAX_THREADS;
	    
	}
	return listA.size();
    }
    
    public static <T extends Comparable<T>, L extends List<T> & RandomAccess> MergeSortThreadedListOperation<T, L> createMergeSortThreadedListOperation(
	    L listA) {
	int threshold = determineInitialThreshold(listA);
	return new MergeSortThreadedListOperation<T, L>(listA,threshold);
    }

    public static <T extends Comparable<T>> List<T> sortParallel(ArrayList<T> listToSort) {
	System.out.println("sort parallel...");
	MergeSortThreadedListOperation<T, ArrayList<T>> mergeSortThreadedListOperation = createMergeSortThreadedListOperation(listToSort);
	mergeSortThreadedListOperation.run();
	System.out.println("done");
	return mergeSortThreadedListOperation.getResult();
    }
    
    @Override
    public void run() {
	if (listA.size() <= 1) {
	    listB = listA;
	    return;
	}

	if (listA.size() <= threshold) {
	    MergeSortListOperation<T, L> mergeSort = new MergeSortListOperation<T, L>(listA);
	    listB = mergeSort.execute();
	    return;
	}

	CopyListOperation<T, L> leftPartition = new CopyListOperation<T, L>(listA,
		(listA.size() / 2) + listA.size() % 2, 0);
	MergeSortThreadedListOperation<T, L> leftSort = new MergeSortThreadedListOperation<T, L>(
		leftPartition.execute(),this.threshold);

	CopyListOperation<T, L> rightPartition = new CopyListOperation<T, L>(listA,
		(listA.size() / 2), (listA.size() / 2) + listA.size() % 2);
	MergeSortThreadedListOperation<T, L> rightSort = new MergeSortThreadedListOperation<T, L>(
		rightPartition.execute(),this.threshold);

	rightSort.executeBegin();

	MergeListOperation<T, L> merge = new MergeListOperation<T, L>(leftSort.execute(),
		rightSort.executeEnd());

	listB = merge.execute();
    }

    @Override
    protected L getResult() {
	return listB;
    }

    public static void mainOld(String[] args) {
	ArrayList<String> stringList = new ArrayList<String>(Arrays.asList("A", "D", "0", "F", "E",
		"B"));
	System.out.println("Before: " + stringList);
	MergeSortThreadedListOperation<String, ArrayList<String>> mergeSortThreadedListOperation = MergeSortThreadedListOperation
		.createMergeSortThreadedListOperation(stringList);
	mergeSortThreadedListOperation.run();
	System.out.println("after: " + mergeSortThreadedListOperation.getResult());
    }

    public static void main(String[] args) {
	if (args.length != 2) {
	    System.out.println("Usage : sortParallel INPUT_FILE_PATH OUTPUT_FILE_PATH");
	    System.exit(1);
	}
	System.out.println("Reading file to list...");
	ArrayList<String> stringList = new ArrayList<String>(FileUtil.getLinesInFile(args[0],true));
	System.out.println("done");

	MergeSortThreadedListOperation<String, ArrayList<String>> mergeSortThreadedListOperation = MergeSortThreadedListOperation
		.createMergeSortThreadedListOperation(stringList);
	System.out.println("start sorting...");
	mergeSortThreadedListOperation.run();
	System.out.println("done");

	BufferedWriter fileWriter = null;
	try {
	    System.out.println("Writing result to file...");
	    fileWriter = new BufferedWriter(new FileWriter(args[1]));
	    for (String string : mergeSortThreadedListOperation.getResult()) {
		fileWriter.write(string + "\n");
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	} finally {
	    FileUtil.closeCloseableIfNotNull(fileWriter);
	}

    }
}