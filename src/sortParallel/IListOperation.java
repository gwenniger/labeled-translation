package sortParallel;

import java.util.List;
import java.util.RandomAccess;

//Source : http://antimatroid.wordpress.com/2012/12/01/parallel-merge-sort-in-java/
public interface IListOperation<T, L extends List<T> & RandomAccess> {

    L execute();
}
