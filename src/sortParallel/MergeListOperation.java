package sortParallel;

import java.util.ArrayList;
import java.util.List;
import java.util.RandomAccess;

//Source : http://antimatroid.wordpress.com/2012/12/01/parallel-merge-sort-in-java/
public class MergeListOperation<T extends Comparable<T>, L extends List<T> & RandomAccess>
	implements IListOperation<T, L> {

    private final L a, b;

    public MergeListOperation(L a, L b) {
	if (a == null)
	    throw new IllegalArgumentException("a must not be null.");

	if (b == null)
	    throw new IllegalArgumentException("b must not be null.");

	this.a = a;
	this.b = b;
    }

    @Override
    public L execute() {
	int length = a.size() + b.size();
	L c = (L) new ArrayList<T>(length);

	int i = 0, j = 0;
	for (int k = 0; k < length; k++) {
	    if (i < a.size() && j < b.size()) {
		if (a.get(i).compareTo(b.get(j)) <= 0) {
		    c.add(k, a.get(i++));
		} else {
		    c.add(k, b.get(j++));
		}
	    } else if (i < a.size() && j >= b.size()) {
		c.add(k, a.get(i++));
	    } else if (i >= a.size() && j < b.size()) {
		c.add(k, b.get(j++));
	    } else {
		break;
	    }
	}

	return c;
    }
}
