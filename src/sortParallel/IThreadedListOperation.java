package sortParallel;

import java.util.List;
import java.util.RandomAccess;

//Source : http://antimatroid.wordpress.com/2012/12/01/parallel-merge-sort-in-java/
abstract public class IThreadedListOperation<T, L extends List<T> & RandomAccess> implements
	Runnable, IListOperation<T, L> {

    private Thread thread;

    public void executeBegin() {
	if (thread != null)
	    throw new IllegalStateException();

	thread = new Thread(this);
	thread.start();
    }

    public L executeEnd() {
	if (thread == null)
	    throw new IllegalStateException();

	try {
	    thread.join();
	} catch (InterruptedException e) {

	}

	return getResult();
    }

    public L execute() {
	if (thread != null)
	    throw new IllegalStateException();

	run();
	return getResult();
    }

    abstract protected L getResult();
}