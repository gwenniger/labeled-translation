package sortParallel;

import java.util.ArrayList;
import java.util.List;
import java.util.RandomAccess;

//Source : http://antimatroid.wordpress.com/2012/12/01/parallel-merge-sort-in-java/
public class CopyListOperation<T, L extends List<T> & RandomAccess> implements IListOperation<T, L> {

    private final L source;
    private final int length, initialIndex;

    public CopyListOperation(L source, int length, int initialIndex) {
	if (source == null)
	    throw new IllegalArgumentException("source must be non-null.");

	if (length < 0)
	    throw new IllegalArgumentException(String.format(
		    "length, %d, must be greater than or equal to zero.", length));

	if (initialIndex < 0)
	    throw new IllegalArgumentException(String.format(
		    "initialIndex, %d, must be greater than or equal to zero.", initialIndex));

	if (initialIndex + length > source.size())
	    throw new IllegalArgumentException(
		    String.format(
			    "initialIndex, %d, + length, %d, must be less than or equal to source.size(), %d.",
			    initialIndex, length, source.size()));

	this.source = source;
	this.length = length;
	this.initialIndex = initialIndex;
    }

    @Override
    public L execute() {
	L destination = (L) new ArrayList<T>(length);
	for (int i = 0; i < length; i++)
	    destination.add(i, source.get(initialIndex + i));
	return destination;
    }
}