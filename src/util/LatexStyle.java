package util;

import java.util.Arrays;
import java.util.List;

public class LatexStyle {
    public static final String NL = "\n";
    public static final String LATEX_TABLE_COLUMN_SEPARATOR = " & ";
    public static final String LATEX_PLUS_MIN_SYMBOL = " $\\pm$ ";
    public static final String LATEX_END_OF_LINE_STRING = "\\\\";
    public static final String HLINE = "\\hline";
    public static final String CENTERING = "\\centering";
    private static final String MULTICOLUMN_FUNCTION_STRING = "\\multicolumn";
    private static final String MULTIROW_FUNCTION_STRING = "\\multirow";
    private static final String CLINE_FUNCTION_STRING = "\\cline";
    public static final String TABLE_STAR_BEGIN = "\\begin{table*}";
    public static final String TABLE_STAR_END = "\\end{table*}";
    public static final String TABULAR_BEGIN = "\\begin{tabular}";
    public static final String TABULAR_END = "\\end{tabular}";
    public static final String TEXT_WIDTH = "\\textwidth";
    public static final String BEGIN_DOCUMENT = "\\begin{document}";
    public static final String END_DOCUMENT = "\\end{document}";
    public static final String USE_PACKAGE_STRING = "\\usepackage";
    public static final String DOCUMENT_CLASS_STRING = "\\documentclass";
    private static final String A4PAPER = "a4paper";
    private static final String TEN_PT = "10pt";
    private static final String ARTICLE = "article";

    private static final String argString(String arg) {
	return "{" + arg + "}";
    }

    public static String multiColumn(int noColumns, String secondArg, String thirdArg) {
	return MULTICOLUMN_FUNCTION_STRING + argString(Integer.toString(noColumns))
		+ argString(secondArg) + argString(thirdArg);
    }

    public static String multiRow(int noRows, String secondArg, String thirdArg) {
	return MULTIROW_FUNCTION_STRING + argString(Integer.toString(noRows))
		+ argString(secondArg) + argString(thirdArg);
    }

    public static String cLine(int begin, int end) {
	return CLINE_FUNCTION_STRING + argString(begin + "-" + end);
    }

    public static String beginTabularCommandWithLColumnSpecification(int noCollumns) {
	String result = TABULAR_BEGIN;
	result += "{|";
	for (int i = 0; i < noCollumns; i++) {
	    result += "l|";
	}
	result += "}";
	return result;
    }

    public static String beginDocumentLine() {
	return BEGIN_DOCUMENT + NL;
    }

    public static String endDocumentLine() {
	return END_DOCUMENT + NL;
    }

    private static String bracketedString(String stringToBracket) {
	return "{" + stringToBracket + "}";
    }

    public static String usePackage(String packageName) {
	String result = USE_PACKAGE_STRING + bracketedString(packageName) + NL;
	return result;
    }

    private static String optionSpecifier(String optionsString) {
	return "[" + optionsString + "]";
    }

    private static String optionSpecifier(List<String> optionsList) {
	return "[" + Utility.stringListStringWithoutBrackets(optionsList) + "]";
    }

    public static String usePackageWithOptions(String packageName, String optionsString) {
	String result = USE_PACKAGE_STRING + optionSpecifier(optionsString)
		+ bracketedString(packageName) + NL;
	return result;
    }

    private static String documentSpecificationString(List<String> optionsList, String documentType) {
	String result = DOCUMENT_CLASS_STRING;
	result += optionSpecifier(optionsList);
	result += bracketedString(documentType);
	result += NL;
	return result;
    }

    public static String standardDocumentSpecificaionString() {
	return documentSpecificationString(Arrays.asList(A4PAPER, TEN_PT), ARTICLE);
    }

}
