package util;

import java.io.File;

import junit.framework.Assert;

public class LinuxTarInterface {
    private static String LINUX_UNTARGZ_COMMAND = "tar -zxvf";
    // See:
    // http://stackoverflow.com/questions/11675419/linux-shell-tar-unwanted-extra-directories
    private static String LINUX_TARGZ_COMMAND = "tar zc -f";
    public static String TAR_GZ_EXTENSION = ".tar.gz";

    private static String getFileFolderPath(String filePath) {
	return new File(filePath).getParent();
    }

    public static String getUnpackedFilePath(String tarGzFilePath) {
	Assert.assertTrue(tarGzFilePath.endsWith(TAR_GZ_EXTENSION));
	return tarGzFilePath.replace(TAR_GZ_EXTENSION, "");
    }

    public static String getTarGzPackedFilePath(String filePath) {
	return filePath + TAR_GZ_EXTENSION;
    }

    public static String getFileName(String filePath) {
	File file = new File(filePath);
	return file.getName();
    }

    private static boolean fileExists(String filePath) {
	File fileToCheck = new File(filePath);
	return fileToCheck.exists();
    }

    private static boolean unpackedFileExists(String tarGzFilePath) {
	return fileExists(getUnpackedFilePath(tarGzFilePath));
    }

    public static void unpackTarGzFile(String tarGzFilePath) {
	String untarCommand = LINUX_UNTARGZ_COMMAND + " " + tarGzFilePath + " -C "
		+ getFileFolderPath(tarGzFilePath);
	System.out.println("Lunux untar command: " + untarCommand);
	if (unpackedFileExists(tarGzFilePath)) {
	    System.out.println("Error: unpacked file already exists!");
	    System.exit(1);
	}
	LinuxInteractor.executeCommand(untarCommand, true);
    }

    public static void packTarGzFile(String filePath) {
	String untarCommand = LINUX_TARGZ_COMMAND + " " + getTarGzPackedFilePath(filePath) + " "
		+ " -C " + getFileFolderPath(filePath) + " " + getFileName(filePath);
	System.out.println("Lunux tar command: " + untarCommand);
	if (fileExists(getTarGzPackedFilePath(filePath))) {
	    System.out.println("Error: packed file already exists!");
	    System.exit(1);
	}
	LinuxInteractor.executeCommand(untarCommand, true);
    }

}
