package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import util.Pair;

// Source:
// http://singztechmusings.wordpress.com/2011/06/21/getting-started-with-javas-processbuilder-a-sample-utility-class-to-interact-with-linux-from-java-program/

// See also
// http://stackoverflow.com/questions/525212/how-to-run-unix-shell-script-from-java-code
// http://docs.oracle.com/javase/6/docs/api/java/lang/ProcessBuilder.html

// How to wait for process to complete
// http://stackoverflow.com/questions/6223765/start-a-java-process-using-runtime-exec-processbuilder-start-with-low-priori

public class LinuxInteractor {

	private static void addEnvoronmentVariablesToProcessBuilder(ProcessBuilder processBuilder, List<Pair<String>> environmentVariables) {
		Map<String, String> env = processBuilder.environment();
		for (Pair<String> environmentVariable : environmentVariables) {
			env.put(environmentVariable.getFirst(), environmentVariable.getSecond());
		}
	}

	private static String executeExtendedCommand(List<String> extendedCommandList, List<Pair<String>> environmentVariables, boolean waitForResponse) {
		String response = "";
		ProcessBuilder processBuilder = new ProcessBuilder(extendedCommandList);
		processBuilder.redirectErrorStream(true);
		addEnvoronmentVariablesToProcessBuilder(processBuilder, environmentVariables);

		try {
			Process shell = processBuilder.start();

			if (waitForResponse) {

				// To capture output from the shell
				InputStream shellIn = shell.getInputStream();

				// Wait for the shell to finish and get the return code
				int shellExitStatus = shell.waitFor();
				System.out.println("Exit status" + shellExitStatus);

				response = convertStreamToStr(shellIn);
				shellIn.close();

				if (exitIndicatesError(shellExitStatus)) {
					System.out.println("LinuxInteractor - external command exited with error." + "\n <response>: \n" + response + "\n</response>");
					throw new RuntimeException("Error : LinuxInteractor got failure result back in execution of external command");
				}

			}
		}

		catch (IOException e) {
			System.out.println("Error occured while executing Linux command. Error Description: " + e.getMessage());
			throw new RuntimeException(e);
		}

		catch (InterruptedException e) {
			System.out.println("Error occured while executing Linux command. Error Description: " + e.getMessage());
			throw new RuntimeException(e);
		}

		return response;

	}

	/**
	 * 
	 * @param command  String with the command to be executed
	 * @return true if an error occurred
	 */
	public static boolean executeExtendedCommandDisplayOutput(String command) {
		return executeExtendedCommandDisplayOutput(command, new ArrayList<Pair<String>>());
	}

	/**
	 * 
	 * @param command
	 * @param environmentVariables
	 * @return true if an error occurred
	 */
	public static boolean executeExtendedCommandDisplayOutput(String command, List<Pair<String>> environmentVariables) {

		//String response = "";
		ProcessBuilder pb = new ProcessBuilder(createBasicExtendedCommandList(command));
		addEnvoronmentVariablesToProcessBuilder(pb, environmentVariables);
		pb.redirectErrorStream(true);
		
		

		boolean error = true;
		
		try {
			Process shell = pb.start();

			InputStream is = shell.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line;

			System.out.println("Output of running is:");

			while ((line = br.readLine()) != null) {
				System.out.println(line);
			}

			System.out.println("Waiting for process to end");
			int shellExitStatus = shell.waitFor();
			System.out.println("Process ended");
			error = exitIndicatesError(shellExitStatus);
		}

		catch (IOException e) {
			System.out.println("Error occured while executing Linux command. Error Description: " + e.getMessage());
		}

		catch (InterruptedException e) {
			System.out.println("Error occured while executing Linux command. Error Description: " + e.getMessage());
		}
		

		return error;
	}

	private static boolean exitIndicatesError(int exitStatus) {
		return exitStatus > 0;
	}

	private static List<String> createBasicExtendedCommandList(String command) {
		// The extra arguments "bash", "-c" are essential to make things work
		// properly
		List<String> extendedCommandList = new ArrayList<String>(Arrays.asList("/bin/bash", "-c"));
		extendedCommandList.add(command);
		return extendedCommandList;
	}

	public static String executeCommand(String command, boolean waitForResponse) {
		return executeCommand(command, new ArrayList<Pair<String>>(), waitForResponse);
	}

	public static String executeCommand(String command, List<Pair<String>> environmentVariables, boolean waitForResponse) {

		System.out.println("Linux command: " + command);
		return executeExtendedCommand(createBasicExtendedCommandList(command), environmentVariables, waitForResponse);
	}

	/*
	 * To convert the InputStream to String we use the Reader.read(char[]
	 * buffer) method. We iterate until the Reader return - which means there's
	 * no more data to read. We use the StringWriter class to produce the
	 * string.
	 */

	public static String convertStreamToStr(InputStream is) throws IOException {
		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {

					writer.write(buffer, 0, n);
				}
			} finally {
				is.close();
			}
			return writer.toString();
		} else {
			return "";
		}
	}

}