package util.threadCpuTime;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.RuntimeMXBean;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.HashMap;
import java.util.Map;

// Adapted from: http://stackoverflow.com/questions/29931391/obtaining-cpu-thread-usage-in-java
public class ThreadCPUUsage {

    private int sampleTime = 10000;
    private ThreadMXBean threadMxBean = ManagementFactory.getThreadMXBean();
    private RuntimeMXBean runtimeMxBean = ManagementFactory.getRuntimeMXBean();
    private OperatingSystemMXBean osMxBean = ManagementFactory.getOperatingSystemMXBean();
    private Map<Long, Long> threadInitialCPU = new HashMap<Long, Long>();
    private Map<Long, Float> threadCPUUsage = new HashMap<Long, Float>();
    private long initialUptime = runtimeMxBean.getUptime();

    private ThreadCPUUsage() {

    }

    private void doComputation(int numAdditions) {
	int result = 1;
	for (int i = 0; i < numAdditions; i++) {
	    result += 2;
	    if ((i % 1000) == 0) {
		System.out.println("result: " + result);
	    }
	}
    }

    private void startTiming(){
	ThreadInfo[] threadInfos = threadMxBean.dumpAllThreads(false, false);
	for (ThreadInfo info : threadInfos) {
	    threadInitialCPU.put(info.getThreadId(),
		    threadMxBean.getThreadCpuTime(info.getThreadId()));
	}
    }
    
    private void computeCPUTime(int numAdditions) {

	

	try {
	    Thread.sleep(sampleTime);
	    doComputation(numAdditions);
	} catch (InterruptedException e) {
	}

	long upTime = runtimeMxBean.getUptime();

	Map<Long, Long> threadCurrentCPU = new HashMap<Long, Long>();
	ThreadInfo[] threadInfos = threadMxBean.dumpAllThreads(false, false);
	for (ThreadInfo info : threadInfos) {
	    threadCurrentCPU.put(info.getThreadId(),
		    threadMxBean.getThreadCpuTime(info.getThreadId()));
	}

	// CPU over all processes
	// int nrCPUs = osMxBean.getAvailableProcessors();
	// total CPU: CPU % can be more than 100% (devided over multiple cpus)
	long nrCPUs = 1;
	// elapsedTime is in ms.
	long elapsedTime = (upTime - initialUptime);
	for (ThreadInfo info : threadInfos) {
	    // elapsedCpu is in ns
	    Long initialCPU = threadInitialCPU.get(info.getThreadId());
	    if (initialCPU != null) {
		long elapsedCpu = threadCurrentCPU.get(info.getThreadId()) - initialCPU;
		float cpuUsage = elapsedCpu / (elapsedTime * 1000000F * nrCPUs);
		threadCPUUsage.put(info.getThreadId(), cpuUsage);
	    }
	}

	// threadCPUUsage contains cpu % per thread
	System.out.println(threadCPUUsage);
	// You can use osMxBean.getThreadInfo(theadId) to get information on
	// every
	// thread reported in threadCPUUsage and analyze the most CPU intentive
	// threads
    }

    public static void main(String[] args) {
	ThreadCPUUsage threadCPUTime = new ThreadCPUUsage();
	threadCPUTime.computeCPUTime(1000);
    }
}
