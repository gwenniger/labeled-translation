package util.threadCpuTime;

public interface ThreadCpuTimer {

    public void startThreadCpuTimer(Thread thread);
    public void stopThreadCpuTimer(Thread thread);
    
}
