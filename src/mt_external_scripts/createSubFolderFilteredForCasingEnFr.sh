#!/bin/bash

# This is a script to make a subfolder containing the lines for which a proper match could be find 
# In the Union of Europarl Version 7  (German) in combination with the files from Europarl version 2 
# and version 3 that were not present in Version 7.
# As some files could not find a corresponding match, it was necessary to filter them out

function deleteLineFromFiles {
	echo "Removing loose line " ${LOOSE_DELETED_LINE} "from the copied files to assure matching with the Cased Reference File"
	sed -i $1d  ${CasedFilesFolder}aligned.0.${ForeignLanguageAcronym}
	sed -i $1d  ${CasedFilesFolder}aligned.0.en
	sed -i $1d  ${CasedFilesFolder}aligned.grow-diag-final-and
	sed -i $1d  ${CasedFilesFolder}aligned.grow-diag-final-and-inverted             
}  

function deleteLinesFromFiles {
	echo "Removing lines span " $1 " to " $2 "from the copied files to assure matching with the Cased Reference File"
	sed -i $1,$2d  ${CasedFilesFolder}aligned.0.${ForeignLanguageAcronym}
	sed -i $1,$2d  ${CasedFilesFolder}aligned.0.en
	sed -i $1,$2d  ${CasedFilesFolder}aligned.grow-diag-final-and
	sed -i $1,$2d  ${CasedFilesFolder}aligned.grow-diag-final-and-inverted
}

ForeignLanguageAcronym=fr  




OriginalFilesFolder=/home/gemaille/AI/AlignedCorpera/en-${ForeignLanguageAcronym}/
CasedFilesFolder=${OriginalFilesFolder}en-${ForeignLanguageAcronym}-Cased/
mkdir ${CasedFilesFolder}


echo "Copying Files to new folder"
cp ${OriginalFilesFolder}aligned.0.${ForeignLanguageAcronym}  ${CasedFilesFolder}aligned.0.${ForeignLanguageAcronym}
cp ${OriginalFilesFolder}aligned.0.en  ${CasedFilesFolder}aligned.0.en
cp ${OriginalFilesFolder}aligned.grow-diag-final-and  ${CasedFilesFolder}aligned.grow-diag-final-and
cp ${OriginalFilesFolder}aligned.grow-diag-final-and-inverted  ${CasedFilesFolder}aligned.grow-diag-final-and-inverted

# En-French
# French
# Delete a block of erroneous sentences in mixed languages (in the English side)
deleteLinesFromFiles 457378 459660
#(total 2283 sentences deleted)

# Delete more missing sentences
FIRST_DELETED_LINE=540594
LAST_DELETED_LINE=541556
deleteLinesFromFiles ${FIRST_DELETED_LINE} ${LAST_DELETED_LINE}






