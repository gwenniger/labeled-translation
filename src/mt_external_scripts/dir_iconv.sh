#!/bin/bash

#Source : http://www.ustrem.org/en/articles/bash-iconv-all-files-from-directory-en/
# Usage : path/to/dir_iconv.sh ~/txt1 cp1251 utf8 - converts all files from directory ~/txt1 от cp1251 (windows-1251) to utf8.


ICONVBIN='/usr/bin/iconv' # path to iconv binary

if [ $# -lt 3 ]
then
    echo "$0 dir from_charset to_charset"
    exit
fi

for f in $1/*
do
    if test -f $f
    then
        echo -e "\nConverting $f"
        /bin/mv $f $f.old
        $ICONVBIN -f $2 -t $3 $f.old > $f
    else
        echo -e "\nSkipping $f - not a regular file";
    fi
done 
