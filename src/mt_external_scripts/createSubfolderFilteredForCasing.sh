#!/bin/bash

# This is a script to make a subfolder containing the lines for which a proper match could be find 
# In the Union of Europarl Version 7  (German) in combination with the files from Europarl version 2 
# and version 3 that were not present in Version 7.
# As some files could not find a corresponding match, it was necessary to filter them out

ForeignLanguageAcronym=fr  
# German
#FIRST_DELETED_LINE=573875
#LAST_DELETED_LINE=574921

# French
FIRST_DELETED_LINE=542877
LAST_DELETED_LINE=543839



OriginalFilesFolder=/home/gemaille/AI/AlignedCorpera/en-${ForeignLanguageAcronym}/
CasedFilesFolder=${OriginalFilesFolder}en-${ForeignLanguageAcronym}-Cased/
mkdir ${CasedFilesFolder}


echo "Copying Files to new folder"
cp ${OriginalFilesFolder}aligned.0.${ForeignLanguageAcronym}  ${CasedFilesFolder}aligned.0.${ForeignLanguageAcronym}
cp ${OriginalFilesFolder}aligned.0.en  ${CasedFilesFolder}aligned.0.en
cp ${OriginalFilesFolder}aligned.grow-diag-final-and  ${CasedFilesFolder}aligned.grow-diag-final-and
cp ${OriginalFilesFolder}aligned.grow-diag-final-and-inverted  ${CasedFilesFolder}aligned.grow-diag-final-and-inverted



echo "Removing lines span " ${FIRST_DELETED_LINE} " to " ${LAST_DELETED_LINE} "from the copied files to assure matching with the Cased Reference File"
sed -i ${FIRST_DELETED_LINE},${LAST_DELETED_LINE}d  ${CasedFilesFolder}aligned.0.${ForeignLanguageAcronym}
sed -i ${FIRST_DELETED_LINE},${LAST_DELETED_LINE}d  ${CasedFilesFolder}aligned.0.en
sed -i ${FIRST_DELETED_LINE},${LAST_DELETED_LINE}d  ${CasedFilesFolder}aligned.grow-diag-final-and
sed -i ${FIRST_DELETED_LINE},${LAST_DELETED_LINE}d  ${CasedFilesFolder}aligned.grow-diag-final-and-inverted

# En-German
#LOOSE_DELETED_LINE=638400
# En-French
LOOSE_DELETED_LINE=457378 
echo "Removing loose line " ${LOOSE_DELETED_LINE} "from the copied files to assure matching with the Cased Reference File"
sed -i ${LOOSE_DELETED_LINE}d  ${CasedFilesFolder}aligned.0.${ForeignLanguageAcronym}
sed -i ${LOOSE_DELETED_LINE}d  ${CasedFilesFolder}aligned.0.en
sed -i ${LOOSE_DELETED_LINE}d  ${CasedFilesFolder}aligned.grow-diag-final-and
sed -i ${LOOSE_DELETED_LINE}d  ${CasedFilesFolder}aligned.grow-diag-final-and-inverted










