package mt_external_scripts;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Lowercase 
{
	private final BufferedReader inputReader;
	private final BufferedWriter outputWriter;
	
	
	private Lowercase(BufferedReader inputReader, BufferedWriter outputWriter)
	{
		this.inputReader = inputReader;
		this.outputWriter = outputWriter;
	}
	
	public static Lowercase createLowercase(String inputFileName, String outputFileName) throws IOException
	{
		BufferedReader inputReader = new BufferedReader(new FileReader(inputFileName));
		BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFileName));
		return new Lowercase(inputReader,outputWriter);
	}
	
	
	public void performLowercaseConversion() throws IOException
	{
		String line;
		while((line = inputReader.readLine()) != null)
		{
			outputWriter.write(line.toLowerCase() + "\n");
		}
		outputWriter.close();
		System.out.println("Completed conversion to lowercase...");
		
	}
	

	public static void main(String[] args)
	{
		if(args.length != 2)
		{
			System.out.println("Usage: java -jar lowercase.jar SourceFile ResultFile");
			System.exit(0);
		}
		String inputFileName = args[0];
		String outputFileName = args[1];
		
		try {
			Lowercase lowercase = createLowercase(inputFileName, outputFileName);
			lowercase.performLowercaseConversion();
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	
}
