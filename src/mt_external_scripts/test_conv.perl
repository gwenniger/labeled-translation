#! /usr/bin/perl

# Usage: test_conv.pl #setid #srclang #trglang #docid #sysid
#
# STDIN:           the translation file from the tested system
#
# STDOUT:          the sgm file for the BLEU evaluator script
#
# comments:        
#

$arg_setid = @ARGV[0];
$arg_srclang = @ARGV[1];
$arg_trglang = @ARGV[2];
$arg_docid = @ARGV[3];
$arg_sysid = @ARGV[4];

print STDOUT "<tstset setid=\"$arg_setid\" srclang=\"$arg_srclang\" trglang=\"$arg_trglang\">\n";
print STDOUT "<DOC docid=\"$arg_docid\" sysid=\"$arg_sysid\">\n";

while (<STDIN>) {
   chomp;

   print STDOUT '<seg>';
   print STDOUT $_;
   print STDOUT "</seg>\n";
}

print STDOUT "</DOC>\n";
print STDOUT "</tstset>\n";
