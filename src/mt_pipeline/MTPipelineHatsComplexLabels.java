package mt_pipeline;

import mt_pipeline.mt_config.MTConfigFile;

public abstract class MTPipelineHatsComplexLabels extends MTPipeline
{
	protected MTPipelineHatsComplexLabels() {
	};

	protected MTPipelineHatsComplexLabels(MTConfigFile theConfig, String configFilePath, int noModelParameters,  boolean extractGrammarsAsExternalProcess,MTSystem mtSystem) {
		super(theConfig, configFilePath, noModelParameters, extractGrammarsAsExternalProcess,mtSystem);
	}
}
