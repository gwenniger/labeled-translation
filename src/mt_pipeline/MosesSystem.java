package mt_pipeline;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import mt_pipeline.mt_config.MTConfigFile;

public class MosesSystem extends MTSystem {
	private static String SYSTEM_TYPE = "Moses";

	protected MosesSystem(MTConfigFile theConfig, DecoderConfigCreater decoderConfigCreater,SystemIdentity systemIdentity) {
		super(theConfig, decoderConfigCreater, systemIdentity);
	}

	public static MosesSystem createMosesSystem(MTConfigFile theConfig,SystemIdentity systemIdentity) {
		return new MosesSystem(theConfig, MosesDecoderConfigCreater.createMosesDecoderConfigCreater(theConfig),systemIdentity);
	}

	public static String getSystemTypeName() {
		return SYSTEM_TYPE;
	}
}
