package mt_pipeline;

public interface MTPipelineCreater {
	public MTPipeline  createMTPipeline(String configFilePath) throws RuntimeException;
}
