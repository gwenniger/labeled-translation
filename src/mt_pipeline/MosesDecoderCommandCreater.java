package mt_pipeline;

import mt_pipeline.mt_config.MTConfigFile;

public class MosesDecoderCommandCreater extends DecoderCommandCreater {

	protected MosesDecoderCommandCreater(MTConfigFile theConfig) {
		super(theConfig);
	}

	public static MosesDecoderCommandCreater createMosesDecoderCommandCreater(MTConfigFile theConfig) {
		return new MosesDecoderCommandCreater(theConfig);
	}

	private String multiThreadingOption() {
		return " -threads " + theConfig.decoderConfig.getNumParallelDecoders() + " ";
	}

	@Override
	public String getDecodeCommand(String category, String configFilePath, boolean useTunedParameters) {
		String result = "";
		result += theConfig.decoderConfig.baseMosesDecoderCall() + multiThreadingOption() + " -f " + configFilePath + theConfig.decoderConfig.getMosesNBestForMBRSpecificationParameterString(theConfig.filesConfig.getDecoderNBestOutputFilePathSystemSpecific(category, useTunedParameters)) + " < "
				+ theConfig.filesConfig.getEnrichedSourceFilePath(category)
				+ DecoderInterface.joshuaDecodeOutputSpecification(theConfig, category, useTunedParameters);
		return result;
	}

}
