package mt_pipeline.tuning;

import mt_pipeline.mt_config.MTConfigFile;

public abstract class MiraCommandCreater extends MosesTuningCommandCreater {

	protected MiraCommandCreater(MTConfigFile theConfig,
			MosesBasedTuningProperties mosesBasedTuningProperties) {
		super(theConfig, mosesBasedTuningProperties);
	}

	@Override
	protected String tuningAlgoirthmSpecification() {
		return "--batch-mira";
	}


	@Override
	protected String getTunerSpecificParameters() {
		return mosesBasedTuningProperties.getMiraRegularizationSpecification();
	}

}
