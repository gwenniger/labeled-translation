package mt_pipeline.tuning;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;

import mt_pipeline.DecoderInterface;
import mt_pipeline.mt_config.MTConfigFile;
import mt_pipeline.mt_config.MTFoldersConfig;

public class MertDecodingCommandFileCreater extends TuningFileCreater {

	protected MertDecodingCommandFileCreater(MTConfigFile theConfig, BufferedWriter outputWriter, SystemIdentity systemIdentity) {
		super(theConfig, outputWriter, systemIdentity);
	}

	public static MertDecodingCommandFileCreater createMertCommandFileCreater(MTConfigFile theConfig,SystemIdentity systemIdentity) {
		return new MertDecodingCommandFileCreater(theConfig, createOutputWriter(theConfig.filesConfig.getMertDecoderCommandFileName()), systemIdentity);
	}

	private String getMertDecoderCommand() {
		String result = "";
		result += theConfig.decoderConfig.joshuaDecoderCallWithConfigFileSpecification(theConfig.systemSpecificConfig.getDecoderMertConfigFileName()) + " < "
				+ theConfig.filesConfig.getEnrichedSourceFilePath(MTConfigFile.DEV_CATEGORY) 
				+ DecoderInterface.joshuaDecodeOutputSpecification(theConfig, MTFoldersConfig.MERT_FOLDER, Tuned); 
				//+ "  >  "
				//+ theConfig.filesConfig.getDecoderOutputFilePath(MTFoldersConfig.MERT_FOLDER, Tuned);
		// result += theConfig.baseJoshuaJavaCall() + " -Djava.library.path="+
		// theConfig.getJoshuaLibDir() + " joshua.decoder.JoshuaDecoder " +
		// theConfig.getJoshuaMertConfigFileName() + " " +
		// theConfig.getSourcePreprocessedFileName(MTConfigFile.DevCategory) +
		// "  " + theConfig.getDecoderOutputFilePath(MTConfigFile.MertFolder);
		return result;
	}

	@Override
	protected String getFileContentsString() {
		return getMertDecoderCommand();
	}

	public static void createExecutableMertCommandFile(MTConfigFile theConfig,SystemIdentity systemIdentity) throws IOException {
		MertDecodingCommandFileCreater mertCommandFileCreater = MertDecodingCommandFileCreater.createMertCommandFileCreater(theConfig,systemIdentity);
		mertCommandFileCreater.writeFile();
		File file = new File(theConfig.filesConfig.getMertDecoderCommandFileName());
		file.setExecutable(true);

		if (!file.canExecute()) {
			System.out.println("File not executable!!!");
			System.exit(1);
		}
	}

}
