package mt_pipeline.tuning;


import mt_pipeline.mt_config.MTConfigFile;

public class JoshuaProCommandCreater extends TuningCommandCreater {

	private static final String PRO_TUNING_PROGRAM_NAME = "PRO";

	protected JoshuaProCommandCreater(MTConfigFile theConfig) {
		super(theConfig);
	}

	public static JoshuaProCommandCreater createJoshuaProCommandCreater(MTConfigFile theConfig) {
		return new JoshuaProCommandCreater(theConfig);
	}

	private int getMaxMemInMB()
	{
		String maxMemoryString = this.theConfig.getXmx();
		maxMemoryString = (maxMemoryString.replace("-Xmx", "")).toLowerCase();
		
		if(maxMemoryString.endsWith("m"))
		{
			return Integer.parseInt(maxMemoryString.substring(0,maxMemoryString.length() - 1));
		}
		else if(maxMemoryString.endsWith("g"))
		{
			return Integer.parseInt(maxMemoryString.substring(0,maxMemoryString.length() - 1)) * 1000;
		}
		else
		{
			throw new RuntimeException("Error getMaxMemInMB: could not extract max memory from " + this.theConfig.getXmx());
		}
	}

	private String getProCommand() {
		String result = theConfig.decoderConfig.baseJoshuaJavaCall() + " joshua.pro.PRO  -maxMem " + getMaxMemInMB() + " " + this.theConfig.filesConfig.getMertConfigFileName();
		return result;

	}

	@Override
	public String getTuningCommand() {
		return getProCommand();
	}

	@Override
	public String getTuningCommandName() {
		return PRO_TUNING_PROGRAM_NAME;
	}
	
}
