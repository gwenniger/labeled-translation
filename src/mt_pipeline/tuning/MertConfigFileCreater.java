package mt_pipeline.tuning;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;

import java.io.BufferedWriter;
import mt_pipeline.mt_config.MTConfigFile;
import mt_pipeline.mt_config.MTFoldersConfig;
import mt_pipeline.mt_config.MTDecoderConfig;

public class MertConfigFileCreater extends TuningFileCreater {

	protected MertConfigFileCreater(MTConfigFile theConfig, BufferedWriter outputWriter, SystemIdentity systemIdentity) {
		super(theConfig, outputWriter, systemIdentity);
	}

	public static MertConfigFileCreater createMertConfigFileCreater(MTConfigFile theConfig, String joshuaConfigFileName,SystemIdentity systemIdentity) {
		return new MertConfigFileCreater(theConfig, createOutputWriter(joshuaConfigFileName), systemIdentity);
	}

	private String getReferenceFile() {
		return theConfig.filesConfig.getTargetTokenizedAndLowerCasedFileNames(MTConfigFile.DEV_CATEGORY).get(0);
	}

	private String getJoshuaMertParallelizationConfigLines() {
		String result = "";
		result += NL + "#  thrCnt threadCount: number of threads to run in parallel when optimizing";
		result += NL + "-thrCnt " + theConfig.decoderConfig.getNumTunerThreads();
		return result;
	}

	public String getNBestFormatStringJosuaTunerConfig() {
		return NL + "-" + getNBestFormatPropertyString() + "\t" + theConfig.decoderConfig.getNBestFormat();
	}

	private String getJoshuaProSpecificConfigLines() {
		String result = "";
		result += NL + "#  trainingMode (1-4) : something for specifying extra sparse parameters, not well documented, the (normal) default seems 1";
		result += NL + "-trainingMode " + getTuningStrategyMode();
		result += NL + "#  -nbestFormat : Format for the nBest list. can be \"dense\" or \"sparse\"";
		result += getNBestFormatStringJosuaTunerConfig();
		// // THE CLASSIFICATION ALGORITHM(PERCEP, MEGAM, MAXENT ...)
		result += NL + "-classifierClass joshua.pro.ClassifierPerceptron";
		result += NL + "-classifierParams	'30 0.5 0.0'";
		result += NL + "-Tau	8000		#num of candidate samples";
		result += NL + "-Xi	50		#num of top candidates";
		result += NL + "-interCoef	" + getTuningInterpolationCoefficient()
				+ "\t#linear interpolation coef. range:[0,1]. 1=using new weights only; 0=using previous weights only";
		result += NL + "-metricDiff	0.05	#threshold for sample selection";
		return result;
	}

	private String getDecoderCommandFileSpecification() {
		return NL + "-cmd " + theConfig.filesConfig.getMertDecoderCommandFileName();
		// return NL +"-s " +
		// theConfig.filesConfig.getEnrichedSourceFilePath(MTConfigFile.DEV_CATEGORY);
	}

	private String getMertConfigFile() {
		String result = "" + NL + "### MERT parameters" + NL + "# target sentences file name (in this case, file name prefix)" + NL + "-r "
				+ getReferenceFile() + NL + "-rps	1			# references per sentence" + NL + "-p " + theConfig.filesConfig.getMertParametersFileName()
				+ "# parameter file" + NL + "-m	BLEU 4 closest		# evaluation metric and its options" + NL + "-maxIt	"
				+ theConfig.decoderConfig.getNoMertIterations() + "			# maximum MERT iterations" + NL
				+ "-ipi	20			# number of intermediate initial points per iteration" + getDecoderCommandFileSpecification()
				+ "    # file containing commands to run decoder" + NL + "-decOut "
				+ theConfig.filesConfig.getDecoderOutputFilePath(MTFoldersConfig.MERT_FOLDER, Tuned) + " # file prodcued by decoder" + NL + "-dcfg	 "
				+ theConfig.systemSpecificConfig.getDecoderMertConfigFileName() + "  # decoder config file" + NL + "-N	" + theConfig.decoderConfig.getNBestSizeTuning()
				+ "               # size of N-best list" + NL + "-v	1                       # verbosity level (0-2; higher value => more verbose)" + NL
				+ "-seed   12341234                # random number generator seed";

		result += getJoshuaMertParallelizationConfigLines();
		if (this.theConfig.filesConfig.useProTuning()) {
			result += getJoshuaProSpecificConfigLines();
		}
		return result;
	}

	@Override
	protected String getFileContentsString() {
		return getMertConfigFile();
	}

}
