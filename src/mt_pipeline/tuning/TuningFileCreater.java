package mt_pipeline.tuning;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import mt_pipeline.DecoderFilesCreaterBase;
import mt_pipeline.mt_config.MTConfigFile;

public abstract class TuningFileCreater extends DecoderFilesCreaterBase {
	protected static final boolean Tuned = true;
	protected final BufferedWriter outputWriter;


	protected TuningFileCreater(MTConfigFile theConfig, BufferedWriter outputWriter, SystemIdentity systemIdentity) {
		super(theConfig,systemIdentity);
		this.outputWriter = outputWriter;
	}

	protected static BufferedWriter createOutputWriter(String outputFileName) {
		try {
			return new BufferedWriter(new FileWriter(outputFileName));
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	protected abstract String getFileContentsString();

	public void writeFile() {
		try {
			this.outputWriter.write(this.getFileContentsString());
			this.outputWriter.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}