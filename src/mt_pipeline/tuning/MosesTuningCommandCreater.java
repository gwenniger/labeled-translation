package mt_pipeline.tuning;

import mt_pipeline.mt_config.MTConfigFile;

public abstract class MosesTuningCommandCreater extends TuningCommandCreater {

	private static final String MOSES_MERT_PROGRAM_NAME = "mert-moses.pl";
	protected static final String PARAMETER_SEPARATOR = " ";




	// private static final String PLAIN_TEXT_GRAMMAR_FILTER_PROGRAM_NAME =
	// "filter-model-given-input.pl";

	protected final MosesBasedTuningProperties mosesBasedTuningProperties;

	protected MosesTuningCommandCreater(MTConfigFile theConfig,MosesBasedTuningProperties mosesBasedTuningProperties) {
		super(theConfig);
		this.mosesBasedTuningProperties = mosesBasedTuningProperties;
	}

	public String getDecoderCommand() {
		return theConfig.decoderConfig.baseMosesDecoderCall();
	}
	
	private String mosesMertProgramCall() {

		String tuningAlgorithmSpecification = this
				.tuningAlgoirthmSpecification();
		if (tuningAlgorithmSpecification.length() > 0) {
			tuningAlgorithmSpecification = PARAMETER_SEPARATOR
					+ tuningAlgorithmSpecification;
		}

		return this.theConfig.decoderConfig.getMosesTrainingScriptsDir()
				+ MOSES_MERT_PROGRAM_NAME + PARAMETER_SEPARATOR
				+ mosesBasedTuningProperties.getDevSourceFile() + PARAMETER_SEPARATOR + mosesBasedTuningProperties.getReferenceFilePrefix()
				+ PARAMETER_SEPARATOR
				+ getDecoderCommand()
				+ PARAMETER_SEPARATOR + mosesBasedTuningProperties.getConfigurationFilePath()
				+ PARAMETER_SEPARATOR + mosesBasedTuningProperties.extraDecoderOptions()
				+ tuningAlgorithmSpecification + PARAMETER_SEPARATOR
				+ getTunerSpecificParameters() + mosesBasedTuningProperties.noGrammarFilteringOption()
				+ PARAMETER_SEPARATOR + mosesBasedTuningProperties.nBestListSizeSpecification()
				+ PARAMETER_SEPARATOR
				+ mosesBasedTuningProperties.useBestScoringDevelopmentRunWeightsFlag()
				+ PARAMETER_SEPARATOR + mosesBasedTuningProperties.specifyMaximumNumberOfIterations()
				+ PARAMETER_SEPARATOR + mosesBasedTuningProperties.getMertDirSpecification()
				+ PARAMETER_SEPARATOR + mosesBasedTuningProperties.getTunerWorkDirSpecification()
				+ PARAMETER_SEPARATOR + " > " + mosesBasedTuningProperties.getMertOutSpecification();
	}

	protected abstract String getTunerSpecificParameters();


	protected abstract String tuningAlgoirthmSpecification();

	@Override
	public String getTuningCommand() {
		return mosesMertProgramCall();
	}

	

	@Override
	public String getTuningCommandName() {
		// TODO Auto-generated method stub
		return null;
	}

}
