package mt_pipeline.tuning;

import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;

import java.io.BufferedWriter;
import java.util.List;

import bitg.Pair;

import mt_pipeline.mt_config.MTConfigFile;

public class TuningParametersFileCreater extends TuningFileCreater {

	protected TuningParametersFileCreater(MTConfigFile theConfig, BufferedWriter outputWriter, SystemIdentity systemIdentity) {
		super(theConfig, outputWriter, systemIdentity);
	}

	public static TuningParametersFileCreater createTuningParametersFileCreater(MTConfigFile theConfig, int noParameters,SystemIdentity systemIdentity) {
		return new TuningParametersFileCreater(theConfig, createOutputWriter(theConfig.filesConfig.getMertParametersFileName()), systemIdentity);
	}

	/*
	 * private String getMertParamsFile() { String result = "" + NL +
	 * "lm			    |||	1.000000		Opt	0.1	+Inf	+0.5	+1.5" +
	 * 
	 * // Almost optimal initial parameters //NL +
	 * "phrasemodel pt 0	|||	1.066893		Opt	-Inf	+Inf	-1	+1" + //NL +
	 * "phrasemodel pt 1	|||	0.752247		Opt	-Inf	+Inf	-1	+1" + //NL +
	 * "phrasemodel pt 2	|||	0.589793		Opt	-Inf	+Inf	-1	+1" + // Sub-optimal
	 * initial parameters NL +
	 * "phrasemodel pt 0  ||| 1.4222868096216315        Opt	-Inf	+Inf	-1	+1" +
	 * NL +
	 * "phrasemodel pt 1  ||| 0.9132292860108913        Opt	-Inf	+Inf	-1	+1" +
	 * NL +
	 * "phrasemodel pt 2  |||  0.1731475933296384       Opt	-Inf	+Inf	-1	+1" +
	 * 
	 * 
	 * NL + "wordpenalty		|||	-2.844814		Opt	-Inf	+Inf	-5	0" + NL +
	 * "normalization = absval 1 lm";
	 * 
	 * 
	 * return result; }
	 */

	private String getUniformParameters() {
		String result = "";

		List<Pair<String, Double>> orderedFeatureNamesWithWeights;
		
		if(useComplexTuningStrategy())
		{
			orderedFeatureNamesWithWeights = geBasicOrderedFeatureNamesWithWeights();
		}
		else
		{
			orderedFeatureNamesWithWeights = getAllOrderedFeatureNamesWithWeights();
		}
		
		int i = 0;
		for (Pair<String, Double> featureNameWeightPair : orderedFeatureNamesWithWeights) {

			//result += NL + "phrasemodel pt " + i + "  ||| " + featureNameWeightPair.last + "  Opt	-Inf	+Inf	-1	+1";
		    	/** 
		    	  the names of the parameters in the parameters file should match those in
		    	  the decoder config. Mostly the parameters file is no longer used, but PRP
		    	  still uses it, and it gets confused if the names differ in the two 
		    	  configuration files.*/ 
		    	result += NL +  "tm_pt_" + i + "  ||| " + featureNameWeightPair.last + "  Opt  -Inf  +Inf  -1  +1";
			i++;
		}
		return result;
	}

	private String getDiscriminativeFeaturesFileSpecificationLine() {
		return NL + "discriminative " + theConfig.filesConfig.getDiscriminativeFeaturesListFilePath() + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR
				+ "1.0 Opt -Inf  +Inf  -5  5";
	}

	private boolean useComplexTuningStrategy() {
		return getTuningStrategyMode() > 1;
	}

	private String getMertParamsFile() {
		String result = "" + NL + "lm			    |||	1.000000		Opt	0.1	+Inf	+0.5	+1.5";

		result += getUniformParameters();

		result += NL + "WordPenalty		|||	-2.844814		Opt	-Inf	+Inf	-5	0";
		if (useComplexTuningStrategy()) {
			result += getDiscriminativeFeaturesFileSpecificationLine();
		}
		result += NL + "normalization = absval 1 lm";

		return result;
	}

	@Override
	protected String getFileContentsString() {
		return getMertParamsFile();
	}
}
