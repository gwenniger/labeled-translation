package mt_pipeline.tuning;

import junit.framework.Assert;
import mt_pipeline.mt_config.MTConfigFile;

public class MosesBasedTuningProperties {

	private static final String MERT_OUT_FILE_NAME = "mert.out";
	
	private static String MOSES_MIRA_REGULARIZATION_PARAMETER = "C";
	// This flag is used to continue tuning from the last finished iteration
	private static String TUNER_CONTINUE_FLAG = "--continue";

	private final MTConfigFile theConfig;

	private MosesBasedTuningProperties(MTConfigFile theConfig) {
		this.theConfig = theConfig;
	}

	public static MosesBasedTuningProperties createMosesBasedTuningProperties(
			MTConfigFile theConfig) {
		return new MosesBasedTuningProperties(theConfig);
	}

	/**
	 * Return the reference name prefix. This is useful to allow the Moses-based tuner to 
	 * use multiple references
	 * @return
	 */
	protected String getReferenceFilePrefix() {
		return theConfig.filesConfig
				.getTargetTokenizedAndLowerCasedFileNamePrefix(MTConfigFile.DEV_CATEGORY);
	}

	protected String getDevSourceFile() {
		return theConfig.filesConfig
				.getEnrichedSourceFilePath(MTConfigFile.DEV_CATEGORY);
	}

	protected String getConfigurationFilePath() {
		return theConfig.systemSpecificConfig.getDecoderMertConfigFileName();
	}

	protected String getMertDirSpecification() {
		String result = "--mertdir " + theConfig.decoderConfig.getMosesBinDir();
		return result;
	}

	protected String specifyMaximumNumberOfIterations() {
		return "--maximum-iterations="
				+ theConfig.decoderConfig.getNoMertIterations();
	}

	/**
	 * Flag that specifies that the weights returning in the best
	 * development-set (BLEU) score should be used rather than the best weights
	 * according to the Tuner internal evaluation. See
	 * http://www.statmt.org/moses/?n=Moses.AdvancedFeatures#ntoc31 as reference
	 * why this is better.
	 * 
	 * @return
	 */
	protected String useBestScoringDevelopmentRunWeightsFlag() {
		return "--return-best-dev";
	}

	protected String getTunerWorkDirSpecification() {
		return "--working-dir="
				+ this.theConfig.foldersConfig.getMosesMertWorkDirFolder();
	}
	
	protected String getTunerWorkDirSpecificationWithoutFinalSlash() {
		String result = getTunerWorkDirSpecification();
		Assert.assertTrue(result.endsWith("/"));
		return result.substring(0,result.length()-1);
	}
	
	protected String getTunerScriptsRootDirSpecification() {
		return "--rootdir="
				+ this.theConfig.decoderConfig.getMosesScriptsDir();
	}

	protected String getMertOutSpecification() {
		return this.theConfig.foldersConfig.getMertRunSpecificFolder()
				+ MERT_OUT_FILE_NAME;
	}
	
	public String getMosesContinueTuningFromLastCompletedIterationFlag(){
	    return TUNER_CONTINUE_FLAG;
	}

	/*
	 * private String usePlainTextGrammarsFilterOption() { return "--filtercmd "
	 * + theConfig.decoderConfig.getMosesTrainingScriptsDir() +
	 * " -Hierarchical " + PLAIN_TEXT_GRAMMAR_FILTER_PROGRAM_NAME; }
	 */

	protected String noGrammarFilteringOption() {
		return "--no-filter-phrase-table";
	}

	protected String extraDecoderOptions() {
		// return " --decoder-flags=\"" + multiThreadingOption() + "\" ";
		return " --decoder-flags=\"" + multiThreadingOption() + "\" ";
	}

	protected String multiThreadingOption() {
		return "-threads " + theConfig.decoderConfig.getNumParallelDecoders();
	}

	protected String nBestListSizeSpecification() {
		return "--nbest=" + theConfig.decoderConfig.getNBestSizeTuning();
	}
	
	protected String getMiraRegularizationSpecification() {
		String regularizationValue = theConfig.decoderConfig
				.getMiraRegularizationPropertyValue();
		if (regularizationValue != null) {
			return "--batch-mira-args=\"" + "-"
					+ MOSES_MIRA_REGULARIZATION_PARAMETER + " "
					+ regularizationValue + "\"" + MosesTuningCommandCreater.PARAMETER_SEPARATOR;
		}
		return "";
	}

}
