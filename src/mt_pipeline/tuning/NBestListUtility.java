package mt_pipeline.tuning;

import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import util.FileUtil;

/**
 * Methods for extracting information from an NBest List
 * 
 * @author Gideon Maillette de Buy Wenniger
 * 
 */
public class NBestListUtility {
    private static final String JOSHUA_PHRASE_WEIGHT_PREFIX = "tm_pt";
    private static final String JOSHUA_OOV_PREFIX = "OOVPenalty";
    private static final String JOSHUA_LANGUAGE_MODEL_PREFIX = "lm";
    private static final String JOSHUA_WORD_PENALTY_PREFIX = "WordPenalty";
    private static final List<String> JOSHUA_STANDARD_FEATURES_PREFIXES_LIST = Arrays.asList(
	    JOSHUA_PHRASE_WEIGHT_PREFIX, JOSHUA_WORD_PENALTY_PREFIX, JOSHUA_OOV_PREFIX,
	    JOSHUA_LANGUAGE_MODEL_PREFIX);

    // See : http://www.regular-expressions.info/lookaround.html
    private static String featureFunctionMatchingRegularExpression(String featureFunctionName) {
	String result = featureFunctionName + ".+?" + "(?=\\=)";
	return result;
    }

    public static List<String> findAllFeatureOccurences(String contentsString,
	    String featureFunctionPrefix) {
	List<String> allMatches = findAllMatches(
		featureFunctionMatchingRegularExpression(featureFunctionPrefix), contentsString);
	return allMatches;
    }

    public static List<String> findAllMatches(String regularExpression, String contentsString) {
	List<String> allMatches = new ArrayList<String>();
	Matcher m = Pattern.compile(regularExpression).matcher(contentsString);
	while (m.find()) {
	    allMatches.add(m.group());
	}
	return allMatches;
    }

    private static String getLabelsSubString(String nBestListLine) {
	List<String> parts = Arrays.asList(JoshuaStyle.spitOnRuleSeparator(nBestListLine));
	return parts.get(2);
    }

    private static List<String> findAllFeatures(String contentsString) {
	List<String> result = new ArrayList<String>();
	List<String> featureStrings = Arrays.asList(JoshuaStyle
		.spitOnSingleWhiteSpace(getLabelsSubString(contentsString)));
	for (String featureString : featureStrings) {
	    String[] featureParts = featureString.split("=");
	    result.add(featureParts[0]);
	}
	return result;
    }

    private static Double getTotalWeightFromNBestLine(String nBestLine) {
	int firstIndexWeightSubstring = nBestLine
		.lastIndexOf(JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR)
		+ JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR.length();
	String weightSubstring = nBestLine.substring(firstIndexWeightSubstring);
	return Double.parseDouble(weightSubstring);
    }

    public static List<Double> getTotalWeightsFromNBestListString(String nBestListAsString) {
	List<Double> result = new ArrayList<Double>();
	String[] lines = nBestListAsString.split("\n");
	for (String line : lines) {
	    result.add(getTotalWeightFromNBestLine(line));
	}
	return result;

    }

    private static boolean startsWithIgnoreCase(String string1, String prefixString) {
	return (string1.toLowerCase()).startsWith(prefixString.toLowerCase());
    }

    private static boolean isStandardFeature(String featureString) {
	for (String standardFeaturePrefix : JOSHUA_STANDARD_FEATURES_PREFIXES_LIST) {
	    if (startsWithIgnoreCase(featureString, standardFeaturePrefix)) {
		return true;
	    }
	}
	return false;
    }

    public static Set<String> findAllNonStandardFeaturesFromNBestFile(String nBestListFilePath) {
	Set<String> allFeatures = new HashSet<String>();
	List<String> lines = FileUtil.getSentences(new File(nBestListFilePath), true);
	// Loop over all lines in the N-best list and collect all occuring
	// feature names
	for (String line : lines) {
	    allFeatures.addAll(findAllFeatures(line));
	}
	HashSet<String> result = new HashSet<String>();
	// Filter to get the non-standard features
	for (String featureName : allFeatures) {
	    if (!isStandardFeature(featureName)) {
		result.add(featureName);
	    }

	}
	return allFeatures;
    }
}
