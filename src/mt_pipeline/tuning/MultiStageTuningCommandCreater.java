package mt_pipeline.tuning;

import mt_pipeline.mt_config.MTConfigFile;

public abstract class MultiStageTuningCommandCreater extends TuningCommandCreater{

    protected MultiStageTuningCommandCreater(MTConfigFile theConfig) {
	super(theConfig);
    }

    public abstract String getFirstStageMultiStageTuningCommand();
    public abstract String getSecondStageMultiStageTuningCommand();
    
}
