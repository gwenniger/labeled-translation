package mt_pipeline.tuning;

import mt_pipeline.MTComponentCreater;
import mt_pipeline.mt_config.MTConfigFile;

public abstract class Tuner<T extends TuningCommandCreater> extends MTComponentCreater {

    protected final T tuningCommandCreater;

    protected Tuner(MTConfigFile theConfig, T tuningCommandCreater) {
	super(theConfig);
	this.tuningCommandCreater = tuningCommandCreater;
    }

    public static BasicTuner createBasicTuner(MTConfigFile theConfig,
	    TuningCommandCreater tuningCommandCreater) {
	return new BasicTuner(theConfig, tuningCommandCreater);
    }

    public abstract void runTuning();

    public String getTuningCommand() {
	return tuningCommandCreater.getTuningCommand();
    }

    public boolean useProTuning() {
	return (tuningCommandCreater instanceof JoshuaProCommandCreater);
    }

    public String getTuningCommandName() {
	return tuningCommandCreater.getTuningCommandName();
    }

    public static class BasicTuner extends Tuner<TuningCommandCreater> {

	private BasicTuner(MTConfigFile theConfig, TuningCommandCreater tuningCommandCreater) {
	    super(theConfig, tuningCommandCreater);
	}

	@Override
	public void runTuning() {
	    runExternalCommandUsingPreSpecifiedJavanBinDir(tuningCommandCreater.getTuningCommand(),
		    true);
	}
    }

    public static class MultiStageTuner extends Tuner<MultiStageTuningCommandCreater> {

	private MultiStageTuner(MTConfigFile theConfig,
		MultiStageTuningCommandCreater tuningCommandCreater) {
	    super(theConfig, tuningCommandCreater);
	}

	@Override
	public void runTuning() {
	    runExternalCommandUsingPreSpecifiedJavanBinDir(
		    tuningCommandCreater.getFirstStageMultiStageTuningCommand(), true);
	    runExternalCommandUsingPreSpecifiedJavanBinDir(
		    tuningCommandCreater.getSecondStageMultiStageTuningCommand(), true);

	}
    }

}
