package mt_pipeline.tuning;

import mt_pipeline.mt_config.MTConfigFile;

public abstract class TuningCommandCreater {
	
	protected final MTConfigFile theConfig;

	protected TuningCommandCreater(MTConfigFile theConfig) {
		this.theConfig = theConfig;
	}

	public abstract String getTuningCommand();

	public abstract String getTuningCommandName();
}
