package mt_pipeline.tuning;

import grammarExtraction.grammarExtractionFoundation.GrammarStructureSettings;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import mt_pipeline.JoshuaConfigCreater;

import java.io.BufferedWriter;

import mt_pipeline.mt_config.MTConfigFile;


/**
 * This class creates an initial empty sparse weights file, which is required by the 
 * tuner in the special case that that initial weights (from the first iteration) gave
 * the best results.
 * @author gemaille
 *
 */
public class SparseWeightsFileFirstIterationCreater extends TuningFileCreater {

    private static String RUN1_SPARSE_WEIGTHS_FILE_NAME = "run1.sparse-weights";
    private final boolean useSparseFeatures;

    protected SparseWeightsFileFirstIterationCreater(MTConfigFile theConfig,
	    BufferedWriter outputWriter, SystemIdentity systemIdentity) {
	super(theConfig, outputWriter, systemIdentity);
	this.useSparseFeatures = useLabelSubstitutionFeatures(theConfig);
    }

    public static boolean useLabelSubstitutionFeatures(MTConfigFile theConfig){
	return theConfig.decoderConfig.useLabelSubstitutionFeaturesWithConsistencyCheck();
    }
    
    private static String firstRunSparseWeightsFilePath(MTConfigFile theConfig) {
	return theConfig.foldersConfig.getMosesMertWorkDirFolder() + RUN1_SPARSE_WEIGTHS_FILE_NAME;
    }

    public static SparseWeightsFileFirstIterationCreater createSparseWeightsFileFirstIterationCreater(MTConfigFile theConfig,
	    SystemIdentity systemIdentity) {

	return new SparseWeightsFileFirstIterationCreater(theConfig,
		createOutputWriter(firstRunSparseWeightsFilePath(theConfig)), systemIdentity);
    }

    @Override
    protected String getFileContentsString() {
	if(this.useSparseFeatures){
	    return mt_pipeline.JoshuaConfigCreater.getOOVPenaltyMosesName() + " "  + mt_pipeline.JoshuaConfigCreater.getOOVPenaltyInitialWeight() + "\n";
	}
	return "";
    }

}
