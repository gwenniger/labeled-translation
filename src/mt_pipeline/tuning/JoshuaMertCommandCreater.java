package mt_pipeline.tuning;


import mt_pipeline.mt_config.MTConfigFile;

public class JoshuaMertCommandCreater extends TuningCommandCreater {

	private static final String MERT_TUNING_PROGRAM_NAME = "ZMERT";

	protected JoshuaMertCommandCreater(MTConfigFile theConfig) {
		super(theConfig);
	}

	public static JoshuaMertCommandCreater createJoshuaMertCommandCreater(MTConfigFile theConfig) {
		return new JoshuaMertCommandCreater(theConfig);
	}

	@Override
	public String getTuningCommand() {
		return mertMainCommand();
	}

	private String mertMainCommand() {
		// String result = "nohup " + theConfig.baseJoshuaJavaCall() +
		// " joshua.zmert.ZMERT  -maxMem 1500 " +
		// this.theConfig.getMertConfigFileName() ;
		String result = theConfig.decoderConfig.baseJoshuaJavaCall() + " joshua.zmert.ZMERT  -maxMem 1500 " + this.theConfig.filesConfig.getMertConfigFileName();
		return result;
	}

	@Override
	public String getTuningCommandName() {
		return MERT_TUNING_PROGRAM_NAME;
	}

}
