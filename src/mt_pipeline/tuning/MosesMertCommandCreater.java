package mt_pipeline.tuning;


import mt_pipeline.mt_config.MTConfigFile;

public class MosesMertCommandCreater extends MosesTuningCommandCreater {

	protected MosesMertCommandCreater(MTConfigFile theConfig,
			MosesBasedTuningProperties mosesBasedTuningProperties) {
		super(theConfig, mosesBasedTuningProperties);
	}

	public static MosesMertCommandCreater createMosesMertCommandCreater(
			MTConfigFile theConfig) {
		return new MosesMertCommandCreater(theConfig,
				MosesBasedTuningProperties
						.createMosesBasedTuningProperties(theConfig));
	}

	@Override
	protected String tuningAlgoirthmSpecification() {
		return "";
	}

	@Override
	protected String getTunerSpecificParameters() {
		return "";
	}
	
}
