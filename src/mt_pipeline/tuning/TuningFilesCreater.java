package mt_pipeline.tuning;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;

import java.io.IOException;

import mt_pipeline.mt_config.MTConfigFile;

public class TuningFilesCreater {

	public static void writeMertFiles(MTConfigFile theConfig, int noMertParameters,SystemIdentity systemIdentity) {
		try {
			MertConfigFileCreater mertConfigCreater = MertConfigFileCreater.createMertConfigFileCreater(theConfig, theConfig.filesConfig.getMertConfigFileName(),systemIdentity);
			mertConfigCreater.writeFile();

			TuningParametersFileCreater tuningParametersFileCreater = TuningParametersFileCreater.createTuningParametersFileCreater(theConfig, noMertParameters,systemIdentity);
			tuningParametersFileCreater.writeFile();

			MertDecodingCommandFileCreater.createExecutableMertCommandFile(theConfig,systemIdentity);
			
			SparseWeightsFileFirstIterationCreater sparseWeightsFileFirstIterationCreater = SparseWeightsFileFirstIterationCreater.createSparseWeightsFileFirstIterationCreater(theConfig, systemIdentity);
			sparseWeightsFileFirstIterationCreater.writeFile();
			

		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
}
