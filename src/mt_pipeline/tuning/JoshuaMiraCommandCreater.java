package mt_pipeline.tuning;

import util.FileUtil;
import mt_pipeline.mt_config.MTConfigFile;

public class JoshuaMiraCommandCreater extends MultiStageTuningCommandCreater {
	
	public static final String JOSHUA_TUNING_FINAL_CONFIG_NAME = "joshua.config.final";

	protected static final String PARAMETER_SEPARATOR = " ";

	// private static final String PLAIN_TEXT_GRAMMAR_FILTER_PROGRAM_NAME =
	// "filter-model-given-input.pl";

	private final MosesBasedTuningProperties mosesBasedTuningProperties;

	protected JoshuaMiraCommandCreater(MTConfigFile theConfig,
			MosesBasedTuningProperties mosesBasedTuningProperties) {
		super(theConfig);
		this.mosesBasedTuningProperties = mosesBasedTuningProperties;
	}

	public static JoshuaMiraCommandCreater createJoshuaMiraCommandCreater(
			MTConfigFile theConfig) {
		return new JoshuaMiraCommandCreater(theConfig,
				MosesBasedTuningProperties
						.createMosesBasedTuningProperties(theConfig));
	}

	public String getJoshuaMiraInterfaceProgramPath() {
		return theConfig.decoderConfig.getJoshuaMiraProgramPath();
	}

	private String getCopiedTuningConfigurationFilePath() {
	    	return mosesBasedTuningProperties.getConfigurationFilePath() + ".copyForJoshuaPerlScript";
	}

	private void makeCopyOfConfigSoJoshuaTuningCanOverwriteTheOld() {
	    	FileUtil.copyFileNative(mosesBasedTuningProperties.getConfigurationFilePath(),
	    		getCopiedTuningConfigurationFilePath(), true);
	}
	
	
	private String tuningConfigurationFileArgument()
	{
	   // if(theConfig.decoderConfig.restartTunerFromLastCompletedIteration()){
		//return "";
	    //}
	   // else{
		return PARAMETER_SEPARATOR + getCopiedTuningConfigurationFilePath();
	   // }
	}
	
	private String tuningContinueArgument()
	{
	    if(theConfig.decoderConfig.restartTunerFromLastCompletedIteration()){
		return PARAMETER_SEPARATOR + mosesBasedTuningProperties.getMosesContinueTuningFromLastCompletedIterationFlag();
	    }
	    else{
		return "";
	    }
	}
	
	
	
	private String joshuaMiraProgramCall() {

		String tuningAlgorithmSpecification = this
				.tuningAlgoirthmSpecification();
		if (tuningAlgorithmSpecification.length() > 0) {
			tuningAlgorithmSpecification = PARAMETER_SEPARATOR
					+ tuningAlgorithmSpecification;
		}
		
		// We need to make a copy of the configuration for tuning, since the
		// Joshua perl tuning script will overwrite the configuration file fed to it, but we
		// still want the original for evaluation of DEV-NO_Tuning
		makeCopyOfConfigSoJoshuaTuningCanOverwriteTheOld();

		
	
		/* See Moses documentation concerning usage multiple references:
		 * To specify multiple references to mert-moses.pl, name each reference file with a prefix followed by a number. 
		 * Pass the prefix as the reference and ensure that the prefix does not exist. 
		 */
		String result = getJoshuaMiraInterfaceProgramPath()
				+ PARAMETER_SEPARATOR
				+ mosesBasedTuningProperties.getDevSourceFile()
				+ PARAMETER_SEPARATOR
				+ mosesBasedTuningProperties.getReferenceFilePrefix()
				+ PARAMETER_SEPARATOR
				+ getDecoderCommand()
				+ tuningConfigurationFileArgument() 
				+ PARAMETER_SEPARATOR
				+ mosesBasedTuningProperties.extraDecoderOptions()
				+ tuningAlgorithmSpecification
				+ PARAMETER_SEPARATOR
				+ getTunerSpecificParameters()
				+ tuningContinueArgument()
				//+ mosesBasedTuningProperties.noGrammarFilteringOption()
				+ PARAMETER_SEPARATOR
				+ mosesBasedTuningProperties.nBestListSizeSpecification()
				+ PARAMETER_SEPARATOR
				+ mosesBasedTuningProperties
						.useBestScoringDevelopmentRunWeightsFlag()
				+ PARAMETER_SEPARATOR
				+ mosesBasedTuningProperties.specifyMaximumNumberOfIterations()
				+ PARAMETER_SEPARATOR
				+ mosesBasedTuningProperties.getMertDirSpecification()
				+ PARAMETER_SEPARATOR
				+ mosesBasedTuningProperties.getTunerScriptsRootDirSpecification()
				+ PARAMETER_SEPARATOR
				+ mosesBasedTuningProperties.getTunerWorkDirSpecificationWithoutFinalSlash();
		
		// Add the rescore-forest option only if set true in the configuration file
		if(theConfig.decoderConfig.useForestMiraJoshua()){
		    result += PARAMETER_SEPARATOR +"--rescore-forest";
		}
		    
		result += PARAMETER_SEPARATOR + " > "+ mosesBasedTuningProperties.getMertOutSpecification();
		return result;
	}
	
	
	
	public String getDecoderCommand() {
		// We need to specify the memory used for the /bin/decoder script
		return theConfig.decoderConfig.getJoshuaHomeDir() + "bin/decoder";
	}

	protected String getTunerSpecificParameters() {
		return mosesBasedTuningProperties.getMiraRegularizationSpecification();
	}

	protected String tuningAlgoirthmSpecification() {
		return "--batch-mira";
	}

	@Override
	public String getTuningCommand() {
		return joshuaMiraProgramCall();
	}

	@Override
	public String getTuningCommandName() {
		return "Joshua_Mira";
	}

	@Override
	public String getFirstStageMultiStageTuningCommand() {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public String getSecondStageMultiStageTuningCommand() {
	    // TODO Auto-generated method stub
	    return null;
	}
}
