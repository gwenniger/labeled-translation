package mt_pipeline.tuning;


import mt_pipeline.mt_config.MTConfigFile;

public class MosesMiraCommandCreater extends MiraCommandCreater {

	// private static String MOSES_MIRA_REGULARIZATION_PARAMETER = "C";

	protected MosesMiraCommandCreater(MTConfigFile theConfig,
			MosesBasedTuningProperties mosesBasedTuningProperties) {
		super(theConfig, mosesBasedTuningProperties);
	}

	public static MosesMiraCommandCreater createMosesMiraCommandCreater(
			MTConfigFile theConfig) {
		return new MosesMiraCommandCreater(theConfig,
				MosesBasedTuningProperties
						.createMosesBasedTuningProperties(theConfig));
	}
	

}
