package mt_pipeline;

import java.io.FileNotFoundException;
import util.ConfigFile;
import util.EmptyLineRemover;
import util.FileUtil;
import util.Pair;
import util.Utility;
import grammarExtraction.chiangGrammarExtraction.HieroGlueGrammarCreater;
import grammarExtraction.grammarExtractionFoundation.MultiThreadGrammarExtractor;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.grammarPacking.DiscriminativeFeaturesListFileCreater;
import grammarExtraction.grammarPacking.FeatureNameIndexFileCreater;
import grammarExtraction.grammarPacking.GrammarPackerInterface;
import grammarExtraction.phraseProbabilityWeights.BasicProbabilityWeightsSetCreater;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsSetCreater;
import grammarExtraction.phraseProbabilityWeights.WordEnrichmentSmoothedProbabilityWeightsSetCreater;
import grammarExtraction.samtGrammarExtraction.ComplexGlueGrammarCreater;
import grammarExtraction.samtGrammarExtraction.GlueGrammarCreater;
import grammarExtraction.translationRuleTrie.FeatureEnrichedGrammarRuleCreator;
import grammarExtraction.translationRuleTrie.GrammarWriter;
import grammarExtraction.translationRuleTrie.TranslationRuleProbabilityTable;
import mt_pipeline.mt_config.GrammarFilesLocations;
import mt_pipeline.mt_config.MTConfigFile;
import mt_pipeline.mt_config.MTGrammarExtractionConfig;

public abstract class GrammarCreaterHats extends GrammarCreater {
    public static String USE_WORD_ENRICHMENT_SMOOTHING_GRAMMAR_FEATURES_PARAMETER = "useWordEnrichmentSmoothingGrammarFeatures";

    private final MultiThreadGrammarExtracterCreater grammarExtracterCreater;
    protected final GrammarCreaterHatsConfigFileCreater configFileCreater;
    protected final GlueGrammarCreater glueGrammarCreater;

    protected GrammarCreaterHats(MTConfigFile theConfig, String grammarOutputPath, String category,
	    MultiThreadGrammarExtracterCreater grammarExtracterCreater,
	    GrammarCreaterHatsConfigFileCreater configFileCreater,
	    GlueGrammarCreater glueGrammarCreater, SystemIdentity systemIdentity) {
	super(theConfig, grammarOutputPath, category, systemIdentity);
	this.grammarExtracterCreater = grammarExtracterCreater;
	this.configFileCreater = configFileCreater;
	this.glueGrammarCreater = glueGrammarCreater;
    }

    protected abstract void writeGrammarExtractionConfigFile();

    protected abstract void extractGrammars();

    public static boolean useWordEnrichmentSmoothingGrammarFeatures(String configFilePath) {
	ConfigFile configFile;
	try {
	    configFile = new ConfigFile(configFilePath);
	    return useWordEnrichmentSmoothingGrammarFeatures(configFile);
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}

    }

    public static boolean useWordEnrichmentSmoothingGrammarFeatures(ConfigFile configFile) {
	return configFile.getBooleanIfPresentOrReturnDefault(
		USE_WORD_ENRICHMENT_SMOOTHING_GRAMMAR_FEATURES_PARAMETER, false);
    }

    public boolean useWordEnrichmentSmoothingGrammarFeatures() {
	return theConfig
		.getBooleanWithPresenceCheck(USE_WORD_ENRICHMENT_SMOOTHING_GRAMMAR_FEATURES_PARAMETER);
    }

    public TranslationRuleProbabilityTable extractGrammarMultiThread(
	    GrammarFilesLocations grammarFileLocations, int numThreads) {
	MultiThreadGrammarExtractor mainGrammarExtractor = grammarExtracterCreater
		.createGrammarExtracterTestFiltered(
			grammarFileLocations.getMainGrammarExtractionConfigFilePath(), numThreads);
	return mainGrammarExtractor.extractGrammarMultiThreadAndWriteResultToFile(
		rawGrammarFilePath(grammarFileLocations.getResultGrammarFilePath()),
		grammarFileLocations.getGapLabelConditionalProbabilityTableFilePath(),
		grammarExtracterCreater.writePlainHieroRules());
    }

    private Pair<TranslationRuleProbabilityTable> extractGrammarMultiThreadWithWordEnrichementSmoothingFeatures(
	    GrammarFilesLocations grammarFileLocations, int numThreads) {
	MultiThreadGrammarExtractor mainGrammarExtractor = grammarExtracterCreater
		.createGrammarExtracterTestFiltered(
			grammarFileLocations.getMainGrammarExtractionConfigFilePath(), numThreads);
	TranslationRuleProbabilityTable mainTable = mainGrammarExtractor
		.createTranslationRuleProbabilityTable();
	MultiThreadGrammarExtractor smoothingGrammarExtractor = grammarExtracterCreater
		.createGrammarExtracterTestFiltered(grammarFileLocations
			.getWordEnrichmentSmoothingGrammarExtractionConfigFilePath(), numThreads);
	TranslationRuleProbabilityTable wordEnrichmentSmoothedTable = smoothingGrammarExtractor
		.createTranslationRuleProbabilityTable();
	mainGrammarExtractor.writeGrammaWithWordEnrichmentSmoothingFeatures(mainTable,
		wordEnrichmentSmoothedTable,
		rawGrammarFilePath(grammarFileLocations.getResultGrammarFilePath()), true,
		grammarExtracterCreater.writePlainHieroRules());

	return new Pair<TranslationRuleProbabilityTable>(mainTable, wordEnrichmentSmoothedTable);
    }

    private String rawGrammarFilePath(String grammarFilePath) {
	return grammarFilePath + ".raw";
    }

    private void extractUnsortedGrammars(GrammarFilesLocations grammarFileLocations,
	    SystemIdentity systemIdentity) {
	TranslationRuleProbabilityTable translationRuleProbabilityTable;

	PhraseProbabilityWeightsSetCreater phraseProbabilityWeightsSetCreater;
	if (useWordEnrichmentSmoothingGrammarFeatures()) {
	    Pair<TranslationRuleProbabilityTable> tables = extractGrammarMultiThreadWithWordEnrichementSmoothingFeatures(
		    grammarFileLocations, theConfig.decoderConfig.getNumParallelGrammarExtractors());
	    translationRuleProbabilityTable = tables.getFirst();
	    phraseProbabilityWeightsSetCreater = WordEnrichmentSmoothedProbabilityWeightsSetCreater
		    .createWordEnrichmentSmoothedProbabilityWeightsSetCreater(
			    translationRuleProbabilityTable, tables.getSecond());

	} else {
	    translationRuleProbabilityTable = extractGrammarMultiThread(grammarFileLocations,
		    theConfig.decoderConfig.getNumParallelGrammarExtractors());
	    phraseProbabilityWeightsSetCreater = BasicProbabilityWeightsSetCreater
		    .createBasicProbabilityWeightsSetCreater(translationRuleProbabilityTable);
	    
	     // Show contents LabelProbabilityEstimator 
	    translationRuleProbabilityTable.getLabelProbabilityEstimator().showContentsForDebugging(translationRuleProbabilityTable.getWordKeyMappingTable());
	}
	FeatureEnrichedGrammarRuleCreator featureEnrichedGrammarRuleCreator = GrammarWriter
		.createFeatureEnrichedGrammarRuleCreator(translationRuleProbabilityTable,
			phraseProbabilityWeightsSetCreater, systemIdentity);

	this.glueGrammarCreater.writeGlueGrammar(translationRuleProbabilityTable,
		rawGrammarFilePath(grammarFileLocations.getGlueGrammarFilePath()),
		featureEnrichedGrammarRuleCreator);
	EmptyLineRemover.removeEmptyLinesInPlace(rawGrammarFilePath(grammarFileLocations
		.getGlueGrammarFilePath()));
    }

    private void sortGrammars(GrammarFilesLocations grammarFileLocations) {
	GrammarCreater.sortGrammar(
		rawGrammarFilePath(grammarFileLocations.getResultGrammarFilePath()),
		grammarFileLocations.getResultGrammarFilePath(),
		theConfig.foldersConfig.getIntermediateOutputFolderName(),
		theConfig.decoderConfig.getNumParallelGrammarExtractors());
	GrammarCreater.sortGrammar(
		rawGrammarFilePath(grammarFileLocations.getGlueGrammarFilePath()),
		grammarFileLocations.getGlueGrammarFilePath(),
		theConfig.foldersConfig.getIntermediateOutputFolderName(),
		theConfig.decoderConfig.getNumParallelGrammarExtractors());
	System.out.println("Done sorting grammars");
    }

    private void deleteUnsortedGrammars(GrammarFilesLocations grammarFileLocations) {
	Utility.deleteFile(rawGrammarFilePath(grammarFileLocations.getResultGrammarFilePath()));
	Utility.deleteFile(rawGrammarFilePath(grammarFileLocations.getGlueGrammarFilePath()));
    }

    protected void concatenateGlueGrammarToMainGrammar(
	    GrammarFilesLocations grammarAndGrammarSupportingFileLocations, ConfigFile configFile) {
	String appendToFilePath = rawGrammarFilePath(grammarAndGrammarSupportingFileLocations
		.getResultGrammarFilePath());
	String appendFilePath = rawGrammarFilePath(grammarAndGrammarSupportingFileLocations
		.getGlueGrammarFilePath());
	FileUtil.appendFile(appendToFilePath, appendFilePath);
    }

    protected void performGrammarExtractionAndSorting(
	    GrammarFilesLocations grammarAndGrammarSupportingFileLocations, ConfigFile configFile) {

	// SystemIdentity systemIdentity =
	// SystemIdentity.createSystemIdentity(configFile,
	// this.grammarExtracterCreater.getSystemName());
	extractUnsortedGrammars(grammarAndGrammarSupportingFileLocations, systemIdentity);

	if (systemIdentity.isMosesSystem()) {
	    concatenateGlueGrammarToMainGrammar(grammarAndGrammarSupportingFileLocations,
		    configFile);
	}

	sortGrammars(grammarAndGrammarSupportingFileLocations);
	deleteUnsortedGrammars(grammarAndGrammarSupportingFileLocations);
	System.out.println("Done performing grammar extraction and sorting");
    }

    @Override
    protected void extractSortedFilteredGrammar() {
	writeGrammarExtractionConfigFile();
	extractGrammars();
    }

    public static String getTestMainGrammarSourceFilterFileName(MTConfigFile theConfig) {
	return theConfig.filesConfig.getEnrichedSourceFilePath(MTConfigFile.TEST_CATEGORY);
    }

    public static String getDevMainGrammarSourceFilterFileName(MTConfigFile theConfig) {
	return theConfig.filesConfig.getEnrichedSourceFilePath(MTConfigFile.DEV_CATEGORY);
    }

    public static String getTestSmoothingGrammarSourceFilterFileName(MTConfigFile theConfig) {
	return theConfig.filesConfig
		.getSourceTokenizedAndLowerCasedFileName(MTConfigFile.TEST_CATEGORY);
    }

    public static String getDevSmoothingGrammarSourceFilterFileName(MTConfigFile theConfig) {
	return theConfig.filesConfig
		.getSourceTokenizedAndLowerCasedFileName(MTConfigFile.DEV_CATEGORY);
    }

    private static GlueGrammarCreater createTestGlueGrammarCreaterHiero(MTConfigFile theConfig,
	    MultiThreadGrammarExtracterCreater grammarExtracterCreaterHiero) {
	GlueGrammarCreater glueGrammarCreaterTest = HieroGlueGrammarCreater
		.createHieroGlueGrammarCreater(false,
			SystemIdentity.createSystemIdentity(theConfig));
	return glueGrammarCreaterTest;

    }

    private static GlueGrammarCreater createDevGlueGrammarCreaterHiero(MTConfigFile theConfig,
	    MultiThreadGrammarExtracterCreater grammarExtracterCreaterHiero) {
	GlueGrammarCreater glueGrammarCreaterDev = HieroGlueGrammarCreater
		.createHieroGlueGrammarCreater(false,
			SystemIdentity.createSystemIdentity(theConfig));
	return glueGrammarCreaterDev;
    }

    private static GlueGrammarCreater createDevGlueGrammarCreater(MTConfigFile theConfig,
	    MultiThreadGrammarExtracterCreater multiThreadGrammarExtracterCreater,
	    boolean useComplexLabels) {
	if (useComplexGlueRules(useComplexLabels, theConfig)) {
	    return createComplexGlueGrammarCreaterDev(theConfig, multiThreadGrammarExtracterCreater);
	} else {
	    return createDevGlueGrammarCreaterHiero(theConfig, multiThreadGrammarExtracterCreater);
	}
    }

    private static GlueGrammarCreater createTestGlueGrammarCreater(MTConfigFile theConfig,
	    MultiThreadGrammarExtracterCreater multiThreadGrammarExtracterCreater,
	    boolean useComplexLabels) {
	if (useComplexGlueRules(useComplexLabels, theConfig)) {
	    return createComplexGlueGrammarCreaterTest(theConfig,
		    multiThreadGrammarExtracterCreater);
	} else {
	    return createTestGlueGrammarCreaterHiero(theConfig, multiThreadGrammarExtracterCreater);
	}
    }

    /**
     * This generic method for extracting dev grammars can be reused for both
     * HAT labels and other complex labels, because the
     * GrammarCreaterHatsConfigFileCreater is added as an argument, as is the
     * parameter useComplex labels
     * 
     * @param theConfig
     * @param grammarExtracterCreaterHiero
     * @param systemIdentity
     * @param grammarCreaterHatsConfigFileCreater
     * @param useComplexLabels
     */
    public static void extractDevGrammarParallelGeneric(MTConfigFile theConfig,
	    MultiThreadGrammarExtracterCreater grammarExtracterCreaterHiero,
	    SystemIdentity systemIdentity,
	    GrammarCreaterHatsConfigFileCreater grammarCreaterHatsConfigFileCreater,
	    boolean useComplexLabels) {
	GrammarCreater devGrammarCreater = GrammarCreaterHatsDev.createDevGrammarCreater(
		theConfig,
		grammarExtracterCreaterHiero,
		grammarCreaterHatsConfigFileCreater,
		createDevGlueGrammarCreater(theConfig, grammarExtracterCreaterHiero,
			useComplexLabels), systemIdentity);

	devGrammarCreater.extractSortedFilteredGrammar();
    }

    /**
     * This generic method for extracting test grammars can be reused for both
     * HAT labels and other complex labels, because the
     * GrammarCreaterHatsConfigFileCreater is added as an argument, as is the
     * parameter useComplex labels
     * 
     * 
     * @param theConfig
     * @param grammarExtracterCreaterHiero
     * @param systemIdentity
     * @param grammarCreaterHatsConfigFileCreater
     * @param useComplexLabels
     */
    public static void extractTestGrammarParallelGeneric(MTConfigFile theConfig,
	    MultiThreadGrammarExtracterCreater grammarExtracterCreaterHiero,
	    SystemIdentity systemIdentity,
	    GrammarCreaterHatsConfigFileCreater grammarCreaterHatsConfigFileCreater,
	    boolean useComplexLabels) {
	GrammarCreater testGrammarCreater = GrammarCreaterHatsTest.createTestGrammarCreater(
		theConfig,
		grammarExtracterCreaterHiero,
		grammarCreaterHatsConfigFileCreater,
		createTestGlueGrammarCreater(theConfig, grammarExtracterCreaterHiero,
			useComplexLabels), systemIdentity);

	testGrammarCreater.extractSortedFilteredGrammar();
    }

    public static void extractFilteredHatsHieroGrammarsPrallel(MTConfigFile theConfig,
	    SystemIdentity systemIdentity) {

	MultiThreadGrammarExtracterCreater grammarExtracterCreaterHiero = GrammarExtracterCreaterHatsHiero
		.createExtracterCreaterHatsHiero(theConfig, systemIdentity);
	extractFilteredHAtsHieroGrammarsPrallel(theConfig, systemIdentity,
		grammarExtracterCreaterHiero);
    }

    public static void extractFilteredHatsHieroGrammarsPrallel(String configFilePath,
	    SystemIdentity systemIdentity) {
	MTConfigFile theConfig = MTConfigFile.createMtConfigFile(configFilePath,
		MTPipelineHatsHiero.SystemName);

	extractFilteredHatsHieroGrammarsPrallel(theConfig, systemIdentity);
    }

    /**
     * This generic grammar extraction method uses the arguments
     * grammarCreaterHatsConfigFileCreaterDev and
     * grammarCreaterHatsConfigFileCreaterTest to determine the behavior,
     * allowing it to be reuse for both HAT-labeled grammar extraction and also
     * extraction of other grammars including boundary-tagged grammars and SAMT
     * grammars
     * 
     * @param theConfig
     * @param systemIdentity
     * @param grammarExtracterCreaterHiero
     * @param grammarCreaterHatsConfigFileCreaterDev
     * @param grammarCreaterHatsConfigFileCreaterTest
     * @param useComplexLabels
     */
    public static void extractFilteredLabeledGrammarsPrallelGeneric(MTConfigFile theConfig,
	    SystemIdentity systemIdentity,
	    MultiThreadGrammarExtracterCreater grammarExtracterCreaterHiero,
	    GrammarCreaterHatsConfigFileCreater grammarCreaterHatsConfigFileCreaterDev,
	    GrammarCreaterHatsConfigFileCreater grammarCreaterHatsConfigFileCreaterTest,
	    boolean useComplexLabels) {

	checkDevAndMertFolderExist(theConfig);

	if (theConfig.extractTestGrammar()) {
	    extractTestGrammarParallelGeneric(theConfig, grammarExtracterCreaterHiero,
		    systemIdentity, grammarCreaterHatsConfigFileCreaterTest, useComplexLabels);
	} else {
	    System.out.println("Warning: not extracting test grammar, switched of in config file");
	}
	if (theConfig.extractDevGrammar()) {
	    extractDevGrammarParallelGeneric(theConfig, grammarExtracterCreaterHiero,
		    systemIdentity, grammarCreaterHatsConfigFileCreaterDev, useComplexLabels);
	} else {
	    System.out.println("Warning: not extracting dev grammar, switched of in config file");
	}
	writePackerSupportingFilesAndPackGrammarsIfRequired(theConfig);
    }

    private static GrammarCreaterHatsConfigFileCreater createGrammarCreaterHatsConfigFileCreaterDev(
	    MTConfigFile theConfig) {

	GrammarCreaterHatsConfigFileCreater grammarCreaterHatsConfigFileCreaterDev = GrammarCreaterHatsConfigFileCreater
		.createHieroGrammarCreaterHatsConfigFileCreater(GrammarCreaterHatsTest
			.getTestMainGrammarSourceFilterFileName(theConfig), GrammarCreaterHatsTest
			.getTestSmoothingGrammarSourceFilterFileName(theConfig), theConfig);
	return grammarCreaterHatsConfigFileCreaterDev;
    }

    private static GrammarCreaterHatsConfigFileCreater createGrammarCreaterHatsConfigFileCreaterTest(
	    MTConfigFile theConfig) {
	GrammarCreaterHatsConfigFileCreater grammarCreaterHatsConfigFileCreaterTest = GrammarCreaterHatsConfigFileCreater
		.createHieroGrammarCreaterHatsConfigFileCreater(
			GrammarCreaterHatsDev.getDevMainGrammarSourceFilterFileName(theConfig),
			GrammarCreaterHatsDev.getDevSmoothingGrammarSourceFilterFileName(theConfig),
			theConfig);
	return grammarCreaterHatsConfigFileCreaterTest;
    }

    public static void extractFilteredHAtsHieroGrammarsPrallel(MTConfigFile theConfig,
	    SystemIdentity systemIdentity,
	    MultiThreadGrammarExtracterCreater grammarExtracterCreaterHiero) {

	MTGrammarExtractionConfig grammarExtractionConfig = MTGrammarExtractionConfig
		.createMTGrammarExtractionConfig(theConfig);

	extractFilteredLabeledGrammarsPrallelGeneric(theConfig, systemIdentity,
		grammarExtracterCreaterHiero,
		createGrammarCreaterHatsConfigFileCreaterDev(theConfig),
		createGrammarCreaterHatsConfigFileCreaterTest(theConfig),
		grammarExtractionConfig.useReorderingLabelExtension());
    }

    public static void extractFilteredHatsSAMTGrammarsPrallel(String configFilePath,
	    SystemIdentity systemIdentity) {
	MTConfigFile theConfig = MTConfigFile.createMtConfigFile(configFilePath,
		MTPipelineHATsSAMT.SystemName);
	extractFilteredHatsSAMTGrammarsPrallel(theConfig, systemIdentity);
    }

    public static void extractFilteredHatsSAMTGrammarsPrallel(MTConfigFile theConfig,
	    SystemIdentity systemIdentity) {
	MultiThreadGrammarExtracterCreater grammarExtracterCreaterSAMT = GrammarExtracterCreaterHatsSAMT
		.createGrammarExtracterCreaterHatsSAMT(theConfig, systemIdentity);
	extractFilteredHatsComplexGrammarsPrallel(theConfig, grammarExtracterCreaterSAMT,
		systemIdentity);
    }

    public static void extractFilteredHatsBoundaryTagLabeledGrammarsPrallel(String configFilePath,
	    SystemIdentity systemIdentity) {
	MTConfigFile theConfig = MTConfigFile.createMtConfigFile(configFilePath,
		MTPipelineHatsBoundaryTagLabeled.SystemName);
	extractFilteredHatsBoundaryTagLabeledGrammarsPrallel(theConfig, systemIdentity);

    }

    public static void extractFilteredHatsBoundaryTagLabeledGrammarsPrallel(MTConfigFile theConfig,
	    SystemIdentity systemIdentity) {
	MultiThreadGrammarExtracterCreater grammarExtracterCreaterBoundaryTagLabeled = GrammarExtracterCreaterHatsBoundaryTagLabeled
		.createGrammarExtracterCreaterHatsBoundaryTagLabeled(theConfig, systemIdentity);
	extractFilteredHatsComplexGrammarsPrallel(theConfig,
		grammarExtracterCreaterBoundaryTagLabeled, systemIdentity);
    }

    public static void extractFilteredHatsCCLGrammarsPrallel(MTConfigFile theConfig,
	    SystemIdentity systemIdentity) {
	CoreContextLabelsTableExtracter.extractCoreContextLabelsTableIfNotPresent(theConfig);
	MultiThreadGrammarExtracterCreater grammarExtracterCreaterCCL = new GrammarExtracterCreaterHatsCCL();
	extractFilteredHatsComplexGrammarsPrallel(theConfig, grammarExtracterCreaterCCL,
		systemIdentity);
    }

    private static GlueGrammarCreater createComplexGlueGrammarCreaterDev(MTConfigFile theConfig,
	    MultiThreadGrammarExtracterCreater multiThreadGrammarExtracterCreater) {
	GlueGrammarCreater glueGrammarCreaterDev = ComplexGlueGrammarCreater
		.createComplexGlueGrammarCreater(
			multiThreadGrammarExtracterCreater.writePlainHieroRules(),
			SystemIdentity.createSystemIdentity(theConfig));
	return glueGrammarCreaterDev;
    }

    private static GlueGrammarCreater createComplexGlueGrammarCreaterTest(MTConfigFile theConfig,
	    MultiThreadGrammarExtracterCreater multiThreadGrammarExtracterCreater) {
	GlueGrammarCreater glueGrammarCreaterTest = ComplexGlueGrammarCreater
		.createComplexGlueGrammarCreater(
			multiThreadGrammarExtracterCreater.writePlainHieroRules(),
			SystemIdentity.createSystemIdentity(theConfig));
	return glueGrammarCreaterTest;
    }

    private static void extractFilteredHatsComplexGrammarsPrallel(MTConfigFile theConfig,
	    MultiThreadGrammarExtracterCreater multiThreadGrammarExtracterCreater,
	    SystemIdentity systemIdentity) {
	checkDevAndMertFolderExist(theConfig);

	GrammarCreaterHatsConfigFileCreater grammarCreaterHatsConfigFileCreaterTest = GrammarCreaterHatsConfigFileCreater
		.createComplexLabelsGrammarCreaterHatsConfigFileCreater(GrammarCreaterHatsTest
			.getTestMainGrammarSourceFilterFileName(theConfig), GrammarCreaterHatsTest
			.getTestSmoothingGrammarSourceFilterFileName(theConfig),
			theConfig.taggerConfig
				.getTaggerSourceOutputFileName(MTConfigFile.TEST_CATEGORY),
			theConfig);

	GrammarCreaterHatsConfigFileCreater grammarCreaterHatsConfigFileCreaterDev = GrammarCreaterHatsConfigFileCreater
		.createComplexLabelsGrammarCreaterHatsConfigFileCreater(GrammarCreaterHatsDev
			.getDevMainGrammarSourceFilterFileName(theConfig), GrammarCreaterHatsDev
			.getDevSmoothingGrammarSourceFilterFileName(theConfig),
			theConfig.taggerConfig
				.getTaggerSourceOutputFileName(MTConfigFile.DEV_CATEGORY),
			theConfig);

	extractFilteredLabeledGrammarsPrallelGeneric(theConfig, systemIdentity,
		multiThreadGrammarExtracterCreater, grammarCreaterHatsConfigFileCreaterDev,
		grammarCreaterHatsConfigFileCreaterTest, true);
    }

    private static void writePackerSupportingFilesAndPackGrammarsIfRequired(MTConfigFile theConfig) {

	SystemIdentity systemIdentity = SystemIdentity.createSystemIdentity(theConfig);

	if (systemIdentity.useReorderingBinaryFeatures() && systemIdentity.usePackedGrammars()) {
	    FeatureNameIndexFileCreater.createPackerSupportingFiles(theConfig, systemIdentity);

	    GrammarPackerInterface grammarPackerInterface = GrammarPackerInterface
		    .createGrammarPackerInterface(theConfig);
	    grammarPackerInterface.packGrammars();

	    DiscriminativeFeaturesListFileCreater discriminativeFeaturesListFileCreater = DiscriminativeFeaturesListFileCreater
		    .createDiscriminativeFeaturesListFileCreater(systemIdentity);
	    discriminativeFeaturesListFileCreater
		    .createDiscriminativeFeaturesListFile(theConfig.filesConfig
			    .getDiscriminativeFeaturesListFilePath());
	}
    }

    public String getDenseMapFilePath() {
	return theConfig.filesConfig.getDenseMapPath();
    }

    public String getPackerConfigFilePath() {
	return theConfig.filesConfig.getPackerConfigFilePath();
    }

}
