package mt_pipeline;

import mt_pipeline.mt_config.MTConfigFile;

public abstract class DecoderCommandCreater {
	protected final MTConfigFile theConfig;

	protected DecoderCommandCreater(MTConfigFile theConfig) {
		this.theConfig = theConfig;
	}

	public abstract String getDecodeCommand(String category, String configFilePath, boolean useTunedParameters);

	public static DecoderCommandCreater createDecoderCommandCreaterFromConfig(MTConfigFile theConfig) {
		return MTConfigFile.createSystemSpecificSupportObjectsFactory(theConfig).createDecoderCommandCreaterFromConfig(theConfig);
	}

}
