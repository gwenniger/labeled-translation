package mt_pipeline;

import grammarExtraction.grammarExtractionFoundation.MultiThreadGrammarExtractor;

public interface MultiThreadGrammarExtracterCreater 
{
	MultiThreadGrammarExtractor createGrammarExtracterTestFiltered(String configFilePath, int numThreads);
	public String getSystemName();
	public boolean writePlainHieroRules();

}
