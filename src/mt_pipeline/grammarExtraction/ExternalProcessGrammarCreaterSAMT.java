package mt_pipeline.grammarExtraction;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import mt_pipeline.GrammarCreaterHats;
import mt_pipeline.MTPipelineHATsSAMT;
import mt_pipeline.mt_config.MTConfigFile;

public class ExternalProcessGrammarCreaterSAMT extends ExternalProcessGrammarCreater {

	protected ExternalProcessGrammarCreaterSAMT(MTConfigFile theConfig) {
		super(theConfig);
	}

	@Override
	protected void extractGrammars(String configFilePath) {
		MTConfigFile mtConfigFile = MTConfigFile.createMtConfigFile(
				configFilePath, MTPipelineHATsSAMT.SystemName);
		SystemIdentity systemIdentity = SystemIdentity.createSystemIdentity(mtConfigFile);
		GrammarCreaterHats.extractFilteredHatsSAMTGrammarsPrallel(configFilePath, systemIdentity);
	}

	public static void extractSAMTGrammarsAsExternalProcess(String configFilePath) {

		ExternalProcessGrammarCreaterSAMT externalProcessGrammarCreaterSAMT = new ExternalProcessGrammarCreaterSAMT(MTConfigFile.createMtConfigFile(
				configFilePath, null));
		externalProcessGrammarCreaterSAMT.extractGrammarsAsExternalProcess(configFilePath);
	}

	public static void main(String[] args) {
		ExternalProcessGrammarCreaterSAMT grammarCreater = new ExternalProcessGrammarCreaterSAMT(null);
		grammarCreater.runGrammarExtraction(args);
		System.out.println("Finished main program  ExternalProcessGrammarCreaterSAMT");
		// Somehow without this the process is not exiting, perhaps it is not
		// clear why
		System.exit(0);
	}

}
