package mt_pipeline.grammarExtraction;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import mt_pipeline.GrammarCreaterHats;
import mt_pipeline.MTPipelineHatsBoundaryTagLabeled;
import mt_pipeline.mt_config.MTConfigFile;

public class ExternalProcessGrammarCreaterBoundaryTagLabeled extends ExternalProcessGrammarCreater {

	protected  ExternalProcessGrammarCreaterBoundaryTagLabeled(MTConfigFile theConfig) {
		super(theConfig);
	}

	@Override
	protected void extractGrammars(String configFilePath) {
		MTConfigFile mtConfigFile = MTConfigFile.createMtConfigFile(
				configFilePath, MTPipelineHatsBoundaryTagLabeled.SystemName);
		SystemIdentity systemIdentity = SystemIdentity.createSystemIdentity(mtConfigFile);
		GrammarCreaterHats.extractFilteredHatsBoundaryTagLabeledGrammarsPrallel(configFilePath,systemIdentity);
	}

	public static void extractBoundaryTaggedLabeledGrammarsAsExternalProcess(String configFilePath) {

		MTConfigFile mtConfigFile = MTConfigFile.createMtConfigFile(
				configFilePath, null);
		ExternalProcessGrammarCreaterBoundaryTagLabeled externalProcessGrammarCreaterBoundaryTagLabeled = new ExternalProcessGrammarCreaterBoundaryTagLabeled(mtConfigFile);
		externalProcessGrammarCreaterBoundaryTagLabeled.extractGrammarsAsExternalProcess(configFilePath);
	}

	public static void main(String[] args) {
		ExternalProcessGrammarCreaterBoundaryTagLabeled grammarCreater = new ExternalProcessGrammarCreaterBoundaryTagLabeled(null);
		grammarCreater.runGrammarExtraction(args);
		System.out.println("Finished main program  ExternalProcessGrammarCreaterBoundaryTagLabeled");
		// Somehow without this the process is not exiting, perhaps it is not
		// clear why
		System.exit(0);
	}

}