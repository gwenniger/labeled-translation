package mt_pipeline.grammarExtraction;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import mt_pipeline.MTComponentCreater;
import mt_pipeline.mt_config.MTConfigFile;

public abstract class ExternalProcessGrammarCreater extends MTComponentCreater {

	protected ExternalProcessGrammarCreater(MTConfigFile theConfig) {
		super(theConfig);
	}

	protected void checkArgs(String[] args) {
		if (args.length != 1) {
			System.out.println("Error: expected one argument with config file location, got: " + args);
			System.exit(1);
		}
	}

	protected void runGrammarExtraction(String[] args) {
		checkArgs(args);
		extractGrammars(args[0]);
	}

	protected abstract void extractGrammars(String configFilePath);

	protected String getClassName() {
		return this.getClass().getName();
	}

	protected String getClassPath() {
		String classPath = System.getProperty("java.class.path", ".") + getClassName();
		return classPath;
	}

	protected boolean isRunningInJar() {
		String resourceString = ExternalProcessGrammarCreater.class.getResource("ExternalProcessGrammarCreater.class").toString();
		return resourceString.contains("jar");
	}

	/**
	 * This method returns the path of the current jar file that is been run See
	 * : http://stackoverflow.com/questions/320542/how-to-get-the-path-of-a-
	 * running-jar-file
	 * 
	 * @return
	 */
	public String pathCurrentJarFile() {

		String path = ExternalProcessGrammarCreater.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		try {
			String decodedPath = URLDecoder.decode(path, "UTF-8");
			return decodedPath;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public String jarClassPathSuffix() {
		if (isRunningInJar()) {
			System.out.println("path current jar file: " + pathCurrentJarFile());
			return " -cp " + pathCurrentJarFile() + " ";
		}
		return " ";
	}

	protected String getExtractionCommand(String configFilePath) {
		String command = theConfig.baseJavaCall() + jarClassPathSuffix() + getClassName() + " " + configFilePath;
		return command;
	}

	public void extractGrammarsAsExternalProcess(String configFilePath) {
		String command = this.getExtractionCommand(configFilePath);
		System.out.println("Extraction command: " + command);
		boolean error = this.runExternalCommandUsingPreSpecifiedJavanBinDir(command, true, this.getClassPath());
		if (error) {
			throw new RuntimeException();
		}
		System.out.println("Finished  extractrammarsAsExternalProcess");
	}

}
