package mt_pipeline.grammarExtraction;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import mt_pipeline.GrammarCreaterHats;
import mt_pipeline.MTPipelineHatsHiero;
import mt_pipeline.mt_config.MTConfigFile;

public class ExternalProcessGrammarCreaterHiero extends ExternalProcessGrammarCreater {

	protected ExternalProcessGrammarCreaterHiero(MTConfigFile theConfig) {
		super(theConfig);
	}

	@Override
	protected void extractGrammars(String configFilePath) {
		MTConfigFile mtConfigFile = MTConfigFile.createMtConfigFile(
				configFilePath, MTPipelineHatsHiero.SystemName);
		SystemIdentity systemIdentity = SystemIdentity.createSystemIdentity(mtConfigFile);
		GrammarCreaterHats.extractFilteredHatsHieroGrammarsPrallel(configFilePath,systemIdentity);
	}

	public static void extractHieroGrammarsAsExternalProcess(String configFilePath) {

	
		MTConfigFile theConfig = MTConfigFile.createMtConfigFile(
				configFilePath, null);
		ExternalProcessGrammarCreaterHiero externalProcessGrammarCreaterHiero = new ExternalProcessGrammarCreaterHiero(theConfig);
		externalProcessGrammarCreaterHiero.extractGrammarsAsExternalProcess(configFilePath);
	}

	public static void main(String[] args) {
		ExternalProcessGrammarCreaterHiero grammarCreater = new ExternalProcessGrammarCreaterHiero(null);
		grammarCreater.runGrammarExtraction(args);
		System.out.println("Finished main program  ExternalProcessGrammarCreaterHiero");
		// Somehow without this the process is not exiting, perhaps it is not
		// clear why
		System.exit(0);
	}

}