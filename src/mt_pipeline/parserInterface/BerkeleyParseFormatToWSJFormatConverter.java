package mt_pipeline.parserInterface;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import mt_pipeline.preAndPostProcessing.ExtraBracketsRemover;

public class BerkeleyParseFormatToWSJFormatConverter {

	private static final String RootString = "(ROOT ";
	private static final String SentenceTopBerkeleyFormat = "(SENT";
	private static final String SentenceTopWSJFormat = "(S";
	public static final String ParseErrorString = "PARSE-ERROR";

	private static String removeLastClosingBracket(String string) {
		String result = string.substring(0, string.lastIndexOf(")") - 1);
		assert (result.length() < string.length());
		return result;
	}

	public static void convertToWSJFormat(String inputFileName, String outputFileName) {
		try {
			BufferedReader fileReader = new BufferedReader(new FileReader(inputFileName));
			BufferedWriter fileWriter = new BufferedWriter(new FileWriter(outputFileName));

			String line;
			while ((line =fileReader.readLine()) != null) {

				line = ExtraBracketsRemover.removeExtraBrackets(line);
				
				if(line.equals("()"))
				{
					fileWriter.write(ParseErrorString + "\n");
				}
				else
				{	
					String convertedString = line;
	
					if (line.contains(RootString)) {
						convertedString = line.replace(RootString, "");
						convertedString = removeLastClosingBracket(convertedString);
					}
	
					convertedString = convertedString.replace(SentenceTopBerkeleyFormat, SentenceTopWSJFormat);
					fileWriter.write(convertedString + "\n");
				}
			}

			fileReader.close();
			fileWriter.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
