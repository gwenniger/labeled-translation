package mt_pipeline.parserInterface;

import java.util.ArrayList;
import java.util.List;

import util.FilePartMerger;
import util.FileUtil;
import mt_pipeline.mt_config.MTConfigFile;

public class CharniakParserInterface extends ParserInterface<CharniakParserProcess> {

	private static final String CharniakParserName = "CharniakParser";

	protected CharniakParserInterface(MTConfigFile theConfig, ParserInputFilePreprocessor parserInputFilePreprocessor, String baseInputFileName, String parseOutputFileName, String parseSubFileName) {
		super(theConfig, parserInputFilePreprocessor, baseInputFileName, parseOutputFileName, parseSubFileName);
	}

	private static CharniakParserInterface createCharniakParserInterface(MTConfigFile theConfig, String baseInputFileName, String parseOutputFileName, String parseSubFileName) {
		return new CharniakParserInterface(theConfig, new CharniakParserInputFilePreprocessor(), baseInputFileName, parseOutputFileName, parseSubFileName);
	}

	public static List<MetaDataAnnotatorInterface<? extends MetaDataAnnotatorProcess<?>>> createParsers(MTConfigFile theConfig) {
		List<MetaDataAnnotatorInterface<? extends MetaDataAnnotatorProcess<?>>> result = new ArrayList<MetaDataAnnotatorInterface<? extends MetaDataAnnotatorProcess<?>>>();
		if (theConfig.parserConfig.parseSourceFile()) {
			result.add(createCharniakParserInterface(theConfig, ParserInterface.getSourceBaseInputFileName(theConfig), ParserInterface.getSourceParseOutputFileName(theConfig),
					ParserInterface.getSourceParseSubFilePath(theConfig)));
		}
		if (theConfig.parserConfig.parseTargetFile()) {
			result.add(createCharniakParserInterface(theConfig, ParserInterface.getTargetBaseInputFileName(theConfig), ParserInterface.getTargetParseOutputFileName(theConfig),
					ParserInterface.getTargetParseSubFilePath(theConfig)));
		}

		return result;
	}

	@Override
	protected CharniakParserProcess createMetaDataCreatorProcess(String partFileName) {
		return new CharniakParserProcess(theConfig, partFileName);
	}

	public static String getParserName() {
		return CharniakParserName;
	}

	private void renameResultFile(String mergeResultFileName, String baseInputFileName) {
		FileUtil.renameFile(mergeResultFileName, theConfig.parserConfig.getParseOutputFileName(baseInputFileName, getParserName()));
	}

	@Override
	protected void mergePartResultsAndProduceFinalResult(String baseInputFileName, int numParts) {
		String mergeResultName = theConfig.parserConfig.getParseOutputFileNameForParserFormat(baseInputFileName, CharniakParserProcess.CharniakFormatSuffix);
		FilePartMerger.produceMergedPartsFile(mergeResultName, numParts);
		FilePartMerger.deletePartFiles(baseInputFileName, numParts);
		renameResultFile(mergeResultName, baseInputFileName);
	}

}
