package mt_pipeline.parserInterface;

import java.util.concurrent.Callable;

import mt_pipeline.MTComponentCreater;
import mt_pipeline.mt_config.MTConfigFile;

public abstract class MetaDataAnnotatorProcess<T extends MetaDataAnnotatorProcess<T>> extends MTComponentCreater  implements Callable<T>
{
	protected final String partFileName;

	protected MetaDataAnnotatorProcess(MTConfigFile theConfig,String partFileName) {
		super(theConfig);
		this.partFileName = partFileName;
	}
}
