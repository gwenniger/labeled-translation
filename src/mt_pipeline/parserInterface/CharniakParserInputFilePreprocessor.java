package mt_pipeline.parserInterface;

import util.ParseAndTagBracketAndLabelConverter;

public class CharniakParserInputFilePreprocessor extends ParserInputFilePreprocessor{

	private String addCharniakSentenceTags(String line)
	{
		return "<s> " + line + " </s>";
	}
	@Override
	public String createPreprocessedLine(String line) {
		return addCharniakSentenceTags(ParseAndTagBracketAndLabelConverter.getStringWithReplacedBrackets(line));
	}

}
