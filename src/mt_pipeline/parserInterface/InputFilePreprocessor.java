package mt_pipeline.parserInterface;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public abstract class InputFilePreprocessor {

	public void createPreprocessedInputFile(String inputFileName, String outputFileName) {
		try {
			BufferedReader fileReader = new BufferedReader(new FileReader(inputFileName));
			BufferedWriter fileWriter = new BufferedWriter(new FileWriter(outputFileName));

			String line;
			while ((line = fileReader.readLine()) != null) {
				String convertedString = createPreprocessedLine(line);
				fileWriter.write(convertedString + "\n");
			}

			fileReader.close();
			fileWriter.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public abstract String createPreprocessedLine(String line);
	
}
