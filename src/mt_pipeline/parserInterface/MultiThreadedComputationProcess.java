package mt_pipeline.parserInterface;

import java.util.concurrent.Callable;

public abstract class MultiThreadedComputationProcess<T extends MultiThreadedComputationProcess<T>>
	implements Callable<T> {
    protected final String partFileName;

    protected MultiThreadedComputationProcess(String partFileName) {
	this.partFileName = partFileName;
    }
}