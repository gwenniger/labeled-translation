package mt_pipeline.parserInterface;

import mt_pipeline.mt_config.MTConfigFile;

public class CharniakParserProcess extends MetaDataAnnotatorProcess<CharniakParserProcess> {

	public static final String CharniakFormatSuffix = ".CharniakFormat";
	
	protected CharniakParserProcess(MTConfigFile theConfig, String partFileName) {
		super(theConfig, partFileName);
	}

	@Override
	public CharniakParserProcess call() throws Exception {
		this.runExternalCommandUsingPreSpecifiedJavanBinDir(getParseCommand(partFileName), false);
		return this;
	}

	private String charniakParseCommand(String inputFileName) {
		String parserDir = theConfig.parserConfig.getCharniakParserRootDir();
		String modelDir = parserDir + "second-stage/models/ec50spfinal";
		String estimatorNickName = "cvlm-l1c10P1";
		String parseProgramCall = parserDir + "first-stage/PARSE/parseIt";		
		String firstStageParseCommand = parseProgramCall + " -l399 -N50 " + parserDir +"first-stage/DATA/EN/ " + inputFileName; 
		String secondStageParseCommand = parserDir + "second-stage/programs/features/best-parses -l " + 
				modelDir + "/features.gz " +modelDir + "/" + estimatorNickName +"-weights.gz";
		System.out.println("second Statge parse command: " + secondStageParseCommand);
		String parseCommand = firstStageParseCommand + "  | " + secondStageParseCommand;
		return parseCommand;
	}

	public String getParseCommand(String inputFileName) {
		String result = charniakParseCommand(inputFileName) + " > " + theConfig.parserConfig.getParsePartOutputFileNameBerkeleyFormat(inputFileName,CharniakFormatSuffix);
		return result;
	}

}
