package mt_pipeline.parserInterface;

import mt_pipeline.mt_config.MTConfigFile;

public class BerkeleyParserProcess extends MetaDataAnnotatorProcess<BerkeleyParserProcess> {
	public static final String BerkeleyFormatSuffix = ".BerkeleyFormat";
	
	private final String grammarFileName;

	protected BerkeleyParserProcess(MTConfigFile theConfig, String partFileName, String grammarFileName) {
		super(theConfig, partFileName);
		this.grammarFileName = grammarFileName;
	}

	@Override
	public BerkeleyParserProcess call() throws Exception {
		this.runExternalCommandUsingPreSpecifiedJavanBinDir(getParseCommand(partFileName, grammarFileName), false);
		return this;
	}

	public String getParseCommand(String inputFileName, String grammarFileName) {
		String result = theConfig.baseJavaCall();
		result += " -jar " + theConfig.parserConfig.getBerkeleyParserJarLocation() + getGrammarSpecification(grammarFileName)
				+ getParseInputFileSpecification(inputFileName) + getParseOutputFileSpecification(inputFileName);
		return result;
	}

	private String getParseInputFileSpecification(String inputFileName) {
		return " -inputFile " + inputFileName;
	}

	private String getParseOutputFileSpecification(String inputFileName) {
		return " -outputFile " + theConfig.parserConfig.getParsePartOutputFileNameBerkeleyFormat(inputFileName,BerkeleyFormatSuffix);
	}

	private String getGrammarSpecification(String grammarFileName) {
		return " -gr " + grammarFileName;
	}
	
	

}
