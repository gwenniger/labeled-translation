package mt_pipeline.parserInterface;

import java.util.ArrayList;
import java.util.List;

import mt_pipeline.MTComponentCreater;
import mt_pipeline.mt_config.MTConfigFile;
import mt_pipeline.mt_config.MTParserConfig.ParserType;
import mt_pipeline.preAndPostProcessing.SubFileCreater;

public abstract class ParserInterface<T extends MetaDataAnnotatorProcess<T>> extends MetaDataAnnotatorInterface<T> {

	private final String parseOutputFileName;
	private final String parseSubFileName;

	protected ParserInterface(MTConfigFile theConfig, InputFilePreprocessor inputFilePreprocessor, String baseInputFileName, String parseOutputFileName, String parseSubFileName) {
		super(theConfig, inputFilePreprocessor, baseInputFileName);
		this.parseOutputFileName = parseOutputFileName;
		this.parseSubFileName = parseSubFileName;
	}

	protected boolean parseResultFileNameAlreadyExistsAndRightSize(String baseInputFileName) {
		String resultFileName = theConfig.parserConfig.getParseOutputFileName(baseInputFileName, theConfig.parserConfig.getParserNameString());
		return MTComponentCreater.fileAlreadyExistsAndRigthSize(baseInputFileName, resultFileName);
	}

	private void parseFilesMultipleProcesses() {

		if (parseResultFileNameAlreadyExistsAndRightSize(baseInputFileName)) {
			System.out.println("NOTE : A parse result file with the right name and size already exists. Keeping that file and skipping reparsing");
		} else {
			createMetaDataFilesMultipleProcesses();
		}
	}

	protected void createPreprocessedInputFiles() {
		inputFilePreprocessor.createPreprocessedInputFile(theConfig.parserConfig.getEnrichmentProducerSourceInputFilePath(MTConfigFile.TRAIN_CATEGORY),
				theConfig.parserConfig.getSourceInputFilePreprocessedPath(MTConfigFile.TRAIN_CATEGORY));
		inputFilePreprocessor.createPreprocessedInputFile(theConfig.parserConfig.getEnrichmentProducerTargetInputFilePath(MTConfigFile.TRAIN_CATEGORY),
				theConfig.parserConfig.getTargetInputFilePreprocessedPathTrain());
	}

	protected void createMetaDataAnnotationFilesFromPreprocessedInputFiles() {
		if (theConfig.parserConfig.parseSourceFile()) {
			// parseFilesMultipleProcesses(theConfig.parserConfig.getSourceInputFilePreprocessedPath(MTConfigFile.TRAIN_CATEGORY));
			parseFilesMultipleProcesses();
		}

		if (theConfig.parserConfig.parseTargetFile()) {
			// parseFilesMultipleProcesses(theConfig.parserConfig.getTargetInputFilePreprocessedPath(MTConfigFile.TRAIN_CATEGORY));
			parseFilesMultipleProcesses();
		}
	}

	public void parseFiles() {
		System.out.println("ParserInterface parseFiles() called...");
		createMetaDataAnnotationBaseFiles();
	}

	public static List<MetaDataAnnotatorInterface<? extends MetaDataAnnotatorProcess<?>>> createParsers(MTConfigFile theConfig) {
		ParserType parserType = theConfig.parserConfig.getParserType();
		if (parserType.equals(ParserType.BERKELEY_PARSER)) {
			return BerkeleyParserInterface.createParsers(theConfig);
		} else if (parserType.equals(ParserType.CHARNIAK_PARSER)) {
			return CharniakParserInterface.createParsers(theConfig);
		} else {
			throw new RuntimeException("Error : unrecognize parser type");
		}
	}

	@Override
	protected void createMetaDataAnnotationBaseFiles() {
		createPreprocessedInputFiles();
		createMetaDataAnnotationFilesFromPreprocessedInputFiles();
	}

	protected static String getSourceBaseInputFileName(MTConfigFile theConfig) {
		return theConfig.parserConfig.getSourceInputFilePreprocessedPath(MTConfigFile.TRAIN_CATEGORY);
	}

	protected static String getTargetBaseInputFileName(MTConfigFile theConfig) {
		return theConfig.parserConfig.getTargetInputFilePreprocessedPathTrain();
	}

	public static String getSourceParseOutputFileName(MTConfigFile theConfig) {
		return theConfig.parserConfig.getSourceParseOutputFileName(theConfig.parserConfig.getParserNameString());
	}

	public static String getSourceParseSubFilePath(MTConfigFile theConfig) {
		return theConfig.filesConfig.getSourceParseSubFilePath(MTConfigFile.TRAIN_CATEGORY);
	}

	public static String getTargetParseOutputFileName(MTConfigFile theConfig) {
		return theConfig.parserConfig.getTargetParseOutputFileName();
	}

	public static String getTargetParseSubFilePath(MTConfigFile theConfig) {
		return theConfig.filesConfig.getTargetParseSubFilePath(MTConfigFile.TRAIN_CATEGORY);
	}

	@Override
	protected List<String> createMetaDataAnnotationSubFiles() {

		List<String> result = new ArrayList<String>();
		SubFileCreater.createSubFile(this.parseOutputFileName, this.parseSubFileName, theConfig.decoderConfig.getDataLength(MTConfigFile.TRAIN_CATEGORY));
		/*
		 * if (theConfig.parserConfig.parseSourceFile()) {
		 * SubFileCreater.createSubFile(theConfig.parserConfig.getSourceParseOutputFileName(theConfig.parserConfig.getParserNameString()),
		 * theConfig.filesConfig.getSourceParseSubFilePath(MTConfigFile.TRAIN_CATEGORY), theConfig.joshuaConfig.genNumOutputLines(MTConfigFile.TRAIN_CATEGORY));
		 * } if (theConfig.parserConfig.parseTargetFile()) {
		 * SubFileCreater.createSubFile(theConfig.parserConfig.getTargetParseOutputFileName(theConfig.parserConfig.getParserNameString()),
		 * theConfig.filesConfig.getTargetParseSubFilePath(MTConfigFile.TRAIN_CATEGORY), theConfig.joshuaConfig.genNumOutputLines(MTConfigFile.TRAIN_CATEGORY));
		 * }
		 */
		result.add(this.parseSubFileName);
		return result;
	}
}
