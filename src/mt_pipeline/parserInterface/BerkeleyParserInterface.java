package mt_pipeline.parserInterface;

import java.util.ArrayList;
import java.util.List;

import util.FilePartMerger;
import mt_pipeline.mt_config.MTConfigFile;

public class BerkeleyParserInterface extends ParserInterface<BerkeleyParserProcess> {
	private static final String BerkeleyParserName = "BerkeleyParser";
	private final String grammarFileName;

	protected BerkeleyParserInterface(MTConfigFile theConfig, ParserInputFilePreprocessor parserInputFilePreprocessor, String baseInputFileName, String parseOutputFileName, String parseSubFileName,
			String grammarFileName) {
		super(theConfig, parserInputFilePreprocessor, baseInputFileName, parseOutputFileName, parseSubFileName);
		this.grammarFileName = grammarFileName;
	}

	private static BerkeleyParserInterface createBerkeleyParserInterface(MTConfigFile theConfig, String baseInputFileName, String parseOutputFileName, String parseSubFileName, String grammarLocation) {
		return new BerkeleyParserInterface(theConfig, new BerkeleyParserInputFilePreprocessor(), baseInputFileName, parseOutputFileName, parseSubFileName, grammarLocation);
	}

	public static List<MetaDataAnnotatorInterface<? extends MetaDataAnnotatorProcess<?>>> createParsers(MTConfigFile theConfig) {
		 List<MetaDataAnnotatorInterface<? extends MetaDataAnnotatorProcess<?>>>  result = new ArrayList<MetaDataAnnotatorInterface<? extends MetaDataAnnotatorProcess<?>>>();
		if (theConfig.parserConfig.parseSourceFile()) {
			result.add(createBerkeleyParserInterface(theConfig, ParserInterface.getSourceBaseInputFileName(theConfig), ParserInterface.getSourceParseOutputFileName(theConfig),
					ParserInterface.getSourceParseSubFilePath(theConfig), theConfig.parserConfig.getBerkeleyParserSourceGrammarLocation()));
		}
		if (theConfig.parserConfig.parseTargetFile()) {
			result.add(createBerkeleyParserInterface(theConfig, ParserInterface.getTargetBaseInputFileName(theConfig), ParserInterface.getTargetParseOutputFileName(theConfig),
					ParserInterface.getTargetParseSubFilePath(theConfig), theConfig.parserConfig.getBerkeleyParserTargetGrammarLocation()));
		}

		return result;
	}

	public static String getParserName() {
		return BerkeleyParserName;
	}

	@Override
	protected BerkeleyParserProcess createMetaDataCreatorProcess(String partFileName) {
		return new BerkeleyParserProcess(theConfig, partFileName, grammarFileName);
	}

	@Override
	protected void mergePartResultsAndProduceFinalResult(String baseInputFileName, int numParts) {
		FilePartMerger.produceMergedPartsFile(theConfig.parserConfig.getParseOutputFileNameForParserFormat(baseInputFileName, BerkeleyParserProcess.BerkeleyFormatSuffix), numParts);
		FilePartMerger.deletePartFiles(baseInputFileName, numParts);
		BerkeleyParseFormatToWSJFormatConverter.convertToWSJFormat(theConfig.parserConfig.getParseOutputFileNameForParserFormat(baseInputFileName, BerkeleyParserProcess.BerkeleyFormatSuffix),
				theConfig.parserConfig.getParseOutputFileName(baseInputFileName, getParserName()));
	}

}
