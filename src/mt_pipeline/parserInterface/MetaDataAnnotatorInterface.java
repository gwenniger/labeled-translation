package mt_pipeline.parserInterface;

import java.util.List;
import mt_pipeline.mt_config.MTConfigFile;
import util.ParseAndTagBracketAndLabelConverter;

public abstract class MetaDataAnnotatorInterface<T extends MetaDataAnnotatorProcess<T>> extends
	MultiThreadedExecutionInterface<T> {

    protected final MTConfigFile theConfig;

    protected MetaDataAnnotatorInterface(MTConfigFile theConfig,
	    InputFilePreprocessor inputFilePreprocessor, String baseInputFileName) {
	super(inputFilePreprocessor, baseInputFileName);
	this.theConfig = theConfig;
    }

    @Override
    public void createResultFilesMutltiThreaded() {
	createMetaDataAnnotationBaseFiles();
	createPostProcessdeMetaDataAnnotationSubFiles();
    }

    @Override
    protected int getNumberOfThreads() {
	int numParts = theConfig.parserConfig.getNumParserProcessesSpecification();
	return numParts;
    }

    protected abstract void createMetaDataAnnotationBaseFiles();

    /**
     * 
     * @return List of the meta data annotation file paths that have been
     *         created
     */
    protected abstract List<String> createMetaDataAnnotationSubFiles();

    protected void createPostProcessdeMetaDataAnnotationSubFiles() {
	List<String> createdFileNames = createMetaDataAnnotationSubFiles();
	for (String fileName : createdFileNames) {
	    ParseAndTagBracketAndLabelConverter.convertAllXLabelsInFile(fileName);
	}
    }
}
