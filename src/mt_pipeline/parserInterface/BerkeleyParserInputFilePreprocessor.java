package mt_pipeline.parserInterface;

import util.ParseAndTagBracketAndLabelConverter;

public class BerkeleyParserInputFilePreprocessor extends ParserInputFilePreprocessor {

	@Override
	public String createPreprocessedLine(String line) {
		return ParseAndTagBracketAndLabelConverter.getStringWithReplacedBrackets(line);
	}

}
