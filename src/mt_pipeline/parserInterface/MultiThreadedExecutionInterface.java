package mt_pipeline.parserInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import datapreparation.SegmenterConfig;
import datapreparation.SegmenterOutputChecker;
import mt_pipeline.MTPipeline;
import util.FilePartSplitter;

public abstract class MultiThreadedExecutionInterface<T extends Callable<T>> {

    protected final InputFilePreprocessor inputFilePreprocessor;
    protected final String baseInputFileName;

    protected MultiThreadedExecutionInterface(InputFilePreprocessor inputFilePreprocessor,
	    String baseInputFileName) {
	this.inputFilePreprocessor = inputFilePreprocessor;
	this.baseInputFileName = baseInputFileName;
    }

    protected void createMetaDataFilesMultipleProcesses() {
	int numParts = getNumberOfThreads();
	FilePartSplitter.splitFileIntoParts(baseInputFileName, numParts);

	List<T> metatDataCreatorProcesses = new ArrayList<T>();
	for (int partNum = 1; partNum <= numParts; partNum++) {
	    String partFileName = baseInputFileName + "Part" + partNum + ".txt";
	    metatDataCreatorProcesses.add(createMetaDataCreatorProcess(partFileName));
	}

	MTPipeline.performThreadComputation(metatDataCreatorProcesses, numParts);
	mergePartResultsAndProduceFinalResult(baseInputFileName, numParts);
    }

    protected abstract int getNumberOfThreads();

    protected abstract T createMetaDataCreatorProcess(String partFileName);

    protected abstract void mergePartResultsAndProduceFinalResult(String baseInputFileName,
	    int numParts);

    public abstract void createResultFilesMutltiThreaded();

    protected abstract void createMetaDataAnnotationBaseFiles();

}
