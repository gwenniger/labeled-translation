package mt_pipeline.taggerInterface;

import util.LinuxInteractor;
import util.Utility;
import mt_pipeline.mt_config.MTConfigFile;
import mt_pipeline.parserInterface.MetaDataAnnotatorProcess;

public abstract class TnTTaggerProcess extends MetaDataAnnotatorProcess<TnTTaggerProcess> {

	protected final TnTInputFileCreator tntInputFileCreator;
	protected final boolean useUniversalTagSet;

	protected TnTTaggerProcess(MTConfigFile theConfig, String partFileName, TnTInputFileCreator tntInputFileCreator, boolean useUniversalTagSet) {
		super(theConfig, partFileName);
		this.tntInputFileCreator = tntInputFileCreator;
		this.useUniversalTagSet = useUniversalTagSet;
	}


	@Override
	public TnTTaggerProcess call() throws Exception {
		String partFileNamePreprocessed = this.theConfig.taggerConfig.getTaggerInputFileNamePreprocessed(partFileName);
		tntInputFileCreator.createPreprocessedInputFile(partFileName, partFileNamePreprocessed);
		LinuxInteractor.executeCommand(tntTagCommand(partFileNamePreprocessed, theConfig.taggerConfig.getTaggerPartOutputFileNameTnTFormat(partFileName)),
				theConfig.taggerConfig.createTnTEnvironmentVariables(), true);
		Utility.deleteFile(partFileNamePreprocessed);
		return this;
	}

	protected abstract String tntTagCommand(String inputFileName, String outputFileName);

}
