package mt_pipeline.taggerInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import util.FilePartMerger;
import util.FileUtil;
import util.Utility;
import mt_pipeline.MTComponentCreater;
import mt_pipeline.mt_config.MTConfigFile;
import mt_pipeline.parserInterface.MetaDataAnnotatorInterface;
import mt_pipeline.parserInterface.MetaDataAnnotatorProcess;

public class TnTTaggerInterface extends TaggerInterface<TnTTaggerProcess> {

	private final TaggerProcessCreater taggerProcessCreater;

	protected TnTTaggerInterface(MTConfigFile theConfig, TnTInputFileCreator inputFileCreator, String baseInputFileName, String tagSubFilePath,
			int numSubFileLines, boolean useUniversalTagSet, TaggerProcessCreater taggerProcessCreater) {
		super(theConfig, inputFileCreator, baseInputFileName, tagSubFilePath, numSubFileLines, useUniversalTagSet);
		this.taggerProcessCreater = taggerProcessCreater;
	}

	@Override
	protected TnTTaggerProcess createMetaDataCreatorProcess(String partFileName) {
		return taggerProcessCreater.createTaggerProcess(theConfig, partFileName, (TnTInputFileCreator) this.inputFilePreprocessor, useUniversalTagSet);
	}

	@Override
	protected void mergePartResultsAndProduceFinalResult(String baseInputFileName, int numParts) {
		FilePartMerger.produceMergedPartsFile(theConfig.taggerConfig.getTaggerOutputFileNameTnTFornat(baseInputFileName), numParts);
		FilePartMerger.deletePartFiles(baseInputFileName, numParts);
		TnTFormatToTagLineFormatConverter.convertToTagLineFormat(theConfig.taggerConfig.getTaggerOutputFileNameTnTFornat(baseInputFileName),
				theConfig.taggerConfig.getTaggerOutputFileName(baseInputFileName, useUniversalTagSet));
		Utility.deleteFile(theConfig.taggerConfig.getTaggerOutputFileNameTnTFornat(baseInputFileName));

	}

	private static void createUniversalSourceTaggerModelIfNotPresent(MTConfigFile theConfig) {

		if (! MTComponentCreater.fileNameAlreadyExists(theConfig.taggerConfig.getCreatedSourceUniversalMapPath())) {

			FileUtil.copyFileNative(theConfig.taggerConfig.getSourceTagsToUniversalTagsMapperFilePath(),
					theConfig.taggerConfig.getCreatedSourceUniversalMapPath(), false);
		}
		if (! MTComponentCreater.fileNameAlreadyExists(theConfig.taggerConfig.getCreatedSourceUniversalMappedModelPath())) {

			FileUtil.copyFileNative(theConfig.taggerConfig.getSourceTaggerModelPath(), theConfig.taggerConfig.getCreatedSourceUniversalMappedModelPath(), false);
		}
	}

	private static void createUniversalTargetTaggerModelIfNotPresent(MTConfigFile theConfig) {

		if (! MTComponentCreater.fileNameAlreadyExists(theConfig.taggerConfig.getCreatedTargetUniversalMapPath())) {

			FileUtil.copyFileNative(theConfig.taggerConfig.getTargetTagsToUniversalTagsMapperFilePath(),
					theConfig.taggerConfig.getCreatedTargetUniversalMapPath(), false);
		}
		if (! MTComponentCreater.fileNameAlreadyExists(theConfig.taggerConfig.getCreatedTargetUniversalMappedModelPath())) {

			FileUtil.copyFileNative(theConfig.taggerConfig.getTargetTaggerModelPath(), theConfig.taggerConfig.getCreatedTargetUniversalMappedModelPath(), false);
		}
	}

	public static TnTTaggerInterface createTnTSourceTaggerInterface(MTConfigFile theConfig, String category) {
		createUniversalSourceTaggerModelIfNotPresent(theConfig);
		return new TnTTaggerInterface(theConfig, new TnTInputFileCreator(), theConfig.taggerConfig.getEnrichmentProducerSourceInputFilePath(category),
				theConfig.filesConfig.getSourceTagSubFilePath(category), theConfig.decoderConfig.getDataLength(category),
				theConfig.taggerConfig.useUniversalTagSetForTaggingSource(), new SourceTaggerProcessCreater());
	}

	public static TnTTaggerInterface createTnTTargetTaggerInterface(MTConfigFile theConfig, String category) {
		createUniversalTargetTaggerModelIfNotPresent(theConfig);
		return new TnTTaggerInterface(theConfig, new TnTInputFileCreator(), theConfig.taggerConfig.getEnrichmentProducerTargetInputFilePath(category),
				theConfig.filesConfig.getTargetTagSubFilePath(category), theConfig.decoderConfig.getDataLength(category),
				theConfig.taggerConfig.useUniversalTagSetForTaggingTarget(), new TargetTaggerProcessCreater());
	}

	public static List<MetaDataAnnotatorInterface<? extends MetaDataAnnotatorProcess<?>>> createTaggers(MTConfigFile theConfig) {
		List<MetaDataAnnotatorInterface<? extends MetaDataAnnotatorProcess<?>>> result = new ArrayList<MetaDataAnnotatorInterface<? extends MetaDataAnnotatorProcess<?>>>();
		final List<String> dataCategories = Arrays.asList(MTConfigFile.TRAIN_CATEGORY, MTConfigFile.TEST_CATEGORY, MTConfigFile.DEV_CATEGORY);

		for (String category : dataCategories) {
			if (theConfig.taggerConfig.tagSourceFile()) {
				result.add(createTnTSourceTaggerInterface(theConfig, category));
			}
			if (theConfig.taggerConfig.tagTargetFile()) {
				result.add(createTnTTargetTaggerInterface(theConfig, category));
			}
		}
		return result;

	}

	private static interface TaggerProcessCreater {
		TnTTaggerProcess createTaggerProcess(MTConfigFile theConfig, String partFileName, TnTInputFileCreator tntInputFileCreator, boolean useUniversalTagSet);
	}

	private static class SourceTaggerProcessCreater implements TaggerProcessCreater {

		@Override
		public TnTTaggerProcess createTaggerProcess(MTConfigFile theConfig, String partFileName, TnTInputFileCreator tntInputFileCreator,
				boolean useUniversalTagSet) {
			return new TnTSourceTaggerProcess(theConfig, partFileName, tntInputFileCreator, useUniversalTagSet);
		}

	}

	private static class TargetTaggerProcessCreater implements TaggerProcessCreater {

		@Override
		public TnTTaggerProcess createTaggerProcess(MTConfigFile theConfig, String partFileName, TnTInputFileCreator tntInputFileCreator,
				boolean useUniversalTagSet) {
			return new TnTTargetTaggerProcess(theConfig, partFileName, tntInputFileCreator, useUniversalTagSet);
		}

	}

}
