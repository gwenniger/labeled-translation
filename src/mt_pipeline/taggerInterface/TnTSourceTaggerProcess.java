package mt_pipeline.taggerInterface;


import mt_pipeline.mt_config.MTConfigFile;

public class TnTSourceTaggerProcess extends TnTTaggerProcess{

	protected TnTSourceTaggerProcess(MTConfigFile theConfig, String partFileName, TnTInputFileCreator tntInputFileCreator, boolean useUniversalTagSet) {
		super(theConfig, partFileName, tntInputFileCreator, useUniversalTagSet);
	}

	@Override
	protected String tntTagCommand(String inputFileName, String outputFileName) {
		String taggerDir = theConfig.taggerConfig.getTnTTaggerRootDir();
		String result;
		if (useUniversalTagSet) {
			result = taggerDir + "tnt " + theConfig.taggerConfig.getSourceUniversalMappedTaggerModelName() + " " + inputFileName + " > " + outputFileName;
		} else {
			result = taggerDir + "tnt " + theConfig.taggerConfig.getSourceTaggerModelName() + " " + inputFileName + " > " + outputFileName;
		}
		return result;
	}
}
