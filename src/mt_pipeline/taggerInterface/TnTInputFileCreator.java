package mt_pipeline.taggerInterface;

import mt_pipeline.parserInterface.InputFilePreprocessor;

public class TnTInputFileCreator extends InputFilePreprocessor {

	private static String createOneWordPerLineString(String line) {
		String[] words = line.split("\\s+");
		String result = "";
		for (String word : words) {
			result += word + "\n";
		}
		// Extra new line to end the sentence
		result += "%% </LineEnd>";
		return result;
	}

	@Override
	public String createPreprocessedLine(String line) {
		return createOneWordPerLineString(line);
	}

}
