package mt_pipeline.taggerInterface;

import java.util.ArrayList;
import java.util.List;

import mt_pipeline.MTComponentCreater;
import mt_pipeline.mt_config.MTConfigFile;
import mt_pipeline.parserInterface.InputFilePreprocessor;
import mt_pipeline.parserInterface.MetaDataAnnotatorInterface;
import mt_pipeline.parserInterface.MetaDataAnnotatorProcess;
import mt_pipeline.preAndPostProcessing.SubFileCreater;

public abstract class TaggerInterface<T extends MetaDataAnnotatorProcess<T>> extends MetaDataAnnotatorInterface<T> {

	protected final boolean useUniversalTagSet;
	protected final String tagSubFilePath;
	protected final int numSubFileLines;

	protected TaggerInterface(MTConfigFile theConfig, InputFilePreprocessor inputFilePreprocessor, String baseInputFileName, String tagSubFilePath, int numSubFileLines,
			boolean useUniversalTagSet) {
		super(theConfig, inputFilePreprocessor, baseInputFileName);
		this.tagSubFilePath = tagSubFilePath;
		this.numSubFileLines = numSubFileLines;
		this.useUniversalTagSet = useUniversalTagSet;

	}

	protected boolean tagResultFileNameAlreadyExistsAndRightSize(String baseInputFileName, boolean useUniversalTagSet) {
		String resultFileName = theConfig.taggerConfig.getTaggerOutputFileName(baseInputFileName, useUniversalTagSet);
		System.out.println("tagResultFileNameAlreadyExistsAndRightSize : - resultFileName: " + resultFileName);
		return  MTComponentCreater.fileAlreadyExistsAndRigthSize(baseInputFileName, resultFileName);
	}

	private void tagFilesMultipleProcesses(boolean useUniveralTags) {

		if (tagResultFileNameAlreadyExistsAndRightSize(baseInputFileName, useUniveralTags)) {
			System.out.println("NOTE : A tag result file with the right name and size already exists. Keeping that file and skipping retagging");
		} else {
			createMetaDataFilesMultipleProcesses();
		}
	}

	protected void createMetaDataAnnotationFilesFromPreprocessedInputFiles() {
		tagFilesMultipleProcesses(this.useUniversalTagSet);
	}

	public void tagFiles() {
		System.out.println("TaggerInterface.tagFiles called...");
		createMetaDataAnnotationBaseFiles();
	}

	@Override
	protected void createMetaDataAnnotationBaseFiles() {
		createMetaDataAnnotationFilesFromPreprocessedInputFiles();
	}

	@Override
	protected List<String> createMetaDataAnnotationSubFiles() {
		List<String> result = new ArrayList<String>();
		SubFileCreater.createSubFile(theConfig.taggerConfig.getTaggerOutputFileName(baseInputFileName, useUniversalTagSet), tagSubFilePath,
				numSubFileLines);
		result.add(tagSubFilePath);
		return result;
	}
}
