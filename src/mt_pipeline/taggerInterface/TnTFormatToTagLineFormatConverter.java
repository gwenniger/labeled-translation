package mt_pipeline.taggerInterface;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class TnTFormatToTagLineFormatConverter {

	private static final String TnTCommentString = "%%";

	public static boolean isCommentLine(String line) {
		return line.startsWith(TnTCommentString, 0);
	}

	public static boolean isEmptyLine(String line) {
		return (line.length() == 0);
	}

	public static void convertToTagLineFormat(String inputFileName, String outputFileName) {
		System.out.println("convertToTagLineFormat(" + inputFileName + "," + outputFileName + ") called...");
		try {
			BufferedReader fileReader = new BufferedReader(new FileReader(inputFileName));
			BufferedWriter fileWriter = new BufferedWriter(new FileWriter(outputFileName));

			System.out.println("Reader and writer created");

			String line;
			String resultLine = "";
			boolean resultLineStarted = false;
			boolean previousLinesWritten = false;
			while ((line = fileReader.readLine()) != null) {

				//System.out.println("line: " + line);

				if (isEmptyLine(line) || isCommentLine(line)) {
					//System.out.println("Is empty line or command line");
					if (resultLineStarted) {
						
						resultLine = resultLine.trim();
						if(previousLinesWritten)
						{
							resultLine = "\n" + resultLine;	
						}
						//System.out.println("Writing line : \"" + resultLine  + "\" to result file...");
						fileWriter.write(resultLine);
						resultLineStarted = false;
						resultLine = "";
						previousLinesWritten = true;
					}
				} else {
					String[] parts = line.split("\\s+");
					resultLine = resultLine + " " + parts[1];
					resultLineStarted = true;
				}
			}
			
			// Write the last result line if present
			if (resultLineStarted) {
				
				resultLine = resultLine.trim();
				if(previousLinesWritten)
				{
					resultLine = "\n" + resultLine;	
				}
				fileWriter.write(resultLine);
			}	

			fileReader.close();
			fileWriter.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
