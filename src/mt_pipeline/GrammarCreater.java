package mt_pipeline;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;

import java.util.concurrent.Callable;

import util.FileUtil;
import util.LinuxInteractor;
import mt_pipeline.mt_config.MTConfigFile;

public abstract class GrammarCreater extends MTComponentCreater implements Callable<GrammarCreater> {

	protected final String grammarOutputPath;
	protected final String category;
	protected final SystemIdentity systemIdentity;

	protected GrammarCreater(MTConfigFile theConfig, String grammarOutputPath, String category, SystemIdentity systemIdentity) {
		super(theConfig);
		this.grammarOutputPath = grammarOutputPath;
		this.category = category;
		this.systemIdentity = systemIdentity;
	}

	protected abstract void extractSortedFilteredGrammar();

	@Override
	public GrammarCreater call() throws Exception {
		extractSortedFilteredGrammar();
		return this;
	}

	public static void sortGrammar(String rawGrammarPath, String sortedGrammarPath, String temporaryDirectoryPath, int noThreads) {

		// This still doesn't work. perhaps splitting the file into an unequal
		// number of parts is a problem, or splitting into parts with different lengths
		// might be
		// MultiThreadSort multiThreadSort =
		// MultiThreadSort.createMultiThreadSort(noThreads, rawGrammarPath,
		// sortedGrammarPath,temporaryDirectoryPath);
		// multiThreadSort.sortLexicographically();
		String filterGrammarCommand = "sort --temporary-directory " + temporaryDirectoryPath + " " + rawGrammarPath + " -o " + sortedGrammarPath;
		System.out.println("filterGrammarCommand :" + filterGrammarCommand);
		boolean errorOccurred = LinuxInteractor.executeExtendedCommandDisplayOutput(filterGrammarCommand);
		if (errorOccurred) {
			throw new RuntimeException("An error occurred during sorting");
		}
	}

	protected static void checkDevAndMertFolderExist(MTConfigFile theConfig) {

		if (!FileUtil.folderExists(theConfig.foldersConfig.getMertFolder())) {
			System.out.println("Error: intermediate-results Mert Folder(\"" + theConfig.foldersConfig.getMertFolder() + "\") does not exist");
			System.exit(1);
		}

		if (!FileUtil.folderExists(theConfig.foldersConfig.getIntermediateTestFolder())) {
			System.out.println("Error: intermediate-results Test Folder does not exist");
			System.exit(1);
		}
		return;
	}
	
	 protected static boolean useComplexGlueRules(boolean useComplexLabels, MTConfigFile theConfig) {
		/**
		 * We use complex glue rules if we use complex labels, but not if we do
		 * fuzzy matching decoding. Fuzzy matching decoding allow every label to
		 * substitute for every other label. This means we should work with
		 * canonical forms of every rule identity. The canonical form of the
		 * glue rules is than the normal glue rule with X as glued symbol. The
		 * other symbols in the grammar can than naturally substitute for it.
		 */
		return (useComplexLabels && (!theConfig.decoderConfig
			.useFuzzyMatchingDecodingWithConsistencyCheck()));
	 }

}
