package mt_pipeline;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.translationRules.TranslationRuleRepresentationCreater;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import bitg.Pair;
import util.TextFileIterator;
import mt_pipeline.mt_config.MTConfigFile;
import mt_pipeline.mt_config.MTDecoderConfig;

/**
 * 
 * @author gemaille
 * 
 *         Class for the automatic generation of a Config file, based on a configuration file. Many parameters are simply fixed - we don't change them, but some
 *         can be changed by specifying parameters in the config file. Using automatically generated config files assures that we have full control of the
 *         experiment: settings must be changed in one central config file, and we avoid putting (variable) settings at multiple places.
 * 
 *         25-7-2012 : Gideon : Adapted for the new version of Joshua, which scraps some old config file options
 */
public class JoshuaConfigCreater extends DecoderConfigCreater {
	protected static final String PHRASE_OWNER = "pt";
	
	// Grammar with no Span limit but should start on left of sentence
	protected static final String GLUE_GRAMMAR_PARAMETER_SIGNATURE = "tm = thrax " + PHRASE_OWNER + " -1 ";	

	protected static final String LANGUAGE_MODEL_SPECIFICATION_PREFIX = "lm = ";
	private static final String MAIN_GRAMMAR_SPECIFICATION_PREFIX = "tm = thrax " + PHRASE_OWNER;
	private static final String NL = "\n";
	
	// Advanced feature for KenLM that specifies whether left-state minimization was requested
	private boolean KENLM_IS_LEFT_MINIMIZING = false;
	
	//private static final int PRUNING_POP_LIMIT = 100;
	private final String unknownWordsLabel;
	
	// Careful : These names are processed case-sensitive in Joshua!
	private static final String OOV_PENALTY_JOSHUA_PROPERTY_NAME = "OOVPenalty";
	private static final String OOV_PENALTY_MOSES_PROPERTY_NAME = "OOV_Penalty";
	private static final String WORD_PENALTY_JOSHUA_PROPERTY_STRING = "WordPenalty";
	
	private static final double OOVPenaltyInitialWeight = 100;


	protected JoshuaConfigCreater(MTConfigFile theConfig, String unknownWordsLabel, SystemIdentity systemIdentity) {
		super(theConfig, systemIdentity);
		this.unknownWordsLabel = unknownWordsLabel;
	}

	public static JoshuaConfigCreater createJoshuaConfigCreater(MTConfigFile theConfig, String unknownWordsLabel) {
		SystemIdentity systemIdentity = SystemIdentity.createSystemIdentity(theConfig);
		return new JoshuaConfigCreater(theConfig, unknownWordsLabel, systemIdentity);
	}

	private int getSpanLimit(){
	    return systemIdentity.getMaxSourceAndTargetLength();
	}
	
	private String getMainGrammarParameterSignature()
	{
	    String mainGrammarParameterSignature =  MAIN_GRAMMAR_SPECIFICATION_PREFIX + " " + getSpanLimit() + " ";
	    return mainGrammarParameterSignature;
	}
	
	@Override
	public void writeMertConfigFile(MTConfigFile theConfig) {
		BufferedWriter outputWriter;
		try {
			String outFileName = theConfig.systemSpecificConfig.getDecoderMertConfigFileName();
			outputWriter = new BufferedWriter(new FileWriter(outFileName));
			outputWriter.write(this.getJoshuaMertConfigFile());
			outputWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void writeTestConfigFile(MTConfigFile theConfig) {
		BufferedWriter outputWriter;
		try {
			String outFileName = theConfig.systemSpecificConfig.getDecoderTestConfigFileName();
			outputWriter = new BufferedWriter(new FileWriter(outFileName));
			outputWriter.write(this.getJoshuaTestConfigFile());
			outputWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	private String glueGrammarConfigTest() {
		String result = NL + NL + this.getTestGlueGrammarConfig() + NL;
		return result;
	}

	/**
	 * See also remarks in ComplexGlueGrammarCreater about 'default_non_terminal' . This symbol is used for Out of Vocabulary words (OOV) If we want to keep
	 * using 'X' we have to add the basic Chiang rules to our grammar as well, otherwise we have to adapt this four our particular grammar. In the case of SAMT
	 * we will use special pre-terminal rules for the unknown words that produce the unknownWordsLabel from all other SAMT labels that occur in the grammar.
	 * This prevents the problem that unknown words can only be generated from the Chiang Glue rule, which forces the grammatical derivations to be broken up
	 * when unknown words occur, and will most likely have a bad influence on performance.
	 * 
	 * @return
	 */

	private String getTranslationModelConfig() {
		// String result = NL + NL + "#translation moldel config" + NL +
		// "span_limit=15" + NL + "phrase_owner=pt" + NL + "mono_owner=mono" +
		// NL +
		// "begin_mono_owner=begin_mono"
		// + NL + "default_non_terminal=" + unknownWordsLabel + NL +
		// "goalSymbol=S";
		String result = NL + NL + "#translation moldel config" + NL + "default_non_terminal=" + unknownWordsLabel + NL + "goalSymbol=" + TranslationRuleRepresentationCreater.GOAL_LABEL;

		return result;

	}

	private String getPopLimitConfiguration() {
		return NL + "pop-limit= " + this.theConfig.decoderConfig.getCubePruningPopLimit();
	}
	
	private String getMaxNumberAlternativeLabeledVersionsPerLanguageModelStateConfiguration() {
		return NL + MTDecoderConfig.MAX_NUMBER_ALTERNATIVE_LABELED_VERSIONS_PER_LANGUAGE_MODEL_STATE +  " = " 
			+ this.theConfig.decoderConfig.getMaxNumberAlternativeLabeledVersionsPerLanguageModelState();
	}

	private String getPruningConfig() {
		// String result = NL + NL + "#pruning config" + NL + "fuzz1=0.1" + NL +
		// "fuzz2=0.1" + NL + "max_n_items=30" + NL + "relative_threshold=10.0"
		// + NL
		// + "max_n_rules=50" + NL + "rule_relative_threshold=10.0";
		// "useBeamAndThresholdPrune=true" + getPopLimitForNewJoshuaVersion() +
		// NL + "fuzz1=0.1" + NL + "fuzz2=0.1" + NL + "max_n_items=30" + NL
		// + "relative_threshold=10.0" + NL + "max_n_rules=50";
		String result = NL + NL + "#pruning config" + getPopLimitConfiguration();
		
		if(theConfig.decoderConfig.getMaxNumberAlternativeLabeledVersionsPerLanguageModelState() > 0){
		    result += getMaxNumberAlternativeLabeledVersionsPerLanguageModelStateConfiguration();
		}		
		return result;
	}

	private String getNBestConfig(int nbest) {
		String result = NL + NL + "#nbest config" + NL 
			+ "n-best-list = USE_NBEST_TUNING_TRUE_THIS_PATH_DOES_NOT_MATTER" + NL
			+"use_unique_nbest=true" + NL + "use_tree_nbest=false" + NL + "top_n=" + nbest;
		return result;
	}
	
	private String getNumTranslationOptionsConfig(){
	    String result =  NL + "#Number of translation options config" + NL;
	    result += MTDecoderConfig.NUM_TRANSLATION_OPTIONS_PROPERTY + "=" 
		    + theConfig.decoderConfig.getNumTranslationOptions();
	    return result;	    
	}
	

	@SuppressWarnings("unused")
	private String getRemoteLMConfig() {
		String result = NL + NL + "#remote lm server config, we should first prepare remote_symbol_tbl before starting any jobs" + NL + "use_remote_lm_server=false" + NL
				+ "remote_symbol_tbl=./voc.remote.sym" + NL + "num_remote_lm_servers=4" + NL + "f_remote_server_list=./remote.lm.server.list" + NL + "remote_lm_server_port=9000";
		return result;
	}

	public String getParametersWithFeatureSpecificWeights() {
		String result = NL + "#phrasemodel owner column(0-indexed) weight";

		int i = 0;
		for (Pair<String, Double> featureNameWeightPair : getAllOrderedFeatureNamesWithWeights()) {

			result += NL + "tm_pt_" + i + " " + featureNameWeightPair.last;
			// result += NL + featureName + " pt " + i + " " + parameterValue;
			i++;
		}
		return result;
	}

	public static double getOOVPenaltyInitialWeight(){
	    return OOVPenaltyInitialWeight;
	}
	
	public static String getOOVPenaltyMosesName(){
	    return OOV_PENALTY_MOSES_PROPERTY_NAME;
	}
	
	private String getModelWeights() {
		String result = NL + NL + "###### model weights" + NL + "#lm order weight" + NL + "lm_0 1.0" + NL +

		getParametersWithFeatureSpecificWeights() +

		NL + NL + "#wordpenalty weight" + NL + "WordPenalty -1.8228045931756582" + NL + "#out of vocubalary penalty weight" + NL + OOV_PENALTY_JOSHUA_PROPERTY_NAME + " " + getOOVPenaltyInitialWeight();
		return result;
	}

	private String getParallelDecoderConfig() {
		String result = NL + NL + "#parallel deocoder: it cannot be used together with remote lm" + NL + "num_parallel_decoders=" + this.theConfig.decoderConfig.getNumParallelDecoders();
		return result;
	}

	
	private String getLabelSubstitutionFeatureSwitchString()
	{
	    String result = "";
	    theConfig.decoderConfig.testConsistencyLabelSplittingSmoothingForLabelSubstitutionFeatures(systemIdentity);
	    if (theConfig.decoderConfig.useLabelSubstitutionFeaturesWithConsistencyCheck()) {
		if(systemIdentity.producesDoubleReorderingLabels() && theConfig.decoderConfig.useLabelSplittingSmoothingForLabelSubstitutionFeatures()){
		    result += NL +  theConfig.decoderConfig.getJoshuaDoubleLabelSmoothedLabelSubsitutionFeatureString() + NL;   
		}
		else if(systemIdentity.producesDoubleReorderingLabels() && theConfig.decoderConfig.useLabelSplittingSmoothingWithOnlyLabelSubstitutionFeaturesForSplittedLabels()){
			    result += NL +  theConfig.decoderConfig.getJoshuaDoubleLabelSmoothedOnlyLabelSubsitutionFeaturesForSingleLabelsString() + NL;   
		}		
		else {
		    result += NL +  theConfig.decoderConfig.getJoshuaLabelSubsitutionFeatureString() + NL;    
		}
	    }
	    System.out.println(">>> getLabelSubstitutionFeatureSwitchString( - Result: " + result);
	    return result;
	}
	
	private String getRuleApplicationFeatureSwitchString(){
	    String result = "";
	    if(theConfig.decoderConfig.useRuleApplicationFeature()){
		result += theConfig.decoderConfig.getJoshuaRuleApplicationFeatureString();
	    }
	    return result;
	}
	
	
	/**
	 * Generate the static part of the Joshua configuration
	 * 
	 * @return
	 */
	private String getStaticConfig() {
		String result = getStaticHeader();
		// result += getLanguageModeConfig();

		result += NL + theConfig.decoderConfig.useDotChartStringString() + NL;
		
		if (theConfig.decoderConfig.useFuzzyMatchingDecodingWithConsistencyCheck()) {
			result += NL +  theConfig.decoderConfig.getJoshuaFuzzyMatchingDecodingString() + NL;
			result += NL +  theConfig.decoderConfig.getRemoveLabelsInsideGrammarTrieForMoreEfficientFuzzyMatchingString() + NL;
			result += NL +  theConfig.decoderConfig.getExploreAllDistinctLabeledRuleVersionsInCubePruningInitializationString() + NL;
			
			if(theConfig.decoderConfig.useSeparateCubePruningStatesForMatchingSubstitutions()){
			    result += theConfig.decoderConfig.getJoshuaSeparateCubePruningStatesForMatchingSubstitutionsDecodingString() + NL;
			}
			if(theConfig.decoderConfig.exploreAllLabelsForGlueRulesInCubePruningInitialization()){
			    result += theConfig.decoderConfig.getExploreAllLabelsForGlueRulesInCubePruningInitialization() + NL;
			}
			if(theConfig.decoderConfig.exploreAllPossibleLabelSubstitutionsForAllRulesInCubePruningInitializationWithConsistencyCheck()){
			   result  += theConfig.decoderConfig.getExploreAllPossibleLabelSubstitutionsForAllRulesInCubePruningInitialization() + NL;	   
			}
			if(theConfig.decoderConfig.useShufflingFeatureIsSpecified()){
			    result += theConfig.decoderConfig.getUseShufflingString() + NL;
			}

		}
		
		result += getLabelSubstitutionFeatureSwitchString();
		result += getRuleApplicationFeatureSwitchString();
		result += getTranslationModelConfig();
		result += getPruningConfig();
		// result += getRemoteLMConfig();
		result += getWordAndOOVSwitchOnString();
		result += getModelWeights();
		return result;
	}
	
	private String featureFunctionSwitchOnLine(String featureName){
	    return "feature-function = " + featureName + NL;
	}
	
	private String getWordAndOOVSwitchOnString(){
	    String result ="";
	    result += NL + NL;
	    result += "# Switches for turning on OOV and WordPenalty feature" + NL;
	    result += featureFunctionSwitchOnLine(WORD_PENALTY_JOSHUA_PROPERTY_STRING);
	    result += featureFunctionSwitchOnLine(OOV_PENALTY_JOSHUA_PROPERTY_NAME);
	    result += NL;
	    return result;	
	}

	private String getLanguageModelName()
	{
	    return this.theConfig.decoderConfig.getLanguageModelName();
	}
	
	
	private String getLMPropertiesSpecification() {
		return getLanguageModelName() + " " + theConfig.decoderConfig.getLanguageModelOrder() + " " +  KENLM_IS_LEFT_MINIMIZING  + " " + "false 100";
	}

	private String getLanguageModelSpecification() {
		return LANGUAGE_MODEL_SPECIFICATION_PREFIX + getLMPropertiesSpecification() + " " + getLanguageModelFilePath();
	}

	/**
	 * Generate the variable part of the Joshua configuration for testing
	 * 
	 * @return
	 */
	private String getTestCustomConfig() {
		String result = getDynamicHeader();
		result += getNBestConfig(MTDecoderConfig.TEST_NBEST);
		result += getNumTranslationOptionsConfig();
		result += getParallelDecoderConfig();
		result += NL + "# Path of the Language Model file";
		result += NL + getLanguageModelSpecification();
		result += NL + "# Path of the grammar file";
		result += getTestMainGrammarConfig();
		result += glueGrammarConfigTest();
		result += getSharedDynamicConfig();
		return result;
	}

	private String getTestMainGrammarName() {
		if (systemIdentity.usePackedGrammars()) {
			return theConfig.filesConfig.getPackedMainGrammarFolderTest();
		} else {
			return theConfig.filesConfig.getFilteredGrammarPathTest();
		}
	}

	private String getTestMainGrammarConfig() {
		String result = NL + getMainGrammarParameterSignature();

		if (systemIdentity.usePackedGrammars()) {
			result += theConfig.filesConfig.getPackedMainGrammarFolderTest();
		} else {
			result += theConfig.filesConfig.getFilteredGrammarPathTest();
		}
		return result;
	}

	private static String getTestGlueGrammarName(MTConfigFile theConfig) {
		return theConfig.filesConfig.getGlueGrammarPathTest();
	}

	private String getTestGlueGrammarConfig() {
		String result = GLUE_GRAMMAR_PARAMETER_SIGNATURE;
		result += theConfig.filesConfig.getGlueGrammarPathTest();
		return result;
	}

	private String getMertMainGrammarConfig() {
		String result = NL;
		result += getMainGrammarParameterSignature();
		if (systemIdentity.usePackedGrammars()) {
			result += theConfig.filesConfig.getPackedMainGrammarFolderMert();
			;
		} else {
			result += theConfig.filesConfig.getFilteredGrammarPathMert();
		}
		return result;
	}

	private String getMertGlueGrammarConfig() {
		String result = "";
		result += GLUE_GRAMMAR_PARAMETER_SIGNATURE;
		result += theConfig.filesConfig.getGlueGrammarPathMert();
		return result;
	}

	private String glueGrammarConfigDev() {
		String result = NL + NL + this.getMertGlueGrammarConfig() + NL;
		return result;
	}

	public String getMertGrammarsConfig() {
		String result = "";
		result += getMertMainGrammarConfig();
		result += glueGrammarConfigDev();
		return result;
	}

	private String getNBestFormatStringJosuaDecoderConfig() {
		return NL + getNBestFormatPropertyString() + "=" + theConfig.decoderConfig.getNBestFormat();
	}

	private String getSharedDynamicConfig() {
		String result = "";
		// result += getNBestFormatStringJosuaDecoderConfig();
		return result;
	}

	/**
	 * Generate the variable part of the Joshua configuration for running MERT
	 * 
	 * @return
	 */
	private String getMertCustomConfig() {
		String result = getDynamicHeader();
		result += getNBestConfig(theConfig.decoderConfig.getNBestSizeTuning());
		result += getNumTranslationOptionsConfig();
		result += getParallelDecoderConfig();
		result += NL + "# Path of the Language Model file";
		result += NL + getLanguageModelSpecification();
		result += NL + "# Path of the grammar file";
		result += getMertGrammarsConfig();
		result += getSharedDynamicConfig();
		return result;
	}

	private String getJoshuaMertConfigFile() {
		String result = getMainHeader();
		result += getMertCustomConfig();
		result += getStaticConfig();

		return result;
	}

	private String getJoshuaTestConfigFile() {
		String result = getMainHeader();
		result += getTestCustomConfig();
		result += getStaticConfig();

		return result;
	}

	private static boolean lineConstitutesMainGrammarParameterSpecification(String line) {
		return ((!lineConstitutesGlueGrammarParameterSpecification(line)) && line.contains(MAIN_GRAMMAR_SPECIFICATION_PREFIX));
	}

	private static boolean lineConstitutesGlueGrammarParameterSpecification(String line) {
		return line.contains(GLUE_GRAMMAR_PARAMETER_SIGNATURE);
	}
	
	private static boolean lineConstituesLanguageModelFileSpecification(String line){
	    return line.contains(LANGUAGE_MODEL_SPECIFICATION_PREFIX);
	}

	public void copyJoshuaConfigFileReplaceGrammarFileParameter(MTConfigFile theConfig, String originalConfigFilePath, String outputFilePath, String newMainGrammarFilePath,
			String newGlueGrammarFilePath) {
		try {
			TextFileIterator textFileIterator = new TextFileIterator(originalConfigFilePath);
			BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFilePath));

			String line = "";

			while (textFileIterator.hasNext()) {
				line = textFileIterator.next();
				if (lineConstitutesMainGrammarParameterSpecification(line)) {
					String newGrammarParameterSpecification = getMainGrammarParameterSignature() + newMainGrammarFilePath;
					outputWriter.write(newGrammarParameterSpecification + NL);
				} else if (lineConstitutesGlueGrammarParameterSpecification(line)) {
					String newGrammarParameterSpecification = GLUE_GRAMMAR_PARAMETER_SIGNATURE + newGlueGrammarFilePath;
					outputWriter.write(newGrammarParameterSpecification + NL);
				} else if(lineConstituesLanguageModelFileSpecification(line)){ 
				    // We rewrite the language model file path to deal with the case that the root output file path changed
				    // in which the old file path is no longer correct 
				    // (this is relevant when we rerun only evaluation including decoding of the test set, but have
				    // moved the experiment folder to a new location
				    outputWriter.write(getLanguageModelSpecification() + NL);
				}
				else {
					// simply copy the old line
					outputWriter.write(line + NL);
				}

			}
			
			// Write CPU logging configuration
			if(theConfig.decoderConfig.logDecodingComputationTimes()){
			    outputWriter.write(theConfig.decoderConfig.getLogDecodingComputationTimesString() + NL);
			    outputWriter.write(theConfig.decoderConfig.getCpuComputationTimesLogFilePathString() + NL);
			}
			
			outputWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void createTunedTestConfig(MTConfigFile theConfig) {
		copyJoshuaConfigFileReplaceGrammarFileParameter(theConfig, theConfig.filesConfig.getJoshuaDevTunedConfigFileName(), theConfig.filesConfig.getDecoderTestTunedConfigFileName(),
				getTestMainGrammarName(), getTestGlueGrammarName(theConfig));
	}

	@Override
	protected String getSystemTypeName() {
		return JoshuaSystem.getSystemTypeName();
	}
}
