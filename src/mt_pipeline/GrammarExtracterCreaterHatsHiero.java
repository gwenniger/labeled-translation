package mt_pipeline;

import mt_pipeline.mt_config.MTConfigFile;
import mt_pipeline.mt_config.MTGrammarExtractionConfig;
import grammarExtraction.chiangGrammarExtraction.MultiThreadGrammarExtractorTestFiltered;
import grammarExtraction.grammarExtractionFoundation.GrammarStructureSettings;
import grammarExtraction.grammarExtractionFoundation.MultiThreadGrammarExtractor;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;

public class GrammarExtracterCreaterHatsHiero extends GrammarExtracterCreater {

	private GrammarExtracterCreaterHatsHiero(boolean writePlainHieroRules,SystemIdentity systemIdentity) {
		super(writePlainHieroRules,systemIdentity);
	}

	public static boolean writePlainHieroRulesIfSwitchedOnOrIfThereIsNoReorderingLabelExtension(MTConfigFile theConfig) {
		return GrammarStructureSettings.writePlainHieroRulesForSmoothing(theConfig)|| (!MTGrammarExtractionConfig.useReorderingLabelExtension(theConfig));
	}

	public static GrammarExtracterCreaterHatsHiero createExtracterCreaterHatsHiero(MTConfigFile theConfig,SystemIdentity systemIdentity) {
		return new GrammarExtracterCreaterHatsHiero(writePlainHieroRulesIfSwitchedOnOrIfThereIsNoReorderingLabelExtension(theConfig),systemIdentity);
	}

	@Override
	public MultiThreadGrammarExtractor createGrammarExtracterTestFiltered(String configFilePath, int numThreads) {
		return MultiThreadGrammarExtractorTestFiltered.createMultiThreadGrammarExtractorTestFilteredHiero(configFilePath, numThreads,systemIdentity);
	}

	@Override
	public String getSystemName() {
		return MTPipelineHatsHiero.SystemName;
	}

}
