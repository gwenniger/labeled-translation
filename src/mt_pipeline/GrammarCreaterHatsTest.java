package mt_pipeline;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.samtGrammarExtraction.GlueGrammarCreater;
import mt_pipeline.mt_config.GrammarFilesLocations;
import mt_pipeline.mt_config.MTConfigFile;

public class GrammarCreaterHatsTest extends GrammarCreaterHats {

	private GrammarCreaterHatsTest(MTConfigFile theConfig, String grammarOutputPath, String category, MultiThreadGrammarExtracterCreater grammarExtracterCreater,
			GrammarCreaterHatsConfigFileCreater configFileCreater, GlueGrammarCreater glueGrammarCreater, SystemIdentity systemIdentity) {
		super(theConfig, grammarOutputPath, category, grammarExtracterCreater, configFileCreater, glueGrammarCreater, systemIdentity);
	}

	public static GrammarCreaterHats createTestGrammarCreater(MTConfigFile theConfig, MultiThreadGrammarExtracterCreater grammarExtracterCreater,
			GrammarCreaterHatsConfigFileCreater configFileCreater, GlueGrammarCreater glueGrammarCreater, SystemIdentity systemIdentity) {
		return new GrammarCreaterHatsTest(theConfig, theConfig.filesConfig.getFilteredGrammarPathTest(), MTConfigFile.TEST_CATEGORY, grammarExtracterCreater, configFileCreater, glueGrammarCreater,
				systemIdentity);
	}

	private String getMainGrammarExtractionConfigFilePath() {
		return theConfig.filesConfig.getHatsMainGrammarExtractorConfigPathTest();
	}

	private String getSmoothingGrammarExtractionConfigFilePath() {
		return theConfig.filesConfig.getHatsSmoothingGrammarExtractorConfigPathTest();
	}

	protected void writeGrammarExtractionConfigFile() {
		configFileCreater.writeHatsGrammarExtractionConfigFiles(getMainGrammarExtractionConfigFilePath(), getSmoothingGrammarExtractionConfigFilePath());
	}

	@Override
	protected void extractGrammars() {
		GrammarFilesLocations grammarFilesLocations = GrammarFilesLocations.createGrammarFilesLocations(getMainGrammarExtractionConfigFilePath(), getSmoothingGrammarExtractionConfigFilePath(),
				theConfig.filesConfig.getFilteredGrammarPathTest(), theConfig.filesConfig.getGlueGrammarPathTest(), getDenseMapFilePath(), getPackerConfigFilePath(),
				theConfig.filesConfig.getGapLabelConditionalProbabilityTableFilePathTest());
		performGrammarExtractionAndSorting(grammarFilesLocations, theConfig);
	}
}
