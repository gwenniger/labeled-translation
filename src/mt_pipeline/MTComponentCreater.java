package mt_pipeline;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import util.FileStatistics;
import util.LinuxInteractor;
import util.Pair;
import mt_pipeline.mt_config.MTConfigFile;

public abstract class MTComponentCreater {
	protected final MTConfigFile theConfig;

	protected MTComponentCreater(MTConfigFile theConfig) {
		this.theConfig = theConfig;
	}

	private List<Pair<String>> createJavaBinPathEnvironmentVariables() {
		List<Pair<String>> environmentVariables = new ArrayList<Pair<String>>();
		environmentVariables.add(this.theConfig.getPathExtendedWithJavaBinPathEnvironmentVariablePair());
		return environmentVariables;
	}

	protected boolean runExternalCommandUsingPreSpecifiedJavanBinDir(String command, boolean displayOutput, String classPath) {

		List<Pair<String>> environmentVariables = createJavaBinPathEnvironmentVariables();
		if (classPath != null) {
			environmentVariables.add(new Pair<String>("CLASSPATH", classPath));
		}

		System.out.println("MTComponentCreater.runExternalCommandUsingPreSpecifiedJavanBinDir -  Command: \n" + command);
		if (displayOutput) {
			return LinuxInteractor.executeExtendedCommandDisplayOutput(command, environmentVariables);
		} else {
			LinuxInteractor.executeCommand(command, environmentVariables, true);
			return true;
		}
	}

	protected void runExternalCommandUsingPreSpecifiedJavanBinDir(String command, boolean displayOutput) {
		runExternalCommandUsingPreSpecifiedJavanBinDir(command, displayOutput, null);
	}

	protected void runExternalCommandUsingPreSpecifiedJavanBinDirAndContinue(String command) {
		System.out.println("MTComponentCreater.runExternalCommandUsingPreSpecifiedJavanBinDir -  Command: \n" + command);
		LinuxInteractor.executeCommand(command, createJavaBinPathEnvironmentVariables(), false);
	}

	public static boolean fileNameAlreadyExists(String fileName) {
		File file = new File(fileName);
		return file.exists();
	}

	public static boolean fileAlreadyExistsAndRigthSize(String sizeReferenceFileName, String checkedFileName) {
		if (fileNameAlreadyExists(checkedFileName)) {
			try {
				if (FileStatistics.countLines(sizeReferenceFileName) == FileStatistics.countLines(checkedFileName)) {
					return true;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

}
