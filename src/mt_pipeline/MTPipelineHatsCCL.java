package mt_pipeline;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import mt_pipeline.mt_config.MTConfigFile;

public class MTPipelineHatsCCL extends MTPipelineHatsComplexLabels{

	public static final String SystemName = "JoshuaHatsCCL";

	protected MTPipelineHatsCCL() {
	};

	protected MTPipelineHatsCCL(MTConfigFile theConfig, String configFilePath, int noModelParameters,MTSystem mtSystem) {
		super(theConfig, configFilePath, noModelParameters,false,mtSystem);
	}
	
	@Override
	public MTPipeline createMTPipeline(String configFilePath) throws RuntimeException {
		return createMTPipelineHatsCCL(configFilePath);
	}

	@Override
	protected void extractGrammars() {
		long startTime = System.currentTimeMillis();
		GrammarCreaterHats.extractFilteredHatsCCLGrammarsPrallel(getTheConfig(),getSystemIdentity());
		long passedTime = (System.currentTimeMillis() - startTime) / 1000;
		System.out.println(">>> HATs grammar extraction took: " + passedTime + "seconds");
	}

	public static MTPipeline createMTPipelineHatsCCL(String configFilePath) throws RuntimeException {
		MTConfigFile mtConfigFile = MTConfigFile.createMtConfigFile(configFilePath, SystemName);
		SystemIdentity systemIdentity = SystemIdentity.createSystemIdentity(mtConfigFile);
		MTSystem mtSystem = MTSystem.createMTSystem(mtConfigFile,systemIdentity);
		return new MTPipelineHatsCCL(mtConfigFile, configFilePath,
				mtSystem.noModelParameters(mtConfigFile,SystemName),mtSystem);
	}
	
}
