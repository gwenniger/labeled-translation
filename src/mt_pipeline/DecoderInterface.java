package mt_pipeline;

import org.junit.Assert;

import mt_pipeline.mbrInterface.BestDistinctYieldDerivationListComputer;
import mt_pipeline.mbrInterface.JoshuaNBestMinRiskRerankerInterface;
import mt_pipeline.mbrInterface.MergedDerivationListComputer;
import mt_pipeline.mbrInterface.MosesToJoshuaNBestListConverter;
import mt_pipeline.mt_config.MTConfigFile;
import mt_pipeline.mt_config.SystemSpecificSupportObjectsFactory;
import mt_pipeline.tuning.Tuner;
import util.FileUtil;
import util.LinuxInteractor;

public class DecoderInterface extends MTComponentCreater {

	private final MTSystem mtSystem;
	private final DecoderCommandCreater decoderCommandCreater;
	private final Tuner tuner;

	protected DecoderInterface(MTConfigFile theConfig, MTSystem mtSystem,
			DecoderCommandCreater decoderCommandCreater,
			Tuner tuner) {
		super(theConfig);
		this.mtSystem = mtSystem;
		this.decoderCommandCreater = decoderCommandCreater;
		this.tuner = tuner;
	}

	public static DecoderInterface createDecoderInterface(
			MTConfigFile theConfig, MTSystem mtSystem) {
		SystemSpecificSupportObjectsFactory systemSpecificSupportObjectsFactory = MTConfigFile
				.createSystemSpecificSupportObjectsFactory(theConfig);
		return new DecoderInterface(theConfig, mtSystem,
				DecoderCommandCreater
						.createDecoderCommandCreaterFromConfig(theConfig),
				systemSpecificSupportObjectsFactory
						.createTunerFromConfig(theConfig));
	}

	private void exportJoshuaLocation() {
		String exportCommand = "export JOSHUA="
				+ theConfig.decoderConfig.getJoshuaHomeDir();
		LinuxInteractor.executeCommand(exportCommand, true);
	}

	private void makeResultsFolder(String category) {
		FileUtil.createFolderIfNotExisting(theConfig.foldersConfig
				.getResultsBaseFolder());
		FileUtil.createFolderIfNotExisting(theConfig.foldersConfig
				.getResultsFolderCategory(category));
	}

	public static String joshuaDecodeOutputSpecification(
			MTConfigFile theConfig, String category, boolean useTunedParameters) {
		String location = theConfig.filesConfig.getDecoderOutputFilePath(
				category, useTunedParameters);
		return "  >  " + location;
	}

	private String filterOneBestCommand(String category,
			boolean useTunedParameters) {
		String decoderOutputFilePath = theConfig.filesConfig
				.getDecoderOutputFilePath(category, useTunedParameters);
		return filterOneBestCommand(decoderOutputFilePath, category,
				useTunedParameters);
	}

	private String filterOneBestCommandCrunching(String category,
			boolean useTunedParameters) {
		String decoderOutputFilePath = theConfig.filesConfig
				.getDecoderNBestPostProcessedOutputFilePath(category,
						useTunedParameters);
		return filterOneBestCommand(decoderOutputFilePath, category,
				useTunedParameters);
	}

	private String filterOneBestCommand(String nBestListFilePath,
			String category, boolean useTunedParameters) {
		String result = "";
		String oneBestFilePath = theConfig.filesConfig
				.getOneBestOutputFilePath(category, useTunedParameters);
		result += theConfig.decoderConfig.baseJoshuaJavaCall()
				+ " joshua.util.ExtractTopCand " + nBestListFilePath + " "
				+ oneBestFilePath;
		return result;
	}

	private void copyDecoderOutputToRightLocation(String category,
			boolean useTunedParameters) {
		String decoderOutputFilePath = theConfig.filesConfig
				.getDecoderOutputFilePath(category, useTunedParameters);
		String oneBestFilePath = theConfig.filesConfig
				.getOneBestOutputFilePath(category, useTunedParameters);
		FileUtil.copyFile(decoderOutputFilePath, oneBestFilePath);
	}

	private void copyNBestMBRResultFileToRightLoaction(String category,
			boolean useTunedParameters) {
		String decoderOutputFilePath = theConfig.filesConfig
				.getDecoderNBestMBRRerankingOutputFilePath(category,
						useTunedParameters);
		String oneBestFilePath = theConfig.filesConfig
				.getOneBestOutputFilePath(category, useTunedParameters);
		FileUtil.copyFile(decoderOutputFilePath, oneBestFilePath);
	}

	private void extractTargetSentencesFromCrunchingOutputAndCopyResultToRightLocation(
			String category, boolean useTunedParameters) {
		runExternalCommandUsingPreSpecifiedJavanBinDir(
				filterOneBestCommandCrunching(category, useTunedParameters),
				false);
	}

	public void runTuning() {
		System.out.println("DecoderInterface.runTuning() called...");
		System.out.println(tuner.getTuningCommand());
		// LinuxInteractor.executeCommand(tuningCommandCreater.getTuningCommand(),
		// new ArrayList<util.Pair<String>>(), true);
		tuner.runTuning();
		System.out.println("DecoderInterface.runTuning() completed...");
	}

	private String getTestConfigFilePath(boolean useTunedParameters) {
		if (useTunedParameters) {
			return theConfig.filesConfig.getDecoderTestTunedConfigFileName();
		} else {
			return theConfig.systemSpecificConfig
					.getDecoderTestConfigFileName();
		}
	}

	private String getDevConfigFilePath(boolean useTunedParameters) {
		if (useTunedParameters) {
			return theConfig.systemSpecificConfig.getDevTunedConfigFileName();
		} else {
			return theConfig.systemSpecificConfig
					.getDecoderMertConfigFileName();
		}
	}

	private String getConfigFilePath(boolean useTunedParameters, String category) {
		if (category.equals(MTConfigFile.DEV_CATEGORY)) {
			return getDevConfigFilePath(useTunedParameters);
		} else if (category.equals(MTConfigFile.TEST_CATEGORY)) {
			return getTestConfigFilePath(useTunedParameters);
		} else {
			throw new RuntimeException("Error : unrecognized data category");
		}
	}

	private boolean useDecoderOutputReranking() {
		return theConfig.decoderConfig.useDecoderOutputReranking();
	}

	private boolean useNBestListMBR() {
		return theConfig.decoderConfig.useNBestListMBR();
	}

	private boolean useCrunching() {
		return theConfig.decoderConfig.useCrunching();
	}

	private void performNBestListPostProcessing(boolean useTunedParameters,
			String category) {
		String nBestFileJoshuaFormat = theConfig.filesConfig
				.getDecoderNBestOutputFilePathJoshuaFormat(category,
						useTunedParameters);
		String nBestFilePostProcessed = theConfig.filesConfig
				.getDecoderNBestPostProcessedOutputFilePath(category,
						useTunedParameters);
		MergedDerivationListComputer mergedDerivationListComputer = BestDistinctYieldDerivationListComputer
				.createMergedDerivationListComputer(theConfig,
						nBestFileJoshuaFormat, nBestFilePostProcessed);
		mergedDerivationListComputer.writeMergedDerivationList();
	}

	private void performMinimumBayesRiskReranking(boolean useTunedParameters,
			String category) {
		String nBestFilePostProcessed = theConfig.filesConfig
				.getDecoderNBestPostProcessedOutputFilePath(category,
						useTunedParameters);
		JoshuaNBestMinRiskRerankerInterface joshuaNBestMinRiskRerankerInterface = JoshuaNBestMinRiskRerankerInterface
				.createJoshuaNBestMinRiskRerankerInterface(theConfig,
						nBestFilePostProcessed, theConfig.filesConfig
								.getDecoderNBestMBRRerankingOutputFilePath(
										category, useTunedParameters));
		joshuaNBestMinRiskRerankerInterface.performReranking();
	}

	private void performRerankingInputPreparationAndReranking(
			boolean useTunedParameters, String category) {
		performNBestListPostProcessing(useTunedParameters, category);

		if (this.useNBestListMBR()) {
			performMinimumBayesRiskReranking(useTunedParameters, category);
			copyNBestMBRResultFileToRightLoaction(category, useTunedParameters);
		} else {
			Assert.assertTrue(useCrunching());
			// In case of Crunching, the NBestListPostProcessing itself is the
			// reranking,
			// the resulting file contains one translation, with the weights etc
			// per
			// source sentence. We extract only the target yield and copy the
			// result
			// to the right location
			extractTargetSentencesFromCrunchingOutputAndCopyResultToRightLocation(
					category, useTunedParameters);
		}
	}

	private void processDecoderOutput(boolean useTunedParameters,
			String category) {

		if (useDecoderOutputReranking()) {
			if (mtSystem.systemIdentity.isJoshuaSystem()) {

				FileUtil.renameFile(theConfig.filesConfig
						.getDecoderNBestOutputFilePathSystemSpecific(category,
								useTunedParameters), theConfig.filesConfig
						.getDecoderNBestOutputFilePathJoshuaFormat(category,
								useTunedParameters));

			} else if (mtSystem.systemIdentity.isMosesSystem()) {
				if (useDecoderOutputReranking()) {
					String nBestFileJoshuaFormat = theConfig.filesConfig
							.getDecoderNBestOutputFilePathJoshuaFormat(
									category, useTunedParameters);
					MosesToJoshuaNBestListConverter mosesToJoshuaNBestListConverter = MosesToJoshuaNBestListConverter
							.createMosesToJoshuaNBestListConverter(
									theConfig.filesConfig
											.getDecoderNBestOutputFilePathSystemSpecific(
													category,
													useTunedParameters),
									nBestFileJoshuaFormat);
					mosesToJoshuaNBestListConverter.writeConvertedNBestFile();
				}
			}
		}

		else {
			if (mtSystem.systemIdentity.isJoshuaSystem()) {
				runExternalCommandUsingPreSpecifiedJavanBinDir(
						filterOneBestCommand(category, useTunedParameters),
						false);

			} else if (mtSystem.systemIdentity.isMosesSystem()) {
				copyDecoderOutputToRightLocation(category, useTunedParameters);

			} else {
				throw new RuntimeException("Unrecognized system type");
			}
		}
	}

	private void decodeSentences(boolean useTunedParameters, String category) {
		makeResultsFolder(category);
		exportJoshuaLocation();

		String resultFileName = theConfig.filesConfig.getOneBestOutputFilePath(
				category, useTunedParameters);
		if (!fileAlreadyExistsAndRigthSize(
				theConfig.filesConfig.getEnrichedSourceFilePath(category),
				resultFileName)) {
			runExternalCommandUsingPreSpecifiedJavanBinDir(
					decoderCommandCreater.getDecodeCommand(category,
							getConfigFilePath(useTunedParameters, category),
							useTunedParameters), true);
			processDecoderOutput(useTunedParameters, category);
		} else {
			System.out
					.println("Info : "
							+ resultFileName
							+ " already exists and has the expected size. Using this existing file");
		}
	}

	private void decodeDevSentences(boolean useTunedParameters) {
		final String category = MTConfigFile.DEV_CATEGORY;
		decodeSentences(useTunedParameters, category);
	}

	private void decodeTestSentences(boolean useTunedParameters) {
		final String category = MTConfigFile.TEST_CATEGORY;
		decodeSentences(useTunedParameters, category);
	}

	private void rerankDevSentences(boolean useTunedParameters) {
		final String category = MTConfigFile.DEV_CATEGORY;
		performRerankingInputPreparationAndReranking(useTunedParameters,
				category);
	}

	private void rerankTestSentences(boolean useTunedParameters) {
		final String category = MTConfigFile.TEST_CATEGORY;
		performRerankingInputPreparationAndReranking(useTunedParameters,
				category);
	}

	public void decodeDevAndTestSentences(boolean decodeWithTunedParameters) {

		// Decode with tuned parameters
		if (decodeWithTunedParameters) {
		    	decodeTestSentences(true);
			decodeDevSentences(true);			
		}
		
		// Decode without tuned parameters
		decodeTestSentences(false);
		decodeDevSentences(false);
		
	}

	public void rerankDevAndTestSentences() {
		if (useDecoderOutputReranking()) {
			rerankDevSentences(false);
			rerankTestSentences(false);
			rerankDevSentences(true);
			rerankTestSentences(true);
		}
	}

}
