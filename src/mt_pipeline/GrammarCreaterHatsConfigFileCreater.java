package mt_pipeline;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import alignmentStatistics.MultiThreadComputationParameters;
import coreContextLabeling.EbitgChartBuilderCoreContextLabeled;
import util.Pair;
import grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagLabelingSettings;
import grammarExtraction.chiangGrammarExtraction.GrammarExtractionConstraints;
import grammarExtraction.chiangGrammarExtraction.LabelSideSettings;
import grammarExtraction.chiangGrammarExtraction.MultiThreadGrammarExtractorTestFiltered;
import grammarExtraction.coreContextLabelGrammarExtraction.CCLRuleGapCreater;
import grammarExtraction.grammarExtractionFoundation.GrammarStructureSettings;
import grammarExtraction.grammarExtractionFoundation.MultiThreadGrammarExtractor;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.labelSmoothing.LabeledRulesSmoother;
import grammarExtraction.samtGrammarExtraction.SAMTGrammarExtractionConstraints;
import mt_pipeline.mt_config.MTConfigFile;
import mt_pipeline.mt_config.MTDecoderConfig;
import mt_pipeline.mt_config.MTFilesConfig;
import mt_pipeline.mt_config.MTGrammarExtractionConfig;

public class GrammarCreaterHatsConfigFileCreater {
    private final List<Pair<String>> mainGrammarConfigPairs;
    private final List<Pair<String>> smoothingGrammarConfigPairs;

    private GrammarCreaterHatsConfigFileCreater(List<Pair<String>> mainGrammarConfigPairs,
	    List<Pair<String>> smoothingGrammarConfigPairs) {
	this.mainGrammarConfigPairs = mainGrammarConfigPairs;
	this.smoothingGrammarConfigPairs = smoothingGrammarConfigPairs;
    }

    public static GrammarCreaterHatsConfigFileCreater createHieroGrammarCreaterHatsConfigFileCreater(
	    String mainGrammarSourceFilterFileName, String smoothingGrammarSourceFilterFileName,
	    MTConfigFile theConfig) {
	List<Pair<String>> mainGrammarConfigPairs = getHieroMainGrammarExtractionConfigPairs(
		mainGrammarSourceFilterFileName, theConfig);
	List<Pair<String>> smoothingGrammarConfigPairs = getHieroSmoothingGrammarExtractionConfigPairs(
		smoothingGrammarSourceFilterFileName, theConfig);
	return new GrammarCreaterHatsConfigFileCreater(mainGrammarConfigPairs,
		smoothingGrammarConfigPairs);
    }

    public static GrammarCreaterHatsConfigFileCreater createComplexLabelsGrammarCreaterHatsConfigFileCreater(
	    String mainGrammarSourceFilterFileName, String smoothingGrammarSourceFilterFileName,
	    String sourceFilterTagFilesPath, MTConfigFile theConfig) {
	List<Pair<String>> mainGrammarConfigPairs = getComplexLabelsGrammarExtractionConfigPairs(
		mainGrammarSourceFilterFileName, sourceFilterTagFilesPath, theConfig);
	List<Pair<String>> smoothingGrammarConfigPairs = getComplexLabelsSmoothingGrammarExtractionConfigPairs(
		smoothingGrammarSourceFilterFileName, sourceFilterTagFilesPath, theConfig);
	return new GrammarCreaterHatsConfigFileCreater(mainGrammarConfigPairs,
		smoothingGrammarConfigPairs);
    }

    private static boolean useCaching(MTConfigFile theConfig) {
	return theConfig.getStringIfPresentOrReturnDefault(
		MultiThreadGrammarExtractor.UseCachingProperty, "true").equals("true");
    }

    private static String getTrainCorpusLocation(MTConfigFile theConfig) {
	return theConfig.foldersConfig.getCategoryDataFolderName(MTConfigFile.TRAIN_CATEGORY);
    }

    private static List<Pair<String>> getHieroMainGrammarExtractionFileConfigPairs(
	    String grammarSourceFilterFileName, MTConfigFile theConfig) {
	List<Pair<String>> result = new ArrayList<Pair<String>>();
	result.add(new Pair<String>("sourceFileName", theConfig.filesConfig
		.getSourceEnrichedFileName()));

	return result;
    }

    private static List<Pair<String>> getHieroSmoothingGrammarExtractionFileConfigPairs(
	    String smoothingGrammarSourceFilterFileName, MTConfigFile theConfig) {
	List<Pair<String>> result = new ArrayList<Pair<String>>();
	result.add(new Pair<String>("sourceFileName", theConfig.filesConfig.getSourceSubFileName()));
	return result;
    }

    private static void addBoudaryTagExtractionProperties(List<Pair<String>> result,
	    MTConfigFile theConfig) {
	result.add(new Pair<String>(MultiThreadComputationParameters.SOURCE_TAG_FILE_NAME_PROPERTY,
		MTFilesConfig.getSourceTagFileName()));
	result.add(new Pair<String>(MultiThreadComputationParameters.TARGET_TAG_FILE_NAME_PROPERTY,
		MTFilesConfig.getTargetTagFileName()));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(BoundaryTagLabelingSettings.SOURCE_INSIDE_TAGS_TYPE_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(BoundaryTagLabelingSettings.SOURCE_OUTSIDE_TAGS_TYPE_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(BoundaryTagLabelingSettings.TARGET_INSIDE_TAGS_TYPE_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(BoundaryTagLabelingSettings.TARGET_OUTSIDE_TAGS_TYPE_PROPERTY));
    }

    private static List<Pair<String>> getHieroGrammareExtractionSharedSettingsFileConfigPairs(
	    String grammarSourceFilterFileName, MTConfigFile theConfig) {
	List<Pair<String>> result = new ArrayList<Pair<String>>();
	result.add(theConfig
		.getConfigPairWithPresenceCheck(SystemIdentity.MAX_SOURCE_AND_TARGET_LENGTH_PROPERTY));
	result.add(new Pair<String>("targetFileName", theConfig.filesConfig.getTargetSubFileName()));
	result.add(new Pair<String>(
		MTGrammarExtractionConfig.GRAMMAR_SOURCE_FILTER_PATH_CONFIG_PROPERTY,
		grammarSourceFilterFileName));
	result.add(new Pair<String>("corpusLocation", getTrainCorpusLocation(theConfig)));
	result.add(new Pair<String>("alignmentsFileName", theConfig.filesConfig
		.getAlignmentsSubFileName()));
	result.add(new Pair<String>(MultiThreadGrammarExtractor.UseCachingProperty, new Boolean(
		useCaching(theConfig)).toString()));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(MTGrammarExtractionConfig.USE_SOURCE_SIDE_LABELS_IN_RULE_FILTERING_CONFIG_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(SAMTGrammarExtractionConstraints.ALLOW_DANGLING_SECONDNOND_TERMINAL_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(SAMTGrammarExtractionConstraints.ALLOW_CONSECUTIVE_NONTERMINALS_IN_NON_ABSTRACTRULES_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(SAMTGrammarExtractionConstraints.ALLOW_SOURCE_ABSTRACT_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(SAMTGrammarExtractionConstraints.ALLOW_TARGET_WORDS_WITHOUT_SOURCE_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(SAMTGrammarExtractionConstraints.USE_EXTRA_SAMT_FEATURES_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(SAMTGrammarExtractionConstraints.ALLOW_DOUBLE_PLUS_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(SAMTGrammarExtractionConstraints.ALLOW_POS_TAG_LABEL_FALLBACK_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(GrammarExtractionConstraints.UNARY_CATEGORY_HANDLER_TYPE_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(EbitgChartBuilderCoreContextLabeled.CoreContextLabelTableFileNameConfigurationProperty));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(CCLRuleGapCreater.MaxiumAllowedCoreContextLabelAmbituityRankProperty));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(GrammarStructureSettings.WRITE_PLAIN_HIERO_RULES_FOR_SMOOTHING));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(LabelSideSettings.LABEL_SIDE_SETTING_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(LabeledRulesSmoother.LABEL_SMOOTHING_TYPE_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(LabeledRulesSmoother.STRIP_SOURCE_PART_LABELS_FOR_SMOOTHING_DOUBLE_LABELD_RULES));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(GrammarExtractionConstraints.USE_REORDERING_LABEL_EXTENSION_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(GrammarExtractionConstraints.USE_REORDERING_LABEL_BINARY_FEATURES_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(GrammarExtractionConstraints.REORDERING_FEATURE_SET_TYPE_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(GrammarExtractionConstraints.REORDER_LABEL_TYPE_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(MTGrammarExtractionConfig.USE_CANONICAL_FORM_LABELED_RULES_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(MTGrammarExtractionConfig.USE_ONLY_HIERO_WEIGHTS_FOR_CANONICAL_FORM_LABELED_RULES_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(LabeledRulesSmoother.REORDERING_LABEL_RULE_SMOOTHING_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(LabeledRulesSmoother.REORDERING_LABEL_RULE_SMOOTHING_RULE_WEIGHT_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(GrammarExtractionConstraints.REORDER_LABEL_HIERO_RULES_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(GrammarExtractionConstraints.REORDER_LABEL_PHRASE_PAIRS_PROPERTY));
	result.add(theConfig
		.getConfigPairWithPresenceCheck(GrammarExtractionConstraints.USE_MOSES_HIERO_UNKNOWN_WORD_RULE_VARIANTS_PROPERTY));
	result.add(new Pair<String>(GrammarStructureSettings.USE_PACKED_GRAMMARS_PROPERTY,
		new Boolean(GrammarStructureSettings.usePackedGrammars(theConfig)).toString()));
	result.add(theConfig.getConfigPairWithPresenceCheck(MTConfigFile.MT_SYSTEM_TYPE_PROPERTY));
	
	
	boolean useFuzzyMatching = MTDecoderConfig.useFuzzyMAtching(theConfig);
	if(useFuzzyMatching){
	    result.add(new Pair<String>(MTDecoderConfig.FUZZY_MATCHING_PROPERTY_NAME, new Boolean(
		useFuzzyMatching).toString()));
	}
	addBoudaryTagExtractionProperties(result, theConfig);
	return result;
    }

    private static List<Pair<String>> getHieroMainGrammarExtractionConfigPairs(
	    String grammarSourceFilterFileName, MTConfigFile theConfig) {
	List<Pair<String>> result = getHieroMainGrammarExtractionFileConfigPairs(
		grammarSourceFilterFileName, theConfig);
	result.addAll(getHieroGrammareExtractionSharedSettingsFileConfigPairs(
		grammarSourceFilterFileName, theConfig));
	return result;
    }

    private static List<Pair<String>> getHieroSmoothingGrammarExtractionConfigPairs(
	    String smoothingGrammarSourceFilterFileName, MTConfigFile theConfig) {
	List<Pair<String>> result = getHieroSmoothingGrammarExtractionFileConfigPairs(
		smoothingGrammarSourceFilterFileName, theConfig);
	result.addAll(getHieroGrammareExtractionSharedSettingsFileConfigPairs(
		smoothingGrammarSourceFilterFileName, theConfig));
	return result;
    }

    private static List<Pair<String>> getSAMTSpecificConfigPairs(String grammarSourceFilterFileName, String sourceFilterTagFilesPath, MTConfigFile theConfig) {
		List<Pair<String>> result = new ArrayList<Pair<String>>();
		
		
		// Non-uniform glue rule weights are only applicable if we are not using fuzzy matching
		boolean useNonUniformGlueRules = theConfig.getBooleanIfPresentOrReturnDefault(SAMTGrammarExtractionConstraints.USE_NON_UNINIFORM_GLUE_RULE_WEIGHTS_PROPERTY, true);
		
		if(theConfig.decoderConfig.useFuzzyMatchingDecodingWithConsistencyCheck()){		
		 
		   if(useNonUniformGlueRules){
		       String errorString = "Trying to use fuzzy matching with non-uniform glue rules, which is inconsistent. " +
		   "Please change the value of " + SAMTGrammarExtractionConstraints.USE_NON_UNINIFORM_GLUE_RULE_WEIGHTS_PROPERTY + " to false";
		       throw new RuntimeException(errorString);
		   }
		}
			    		 	
		result.add(new Pair<String>(SAMTGrammarExtractionConstraints.USE_NON_UNINIFORM_GLUE_RULE_WEIGHTS_PROPERTY, theConfig
			.getValueWithPresenceCheck(SAMTGrammarExtractionConstraints.USE_NON_UNINIFORM_GLUE_RULE_WEIGHTS_PROPERTY)));
		
		
		result.add(new Pair<String>(MTGrammarExtractionConfig.GRAMMAR_SOURCE_FILTER_TAGS_FILE_PATH_CONFIG_PROPERTY, sourceFilterTagFilesPath));
		result.add(new Pair<String>("targetParseFileName", "targetParse.txt"));
		return result;
	}

    private static List<Pair<String>> getComplexLabelsGrammarExtractionConfigPairs(
	    String grammarSourceFilterFileName, String smoothingGrammarSourceFilterFileName,
	    MTConfigFile theConfig) {
	List<Pair<String>> result = getHieroMainGrammarExtractionConfigPairs(
		grammarSourceFilterFileName, theConfig);
	result.addAll(getSAMTSpecificConfigPairs(grammarSourceFilterFileName,
		smoothingGrammarSourceFilterFileName, theConfig));
	return result;
    }

    private static List<Pair<String>> getComplexLabelsSmoothingGrammarExtractionConfigPairs(
	    String grammarSourceFilterFileName, String smoothingGrammarSourceFilterFileName,
	    MTConfigFile theConfig) {
	List<Pair<String>> result = getHieroSmoothingGrammarExtractionConfigPairs(
		grammarSourceFilterFileName, theConfig);
	result.addAll(getSAMTSpecificConfigPairs(grammarSourceFilterFileName,
		smoothingGrammarSourceFilterFileName, theConfig));
	return result;
    }

    protected void writeHatsGrammarExtractionConfigFiles(
	    String mainGrammarExtractionConfigFileName,
	    String smoothingGrammarExtractionConfigFileName) {
	writeHatsGrammarExtractionConfigFile(mainGrammarExtractionConfigFileName,
		mainGrammarConfigPairs);
	writeHatsGrammarExtractionConfigFile(smoothingGrammarExtractionConfigFileName,
		smoothingGrammarConfigPairs);
    }

    protected static void writeHatsGrammarExtractionConfigFile(String outFilePath,
	    List<Pair<String>> configValuePairs) {
	BufferedWriter outputWriter;
	try {
	    outputWriter = new BufferedWriter(new FileWriter(outFilePath));

	    for (Pair<String> configPair : configValuePairs) {
		outputWriter.write(configPair.getFirst() + " = " + configPair.getSecond() + "\n");
	    }
	    outputWriter.close();
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }
}
