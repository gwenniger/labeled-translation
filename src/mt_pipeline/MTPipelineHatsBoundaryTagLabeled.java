package mt_pipeline;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import mt_pipeline.grammarExtraction.ExternalProcessGrammarCreaterBoundaryTagLabeled;
import mt_pipeline.mt_config.MTConfigFile;

public class MTPipelineHatsBoundaryTagLabeled extends MTPipelineHatsComplexLabels {
	public static final String SystemName = "JoshuaHatsBoundaryTagLabeled";

	protected MTPipelineHatsBoundaryTagLabeled() {
	};

	protected MTPipelineHatsBoundaryTagLabeled(MTConfigFile theConfig, String configFilePath, int noModelParameters, boolean extractGrammarsAsExternalProcess, MTSystem mtSystem) {
		super(theConfig, configFilePath, noModelParameters, extractGrammarsAsExternalProcess, mtSystem);
	}

	@Override
	protected void extractGrammars() {
		long startTime = System.currentTimeMillis();

		if (extractGrammarAsExternalProcess()) {
			ExternalProcessGrammarCreaterBoundaryTagLabeled.extractBoundaryTaggedLabeledGrammarsAsExternalProcess(getConfigFilePath());
		} else {
			GrammarCreaterHats.extractFilteredHatsBoundaryTagLabeledGrammarsPrallel(getTheConfig(),getSystemIdentity());
		}

		long passedTime = (System.currentTimeMillis() - startTime) / 1000;
		System.out.println(">>> HATs grammar extraction took: " + passedTime + "seconds");
	}

	@Override
	public MTPipeline createMTPipeline(String configFilePath) throws RuntimeException {
		MTConfigFile mtConfigFile = MTConfigFile.createMtConfigFile(configFilePath, SystemName);
		SystemIdentity systemIdentity = SystemIdentity.createSystemIdentity(mtConfigFile);
		MTSystem mtSystem = MTSystem.createMTSystem(mtConfigFile, systemIdentity);
		return new MTPipelineHatsBoundaryTagLabeled(mtConfigFile, configFilePath, mtSystem.noModelParameters(mtConfigFile, SystemName),
				MTPipeline.extractGrammarsAsExternalProcess(configFilePath), mtSystem);
	}

}
