package mt_pipeline.mbrInterface;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.List;

import mt_pipeline.evaluation.SentencesNBestCandidatesListIterator;

public class CrunchingDerivationsListComputer extends MergedDerivationListComputer {

	protected CrunchingDerivationsListComputer(SentencesNBestCandidatesListIterator sentencesNBestCandidatesListIterator, BufferedWriter outputWriter, DerivationCombiner derivationCombiner) {
		super(sentencesNBestCandidatesListIterator, outputWriter, derivationCombiner);
	}

	public static CrunchingDerivationsListComputer createCrunchingDerivationsListComputer(SentencesNBestCandidatesListIterator sentencesNBestCandidatesListIterator, BufferedWriter outputWriter) {
		return new CrunchingDerivationsListComputer(sentencesNBestCandidatesListIterator, outputWriter, new SummationDerivationCombiner());
	}

	@Override
	protected int getNumberTranslationsPerSentenceToOutput(List<WeightedTranslation> weightedUniqueYieldBestDerivations) {
		return 1;
	}

}
