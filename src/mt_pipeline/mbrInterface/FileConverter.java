package mt_pipeline.mbrInterface;

import java.io.BufferedReader;
import java.io.BufferedWriter;

public abstract class FileConverter {

	protected final BufferedReader inputReader;
	protected final BufferedWriter outputWriter;

	protected FileConverter(BufferedReader inputReader, BufferedWriter outputWriter) {
		this.inputReader = inputReader;
		this.outputWriter = outputWriter;
	}
}
