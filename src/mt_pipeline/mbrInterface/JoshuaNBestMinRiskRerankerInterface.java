package mt_pipeline.mbrInterface;

import util.LinuxInteractor;
import mt_pipeline.MTComponentCreater;
import mt_pipeline.mt_config.MTConfigFile;

public class JoshuaNBestMinRiskRerankerInterface extends MTComponentCreater {
	private static final String JOSHUA_NBEST_MIN_RISK_RERANKER_PROGRAM_NAME = "joshua.decoder.NbestMinRiskReranker";
	private static final String DEFAULT_ARGUMENTS_NBEST_MIN_RISK_RERANKER = "false 1";

	protected final String inputFilePath;
	protected final String resultFilePath;

	protected JoshuaNBestMinRiskRerankerInterface(MTConfigFile theConfig, String inputFilePath, String resultFilePath) {
		super(theConfig);
		this.inputFilePath = inputFilePath;
		this.resultFilePath = resultFilePath;
	}

	public static JoshuaNBestMinRiskRerankerInterface createJoshuaNBestMinRiskRerankerInterface(MTConfigFile theConfig, String inputFilePath, String outputFilePath) {

		return new JoshuaNBestMinRiskRerankerInterface(theConfig, inputFilePath, outputFilePath);

	}

	private String rerankerCommand() {
		return "cat " + inputFilePath + " | " + this.theConfig.decoderConfig.baseJoshuaJavaCall() + " " + JOSHUA_NBEST_MIN_RISK_RERANKER_PROGRAM_NAME + " " + DEFAULT_ARGUMENTS_NBEST_MIN_RISK_RERANKER + " "
				+ theConfig.decoderConfig.getNumTunerThreads() + " > " + this.resultFilePath;
	}

	public void performReranking() {
		String command = rerankerCommand();
		System.out.println("command: " + command);
		// System.exit(0);
		LinuxInteractor.executeExtendedCommandDisplayOutput(command);
	}

}
