package mt_pipeline.mbrInterface;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

//import java.util.regex.Pattern;

public class JosuhuaUnknownWordSymbolRemover extends FileConverter{
	private static String JOSHUA_UNKNOWN_WORD_SUFFIX = "_OOV";
	private static String JOSHUA_UNLNOWN_WORD_PATTERN_STRING = "\\s(\\S)+" + JOSHUA_UNKNOWN_WORD_SUFFIX;
	// private static Pattern JOSHUA_UNKNOWN_WORD_PATTERN = Pattern.compile(JOSHUA_UNLNOWN_WORD_PATTERN_STRING);

	protected JosuhuaUnknownWordSymbolRemover(BufferedReader inputReader, BufferedWriter outputWriter) {
		super(inputReader, outputWriter);
	}

	public static JosuhuaUnknownWordSymbolRemover createJosuhuaUnknownWordSymbolRemover(String inputFilePath, String outputFilePath) {
		BufferedReader inputReader;
		BufferedWriter outputWriter;
		try {
			inputReader = new BufferedReader(new FileReader(inputFilePath));
			outputWriter = new BufferedWriter(new FileWriter(outputFilePath));
			return new JosuhuaUnknownWordSymbolRemover(inputReader, outputWriter);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public static String createNBestStringWithoutUnknownWordSymbols(String inputString) {
		return inputString.replaceAll(JOSHUA_UNLNOWN_WORD_PATTERN_STRING, "");
	}

	public void writeNBestFileWithoutUnknownWordSymbols() {
		String inputString;

		try {
			while ((inputString = inputReader.readLine()) != null) {
				String resultString = createNBestStringWithoutUnknownWordSymbols(inputString);
				System.out.println("input String: " + inputString);
				System.out.println("result String: " + resultString);
				outputWriter.write(resultString);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
