package mt_pipeline.mbrInterface;

import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import junit.framework.Assert;

public class WeightedTranslation implements Comparable<WeightedTranslation> {
	private final int sentenceNumber;
	private String targetYield;
	private List<Double> featureWeights;
	private double totalWeight;

	private WeightedTranslation(int sentenceNumber, String targetYield, List<Double> featureWeights, double totalWeight) {
		this.sentenceNumber = sentenceNumber;
		this.targetYield = targetYield;
		this.featureWeights = featureWeights;
		this.totalWeight = totalWeight;
	}

	public static WeightedTranslation createWeightedTranslation(String weightedTranslationString) {
		String[] parts = JoshuaStyle.spitOnRuleSeparator(weightedTranslationString);
		Assert.assertEquals(4, parts.length);
		int sentenceNumber = Integer.parseInt(parts[0]);
		String targetSentence = parts[1];
		List<Double> featureWeights = getWeightsListFromWeightsListString(parts[2]);
		double totalWeigth = Double.parseDouble(parts[3]);
		return new WeightedTranslation(sentenceNumber, targetSentence, featureWeights, totalWeigth);
	}

	private static List<Double> getWeightsListFromWeightsListString(String weightsListString) {
		List<Double> result = new ArrayList<>();
		String[] weightStrings = JoshuaStyle.spitOnSingleWhiteSpace(weightsListString);
		for (String weightString : weightStrings) {
			result.add(Double.parseDouble(weightString));
		}
		return result;
	}

	/**
	 * The log Sum exponent trick formula. Compute the log of a sum of exponents, which leads to underflow under normal computation, since the log probabilities
	 * that are combined are too small.
	 * The trick boils down to rewriting the formula in order to take out the largest 
     * log probability, decreasing the magnitude of the other negative exponents in the process
     * This is done by using the following equivalences:
     * 
     * log (Sum_t exp(a_t))  = log (Sum_t exp(a_t) * exp(A - A)) 
     * = log (Sum_t exp(a_t - A) * exp(A))    <==== Use exp(a) * exp(b) = exp(a+b)
     * = log(exp(A) * Sum_t exp(a_t - A) )  <== simply change order elements 
     * = log(exp(A)) +  log(Sum_t exp(a_t - A) ))  <==== Use exp(a) * exp(b) = exp(a+b)
     * = A + log (Sum_t exp(a_t - A))   <===== use log(exp(A)) == A
     * 
     * where A = max{a}   (i.e. the maximum log probability)
     * 
	 * 
	 * See : http://makarandtapaswi.wordpress.com/2012/07/18/log-sum-exp-trick/ machineintelligence.tumblr.com/post/4998477107/the-log-sum-exp-trick
	 * http://www.themathpage.com/aprecalc/logarithms.htm#laws
	 * 
	 * @param logProbabilities
	 *            : A list of log probabilities (negative values)
	 * @return
	 */
	public static double logSumExponent(List<Double> logProbabilities) {
		double result = 0;
		
		// We have to take the maximum value and take it out of the sum using this trick
		// To realize why this is sensible, if we would take the minimum value, and 
		// we would have 2 negative values e.g. -1 and -1000 , we would end up 
		// taking exp(-1 - -1000) = exp(999) and we would end up with overflow.
		// Taking the maximum value assures that the absolute value of all exponents 
		// decreases, and we don't get this case of flipping from negative to positive,
		// leading to overflow and generally high imprecision
		double largestLogProb = Collections.max(logProbabilities);

		result = largestLogProb;

		double sumOfExponents = 0;
		for (Double logProb : logProbabilities) {
			sumOfExponents += Math.exp(logProb - largestLogProb);
		}
		result += Math.log(sumOfExponents);
		//Assert.assertTrue(result >= largestLogProb);
		return result;
	}

	public static double logSumExponentBase10(List<Double> logProbabilities) {
		double result = 0;
		
		// We have to take the maximum value and take it out of the sum using this trick
		// To realize why this is sensible, if we would take the minimum value, and 
		// we would have 2 negative values e.g. -1 and -1000 , we would end up 
		// taking exp(-1 - -1000) = exp(999) and we would end up with overflow.
		// Taking the maximum value assures that the absolute value of all exponents 
		// decreases, and we don't get this case of flipping from negative to positive,
		// leading to overflow and generally high imprecision
		double largestLogProb = Collections.max(logProbabilities);

		result = largestLogProb;

		double sumOfExponents = 0;
		for (Double logProb : logProbabilities) {
			sumOfExponents += Math.pow(10,logProb - largestLogProb);
		}
		result += Math.log10(sumOfExponents);
		//Assert.assertTrue(result >= largestLogProb);
		return result;
	}
	
	/*
	 * Compute the combined weight of two log probabilities. To sum the probabilities we have to first go back from log-space to probability space by taking the
	 * exponent. We then sum the probabilities, and finally take the log again to come back to exponent space.
	 */
	private Double computeCombinedWeight(Double weight1, Double weight2) {

		// It was not clear at first which base to use for the log, but base E seemed not to 
		// work,  both Joshua and Moses seem to work with base 10

		//Double result = logSumExponent(Arrays.asList(weight1, weight2));
		Double result = logSumExponentBase10(Arrays.asList(weight1, weight2));
		
		if (result < weight1) {
			String errorString = "Error in computeCombinedWeight :  result got smaller than weight1: " + "weight1: " + weight1 + " weight2: " + weight2 + "result: " + result + " Math.exp(weight1)"
					+ Math.exp(weight1) + " Math.exp(weight2)" + Math.exp(weight2);
			throw new RuntimeException(errorString);
		}

		if (result < weight2) {
			String errorString = "Error in computeCombinedWeight :  result got smaller than weight2: " + "weight1: " + weight1 + " weight2: " + weight2 + "result: " + result + " Math.exp(weight1)"
					+ Math.exp(weight1) + " Math.exp(weight2)" + Math.exp(weight2);
			throw new RuntimeException(errorString);
		}

		return result;
	}

	public void addWeigt(WeightedTranslation weightedTranslation) {
		List<Double> newWeightsList = new ArrayList<>();
		Assert.assertEquals(this.featureWeights.size(), weightedTranslation.featureWeights.size());

		for (int i = 0; i < this.featureWeights.size(); i++) {
			Double newWeight = computeCombinedWeight(this.featureWeights.get(i), weightedTranslation.featureWeights.get(i));
			newWeightsList.add(newWeight);
		}
		this.featureWeights = newWeightsList;
		this.totalWeight = computeCombinedWeight(this.totalWeight, weightedTranslation.totalWeight);
	}

	private String getWeightsString() {
		String result = "";
		for (int i = 0; i < this.featureWeights.size() - 1; i++) {
			result += this.featureWeights.get(i) + " ";
		}
		if (this.featureWeights.size() > 0) {
			result += this.featureWeights.get(this.featureWeights.size() - 1);
		}
		return result;
	}

	public String getStringRepresentation() {
		String result = "";
		result += this.sentenceNumber;
		result += JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR + this.targetYield;
		result += JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR + getWeightsString();
		result += JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR + this.totalWeight;
		return result;
	}

	@Override
	public int compareTo(WeightedTranslation weightedTranslation) {
		return Double.compare(this.totalWeight, weightedTranslation.totalWeight);
	}

	public int getSentenceNumber() {
		return this.sentenceNumber;
	}

	public String getTargetYield() {
		return this.targetYield;
	}

	public double getTotalWeight() {
		return this.totalWeight;
	}
}
