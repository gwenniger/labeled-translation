package mt_pipeline.mbrInterface;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;
import mt_pipeline.evaluation.SentencesNBestCandidatesListIterator;
import mt_pipeline.mt_config.MTConfigFile;
import util.FileUtil;

public abstract class MergedDerivationListComputer {

	private final SentencesNBestCandidatesListIterator sentencesNBestCandidatesListIterator;
	private final BufferedWriter outputWriter;
	Map<String, WeightedTranslation> bestDistinctDerivationMap;
	private final DerivationCombiner derivationCombiner;

	protected MergedDerivationListComputer(SentencesNBestCandidatesListIterator sentencesNBestCandidatesListIterator, BufferedWriter outputWriter,DerivationCombiner derivationCombiner) {
		this.sentencesNBestCandidatesListIterator = sentencesNBestCandidatesListIterator;
		this.outputWriter = outputWriter;
		this.derivationCombiner = derivationCombiner;
	}

	public static MergedDerivationListComputer createMergedDerivationListComputer(MTConfigFile theConfig, String inputFilePath, String outputFilePath) {
		BufferedReader inputReader;
		BufferedWriter outputWriter;
		try {
			SentencesNBestCandidatesListIterator sentencesNBestCandidatesListIterator = SentencesNBestCandidatesListIterator.createSentencesNBestCandidatesListIterator(inputFilePath);
			outputWriter = new BufferedWriter(new FileWriter(outputFilePath));
			boolean useCrunching = theConfig.decoderConfig.useCrunching();
			if (useCrunching) {
				return CrunchingDerivationsListComputer.createCrunchingDerivationsListComputer(sentencesNBestCandidatesListIterator, outputWriter);
			} else {
				boolean sumDerivationsWithSameYield = theConfig.decoderConfig.sumDerivationsWithSameYieldForNBestMBR();
				DerivationCombiner derivationCombiner = DerivationCombiner.createDerivationCombiner(sumDerivationsWithSameYield);
				return new BestDistinctYieldDerivationListComputer(sentencesNBestCandidatesListIterator, outputWriter, theConfig.decoderConfig.getMaxNumUniqueTranslationsForNBestMBR(), derivationCombiner);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	protected abstract int getNumberTranslationsPerSentenceToOutput(List<WeightedTranslation> weightedUniqueYieldBestDerivations);

	private void writeBestDistinctDerivationsInMapToResult(int currentSentenceNumber) throws IOException {
		//System.out.println("BestDistinctYieldDerivationsListComputer: Writing combined derivations for sentence " + currentSentenceNumber);

		List<WeightedTranslation> weightedUniqueYieldBestDerivations = new ArrayList<>();
		weightedUniqueYieldBestDerivations.addAll(this.bestDistinctDerivationMap.values());

		// We sort with a reverse NumberedStringComparator, so that the derivations with the highest scores
		// (i.e. least negative) will come first in the resulting sorted list
		weightedUniqueYieldBestDerivations.sort(null);;
		// Sorting is ascending by default, so reverse the list to get the best translations in descending order
		Collections.reverse(weightedUniqueYieldBestDerivations);

		int maxNumTranslationsToOutput = getNumberTranslationsPerSentenceToOutput(weightedUniqueYieldBestDerivations);
		//System.out.println("maxNumTranslationsToOutput: " + maxNumTranslationsToOutput);
		//System.out.println("weightedUniqueYieldBestDerivations: "+ weightedUniqueYieldBestDerivations);
		// Write maximally maxNumUniqueTranslationsForNBestMBR unique translations per sentence
		for (int i = 0; i < maxNumTranslationsToOutput; i++) {
			WeightedTranslation weightedTranslation = weightedUniqueYieldBestDerivations.get(i);
			this.outputWriter.write(weightedTranslation.getStringRepresentation() + "\n");

		}
	}

	public void writeMergedDerivationList() {
		
		try {
			//while ((inputString = inputReader.readLine()) != null) {
		    	while(sentencesNBestCandidatesListIterator.hasNext())
		    	{
		    	    bestDistinctDerivationMap = new HashMap<String, WeightedTranslation>();
		    	    List<String> nBestCandidatesList = sentencesNBestCandidatesListIterator.next();
			
		    	    WeightedTranslation firsCandidate = WeightedTranslation.createWeightedTranslation(nBestCandidatesList.get(0));
		    	    int sentenceNumber = firsCandidate.getSentenceNumber();

		    	    for(String nBestCandidateString : nBestCandidatesList){
		    		WeightedTranslation weightedTranslation = WeightedTranslation.createWeightedTranslation(nBestCandidateString);
		    		derivationCombiner.addDerivationToMap(bestDistinctDerivationMap, weightedTranslation);
		    	    }		    	    
        		    writeBestDistinctDerivationsInMapToResult(sentenceNumber);
        			        			        		    	    
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			FileUtil.closeCloseableIfNotNull(outputWriter);
		}

	}

	static abstract class DerivationCombiner {

		private void addDerivationToMap(Map<String, WeightedTranslation> bestDistinctDerivationMap, WeightedTranslation weightedTranslation) {
			//System.out.println("addDerivationToMap : derivationYield: " + weightedTranslation.getTargetYield());
			if (bestDistinctDerivationMap.containsKey(weightedTranslation.getTargetYield())) {
				// System.out.println("already seen derivation yield...");
				addDerivationWithAlreadyPresentYield(bestDistinctDerivationMap, weightedTranslation);
			}
			// No derivation with this yield exists yet, so create a new entry
			else {
				// System.out.println("new derivation yield!!!");
				bestDistinctDerivationMap.put(weightedTranslation.getTargetYield(), weightedTranslation);
				Assert.assertTrue(bestDistinctDerivationMap.containsKey(weightedTranslation.getTargetYield()));
			}
		}

		protected abstract void addDerivationWithAlreadyPresentYield(Map<String, WeightedTranslation> bestDistinctDerivationMap, WeightedTranslation weightedTranslation);

		private static DerivationCombiner createDerivationCombiner(boolean sumDerivationsWithSameYield) {
			if (sumDerivationsWithSameYield) {
				return new SummationDerivationCombiner();
			} else {
				return new ViterbiDerivationCombiner();
			}
		}

	}

	private static class ViterbiDerivationCombiner extends DerivationCombiner {
		@Override
		protected void addDerivationWithAlreadyPresentYield(Map<String, WeightedTranslation> bestDistinctDerivationMap, WeightedTranslation weightedTranslation) {
			double currentBestWeight = bestDistinctDerivationMap.get(weightedTranslation.getTargetYield()).getTotalWeight();

			// Replace the current derivation only if the weight of the new
			// candidate is better
			if (weightedTranslation.getTotalWeight() > currentBestWeight) {
				bestDistinctDerivationMap.put(weightedTranslation.getTargetYield(), weightedTranslation);
			}
		}
	}

	public static class SummationDerivationCombiner extends DerivationCombiner {
		@Override
		protected void addDerivationWithAlreadyPresentYield(Map<String, WeightedTranslation> bestDistinctDerivationMap, WeightedTranslation weightedTranslation) {
			WeightedTranslation existingWeightedTranslation = bestDistinctDerivationMap.get(weightedTranslation.getTargetYield());
			// add the weight of the new WeightedTranslation to the existing WeightedTranslation
			existingWeightedTranslation.addWeigt(weightedTranslation);
		}
	}
}
