package mt_pipeline.mbrInterface;

import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import util.FileUtil;

public class MosesToJoshuaNBestListConverter extends FileConverter {

	protected MosesToJoshuaNBestListConverter(BufferedReader inputReader, BufferedWriter outputWriter) {
		super(inputReader, outputWriter);
	}

	public static MosesToJoshuaNBestListConverter createMosesToJoshuaNBestListConverter(String inputFilePath, String outputFilePath) {
		BufferedReader inputReader;
		BufferedWriter outputWriter;
		try {
			inputReader = new BufferedReader(new FileReader(inputFilePath));
			outputWriter = new BufferedWriter(new FileWriter(outputFilePath));
			return new MosesToJoshuaNBestListConverter(inputReader, outputWriter);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	private boolean isMosesLabelString(String string) {
		return string.contains(":");
	}

	public String createConvertedNBestString(String inputString) {
		String[] parts = JoshuaStyle.spitOnRuleSeparator(inputString);
		String weightsString = parts[2];
		String[] weightParts = weightsString.split(" ");
		String convertedWeightsString = "";
		for (String weigtPart : weightParts) {
			if (!isMosesLabelString(weigtPart)) {
				convertedWeightsString = convertedWeightsString + " " + weigtPart;
			}
		}
		convertedWeightsString = convertedWeightsString.trim();

		String result = parts[0] + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR + parts[1].trim() + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR + convertedWeightsString + JoshuaStyle.JOSHUA_STYLE_RULE_SEPERATOR
				+ parts[3];
		return result;
	}

	public void writeConvertedNBestFile() {
		String inputString;
		try {
			while ((inputString = inputReader.readLine()) != null) {
				String resultString = createConvertedNBestString(inputString);
				outputWriter.write(resultString + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally
		{
			FileUtil.closeCloseableIfNotNull(outputWriter);
		}
	}

}
