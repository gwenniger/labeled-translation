package mt_pipeline.mbrInterface;

import java.io.BufferedWriter;
import java.util.List;
import mt_pipeline.evaluation.SentencesNBestCandidatesListIterator;

public class BestDistinctYieldDerivationListComputer extends MergedDerivationListComputer {

	protected final int maxNumUniqueTranslationsForNBestMBR;

	protected BestDistinctYieldDerivationListComputer(SentencesNBestCandidatesListIterator sentencesNBestCandidatesListIterator, BufferedWriter outputWriter, int maxNumUniqueTranslationsForNBestMBR, DerivationCombiner derivationCombiner) {
		super(sentencesNBestCandidatesListIterator, outputWriter, derivationCombiner);
		this.maxNumUniqueTranslationsForNBestMBR = maxNumUniqueTranslationsForNBestMBR;
	}

	@Override
	protected int getNumberTranslationsPerSentenceToOutput(List<WeightedTranslation> weightedUniqueYieldBestDerivations) {
		if (maxNumUniqueTranslationsForNBestMBR > 1) {
			int result = Math.min(maxNumUniqueTranslationsForNBestMBR, weightedUniqueYieldBestDerivations.size());
			System.out.println("Specified maxNumUniqueTranslationsForNBestMBR: " + maxNumUniqueTranslationsForNBestMBR + " using " + result + " translations");
			return result;

		} else if (maxNumUniqueTranslationsForNBestMBR == -1) {
			int result = weightedUniqueYieldBestDerivations.size();
			System.out.println("Specified maxNumUniqueTranslationsForNBestMBR -1 : using all(=" + result + ")translations");
			return result;
		} else {
			String errorMessage = "Error : Expected a value for  maxNumUniqueTranslationsForNBestMBR greater than 0 (specific limit) or -1 (no limit) but got :" + maxNumUniqueTranslationsForNBestMBR;
			throw new RuntimeException(errorMessage);
		}
	}

}
