package mt_pipeline;

import grammarExtraction.chiangGrammarExtraction.MultiThreadGrammarExtractorTestFiltered;
import grammarExtraction.grammarExtractionFoundation.MultiThreadGrammarExtractor;

public class GrammarExtracterCreaterHatsCCL implements MultiThreadGrammarExtracterCreater{


	@Override
	public MultiThreadGrammarExtractor createGrammarExtracterTestFiltered(String configFilePath, int numThreads) {
		return MultiThreadGrammarExtractorTestFiltered.createMultiThreadGrammarExtractorTestFilteredCCL(configFilePath, numThreads);
	}

	@Override
	public String getSystemName() {
		return MTPipelineHatsCCL.SystemName;
	}

	@Override
	public boolean writePlainHieroRules() {
		return true;
	}

}
