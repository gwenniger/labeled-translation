package mt_pipeline.mt_config;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import mt_pipeline.MTSystem;
import mt_pipeline.MosesSystem;

public class MosesSpecificConfig extends SystemSpecificConfig {

	private static final String MOSES_CONFIG_SUFFIX = ".moses.ini";

	protected MosesSpecificConfig(MTConfigFile theConfig) {
		super(theConfig);
	}

	@Override
	protected String getSystemSpecificConfigSuffix() {
		return MOSES_CONFIG_SUFFIX;
	}

	@Override
	public MTSystem createMTSystem(MTConfigFile theConfig, String unknownWordsLabel,SystemIdentity systemIdentity) {
		return MosesSystem.createMosesSystem(theConfig,systemIdentity);
	}

	@Override
	public String getDevTunedConfigFileName() {
		return theConfig.filesConfig.getMosesDevTunedConfigFileName();
	}

}
