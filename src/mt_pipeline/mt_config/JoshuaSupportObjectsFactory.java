package mt_pipeline.mt_config;

import mt_pipeline.DecoderCommandCreater;
import mt_pipeline.JoshuaDecoderCommandCreater;
import mt_pipeline.tuning.JoshuaMertCommandCreater;
import mt_pipeline.tuning.JoshuaMiraCommandCreater;
import mt_pipeline.tuning.JoshuaProCommandCreater;
import mt_pipeline.tuning.Tuner;
import mt_pipeline.tuning.TuningCommandCreater;
import grammarExtraction.chiangGrammarExtraction.HieroGlueGrammarCreater;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatter;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatterJoshua;
import grammarExtraction.translationRules.JoshuaRuleRepresentationCreater;
import grammarExtraction.translationRules.TranslationRuleRepresentationCreater;
import util.ConfigFile;

public class JoshuaSupportObjectsFactory extends
		SystemSpecificSupportObjectsFactory {

	public static final String UnknownWordPreTerminalJoshuaComplexLabels = "UnknownWord";

	@Override
	public TranslationRuleRepresentationCreater createTranslationRuleRepresentationCreater(
			ConfigFile theConfig, String systemName) {

		if (this.useNonHieroLabels(theConfig, systemName)) {
			return JoshuaRuleRepresentationCreater
					.createJoshuaRuleRepresentationCreaterFactoryComplexLabeled();
		} else {
			return JoshuaRuleRepresentationCreater
					.createJoshuaRuleRepresentationCreaterFactoryHieroLabeled();
		}
	}

	@Override
	public SystemSpecificConfig createSystemSpecificConfiguration(
			MTConfigFile theConfig) {
		return new JoshuaSpecificConfig(theConfig);
	}

	@Override
	public DecoderCommandCreater createDecoderCommandCreaterFromConfig(
			MTConfigFile theConfig) {
		return JoshuaDecoderCommandCreater
				.createJoshuaDecoderCommandCreater(theConfig);
	}

	@Override
	public FeatureValueFormatter createFeatureValueFormatter(
			ConfigFile theConfig) {
		return new FeatureValueFormatterJoshua();
	}

	@Override
	public Tuner createTunerFromConfig(
			MTConfigFile theConfig) {
		
		String tunerType = theConfig.decoderConfig.getTunerType();
		System.out.println("tunerType: " + tunerType);
		TuningCommandCreater tuningCommandCreater = null;
		if (tunerType.equals(MTDecoderConfig.MERT_TUNING_TYPE)) {
			tuningCommandCreater = JoshuaMertCommandCreater
					.createJoshuaMertCommandCreater(theConfig);
		} else if (tunerType.equals(MTDecoderConfig.PRO_TUNING_TYPE)) {
		    tuningCommandCreater = JoshuaProCommandCreater
					.createJoshuaProCommandCreater(theConfig);
		} else if (tunerType.equals(MTDecoderConfig.MIRA_TUNING_TYPE)) {
		    tuningCommandCreater = JoshuaMiraCommandCreater
					.createJoshuaMiraCommandCreater(theConfig);
		} else {
			throw new RuntimeException("Error : unrecognized tuning type");
		}
		return Tuner.createBasicTuner(theConfig, tuningCommandCreater);
	}

	@Override
	public String getUnknownWordsLabel(ConfigFile theConfig, String systemName) {
		if (useNonHieroLabels(theConfig, systemName)) {
			return UnknownWordPreTerminalJoshuaComplexLabels;
		} else {
			return HieroGlueGrammarCreater.ChiangLabel;
		}
	}

}
