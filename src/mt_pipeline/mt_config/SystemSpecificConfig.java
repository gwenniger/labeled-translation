package mt_pipeline.mt_config;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import mt_pipeline.MTSystem;

public abstract class SystemSpecificConfig {

	protected static final String DECODER_MERT_CONFIG_FILE_NAME = "mert_config";
	protected static final String DECODER_TEST_CONFIG_FILE_NAME = "test_config";
	protected static final String DECODER_TEST_TUNED_CONFIG_FILE_NAME = "test_config_tuned";

	protected final MTConfigFile theConfig;

	protected SystemSpecificConfig(MTConfigFile theConfig) {
		this.theConfig = theConfig;
	}

	public String getDecoderTestConfigFileName() {
		return this.theConfig.foldersConfig.getIntermediateRunSpecificTestFolder() + DECODER_TEST_CONFIG_FILE_NAME + getSystemSpecificConfigSuffix();
	}

	public String getDecoderMertConfigFileName() {
		return this.theConfig.foldersConfig.getMertRunSpecificFolder() + DECODER_MERT_CONFIG_FILE_NAME + getSystemSpecificConfigSuffix();
	}
	
	public abstract String getDevTunedConfigFileName();

	protected abstract String getSystemSpecificConfigSuffix();
	

	public abstract MTSystem createMTSystem(MTConfigFile theConfig, String unknownWordsLabel,SystemIdentity systemIdentity);
}
