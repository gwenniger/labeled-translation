package mt_pipeline.mt_config;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import mt_pipeline.JoshuaSystem;
import mt_pipeline.MTSystem;

public class JoshuaSpecificConfig extends SystemSpecificConfig {
	private static final String JOSHUA_CONFIG_SUFFIX = ".joshua";

	protected JoshuaSpecificConfig(MTConfigFile theConfig) {
		super(theConfig);
	}

	@Override
	protected String getSystemSpecificConfigSuffix() {
		return JOSHUA_CONFIG_SUFFIX;
	}

	@Override
	public MTSystem createMTSystem(MTConfigFile theConfig, String unknownWordsLabel,SystemIdentity systemIdentity) {
		return JoshuaSystem.createJoshuaSystem(theConfig, unknownWordsLabel, systemIdentity);
	}

	@Override
	public String getDevTunedConfigFileName() {
		return theConfig.filesConfig.getJoshuaDevTunedConfigFileName();
	}

}
