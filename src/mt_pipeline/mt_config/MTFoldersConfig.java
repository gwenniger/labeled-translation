package mt_pipeline.mt_config;

import java.io.FileNotFoundException;
import java.util.List;

import util.FileUtil;

public class MTFoldersConfig {
	final private MTConfigFile theConfig;

	public final static String SRILM_BIN_DIR_PROPERTY = "srilmBinDir";
	public final static String BERKELEYLM_BIN_DIR_PROPERTY = "berkeleyLMBinDir";

	public static final String SCRIPT_SDIR_PROPERTY = "scriptsDir";
	public static final String ROOT_OUTPUT_FOLDER = "rootOutputFolder";
	private static final String RESOURCES_FOLDER = "resources";
	private static final String RESULTS_FOLDER = "results";
	public static final String MERT_FOLDER = "mert";
	public static final String JOSHUA_SUFFIX_ARRAY_FOLDER_NAME_PROPERTY = "joshuaSuffixArrayFolderName";

	private static final String INTERMEDIATE_RESULTS_STRING = "intermediate-results";

	public static final String DATA_NAME = "data";
	private static final String MERT_WORK_DIR_NAME = "tuner-work-dir";
	
	public static final String SPECIFIC_RUN_SUBFOLDER_NAME_PROPERTY = "SpecificRunSubFolderName";
	

	protected MTFoldersConfig(MTConfigFile theConfig) throws FileNotFoundException {
		this.theConfig = theConfig;
	}

	public String getRootOutputFolderName() {
		String rootOutputFolderName = theConfig.getValue(ROOT_OUTPUT_FOLDER);
		return rootOutputFolderName;
	}

	protected boolean hasValue(String parameterName) {
		return theConfig.hasValue(parameterName);
	}

	protected String getValue(String parameterName) {
		return theConfig.getValue(parameterName);
	}

	public String getValueWithPresenceCheck(String parameterName, String methodName) {
		return theConfig.getValueWithPresenceCheck(parameterName, methodName);
	}
	
	public List<String> getMultiValueParmeterAsListWithPresenceCheck(String parameterName){
	    return theConfig.getMultiValueParmeterAsListWithPresenceCheck(parameterName);
	}

	public String getScriptsDir() {
		return this.getValue(SCRIPT_SDIR_PROPERTY);
	}

	public String getDataOutputFolderName() {
		String dataOutputFolderName = getRootOutputFolderName() + DATA_NAME + "/";
		return dataOutputFolderName;
	}

	public String getSystemOutputFolderName() {
		String result = getRootOutputFolderName() + this.theConfig.getSystemName() + "/";
		return result;
	}

	public static String getIntermediateOutputFolderName(String systemOutputFolderName) {
		String dataOutputFolderName = systemOutputFolderName + INTERMEDIATE_RESULTS_STRING + "/";
		return dataOutputFolderName;
	}

	public String getIntermediateOutputFolderName() {
		return getIntermediateOutputFolderName(getSystemOutputFolderName());
	}

	public String getCategoryDataFolderName(String category) {
		String categoryOutputFolderName = getDataOutputFolderName() + category + "/";
		return categoryOutputFolderName;
	}

	public static String getCategoryIntermediateResultsFolderName(String category, String systemOutputFolderName) {
		String categoryOutputFolderName = getIntermediateOutputFolderName(systemOutputFolderName) + category + "/";
		return categoryOutputFolderName;
	}

	public String getCategoryIntermediateResultsFolderName(String category) {
		return getCategoryIntermediateResultsFolderName(category, getSystemOutputFolderName());
	}

	
	/**
	 * This method creates the path to a run-specific folder, if usage of a specific run sub-folder
	 * is turned on, by specific the name of the specific run sub-folder in the config file.
	 * The motivation is, that for a specific experiment, typically it may be required to 
	 * run it several times, in which the extracted grammars can be reused (grammar extraction 
	 * is skipped), and the results of tuning and translating the test set should be stored 
	 * in appropriate sub folders. 
	 * Rather than creating copies manually of the entire experiment and/or copying and renaming 
	 * the earlier results manually in sub-folders, here we automate these steps. 
	 * Also, it is possible to do the same not only for different runs, but also for different 
	 * runs with different settings, that is change the (decoding) configuration and add 
	 * a run configuration parameter of the form: 
	 * "SpecificRunSubFolderName = BasicAndSparseFeatures-run2" , just to give an example. 
	 * @param category
	 * @return
	 */
	public String getCategoryIntermediateResultsFolderNameUsingRunSubfolderIfApplicable(String category) {
		String result = getCategoryIntermediateResultsFolderName(category, getSystemOutputFolderName());
		
		if(useSpecificRunSubFolderName()){
		    result += getSpecificRunSubFolderName() + "/";
		}
		
		return result;
	}
	
	public String getResourcesFolder() {
		return getCategoryIntermediateResultsFolderName(RESOURCES_FOLDER);
	}

	public String getMertFolder() {
		return getCategoryIntermediateResultsFolderName(MERT_FOLDER);
	}
	
	public String getMertRunSpecificFolder() {
		return getCategoryIntermediateResultsFolderNameUsingRunSubfolderIfApplicable(MERT_FOLDER);
	}

	public String getIntermediateTestFolder() {
		return getCategoryIntermediateResultsFolderName(MTConfigFile.TEST_CATEGORY);
	}
	
	public String getIntermediateRunSpecificTestFolder() {
		return getCategoryIntermediateResultsFolderNameUsingRunSubfolderIfApplicable(MTConfigFile.TEST_CATEGORY);
	}

	public String getIntermediateDevFolder() {
		return getCategoryIntermediateResultsFolderName(MTConfigFile.DEV_CATEGORY);
	}
	
	public String getIntermediateRunSpecificDevFolder() {
		return getCategoryIntermediateResultsFolderNameUsingRunSubfolderIfApplicable(MTConfigFile.DEV_CATEGORY);
	}

	private static String getResultsBaseFolder(String sytemBaseFolder,String specificRunSubFolderName) {
		String result =  sytemBaseFolder + RESULTS_FOLDER; 
		if(specificRunSubFolderName != null){
		    result += "/" + specificRunSubFolderName + "/";
		}
		else{
		    result += "/";
		}
		return result;
	}

	public String getResultsBaseFolder() {
		return getResultsBaseFolder(this.getSystemOutputFolderName(), this.getSpecificRunSubFolderName());
	}

	public String getInputFolderName(String category) {
		String inputFolderName = getValue(category + "DataInputDirectory");
		return inputFolderName;
	}

	public String getSuffixArrayFolder() {
		return getResourcesFolder() + getValueWithPresenceCheck(JOSHUA_SUFFIX_ARRAY_FOLDER_NAME_PROPERTY, "getJoshuaSuffixArrayFolder()");
	}

	public static String getResultsFolderCategory(String systemBaseFolder, String category,String specificRunSubFolderName) {
		return getResultsBaseFolder(systemBaseFolder,specificRunSubFolderName) + category + "/";
	}

	public String getResultsFolderCategory(String category) {
		return getResultsBaseFolder() + category + "/";
	}

	protected MTConfigFile getTheConfig() {
		return this.theConfig;
	}

	public String getMosesMertWorkDirFolder() {
		String result =  this.getMertRunSpecificFolder() + MERT_WORK_DIR_NAME + "/";
		FileUtil.createFolderIfNotExisting(result);
		return result;
	}
	
	private boolean useSpecificRunSubFolderName(){
	    if(theConfig.hasValue(SPECIFIC_RUN_SUBFOLDER_NAME_PROPERTY)){
		return true;
	    }
	    return false;
	}
	
	/**
	 * A specific run sub-folder name such as "SomeExperiment/run1" can be used
	 * to separate different experiments and runs for those experiments, while re-using 
	 * the same grammars. This removes the need to manually copy over the entire experiment,
	 * in order to run a new experiment or different parallel runs of the same experiment.
	 *  
	 * @return
	 */
	private String getSpecificRunSubFolderName(){
	    return theConfig.getValueWithPresenceCheck(SPECIFIC_RUN_SUBFOLDER_NAME_PROPERTY);
	}

}
