package mt_pipeline.mt_config;

import java.util.ArrayList;
import java.util.List;

import util.Pair;

public class MTTaggerConfig extends MTMetaDataCreatorConfig {

	public static final String PRE_PROCESSED_TAGGING_SUFFIX = ".PreprocessedForTagging";
	public static final String TNT_TAGGER_ROOT_DIR_PROPERTY = "tntTaggerRootDir";
	public static final String TAG_SOURCE_FILE_PROPERTY = "tagSourceFile";
	public static final String TAG_TARGET_FILE_PROPERTY = "tagTargetFile";
	private static final String TAGGER_RESULT_FILE_SUFFIX = ".Tags";
	private static final String TNT_FORMAT_SUFFIX = ".TnTFormat";
	private static final String TAGGER_NAME = "tnt";
	private static final String UNIVERSAL_TAGS_SUFFIX = "-UniversalTags";
	public static final String USE_UNIVERSAL_TAGSET_FOR_TAGGING_SOURCE_PROPERTY = "useUniversalTagSetForTaggingSource";
	public static final String USE_UNIVERSAL_TAGSET_FOR_TAGGING_TARGET_PROPERTY = "useUniversalTagSetForTaggingTarget";
	public static final String UNIVERSAL_TAGSET_MAPS_DIR_PROPERTY = "universalTagSetMapsDir";
	public static final String SOURCE_TAGGER_MODEL_NAME_PROPERTY = "sourceTaggerModelName"; // "negra";
	public static final String SOURCE_TO_UNIVERSAL_TAGS_MAPPER_NAME_PROPERTY = "sourceToUniversalTaggerMapName"; // "de-tiger.map";
	public static final String TARGET_TAGGER_MODEL_NAME_PROPERTY = "targetTaggerModelName"; // "negra";
	public static final String TARGET_TO_UNIVERSAL_TAGS_MAPPER_NAME_PROPERTY = "targetToUniversalTaggerMapName"; // "de-tiger.map";
	private static final String TNT_SUFFIX = ".tnt";
	private static final String UNIVERSAL_SUFFIX = "-UNIVERSAL";
	private static final String MAP_SUFFIX = ".map";

	protected MTTaggerConfig(MTConfigFile theConfig) {
		super(theConfig);
	}

	@Override
	protected String preprocessedFileSuffix() {
		return PRE_PROCESSED_TAGGING_SUFFIX;
	}

	public String getTaggerInputFileNamePreprocessed(String inputFileName) {
		return inputFileName + preprocessedFileSuffix();
	}

	public String getTaggerOutputFileName(String inputFileName, boolean useUniversalTagSet) {
		String result = inputFileName + TAGGER_RESULT_FILE_SUFFIX + "." + TAGGER_NAME;
		if (useUniversalTagSet) {
			result += UNIVERSAL_TAGS_SUFFIX;
		}

		return result;
	}

	public String getTaggerOutputFileNameTnTFornat(String inputFileName) {
		return inputFileName + TAGGER_RESULT_FILE_SUFFIX + TNT_FORMAT_SUFFIX;
	}

	public String getTaggerPartOutputFileNameTnTFormat(String inputFileName) {
		String[] parts = inputFileName.split("Part");
		return parts[0] + TAGGER_RESULT_FILE_SUFFIX + TNT_FORMAT_SUFFIX + "Part" + parts[1];
	}

	public String getTaggerSourceOutputFileName(String category) {
		return getTaggerOutputFileName(theConfig.taggerConfig.getEnrichmentProducerSourceInputFilePath(category), useUniversalTagSetForTaggingSource());
	}

	public String getTaggerTargetTargetOutputFileName(String category) {
		return getTaggerOutputFileName(theConfig.taggerConfig.getEnrichmentProducerTargetInputFilePath(category), useUniversalTagSetForTaggingTarget());
	}

	public String getTnTTaggerRootDir() {
		return this.theConfig.getValueWithPresenceCheck(TNT_TAGGER_ROOT_DIR_PROPERTY, "getTnTTaggerRootDir");
	}

	private String getUniversalTagSetMappingsDir() {
		return this.theConfig.getValueWithPresenceCheck(UNIVERSAL_TAGSET_MAPS_DIR_PROPERTY);
	}

	public String getSourceTagsToUniversalTagsMapperFilePath() {
		return getUniversalTagSetMappingsDir() + this.theConfig.getValueWithPresenceCheck(SOURCE_TO_UNIVERSAL_TAGS_MAPPER_NAME_PROPERTY);
	}

	public String getTargetTagsToUniversalTagsMapperFilePath() {
		return getUniversalTagSetMappingsDir() + this.theConfig.getValueWithPresenceCheck(TARGET_TO_UNIVERSAL_TAGS_MAPPER_NAME_PROPERTY);
	}

	public String getTnTTaggerModelsDir() {
		return getTnTTaggerRootDir() + "models/";
	}

	public boolean tagSourceFile() {
		return Boolean.parseBoolean(this.theConfig.getValueWithPresenceCheck(TAG_SOURCE_FILE_PROPERTY));
	}

	public boolean tagTargetFile() {
		return Boolean.parseBoolean(this.theConfig.getValueWithPresenceCheck(TAG_TARGET_FILE_PROPERTY));
	}

	private Pair<String> getTnTModelEnvironmentVariablePair() {
		return new Pair<String>("TNT_MODELS", getTnTTaggerModelsDir());
	}

	public List<Pair<String>> createTnTEnvironmentVariables() {
		List<Pair<String>> environmentVariables = new ArrayList<Pair<String>>();
		environmentVariables.add(getTnTModelEnvironmentVariablePair());
		return environmentVariables;
	}

	public boolean useUniversalTagSetForTaggingSource() {
		boolean useUniversalTagSet = theConfig.getBooleanWithPresenceCheck(USE_UNIVERSAL_TAGSET_FOR_TAGGING_SOURCE_PROPERTY);
		return useUniversalTagSet;
	}

	public boolean useUniversalTagSetForTaggingTarget() {
		boolean useUniversalTagSet = theConfig.getBooleanWithPresenceCheck(USE_UNIVERSAL_TAGSET_FOR_TAGGING_TARGET_PROPERTY);
		return useUniversalTagSet;
	}

	public String getSourceTaggerModelName() {
		return this.theConfig.getValueWithPresenceCheck(SOURCE_TAGGER_MODEL_NAME_PROPERTY);
	}

	private String createUniversalModelName(String modelName) {
		String result = modelName;
		result = result.replace(TNT_SUFFIX, "");
		result += UNIVERSAL_SUFFIX;
		return result;
	}

	public String getSourceUniversalMappedTaggerModelName() {
		return createUniversalModelName(getSourceTaggerModelName());
	}

	public String getTargetUniversalMappedTaggerModelName() {
		return createUniversalModelName(getTargetTaggerModelName());
	}

	public String getTargetTaggerModelName() {
		return this.theConfig.getValueWithPresenceCheck(TARGET_TAGGER_MODEL_NAME_PROPERTY);
	}

	public String getSourceTaggerModelPath() {
		return getTnTTaggerModelsDir() + getSourceTaggerModelName();
	}

	public String getTargetTaggerModelPath() {
		return getTnTTaggerModelsDir() + getTargetTaggerModelName();
	}

	public String getCreatedSourceUniversalMappedModelPath() {
		return getTnTTaggerModelsDir() + getSourceUniversalMappedTaggerModelName()  + TNT_SUFFIX;
	}

	public String getCreatedTargetUniversalMappedModelPath() {
		return getTnTTaggerModelsDir() + getTargetUniversalMappedTaggerModelName() + TNT_SUFFIX;
	}
	
	public String getCreatedSourceUniversalMapPath() {
		return getTnTTaggerModelsDir() + getSourceUniversalMappedTaggerModelName() + MAP_SUFFIX;
	}
	
	public String getCreatedTargetUniversalMapPath() {
		return getTnTTaggerModelsDir() + getTargetUniversalMappedTaggerModelName() + MAP_SUFFIX;
	}
}
