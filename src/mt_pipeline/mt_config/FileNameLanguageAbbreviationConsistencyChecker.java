package mt_pipeline.mt_config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileNameLanguageAbbreviationConsistencyChecker {
    private final String CONTINUATION_STRING = "continue";
    private static final String SOURCE_SIDE = "source";
    private static final String TARGET_SIDE = "target";

    private final MTConfigFile theConfig;

    private FileNameLanguageAbbreviationConsistencyChecker(MTConfigFile theConfig) {
	this.theConfig = theConfig;
    }

    public static FileNameLanguageAbbreviationConsistencyChecker createFileNameLanguageAbbreviationConsistencyChecker(
	    MTConfigFile theConfig) {
	return new FileNameLanguageAbbreviationConsistencyChecker(theConfig);
    }

    private String getLanguageSuffix(String fileName) {
	if (fileName.contains(".")) {
	    String[] partsArray = fileName.split("\\.");
	    if (partsArray.length > 0) {
		return partsArray[partsArray.length - 1];
	    }
	}
	return null;

    }

    private List<FileNameLanguageAbbreviationNameWarning> checkConsistencyInputNamesAndLanguageAbbreviations(
	    String category, String side, String fileNameLaguageAbbreviation,
	    String languageAbbreviation, String inputFilePath) {
	List<FileNameLanguageAbbreviationNameWarning> result = new ArrayList<FileNameLanguageAbbreviationNameWarning>();
	if (fileNameLaguageAbbreviation == null) {
	    result.add(FileNameLanguageAbbreviationNameWarning
		    .createFileNameLanguageAbbreviationNameUncheckableWarning(category, side,
			    inputFilePath));
	} else if (!fileNameLaguageAbbreviation.equals(languageAbbreviation)) {
	    result.add(FileNameLanguageAbbreviationNameWarning
		    .createFileNameLanguageAbbreviationNameInonsistencyWarning(category, side,
			    inputFilePath, fileNameLaguageAbbreviation, languageAbbreviation));
	}
	return result;

    }

    private List<FileNameLanguageAbbreviationNameWarning> checkConsistencyInputNamesAndLanguageAbbreviations(
	    String category) {
	String sourceInputFileName = theConfig.filesConfig.getSourceInputFilePath(category);
	List<String> targetInputFileNames = theConfig.filesConfig.getTargetInputFilePaths(category);
	String sourceFileLanguageAbbreviation = getLanguageSuffix(sourceInputFileName);

	String sourceLanguageAbbeviation = theConfig.filesConfig.getSourceLanguageAbbreviation();
	String targetLanguageAbbeviation = theConfig.filesConfig.getTargetLanguageAbbreviation();

	List<FileNameLanguageAbbreviationNameWarning> result = new ArrayList<FileNameLanguageAbbreviationNameWarning>();
	result.addAll(checkConsistencyInputNamesAndLanguageAbbreviations(category, SOURCE_SIDE,
		sourceFileLanguageAbbreviation, sourceLanguageAbbeviation, sourceInputFileName));

	for (String targetInputFileName : targetInputFileNames) {
	    String targetFileLanguageAbbreviation = getLanguageSuffix(targetInputFileName);
	    result.addAll(checkConsistencyInputNamesAndLanguageAbbreviations(category, TARGET_SIDE,
		    targetFileLanguageAbbreviation, targetLanguageAbbeviation, targetInputFileName));
	}
	return result;

    }

    private String readUserChoice() {
	System.out.print("Type \"" + CONTINUATION_STRING
		+ "\" to coninue anyway, or any other key to quit");
	String input = System.console().readLine();
	return input;
    }

    public void checkConsistencyInputNamesAndLanguageAbbreviations() {
	List<String> categoriesList = Arrays.asList(MTConfigFile.TRAIN_CATEGORY,
		MTConfigFile.DEV_CATEGORY, MTConfigFile.TEST_CATEGORY);

	List<FileNameLanguageAbbreviationNameWarning> possibleInconsitencyWarningsList = new ArrayList<FileNameLanguageAbbreviationNameWarning>();
	for (String category : categoriesList) {
	    possibleInconsitencyWarningsList
		    .addAll(checkConsistencyInputNamesAndLanguageAbbreviations(category));
	}

	if (!possibleInconsitencyWarningsList.isEmpty()) {
	    int i = 1;
	    System.out
		    .println("WARNING!!! SOME POSSIBLE INCONSISTENCIES BETWEEN FILE NAMES AND LANGUAGE ABREVIATION NAMES WERE DETECTED: ");
	    for (FileNameLanguageAbbreviationNameWarning warning : possibleInconsitencyWarningsList) {
		System.out.println("Warning " + i + "\n" + warning.warningMessage());
	    }

	    String userChoice = readUserChoice();
	    if (!userChoice.equals(CONTINUATION_STRING)) {
		System.out.println("quiting...");
		System.exit(1);
	    }
	}
    }

    private static abstract class FileNameLanguageAbbreviationNameWarning {
	protected final String category;
	protected final String side;
	protected final String inputFilePath;

	protected FileNameLanguageAbbreviationNameWarning(String category, String side,
		String inputFilePath) {
	    this.category = category;
	    this.side = side;
	    this.inputFilePath = inputFilePath;
	}

	public static FileNameLanguageAbbreviationNameUncheckableWarning createFileNameLanguageAbbreviationNameUncheckableWarning(
		String category, String side, String inputFilePath) {
	    return new FileNameLanguageAbbreviationNameUncheckableWarning(category, side,
		    inputFilePath);
	}

	public static FileNameLanguageAbbreviationNameInonsistencyWarning createFileNameLanguageAbbreviationNameInonsistencyWarning(
		String category, String side, String inputFilePath,
		String fileNameLaguageAbbreviation, String languageAbbreviation) {
	    return new FileNameLanguageAbbreviationNameInonsistencyWarning(category, side,
		    inputFilePath, fileNameLaguageAbbreviation, languageAbbreviation);
	}

	protected abstract String warningMessage();

	protected String categoryAndSideString() {
	    return "Data Category: " + category + " input side:  " + side + "\n";
	}
    }

    private static class FileNameLanguageAbbreviationNameUncheckableWarning extends
	    FileNameLanguageAbbreviationNameWarning {

	protected FileNameLanguageAbbreviationNameUncheckableWarning(String category, String side,
		String inputFilePath) {
	    super(category, side, inputFilePath);
	}

	@Override
	protected String warningMessage() {
	    return "Warning - " + categoryAndSideString()
		    + " no language abbreviation found for input file " + inputFilePath
		    + " , cannot check consistency with language abbreviation";
	}

    }

    private static class FileNameLanguageAbbreviationNameInonsistencyWarning extends
	    FileNameLanguageAbbreviationNameWarning {

	private final String fileNameLaguageAbbreviation;
	private final String languageAbbreviation;

	protected FileNameLanguageAbbreviationNameInonsistencyWarning(String category, String side,
		String inputFilePath, String fileNameLaguageAbbreviation,
		String languageAbbreviation) {
	    super(category, side, inputFilePath);
	    this.fileNameLaguageAbbreviation = fileNameLaguageAbbreviation;
	    this.languageAbbreviation = languageAbbreviation;
	}

	@Override
	protected String warningMessage() {
	    return "Warning - " + categoryAndSideString() + " fileNameLanguageAbbreviation\""
		    + fileNameLaguageAbbreviation
		    + "\" does not correspond to language abbreviation specified in config \""
		    + languageAbbreviation + "\"";
	}
    }

}
