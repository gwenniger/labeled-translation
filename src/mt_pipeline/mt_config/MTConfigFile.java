package mt_pipeline.mt_config;

import grammarExtraction.samtGrammarExtraction.SAMTGrammarExtractionConstraints;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;

import datapreparation.SegmenterConfig;
import mt_pipeline.JoshuaSystem;
import mt_pipeline.MosesSystem;
import util.ConfigFile;
import util.Pair;

public class MTConfigFile extends ConfigFile {
	public static final String TRAIN_CATEGORY = "train";
	public static final String TEST_CATEGORY = "test";
	public static final String DEV_CATEGORY = "dev";
	public static final String JDK_HOME_DIR_PROPERTY = "jdkHomeDir";
	public final static String SYSTEM_NAMES_PROPERTY = "systemNames";
	public final static String EXTRACT_DEV_GRAMMAR_PROPERTY = "extractDevGrammar";
	public final static String EXTRACT_TEST_GRAMMAR_PROPERTY = "extractTestGrammar";
	public static final String PERFORM_TUNING_PROPERTY = "performTuning";

	public static enum DataCategory {
		TrainCategory, TestCategory, DevCategory
	};

	public final static String MT_SYSTEM_TYPE_PROPERTY = "mtSystemType";

	public final MTFoldersConfig foldersConfig;
	public final MTFilesConfig filesConfig;
	public final MTDecoderConfig decoderConfig;
	public final MTParserConfig parserConfig;
	public final MTTaggerConfig taggerConfig;
	public final MTSegmenterConfig segmenterConfig;
	public final SystemSpecificConfig systemSpecificConfig;
	

	private final String systemName;
	private static final String Xmx = "Xmx";

	// How the internal variables are called in the config file or the files that are generated

	private MTConfigFile(String inputFileName, String systemName) throws FileNotFoundException {
		super(inputFileName);
		this.decoderConfig = new MTDecoderConfig(this);
		this.foldersConfig = new MTFoldersConfig(this);
		this.filesConfig = new MTFilesConfig(this.foldersConfig);
		this.parserConfig = new MTParserConfig(this);
		this.taggerConfig = new MTTaggerConfig(this);
		this.segmenterConfig = MTSegmenterConfig.createMTSegmenterConfig(this);
		this.systemName = systemName;
		this.systemSpecificConfig = createSystemSpecificSupportObjectsFactory(this).createSystemSpecificConfiguration(this);
	}

	public static MTConfigFile createMtConfigFile(String inputFileName, String systemName) {
		try {
			return new MTConfigFile(inputFileName, systemName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("MTConfigFile - config file not found" + e);
		}
	}

	private static boolean isJoshuaSystem(String mtSystemType) {
		return (mtSystemType.toLowerCase().equals(JoshuaSystem.getSystemTypeName().toLowerCase()));
	}

	private static boolean isMosesSystem(String mtSystemType) {
		return (mtSystemType.toLowerCase().equals(MosesSystem.getSystemTypeName().toLowerCase()));
	}

	public static SystemSpecificSupportObjectsFactory createSystemSpecificSupportObjectsFactory(ConfigFile theConfig) {
		String mtSystemType = theConfig.getValueWithPresenceCheck(MT_SYSTEM_TYPE_PROPERTY);

		if (isJoshuaSystem(mtSystemType)) {
			return new JoshuaSupportObjectsFactory();
		} else if (isMosesSystem(mtSystemType)) {
			return new MosesSupportObjectsFactory();
		} else {
			throw new RuntimeException("unknown system type. Please choose one of " + MosesSystem.getSystemTypeName() + " or " + JoshuaSystem.getSystemTypeName());
		}
	}

	protected String getValueWithPresenceCheck(String parameterName, String methodName) {
		if (hasValue(parameterName)) {
			return getValue(parameterName);
		}

		throw new RuntimeException(methodName + " : " + parameterName + " unspecified in config file");
	}

	public String getSystemName() {
		return this.systemName;
	}

	public String getJDKHomeDir() {
		return this.getValueWithPresenceCheck(JDK_HOME_DIR_PROPERTY, "getJDKHomeDir()");
	}

	public String baseJavaCall() {
		return "java -Dfile.encoding=UTF8 " + getXmx();
	}

	public String getXmx() {
		return "-Xmx" + this.getValue(Xmx);
	}
	
	public String getMaxMemory(){
		return this.getValue(Xmx);
	}

	private String getPathExtendedWithJavaBinPath() {
		Map<String, String> env = new ProcessBuilder().environment();
		String path = env.get("PATH");
		System.out.println("LinuxInteractor: path - " + path);
		// path = path + File.pathSeparator + "/usr/bin/";
		String javaBinLocation = getJDKHomeDir() + "bin/";
		path = javaBinLocation + File.pathSeparator + path;
		return path;
	}

	public Pair<String> getPathExtendedWithJavaBinPathEnvironmentVariablePair() {
		return new Pair<String>("PATH", getPathExtendedWithJavaBinPath());
	}

	public boolean useExtraSAMTFeatures() {
		return this.getBooleanWithPresenceCheck(SAMTGrammarExtractionConstraints.USE_EXTRA_SAMT_FEATURES_PROPERTY);
	}

	public Pair<String> getConfigPairWithPresenceCheck(String propertyName) {
		return new Pair<String>(propertyName, this.getValueWithPresenceCheck(propertyName));
	}

	public boolean extractDevGrammar() {
		return this.getBooleanIfPresentOrReturnDefault(EXTRACT_DEV_GRAMMAR_PROPERTY, true);
	}

	public boolean extractTestGrammar() {
		return this.getBooleanIfPresentOrReturnDefault(EXTRACT_TEST_GRAMMAR_PROPERTY, true);
	}

	public boolean tunerTypeIsMira() {
		return this.decoderConfig.tunerTypeIsMira();
	}
	
	public static boolean performTuning(ConfigFile theConfig){
	    return theConfig.getBooleanIfPresentOrReturnDefault(PERFORM_TUNING_PROPERTY, true);
	}
	
	public void checkLanguageAbbreviationConsistency(){
	    FileNameLanguageAbbreviationConsistencyChecker.createFileNameLanguageAbbreviationConsistencyChecker(this).checkConsistencyInputNamesAndLanguageAbbreviations();
	}
}
