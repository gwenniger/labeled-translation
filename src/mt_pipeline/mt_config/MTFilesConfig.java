package mt_pipeline.mt_config;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;

import util.FileUtil;
import mt_pipeline.tuning.JoshuaMiraCommandCreater;
import mt_pipeline.tuning.Tuner;

public class MTFilesConfig {

    private static final String TXT_SUFFIX = ".txt";
    private static final String SENTENCE_INDIVIDUAL_SCORES_STRING = "SentenceIndividualScores";
    private static final String SET_RESULT_STRING = "SetResult";
    private static final String BINARY_SUFFIX = ".binary";
    private static final String TARGET_NAME_CASED_SUFFIX = "TargetNameCased";
    private static final String SOURCE_NAME_CASED_SUFFIX = "SourceNameCased";
    private static final String SOURCE_NAME_SUFFIX = "SourceName";
    private static final String TARGET_NAME_SUFFIX = "TargetName";
    private static final String TARGET_NAMES_SUFFIX = "TargetNames";
    private static final String ALIGNMENTS_NAME_SUFFIX = "AlignmentsName";
    public static final String LM_FILE_EXTERNALLY_GENERATED_PATH_PROPERTY = "lmExternallyGeneratedPath";
    public static final String LM_INPUT_FILE_PROPERTY = "lmInputFile";
    private static final String SOURCE_FILE_NAME = "source.txt";
    private static final String SOURCE_PARSE_FILE_NAME = "sourceParse.txt";
    private static final String TARGET_FILE_PREFIX = "target";
    private static final String TARGET_FILE_NAME = "target.txt";
    private static final String TARGET_PARSE_FILE_NAME = "targetParse.txt";
    private static final String ALIGNMENTS_FILE_NAME = "alignments.txt";
    private static final String SOURCE_TOKENIZED_FILE_NAME = "source.tokenized.txt";
    private static final String TOKENIZED_FILE_SUFFIX = ".tokenized.txt";
    private static final String SOURCE_TOKENIZED_AND_LOWERCASED_FILE_NAME = "source.tokenized.lowercased.txt";
    private static final String TOKENIZED_AND_LOWERCASED_FILE_SUFFIX = ".tokenized.lowercased.";
    private static final String MULTIPLE_REFERENCES_TARGET_PREFIX = "target"; 
    private static final String SEGMENTED_FILE_SUFFIX = ".segmented.txt";
    private static final String SEGMENTED_AND_LOWERCASED_FILE_SUFFIX = ".segmented.lowercased.";
    private static final String SOURCE_SEGMENTED_FILE_NAME = "source.segmented.txt";
    // private static final String TARGET_SEGMENTED_FILE_NAME =
    // "target.segmented.txt";
    private static final String SOURCE_SEGMENTED_AND_LOWERCASED_FILE_NAME = "source.segmented.lowercased.txt";
    // private static final String TARGET_SEGMENTED_AND_LOWERCASED_FILE_NAME =
    // "target.segmented.lowercased.txt";
    private static final String REF_GML_FILE_NAME = "ref.gml";
    private static final String SRC_GML_FILE_NAME = "src.gml";
    public static final String SOURCE_LANGUAGE_ABBREVIATION_PROPERTY = "SourceLanguageAbbreviation";
    public static final String TARGET_LANGUAGE_ABBREVIATION_PROPERTY = "TargetLanguageAbbreviation";
    private static final String MERT_CONFIG_FILE_NAME = "mert.config";
    private static final String DECODER_TEST_TUNED_CONFIG_FILE_NAME = "test_config_tuned.ini";
    private static final String MERT_PARAMETERS_FILE_NAME = "params.txt";
    private static final String MERT_DECODER_COMMAND_FILE_NAME = "decoder_command";
    // private static final String MERT_FINAL_CONFIG_FILE_NAME =
    // "mert_config.joshua.ZMERT.final";

    private static final String DECODER_OUTPUT_NAME = "output.nbest";
    private static final String ONE_BEST_OUTPUT_NAME = "output.1best";

    // The names of the filteredGrammars that are generated
    public static final String FILTERED_GRAMMAR_NAME_TEST_PROPERTY = "filteredGrammarNameTest";
    public static final String FILTERED_GRAMMAR_NAME_MERT_PROPERTY = "filteredGrammarNameMert";
    public static final String PACKED_MAIN_GRAMMAR_FOLDER_NAME_TEST = "packed_main_grammar_test";
    public static final String PACKED_MAIN_GRAMMAR_FOLDER_NAME_MERT = "packed_main_grammar_mert";
    // The names of the Glue grammars that are generated
    public static final String GLUE_GRAMMAR_NAME_TEST_PROPERTY = "glueGrammarNameTest";
    public static final String GLUE_GRAMMAR_NAME_MERT_PROPERTY = "glueGrammarNameMert";
    public static final String PACKED_GLUE_GRAMMAR_FOLDER_NAME_TEST = "packed_glue_grammar_test";
    public static final String PACKED_GLUE_GRAMMAR_FOLDER_NAME_MERT = "packed_glue_grammar_mert";

    private static final String USED_CONFIG_NAME = "experimentConfigFile.txt";
    private static final String RESULTS_SUMMARY_FILE_NAME = "summarizedResults.tex";

    private static final String HATS_MAIN_GRAMMAR_EXTRACTOR_CONFIG_NAME_MERT = "hatsMainGrammarExtractorMertConfig.txt";
    private static final String HATS_SMOOTHING_GRAMMAR_EXTRACTOR_CONFIG_NAME_MERT = "hatsSmoothingGrammarExtractorMertConfig.txt";
    private static final String HATS_MAIN_GRAMMAR_EXTRACTOR_CONFIG_NAME_TEST = "hatsMainGrammarExtractorTestConfig.txt";
    private static final String HATS_SMOOTHING_GRAMMAR_EXTRACTOR_CONFIG_NAME_TEST = "hatsSmoothingGrammarExtractorTestConfig.txt";
    private static final String SOURCE_TAG_FILE_NAME = "sourceTags.txt";
    private static final String TARGET_TAG_FILE_NAME = "targetTags.txt";
    private static final String SOURCE_ENRICHED_FILE_NAME = "sourceEnriched.txt";
    protected static final String DENSE_MAP_FILE_NAME = "dense_map";
    protected static final String DISCRIMINATIVEFE_FEATURES_LIST_FILE_NAME = "discriminative_features_specification";
    protected static final String PACKER_CONFIG_FILE_NAME = "packer.config";
    protected static final String MOSES_INI_DEFAULT_NAME = "moses.ini";
    protected static final String NBEST_SUFFIX = ".NBEST";
    protected static final String NBEST_MBR_SUFFIX = ".NBEST_MBR-Reranked";
    protected static final String SYSTEM_SPECIFIC_SUFFIX = ".SYSTEM_SPECIFIC";
    private static final String VITERBI_SUFFIX = "-VITERBI";
    private static final String SUMMATION_SUFFIX = "-SUMMATION";
    private static final String POST_PROCESSED_VITERBI_SUFFIX = ".POST_PROCESSED_VITERBI";
    private static final String POST_PROCESSED_SUMMATION_SUFFIX = ".POST_PROCESSED_SUMMATION";
    private static final String CRUNCHING_SUFFIX = ".NBEST_Crunching-Reranked";
    private static final String GAP_LABEL_CONDITIONAL_PROBABILITY_TABLE_FILE_NAME = "GapLabelConditionalProbabilityTable.txt";

    private static final String TUNING_FINAL_SUFFIX = ".final";
    
    final private MTFoldersConfig mtFoldersConfig;

    protected MTFilesConfig(MTFoldersConfig mtFoldersConfig) throws FileNotFoundException {
	this.mtFoldersConfig = mtFoldersConfig;
    }

    public String getSourceLanguageAbbreviation() {
	return this.mtFoldersConfig.getValue(SOURCE_LANGUAGE_ABBREVIATION_PROPERTY);
    }

    public String getTargetLanguageAbbreviation() {
	return this.mtFoldersConfig.getValue(TARGET_LANGUAGE_ABBREVIATION_PROPERTY);
    }

    public static String getSourceTagFileName() {
	return SOURCE_TAG_FILE_NAME;
    }

    public static String getTargetTagFileName() {
	return TARGET_TAG_FILE_NAME;
    }

    public String getSourceSubFilePath(String category) {
	return this.mtFoldersConfig.getCategoryDataFolderName(category) + SOURCE_FILE_NAME;
    }

    public String getSourceParseSubFilePath(String category) {
	return this.mtFoldersConfig.getCategoryDataFolderName(category) + SOURCE_PARSE_FILE_NAME;
    }

    public String getSourceTagSubFilePath(String category) {
	return this.mtFoldersConfig.getCategoryDataFolderName(category) + getSourceTagFileName();
    }

    public String getTargetSubFilePathTrain() {
	return this.mtFoldersConfig.getCategoryDataFolderName(MTConfigFile.TRAIN_CATEGORY)
		+ TARGET_FILE_NAME;
    }

    private List<String> getTargetPrefixesList(String category) {
	List<String> result = new ArrayList<String>();
	result.add(TARGET_FILE_PREFIX);
	for (int i = 2; i <= getTargetInputFileNames(category).size(); i++) {
	    result.add(TARGET_FILE_PREFIX + i);
	}
	return result;
    }

    public List<String> getTargetSubFilePaths(String category) {
	List<String> result = new ArrayList<String>();
	for (String targetFilePrefix : getTargetPrefixesList(category)) {
	    String ithReferenceTargetFilePath = this.mtFoldersConfig
		    .getCategoryDataFolderName(category) + targetFilePrefix + ".txt";
	    result.add(ithReferenceTargetFilePath);
	}
	return result;
    }

    public String getTargetParseSubFilePath(String category) {
	return this.mtFoldersConfig.getCategoryDataFolderName(category) + TARGET_PARSE_FILE_NAME;
    }

    public String getTargetTagSubFilePath(String category) {
	return this.mtFoldersConfig.getCategoryDataFolderName(category) + getTargetTagFileName();
    }

    public String getAlignmentsSubFilePath() {
	return this.mtFoldersConfig.getCategoryDataFolderName(MTConfigFile.TRAIN_CATEGORY)
		+ ALIGNMENTS_FILE_NAME;
    }

    public String getSourceEnrichedFileName() {
	return SOURCE_ENRICHED_FILE_NAME;
    }

    public String getSourceSubFileName() {
	return SOURCE_FILE_NAME;
    }

    public String getTargetSubFileName() {
	return TARGET_FILE_NAME;
    }

    public String getAlignmentsSubFileName() {
	return ALIGNMENTS_FILE_NAME;
    }

    public boolean segmentInsteadOfTokenize(String language) {
	return this.mtFoldersConfig.getTheConfig().segmenterConfig
		.segmentInsteadOfTokenize(language);
    }

    private boolean segmentSourceFileName() {
	return segmentInsteadOfTokenize(getSourceLanguageAbbreviation());
    }

    private boolean segmentTargetFileName() {
	return segmentInsteadOfTokenize(getTargetLanguageAbbreviation());
    }

    public String getSourceTokenizedFileName(String category) {
	String result = this.mtFoldersConfig.getCategoryDataFolderName(category);
	if (segmentSourceFileName()) {
	    result += SOURCE_SEGMENTED_FILE_NAME;
	} else {
	    result += SOURCE_TOKENIZED_FILE_NAME;
	}
	return result;
    }

    public List<String> getTargetTokenizedFileNames(String category) {
	List<String> result = new ArrayList<String>();

	for (String targetPrefix : getTargetPrefixesList(category)) {
	    String path = this.mtFoldersConfig.getCategoryDataFolderName(category) + targetPrefix;
	    if (segmentTargetFileName()) {
		path += SEGMENTED_FILE_SUFFIX;
	    } else {
		path += TOKENIZED_FILE_SUFFIX;
	    }
	    result.add(path);
	}
	return result;
    }

    private String referenceNumerSuffix(int referenceNumber){
	return "" + referenceNumber;
    }
    
    
    public String getTargetTokenizedAndLowerCasedFileNamePrefix(String category){
	 String path = this.mtFoldersConfig.getCategoryDataFolderName(category) + MULTIPLE_REFERENCES_TARGET_PREFIX;
	    if (segmentTargetFileName()) {
		path += SEGMENTED_AND_LOWERCASED_FILE_SUFFIX; 
	    } else {
		path += TOKENIZED_AND_LOWERCASED_FILE_SUFFIX;
	    }
	    return path;
    }
    
    
    public List<String> getTargetTokenizedAndLowerCasedFileNames(String category) {

	List<String> result = new ArrayList<String>();
	
	int numTargetFiles = getTargetPrefixesList(category).size();
	for (int i = 0; i < numTargetFiles ; i++) {	   
	    String path = getTargetTokenizedAndLowerCasedFileNamePrefix(category)  + referenceNumerSuffix(i);
	    result.add(path);
	}
	return result;
    }

    public String getSourceTokenizedAndLowerCasedFileName(String category) {
	String result = this.mtFoldersConfig.getCategoryDataFolderName(category);
	if (segmentSourceFileName()) {
	    result += SOURCE_SEGMENTED_AND_LOWERCASED_FILE_NAME;
	} else {
	    result += SOURCE_TOKENIZED_AND_LOWERCASED_FILE_NAME;
	}
	return result;
    }

    public String getEnrichedSourceFilePath(String category) {
	return this.mtFoldersConfig.getCategoryDataFolderName(category) + SOURCE_ENRICHED_FILE_NAME;
    }

    public String getGMLSrcFileName(String category) {
	return this.mtFoldersConfig.getCategoryDataFolderName(category) + SRC_GML_FILE_NAME;
    }

    public String getGMLRefFileName(String category) {
	return this.mtFoldersConfig.getCategoryDataFolderName(category) + REF_GML_FILE_NAME;
    }

    public String getAlignmentsInputFilePath() {
	return this.mtFoldersConfig.getInputFolderName(MTConfigFile.TRAIN_CATEGORY)
		+ getAlignmentsInputFileName();
    }

    public List<String> getTargetInputFilePaths(String category) {
	List<String> result = new ArrayList<String>();
	for (String targetFileName : getTargetInputFileNames(category)) {
	    result.add(this.mtFoldersConfig.getInputFolderName(category) + targetFileName);
	}
	return result;
    }

    public String getSourceInputFilePath(String category) {
	return this.mtFoldersConfig.getInputFolderName(category) + getSourceInputFileName(category);
    }

    public String getSourceCasedTrainFilePath() {
	return this.mtFoldersConfig.getInputFolderName(MTConfigFile.TRAIN_CATEGORY)
		+ getSourceCasedTrainFileName();
    }

    public String getTargetCasedTrainFilePath() {
	return this.mtFoldersConfig.getInputFolderName(MTConfigFile.TRAIN_CATEGORY)
		+ getTargetCasedTrainFileName();
    }

    public String getAlignmentsInputFileName() {
	return this.mtFoldersConfig.getValue(MTConfigFile.TRAIN_CATEGORY + ALIGNMENTS_NAME_SUFFIX);
    }

    public static String getTargetInputFileNameProperty(String category) {
	return category + TARGET_NAME_SUFFIX;
    }

    public static String getTargetInputFileNamesProperty(String category) {
	return category + TARGET_NAMES_SUFFIX;
    }

    private List<String> getTargetInputFileNames(String category) {
	if (this.mtFoldersConfig.hasValue(getTargetInputFileNamesProperty(category))) {
	    return this.mtFoldersConfig
		    .getMultiValueParmeterAsListWithPresenceCheck(getTargetInputFileNamesProperty(category));
	}
	return Collections.singletonList(this.mtFoldersConfig
		.getValue(getTargetInputFileNameProperty(category)));
    }

    public static String getSourceInputFileNameProperty(String category) {
	return category + SOURCE_NAME_SUFFIX;
    }

    private String getSourceInputFileName(String category) {
	return this.mtFoldersConfig.getValue(getSourceInputFileNameProperty(category));
    }

    public static String getSourceCasedTrainFileNameProperty() {
	return MTConfigFile.TRAIN_CATEGORY + SOURCE_NAME_CASED_SUFFIX;
    }

    private String getSourceCasedTrainFileName() {
	return this.getValueWithPresenceCheck(getSourceCasedTrainFileNameProperty(),
		"MTFilesConfig.getSourceCasedTrainFileName");
    }

    public static String getTargetCasedTrainFileNameProperty() {
	return MTConfigFile.TRAIN_CATEGORY + TARGET_NAME_CASED_SUFFIX;
    }

    private String getTargetCasedTrainFileName() {
	return this.getValueWithPresenceCheck(getTargetCasedTrainFileNameProperty(),
		"MTFilesConfig.getTargetCasedTrainFileName");
    }

    public String getArpaLanguageModeFilePath() {
	return this.mtFoldersConfig.getResourcesFolder() + "LM-"
		+ this.getTargetLanguageAbbreviation() + ".arpa";
    }

    public boolean useExternallyGeneratedLanguageModel() {
	return this.mtFoldersConfig.hasValue(LM_FILE_EXTERNALLY_GENERATED_PATH_PROPERTY);
    }

    public String getLanguageExternallyGeneratedPath() {
	return this.getValueWithPresenceCheck(LM_FILE_EXTERNALLY_GENERATED_PATH_PROPERTY,
		"getLanguageExternallyGeneratedPath");
    }

    public String getBinaryLanguageModelFilePath() {
	if (this.useExternallyGeneratedLanguageModel()) {
	    return this.getLanguageExternallyGeneratedPath();
	} else {
	    return this.mtFoldersConfig.getResourcesFolder() + "LM-"
		    + this.getTargetLanguageAbbreviation() + BINARY_SUFFIX;
	}
    }

    public String getHatsMainGrammarExtractorConfigPathMert() {
	return this.mtFoldersConfig.getMertFolder() + HATS_MAIN_GRAMMAR_EXTRACTOR_CONFIG_NAME_MERT;
    }

    public String getHatsSmoothingGrammarExtractorConfigPathMert() {
	return this.mtFoldersConfig.getMertFolder()
		+ HATS_SMOOTHING_GRAMMAR_EXTRACTOR_CONFIG_NAME_MERT;
    }

    public String getDenseMapPath() {
	return this.mtFoldersConfig.getResourcesFolder() + DENSE_MAP_FILE_NAME;
    }

    public String getDenseMapFileName() {
	return DENSE_MAP_FILE_NAME;
    }

    public String getPackerConfigFilePath() {
	return this.mtFoldersConfig.getResourcesFolder() + PACKER_CONFIG_FILE_NAME;
    }

    public String getDiscriminativeFeaturesListFilePath() {
	return this.mtFoldersConfig.getResourcesFolder() + DISCRIMINATIVEFE_FEATURES_LIST_FILE_NAME;
    }

    public String getPackerConfigFileName() {
	return PACKER_CONFIG_FILE_NAME;
    }

    public String getHatsMainGrammarExtractorConfigPathTest() {
	return this.mtFoldersConfig
		.getCategoryIntermediateResultsFolderName(MTConfigFile.TEST_CATEGORY)
		+ HATS_MAIN_GRAMMAR_EXTRACTOR_CONFIG_NAME_TEST;
    }

    public String getHatsSmoothingGrammarExtractorConfigPathTest() {
	return this.mtFoldersConfig
		.getCategoryIntermediateResultsFolderName(MTConfigFile.TEST_CATEGORY)
		+ HATS_SMOOTHING_GRAMMAR_EXTRACTOR_CONFIG_NAME_TEST;
    }

    public String getFilteredGrammarPathTest() {
	return this.mtFoldersConfig.getIntermediateTestFolder()
		+ this.getValueWithPresenceCheck(FILTERED_GRAMMAR_NAME_TEST_PROPERTY,
			"getFilteredGrammarTest()");
    }
    
    public String getGapLabelConditionalProbabilityTableFilePathTest() {
	return this.mtFoldersConfig.getIntermediateTestFolder()
		+ GAP_LABEL_CONDITIONAL_PROBABILITY_TABLE_FILE_NAME;
    }

    public String getPackedMainGrammarFolderTest() {
	return this.mtFoldersConfig.getIntermediateTestFolder()
		+ PACKED_MAIN_GRAMMAR_FOLDER_NAME_TEST;
    }

    public String getPackedMainGrammarFolderMert() {
	return this.mtFoldersConfig.getMertFolder() + PACKED_MAIN_GRAMMAR_FOLDER_NAME_MERT;
    }

    public String getFilteredGrammarPathMert() {
	return this.mtFoldersConfig.getMertFolder()
		+ this.getValueWithPresenceCheck(FILTERED_GRAMMAR_NAME_MERT_PROPERTY,
			"getFilteredGrammarPathMert()");
    }

    public String getGapLabelConditionalProbabilityTableFilePathMert() {
	return this.mtFoldersConfig.getMertFolder()
		+ GAP_LABEL_CONDITIONAL_PROBABILITY_TABLE_FILE_NAME;
    }
    
    public String getGlueGrammarPathTest() {
	return this.mtFoldersConfig.getIntermediateTestFolder()
		+ this.getValueWithPresenceCheck(GLUE_GRAMMAR_NAME_TEST_PROPERTY,
			"getGlueGrammarPathTest()");
    }

    public String getGlueGrammarPathMert() {
	return this.mtFoldersConfig.getMertFolder()
		+ this.getValueWithPresenceCheck(GLUE_GRAMMAR_NAME_MERT_PROPERTY,
			"getGlueGrammarPathMert()");
    }

    public String getPackedGlueGrammarFolderTest() {
	return this.mtFoldersConfig.getIntermediateTestFolder()
		+ PACKED_GLUE_GRAMMAR_FOLDER_NAME_TEST;
    }

    public String getPackedGlueGrammarFolderMert() {
	return this.mtFoldersConfig.getMertFolder() + PACKED_GLUE_GRAMMAR_FOLDER_NAME_MERT;
    }

    public String getDecoderTestTunedConfigFileName() {
	return this.mtFoldersConfig.getIntermediateRunSpecificTestFolder()
		+ DECODER_TEST_TUNED_CONFIG_FILE_NAME;
    }

    public String getJoshuaDevTunedConfigFileName() {

	if (this.getTheConfig().tunerTypeIsMira()) {
	    String mosesMertWorkDirFolder = this.mtFoldersConfig.getMosesMertWorkDirFolder();
	    return mosesMertWorkDirFolder
		    + JoshuaMiraCommandCreater.JOSHUA_TUNING_FINAL_CONFIG_NAME;
	} else {
	    return this.mtFoldersConfig.getMertRunSpecificFolder() + getFinalJoshuaTuningFileName();
	}
    }

    public String getMosesDevTunedConfigFileName() {
	String mosesMertWorkDirFolder = this.mtFoldersConfig.getMosesMertWorkDirFolder();
	File file = new File(mosesMertWorkDirFolder);
	FileUtil.createFolderIfNotExisting(mosesMertWorkDirFolder);
	Assert.assertTrue(file.exists());
	String result = mosesMertWorkDirFolder + MOSES_INI_DEFAULT_NAME;
	return result;
    }

    public String getFinalJoshuaTuningFileName() {
	String result = "mert_config.joshua.";
	result += getTuner().getTuningCommandName();
	result += TUNING_FINAL_SUFFIX;   
	return result;
    }

    public String getMertConfigFileName() {
	return this.mtFoldersConfig.getMertRunSpecificFolder() + MERT_CONFIG_FILE_NAME;
    }

    public String getMertParametersFileName() {
	return this.mtFoldersConfig.getMertRunSpecificFolder() + MERT_PARAMETERS_FILE_NAME;
    }

    public String getMertDecoderCommandFileName() {
	return this.mtFoldersConfig.getMertRunSpecificFolder() + MERT_DECODER_COMMAND_FILE_NAME;
    }

    public String getDecoderOutputFilePath(String category, boolean useTunedParameters) {
	String result = this.mtFoldersConfig.getCategoryIntermediateResultsFolderNameUsingRunSubfolderIfApplicable(category)
		+ DECODER_OUTPUT_NAME + tuningSuffix(useTunedParameters);
	return result;
    }
    
    /**
     * Convenience method to get the path to the tuned test set output
     * @return
     */
    public String getDecoderOutputFilePathTestSetTuned(){
	return getDecoderOutputFilePath(MTConfigFile.TEST_CATEGORY, true);
    }

    public String getNBestListCombinationTypeSuffix() {

	if (this.mtFoldersConfig.getTheConfig().decoderConfig.useCrunching()) {
	    return CRUNCHING_SUFFIX;
	} else {
	    if (this.mtFoldersConfig.getTheConfig().decoderConfig
		    .sumDerivationsWithSameYieldForNBestMBR()) {
		return POST_PROCESSED_SUMMATION_SUFFIX;
	    } else {
		return POST_PROCESSED_VITERBI_SUFFIX;
	    }
	}
    }

    public String getNBestListMBRInputTypeSuffix() {
	if (this.mtFoldersConfig.getTheConfig().decoderConfig
		.sumDerivationsWithSameYieldForNBestMBR()) {
	    return SUMMATION_SUFFIX;
	} else {
	    return VITERBI_SUFFIX;
	}
    }

    public String getDecoderNBestPostProcessedOutputFilePath(String category,
	    boolean useTunedParameters) {
	String result = this.mtFoldersConfig.getCategoryIntermediateResultsFolderNameUsingRunSubfolderIfApplicable(category)
		+ DECODER_OUTPUT_NAME + tuningSuffix(useTunedParameters)
		+ getNBestListCombinationTypeSuffix();
	return result;
    }

    public String getDecoderNBestOutputFilePathJoshuaFormat(String category,
	    boolean useTunedParameters) {
	String result = this.mtFoldersConfig.getCategoryIntermediateResultsFolderNameUsingRunSubfolderIfApplicable(category)
		+ DECODER_OUTPUT_NAME + tuningSuffix(useTunedParameters) + NBEST_SUFFIX;
	return result;
    }

    public String getDecoderNBestOutputFilePathSystemSpecific(String category,
	    boolean useTunedParameters) {
	String result = this.mtFoldersConfig.getCategoryIntermediateResultsFolderNameUsingRunSubfolderIfApplicable(category)
		+ DECODER_OUTPUT_NAME + tuningSuffix(useTunedParameters) + NBEST_SUFFIX;
	result += SYSTEM_SPECIFIC_SUFFIX;
	return result;
    }

    public String getDecoderNBestMBRRerankingOutputFilePath(String category,
	    boolean useTunedParameters) {
	String result = this.mtFoldersConfig.getCategoryIntermediateResultsFolderNameUsingRunSubfolderIfApplicable(category)
		+ DECODER_OUTPUT_NAME + tuningSuffix(useTunedParameters) + NBEST_MBR_SUFFIX
		+ getNBestListMBRInputTypeSuffix();
	return result;
    }

    private static String tuningSuffix(boolean useTunedParameters) {
	if (useTunedParameters) {
	    return ".TUNED";
	} else {
	    return ".NO_TUNING";
	}

    }

    public String getOneBestOutputFilePath(String category, boolean useTunedParameters) {
	String result = this.mtFoldersConfig.getResultsFolderCategory(category)
		+ ONE_BEST_OUTPUT_NAME + tuningSuffix(useTunedParameters);
	return result;
    }

    public String getOneBestOutputFilePathGML(String category, boolean useTunedParameters) {
	return this.mtFoldersConfig.getResultsFolderCategory(category) + ONE_BEST_OUTPUT_NAME
		+ tuningSuffix(useTunedParameters) + ".gml";
    }

    public String getResultPathForCategory(boolean useTunedParameters, String category) {
	return this.mtFoldersConfig.getResultsFolderCategory(category) + category
		+ SET_RESULT_STRING + tuningSuffix(useTunedParameters) + TXT_SUFFIX;
    }

    private static String getSentencesIndividualScoresResultName(boolean useTunedParameters,
	    String category) {
	return category + SET_RESULT_STRING + tuningSuffix(useTunedParameters)
		+ SENTENCE_INDIVIDUAL_SCORES_STRING + TXT_SUFFIX;
    }

    public static String getSentencesIndividualScoresResultPathForCategory(String systemRootFolder,
	    boolean useTunedParameters, String category) {
	return MTFoldersConfig.getResultsFolderCategory(systemRootFolder, category,null)
		+ getSentencesIndividualScoresResultName(useTunedParameters, category);
    }

    public String getSentencesIndividualScoresResultPathForCategory(boolean useTunedParameters,
	    String category) {
	return this.mtFoldersConfig.getResultsFolderCategory(category)
		+ getSentencesIndividualScoresResultName(useTunedParameters, category);
    }

    public String getSetAndDocID(String category) {
	String parameterName = category + "SetAndDocID";
	if (this.mtFoldersConfig.hasValue(parameterName)) {
	    return this.mtFoldersConfig.getValue(parameterName);
	}
	throw new RuntimeException("MTDataFilesCreater: parameter - " + parameterName
		+ " required by getSetAndDocID but missin in configfile");
    }

    public String getSavedConfigFilePath() {
	return this.mtFoldersConfig.getResultsBaseFolder() + USED_CONFIG_NAME;
    }

    public String getResultsSummarayFilePath() {
	return this.mtFoldersConfig.getRootOutputFolderName() + RESULTS_SUMMARY_FILE_NAME;
    }

    protected MTConfigFile getTheConfig() {
	return this.mtFoldersConfig.getTheConfig();
    }

    @SuppressWarnings("rawtypes")
    public Tuner getTuner() {
	SystemSpecificSupportObjectsFactory systemSpecificSupportObjectsFactory = MTConfigFile
		.createSystemSpecificSupportObjectsFactory(getTheConfig());
	return systemSpecificSupportObjectsFactory.createTunerFromConfig(getTheConfig());
    }

    public boolean useProTuning() {
	return (getTuner().useProTuning());
    }

    public String getValueWithPresenceCheck(String parameterName, String methodName) {
	return this.mtFoldersConfig.getValueWithPresenceCheck(parameterName, methodName);
    }
}
