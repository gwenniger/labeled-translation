package mt_pipeline.mt_config;

public class GrammarFilesLocations {

	private final String mainGrammarExtractionConfigFilePath;
	private final String wordEnrichmentSmoothingGrammarExtractionConfigFilePath;
	private final String resultGrammarFilePath;
	private final String glueGrammarFilePath;
	private final String denseMapFilePath;
	private final String packerConfigFilePath;
	private final String gapLabelConditionalProbabilityTableFilePath;

	private GrammarFilesLocations(String mainGrammarExtractionConfigFilePath, String wordEnrichmentsmoothingGrammarExtractionConfigFilePath, String resultGrammarFilePath,
			String glueGrammarFilePath, String denseMapFilePath, String packerConfigFilePath,
			String gapLabelConditionalProbabilityTableFilePath) {
		this.mainGrammarExtractionConfigFilePath = mainGrammarExtractionConfigFilePath;
		this.wordEnrichmentSmoothingGrammarExtractionConfigFilePath = wordEnrichmentsmoothingGrammarExtractionConfigFilePath;
		this.resultGrammarFilePath = resultGrammarFilePath;
		this.glueGrammarFilePath = glueGrammarFilePath;
		this.denseMapFilePath = denseMapFilePath;
		this.packerConfigFilePath = packerConfigFilePath;
		this.gapLabelConditionalProbabilityTableFilePath = gapLabelConditionalProbabilityTableFilePath;
	}

	public static GrammarFilesLocations createGrammarFilesLocations(String mainGrammarExtractionConfigFilePath,
			String wordEnrichmentsmoothingGrammarExtractionConfigFilePath, String resultGrammarFilePath, String glueGrammarFilePath, String denseMapFilePath, String packerConfigFilePath,
			String gapLabelConditionalProbabilityTableFilePath) {
		return new GrammarFilesLocations(mainGrammarExtractionConfigFilePath, wordEnrichmentsmoothingGrammarExtractionConfigFilePath, resultGrammarFilePath, glueGrammarFilePath,
				denseMapFilePath, packerConfigFilePath,gapLabelConditionalProbabilityTableFilePath);
	}

	public String getMainGrammarExtractionConfigFilePath() {
		return this.mainGrammarExtractionConfigFilePath;
	}

	public String getWordEnrichmentSmoothingGrammarExtractionConfigFilePath() {
		return this.wordEnrichmentSmoothingGrammarExtractionConfigFilePath;
	}

	public String getResultGrammarFilePath() {
		return this.resultGrammarFilePath;
	}

	public String getGlueGrammarFilePath() {
		return this.glueGrammarFilePath;
	}

	public String getDenseMapFilePath() {
		return this.denseMapFilePath;
	}

	public String getPackerConfigFilePath() {
		return this.packerConfigFilePath;
	}

	public String getGapLabelConditionalProbabilityTableFilePath() {
	    return gapLabelConditionalProbabilityTableFilePath;
	}
}
