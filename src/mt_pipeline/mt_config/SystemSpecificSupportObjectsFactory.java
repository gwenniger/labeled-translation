package mt_pipeline.mt_config;

import mt_pipeline.DecoderCommandCreater;
import mt_pipeline.MTPipelineHATsSAMT;
import mt_pipeline.MTPipelineHatsBoundaryTagLabeled;
import mt_pipeline.MTPipelineHatsCCL;
import mt_pipeline.tuning.Tuner;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatter;
import grammarExtraction.translationRules.TranslationRuleRepresentationCreater;
import util.ConfigFile;

public abstract class SystemSpecificSupportObjectsFactory {

	public abstract FeatureValueFormatter createFeatureValueFormatter(ConfigFile theConfig);

	public abstract TranslationRuleRepresentationCreater createTranslationRuleRepresentationCreater(ConfigFile theConfig,String systemName);

	public abstract SystemSpecificConfig createSystemSpecificConfiguration(MTConfigFile theConfig);

	public abstract DecoderCommandCreater createDecoderCommandCreaterFromConfig(MTConfigFile theConfig);

	public abstract Tuner createTunerFromConfig(MTConfigFile theConfig);

	public abstract String getUnknownWordsLabel(ConfigFile theConfig, String systemName);
	
	private boolean isSAMTSystem(String systemName) {
		return systemName.equals(MTPipelineHATsSAMT.SystemName);
	}

	private boolean isHatsBoundaryTagLabeledSystem(String systemName) {
		return systemName.equals(MTPipelineHatsBoundaryTagLabeled.SystemName);
	}

	private boolean isCoreContextLabeledSystem(String systemName) {
		return systemName.equals(MTPipelineHatsCCL.SystemName);
	}

	private boolean isCopmplexLabelPipelineCategory(String systemName) {
		return (isSAMTSystem(systemName) || isHatsBoundaryTagLabeledSystem(systemName) || isCoreContextLabeledSystem(systemName));
	}

	public boolean useNonHieroLabels(ConfigFile theConfig, String systemName) {
		return isCopmplexLabelPipelineCategory(systemName) || MTGrammarExtractionConfig.getReorderinLabelSettings(theConfig).useReorderingLabelExtension();
	}
}
