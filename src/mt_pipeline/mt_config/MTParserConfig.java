package mt_pipeline.mt_config;

import java.util.Arrays;
import java.util.List;

import mt_pipeline.parserInterface.BerkeleyParserInterface;
import mt_pipeline.parserInterface.CharniakParserInterface;

public class MTParserConfig extends MTMetaDataCreatorConfig {

    public static final String PARSER_NAME_PROPERTY = "parserName";
    public static final String BERKELEY_PARSER_JAR_LOCATION = "berkeleyParserJarLocation";
    public static final String CHRANIAK_PARSER_ROOT_DIR_PROPERTY = "charniakParserRootDir";
    private static final String BERKELEY_PARSER_SOURCE_GRAMMAR_LOCATION = "berkeleyParserSourceGrammarLocation";
    private static final String BERKELEY_PARSER_TARGET_GRAMMAR_LOCATION = "berkeleyParserTargetGrammarLocation";
    public static final String PARSE_SOURCE_FILE_PROPERTY = "parseSourceFile";
    public static final String PARSE_TARGET_FILE_PROPERTY = "parseTargetFile";
    private static final String PARSER_RESULT_FILE_SUFFIX = ".Parses";
    public static final String NUM_PARSER_PROCESSES = "num_parallel_parsers";
    private static final String PRE_PROCESSED_PARSING_SUFFIX = ".PreprocessedForParsing";

    public static enum ParserType {
	BERKELEY_PARSER, CHARNIAK_PARSER
    };

    protected MTParserConfig(MTConfigFile theConfig) {
	super(theConfig);
    }

    public String getBerkeleyParserJarLocation() {
	return this.theConfig.getValueWithPresenceCheck(BERKELEY_PARSER_JAR_LOCATION,
		"getBerkeleyParserJarLocation");
    }

    public String getCharniakParserRootDir() {
	return this.theConfig.getValueWithPresenceCheck(CHRANIAK_PARSER_ROOT_DIR_PROPERTY,
		"getCharniakParserRootDir");
    }

    public String getBerkeleyParserSourceGrammarLocation() {
	return this.theConfig.getValueWithPresenceCheck(BERKELEY_PARSER_SOURCE_GRAMMAR_LOCATION,
		"getBerkeleyParserSourceGrammarLocation");
    }

    public String getBerkeleyParserTargetGrammarLocation() {
	return this.theConfig.getValueWithPresenceCheck(BERKELEY_PARSER_TARGET_GRAMMAR_LOCATION,
		"getBerkeleyParserTargetGrammarLocation");
    }

    public String getParseOutputFileName(String inputFileName, String parserName) {
	return inputFileName + PARSER_RESULT_FILE_SUFFIX + "." + parserName;
    }

    public String getParseOutputFileNameForParserFormat(String inputFileName,
	    String parserFormatSuffix) {
	return inputFileName + PARSER_RESULT_FILE_SUFFIX + parserFormatSuffix;
    }

    public String getSourceParseOutputFileName(String parserName) {
	return getParseOutputFileName(
		getSourceInputFilePreprocessedPath(MTConfigFile.TRAIN_CATEGORY), parserName);
    }

    public String getTargetParseOutputFileName() {
	return getParseOutputFileName(
		getTargetInputFilePreprocessedPathTrain(),
		getParserNameString());
    }

    public String getParsePartOutputFileNameBerkeleyFormat(String inputFileName,
	    String parserFormatSuffix) {
	String[] parts = inputFileName.split("Part");
	return parts[0] + PARSER_RESULT_FILE_SUFFIX + parserFormatSuffix + "Part" + parts[1];
    }

    public int getNumParserProcessesSpecification() {
	return Integer.parseInt(this.theConfig.getValueWithPresenceCheck(NUM_PARSER_PROCESSES,
		"getNumParserProcessesSpecification"));
    }

    public boolean parseSourceFile() {
	return Boolean.parseBoolean(this.theConfig.getStringIfPresentOrReturnDefault(
		PARSE_SOURCE_FILE_PROPERTY, "false"));
    }

    public boolean parseTargetFile() {
	return Boolean.parseBoolean(this.theConfig.getStringIfPresentOrReturnDefault(
		PARSE_TARGET_FILE_PROPERTY, "false"));
    }

    private static List<String> parserNames() {
	return Arrays.asList(BerkeleyParserInterface.getParserName(),
		CharniakParserInterface.getParserName());
    }

    public String getParserNameString() {
	String parserName = this.theConfig.getValueWithPresenceCheck(PARSER_NAME_PROPERTY);

	if (parserNames().contains(parserName)) {
	    return parserName;
	} else {
	    String errorMessage = "";
	    errorMessage += "\nError : invalid parser name specified in configuration file. "
		    + PARSER_NAME_PROPERTY + " has value " + parserName + " in configuration file";
	    errorMessage += "Please chose a value from: ";
	    for (String parseName : parserNames()) {
		errorMessage += parseName + "  ";
	    }
	    throw new RuntimeException(errorMessage);
	}
    }

    public ParserType getParserType() {
	String parserName = getParserNameString();

	if (parserName.equals(BerkeleyParserInterface.getParserName())) {
	    return ParserType.BERKELEY_PARSER;
	} else if (parserName.equals(CharniakParserInterface.getParserName())) {
	    return ParserType.CHARNIAK_PARSER;
	} else {
	    throw new RuntimeException("Error: unrecognized parser name");
	}
    }

    @Override
    protected String preprocessedFileSuffix() {
	return PRE_PROCESSED_PARSING_SUFFIX;
    }
}
