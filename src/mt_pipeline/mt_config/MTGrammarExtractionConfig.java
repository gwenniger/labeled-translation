package mt_pipeline.mt_config;

import grammarExtraction.chiangGrammarExtraction.GrammarExtractionConstraints;
import grammarExtraction.chiangGrammarExtraction.LabelSideSettings;
import grammarExtraction.chiangGrammarExtraction.ReorderLabelingSettings;
import reorderingLabeling.CoarsePhraseCentricReorderingLabel;
import grammarExtraction.reorderingLabeling.CoarseReorderingFeatureSet;
import grammarExtraction.reorderingLabeling.FineHatReorderingFeaturesSet;
import grammarExtraction.reorderingLabeling.HATComplexityTypeReorderingLabel;
import grammarExtraction.reorderingLabeling.HatLabelRefinedPhraseCentricReorderingLabel;
import grammarExtraction.reorderingLabeling.PhraseCentricPlusParentRelativeReorderingLabel;
import reorderingLabeling.CoarseParentRelativeReorderingLabel;
import reorderingLabeling.FineParentRelativeReorderingLabel;
import reorderingLabeling.FinePhraseCentricReorderingLabelBasic;
import reorderingLabeling.ThreeCaseOnlyPhraseCentricReorderingLabel;
import grammarExtraction.reorderingLabeling.ReorderingFeaturesSet;
import reorderingLabeling.ReorderingLabelCreater;
import grammarExtraction.reorderingLabeling.ReorderingFeaturesSet.ReorderingFeaturesSetType;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;

import util.ConfigFile;

public class MTGrammarExtractionConfig {

	public static final String USE_SOURCE_SIDE_LABELS_IN_RULE_FILTERING_CONFIG_PROPERTY = "useSourceSideLabelsInRuleFiltering";
	public static final String GRAMMAR_SOURCE_FILTER_PATH_CONFIG_PROPERTY = "grammerSourceFilterFilePath";
	public static final String GRAMMAR_SOURCE_FILTER_TAGS_FILE_PATH_CONFIG_PROPERTY = "grammerSourceFilterTagsFilePath";
	public static final String USE_CANONICAL_FORM_LABELED_RULES_PROPERTY = "useCanonicalFormLabeledRules";
	public static final String USE_ONLY_HIERO_WEIGHTS_FOR_CANONICAL_FORM_LABELED_RULES_PROPERTY = "useOnlyHieroWeightsForCanonicalFormLabeledRules";
	public static final String RESTRICT_LABEL_VARIATION_FOR_CANONICAL_FORM_LABELED_RULES_PROPERTY = "restrictLabelVariationForCanonicalFormLabeledRules";
	public static final String NUM_ALLOWED_TYPES_PER_LABEL_FOR_SOURCE_PROPERTY = "numAllowedTypesPerLabelForSource";
	final private ConfigFile theConfig;

	private MTGrammarExtractionConfig(ConfigFile theConfig) {
		this.theConfig = theConfig;
	}

	public static MTGrammarExtractionConfig createMTGrammarExtractionConfig(String configFilePath) {
		try {
			return new MTGrammarExtractionConfig(new ConfigFile(configFilePath));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public static MTGrammarExtractionConfig createMTGrammarExtractionConfig(ConfigFile theConfig) {
		return new MTGrammarExtractionConfig(theConfig);
	}

	public boolean allowDanglingSecondNonterminal() {
		return theConfig.getBooleanWithPresenceCheck(GrammarExtractionConstraints.ALLOW_DANGLING_SECONDNOND_TERMINAL_PROPERTY);
	}

	public boolean allowConsecutiveNonterminalsInNonAbstractRules() {
		return theConfig.getBooleanWithPresenceCheck(GrammarExtractionConstraints.ALLOW_CONSECUTIVE_NONTERMINALS_IN_NON_ABSTRACTRULES_PROPERTY);
	}

	public boolean allowSourceAbstract() {
		return theConfig.getBooleanWithPresenceCheck(GrammarExtractionConstraints.ALLOW_SOURCE_ABSTRACT_PROPERTY);
	}

	public boolean allowtargetWordsWithoutSource() {
		return theConfig.getBooleanWithPresenceCheck(GrammarExtractionConstraints.ALLOW_TARGET_WORDS_WITHOUT_SOURCE_PROPERTY);
	}

	public boolean allowDoublePlus() {
		return theConfig.getBooleanWithPresenceCheck(GrammarExtractionConstraints.ALLOW_DOUBLE_PLUS_PROPERTY);
	}

	public boolean allowPosTagLabelFallback() {
		return theConfig.getBooleanWithPresenceCheck(GrammarExtractionConstraints.ALLOW_POS_TAG_LABEL_FALLBACK_PROPERTY);
	}

	public ReorderLabelingSettings getReorderinLabelSettings() {
		return getReorderinLabelSettings(theConfig);
	}

	public boolean useReorderingLabelExtension() {
		return getReorderinLabelSettings(theConfig).useReorderingLabelExtension();
	}

	private static List<String> getSupportedReorderLabelTypeStrings() {
		return Arrays.asList(FineParentRelativeReorderingLabel.getLabelTypeName(),CoarseParentRelativeReorderingLabel.getLabelTypeName(), FinePhraseCentricReorderingLabelBasic.getLabelTypeName(), CoarsePhraseCentricReorderingLabel.getLabelTypeName(),
				HatLabelRefinedPhraseCentricReorderingLabel.getLabelTypeName(), ThreeCaseOnlyPhraseCentricReorderingLabel.getLabelTypeName(),
				PhraseCentricPlusParentRelativeReorderingLabel.getLabelTypeName(),HATComplexityTypeReorderingLabel.getLabelTypeName());
	}

	private static boolean mustUseThreeCaseReorderingLabeling(ConfigFile theConfig) {
		return useReorderingLabelExtension(theConfig) && (!reorderLabelHieroRules(theConfig));
	}

	public static ReorderingLabelCreater getReorderingLabelCreater(ConfigFile theConfig) {

		Boolean useReorderingLabeling = theConfig.getBooleanIfPresentOrReturnDefault(GrammarExtractionConstraints.USE_REORDERING_LABEL_EXTENSION_PROPERTY, false);
		if ((!useReorderingLabeling) || (!theConfig.hasValue(GrammarExtractionConstraints.REORDER_LABEL_TYPE_PROPERTY))) {
			return null;
		}

		String reorderTypeProperty = theConfig.getValueWithPresenceCheck(GrammarExtractionConstraints.REORDER_LABEL_TYPE_PROPERTY);

		if (mustUseThreeCaseReorderingLabeling(theConfig)) {
			if (reorderTypeProperty.equals(ThreeCaseOnlyPhraseCentricReorderingLabel.getLabelTypeName())) {
				return ThreeCaseOnlyPhraseCentricReorderingLabel.createReorderingLabelCreater();
			}

			throw new RuntimeException("Error: You are using reorderlabeling with no reorder labeling of hiero rules (only abstract rules are reordering labeled)" + "- this means you must use "
					+ ThreeCaseOnlyPhraseCentricReorderingLabel.getLabelTypeName() + " for the configuration value " + GrammarExtractionConstraints.REORDER_LABEL_TYPE_PROPERTY);

		}

		if (reorderTypeProperty.equals(ThreeCaseOnlyPhraseCentricReorderingLabel.getLabelTypeName())) {
			return ThreeCaseOnlyPhraseCentricReorderingLabel.createReorderingLabelCreater();
		} else if (reorderTypeProperty.equals(FineParentRelativeReorderingLabel.getLabelTypeName())) {
			return FineParentRelativeReorderingLabel.createReorderingLabelCreater();
		} else if (reorderTypeProperty.equals(CoarseParentRelativeReorderingLabel.getLabelTypeName())) {
			return CoarseParentRelativeReorderingLabel.createReorderingLabelCreater();
		}else if (reorderTypeProperty.equals(FinePhraseCentricReorderingLabelBasic.getLabelTypeName())) {
			return FinePhraseCentricReorderingLabelBasic.createReorderingLabelCreater();
		} else if (reorderTypeProperty.equals(CoarsePhraseCentricReorderingLabel.getLabelTypeName())) {
			return CoarsePhraseCentricReorderingLabel.createReorderingLabelCreater();
		} else if (reorderTypeProperty.equals(HatLabelRefinedPhraseCentricReorderingLabel.getLabelTypeName())) {
			return HatLabelRefinedPhraseCentricReorderingLabel.createReorderingLabelCreater();
		} else if(reorderTypeProperty.equals(PhraseCentricPlusParentRelativeReorderingLabel.getLabelTypeName())){
		    	return PhraseCentricPlusParentRelativeReorderingLabel.createReorderingLabelCreater(); 
		}
		else if(reorderTypeProperty.equals(HATComplexityTypeReorderingLabel.getLabelTypeName())){
		    	return HATComplexityTypeReorderingLabel.createReorderingLabelCreater(); 
		}
		  else {
			throw new RuntimeException("Error : unrecognized reorderTypeProperty - Please select one from :" + getSupportedReorderLabelTypeStrings());
		}
	}

	public static boolean useReorderingLabelBinaryFeaturesWithConsistencyCheck(ConfigFile theConfig) {
		boolean useReorderingLabelExtension = theConfig.getBooleanIfPresentOrReturnDefault(GrammarExtractionConstraints.USE_REORDERING_LABEL_EXTENSION_PROPERTY, false);
		boolean useReorderingLabelBinaryFeatures = theConfig.getBooleanIfPresentOrReturnDefault(GrammarExtractionConstraints.USE_REORDERING_LABEL_BINARY_FEATURES_PROPERTY, false);

		if (useReorderingLabelBinaryFeatures) {
			if (!useReorderingLabelExtension) {
				throw new RuntimeException(
						"Error: MTGrammarExtractionConfig.  useReorderingLabelBinaryFeaturesWithConsistencyCheck - Inconsistent configuration file: trying to use reorderingLabelingBinaryFeatures without actually using reorderingLabeling");
			}
		}
		return useReorderingLabelBinaryFeatures;
	}

	public static ReorderingFeaturesSet.ReorderingFeaturesSetType getReorderingFeaturesSetType(ConfigFile theConfig) {
		String typeString = theConfig.getValueWithPresenceCheck(GrammarExtractionConstraints.REORDERING_FEATURE_SET_TYPE_PROPERTY);

		if (typeString.equals(CoarseReorderingFeatureSet.COARSE_REORDERING_FEATURES_SET_NAME)) {
			return ReorderingFeaturesSetType.COARSE_REORDERING_FEATURES_SET;
		} else if (typeString.equals(FineHatReorderingFeaturesSet.FINE_HAT_REORDERING_FEATURES_SET_NAME)) {
			return ReorderingFeaturesSetType.FINE_HAT_REORDERING_FEATURES_SET;
		} else {
			throw new RuntimeException("Error: Unrecognized reordering feature set type - please specify one of : " + CoarseReorderingFeatureSet.COARSE_REORDERING_FEATURES_SET_NAME + " or "
					+ FineHatReorderingFeaturesSet.FINE_HAT_REORDERING_FEATURES_SET_NAME);
		}
	}

	public static boolean useReorderLabelExtensiont(ConfigFile theConfig) {
		boolean useReorderingLabelExtension = theConfig.getBooleanIfPresentOrReturnDefault(GrammarExtractionConstraints.USE_REORDERING_LABEL_EXTENSION_PROPERTY, false);
		return useReorderingLabelExtension;
	}

	public static boolean reorderLabelHieroRules(ConfigFile theConfig) {
		boolean defaultReorderLabelHieroRules = useReorderingLabelExtension(theConfig) ? true : false;
		boolean reorderLabelHieroRules = theConfig.getBooleanIfPresentOrReturnDefault(GrammarExtractionConstraints.REORDER_LABEL_HIERO_RULES_PROPERTY, defaultReorderLabelHieroRules);
		return reorderLabelHieroRules;
	}

	public static ReorderLabelingSettings getReorderinLabelSettings(ConfigFile theConfig) {

		boolean reorderLabelPhrasePairs = theConfig.getBooleanIfPresentOrReturnDefault(GrammarExtractionConstraints.REORDER_LABEL_PHRASE_PAIRS_PROPERTY, false);
		return ReorderLabelingSettings.createReorderLabelingSettings(getReorderingLabelCreater(theConfig), useReorderingLabelExtension(theConfig), reorderLabelHieroRules(theConfig),
				reorderLabelPhrasePairs);
	}

	public static boolean useReorderingLabelExtension(ConfigFile theConfig) {
		return theConfig.getBooleanIfPresentOrReturnDefault(GrammarExtractionConstraints.USE_REORDERING_LABEL_EXTENSION_PROPERTY, false);
	}

	public static boolean useMosesHieroUnknownWordRuleVariants(ConfigFile theConfig) {
		boolean result = theConfig.getBooleanIfPresentOrReturnDefault(GrammarExtractionConstraints.USE_MOSES_HIERO_UNKNOWN_WORD_RULE_VARIANTS_PROPERTY, false);
		return result;
	}

	public String getUnaryCategoryHandlerType() {
		String unaryCategoryHandlerType = this.theConfig.getValueWithPresenceCheck(GrammarExtractionConstraints.UNARY_CATEGORY_HANDLER_TYPE_PROPERTY);
		return unaryCategoryHandlerType;
	}

	public LabelSideSettings getLabelSideSettings() {
		return LabelSideSettings.getLabelSideSettingsFromConfig(theConfig);
	}

	public static String getGrammerSourceFilterPath(ConfigFile theConfig) {
		String filterFileName = theConfig.getStringIfPresentOrReturnDefault(GRAMMAR_SOURCE_FILTER_PATH_CONFIG_PROPERTY, "grammerSourceFilterFilePath");
		return filterFileName;
	}

	public static String getSourceTagFilePath(ConfigFile configFile) {
		return configFile.getValueWithPresenceCheck(GRAMMAR_SOURCE_FILTER_TAGS_FILE_PATH_CONFIG_PROPERTY);
	}
	
	public static boolean useSourceSideLabelsInRuleFiltering(ConfigFile configFile) {
		return configFile.getBooleanWithPresenceCheck(USE_SOURCE_SIDE_LABELS_IN_RULE_FILTERING_CONFIG_PROPERTY);
	}
	
	public static boolean useCanonicalFormLabeledRules(ConfigFile theConfig){
	    return theConfig.getBooleanWithPresenceCheck(USE_CANONICAL_FORM_LABELED_RULES_PROPERTY);
	}
	
	public boolean  useCanonicalFormLabeledRules(){
		return useCanonicalFormLabeledRules(theConfig);
	}
	
	
	public static boolean useOnlyHieroWeightsForCanonicalFormLabeledRules(ConfigFile configFile){
	    if(useCanonicalFormLabeledRules(configFile)){	
		return configFile.getBooleanWithPresenceCheck(USE_ONLY_HIERO_WEIGHTS_FOR_CANONICAL_FORM_LABELED_RULES_PROPERTY);
	    }
	    return false;
	}
	
	public boolean useOnlyHieroWeightsForCanonicalFormLabeledRules(){
	    return useOnlyHieroWeightsForCanonicalFormLabeledRules(theConfig);
	}
	
	public boolean restrictLabelVariationForCanonicalFormLabeledRules()
	{
	    if(useCanonicalFormLabeledRules()){
		return theConfig.getBooleanWithPresenceCheck(RESTRICT_LABEL_VARIATION_FOR_CANONICAL_FORM_LABELED_RULES_PROPERTY);
	    }
	    return false;
	}
	
	public int numAllowedTypesPerLabelForSource()
	{
	    if(restrictLabelVariationForCanonicalFormLabeledRules()){
		String valueString = theConfig.getValueWithPresenceCheck(NUM_ALLOWED_TYPES_PER_LABEL_FOR_SOURCE_PROPERTY);
		return Integer.parseInt(valueString);
	    }
	    return -1;
	}
}
