package mt_pipeline.mt_config;

import datapreparation.SegmenterConfig;

public class MTSegmenterConfig {
    private static String CHINESE_LANGUAGE_ABBREVIATION = "zh";

    private MTConfigFile theConfig;

    private MTSegmenterConfig(MTConfigFile theConfig) {
	this.theConfig = theConfig;
    }

    public static MTSegmenterConfig createMTSegmenterConfig(MTConfigFile theConfig) {
	return new MTSegmenterConfig(theConfig);
    }

    public String getStanfordSegmenterRootLocation() {
	return this.theConfig
		.getValueWithPresenceCheck(SegmenterConfig.STANFORD_SEGMENTER_ROOT_DIR_LOCATION);
    }

    public int getNumSegmenterThreads() {
	String valueString = this.theConfig
		.getValueWithPresenceCheck(SegmenterConfig.NUM_SEGMENTER_THREADS_PROPERTY);
	return Integer.parseInt(valueString);
    }

    public boolean segmentInsteadOfTokenize(String language) {
	return language.equals(CHINESE_LANGUAGE_ABBREVIATION);
    }

    public String getSegmentationModelSpecificationString() {
	System.out
		.println("Note: Take care to use the same segmantation model during decoding as the one used for segmenting the training data");
	return SegmenterConfig.getSegmentationModelSpecificationString(this.theConfig);
    }

}
