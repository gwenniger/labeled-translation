package mt_pipeline.mt_config;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import integration_tests.MTPipelineTest.TUNER_TYPE;
import java.util.Arrays;
import java.util.List;
import util.ConfigFile;
import util.Utility;

public class MTDecoderConfig {
    final private MTConfigFile theConfig;

    public static final String JOSHUA_HOME_DIR_PROPERTY = "joshuaHomeDir";
    public static final String MOSES_HOME_DIR_PROPERTY = "mosesHomeDir";
    private static final String LIB_DIR = "lib/";
    private static final String CLASS_DIR = "class/";
    public static final String NUM_PARALLEL_DECODERS_PROPERTY = "num_parallel_decoders";
    public static final String NUM_TUNER_THREADS_PROPERTY = "num_tuner_threads";
    public static final String NUM_PARALLEL_GRAMMAR_EXTRACTORS_PROPERTY = "num_parallel_grammar_extractors";
    public static final String TUNING_NBEST_SIZE_PROPERTY = "top_n";
    public static final int TUNING_DEFAULT_NBEST_SIZE = 300;
    public static final String NUM_TRANSLATION_OPTIONS_PROPERTY = "num-translation-options";
    public static final int TEST_NBEST = 1;
    public static final String NO_MERT_ITERATIONS_PROPERTY = "noMertIterations";
    private static final String DATA_LENGTH_SUFFIX = "DataLength";
    public static final String PRO_TUNING_TYPE = "pro";
    public static final String MERT_TUNING_TYPE = "mert";
    public static final String MIRA_TUNING_TYPE = "mira";
    public static final String RESTART_TUNING_FROM_LAST_COMPLETED_ITERATION_FLAG = "restartTuningFromLastCompletedItartion";
    public static final String MIRA_REGULARIZATION_PARAMETER_PROPERTY = "miraRegularizationParameter";
    public static final String TUNER_TYPE_PROPERTY = "tuner";
    public static final String DISCRIMINATIVE_FEATURE_INITIAL_WEIGHT_PROPERTY = "discriminativeFeatureInitialWeight";
    public static final String NBEST_FORMAT_PROPERTY_STRING = "nbestFormat";
    public static final String SPARSE_NBEST_FORMAT_STRING = "sparse";
    public static final String DENSE_NBEST_FORMAT_STRING = "dense";
    public static final String TUNING_STRATEGY_MODE_PROPERTY = "tuningStrategyMode";
    public static final String TUNING_INTERPOLATION_COEFFICIENT_PROPERTY = "tuningInterpolationCoefficient";
    private static final String BIN_DIR_SUFFIX = "bin/";
    private static final String MOSES_CHART_DECODER_FILE_NAME = "moses_chart";
    private static final String MOSES_SCRIPTS_DIR_SUFFIX = "scripts/";
    private static final String MOSES_TRAINING_SCRIPTS_DIR_SUFFIX = "scripts/training/";
    public static final String RERANKING_PROPERTY = "useDecoderOutputReranking";
    public static final String NO_RERANKING = "none";
    public static final String MINIMUM_BAYES_RISK = "mbr";
    public static final String CRUNCHING_RERANKING = "crunching";
    private static final String MOSES_SHOW_WEIGTHS_FLAG = "-n-best-list";
    public static final String NBEST_SIZE_FOR_NBEST_MBR_PROPERTY = "nBestSizeForNBestMBR";
    public static final String MAX_NUM_UNIQUE_TRANSLATIONS_FOR_NBEST_MBR_PROPERTY = "maxNumUniqueTranslationsForNBestMBR";
    public static final String SUM_DERIVATIONS_WITH_SAME_YIELD_FOR_NBEST_MBR_PARAMETER = "sumDerivationsWithSameYieldForNbestMBR";
    public static final String JOSHUA_MIRA_INTERFACE_PROGRAM_RELATIVE_PATH = "scripts/training/mira/run-mira.pl";
    // public static final String JOSHUA_MIRA_INTERFACE_PROGRAM_RELATIVE_PATH =
    // "scripts/training/mira/run-mira-AlternativeVersion.pl";
    public static final String MOSES_STYLE_TUNING_PROPERTY_NAME = "moses";
    public static final String FUZZY_MATCHING_PROPERTY_NAME = "fuzzy_matching";
    public static final String REMOVE_LABELS_INSIDE_GRAMMAR_TRIE_FOR_MORE_EFFICIENT_FUZZY_MATCHING_PROPERTY_NAME = "remove_labels_inside_grammar_trie_for_more_efficient_fuzzy_matching";
    public static final String EXPLORE_ALL_DISTINCT_LABLED_RULE_VERSIONS_IN_CUBE_PRUNING_INITIALIZATION_PROPERTY_NAME = "explore_all_distinct_labled_rule_versions_in_cube_pruning_initialization";
    public static final String USE_SHUFFLING_TO_RANDOMIZE_RULE_ORDER_OF_ADDING_RULES_TO_INITIAL_CUBE_PRUNING_QUEUE_PROPERTY_NAME = "use_shuffling";
    public static final String USE_DOT_CHART_PROPERTY_STRING = "use_dot_chart";
    public static final String USE_DOT_CHART_FLAG_NAME_JOSHUA = "no-dot-chart";
    public static final String SEPARATE_CUBE_PRUNING_STATES_MATCHING_SUBSTITUTIONS_PROPERTY_NAME = "separate_cube_pruning_states_for_matching_substitutions";
    public static final String EXPLORE_ALL_LABELS_FOR_GLUE_RULES_IN_CUBE_PRUNING_INITIALIZATION_PROPERTY_NAME = "explore_all_labels_for_glue_rules_in_cube_pruning_initialization";
    public static final String EXPLORE_ALL_POSSIBLE_LABEL_SUBSTITUTIONS_FOR_ALL_RULES_IN_CUBE_PRUNING_INITIALIZATION_PROPERTY_NAME = "explore_all_possible_label_substitutions_for_all_rules_in_cube_pruning_initialization";
    public static final String LABEL_SUBSTITUTION_FEATURE_PROPERTY = "useLabelSubstitutionFeatures";
    public static final String SPARSE_LABEL_SUBSTITUTION_FEATURE_PROPERTY = "useSparseLabelSubstitutionFeatures";
    public static final String USE_LABEL_SPLITTING_SMOOTHING_WITH_ONLY_LABEL_SUBSTITUTION_FEATURES_FOR_SPLITTED_LABELS_PROPERTY = "useLabelSplittingSmoothingWithOnlySinglLabelLabelSubstitutionFeatures";
    public static final String USE_LABEL_SPLITTING_SMOOTHING_PROPERTY_FOR_LABEL_SUBSTITUTION_FEATURES = "useLabelSplittingSmoothingForLabelSubstitutionFeatures";
    private static final String LABEL_SUBSTITUTION_FEATURE_NAME_JOSHUA = "LabelSubstitution";
    private static final String RULE_APPLICATION_FEATURE_NAME_JOSHUA = "RuleApplication";
    public static final String RULE_APPLICATION_FEATURE_PROPERTY = "useRuleApplicationFeatures";
    private static final String SPARSE_LABEL_SUBSTITUTION_FEATURE_NAME_JOSHUA = "LabelSubstitutionSparse";
    private static final String DOUBLE_LABEL_WITH_LABEL_SPLITTING_SMOOTHING_ARGUMENT = "DoubleLabelWithLabelSplittingSmoothing";
    private static final String DOUBLE_LABEL_WITH_LABEL_SPLITTING_SMOOTHING_ONLY_SINGLE_LABEL_FEATURES_ARGUMENT = "DoubleLabelWithLabelSplittingSmoothingOnlySingleLabelFeatures";
    public static final String CUBE_PRUNING_POP_LIMIT_PROPERTY = "cubePruningPopLimit";
    public static final String MAX_NUMBER_ALTERNATIVE_LABELED_VERSIONS_PER_LANGUAGE_MODEL_STATE = "max_number_alternative_labeled_versions_per_language_model_state";
    public static final String KENLM_NAME = "kenlm";
    public static final String BERKELEYLM_NAME = "berkeleylm";
    public static final String LANGUAGE_MODEL_NAME_PROPERTY = "languageModelName";
    public static final String LANGUAGE_MODEL_ORDER_PROPERTY = "languageModelOrder";
    public static final String USE_FOREST_MIRA_JOSHUA_PROPERTY = "useForestMiraJoshua";
    public static final String TUNE_NON_BASIC_FEATURES_SEPERATELY_PROPERTY = "tuneNonBasicFeaturesSeperately";
    public static final String LOG_CPU_COMPUTATION_TIMES_PROPERTY = "log_cpu_computation_times";
    private static final String CPU_COMPUTATION_TIMES_LOG_FILE_PATH_PROPERTY = "cpu_computation_times_log_file_path";
    private static final String CPU_COMPUTATION_LOG_FILE_SUFFFIX = ".CpuComputationLogFile.txt"; 

    protected MTDecoderConfig(MTConfigFile theConfig) {
	this.theConfig = theConfig;
    }

    public String getJoshuaHomeDir() {
	return this.theConfig.getValueWithPresenceCheck(JOSHUA_HOME_DIR_PROPERTY,
		"getJoshuaHomeDir()");
    }

    public String getMosesHomeDir() {
	return this.theConfig.getValueWithPresenceCheck(MOSES_HOME_DIR_PROPERTY,
		"getMosesHomeDir()");
    }

    public String getMosesScriptsDir() {
	return getMosesHomeDir() + MOSES_SCRIPTS_DIR_SUFFIX;
    }

    public String getMosesTrainingScriptsDir() {
	return getMosesHomeDir() + MOSES_TRAINING_SCRIPTS_DIR_SUFFIX;
    }

    public String getJoshuaClassDir() {
	return getJoshuaHomeDir() + CLASS_DIR;
    }

    public String getThraxJarLocation() {
	return getJoshuaHomeDir() + "thrax/bin/thrax.jar";
    }

    public String getBerkeleyLMJarLocation() {
	return getJoshuaHomeDir() + "lib/berkeleylm.jar";
    }

    public String getJunitJarLocation() {
	return getJoshuaHomeDir() + "lib/junit-4.10.jar";
    }

    public String getJoshuaLibDir() {
	return getJoshuaHomeDir() + LIB_DIR;
    }

    public int getNumParallelGrammarExtractors() {
	return Integer.parseInt(this.theConfig.getValueWithPresenceCheck(
		NUM_PARALLEL_GRAMMAR_EXTRACTORS_PROPERTY, "getNumParallelGrammarExtractors()"));
    }

    public int getNumParallelDecoders() {
	return Integer.parseInt(this.theConfig.getValueWithPresenceCheck(
		NUM_PARALLEL_DECODERS_PROPERTY, "getNumParallelDecoders()"));
    }

    public int getNumTunerThreads() {
	return Integer.parseInt(this.theConfig.getValueWithPresenceCheck(NUM_TUNER_THREADS_PROPERTY,
		"getNumTunerThreads()"));
    }

    public boolean restartTunerFromLastCompletedIteration() {
	return this.theConfig.getBooleanIfPresentOrReturnDefault(
		RESTART_TUNING_FROM_LAST_COMPLETED_ITERATION_FLAG, false);
    }

    public String joshuaLibraryPathOptionSpecification() {
	return "-Djava.library.path=" + theConfig.decoderConfig.getJoshuaLibDir();
    }

    private String joshuaClassPath() {
	return " -cp " + getJoshuaClassDir() + ":" + getThraxJarLocation() + ":"
		+ getBerkeleyLMJarLocation() + ":" + getJunitJarLocation();
    }

    public String baseJoshuaJavaCall() {
	return theConfig.baseJavaCall() + joshuaClassPath() + " "
		+ joshuaLibraryPathOptionSpecification();
    }

    public String joshuaDecoderCallWithConfigFileSpecification(String decoderConfigFilePath) {
	return theConfig.decoderConfig.baseJoshuaJavaCall() + " -Djava.library.path="
		+ theConfig.decoderConfig.getJoshuaLibDir() + " joshua.decoder.JoshuaDecoder "
		+ "-config " + decoderConfigFilePath;
    }

    public String getMosesBinDir() {
	return this.getMosesHomeDir() + BIN_DIR_SUFFIX;
    }

    public String baseMosesDecoderCall() {
	return getMosesBinDir() + MOSES_CHART_DECODER_FILE_NAME;
    }

    public String baseJoshuaJavaCallWithExtendedClassPath() {
	String runningProgramClassPath = System.getProperty("java.class.path");
	String extendedClassPath = joshuaClassPath() + ":" + runningProgramClassPath;
	return theConfig.baseJavaCall() + " -cp " + extendedClassPath + " "
		+ joshuaLibraryPathOptionSpecification();
    }

    public static String getDataLengthProperty(String category) {
	return category + DATA_LENGTH_SUFFIX;
    }

    public int getDataLength(String category) {
	return this.theConfig.getIntIfPresentOrReturnDefault(getDataLengthProperty(category), -1);
    }

    public int getNoMertIterations() {
	return this.theConfig.getIntIfPresentOrReturnDefault(NO_MERT_ITERATIONS_PROPERTY, 10);
    }

    public int getNBestSizeTuning() {
	if (this.theConfig.hasValue(TUNING_NBEST_SIZE_PROPERTY)) {
	    return Integer.parseInt(theConfig.getValue(TUNING_NBEST_SIZE_PROPERTY));
	}
	return TUNING_DEFAULT_NBEST_SIZE;
    }

    public int getCubePruningPopLimit() {
	String valueString = theConfig
		.getValueWithPresenceCheck(MTDecoderConfig.CUBE_PRUNING_POP_LIMIT_PROPERTY);
	return Integer.parseInt(valueString);
    }

    public int getNumTranslationOptions() {
	String valueString = theConfig.getValueWithPresenceCheck(NUM_TRANSLATION_OPTIONS_PROPERTY);
	return Integer.parseInt(valueString);
    }

    /**
     * A maximum number of alternative labeled versions per language model state
     * greater than zero can be used to perform a kind of pruning, whereby the
     * number of labeled versions per language model state is restricted. The
     * labeled versions that are retained are chosen to be the ones with the
     * highest log probability. This can be important when working with big
     * label sets (many different labels). This form of pruning will decrease
     * memory usage and decoding time, but may also improve results when working
     * with big label sets, because it will prevent the search from wasting too
     * much efforts on many alternatively labeled versions of the same
     * translations.
     * 
     * @return
     */
    public int getMaxNumberAlternativeLabeledVersionsPerLanguageModelState() {
	String valueString = theConfig.getValue(
		MTDecoderConfig.MAX_NUMBER_ALTERNATIVE_LABELED_VERSIONS_PER_LANGUAGE_MODEL_STATE);
	if (valueString != null) {
	    return Integer.parseInt(valueString);
	}
	return 0;
    }

    private String languageModelNameUnspeciffiedString() {
	return "Error: Please specify a valid language model name : \"kenlm\"(default)  or \"berkeleylm\" in the configuration file"
		+ "By adding : \"" + LANGUAGE_MODEL_NAME_PROPERTY
		+ " = LANGUAGE_MODEL_NAME\" to it";
    }

    public String getLanguageModelName() {
	if (!theConfig.hasValue(LANGUAGE_MODEL_NAME_PROPERTY)) {
	    return KENLM_NAME;
	}
	String valueString = this.theConfig.getValueWithPresenceCheck(LANGUAGE_MODEL_NAME_PROPERTY);
	if (!(valueString.equals(KENLM_NAME) || valueString.equals(BERKELEYLM_NAME))) {
	    System.out.println(LANGUAGE_MODEL_NAME_PROPERTY + " = " + valueString);
	    System.out.println(languageModelNameUnspeciffiedString());
	    System.exit(0);
	}
	return valueString;
    }

    public int getLanguageModelOrder() {
	if (!theConfig.hasValue(LANGUAGE_MODEL_ORDER_PROPERTY)) {
	    System.out.println(
		    "Error: Please specify the language model order : 1 => unigram, 2 => bigram, 3 => trigram 4 => 4-gram etc in the configuration file"
			    + "By adding : \"" + LANGUAGE_MODEL_ORDER_PROPERTY
			    + " = LANGUAGE_MODEL_ORDER\" to it");
	    System.exit(0);
	}
	String valueString = this.theConfig
		.getValueWithPresenceCheck(LANGUAGE_MODEL_ORDER_PROPERTY);
	return Integer.parseInt(valueString);
    }

    public boolean useForestMiraJoshua() {
	String valueString = theConfig.getValueWithPresenceCheck(USE_FOREST_MIRA_JOSHUA_PROPERTY);
	return Boolean.parseBoolean(valueString);
    }

    public String getTunerType() {
	String tunerType = theConfig.getValueWithPresenceCheck(MTDecoderConfig.TUNER_TYPE_PROPERTY);
	return tunerType;
    }

    public boolean tunerTypeIsMira() {
	return (getTunerType().equalsIgnoreCase(MIRA_TUNING_TYPE));
    }

    public static String getTunerTypeString(TUNER_TYPE tunerType) {

	if (tunerType == TUNER_TYPE.MERT_TUNING) {
	    return MERT_TUNING_TYPE;
	} else if (tunerType == TUNER_TYPE.PRO_TUNING) {
	    return PRO_TUNING_TYPE;
	} else if (tunerType == TUNER_TYPE.MIRA_TUNING) {
	    return MIRA_TUNING_TYPE;
	} else {
	    throw new RuntimeException("Error : unrecognized tuner type");
	}
    }

    /**
     * We want to fuzzy_matching and labelSubstitutionFeatures to always be used
     * simultaneously For something like merely counting the number of rule or
     * label types (i.e. under strict matching decoding)
     * labelSubstitutionFeatures should not be used, but rather a specific (rule
     * application / label application) feature made for that purpose. To
     * provide safety against mistakes we therefore enforce both to be used
     * simultaneously or not at all.
     * 
     */
    private void testLabelSubstitutionAndFuzzyMatchingConsistency() {
	if (useFuzzyMatchingDecoding()) {
	    if (!useLabelSubstitutionFeatures()) {
		throw new RuntimeException(
			"Error: attempting to use fuzzy matching without label substitution features."
				+ "\nThis makes no sense, as it is better then to do no labeling."
				+ "\nPlease turn on label substitution as well in the config with "
				+ "\n" + LABEL_SUBSTITUTION_FEATURE_PROPERTY + " = true");
	    }
	}
	if (useLabelSubstitutionFeatures()) {
	    if (!useFuzzyMatchingDecoding()) {

		System.out.println(
			"Error: attempting to use label substitution features without fuzzy matching."
				+ "\nThis makes no sense, as the label substitution features will not add "
				+ "any information when only strict matching of rules is allowed,"
				+ "except perhaps in a very degenerate way for which this feature should not be used ."
				+ "\nPlease turn on label fuzzy_matching as well in the config with "
				+ "\n" + FUZZY_MATCHING_PROPERTY_NAME + " = true");

	    }

	}

    }

    /**
     * We want to always used canonical form labeled rules, when we use
     * fuzzy_matching. Why: Because without that, the complexity will be come
     * really high, and the effect of fuzzy matching becomes badly defined. (And
     * it will almost certainly lead to bad results.)
     * 
     */
    private void testCanonicalFormRulesAndFuzzyMatchingConsistency() {
	if (useFuzzyMatchingDecoding()) {
	    if (!MTGrammarExtractionConfig.useCanonicalFormLabeledRules(theConfig)) {
		throw new RuntimeException(
			"Error: attempting to use fuzzy matching without canonical form labeled rules."
				+ "This leads to high decoder complexity and badly defined behavior."
				+ "You most likely don't want this, therefore we do not allow it."
				+ "Please turn on canonical form for rules as well in the config with "
				+ "\n"
				+ MTGrammarExtractionConfig.USE_CANONICAL_FORM_LABELED_RULES_PROPERTY
				+ " = true");
	    }
	}
    }

    private boolean useFuzzyMatchingDecoding() {
	if (theConfig.hasValue(FUZZY_MATCHING_PROPERTY_NAME)) {
	    return Boolean.parseBoolean(theConfig.getValue(FUZZY_MATCHING_PROPERTY_NAME));
	} else {
	    // If undefined we assume the value is false
	    return false;
	}
    }

    public static boolean useFuzzyMAtching(ConfigFile theConfig) {
	if (theConfig.hasValue(FUZZY_MATCHING_PROPERTY_NAME)) {
	    return Boolean.parseBoolean(theConfig.getValue(FUZZY_MATCHING_PROPERTY_NAME));
	}
	// If fuzzy matching is undefined we assume the value is false
	return false;

    }

    private static boolean removeLabelsInsideGrammarTrieForMoreEfficientFuzzyMatching(
	    ConfigFile theConfig) {
	if (useFuzzyMAtching(theConfig)) {
	    return theConfig.getBooleanWithPresenceCheck(
		    MTDecoderConfig.REMOVE_LABELS_INSIDE_GRAMMAR_TRIE_FOR_MORE_EFFICIENT_FUZZY_MATCHING_PROPERTY_NAME);
	}
	// If fuzzy matching is undefined we assume the value is false
	return false;
    }

    private static boolean exploreAllDistinctLabeledRuleVersionsInCubePruningInitialization(
	    ConfigFile theConfig) {
	if (useFuzzyMAtching(theConfig)) {
	    return theConfig.getBooleanWithPresenceCheck(
		    MTDecoderConfig.EXPLORE_ALL_DISTINCT_LABLED_RULE_VERSIONS_IN_CUBE_PRUNING_INITIALIZATION_PROPERTY_NAME);
	}
	// If fuzzy matching is undefined we assume the value is false
	return false;
    }

    public boolean useShufflingFeatureIsSpecified() {
	return theConfig.hasValue(
		USE_SHUFFLING_TO_RANDOMIZE_RULE_ORDER_OF_ADDING_RULES_TO_INITIAL_CUBE_PRUNING_QUEUE_PROPERTY_NAME);
    }

    private boolean useShufflingToRandomizeRuleOrderOfAddingRulesToInitialCubeCruningQueue() {
	if (useShufflingFeatureIsSpecified()) {
	    String valueString = theConfig.getValue(
		    MTDecoderConfig.USE_SHUFFLING_TO_RANDOMIZE_RULE_ORDER_OF_ADDING_RULES_TO_INITIAL_CUBE_PRUNING_QUEUE_PROPERTY_NAME);
	    return Boolean.parseBoolean(valueString);
	}
	return false;
    }

    private boolean useDotChart() {
	String valueString = theConfig.getValueWithPresenceCheck(USE_DOT_CHART_PROPERTY_STRING);
	return Boolean.parseBoolean(valueString);
    }

    public boolean useDotChartWithConsistencyCheck() {
	boolean useDotChart = useDotChart();
	if (!useDotChart) {
	    if (useFuzzyMatchingDecoding()) {

		if (exploreAllLabelsForGlueRulesInCubePruningInitialization()) {
		    throw new RuntimeException(
			    "Error: trying to use \"exploreAllLabelsForGlueRulesInCubePruningInitialization\" without dot chart, which is not supported");
		}

		if (exploreAllPossibleLabelSubstitutionsForAllRulesInCubePruningInitialization()) {
		    throw new RuntimeException(
			    "Error: trying to use \"exploreAllPossibleLabelSubstitutionsForAllRulesInCubePruningInitialization\" without dot chart, which is not supported");
		}

		if (useSeparateCubePruningStatesForMatchingSubstitutions()) {
		    throw new RuntimeException(
			    "Error: trying to use \"useSeparateCubePruningStatesForMatchingSubstitutions\" without dot chart, which is not supported");
		}
	    }
	}
	return useDotChart;
    }

    public String useDotChartStringString() {

	if (!useDotChartWithConsistencyCheck()) {
	    return USE_DOT_CHART_FLAG_NAME_JOSHUA + " = true";
	}
	return "";
    }

    public boolean useFuzzyMatchingDecodingWithConsistencyCheck() {
	testLabelSubstitutionAndFuzzyMatchingConsistency();
	testCanonicalFormRulesAndFuzzyMatchingConsistency();
	return useFuzzyMatchingDecoding();
    }

    public boolean useSeparateCubePruningStatesForMatchingSubstitutions() {
	return theConfig.getBooleanWithPresenceCheck(
		MTDecoderConfig.SEPARATE_CUBE_PRUNING_STATES_MATCHING_SUBSTITUTIONS_PROPERTY_NAME);
    }

    public boolean exploreAllLabelsForGlueRulesInCubePruningInitialization() {
	return theConfig.getBooleanWithPresenceCheck(
		MTDecoderConfig.EXPLORE_ALL_LABELS_FOR_GLUE_RULES_IN_CUBE_PRUNING_INITIALIZATION_PROPERTY_NAME);
    }

    private boolean exploreAllPossibleLabelSubstitutionsForAllRulesInCubePruningInitialization() {
	return theConfig.getBooleanWithPresenceCheck(
		MTDecoderConfig.EXPLORE_ALL_POSSIBLE_LABEL_SUBSTITUTIONS_FOR_ALL_RULES_IN_CUBE_PRUNING_INITIALIZATION_PROPERTY_NAME);
    }

    public boolean exploreAllPossibleLabelSubstitutionsForAllRulesInCubePruningInitializationWithConsistencyCheck() {
	boolean exploreAllPossibleLabelSubstitutionsForAllRulesInCubePruningInitialization = theConfig
		.getBooleanWithPresenceCheck(
			MTDecoderConfig.EXPLORE_ALL_POSSIBLE_LABEL_SUBSTITUTIONS_FOR_ALL_RULES_IN_CUBE_PRUNING_INITIALIZATION_PROPERTY_NAME);
	if (exploreAllPossibleLabelSubstitutionsForAllRulesInCubePruningInitialization) {
	    String errorString = "Error: using "
		    + MTDecoderConfig.EXPLORE_ALL_POSSIBLE_LABEL_SUBSTITUTIONS_FOR_ALL_RULES_IN_CUBE_PRUNING_INITIALIZATION_PROPERTY_NAME
		    + " = true";
	    if (exploreAllLabelsForGlueRulesInCubePruningInitialization()) {
		errorString += "\n this requires that: "
			+ EXPLORE_ALL_LABELS_FOR_GLUE_RULES_IN_CUBE_PRUNING_INITIALIZATION_PROPERTY_NAME
			+ " = false";
		throw new RuntimeException(errorString);
	    }
	    if (useSeparateCubePruningStatesForMatchingSubstitutions()) {
		errorString += "\n this requires that: "
			+ SEPARATE_CUBE_PRUNING_STATES_MATCHING_SUBSTITUTIONS_PROPERTY_NAME
			+ " = false";
		throw new RuntimeException(errorString);
	    }
	}
	return exploreAllPossibleLabelSubstitutionsForAllRulesInCubePruningInitialization;

    }

    private boolean useLabelSubstitutionFeatures() {
	if (theConfig.hasValue(LABEL_SUBSTITUTION_FEATURE_PROPERTY)) {
	    return Boolean.parseBoolean(theConfig.getValue(LABEL_SUBSTITUTION_FEATURE_PROPERTY));
	} else {
	    // If undefined we assume the value is false
	    return false;
	}
    }

    public boolean useLabelSplittingSmoothingForLabelSubstitutionFeatures() {
	return Boolean.parseBoolean(theConfig.getValueWithPresenceCheck(
		USE_LABEL_SPLITTING_SMOOTHING_PROPERTY_FOR_LABEL_SUBSTITUTION_FEATURES));

    }

    public boolean useLabelSplittingSmoothingWithOnlyLabelSubstitutionFeaturesForSplittedLabels() {
	return Boolean.parseBoolean(theConfig.getValueWithPresenceCheck(
		USE_LABEL_SPLITTING_SMOOTHING_WITH_ONLY_LABEL_SUBSTITUTION_FEATURES_FOR_SPLITTED_LABELS_PROPERTY));
    }

    public void testConsistencyLabelSplittingSmoothingForLabelSubstitutionFeatures(
	    SystemIdentity systemIdentity) {

	String value = theConfig
		.getValue(USE_LABEL_SPLITTING_SMOOTHING_PROPERTY_FOR_LABEL_SUBSTITUTION_FEATURES);
	boolean useLabelSplittingSmoothingForLabelSubstitutionFeatures = false;
	if (value != null) {
	    useLabelSplittingSmoothingForLabelSubstitutionFeatures = Boolean.parseBoolean(value);
	}

	boolean useLabelSplittingSmoothingOnlySingleLabelFeatures = false;
	value = theConfig.getValue(
		USE_LABEL_SPLITTING_SMOOTHING_WITH_ONLY_LABEL_SUBSTITUTION_FEATURES_FOR_SPLITTED_LABELS_PROPERTY);
	if (value != null) {
	    useLabelSplittingSmoothingOnlySingleLabelFeatures = Boolean.parseBoolean(value);
	}

	if (!systemIdentity.producesDoubleReorderingLabels()) {
	    if (useLabelSplittingSmoothingForLabelSubstitutionFeatures
		    || useLabelSplittingSmoothingOnlySingleLabelFeatures) {
		throw new RuntimeException(
			"Error inconsistent configuration: trying to use label splitting smoothing for label substitution features for a system without double labels");
	    }
	}

	if (useLabelSplittingSmoothingForLabelSubstitutionFeatures
		&& useLabelSplittingSmoothingOnlySingleLabelFeatures) {
	    throw new RuntimeException("Error: trying to use both "
		    + USE_LABEL_SPLITTING_SMOOTHING_PROPERTY_FOR_LABEL_SUBSTITUTION_FEATURES
		    + " and "
		    + USE_LABEL_SPLITTING_SMOOTHING_WITH_ONLY_LABEL_SUBSTITUTION_FEATURES_FOR_SPLITTED_LABELS_PROPERTY
		    + " , these are mutually exclusive ");
	}

    }

    public boolean useLabelSubstitutionFeaturesWithConsistencyCheck() {
	testLabelSubstitutionAndFuzzyMatchingConsistency();
	return useLabelSubstitutionFeatures();
    }

    /**
     * The Rule Application Feature can be used to mark all rules that are used
     * to produce a translation. This is mainly useful for debugging, to better
     * understand how a certain (unexpected) translation was created.
     * 
     * @return
     */
    public boolean useRuleApplicationFeature() {
	String featureString = null;
	if (theConfig.hasValue(RULE_APPLICATION_FEATURE_PROPERTY)) {
	    featureString = theConfig.getValue(RULE_APPLICATION_FEATURE_PROPERTY);
	}
	return Boolean.parseBoolean(featureString);
    }

    public boolean useSparseLabelSubstitutionFeatures() {
	return Boolean.parseBoolean(
		theConfig.getValueWithPresenceCheck(SPARSE_LABEL_SUBSTITUTION_FEATURE_PROPERTY));
    }

    public String getJoshuaFuzzyMatchingDecodingString() {

	return FUZZY_MATCHING_PROPERTY_NAME + " = "
		+ useFuzzyMatchingDecodingWithConsistencyCheck();
    }

    public String getRemoveLabelsInsideGrammarTrieForMoreEfficientFuzzyMatchingString() {

	return REMOVE_LABELS_INSIDE_GRAMMAR_TRIE_FOR_MORE_EFFICIENT_FUZZY_MATCHING_PROPERTY_NAME
		+ " = " + removeLabelsInsideGrammarTrieForMoreEfficientFuzzyMatching(theConfig);
    }

    public String getExploreAllDistinctLabeledRuleVersionsInCubePruningInitializationString() {

	return EXPLORE_ALL_DISTINCT_LABLED_RULE_VERSIONS_IN_CUBE_PRUNING_INITIALIZATION_PROPERTY_NAME
		+ " = "
		+ exploreAllDistinctLabeledRuleVersionsInCubePruningInitialization(theConfig);
    }

    public String getUseShufflingString() {
	if (useShufflingFeatureIsSpecified()) {
	    return USE_SHUFFLING_TO_RANDOMIZE_RULE_ORDER_OF_ADDING_RULES_TO_INITIAL_CUBE_PRUNING_QUEUE_PROPERTY_NAME
		    + " = "
		    + useShufflingToRandomizeRuleOrderOfAddingRulesToInitialCubeCruningQueue();
	}
	return "";
    }

    public String getJoshuaSeparateCubePruningStatesForMatchingSubstitutionsDecodingString() {

	return SEPARATE_CUBE_PRUNING_STATES_MATCHING_SUBSTITUTIONS_PROPERTY_NAME + " = "
		+ useSeparateCubePruningStatesForMatchingSubstitutions();
    }

    public String getExploreAllLabelsForGlueRulesInCubePruningInitialization() {
	return EXPLORE_ALL_LABELS_FOR_GLUE_RULES_IN_CUBE_PRUNING_INITIALIZATION_PROPERTY_NAME
		+ " = " + exploreAllLabelsForGlueRulesInCubePruningInitialization();
    }

    public String getExploreAllPossibleLabelSubstitutionsForAllRulesInCubePruningInitialization() {
	return EXPLORE_ALL_POSSIBLE_LABEL_SUBSTITUTIONS_FOR_ALL_RULES_IN_CUBE_PRUNING_INITIALIZATION_PROPERTY_NAME
		+ " = "
		+ exploreAllPossibleLabelSubstitutionsForAllRulesInCubePruningInitializationWithConsistencyCheck();
    }

    public boolean logDecodingComputationTimes() {
	return Boolean.parseBoolean(
		this.theConfig.getValueWithPresenceCheck(LOG_CPU_COMPUTATION_TIMES_PROPERTY));
    }

    public String getLogDecodingComputationTimesString() {
	return LOG_CPU_COMPUTATION_TIMES_PROPERTY + " = " + logDecodingComputationTimes();
    }
    
    public String getCpuComputationTimesLogFilePath(){
	return this.theConfig.filesConfig.getDecoderOutputFilePathTestSetTuned() + CPU_COMPUTATION_LOG_FILE_SUFFFIX;
    }
    
    public String getCpuComputationTimesLogFilePathString() {
   	return CPU_COMPUTATION_TIMES_LOG_FILE_PATH_PROPERTY + " = " + getCpuComputationTimesLogFilePath();
       }


    public String getJoshuaLabelSubsitutionFeatureString() {

	String result = "feature-function = ";
	if (useSparseLabelSubstitutionFeatures()) {
	    result += SPARSE_LABEL_SUBSTITUTION_FEATURE_NAME_JOSHUA;
	} else {
	    result += LABEL_SUBSTITUTION_FEATURE_NAME_JOSHUA;
	}
	return result;
    }

    public String getJoshuaRuleApplicationFeatureString() {
	String result = "feature-function = " + RULE_APPLICATION_FEATURE_NAME_JOSHUA;
	return result;
    }

    public String getJoshuaDoubleLabelSmoothedLabelSubsitutionFeatureString() {

	String result = "feature-function = ";
	if (useSparseLabelSubstitutionFeatures()) {
	    result += SPARSE_LABEL_SUBSTITUTION_FEATURE_NAME_JOSHUA + " "
		    + DOUBLE_LABEL_WITH_LABEL_SPLITTING_SMOOTHING_ARGUMENT;
	} else {
	    result += LABEL_SUBSTITUTION_FEATURE_NAME_JOSHUA + " "
		    + DOUBLE_LABEL_WITH_LABEL_SPLITTING_SMOOTHING_ARGUMENT;
	}

	return result;
    }

    public String getJoshuaDoubleLabelSmoothedOnlyLabelSubsitutionFeaturesForSingleLabelsString() {

	String result = "feature-function = ";
	if (useSparseLabelSubstitutionFeatures()) {
	    result += SPARSE_LABEL_SUBSTITUTION_FEATURE_NAME_JOSHUA + " "
		    + DOUBLE_LABEL_WITH_LABEL_SPLITTING_SMOOTHING_ONLY_SINGLE_LABEL_FEATURES_ARGUMENT;
	} else {
	    result += LABEL_SUBSTITUTION_FEATURE_NAME_JOSHUA + " "
		    + DOUBLE_LABEL_WITH_LABEL_SPLITTING_SMOOTHING_ONLY_SINGLE_LABEL_FEATURES_ARGUMENT;
	}

	return result;
    }

    public double getDiscriminativeFeatureInitialWeight() {
	return Double.parseDouble(this.theConfig
		.getValueWithPresenceCheck(DISCRIMINATIVE_FEATURE_INITIAL_WEIGHT_PROPERTY));
    }

    public boolean useProTuning() {
	String tunerType = this.theConfig.getValueWithPresenceCheck(TUNER_TYPE_PROPERTY);
	return (tunerType.equals(PRO_TUNING_TYPE));
    }

    public boolean useMertTuning() {
	String tunerType = this.theConfig.getValueWithPresenceCheck(TUNER_TYPE_PROPERTY);
	return (tunerType.equals(MERT_TUNING_TYPE));
    }

    public String getNBestFormat() {
	String result = theConfig.getStringIfPresentOrReturnDefault(NBEST_FORMAT_PROPERTY_STRING,
		DENSE_NBEST_FORMAT_STRING);
	System.out.println("nBestFormat: " + result);

	if (!(result.equals(DENSE_NBEST_FORMAT_STRING)
		|| result.equals(SPARSE_NBEST_FORMAT_STRING))) {
	    throw new RuntimeException("Error : getNBestFormat - illegal value, must be"
		    + DENSE_NBEST_FORMAT_STRING + " or " + SPARSE_NBEST_FORMAT_STRING);
	}
	return result;
    }

    private Integer getIntegerFromStringAndReturnNullWhenNotParsable(String valueString) {
	try {
	    return Integer.parseInt(valueString);
	} catch (NumberFormatException e) {
	    return null;
	}
    }

    private Double getDoubleFromStringAndReturnNullWhenNotParsable(String valueString) {
	try {
	    return Double.parseDouble(valueString);
	} catch (NumberFormatException e) {
	    return null;
	}
    }

    public int getTuningStrategyMode() {
	String valueString = theConfig.getValueWithPresenceCheck(TUNING_STRATEGY_MODE_PROPERTY,
		" getTuningStrategyMode");
	Integer propertyValue = getIntegerFromStringAndReturnNullWhenNotParsable(valueString);

	if ((propertyValue != null) && (propertyValue > 0) && (propertyValue < 5)) {
	    return propertyValue;
	} else {
	    throw new RuntimeException(
		    "Error: please specify an integer from 1 to 4 for the property "
			    + TUNING_STRATEGY_MODE_PROPERTY);
	}
    }

    public double getTuningInterpolationCoefficient() {
	String valueString = theConfig.getValueWithPresenceCheck(
		TUNING_INTERPOLATION_COEFFICIENT_PROPERTY, "getTuningInterpolationCoefficient");
	Double propertyValue = getDoubleFromStringAndReturnNullWhenNotParsable(valueString);

	if ((propertyValue != null) && (propertyValue >= 0) && (propertyValue <= 1)) {
	    return propertyValue;
	} else {
	    throw new RuntimeException(
		    "Error: please specify a value V sucht that 0 <= V <= 1 for the property "
			    + TUNING_STRATEGY_MODE_PROPERTY);
	}

    }

    public String getMiraRegularizationPropertyValue() {

	String valueString = this.theConfig.getValue(MIRA_REGULARIZATION_PARAMETER_PROPERTY);
	if (valueString != null) {
	    try {
		double value = Double.parseDouble(valueString);
		if ((value < 0) || (value > 1)) {
		    throw new RuntimeException();
		}
		return valueString;
	    } catch (Exception e) {
		String errorString = "Error:  getMiraRegularizationPropertyValue() please use a proper double value for parameter \""
			+ MIRA_REGULARIZATION_PARAMETER_PROPERTY + "\"  between 0 and 1 ";
		System.out.println(errorString);
		throw new RuntimeException(errorString);
	    }
	}
	return null;
    }

    private int getNBestSizeForNBestMBR() {
	String sizeString = theConfig.getValueWithPresenceCheck(NBEST_SIZE_FOR_NBEST_MBR_PROPERTY);
	int result = Integer.parseInt(sizeString);
	return result;
    }

    public int getMaxNumUniqueTranslationsForNBestMBR() {
	String sizeString = theConfig
		.getValueWithPresenceCheck(MAX_NUM_UNIQUE_TRANSLATIONS_FOR_NBEST_MBR_PROPERTY);
	int result = Integer.parseInt(sizeString);
	return result;
    }

    public boolean sumDerivationsWithSameYieldForNBestMBR() {
	return theConfig.getBooleanWithPresenceCheck(
		SUM_DERIVATIONS_WITH_SAME_YIELD_FOR_NBEST_MBR_PARAMETER);
    }

    public String getMosesNBestForMBRSpecificationParameterString(String nBestOutputFilePath) {
	RerankingType rerankingType = RerankingType.createRerankingTypeFromConfig(theConfig);

	if (rerankingType.useDecoderOutputReranking()) {
	    return " " + rerankingType.nBestForMBRSpecificationParameterString(nBestOutputFilePath,
		    getNBestSizeForNBestMBR());
	}
	return "";
    }

    public boolean useDecoderOutputReranking() {
	RerankingType rerankingType = RerankingType.createRerankingTypeFromConfig(theConfig);
	return rerankingType.useDecoderOutputReranking();
    }

    public boolean useCrunching() {
	RerankingType rerankingType = RerankingType.createRerankingTypeFromConfig(theConfig);
	return rerankingType.useCrunching();
    }

    public boolean useNBestListMBR() {
	RerankingType rerankingType = RerankingType.createRerankingTypeFromConfig(theConfig);
	return rerankingType.useNBestListMBR();
    }

    private static abstract class RerankingType {
	private static final List<String> ACCEPTED_MBR_PROPERTY_VALUES = Arrays.asList(NO_RERANKING,
		MINIMUM_BAYES_RISK, CRUNCHING_RERANKING);

	protected boolean useDecoderOutputReranking() {
	    return useCrunching() || useNBestListMBR();
	}

	protected abstract boolean useCrunching();

	protected abstract boolean useNBestListMBR();

	protected abstract String nBestForMBRSpecificationParameterString(
		String nBestOutputFilePath, int nBestListSizeForMBRInput);

	protected static String getMBRPRopertyValue(MTConfigFile theConfig) {
	    return theConfig.getValueWithPresenceCheck(RERANKING_PROPERTY);
	}

	public static RerankingType createRerankingTypeFromConfig(MTConfigFile theConfig) {
	    String mbrPropertyValue = getMBRPRopertyValue(theConfig);

	    if (mbrPropertyValue.equals(NO_RERANKING)) {
		return new NoRerankingType();
	    } else if (mbrPropertyValue.equals(MINIMUM_BAYES_RISK)) {
		return new BasicMBRType();
	    } else if (mbrPropertyValue.equals(CRUNCHING_RERANKING)) {
		return new CrunchingRerankingTypeType();
	    } else {
		String errorString = "Error : Please make sure to choose a value from : \n"
			+ Utility.stringListString(ACCEPTED_MBR_PROPERTY_VALUES)
			+ "\n for the property " + RERANKING_PROPERTY
			+ " in the configuration file";
		throw new RuntimeException(errorString);
	    }

	}

    }

    private static class NoRerankingType extends RerankingType {
	@Override
	protected String nBestForMBRSpecificationParameterString(String nBestOutputFilePath,
		int nBestListSizeForMBRInput) {
	    throw new RuntimeException("Not Implemented");
	}

	@Override
	protected boolean useCrunching() {
	    return false;
	}

	@Override
	protected boolean useNBestListMBR() {
	    return false;
	}

    }

    private static class BasicMBRType extends RerankingType {

	@Override
	protected String nBestForMBRSpecificationParameterString(String nBestOutputFilePath,
		int nBestListSizeForMBRInput) {

	    return MOSES_SHOW_WEIGTHS_FLAG + " " + nBestOutputFilePath + " "
		    + nBestListSizeForMBRInput;
	}

	@Override
	protected boolean useCrunching() {
	    return false;
	}

	@Override
	protected boolean useNBestListMBR() {
	    return true;
	}
    }

    private static class CrunchingRerankingTypeType extends RerankingType {
	@Override
	protected String nBestForMBRSpecificationParameterString(String nBestOutputFilePath,
		int nBestListSizeForMBRInput) {
	    return MOSES_SHOW_WEIGTHS_FLAG + " " + nBestOutputFilePath + " "
		    + nBestListSizeForMBRInput;
	}

	@Override
	protected boolean useCrunching() {
	    return true;
	}

	@Override
	protected boolean useNBestListMBR() {
	    return false;
	}
    }

    public String getJoshuaMiraProgramPath() {
	return getJoshuaHomeDir() + JOSHUA_MIRA_INTERFACE_PROGRAM_RELATIVE_PATH;
    }

    public String tuneNonBasicFeaturesSeperately() {
	return theConfig.getValueWithPresenceCheck(TUNE_NON_BASIC_FEATURES_SEPERATELY_PROPERTY);
    }

}
