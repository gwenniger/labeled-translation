package mt_pipeline.mt_config;

import mt_pipeline.DecoderCommandCreater;
import mt_pipeline.MosesDecoderCommandCreater;
import mt_pipeline.tuning.MosesMertCommandCreater;
import mt_pipeline.tuning.MosesMiraCommandCreater;
import mt_pipeline.tuning.Tuner;
import mt_pipeline.tuning.TuningCommandCreater;
import grammarExtraction.chiangGrammarExtraction.HieroGlueGrammarCreater;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatter;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatterMoses;
import grammarExtraction.translationRules.MosesRuleRepresentationCreater;
import grammarExtraction.translationRules.TranslationRuleRepresentationCreater;
import util.ConfigFile;

public class MosesSupportObjectsFactory extends SystemSpecificSupportObjectsFactory {

	@Override
	public TranslationRuleRepresentationCreater createTranslationRuleRepresentationCreater(ConfigFile theConfig, String systemName) {
		return MosesRuleRepresentationCreater.createMosesRuleRepresentationCreaterFactory();
	}

	@Override
	public SystemSpecificConfig createSystemSpecificConfiguration(MTConfigFile theConfig) {
		return new MosesSpecificConfig(theConfig);
	}

	@Override
	public DecoderCommandCreater createDecoderCommandCreaterFromConfig(MTConfigFile theConfig) {
		return MosesDecoderCommandCreater.createMosesDecoderCommandCreater(theConfig);
	}

	@Override
	public FeatureValueFormatter createFeatureValueFormatter(ConfigFile theConfig) {
		return new FeatureValueFormatterMoses();
	}

	@Override
	public Tuner createTunerFromConfig(MTConfigFile theConfig) {
		String tunerType = theConfig.decoderConfig.getTunerType();
		System.out.println("tunerType: " + tunerType);

		TuningCommandCreater tuningCommandCreater = null;	
		if (tunerType.equals(MTDecoderConfig.MERT_TUNING_TYPE)) {
		    	tuningCommandCreater = MosesMertCommandCreater.createMosesMertCommandCreater(theConfig);
		} else if (tunerType.equals(MTDecoderConfig.MIRA_TUNING_TYPE)) {
		    	tuningCommandCreater = MosesMiraCommandCreater.createMosesMiraCommandCreater(theConfig);
		} else if (tunerType.equals(MTDecoderConfig.PRO_TUNING_TYPE)) {
		    	throw new RuntimeException("Error : unrecognized tuning type");
		} else {
			throw new RuntimeException("Error : unrecognized tuning type");
		}
		return Tuner.createBasicTuner(theConfig, tuningCommandCreater);
	}

	@Override
	public String getUnknownWordsLabel(ConfigFile theConfig, String systemName) {
		return HieroGlueGrammarCreater.ChiangLabel;
	}
}
