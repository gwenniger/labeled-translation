package mt_pipeline.mt_config;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

public abstract class MTMetaDataCreatorConfig {
    protected MTConfigFile theConfig;

    protected MTMetaDataCreatorConfig(MTConfigFile theConfig) {
	this.theConfig = theConfig;
    }

    private boolean isTestOrDevCategory(String category) {
	return (category.equals(MTConfigFile.TEST_CATEGORY) || category
		.equals(MTConfigFile.DEV_CATEGORY));
    }

    public String getEnrichmentProducerSourceInputFilePath(String category) {
	// The test and dev category use the pre-processed (i.e. tokeninzed)
	// file from the generated data folder
	if (isTestOrDevCategory(category)) {
	    return this.theConfig.filesConfig.getSourceTokenizedFileName(category);
	}
	// The train category uses the cased train input files directly
	Assert.assertEquals(category, MTConfigFile.TRAIN_CATEGORY);
	return this.theConfig.filesConfig.getSourceCasedTrainFilePath();
    }

    public String getEnrichmentProducerTargetInputFilePath(String category) {
	// The test and dev category use the pre-processed (i.e. tokeninzed)
	// file from the generated data folder
	if (isTestOrDevCategory(category)) {
	    return this.theConfig.filesConfig.getTargetTokenizedFileNames(category).get(0);
	}

	// The train category uses the cased train input files directly
	Assert.assertEquals(category, MTConfigFile.TRAIN_CATEGORY);
	return this.theConfig.filesConfig.getTargetCasedTrainFilePath();
    }

    public String getEnrichmentPairerSourceInputFilePath(String category) {
	// The test and dev category use the pre-processed (i.e. tokeninzed)
	// file from the generated data folder
	if (isTestOrDevCategory(category)) {
	    return this.theConfig.filesConfig.getSourceTokenizedAndLowerCasedFileName(category);
	}
	// The train category uses the cased train input files directly
	Assert.assertEquals(category, MTConfigFile.TRAIN_CATEGORY);
	return this.theConfig.filesConfig.getSourceInputFilePath(category);
    }

    public String getSourceInputFilePreprocessedPath(String category) {
	return this.theConfig.filesConfig.getSourceInputFilePath(category)
		+ preprocessedFileSuffix();
    }

    public String getTargetInputFilePreprocessedPathTrain() {
	String targetFilePath = this.theConfig.filesConfig.getTargetInputFilePaths(
		MTConfigFile.TRAIN_CATEGORY).get(0);
	return targetFilePath + preprocessedFileSuffix();
    }

    public List<String> getTargetInputFilePreprocessedPaths(String category) {
	List<String> result = new ArrayList<String>();
	for (String targetFilePath : this.theConfig.filesConfig.getTargetInputFilePaths(category)) {
	    result.add(targetFilePath + preprocessedFileSuffix());
	}
	return result;
    }

    protected abstract String preprocessedFileSuffix();

}
