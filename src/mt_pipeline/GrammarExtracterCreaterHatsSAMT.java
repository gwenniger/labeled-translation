package mt_pipeline;

import mt_pipeline.mt_config.MTConfigFile;
import grammarExtraction.chiangGrammarExtraction.MultiThreadGrammarExtractorTestFiltered;
import grammarExtraction.grammarExtractionFoundation.MultiThreadGrammarExtractor;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;

public class GrammarExtracterCreaterHatsSAMT extends GrammarExtracterCreater {

	private GrammarExtracterCreaterHatsSAMT(boolean writePlainHieroRules,SystemIdentity systemIdentity) {
		super(writePlainHieroRules,systemIdentity);
	}

	public static GrammarExtracterCreaterHatsSAMT createGrammarExtracterCreaterHatsSAMT(MTConfigFile theConfig,SystemIdentity systemIdentity) {
		return new GrammarExtracterCreaterHatsSAMT(systemIdentity.writePlainHieroRulesForSmoothing(),systemIdentity);
	}

	@Override
	public MultiThreadGrammarExtractor createGrammarExtracterTestFiltered(String configFilePath, int numThreads) {
		return MultiThreadGrammarExtractorTestFiltered.createMultiThreadGrammarExtractorTestFilteredSAMT(configFilePath, numThreads,systemIdentity);
	}

	@Override
	public String getSystemName() {
		return MTPipelineHATsSAMT.SystemName;
	}

}
