package mt_pipeline;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import util.FileUtil;
import util.TextFileIterator;
import bitg.Pair;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import mt_pipeline.mt_config.MTConfigFile;

public class MosesDecoderConfigCreater extends DecoderConfigCreater {

	private static final int SEARCH_ALGORITHM_TYPE_CODE = 3; // 3;
	private static final int INPUT_TYPE_CODE = 3;
	// The maximum Chart span length
	// In the limit we have to increase this even further, but this should be enough for all "normal" sentences
	private static final int MAX_CHART_SPAN = 300;
	// A value of 0 means there is no limit
	private static final int TRANSLATION_TABLE_LOADING_LIMIT = 20; // (7-5-2013) : Changed back from 90 to 20 to facilitate SAMT which becomes very slow with 90
	private static final String PHRASE_DICTIONARY_MEMORY_STRING = "PhraseDictionaryMemory";
	protected static final List<String> MAIN_GRAMMAR_PARAMETER_SIGNATURE_STRINGS_LIST = Arrays.asList("PhraseDictionaryMemory", "path=");
	private static final String PHRASE_DICTIONARY_MEMORY_FACTOR_SPECIFICATION_STRING = "input-factor=0 output-factor=0";
	private static final String FEATURE_HEADER = "[feature]";
	private static final String WORD_PENALTY_FEATURE_NAME = "WordPenalty";
	private static final String UNKNOWN_WORD_PENALTY_FEATURE_NAME = "UnknownWordPenalty";
	private static final String WEIGHTS_HEADER = "[weight]";

	public MosesDecoderConfigCreater(MTConfigFile theConfig, SystemIdentity systemIdentity) {
		super(theConfig, systemIdentity);
	}

	public static MosesDecoderConfigCreater createMosesDecoderConfigCreater(MTConfigFile theConfig) {
		SystemIdentity systemIdentity = SystemIdentity.createSystemIdentity(theConfig);
		return new MosesDecoderConfigCreater(theConfig, systemIdentity);
	}

	private static String getTestMainGrammarName(MTConfigFile theConfig) {

		return theConfig.filesConfig.getFilteredGrammarPathTest();

	}

	@Override
	public void createTunedTestConfig(MTConfigFile theConfig) {
		copyJoshuaConfigFileReplaceGrammarFileParameter(theConfig, theConfig.filesConfig.getMosesDevTunedConfigFileName(), theConfig.filesConfig.getDecoderTestTunedConfigFileName(),
				getTestMainGrammarName(theConfig));
	}

	private static boolean lineConstitutesMainGrammarParameterSpecification(String line) {
		for (String signatureWord : MAIN_GRAMMAR_PARAMETER_SIGNATURE_STRINGS_LIST) {
			if (!line.contains(signatureWord)) {
				return false;
			}
		}
		return true;
	}

	public void copyJoshuaConfigFileReplaceGrammarFileParameter(MTConfigFile theConfig, String originalConfigFilePath, String outputFilePath, String newMainGrammarFilePath) {
		try {
			System.out.println("originalConfigFilePath: " + originalConfigFilePath);

			File file = new File(originalConfigFilePath);
			// FileWaiter.waitWhileFileDoesNotExistsSimple(originalConfigFilePath);
			// FileWaiter.waitUntilFileExists(originalConfigFilePath);
			Assert.assertTrue(file.exists());

			TextFileIterator textFileIterator = new TextFileIterator(originalConfigFilePath);
			BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFilePath));

			String line = "";

			while (textFileIterator.hasNext()) {
				line = textFileIterator.next();

				if (lineConstitutesMainGrammarParameterSpecification(line)) {
					String newGrammarParameterSpecification = getTranslationTablesSpecification(newMainGrammarFilePath, theConfig.filesConfig.getGlueGrammarPathTest());
					outputWriter.write(newGrammarParameterSpecification + NL);

				} else {
					// simply copy the old line
					outputWriter.write(line + NL);
				}
			}
			outputWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void writeMertConfigFile(MTConfigFile theConfig) {
		String outFileName = theConfig.systemSpecificConfig.getDecoderMertConfigFileName();
		BufferedWriter outputWriter = null;
		try {
			outputWriter = new BufferedWriter(new FileWriter(outFileName));
			outputWriter.write(getMosesMertConfigFile());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			FileUtil.closeCloseableIfNotNull(outputWriter);
		}
	}

	@Override
	public void writeTestConfigFile(MTConfigFile theConfig) {
		String outFileName = theConfig.systemSpecificConfig.getDecoderTestConfigFileName();
		BufferedWriter outputWriter = null;
		try {
			outputWriter = new BufferedWriter(new FileWriter(outFileName));
			outputWriter.write(getMosesTestConfigFile());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			FileUtil.closeCloseableIfNotNull(outputWriter);
		}

	}

	private int getNumberOfScores() {
		return getAllOrderedFeatureNamesWithWeights().size();
	}

	private static String tuneableString(boolean isTuneable) {
		return "tuneable=" + isTuneable;
	}

	private String getTranslationTablesSpecification(String mainGrammarPath, String glueGrammarPath) {
		String result = "";
		result += PHRASE_DICTIONARY_MEMORY_STRING + " " + tuneableString(true) + " " + PHRASE_DICTIONARY_MEMORY_FACTOR_SPECIFICATION_STRING + " num-features=" + getNumberOfScores() + " " + "path="
				+ mainGrammarPath + " " + getTranslationTableLimitString();
		// glueGrammarPath;
		return result;
	}

	private String getTestTranslationTablesSpecification() {
		return NL + getTranslationTablesSpecification(theConfig.filesConfig.getFilteredGrammarPathTest(), theConfig.filesConfig.getGlueGrammarPathTest());
	}

	private String getMertTranslationTablesSpecification() {
		return NL + getTranslationTablesSpecification(theConfig.filesConfig.getFilteredGrammarPathMert(), theConfig.filesConfig.getGlueGrammarPathMert());
	}

	private String getLanguageModelSpecification() {
		String result = "";
		result += NL + "KENLM name=LM factor=0 order=3 num-features=1 path=" + theConfig.filesConfig.getBinaryLanguageModelFilePath();
		return result;
	}

	private String getInputFactorsSpecification() {
		String result = "";
		result += NL + "# input factors";
		result += NL + "[input-factors]";
		result += NL + "0" + NL;
		return result;
	}

	private String getMappingStepsSpecification() {
		String result = "";
		result += NL + "# mapping steps";
		result += NL + "[mapping]";
		result += NL + "0 T 0";
		// result += NL + "1 T 1";
		result += NL;
		return result;
	}

	private String getTranslationTableLimitString() {
		String result = "table-limit=" + TRANSLATION_TABLE_LOADING_LIMIT + NL;
		return result;
	}

	private String getParametersWithFeatureSpecificWeights() {
		String result = NL + "# translation model weights";

		result += NL + "PhraseDictionaryMemory0= ";
		for (Pair<String, Double> featureNameWeightPair : getAllOrderedFeatureNamesWithWeights()) {
			result += " " + featureNameWeightPair.last;
		}
		return result;
	}

	private String getWordPenaltyWeight() {
		String result = NL + "#word penalty" + NL + WORD_PENALTY_FEATURE_NAME + "0=" + " " + 0;
		return result;
	}

	private String getModelWeights() {
		String result = NL + WEIGHTS_HEADER + getWordPenaltyWeight();
		result += NL + "# language model weights" + NL + "LM= " + 0.5;
		result += getParametersWithFeatureSpecificWeights() + NL;
		result += NL;
		return result;
	}

	/*
	 * private String getParametersWithFeatureSpecificWeights() { String result = NL + "# translation model weights" + NL + "PhraseDictionaryMemory0= ";
	 * 
	 * for (Pair<String, Double> featureNameWeightPair : getAllOrderedFeatureNamesWithWeights()) {
	 * 
	 * result += " " + featureNameWeightPair.last; } return result; }
	 * 
	 * private String getWordPenaltyWeight() { String result = NL + "#word penalty" + NL + "WordPenalty0=" + " " + 0.0 + NL; return result; }
	 * 
	 * private String getModelWeights() { String result = NL + "# language model weights" + NL + "[weight]" + NL + "KENLM0=" + " " + 0.5 + NL; result +=
	 * getParametersWithFeatureSpecificWeights() + NL; result += getWordPenaltyWeight(); result += NL; return result; }
	 */
	private String getPruningConfig() {
		String result = "";
		result += NL + "[cube-pruning-pop-limit]";
		result += NL + this.theConfig.decoderConfig.getCubePruningPopLimit();
		result += NL;
		return result;
	}

	private String getNonTerminalsSpecification() {
		String result = "";
		result += NL + "[non-terminals]" + NL + "X" + NL + "S";
		result += NL + "X-MONO" + NL + "X-ATOMIC" + NL + "X-BITT" + NL + "X-PET" + NL + "X-HAT" + NL;
		result += NL;
		return result;
	}

	private String getSearhAlgorithmSpecification() {
		String result = "";
		result += NL + "[search-algorithm]" + NL + SEARCH_ALGORITHM_TYPE_CODE;
		result += NL;
		return result;
	}

	private String getInputTypeSpecification() {
		String result = "";
		result += NL + "[inputtype]" + NL + INPUT_TYPE_CODE;
		result += NL;
		return result;
	}

	private String getMaxChartSpanSpecification() {
		String result = "";
		result += NL + "[max-chart-span]" + NL + MAX_CHART_SPAN + NL + 1000;
		result += NL;
		return result;
	}

	private String getTranslationDetailsLogSpecification() {
		String result = "";
		result += NL + "[translation-details]" + NL + "translation-details.log";
		result += NL;
		return result;
	}

	private String getSharedFeatureConfig() {
		String result = NL + FEATURE_HEADER;
		result += getLanguageModelSpecification();
		result += NL + WORD_PENALTY_FEATURE_NAME + " " + tuneableString(true);
		result += NL + UNKNOWN_WORD_PENALTY_FEATURE_NAME;
		return result;
	}

	private String getTestCustomConfig() {
		String result = getDynamicHeader();
		result += getSharedFeatureConfig();
		result += getTestTranslationTablesSpecification();
		return result;
	}

	private String getMertCustomConfig() {
		String result = getDynamicHeader();
		result += getSharedFeatureConfig();
		result += getMertTranslationTablesSpecification();
		return result;
	}

	/**
	 * Generate the static part of the Moses configuration
	 * 
	 * @return
	 */
	private String getStaticConfig() {
		String result = getStaticHeader();
		result += getModelWeights();
		result += getPruningConfig();
		result += getNonTerminalsSpecification();
		result += getSearhAlgorithmSpecification();
		result += getInputTypeSpecification();
		result += getMaxChartSpanSpecification();
		result += getTranslationDetailsLogSpecification();
		return result;
	}

	private String getMappingAndInputFactorsConfig() {
		String result = getInputFactorsSpecification();
		result += getMappingStepsSpecification();
		return result;
	}

	private String getMosesMertConfigFile() {
		String result = getMainHeader();
		result += getMappingAndInputFactorsConfig();
		result += getMertCustomConfig();
		result += getStaticConfig();
		return result;
	}

	private String getMosesTestConfigFile() {
		String result = getMainHeader();
		result += getMappingAndInputFactorsConfig();
		result += getTestCustomConfig();
		result += getStaticConfig();

		return result;
	}

	@Override
	protected String getSystemTypeName() {
		return MosesSystem.getSystemTypeName();
	}

}
