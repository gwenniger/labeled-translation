package mt_pipeline;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import mt_pipeline.grammarExtraction.ExternalProcessGrammarCreaterHiero;
import mt_pipeline.mt_config.MTConfigFile;

public class MTPipelineHatsHiero extends MTPipeline {
	public static final String SystemName = "JoshuaHatsHiero";
	private final String unknownWordsLabel;

	protected MTPipelineHatsHiero() {
		this.unknownWordsLabel = null;
	};

	private MTPipelineHatsHiero(MTConfigFile theConfig, String configFilePath, int noModelParameters, String unknownWordsLabel, boolean extractGrammarsAsExternalProcess, MTSystem mtSystem) {
		super(theConfig, configFilePath, noModelParameters, extractGrammarsAsExternalProcess, mtSystem);
		this.unknownWordsLabel = unknownWordsLabel;
	}

	public static MTPipeline createMTPipelineHatsHiero(String configFilePath) throws RuntimeException {
		MTConfigFile theConfig = MTConfigFile.createMtConfigFile(configFilePath, SystemName);
		SystemIdentity systemIdentity = SystemIdentity.createSystemIdentity(theConfig);
		MTSystem mtSystem = MTSystem.createMTSystem(theConfig, systemIdentity);
		return new MTPipelineHatsHiero(theConfig, configFilePath, mtSystem.noModelParameters(theConfig, SystemName), systemIdentity.getUnknownWordsLabel(),
				MTPipeline.extractGrammarsAsExternalProcess(configFilePath), mtSystem);
	}

	@Override
	protected void extractGrammars() {
		long startTime = System.currentTimeMillis();

		if (extractGrammarAsExternalProcess()) {
			ExternalProcessGrammarCreaterHiero.extractHieroGrammarsAsExternalProcess(getConfigFilePath());
		} else {
			GrammarCreaterHats.extractFilteredHatsHieroGrammarsPrallel(getTheConfig(),getSystemIdentity());
		}
		long passedTime = (System.currentTimeMillis() - startTime) / 1000;

		System.out.println(">>> HATs grammar extraction took: " + passedTime + "seconds");
	}

	@Override
	public MTPipeline createMTPipeline(String configFilePath) throws RuntimeException {
		return createMTPipelineHatsHiero(configFilePath);
	}

	@Override
	protected String getUnknownWordsLabel() {
		return this.unknownWordsLabel;
	}

}
