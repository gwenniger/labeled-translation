package mt_pipeline;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.samtGrammarExtraction.GlueGrammarCreater;
import mt_pipeline.mt_config.GrammarFilesLocations;
import mt_pipeline.mt_config.MTConfigFile;

public class GrammarCreaterHatsDev extends GrammarCreaterHats {

	private GrammarCreaterHatsDev(MTConfigFile theConfig, String grammarOutputPath, String category, MultiThreadGrammarExtracterCreater grammarExtracterCreater,
			GrammarCreaterHatsConfigFileCreater configFileCreater, GlueGrammarCreater glueGrammarCreater, SystemIdentity systemIdentity) {
		super(theConfig, grammarOutputPath, category, grammarExtracterCreater, configFileCreater, glueGrammarCreater, systemIdentity);
	}

	public static GrammarCreaterHatsDev createDevGrammarCreater(MTConfigFile theConfig, MultiThreadGrammarExtracterCreater grammarExtracterCreater,
			GrammarCreaterHatsConfigFileCreater configFileCreater, GlueGrammarCreater glueGrammarCreater, SystemIdentity systemIdentity) {
		return new GrammarCreaterHatsDev(theConfig, theConfig.filesConfig.getFilteredGrammarPathMert(), MTConfigFile.DEV_CATEGORY, grammarExtracterCreater, configFileCreater, glueGrammarCreater,
				systemIdentity);
	}

	private String getMainGrammarExtractionConfigFilePath() {
		return theConfig.filesConfig.getHatsMainGrammarExtractorConfigPathMert();
	}

	private String getSmoothingGrammarExtractionConfigFilePath() {
		return theConfig.filesConfig.getHatsSmoothingGrammarExtractorConfigPathMert();
	}

	@Override
	protected void writeGrammarExtractionConfigFile() {
		configFileCreater.writeHatsGrammarExtractionConfigFiles(getMainGrammarExtractionConfigFilePath(), getSmoothingGrammarExtractionConfigFilePath());
	}

	@Override
	protected void extractGrammars() {
		GrammarFilesLocations grammarFilesLocations = GrammarFilesLocations.createGrammarFilesLocations(getMainGrammarExtractionConfigFilePath(), getSmoothingGrammarExtractionConfigFilePath(),
				theConfig.filesConfig.getFilteredGrammarPathMert(), theConfig.filesConfig.getGlueGrammarPathMert(), getDenseMapFilePath(), getPackerConfigFilePath(),
				theConfig.filesConfig.getGapLabelConditionalProbabilityTableFilePathMert());

		performGrammarExtractionAndSorting(grammarFilesLocations, theConfig);
	}

}
