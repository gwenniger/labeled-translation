package mt_pipeline;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;
import grammarExtraction.translationRules.TranslationRuleStringLabelExtracter;

public class GrammarSanityChecker {
    private static final int MAX_NUM_LINES_TO_CHECK = 1000;

    private final String grammarFilePath;
    private static final TranslationRuleStringLabelExtracter TRANSLATION_RULE_STRING_LABEL_EXTRACTER = new TranslationRuleStringLabelExtracter();

    public GrammarSanityChecker(String grammarFilePath) {
	this.grammarFilePath = grammarFilePath;
    }

    private static int getLabelIndex(String label) {
	System.out.println("getLabelIndex: label:" + label);
	// Get the index from label of the form "[LABEL,INDEX]" e.g. "[HAT,1]"
	String indexSubstring = label.substring(label.lastIndexOf(",") + 1, label.length() - 1);
	return Integer.parseInt(indexSubstring);
    }

    private static List<String> getLabelsForRulePart(String rulePart) {
	List<String> labels = TRANSLATION_RULE_STRING_LABEL_EXTRACTER.getLabelsForRHSPart(rulePart,
		true);
	return labels;
    }

    private static Set<Integer> testRulePartAndReturnSetOfGapIndices(String rulePart) {
	List<String> labels = getLabelsForRulePart(rulePart);
	Set<Integer> observedLabelIndices = new HashSet<>();
	for (String label : labels) {
	    int index = getLabelIndex(label);
	    if (observedLabelIndices.contains(index)) {
		String errorMessage = "GrammarSanityChecker: Error: " + "rule part \"" + rulePart
			+ "\" contains multiple occurences of gap with the same index";
		System.out.println(errorMessage);
		throw new RuntimeException(errorMessage);
	    }
	    observedLabelIndices.add(index);
	}
	return observedLabelIndices;
    }

    public static void assertEqualLabelIndices(Set<Integer> rulePartOneLabelIndices,
	    Set<Integer> rulePartTwoLabelIndices, String rulePartOne, String rulePartTwo) {
	String errorMessage = "GrammarSanityChecker: Error: rule RHS part 1:" + rulePartOne
		+ " and rule RHS part 2: " + rulePartTwo + "do not contain matching label indices";
	if (!rulePartOneLabelIndices.equals(rulePartTwoLabelIndices)) {
	    System.out.println(errorMessage);
	    throw new RuntimeException(errorMessage);
	}
    }

    /**
     * This method checks that: 1. Both RHS rule parts contain every rule gap
     * index only ones 2. The set of rule gap indices for the first RHS part
     * matches those of the second RHS part
     * 
     * @param ruleString
     */
    public static void testString(String ruleString) {
	String[] ruleParts = JoshuaStyle.spitOnRuleSeparator(ruleString);
	Set<Integer> rulePartOneLabelIndices = testRulePartAndReturnSetOfGapIndices(ruleParts[1]);
	Set<Integer> rulePartTwoLabelIndices = testRulePartAndReturnSetOfGapIndices(ruleParts[2]);
	assertEqualLabelIndices(rulePartOneLabelIndices, rulePartTwoLabelIndices, ruleParts[1],
		ruleParts[2]);
    }

    public void checkGrammarRulesForErrors() {
	System.out.println("\nChecking the first rules (up to " + MAX_NUM_LINES_TO_CHECK + ") in "
		+ grammarFilePath + " for format errors:");
	System.out.println(
		"- Making sure that every gap index occurs only once per rule-RHS part ...");
	System.out.println(
		"- Also assuring the observed gap indices match up between the source- and target rule-RHS parts...\n");
	Path path = Paths.get(grammarFilePath);
	try (Stream<String> lines = Files.lines(path)) {
	    lines.forEach(GrammarSanityChecker::testString);
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }
}
