package mt_pipeline;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import junit.framework.Assert;
import datapreparation.FileEncodingConverter;
import datapreparation.StanfordSegmenterInterface;
import datapreparation.TokenizerInterface;
import mt_pipeline.mt_config.MTConfigFile;
import mt_pipeline.mt_config.MTFilesConfig;
import mt_pipeline.parserInterface.MetaDataAnnotatorInterface;
import mt_pipeline.parserInterface.MetaDataAnnotatorProcess;
import mt_pipeline.preAndPostProcessing.EnrichedSourceFileCreator;
import mt_pipeline.evaluation.GmlWrapper;
import mt_pipeline.preAndPostProcessing.Lowercase;
import mt_pipeline.preAndPostProcessing.SubFileCreater;
import mt_pipeline.tuning.TuningFilesCreater;
import util.FileUtil;
import util.Utility;

public class MTDataFilesCreater {

    protected final MTConfigFile theConfig;
    private final EnrichedSourceFileCreator sourceSubFileCreator;
    private final List<MetaDataAnnotatorInterface<? extends MetaDataAnnotatorProcess<?>>> metaDataAnnotators;
    private final MTSystem mtSystem;

    protected MTDataFilesCreater(
	    MTConfigFile theConfig,
	    List<MetaDataAnnotatorInterface<? extends MetaDataAnnotatorProcess<?>>> metaDataAnnotators,
	    EnrichedSourceFileCreator sourceSubFileCreator, MTSystem mtSystem) {
	this.theConfig = theConfig;
	this.sourceSubFileCreator = sourceSubFileCreator;
	this.metaDataAnnotators = metaDataAnnotators;
	this.mtSystem = mtSystem;
    }

    private static String missingParameterMessage(List<String> paramterAlternatives,
	    String methodName) {
	return "MTDataFilesCreater:\n" + "Required parameter alternatives:  "
		+ Utility.stringListStringWithoutBrackets(paramterAlternatives) + "\n for "
		+ methodName + " is not present in configfile."
		+ "\n Could not create train data.\n Exiting";
    }

    private boolean parameterPresent(String parameterName) {
	return this.theConfig.hasValue(parameterName);
    }

    private boolean allAlternativesMissing(List<String> parameterAlternatives) {
	boolean result = true;
	for (String parameterAlternative : parameterAlternatives) {
	    if (parameterPresent(parameterAlternative)) {
		result = false;
	    }
	}
	return result;
    }

    private void checkAlrequiredParametersArePresent(
	    List<List<String>> requiredParameterAlternativesLists, String methodName) {
	for (List<String> parameterAlternatives : requiredParameterAlternativesLists) {
	    if (allAlternativesMissing(parameterAlternatives)) {
		System.out.println(missingParameterMessage(parameterAlternatives, methodName));
		System.exit(1);
	    }
	}
    }

    private void createSourceAndTargetFiles(String category) {
	FileUtil.createFolderIfNotExisting(theConfig.foldersConfig.getDataOutputFolderName());
	FileUtil.createFolderIfNotExisting(theConfig.foldersConfig
		.getCategoryDataFolderName(category));

	// this.sourceSubFileCreator.createSourceSubFile(theConfig, category);
	SubFileCreater.createSubFile(theConfig.filesConfig.getSourceInputFilePath(category),
		theConfig.filesConfig.getSourceSubFilePath(category),
		theConfig.decoderConfig.getDataLength(category));

	List<String> targetFilePaths = theConfig.filesConfig.getTargetInputFilePaths(category);
	List<String> targetSubFilePaths = theConfig.filesConfig.getTargetSubFilePaths(category);
	Assert.assertEquals(targetFilePaths.size(), targetSubFilePaths.size());
	for (int i = 0; i < targetFilePaths.size(); i++)
	    SubFileCreater.createSubFile(targetFilePaths.get(i), targetSubFilePaths.get(i),
		    theConfig.decoderConfig.getDataLength(category));
    }

    private void createTokenizedFile(String inputFileName, String tokenizedOutputFileName,
	    String language) {
	TokenizerInterface tokenizerInterface = TokenizerInterface
		.createTokenizerInterface(this.theConfig.foldersConfig.getScriptsDir());
	tokenizerInterface.createTokenizedFile(inputFileName, tokenizedOutputFileName, language);

    }

    private void createSegmentedFile(String inputFileName, String tokenizedOutputFileName,
	    String language) {
	String stanfordSegmenterRootDir = this.theConfig.segmenterConfig
		.getStanfordSegmenterRootLocation();
	int numSegmenterThreads = this.theConfig.segmenterConfig.getNumSegmenterThreads();
	StanfordSegmenterInterface stanfordSegmenterInterface = StanfordSegmenterInterface

	.createStanfordSegmenterInterface(inputFileName, FileEncodingConverter.UTF8_ENCODING,
		this.theConfig.segmenterConfig.getSegmentationModelSpecificationString(),
		stanfordSegmenterRootDir, numSegmenterThreads);
	stanfordSegmenterInterface.createResultFilesMutltiThreaded();
	FileUtil.renameFile(stanfordSegmenterInterface.getSegmenterOutputFileName(),
		tokenizedOutputFileName);
    }

    private void createPreprocessedFile(String inputFileName, String tokenizedOutputFileName,
	    String tokenizedAndLowerCasedOutputFileName, String language) {
	// See:
	// http://stackoverflow.com/questions/525212/how-to-run-unix-shell-script-from-java-code
	// http://docs.oracle.com/javase/6/docs/api/java/lang/ProcessBuilder.html

	// LinuxInteractor.executeCommand(new String[]{getScriptsDir() +
	// "preprocess_data.sh", inputFileName, outputFileName, language},true);

	if (theConfig.segmenterConfig.segmentInsteadOfTokenize(language)) {
	    createSegmentedFile(inputFileName, tokenizedOutputFileName, language);
	} else {
	    createTokenizedFile(inputFileName, tokenizedOutputFileName, language);
	}
	Lowercase.toLowercase(tokenizedOutputFileName, tokenizedAndLowerCasedOutputFileName);

	// remove the old input file
	File file = new File(inputFileName);
	file.delete();

    }

    private void createSourceAndTargetPreprocessedFile(String category) {
	createPreprocessedFile(theConfig.filesConfig.getSourceSubFilePath(category),
		theConfig.filesConfig.getSourceTokenizedFileName(category),
		theConfig.filesConfig.getSourceTokenizedAndLowerCasedFileName(category),
		theConfig.filesConfig.getSourceLanguageAbbreviation());

	List<String> targetSubFilePaths = theConfig.filesConfig.getTargetSubFilePaths(category);
	List<String> targetTokenizedFileNames = theConfig.filesConfig
		.getTargetTokenizedFileNames(category);
	List<String> targetTokenizedAndLowercasedFileNames = theConfig.filesConfig
		.getTargetTokenizedAndLowerCasedFileNames(category);
	Assert.assertEquals(targetSubFilePaths.size(), targetTokenizedFileNames.size());
	Assert.assertEquals(targetTokenizedFileNames.size(),
		targetTokenizedAndLowercasedFileNames.size());

	for (int i = 0; i < targetSubFilePaths.size(); i++) {
	    createPreprocessedFile(targetSubFilePaths.get(i), targetTokenizedFileNames.get(i),
		    targetTokenizedAndLowercasedFileNames.get(i),
		    theConfig.filesConfig.getTargetLanguageAbbreviation());
	}
    }

    private void createGMLWrappedTestFiles(String category) {
	// Create src file
	GmlWrapper.createGMLWrappedFile(
		theConfig.filesConfig.getSourceTokenizedAndLowerCasedFileName(category),
		theConfig.filesConfig.getGMLSrcFileName(category),
		theConfig.filesConfig.getSourceLanguageAbbreviation(),
		theConfig.filesConfig.getTargetLanguageAbbreviation(),
		theConfig.filesConfig.getSetAndDocID(category), GmlWrapper.SOURCE_TYPE);
	// create ref file
	GmlWrapper.createGMLWrappedFile(
		theConfig.filesConfig.getTargetTokenizedAndLowerCasedFileNames(category),
		theConfig.filesConfig.getGMLRefFileName(category),
		theConfig.filesConfig.getSourceLanguageAbbreviation(),
		theConfig.filesConfig.getTargetLanguageAbbreviation(),
		theConfig.filesConfig.getSetAndDocID(category));
    }

    private void createAlignmentFile() {
	SubFileCreater.createSubFile(theConfig.filesConfig.getAlignmentsInputFilePath(),
		theConfig.foldersConfig.getCategoryDataFolderName(MTConfigFile.TRAIN_CATEGORY)
			+ "alignments.txt",
		theConfig.decoderConfig.getDataLength(MTConfigFile.TRAIN_CATEGORY));
    }

    private void createTrainDataFiles() {
	List<List<String>> requiredParameters = Arrays.asList(
		Collections.singletonList("rootOutputFolder"),
		Collections.singletonList("trainDataInputDirectory"),
		Collections.singletonList("trainSourceName"),
		Collections.singletonList("trainTargetName"),
		Collections.singletonList("trainAlignmentsName"),
		Collections.singletonList("trainDataLength"));
	checkAlrequiredParametersArePresent(requiredParameters, "createTrainDataFiles()");

	createSourceAndTargetFiles(MTConfigFile.TRAIN_CATEGORY);
	createAlignmentFile();
    }

    private void createMetaDataAnnotationFiles() {
	for (MetaDataAnnotatorInterface<?> metaDataAnnotator : this.metaDataAnnotators) {
	    metaDataAnnotator.createResultFilesMutltiThreaded();
	}
    }

    private void createCategoryOutputFiles(String category) {
	List<List<String>> requiredParameters = Arrays.asList(
		Collections.singletonList("rootOutputFolder"),
		Collections.singletonList(category + "DataInputDirectory"),
		Collections.singletonList(category + "SourceName"),
		Arrays.asList(category + "TargetName", category + "TargetNames"),
		Collections.singletonList(category + "DataLength"));
	checkAlrequiredParametersArePresent(requiredParameters, "createCategoryOutputFiles()");
	createSourceAndTargetFiles(category);
	createSourceAndTargetPreprocessedFile(category);

    }

    private void createDevDataFiles() {
	createCategoryOutputFiles(MTConfigFile.DEV_CATEGORY);
	createGMLWrappedTestFiles(MTConfigFile.DEV_CATEGORY);
    }

    private void createTestDataFiles() {
	createCategoryOutputFiles(MTConfigFile.TEST_CATEGORY);
	createGMLWrappedTestFiles(MTConfigFile.TEST_CATEGORY);
    }

    protected void createSharedDataFiles() {
	createTrainDataFiles();
	createDevDataFiles();
	createTestDataFiles();
	createMetaDataAnnotationFiles();
	this.sourceSubFileCreator.createEnrichedSourceFile(theConfig, MTConfigFile.TRAIN_CATEGORY);
	this.sourceSubFileCreator.createEnrichedSourceFile(theConfig, MTConfigFile.TEST_CATEGORY);
	this.sourceSubFileCreator.createEnrichedSourceFile(theConfig, MTConfigFile.DEV_CATEGORY);

	FileUtil.createFolderIfNotExisting(theConfig.foldersConfig.getResourcesFolder());
	FileUtil.createFolderIfNotExisting(theConfig.foldersConfig.getIntermediateDevFolder());
	FileUtil.createFolderIfNotExisting(theConfig.foldersConfig.getIntermediateRunSpecificDevFolder());
	FileUtil.createFolderIfNotExisting(theConfig.foldersConfig.getIntermediateTestFolder());
	FileUtil.createFolderIfNotExisting(theConfig.foldersConfig.getIntermediateRunSpecificTestFolder());
	createLanguageModelIfNotCreatedExternally();
    }

    private void createLanguageModelIfNotCreatedExternally() {
	if (!theConfig.filesConfig.useExternallyGeneratedLanguageModel()) {
	    LanguageModelCreater.createLM(this.theConfig, mtSystem.getSystemIdentity());
	} else {
	    if (!FileUtil.fileExists(theConfig.filesConfig.getLanguageExternallyGeneratedPath())) {
		throw new RuntimeException("Error: you set the paramter "
			+ MTFilesConfig.LM_FILE_EXTERNALLY_GENERATED_PATH_PROPERTY + " = "
			+ theConfig.filesConfig.getLanguageExternallyGeneratedPath()
			+ " but the file at this path does not exist");
	    }
	}
    }

    protected static void createJoshuaPrefixTable(MTConfigFile theConfig,
	    SystemIdentity systemIdentity) {
	GrammarCreaterJoshuaPure prefixTableCreater = GrammarCreaterJoshuaPure
		.createTestGrammarCreater(theConfig, systemIdentity);
	prefixTableCreater.createPrefixTable();
    }

    protected void createSystemSpecificFilesMertAndEvaluation(int noModelParameters) {
	System.out.println("Entered createSystemSpecificFilesMertAndEvaluation...");

	// The intermediate dev and test folder also needs to be created, as it is new for every  
	// run and system when "SpecificRunSubFolderName" is used to specify a run and system combination
	FileUtil.createFolderIfNotExisting(theConfig.foldersConfig
		.getIntermediateRunSpecificDevFolder());	
	FileUtil.createFolderIfNotExisting(theConfig.foldersConfig
		.getIntermediateRunSpecificTestFolder());

	// MERT
	FileUtil.createFolderIfNotExisting(theConfig.foldersConfig.getMertFolder());
	// Create run specific MERT folder
	FileUtil.createFolderIfNotExisting(theConfig.foldersConfig.getMertRunSpecificFolder());

	TuningFilesCreater.writeMertFiles(theConfig, noModelParameters,
		mtSystem.getSystemIdentity());
	mtSystem.writeDecodingConfigFiles();
    }
}
