package mt_pipeline;

import mt_pipeline.mt_config.MTConfigFile;
import grammarExtraction.chiangGrammarExtraction.MultiThreadGrammarExtractorTestFiltered;
import grammarExtraction.grammarExtractionFoundation.MultiThreadGrammarExtractor;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;

public class GrammarExtracterCreaterHatsBoundaryTagLabeled extends GrammarExtracterCreater {

	private GrammarExtracterCreaterHatsBoundaryTagLabeled(boolean writePlainHieroRules,SystemIdentity systemIdentity) {
		super(writePlainHieroRules,systemIdentity);
	}

	public static GrammarExtracterCreaterHatsBoundaryTagLabeled createGrammarExtracterCreaterHatsBoundaryTagLabeled(MTConfigFile theConfig,SystemIdentity systemIdentity) {
		return new GrammarExtracterCreaterHatsBoundaryTagLabeled(systemIdentity.writePlainHieroRulesForSmoothing(),systemIdentity);
	}

	@Override
	public MultiThreadGrammarExtractor createGrammarExtracterTestFiltered(String configFilePath, int numThreads) {
		return MultiThreadGrammarExtractorTestFiltered.createMultiThreadGrammarExtractorTestFilteredBoundaryTagLabeled(configFilePath, numThreads,this.systemIdentity);
	}

	@Override
	public String getSystemName() {
		return MTPipelineHatsBoundaryTagLabeled.SystemName;
	}

}
