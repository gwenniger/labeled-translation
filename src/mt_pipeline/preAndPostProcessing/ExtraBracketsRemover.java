package mt_pipeline.preAndPostProcessing;

import util.Span;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExtraBracketsRemover {

	private final static Pattern BracketsAndWhiteSpacePattern = Pattern.compile("[()\\s]*");

	private static boolean isOpeningBracket(char c) {
		return c == '(';
	}

	private static boolean isClosingBracket(char c) {
		return c == ')';
	}

	private static Deque<Span> computeBracketPairPositions(String s) {
		Deque<Span> result = new ArrayDeque<Span>();
		Deque<Integer> openBrackenPositionsStack = new ArrayDeque<Integer>();

		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);

			if (isOpeningBracket(c)) {
				openBrackenPositionsStack.push(i);
			} else if (isClosingBracket(c)) {
				Integer openingPosition = openBrackenPositionsStack.pop();
				Integer closingPosition = i;
				result.push(new Span(openingPosition, closingPosition));
			}
		}
		return result;
	}

	private static String outerMinusInnerSpanSubString(String s, Span outerBracketsSpan, Span innerBracketsSpan) {
		return s.substring(outerBracketsSpan.getFirst(), innerBracketsSpan.getFirst())
				+ s.substring(innerBracketsSpan.getSecond() + 1, outerBracketsSpan.getSecond() + 1);
	}

	public static boolean containsOnlyBracketsAndWhiteSpaceCharacters(String s) {
		Matcher matcher = BracketsAndWhiteSpacePattern.matcher(s);
		return matcher.matches();
	}

	private static Span getOuterNescessaryBracketsPositions(String string, Deque<Span> bracketPairPositions) {
		Span outerBracketsSpan, innerBracketsSpan;
		String outerMinusInnerSpanSubString;

		if (bracketPairPositions.isEmpty()) {
			throw new RuntimeException("There are no brackets at all!!!");
		}

		innerBracketsSpan = bracketPairPositions.pop();

		do {
			outerBracketsSpan = innerBracketsSpan;

			if (bracketPairPositions.isEmpty()) {
				return outerBracketsSpan;
			}

			innerBracketsSpan = bracketPairPositions.pop();
			//System.out.println("outerBracketsSpan: " + outerBracketsSpan + " " + "innerBracketsSpan: " + innerBracketsSpan);
			outerMinusInnerSpanSubString = outerMinusInnerSpanSubString(string, outerBracketsSpan, innerBracketsSpan);
			//System.out.println("outerMinusInnerSpanSubString : " + outerMinusInnerSpanSubString);

		} while (outerBracketsSpan.contains(innerBracketsSpan) && containsOnlyBracketsAndWhiteSpaceCharacters(outerMinusInnerSpanSubString));

		return outerBracketsSpan;
	}

	public static String removeExtraBrackets(String s) {
		//System.out.println("removeExtraBracket called for :  " + s);
		Deque<Span> bracketPairPositions = computeBracketPairPositions(s);
		Span nescessaryBracketPositionsSpan = getOuterNescessaryBracketsPositions(s, bracketPairPositions);
		return s.substring(nescessaryBracketPositionsSpan.getFirst(), nescessaryBracketPositionsSpan.getSecond() + 1);
	}
}
