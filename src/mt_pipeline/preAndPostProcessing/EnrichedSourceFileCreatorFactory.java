package mt_pipeline.preAndPostProcessing;

import grammarExtraction.sourceEnrichment.CenterTagEnrichedSourceFileCreator;
import grammarExtraction.sourceEnrichment.TagTripleEnrichedSourceFileCreator;

import java.util.Arrays;
import java.util.List;

import util.ConfigFile;

import mt_pipeline.mt_config.MTConfigFile;

public class EnrichedSourceFileCreatorFactory {

	private static String getSourceEnrichmentScheme(ConfigFile theConfig) {
		if (!theConfig.hasValue(EnrichedSourceFileCreator.SOURCE_ENRICHMENT_SCHEME_PROPERTY)) {
			System.out.println("Error: Required parameter " + EnrichedSourceFileCreator.SOURCE_ENRICHMENT_SCHEME_PROPERTY + " not specified in config file.");
			printSpecifySourceEnrichmentMessage();
			System.exit(0);

		}
		String sourceSubFileEnrichmentScheme = theConfig.getValueWithPresenceCheck(EnrichedSourceFileCreator.SOURCE_ENRICHMENT_SCHEME_PROPERTY);
		return sourceSubFileEnrichmentScheme;
	}

	public static boolean useSourceEnrichment(ConfigFile theConfig) {

		if (!theConfig.hasValue(EnrichedSourceFileCreator.SOURCE_ENRICHMENT_SCHEME_PROPERTY)) {
			return false;
		}

		String sourceSubFileEnrichmentScheme = getSourceEnrichmentScheme(theConfig);
		if ((sourceSubFileEnrichmentScheme.equals(CenterTagEnrichedSourceFileCreator.SourceEnrichmentSchemeName))
				|| sourceSubFileEnrichmentScheme.equals(TagTripleEnrichedSourceFileCreator.SourceEnrichmentSchemeName)) {
			return true;
		}
		return false;
	}

	public static EnrichedSourceFileCreator getSourceSubFileCreaterBasedOnConfig(MTConfigFile theConfig) {
		String sourceSubFileEnrichmentScheme = getSourceEnrichmentScheme(theConfig);

		if (knownSourceEnrichmentSchemes().contains(sourceSubFileEnrichmentScheme)) {
			if (sourceSubFileEnrichmentScheme.equals(PlainSourceFileCreator.SOURCE_ENRICHMENT_SCHEME_NAME)) {
				return new PlainSourceFileCreator();
			} else if (sourceSubFileEnrichmentScheme.equals(CenterTagEnrichedSourceFileCreator.SourceEnrichmentSchemeName)) {
				return CenterTagEnrichedSourceFileCreator.createCenterTagEnrichedSourceFileCreator();
			} else if (sourceSubFileEnrichmentScheme.equals(TagTripleEnrichedSourceFileCreator.SourceEnrichmentSchemeName)) {
				return TagTripleEnrichedSourceFileCreator.createTagTripleEnrichedSourceFileCreator();
			}
		}
		System.out.println("The Source Enrichment Scheme named \"" + sourceSubFileEnrichmentScheme + "\" is unknows");
		printSpecifySourceEnrichmentMessage();
		throw new RuntimeException("Error: Unrecognized or unspecified source subfile creater scheme");

	}

	private static void printSpecifySourceEnrichmentMessage() {
		System.out.println("Please make sure to specify a source enrichement scheme from the following list:");
		for (String knownSourceEnrichmentScheme : knownSourceEnrichmentSchemes()) {
			System.out.println(knownSourceEnrichmentScheme);
		}
	}

	private static List<String> knownSourceEnrichmentSchemes() {
		List<String> result = Arrays.asList(PlainSourceFileCreator.SOURCE_ENRICHMENT_SCHEME_NAME,
				CenterTagEnrichedSourceFileCreator.SourceEnrichmentSchemeName, TagTripleEnrichedSourceFileCreator.SourceEnrichmentSchemeName);
		return result;
	}

}
