package mt_pipeline.preAndPostProcessing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

public class SubFileCreater extends FileConverter {
    private final int numLinesToCopy;
    private final int startLine;

    protected SubFileCreater(BufferedReader inputReader, BufferedWriter outputWriter,
	    int numLinesToCopy, int startLine) {
	super(inputReader, outputWriter);
	this.numLinesToCopy = numLinesToCopy;
	this.startLine = startLine;
    }

    private static SubFileCreater createSubFileCreater(String inputFileName, String outputFileName,
	    int numLinesToCopy, int startLine) throws IOException {
	System.out.println("createSubFileCreater: inputFile:" + inputFileName + " , outputFile: "
		+ outputFileName);
	return new SubFileCreater(createInputReader(inputFileName),
		createOutputWriter(outputFileName), numLinesToCopy, startLine);
    }

    public static void createSubFile(String inputFileName, String outputFileName, int numLinesToCopy) {
	SubFileCreater sfc;
	try {
	    sfc = createSubFileCreater(inputFileName, outputFileName, numLinesToCopy, 0);
	    sfc.performConversion();
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    public static void createSubFile(String inputFileName, String outputFileName,
	    int numLinesToCopy, int startLine) {
	SubFileCreater sfc;
	try {
	    sfc = createSubFileCreater(inputFileName, outputFileName, numLinesToCopy, startLine);
	    sfc.performConversion();
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private void createSubFile() throws IOException {

	System.out.println("Create sub file - noLinesToCopy: " + numLinesToCopy);
	String line;

	// First read lines without processing until the start line is reached
	for (int i = 0; i < startLine; i++) {
	    line = inputReader.readLine();
	}

	int processedLines = 0;
	// Then add numLinesToCopy lines to the output writer
	while ((processedLines < numLinesToCopy) && ((line = inputReader.readLine()) != null)) {
	    outputWriter.write(line + "\n");
	    processedLines++;
	}
	outputWriter.close();
	inputReader.close();
	System.out.println("Completed subfile creation...");
    }

    @Override
    protected void performFileOperation() throws IOException {
	createSubFile();
    }

}
