package mt_pipeline.preAndPostProcessing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;


public class Lowercase extends FileConverter {
	private Lowercase(BufferedReader inputReader, BufferedWriter outputWriter) {
		super(inputReader, outputWriter);
	}

	public static Lowercase createLowercase(String inputFileName, String outputFileName) throws IOException {
		return new Lowercase(createInputReader(inputFileName), createOutputWriter(outputFileName));
	}

	public static void toLowercase(String inputFileName, String outputFileName) {
		try {
			Lowercase lowerCase = Lowercase.createLowercase(inputFileName, outputFileName);
			lowerCase.performConversion();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	@Override
	protected void performFileOperation() throws IOException {
		performLowercaseConversion();
	}

	private void performLowercaseConversion() throws IOException {
		String line;
		while ((line = inputReader.readLine()) != null) {
			outputWriter.write(line.toLowerCase() + "\n");
		}

		outputWriter.close();
		System.out.println("Completed conversion to lowercase...");
	}

	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println("Usage: java -jar lowercase.jar SourceFile ResultFile");
			System.exit(0);
		}
		String inputFileName = args[0];
		String outputFileName = args[1];

		try {
			Lowercase lowercase = createLowercase(inputFileName, outputFileName);
			lowercase.performLowercaseConversion();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
