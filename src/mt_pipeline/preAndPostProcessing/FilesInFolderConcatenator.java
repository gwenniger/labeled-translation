package mt_pipeline.preAndPostProcessing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;

public class FilesInFolderConcatenator {

	private static void concatenateFilesOrderedLexicographically(String folderPath, String outputPath) {
		try {

			BufferedWriter outputWriter = new BufferedWriter(new OutputStreamWriter(
			        new FileOutputStream(outputPath), "UTF8")); 
			File folder = new File(folderPath);
			File[] files = folder.listFiles();
			// Sort the files
			Arrays.sort(files);

			for (int i = 0; i < files.length; i++) {

				System.out.println("Processing " + files[i].getPath() + "... ");
				BufferedReader br = new BufferedReader(new FileReader(files[i].getPath()));
				String line = br.readLine();
				while (line != null) {
					outputWriter.write(line + "\n");
					line = br.readLine();
				}
				br.close();
			}
			outputWriter.close();
			System.out.println("All files have been concatenated into" + outputPath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	static public void main(String args[]) {

		if (args.length != 2) {
			System.out.println("Usage : concatenateFilesOrderedLexicographically InputFolder OutputFilePath");
			System.exit(0);
		}

		concatenateFilesOrderedLexicographically(args[0], args[1]);
	}

}
