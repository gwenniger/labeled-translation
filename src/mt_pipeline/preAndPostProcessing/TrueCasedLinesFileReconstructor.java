package mt_pipeline.preAndPostProcessing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

public class TrueCasedLinesFileReconstructor {

	/*
	 * The problem this class solves is where you have a file File1 with lowercased sentences and a file File2 with the same sentences, but TrueCased, in the
	 * same order, but containing some extra "pushed in between" sentences as well that were not in File1. To make things even worse, the sentences in File1 may
	 * span more then one line File2 as a result of a different sentence alignment in File2. (I.e. this happens as the sentence alignment changed with newer
	 * version of Europarl). As a result, also the whitespace may change. In other words, the only thing that is sure that the relative order of sentences in
	 * File1 is the same in File2. And also any sentence as a sequence of non-whitespace letters in File1 occurs in in File2, when whitespace is completely
	 * disregarded. While difficult, these constraints are sufficient to be able to form the TrueCased version of File1 from File2. Now you want to create the
	 * TrueCased equivalent of File1, based on File2, but because of the "pushed in between" noise sentences, you have to do some realignment to be able to do
	 * this. This program solves exactly this problem
	 */

	private final BufferedReader lowerCasedSentencesListReader;
	// a "super list" is the analog of the superset for lists
	// It contains the sentences of the original list in preserved order plus
	// some extra
	// "noise" sentences that are so to say pushed in between
	private final String trueCasedSentencesFilePath;

	private final BufferedWriter resultWriter;

	private boolean orderMaintainedInCasedFile;

	private TrueCasedLinesFileReconstructor(BufferedReader lowerCasedSentencesListReader, String trueCasedSentencesFilePath, BufferedWriter resultWriter, boolean orderMaintainedInCasedFile) {
		this.lowerCasedSentencesListReader = lowerCasedSentencesListReader;
		this.trueCasedSentencesFilePath = trueCasedSentencesFilePath;
		;
		this.resultWriter = resultWriter;
		this.orderMaintainedInCasedFile = orderMaintainedInCasedFile;
	}

	private static BufferedReader createCasedSentencesReader(String caseSentencesFilePath) {
		BufferedReader result;
		try {
			result = new BufferedReader(new InputStreamReader(new FileInputStream(caseSentencesFilePath), "UTF8"));
			return result;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	public static TrueCasedLinesFileReconstructor createMisalignedTrueCasedLinesFileAligner(String lowerCasedSentencesFilePath, String trueCasedSentencesFilePath, String resultFilePath,
			boolean orderMaintainedInCasedFile) {
		try {
			BufferedReader lowerCasedSentencesListReader = createCasedSentencesReader(lowerCasedSentencesFilePath);
			BufferedWriter resultWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(resultFilePath), "UTF8"));
			return new TrueCasedLinesFileReconstructor(lowerCasedSentencesListReader, trueCasedSentencesFilePath, resultWriter, orderMaintainedInCasedFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	private void closeAllReadersAndWriters() throws IOException {
		this.lowerCasedSentencesListReader.close();
		this.resultWriter.close();
	}

	private boolean produceLowercaseFileAlignedTrueCaseResultFile() {
		String lowerCasedFileLine;
		lowerCasedFileLine = null;

		TrueCasedLineReconstructor trueCasedLineReconstructor = TrueCasedLineReconstructor.createTrueCasedLineReconstructor();

		try {

			FileInputStream fIn = new FileInputStream(trueCasedSentencesFilePath);
			BufferedReader trueCasedSentencesSuperListReader = new BufferedReader(new InputStreamReader(fIn, "UTF8"));

			int i = 1;
			int trueCasedLineNum = 1;
			boolean endReachedPrematurely = false;
			while ((!endReachedPrematurely) && ((lowerCasedFileLine = lowerCasedSentencesListReader.readLine()) != null)) {

				if ((i % 10000) == 0) {
					System.out.println("Processing lowercased sentence " + i);
				}
				System.out.println("Searching match for line " + i + " : " + lowerCasedFileLine);
				System.out.println("==> with only letters " + i + " : " + TrueCasedLineReconstructor.stringWithoutAnyNonLetterCharacters(lowerCasedFileLine));

				String trueCasedFileLine = null;
				String trueCasedResultLine = null;
				trueCasedResultLine = trueCasedLineReconstructor.getTrueCasedMatchingLine(lowerCasedFileLine, trueCasedLineNum);

				while ((trueCasedResultLine == null) && (!endReachedPrematurely)) {
					trueCasedFileLine = trueCasedSentencesSuperListReader.readLine();

					endReachedPrematurely = (trueCasedFileLine == null);
					if (!endReachedPrematurely) {
						trueCasedLineReconstructor.addTrueCasedLineToBuffers(trueCasedFileLine);
						// System.out.println("lowercaseFileLine: "
						// +lowerCasedFileLine);
						trueCasedResultLine = trueCasedLineReconstructor.getTrueCasedMatchingLine(lowerCasedFileLine, trueCasedLineNum);
					}
					trueCasedLineNum++;
				}

				if ((trueCasedResultLine == null) && (!orderMaintainedInCasedFile)) {

					System.out.println(" Retry the search over the whole file, starting from the beginning");
					endReachedPrematurely = false;

					fIn.getChannel().position(0);
					trueCasedSentencesSuperListReader = new BufferedReader(new InputStreamReader(fIn, "UTF8"));
					trueCasedLineNum = 1;

					trueCasedLineReconstructor.clearBuffers();
					trueCasedResultLine = trueCasedLineReconstructor.getTrueCasedMatchingLine(lowerCasedFileLine, trueCasedLineNum);

					while ((trueCasedResultLine == null) && (!endReachedPrematurely)) {
						trueCasedFileLine = trueCasedSentencesSuperListReader.readLine();

						endReachedPrematurely = (trueCasedFileLine == null);
						if (!endReachedPrematurely) {
							trueCasedLineReconstructor.addTrueCasedLineToBuffers(trueCasedFileLine);
							// System.out.println("lowercaseFileLine: "
							// +lowerCasedFileLine);
							trueCasedResultLine = trueCasedLineReconstructor.getTrueCasedMatchingLine(lowerCasedFileLine, trueCasedLineNum);
						}
						trueCasedLineNum++;
					}

				}

				if (!endReachedPrematurely) {
					System.out.println("Writing matched line for line " + i + " \"" + lowerCasedFileLine + "\"");
					System.out.println("Matched line \"" + trueCasedResultLine + "\"");
					resultWriter.write(trueCasedResultLine + "\n");
					i++;
				}

			}

			trueCasedSentencesSuperListReader.close();
			closeAllReadersAndWriters();

			if (endReachedPrematurely) {
				System.out.println("Could not find a matching line for sentence number  : " + i + "\"" + lowerCasedFileLine + "\"");
				return false;
			} else {
				return true;
			}

		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
		}

	}

	public static void produceTrueCasedEquivalentLowerCasedFile(String lowerCasedSentencesFilePath, String trueCasedSentencesFilePath, String resultFilePath) {
		boolean success = false;

		TrueCasedLinesFileReconstructor misalignedTrueCasedLinesFileAligner = TrueCasedLinesFileReconstructor.createMisalignedTrueCasedLinesFileAligner(lowerCasedSentencesFilePath,
				trueCasedSentencesFilePath, resultFilePath, false);
		success = misalignedTrueCasedLinesFileAligner.produceLowercaseFileAlignedTrueCaseResultFile();

		if (!success) {
			System.out.println("Failed to extract matches for all lines");
			System.exit(0);
		}

	}

	public static void testCorrectnessResultFile(String lowerCasedSentencesFilePath, String resultFilePath) {

		BufferedReader lowerCasedFileReader = null;
		BufferedReader resultFileReader = null;

		try {
			lowerCasedFileReader = new BufferedReader(new FileReader(lowerCasedSentencesFilePath));
			resultFileReader = new BufferedReader(new FileReader(resultFilePath));

			String lowerCasedLine, resultLine;
			boolean lowerCasedReaderHasMoreLines, resultReaderHasMoreLines;
			lowerCasedReaderHasMoreLines = true;
			resultReaderHasMoreLines = true;

			int i = 1;
			while (lowerCasedReaderHasMoreLines && resultReaderHasMoreLines) {
				System.out.println("Checking that line " + i + " is equal in both files");
				lowerCasedLine = lowerCasedFileReader.readLine();
				resultLine = resultFileReader.readLine();
				lowerCasedReaderHasMoreLines = (lowerCasedLine != null);
				resultReaderHasMoreLines = (resultLine != null);

				if (lowerCasedReaderHasMoreLines != resultReaderHasMoreLines) {
					lowerCasedFileReader.close();
					resultFileReader.close();
					throw new RuntimeException("Failure : Files have unequal number of lines");
				}
				if (lowerCasedReaderHasMoreLines && resultReaderHasMoreLines) {
					if (!lowerCasedLine.equalsIgnoreCase(resultLine)) {
						String errorMessage = "ErroExpected two equal lines, got:\n" + "lowerCasedLine : " + lowerCasedLine + "\n" + "resultLine : " + resultLine + "\n";
						lowerCasedFileReader.close();
						resultFileReader.close();
						throw new RuntimeException(errorMessage);
					}
				}
				i++;
			}
			System.out.println("Succes: every line in the lower cased line has a corresponding (Cased) match in the resultFile");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			try {
				lowerCasedFileReader.close();
				resultFileReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

	public static void main(String[] args) {
		if (args.length != 3) {
			int i = 0;
			for (String arg : args) {
				System.out.println("arg " + i + " " + arg);
				i++;
			}

			System.out.println("Usage : produceTrueCasedEquivalentLowerCasedFile  lowerCasedSentencesFilePath trueCasedSentencesFilePath resultFilePath");
			System.exit(0);
		}

		produceTrueCasedEquivalentLowerCasedFile(args[0], args[1], args[2]);
		testCorrectnessResultFile(args[0], args[2]);
	}

}
