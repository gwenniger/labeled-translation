package mt_pipeline.preAndPostProcessing;

import mt_pipeline.mt_config.MTConfigFile;

public interface EnrichedSourceFileCreator {
	public static String WORD_ENRICHMENT_SEPARATOR = "<W-E>";
	public static String SOURCE_ENRICHMENT_SCHEME_PROPERTY = "sourceEnrichmentScheme";
	public void createEnrichedSourceFile(MTConfigFile theConfig,String category);
}
