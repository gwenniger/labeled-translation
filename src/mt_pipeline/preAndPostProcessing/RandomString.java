package mt_pipeline.preAndPostProcessing;

import java.util.Random;

public class RandomString {

	private static final char[] symbols = createInitialSymbols();

	private static char[] createInitialSymbols() {
		char[] symbols = new char[36];
		for (int idx = 0; idx < 10; ++idx)
			symbols[idx] = (char) ('0' + idx);
		for (int idx = 10; idx < 36; ++idx)
			symbols[idx] = (char) ('a' + idx - 10);
		return symbols;
	}

	private final Random random = new Random();

	private final char[] buf;

	public RandomString(int length) {
		if (length < 1)
			throw new IllegalArgumentException("length < 1: " + length);
		buf = new char[length];
	}

	public String nextString() {
		for (int idx = 0; idx < buf.length; ++idx)
			buf[idx] = symbols[random.nextInt(symbols.length)];
		return new String(buf);
	}

	public String nextString(int length) {
		char[] localBuffer = new char[length];
		for (int idx = 0; idx < localBuffer.length; ++idx)
			localBuffer[idx] = symbols[random.nextInt(symbols.length)];
		return new String(localBuffer);
	}

	public String nextStringRandomLength(int maxLength) {
		int length = random.nextInt(maxLength);
		char[] localBuffer = new char[length];
		for (int idx = 0; idx < localBuffer.length; ++idx)
			localBuffer[idx] = symbols[random.nextInt(symbols.length)];
		return new String(localBuffer);
	}
}
