package mt_pipeline.preAndPostProcessing;

import junit.framework.Assert;

public class TrueCasedLineReconstructor {

	private static String AnyUnicodeLetterExpression = "\\p{L}";
	private static String AnyNonUnicodeLetterExpression = "[^" + AnyUnicodeLetterExpression + "]";

	private final StringBuilder lowerCasedLettersOnlyCharactersBuffer;
	private final StringBuilder trueCasedLettersOnlyCharactersBuffer;

	private int lastNoCharactersAdded = 0;

	private TrueCasedLineReconstructor(StringBuilder lowerCasedLettersOnlyCharactersBuffer, StringBuilder trueCasedLettersOnlyCharactersBuffer) {
		this.lowerCasedLettersOnlyCharactersBuffer = lowerCasedLettersOnlyCharactersBuffer;
		this.trueCasedLettersOnlyCharactersBuffer = trueCasedLettersOnlyCharactersBuffer;
	}

	protected static TrueCasedLineReconstructor createTrueCasedLineReconstructor() {
		return new TrueCasedLineReconstructor(new StringBuilder(), new StringBuilder());
	}

	/*
	 * private String stringWithoutWhiteSpace(String string) { return
	 * string.replaceAll("\\s", ""); }
	 */

	public static String stringWithoutAnyNonLetterCharacters(String string) {
		return string.replaceAll(AnyNonUnicodeLetterExpression, "");
	}

	public void clearBuffers() {
		this.trueCasedLettersOnlyCharactersBuffer.delete(0, this.trueCasedLettersOnlyCharactersBuffer.length());
		this.lowerCasedLettersOnlyCharactersBuffer.delete(0, this.lowerCasedLettersOnlyCharactersBuffer.length());
	}

	protected void addTrueCasedLineToBuffers(String line) {
		// String trueCasedLettersOnlyLine =
		// stringWithoutAnyNonLetterCharacters(line);
		// String lowerCasedLettersOnlyLine =
		// stringWithoutAnyNonLetterCharacters(line.toLowerCase());
		String trueCasedLettersOnlyLine = stringWithoutAnyNonLetterCharacters(line);
		String lowerCasedLettersOnlyLine = stringWithoutAnyNonLetterCharacters(line.toLowerCase());

		Assert.assertEquals(trueCasedLettersOnlyLine.length(), lowerCasedLettersOnlyLine.length());
		// System.out.println(" Line to add :" + lowerCasedLettersOnlyLine);
		// System.out.println("Adding " + trueCasedLettersOnlyLine.length() +
		// " characters to the buffers");
		this.trueCasedLettersOnlyCharactersBuffer.append(trueCasedLettersOnlyLine);
		this.lowerCasedLettersOnlyCharactersBuffer.append(lowerCasedLettersOnlyLine);
		// System.out.println("New buffer: " +
		// this.lowerCasedLettersOnlyCharactersBuffer.toString());
		lastNoCharactersAdded = lowerCasedLettersOnlyLine.length();
	}

	private int indexOfLowerCasedLineIgnoringNonLetters(String lowerCasedLineLettersOnly) {

		if (lowerCasedLettersOnlyCharactersBuffer.indexOf("fraukeßlerimnamendes") > 0) {
			System.out.println("buffer: " + lowerCasedLettersOnlyCharactersBuffer.toString());
		}

		return this.lowerCasedLettersOnlyCharactersBuffer.indexOf(lowerCasedLineLettersOnly);
	}

	/**
	 * Delete the span from begin (inclusive) to end (exclusive) from both
	 * buffers
	 * 
	 * 
	 * @param begin
	 *            : The first index of the span that is deleted
	 * @param end
	 *            : The first index after the deleted span that is kept
	 */
	private void deleteSpanFromBuffers(int begin, int end) {
		this.trueCasedLettersOnlyCharactersBuffer.delete(begin, end);
		this.lowerCasedLettersOnlyCharactersBuffer.delete(begin, end);
	}

	private void removeAllButLastNElementsFromBuffer(int noElementsToKeepAtEnd) {
		int noElementsToDelete = Math.max(0, this.lowerCasedLettersOnlyCharactersBuffer.length() - noElementsToKeepAtEnd);
		// System.out.println("No Elements in buffer: " +
		// this.lowerCasedLettersOnlyCharactersBuffer.length() +
		// "noElements to keep: " + noElementsToKeepAtEnd
		// + "  noElements to delete : " + noElementsToDelete);
		assert ((this.lowerCasedLettersOnlyCharactersBuffer.length() - noElementsToDelete) <= noElementsToKeepAtEnd);
		assert ((this.trueCasedLettersOnlyCharactersBuffer.length() - noElementsToDelete) <= noElementsToKeepAtEnd);
		this.trueCasedLettersOnlyCharactersBuffer.delete(0, noElementsToDelete);
		this.lowerCasedLettersOnlyCharactersBuffer.delete(0, noElementsToDelete);

	}

	private void checkBufferSizesInvariant(String lowerCasedSearchLineLettersOnly) {
		int maxAllowedBufferLenth = lowerCasedSearchLineLettersOnly.length() + lastNoCharactersAdded - 1;
		if ((!(this.lowerCasedLettersOnlyCharactersBuffer.length() <= maxAllowedBufferLenth))
				|| (!(this.trueCasedLettersOnlyCharactersBuffer.length() <= maxAllowedBufferLenth))) {
			throw new RuntimeException("Inconsistent size of buffers: maxAllowed size : " + maxAllowedBufferLenth + "lower cased buffer length: "
					+ this.lowerCasedLettersOnlyCharactersBuffer.length() + "true cased buffer length: " + this.trueCasedLettersOnlyCharactersBuffer.length());

		}
	}

	private String getTrueCasedMatchingLettersOnlyLine(String lowerCasedLineWithWhiteSpace, int lastTrueCasedLineNumAdded) {
		// String lowerCasedLineLettersOnly =
		// stringWithoutAnyNonLetterCharacters(lowerCasedLineWithWhiteSpace);
		String lowerCasedLineLettersOnly = stringWithoutAnyNonLetterCharacters(lowerCasedLineWithWhiteSpace);
		checkBufferSizesInvariant(lowerCasedLineLettersOnly);
		// System.out.println("lowerCaseddLineLettersOnly: " +
		// lowerCasedLineLettersOnly);
		int indexOfLowerCasedLineIgnoringWhiteSpace = indexOfLowerCasedLineIgnoringNonLetters(lowerCasedLineLettersOnly);
		boolean bufferContainsLowerCasedLineIgnoringWhiteSpace = (indexOfLowerCasedLineIgnoringWhiteSpace >= 0);

		if (bufferContainsLowerCasedLineIgnoringWhiteSpace) {
			int firstPositionAfterMatchedString = indexOfLowerCasedLineIgnoringWhiteSpace + lowerCasedLineLettersOnly.length();
			// System.out.println("lastTrueCasedLineNumAdded: " +
			// lastTrueCasedLineNumAdded);
			String result = this.trueCasedLettersOnlyCharactersBuffer.substring(indexOfLowerCasedLineIgnoringWhiteSpace, firstPositionAfterMatchedString);
			int bufferSizeBeforeDeleting = this.lowerCasedLettersOnlyCharactersBuffer.length();
			// System.out.println("Buffer before deleting: " +
			// this.lowerCasedLettersOnlyCharactersBuffer.toString());
			// System.out.println("span to be deleted: " +
			// this.lowerCasedLettersOnlyCharactersBuffer.substring(0,
			// firstPositionAfterMatchedString));
			deleteSpanFromBuffers(0, firstPositionAfterMatchedString);
			int bufferdSizeAfterDeleting = this.lowerCasedLettersOnlyCharactersBuffer.length();
			// System.out.println("(bufferSizeBeforeDeleting: " +
			// bufferSizeBeforeDeleting + " lowerCasedLineLettersOnly.length: "
			// + lowerCasedLineLettersOnly.length() +
			// " bufferdSizeAfterDeleting: " + bufferdSizeAfterDeleting);
			Assert.assertEquals(this.lowerCasedLettersOnlyCharactersBuffer.length(), this.trueCasedLettersOnlyCharactersBuffer.length());
			Assert.assertEquals(bufferSizeBeforeDeleting - lowerCasedLineLettersOnly.length() - indexOfLowerCasedLineIgnoringWhiteSpace,
					bufferdSizeAfterDeleting);
			// System.out.println("buffer after deleting: " +
			// this.lowerCasedLettersOnlyCharactersBuffer.toString());
			return result;
		}
		// We did not find a match, so it is safe to delete all but the
		// lowerCasedLineLettersOnly.length() - 1 characters
		// in both buffers
		int noElementsToKeepAtEnd =  (lowerCasedLineLettersOnly.length() - 1);
		// System.out.println("No elements to keep: " + noElementsToKeepAtEnd);
		removeAllButLastNElementsFromBuffer(noElementsToKeepAtEnd);

		return null;
	}

	protected String getTrueCasedMatchingLine(String lowerCasedLineWithWhiteSpace, int lastTrueCasedLineNumAdded) {
		String trueCasedMatchingLettersOnlyLine = getTrueCasedMatchingLettersOnlyLine(lowerCasedLineWithWhiteSpace, lastTrueCasedLineNumAdded);
		// System.out.println("trueCasedMatchingLettersOnlyLine: " +
		// trueCasedMatchingLettersOnlyLine);
		if (trueCasedMatchingLettersOnlyLine != null) {
			String result = "";
			int LettersOnlyIndex = 0;
			for (int i = 0; i < lowerCasedLineWithWhiteSpace.length(); i++) {
				char charI = lowerCasedLineWithWhiteSpace.charAt(i);
				if (Character.isLetter(charI)) {
					// if (!Character.isWhitespace(charI)) {
					result += trueCasedMatchingLettersOnlyLine.charAt(LettersOnlyIndex);
					LettersOnlyIndex++;
				} else {
					result += charI;
				}
			}
			return result;
		}
		return null;
	}
}
