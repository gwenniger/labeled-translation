package mt_pipeline.preAndPostProcessing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public abstract class FileConverter {
	protected final BufferedReader inputReader;
	protected final BufferedWriter outputWriter;

	protected FileConverter(BufferedReader inputReader, BufferedWriter outputWriter) {
		this.inputReader = inputReader;
		this.outputWriter = outputWriter;
	}

	public static BufferedReader createInputReader(String inputFileName) throws IOException {
		return new BufferedReader(new FileReader(inputFileName));
	}

	public static BufferedWriter createOutputWriter(String outputFileName) throws IOException {
		return new BufferedWriter(new FileWriter(outputFileName));
	}

	protected abstract void performFileOperation() throws IOException;

	public void performConversion() {
		try {
			performFileOperation();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
