package mt_pipeline.preAndPostProcessing;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import util.ConfigFile;
import util.FileUtil;
import util.LinuxInteractor;

public class LanguageModelDataPreparer {

	private static String COPRPORA_WEB_ADRESSES_PROPERTY = "corporaWebAdresses";
	private static String LANGUAGE_ABBREIATION_PROPERTY = "languageAbbreviation";
	private static String WORK_DIRECTORY_PATH_PROPERTY = "workDirectoryPath";

	private static String TRAINING_CORPUS_TARGET_FILE_PATH_PROPERTY = "trainingCorpusTargetFilePath";

	private static String SCRIPTS_DIR_PROPERTY = "scriptsDir";
	private static String DOWNLOAD_CORPORA_PROPERTY = "downloadCorpora";

	private static String TGZ_EXTENSION = ".tgz";
	private static String RESULT_FILE_PREFIX = "LanguageModelTrainingFile";

	// Web addresses to download the corpora files from
	private final List<String> corporaWebAdresses;
	// Abbreviation property, used to specify the language abbreviation to be
	// used to select the files of the right language automatically
	private final String languageAbbreviation;
	// The path to the folder were things will be downloaded and stored
	private final String workDirectoryPath;
	// The target side of the training corpus should also be included and this
	// path is for
	// specifying its location
	private final String trainingCorpusTargetFilePath;
	// String location of the scripts directory, needed for the tokenization
	private final String scriptsDir;
	// Boolean that can be set to false from the configuration if the corpora
	// are
	// already downloaded to skip the wget downloading step
	private final boolean downloadCorpora;

	private LanguageModelDataPreparer(List<String> corporaWebAdresses,
			String languageAbbreviation, String workDirectoryPath,
			String trainingCorpusTargetFilePath, String scriptsDir,
			boolean downloadCorpora) {
		this.corporaWebAdresses = corporaWebAdresses;
		this.languageAbbreviation = languageAbbreviation;
		this.workDirectoryPath = workDirectoryPath;
		this.trainingCorpusTargetFilePath = trainingCorpusTargetFilePath;
		this.scriptsDir = scriptsDir;
		this.downloadCorpora = downloadCorpora;
	}

	public static LanguageModelDataPreparer createLanguageModelDataPreparer(
			String configFilePath) {
		try {
			ConfigFile theConfig = new ConfigFile(configFilePath);
			List<String> corporaWebAdresses = theConfig
					.getMultiValueParmeterAsList(COPRPORA_WEB_ADRESSES_PROPERTY);
			String languageAbbreviation = theConfig
					.getValueWithPresenceCheck(LANGUAGE_ABBREIATION_PROPERTY);
			String workDirectoryPath = theConfig
					.getValueWithPresenceCheck(WORK_DIRECTORY_PATH_PROPERTY);
			String trainingCorpusTargetFilePath = theConfig
					.getValueWithPresenceCheck(TRAINING_CORPUS_TARGET_FILE_PATH_PROPERTY);
			String scriptsDir = theConfig
					.getValueWithPresenceCheck(SCRIPTS_DIR_PROPERTY);
			boolean downloadCorpora = theConfig
					.getBooleanIfPresentOrReturnDefault(
							DOWNLOAD_CORPORA_PROPERTY, true);
			return new LanguageModelDataPreparer(corporaWebAdresses,
					languageAbbreviation, workDirectoryPath,
					trainingCorpusTargetFilePath, scriptsDir, downloadCorpora);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	private void createWorkDirectory() {
		FileUtil.createFolderIfNotExisting(workDirectoryPath);
	}

	private String wgetCommand(String corpusWebAddress) {
		String result = "wget -P " + this.workDirectoryPath + " "
				+ corpusWebAddress;
		return result;
	}

	private void downloadCorpora() {
		for (String corpusWebAddress : corporaWebAdresses) {
			LinuxInteractor
					.executeExtendedCommandDisplayOutput(wgetCommand(corpusWebAddress));
		}
	}

	private String unpackCommand(String gzippedFilePath) {
		String result = "tar -zxvf " + gzippedFilePath + " -C "
				+ gzipFilePathBasedDirectoryName(gzippedFilePath);
		System.out.println("unpack command:  " + result);
		return result;
	}

	private String gzipFilePathBasedDirectoryName(String gzippedFilePath) {
		String result = gzippedFilePath.substring(0,
				gzippedFilePath.lastIndexOf(TGZ_EXTENSION));
		result += "/";
		return result;
	}

	private List<String> getTGZFileNamesInWorkDirectory() {
		try {
			List<String> result = new ArrayList<String>();
			File dir = new File(workDirectoryPath);
			for (File child : dir.listFiles()) {
				if (child.isFile()
						&& child.getCanonicalPath().endsWith(TGZ_EXTENSION)) {

					result.add(child.getCanonicalPath());
				}
			}
			return result;
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	private void unpackFileToFolderBasedOnNameWithoutTGZ(String gzippedFilePath) {
		FileUtil.createFolderIfNotExisting(gzipFilePathBasedDirectoryName(gzippedFilePath));
		LinuxInteractor
				.executeExtendedCommandDisplayOutput(unpackCommand(gzippedFilePath));
	}

	private void unpackGZIPFilesToIndividualFolders() {
		for (String gzipFilePath : getTGZFileNamesInWorkDirectory()) {
			unpackFileToFolderBasedOnNameWithoutTGZ(gzipFilePath);
		}
	}

	private List<String> getAllProperFilePathsInFolderRecursively(
			String folderPath) {
		List<String> result = new ArrayList<String>();
		List<File> filesList = FileUtil.getAllFilesInFolder(
				new File(folderPath), true);
		for (File file : filesList) {
			try {
				result.add(file.getCanonicalPath());
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
		return result;
	}

	private boolean isCorrectLanguageFilePath(String filePath) {
		String correctLanguageIndicatingFileString = "."
				+ this.languageAbbreviation;
		return filePath.contains(correctLanguageIndicatingFileString);
	}

	private List<String> getAllCorrectLanguageFilePathsInFolderRecursively(
			String folderPath) {
		List<String> result = new ArrayList<String>();
		for (String filePath : getAllProperFilePathsInFolderRecursively(folderPath)) {
			if (isCorrectLanguageFilePath(filePath)) {
				result.add(filePath);
			}
		}
		return result;
	}

	private List<String> getAllCorrectLanguageFilePathsFromAllSubfolders() {
		try {
			List<String> result = new ArrayList<String>();
			List<File> subfoldersList = FileUtil
					.getAllFoldersInFolder(new File(this.workDirectoryPath));
			for (File file : subfoldersList) {
				result.addAll(getAllCorrectLanguageFilePathsInFolderRecursively(file
						.getCanonicalPath()));
			}
			return result;
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	private String resultFilePrefix() {
		String result = workDirectoryPath + RESULT_FILE_PREFIX + "."
				+ languageAbbreviation;
		return result;
	}

	private String resultFilePath() {
		return resultFilePrefix() + ".txt";
	}

	private String resultFilePathTokenized() {
		return resultFilePrefix() + ".tok.txt";
	}

	private String resultFilePathTokenizedAndLowercased() {
		return resultFilePrefix() + ".tok.lowercased.txt";
	}

	private void createEmptyResultFile() {
		File resultFile = new File(resultFilePath());
		try {
			boolean result = resultFile.createNewFile();
			if (!result && !FileUtil.fileIsEmpty(resultFile)) {
				System.out.println("Error: result file " + resultFilePath()
						+ " already exists. Quiting");
				System.exit(0);
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	private void appendFileToResultFile(String resultFilePath,
			String appendedFilePath) {
		System.out.println("Appending " + appendedFilePath + " to result file "
				+ resultFilePath);
		FileUtil.appendFile(resultFilePath, appendedFilePath);
	}

	private void createAllCorrectLanguageFilesAppendedBigFile() {
		createEmptyResultFile();

		// Next we append the correct language files from our downloaded corpora
		for (String correctLanguageFilePath : getAllCorrectLanguageFilePathsFromAllSubfolders()) {
			appendFileToResultFile(resultFilePath(), correctLanguageFilePath);
		}
	}

	private void createTokenizedAndLowercasedResultFileFromBigFile() {
		// See:
		// http://stackoverflow.com/questions/525212/how-to-run-unix-shell-script-from-java-code
		// http://docs.oracle.com/javase/6/docs/api/java/lang/ProcessBuilder.html

		// LinuxInteractor.executeCommand(new String[]{getScriptsDir() +
		// "preprocess_data.sh", inputFileName, outputFileName, language},true);
		String tokenizationCommand = scriptsDir + "tokenizer.perl -l "
				+ this.languageAbbreviation + " < " + resultFilePath() + " > "
				+ resultFilePathTokenized();

		LinuxInteractor.executeCommand(tokenizationCommand, true);
		Lowercase.toLowercase(resultFilePathTokenized(),
				resultFilePathTokenizedAndLowercased());

	}

	private void createFinalResultFile() {
		createTokenizedAndLowercasedResultFileFromBigFile();
		// The basis of our result file is the contents of the training corpus
		// target file. This file is already lowercased and tokenized, so it is
		// appended
		// as a last step to the big tokenized and lowercased result file
		// created from all
		// other material
		appendFileToResultFile(resultFilePathTokenizedAndLowercased(),
				trainingCorpusTargetFilePath);
	}

	private void createLangguageModelBigInputFile() {
		createWorkDirectory();
		if (downloadCorpora) {
			downloadCorpora();
		}
		unpackGZIPFilesToIndividualFolders();
		createAllCorrectLanguageFilesAppendedBigFile();
		createFinalResultFile();
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			System.out
					.println("usage: languageModelDataPreparer CCONFIG_FILE_PATH");
			System.exit(0);
		}
		String configFilePath = args[0];
		LanguageModelDataPreparer languageModelDataPreparer = LanguageModelDataPreparer
				.createLanguageModelDataPreparer(configFilePath);
		languageModelDataPreparer.createLangguageModelBigInputFile();
	}

}
