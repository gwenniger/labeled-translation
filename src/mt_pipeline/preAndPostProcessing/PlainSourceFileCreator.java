package mt_pipeline.preAndPostProcessing;

import mt_pipeline.mt_config.MTConfigFile;

public class PlainSourceFileCreator implements EnrichedSourceFileCreator {

	public static final String SOURCE_ENRICHMENT_SCHEME_NAME = "plainSource";

	@Override
	public void createEnrichedSourceFile(MTConfigFile theConfig, String category) {

		String inputFileName = theConfig.taggerConfig.getEnrichmentPairerSourceInputFilePath(category);
		String outputFileName = theConfig.filesConfig.getEnrichedSourceFilePath(category);
		int numLinesToCopy = theConfig.decoderConfig.getDataLength(category);
		SubFileCreater.createSubFile(inputFileName, outputFileName, numLinesToCopy);
	}

}
