package mt_pipeline;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;

public abstract class GrammarExtracterCreater implements MultiThreadGrammarExtracterCreater {
	private final boolean writePlainHieroRules;
	protected final SystemIdentity systemIdentity;

	protected GrammarExtracterCreater(boolean writePlainHieroRules,SystemIdentity systemIdentity) {
		this.writePlainHieroRules = writePlainHieroRules;
		this.systemIdentity = systemIdentity;
	}

	@Override
	public boolean writePlainHieroRules() {
		return writePlainHieroRules;
	}
}
