package mt_pipeline.mtPipelineTest;

public class SourceAndTargetLanguageAbbreviations {
    private final String sourceLanguageAbbreviation;
    private final String targetLanguageAbbreviation;

    private SourceAndTargetLanguageAbbreviations(String sourceLanguageAbbreviation,
	    String targetLanguageAbbreviation) {
	this.sourceLanguageAbbreviation = sourceLanguageAbbreviation;
	this.targetLanguageAbbreviation = targetLanguageAbbreviation;
    }

    public static SourceAndTargetLanguageAbbreviations createSourceAndTargetLanguageAbbreviations(
	    String sourceLanguageAbbreviation, String targetLanguageAbbreviation) {
	return new SourceAndTargetLanguageAbbreviations(sourceLanguageAbbreviation,
		targetLanguageAbbreviation);
    }

    public String getSourceLanguageAbbreviation() {
	return sourceLanguageAbbreviation;
    }

    public String getTargetLanguageAbbreviation() {
	return targetLanguageAbbreviation;
    }

}
