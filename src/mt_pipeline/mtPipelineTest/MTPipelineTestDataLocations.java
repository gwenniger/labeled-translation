package mt_pipeline.mtPipelineTest;

import java.io.FileNotFoundException;
import java.util.List;

import util.ConfigFile;

public class MTPipelineTestDataLocations {

	private static final String TRAINING_DATA_FOLDER_NAME_PROPERT = "trainingDataFolderName";
	private static final String DEV_DATA_FOLDER_NAME_PROPERTY = "devDataFolderName";
	private static final String TEST_DATA_FOLDER_NAME_PROPERTY = "testDataFolderName";
	private static final String TRAIN_SOURCE_NAME_PROPERTY = "trainSourceName";
	private static final String TRAIN_TARGET_NAME_PROPERTY = "trainTargetName";
	private static final String TRAIN_SOURCE_NAME_CASED_PROPERTY = "trainSourceNameCased";
	private static final String TRAIN_TARGET_NAME_CASED_PROPERTY = "trainTargetNameCased";
	private static final String TESTS_OUTPUT_FOLDER_NAME_PROPERTY = "testOutputFolderName";
	private static final String TRAIN_ALIGNMENTS_NAME_PROPERTY = "trainAlignmentsName";
	private static final String DEV_SOURCE_NAME_PROPERTY = "devSourceName";
	private static final String DEV_TARGET_NAMES_PROPERTY = "devTargetNames";
	private static final String DEV_TARGET_NAME_PROPERTY = "devTargetName";
	private static final String TEST_SOURCE_NAME_PROPERTY = "testSourceName";
	private static final String TEST_TARGET_NAME_PROPERTY = "testTargetName";
	private static final String TEST_TARGET_NAMES_PROPERTY = "testTargetNames";
	private static final String LM_INPUT_FILE_PATH_PROPERTY = "lmInputFilePath";
	private static final String SourceLanguageAbbreviationProperty = "SourceLanguageAbbreviation";
	private static final String TargetLanguageAbbreviationProperty = "TargetLanguageAbbreviation";

	/*
	private static final String TRAINING_DATA_FOLDER_NAME = "mtPipelineTestSmallDataDeEn";
	private static final String DEV_TEST_DATA_FOLDER_NAME = "mtPipelineTestDevTestData";
	private static final String TRAIN_SOURCE_NAME = "aligned.0.de";
	private static final String TRAIN_TARGET_NAME = "aligned.0.en";
	private static final String TRAIN_SOURCE_NAME_CASED = "aligned.0.de.Cased";
	private static final String TRAIN_TARGET_NAME_CASED = "aligned.0.en.Cased";
	private static final String TESTS_OUTPUT_FOLDER_NAME = "mtPipelineTestsResults";
	private static final String TRAIN_ALIGNMENTS_NAME = "aligned.grow-diag-final-and";
	private static final String DEV_SOURCE_NAME = "test2006.de";
	private static final String DEV_TARGET_NAME = "test2006.en";
	private static final String TEST_SOURCE_NAME = "test2007.de";
	private static final String TEST_TARGET_NAME = "test2007.en";
    */
	
	private final ConfigFile theConfig;

	private MTPipelineTestDataLocations(ConfigFile theConfig) {
		this.theConfig = theConfig;
	}

	public static MTPipelineTestDataLocations createMTPipelineTestDataLocations(String configFilePath) {
		ConfigFile theConfig;
		try {
			System.out.println("MTPipelineTestDataLocations - configFilePath: " + configFilePath);
			theConfig = new ConfigFile(configFilePath);
			return new MTPipelineTestDataLocations(theConfig);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	public String getTrainingDataFolderName() {
		return this.theConfig.getValueWithPresenceCheck(TRAINING_DATA_FOLDER_NAME_PROPERT);
	}

	public String getDevDataFolderName() {
		return this.theConfig.getValueWithPresenceCheck(DEV_DATA_FOLDER_NAME_PROPERTY);
	}

	public String getTestDataFolderName() {
		return this.theConfig.getValueWithPresenceCheck(TEST_DATA_FOLDER_NAME_PROPERTY);
	}

	public String getTrainSourceName() {
		return this.theConfig.getValueWithPresenceCheck(TRAIN_SOURCE_NAME_PROPERTY);
	}

	public String getTrainTargetName() {
		return this.theConfig.getValueWithPresenceCheck(TRAIN_TARGET_NAME_PROPERTY);
	}

	public String getTrainSourceNameCased() {
		return this.theConfig.getValueWithPresenceCheck(TRAIN_SOURCE_NAME_CASED_PROPERTY);
	}

	public String getTrainTargetNameCased() {
		return this.theConfig.getValueWithPresenceCheck(TRAIN_TARGET_NAME_CASED_PROPERTY);
	}

	public String getTestOutputFolderName() {
		return this.theConfig.getValueWithPresenceCheck(TESTS_OUTPUT_FOLDER_NAME_PROPERTY);
	}

	public String getTrainAlignmentsName() {
		return this.theConfig.getValueWithPresenceCheck(TRAIN_ALIGNMENTS_NAME_PROPERTY);
	}

	public String getDevSourceName() {
		return this.theConfig.getValueWithPresenceCheck(DEV_SOURCE_NAME_PROPERTY);
	}

	public String getDevTargetName() {
		return this.theConfig.getValueWithPresenceCheck(DEV_TARGET_NAME_PROPERTY);
	}

	public String getTestSourceName() {
		return this.theConfig.getValueWithPresenceCheck(TEST_SOURCE_NAME_PROPERTY);
	}

	public String getTestTargetName() {
		return this.theConfig.getValueWithPresenceCheck(TEST_TARGET_NAME_PROPERTY);
	}
	
	public boolean hasDevTargetNames(){
	    return this.theConfig.hasValue(DEV_TARGET_NAMES_PROPERTY);
	}
	
	public List<String> getDevTargetNames(){
	    return this.theConfig.getMultiValueParmeterAsListWithPresenceCheck(DEV_TARGET_NAMES_PROPERTY);
	}
	
	public boolean hasTestTargetNames(){
	    return this.theConfig.hasValue(TEST_TARGET_NAMES_PROPERTY);
	}
	
	public List<String> getTestTargetNames(){
	    return this.theConfig.getMultiValueParmeterAsListWithPresenceCheck(TEST_TARGET_NAMES_PROPERTY);
	}

	public String getLMInputFile() {
		return this.theConfig.getValueWithPresenceCheck(LM_INPUT_FILE_PATH_PROPERTY);
	}
	
	public String getSourceLanguageAbbreviation()
	{
		return SourceLanguageAbbreviationProperty;
	}
	
	public String getTargetLanguageAbbreviation()
	{
		return TargetLanguageAbbreviationProperty;
	}
}
