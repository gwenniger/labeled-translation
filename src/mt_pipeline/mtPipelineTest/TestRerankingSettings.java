package mt_pipeline.mtPipelineTest;

import mt_pipeline.mt_config.MTDecoderConfig;

public class TestRerankingSettings {
	private static final int STANDARD_MAX_NUM_UNIQUE_TRANSLATIONS_FOR_NBEST_MBR = 300;
	private final String rerankingType;
	private final int maxNumUniqueTranslationsForNBestMBR;

	private TestRerankingSettings(String rerankingType, int maxNumUniqueTranslationsForNBestMBR) {
		this.rerankingType = rerankingType;
		this.maxNumUniqueTranslationsForNBestMBR = maxNumUniqueTranslationsForNBestMBR;
	}

	public static TestRerankingSettings createCrunchingTestRerankingSettings() {
		return new TestRerankingSettings(MTDecoderConfig.CRUNCHING_RERANKING, STANDARD_MAX_NUM_UNIQUE_TRANSLATIONS_FOR_NBEST_MBR);
	}

	public static TestRerankingSettings createNBestListMBRRerankingSettings() {
		return new TestRerankingSettings(MTDecoderConfig.MINIMUM_BAYES_RISK, STANDARD_MAX_NUM_UNIQUE_TRANSLATIONS_FOR_NBEST_MBR);
	}

	public static TestRerankingSettings createNoRerankingSettings() {
		return new TestRerankingSettings(MTDecoderConfig.NO_RERANKING, 0);
	}

	public String getRerankingType() {
		return this.rerankingType;
	}

	public int getMaxNumUniqueTranslationsForNBestMBR() {
		return this.maxNumUniqueTranslationsForNBestMBR;
	}

}
