package mt_pipeline.mtPipelineTest;

import grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagLabelingSettings;
import grammarExtraction.chiangGrammarExtraction.GrammarExtractionConstraints;
import grammarExtraction.chiangGrammarExtraction.LabelSideSettings;
import grammarExtraction.coreContextLabelGrammarExtraction.CCLRuleGapCreater;
import grammarExtraction.grammarExtractionFoundation.GrammarStructureSettings;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.labelSmoothing.LabeledRulesSmoother;
import grammarExtraction.reorderingLabeling.CoarseReorderingFeatureSet;
import grammarExtraction.reorderingLabeling.PhraseCentricPlusParentRelativeReorderingLabel;
import grammarExtraction.samtGrammarExtraction.SAMTGrammarExtractionConstraints;
import integration_tests.MTPipelineTest.TUNER_TYPE;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import junit.framework.Assert;
import mt_pipeline.GrammarCreaterHats;
import mt_pipeline.JoshuaSystem;
import mt_pipeline.MTPipeline;
import mt_pipeline.MosesSystem;
import mt_pipeline.mt_config.MTConfigFile;
import mt_pipeline.mt_config.MTFilesConfig;
import mt_pipeline.mt_config.MTFoldersConfig;
import mt_pipeline.mt_config.MTDecoderConfig;
import mt_pipeline.mt_config.MTGrammarExtractionConfig;
import mt_pipeline.mt_config.MTParserConfig;
import mt_pipeline.mt_config.MTTaggerConfig;
import mt_pipeline.parserInterface.ParserInterface;
import mt_pipeline.preAndPostProcessing.EnrichedSourceFileCreator;
import mt_pipeline.preAndPostProcessing.PlainSourceFileCreator;
import coreContextLabeling.EbitgChartBuilderCoreContextLabeled;
import datapreparation.SegmenterConfig;
import util.ConfigFile;
import util.Utility;

public class MTPipelineTestConfigFileCreater {

	private static final String NL = "\n";
	private static final String CODE_ROOT_DIR_PROPERTY = "codeRootDir";

	private static final String NULL = "NULL";
	private static final String FALSE = "false";
	private static final String TRUE = "true";
	private static final String MERT_GRAMMAR_NAME = "de-en-grammar-mert";
	private static final String TEST_GRAMMAR_NAME = "de-en-grammar";
	final static String TESTS_OUTPUT_FOLDER_NAME = "mtPipelineTestsResults";
	private static final int TUNING_STRATEGY_TYPE_PROPERTY_VALUE = 1;
	private static int LANGUAGE_MODEL_ORDER = 3;

	private final String resultFileName;

	private final String systemNames;
	private final String jdkHomDir;
	private final String scriptsDir;
	private final String joshuaHomeDir;
	private final String mosesHomeDir;
	private final String srilmBinDir;
	private final String charniakParserRootDir;
	private final String tntTaggerRootDir;
	private final String stanfordSegmenterRootDir;
	private final String universalTagSetMapsDir;
	private final String codeRootDir;
	private final String rootOutputDir;
	private final TestLabelingSettings testLabelingSettings;
	private final boolean usePackedGrammars;
	private final String tunerTypeString;
	private final boolean parseTargetFile;
	private final int trainDataLength;
	private final MTPipelineTestDataLocations mtPipelineTestDataLocations;
	private final String mtSystemTypeProperty;
	private final boolean writePlainHieroRulesForSmoothing;
	private final String labelSmoothingTypeProperty;
	private final TestRerankingSettings testRerankingSettings;
	private final boolean useSoftSyntacticConstraintDecoding;
	private final boolean useLabelSubstitutionFeatures;
	private final boolean use_rule_application_features;
	private final SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations;
	private final boolean performTuning;
	private final boolean resumeTuning = false;

	private MTPipelineTestConfigFileCreater(String resultFileName, String systemNames, String jdkHomDir, String scriptsDir, String joshuaHomeDir,
			String mosesHomeDir, String srilmBinDir, String charniakParserRootDir, String tntTaggerRootDir,String stanfordSegmenterRootDir, String universalTagSetMapsDir, String codeRootDir,
			String rootOutputDir, String tunerTypeString, boolean parseTargetFile, int trainDataLength,
			MTPipelineTestDataLocations mtPipelineTestDataLocations, boolean usePackedGrammars, String mtSystemTypeProperty,
			boolean writePlainHieroRulesForSmoothing, String labelSmoothingTypeProperty, TestLabelingSettings testReorderingLabelingSettings,
			TestRerankingSettings teRerankingSettings,
			 boolean useSoftSyntacticConstraintDecoding,boolean useLabelSubstitutionFeatures,
			 SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations,
			 boolean performTuning,boolean use_rule_application_features) {
		this.systemNames = systemNames;
		this.jdkHomDir = jdkHomDir;
		this.scriptsDir = scriptsDir;
		this.joshuaHomeDir = joshuaHomeDir;
		this.mosesHomeDir = mosesHomeDir;
		this.srilmBinDir = srilmBinDir;
		this.charniakParserRootDir = charniakParserRootDir;
		this.tntTaggerRootDir = tntTaggerRootDir;
		this.stanfordSegmenterRootDir = stanfordSegmenterRootDir;
		this.universalTagSetMapsDir = universalTagSetMapsDir;
		this.codeRootDir = codeRootDir;
		this.resultFileName = resultFileName;
		this.rootOutputDir = rootOutputDir;
		this.testLabelingSettings = testReorderingLabelingSettings;
		this.tunerTypeString = tunerTypeString;
		this.parseTargetFile = parseTargetFile;
		this.trainDataLength = trainDataLength;
		this.mtPipelineTestDataLocations = mtPipelineTestDataLocations;
		this.usePackedGrammars = usePackedGrammars;
		this.mtSystemTypeProperty = mtSystemTypeProperty;
		this.writePlainHieroRulesForSmoothing = writePlainHieroRulesForSmoothing;
		this.labelSmoothingTypeProperty = labelSmoothingTypeProperty;
		this.testRerankingSettings = teRerankingSettings;
		this.useSoftSyntacticConstraintDecoding = useSoftSyntacticConstraintDecoding;
		this.useLabelSubstitutionFeatures = useLabelSubstitutionFeatures;
		this.sourceAndTargetLanguageAbbreviations = sourceAndTargetLanguageAbbreviations;
		this.performTuning = performTuning;
		this.use_rule_application_features = use_rule_application_features;
	}

	public static String getJDKHomeDir(ConfigFile configFile) {
		return configFile.getValueWithPresenceCheck(MTConfigFile.JDK_HOME_DIR_PROPERTY);
	}

	public static String getScriptsDir(ConfigFile configFile) {
		return configFile.getValueWithPresenceCheck(MTFoldersConfig.SCRIPT_SDIR_PROPERTY);
	}

	public static String getJoshuaHomeDir(ConfigFile configFile) {
		return configFile.getValueWithPresenceCheck(MTDecoderConfig.JOSHUA_HOME_DIR_PROPERTY);
	}

	public static String getMosesHomeDir(ConfigFile configFile) {
		return configFile.getValueWithPresenceCheck(MTDecoderConfig.MOSES_HOME_DIR_PROPERTY);
	}

	public static String getSrilmBinDir(ConfigFile configFile) {
		return configFile.getValueWithPresenceCheck(MTFoldersConfig.SRILM_BIN_DIR_PROPERTY);
	}

	public static String getCharniakParserRootDir(ConfigFile configFile) {
		return configFile.getValueWithPresenceCheck(MTParserConfig.CHRANIAK_PARSER_ROOT_DIR_PROPERTY);
	}

	public static String getTnTTaggerRootDir(ConfigFile configFile) {
		return configFile.getValueWithPresenceCheck(MTTaggerConfig.TNT_TAGGER_ROOT_DIR_PROPERTY);
	}
	
	public static String getStanfordSegmenterRootDir(ConfigFile configFile) {
		return configFile.getValueWithPresenceCheck(SegmenterConfig.STANFORD_SEGMENTER_ROOT_DIR_LOCATION);
	}

	public static String getUniversalTagSetsMapDir(ConfigFile configFile) {
		return configFile.getValueWithPresenceCheck(MTTaggerConfig.UNIVERSAL_TAGSET_MAPS_DIR_PROPERTY);
	}

	public static String getCodeRootDir(ConfigFile configFile) {
		return configFile.getValueWithPresenceCheck(CODE_ROOT_DIR_PROPERTY);
	}

	public void writeExperimentsConfigFile() {
		try {
			BufferedWriter fileWriter = new BufferedWriter(new FileWriter(this.resultFileName));
			writePlatformSpecificSettings(fileWriter);
			writeGeneralTestSettings(fileWriter);
			writeSAMTTestSettings(fileWriter);
			writeDataLocationSettings(fileWriter);
			writeParseConfiguration(fileWriter);
			writeDevTestDataLocations(fileWriter);
			writeTaggerConfiguration(fileWriter);
			writeSegmenterConfiguration(fileWriter);
			writeSmoothingConfiguration(fileWriter);
			writeCoreContextLabelConfiguration(fileWriter);
			writeRunSpecificationConfiguration(fileWriter);

			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void writePlatformSpecificSettings(BufferedWriter fileWriter) throws IOException {
		fileWriter.write("% Platform specific seetings" + NL);
		fileWriter.write(propertyValueString(MTConfigFile.SYSTEM_NAMES_PROPERTY, systemNames));
		fileWriter.write(propertyValueString(MTConfigFile.MT_SYSTEM_TYPE_PROPERTY, mtSystemTypeProperty));
		fileWriter.write(propertyValueString(MTConfigFile.JDK_HOME_DIR_PROPERTY, jdkHomDir));
		fileWriter.write("% The location were the scripts are located" + NL);
		fileWriter.write(propertyValueString(MTFoldersConfig.SCRIPT_SDIR_PROPERTY, scriptsDir));
		fileWriter.write(propertyValueString(MTDecoderConfig.JOSHUA_HOME_DIR_PROPERTY, joshuaHomeDir));
		fileWriter.write(propertyValueString(MTDecoderConfig.MOSES_HOME_DIR_PROPERTY, mosesHomeDir));
		fileWriter.write(propertyValueString(MTFoldersConfig.SRILM_BIN_DIR_PROPERTY, srilmBinDir));
		fileWriter.write(propertyValueString(MTParserConfig.CHRANIAK_PARSER_ROOT_DIR_PROPERTY, charniakParserRootDir));
		fileWriter.write(propertyValueString((SegmenterConfig.STANFORD_SEGMENTER_ROOT_DIR_LOCATION), stanfordSegmenterRootDir));
		fileWriter.write(propertyValueString((SegmenterConfig.NUM_SEGMENTER_THREADS_PROPERTY), "" + 4));
		
	}
	
	
	private String getLanguageModelName()
	{
	    if(LANGUAGE_MODEL_ORDER <= 1){
		return MTDecoderConfig.BERKELEYLM_NAME;
	    }
	    else{
		return MTDecoderConfig.KENLM_NAME;
	    }
	}
	

	private final String getExecutionString()
	{
	    if(resumeTuning){
		return MTPipeline.EXECUTE_MERT_AND_EVALUATION;
	    }
	    else{
		return MTPipeline.EXECUTE_EVERYTHING_STRING;		
	    }
	}
	
	
	private void writeGeneralTestSettings(BufferedWriter fileWriter) throws IOException {
		fileWriter.write(propertyValueString(MTPipeline.EXECUTION_SCHEME_PROPERTY, getExecutionString()) + NL);
		fileWriter.write("% Max memory usage for external java programs that are called" + NL + "Xmx = 6000m" + NL);
		fileWriter.write(propertyValueString(MTDecoderConfig.NO_MERT_ITERATIONS_PROPERTY, "1"));
		
		if(resumeTuning){
		    fileWriter.write(propertyValueString(MTDecoderConfig.RESTART_TUNING_FROM_LAST_COMPLETED_ITERATION_FLAG, "true"));
		}
		
		fileWriter.write(propertyValueString(MTDecoderConfig.CUBE_PRUNING_POP_LIMIT_PROPERTY, "300"));
		fileWriter.write(propertyValueString(MTDecoderConfig.NUM_TRANSLATION_OPTIONS_PROPERTY, "0"));
		fileWriter.write(propertyValueString(MTDecoderConfig.LANGUAGE_MODEL_NAME_PROPERTY, "" +getLanguageModelName()));
		fileWriter.write(propertyValueString(MTDecoderConfig.LANGUAGE_MODEL_ORDER_PROPERTY, "" +LANGUAGE_MODEL_ORDER));
		fileWriter.write(propertyValueString(SystemIdentity.MAX_SOURCE_AND_TARGET_LENGTH_PROPERTY, ""+SystemIdentity.STANDARD_MAX_SOURCE_AND_TARGET_LENGTH));
		fileWriter.write(propertyValueString(MTDecoderConfig.NUM_PARALLEL_DECODERS_PROPERTY, "1"));
		fileWriter.write(propertyValueString(MTDecoderConfig.NUM_TUNER_THREADS_PROPERTY, "1"));
		fileWriter.write(propertyValueString(MTConfigFile.PERFORM_TUNING_PROPERTY, ""+performTuning));
		fileWriter.write(propertyValueString(MTDecoderConfig.LOG_CPU_COMPUTATION_TIMES_PROPERTY, TRUE));
		
		
	}

	private String getLMInputFile() {
		return mtPipelineTestDataLocations.getLMInputFile();
	}

	public String getTrainingDataFolderPath() {
		return this.codeRootDir + mtPipelineTestDataLocations.getTrainingDataFolderName() + "/";
	}

	private String getDevDataFolderPath() {
		return this.codeRootDir + mtPipelineTestDataLocations.getDevDataFolderName() + "/";
	}

	private String getTestDataFolderPath() {
		return this.codeRootDir + mtPipelineTestDataLocations.getTestDataFolderName() + "/";
	}

	public String getRootOutputFolderPath() {
		return rootOutputDir;
	}

	private String propertyValueString(String property, String value) {
		return property + " = " + value + NL;
	}

	private void writeDataLocationSettings(BufferedWriter fileWriter) throws IOException {
		fileWriter.write("% The text file used to train the language model from" + NL);
		fileWriter.write(propertyValueString(MTFilesConfig.LM_INPUT_FILE_PROPERTY, getLMInputFile()));
		fileWriter.write(propertyValueString(MTFilesConfig.SOURCE_LANGUAGE_ABBREVIATION_PROPERTY, sourceAndTargetLanguageAbbreviations.getSourceLanguageAbbreviation()));
		fileWriter.write(propertyValueString(MTFilesConfig.TARGET_LANGUAGE_ABBREVIATION_PROPERTY, sourceAndTargetLanguageAbbreviations.getTargetLanguageAbbreviation())
				+ NL);
		fileWriter.write("% The folder were Joshua puts the suffixArray and associated files" + NL);
		fileWriter.write(propertyValueString(MTFoldersConfig.JOSHUA_SUFFIX_ARRAY_FOLDER_NAME_PROPERTY, "de-en-suffixArray.josh"));
		fileWriter.write(propertyValueString(MTFilesConfig.FILTERED_GRAMMAR_NAME_TEST_PROPERTY, TEST_GRAMMAR_NAME));
		fileWriter.write(propertyValueString(MTFilesConfig.FILTERED_GRAMMAR_NAME_MERT_PROPERTY, MERT_GRAMMAR_NAME));
		fileWriter.write(propertyValueString(MTFilesConfig.GLUE_GRAMMAR_NAME_TEST_PROPERTY, "glue-grammar-test"));
		fileWriter.write(propertyValueString(MTFilesConfig.GLUE_GRAMMAR_NAME_MERT_PROPERTY, "glue-grammar-mert"));
		fileWriter.write("% Where the train Data input is located" + NL);
		fileWriter.write(propertyValueString("trainDataInputDirectory", getTrainingDataFolderPath()));
		fileWriter.write("% Where the generated data is put" + NL + propertyValueString(MTFoldersConfig.ROOT_OUTPUT_FOLDER, getRootOutputFolderPath()));
		fileWriter.write("% Name of the train source file" + NL);
		fileWriter.write(propertyValueString(MTFilesConfig.getSourceInputFileNameProperty(MTConfigFile.TRAIN_CATEGORY),
				mtPipelineTestDataLocations.getTrainSourceName()));
		fileWriter.write("% Name of the cased version of train source file" + NL);
		fileWriter.write(propertyValueString(MTFilesConfig.getSourceCasedTrainFileNameProperty(), mtPipelineTestDataLocations.getTrainSourceNameCased()));
		fileWriter.write("% Name of the train target file" + NL);
		fileWriter.write(propertyValueString(MTFilesConfig.getTargetInputFileNameProperty(MTConfigFile.TRAIN_CATEGORY),
				mtPipelineTestDataLocations.getTrainTargetName()));
		fileWriter.write("% Name of the cased version of train target file" + NL);
		fileWriter.write(propertyValueString(MTFilesConfig.getTargetCasedTrainFileNameProperty(), mtPipelineTestDataLocations.getTrainTargetNameCased()));
		fileWriter.write("% Name of the train alignments file" + NL);
		fileWriter.write(propertyValueString("trainAlignmentsName", mtPipelineTestDataLocations.getTrainAlignmentsName()));
		fileWriter.write("% Length of the train data to be generated" + NL);
		fileWriter.write(propertyValueString("trainDataLength", trainDataLength + ""));

	}

	private void writeSAMTTestSettings(BufferedWriter fileWriter) throws IOException {
    	    	fileWriter.write(propertyValueString(SAMTGrammarExtractionConstraints.USE_EXTRA_SAMT_FEATURES_PROPERTY, TRUE));    
	    	if(!useSoftSyntacticConstraintDecoding){
	    	    fileWriter.write(propertyValueString(SAMTGrammarExtractionConstraints.USE_NON_UNINIFORM_GLUE_RULE_WEIGHTS_PROPERTY, TRUE));
	    	}else{
	    	fileWriter.write(propertyValueString(SAMTGrammarExtractionConstraints.USE_NON_UNINIFORM_GLUE_RULE_WEIGHTS_PROPERTY, FALSE));
	    	}
		fileWriter.write(propertyValueString(GrammarExtractionConstraints.ALLOW_DANGLING_SECONDNOND_TERMINAL_PROPERTY, TRUE));
		fileWriter.write(propertyValueString(GrammarExtractionConstraints.ALLOW_CONSECUTIVE_NONTERMINALS_IN_NON_ABSTRACTRULES_PROPERTY, FALSE));
		fileWriter.write(propertyValueString(GrammarExtractionConstraints.ALLOW_SOURCE_ABSTRACT_PROPERTY, FALSE));
		fileWriter.write(propertyValueString(GrammarExtractionConstraints.ALLOW_TARGET_WORDS_WITHOUT_SOURCE_PROPERTY, FALSE));
		fileWriter.write(propertyValueString(GrammarExtractionConstraints.ALLOW_DOUBLE_PLUS_PROPERTY, TRUE));
		fileWriter.write(propertyValueString(GrammarExtractionConstraints.UNARY_CATEGORY_HANDLER_TYPE_PROPERTY, "all"));
		fileWriter.write(propertyValueString(GrammarExtractionConstraints.ALLOW_POS_TAG_LABEL_FALLBACK_PROPERTY, FALSE));
	}

	private void writeParseConfiguration(BufferedWriter fileWriter) throws IOException {
		fileWriter.write("% Parse configuration for SAAMT" + NL);
		fileWriter.write(propertyValueString(MTParserConfig.PARSER_NAME_PROPERTY, "CharniakParser"));
		fileWriter.write(propertyValueString(MTParserConfig.CHRANIAK_PARSER_ROOT_DIR_PROPERTY, charniakParserRootDir));
		fileWriter.write(propertyValueString(MTParserConfig.PARSE_TARGET_FILE_PROPERTY, new Boolean(this.parseTargetFile).toString()));
		fileWriter.write(propertyValueString(MTParserConfig.NUM_PARSER_PROCESSES, "4"));
		fileWriter.write(propertyValueString(MTDecoderConfig.NUM_PARALLEL_GRAMMAR_EXTRACTORS_PROPERTY, "1"));
	}

	private String getDataInputDirectory(String category) {
		if (category.equals(MTConfigFile.DEV_CATEGORY)) {
			return getDevDataFolderPath();
		} else if (category.equals(MTConfigFile.TEST_CATEGORY)) {
			return getTestDataFolderPath();
		} else {
			throw new RuntimeException("Error : wrong category - expected : " + MTConfigFile.DEV_CATEGORY + " or " + MTConfigFile.TEST_CATEGORY);
		}
	}

	private void writeDataLocations1(BufferedWriter fileWriter, String category, String sourceName,String typeSetAndDocID)
		throws IOException {
	    fileWriter.write("% Where the " + category + " data is located" + NL);
	    fileWriter.write(propertyValueString(category + "DataInputDirectory", getDataInputDirectory(category)));
	    fileWriter.write("% Name of the " + category + " source file" + NL);
	    fileWriter.write(propertyValueString(MTFilesConfig.getSourceInputFileNameProperty(category), sourceName));
	}
	
	private void writeDataLocations2(BufferedWriter fileWriter, String category, String sourceName,String typeSetAndDocID)
		throws IOException {
	    fileWriter.write(" % Length of the " + category + " data to be generated" + NL);
	    fileWriter.write(propertyValueString(MTDecoderConfig.getDataLengthProperty(category), "10"));
	    fileWriter.write("% The setID/docID of the " + category + "set - to be used in the gml file production" + NL);
	    fileWriter.write(propertyValueString(category + "SetAndDocID", typeSetAndDocID));
	}
	
	private void writeDataLocations(BufferedWriter fileWriter, String category, String sourceName, String targetName, String typeSetAndDocID)
			throws IOException {
		writeDataLocations1(fileWriter, category, sourceName,typeSetAndDocID);
	    	fileWriter.write("% Name of the " + category + " target file" + NL);
		fileWriter.write(propertyValueString(MTFilesConfig.getTargetInputFileNameProperty(category), targetName));
		writeDataLocations2(fileWriter, category, sourceName, typeSetAndDocID);
	}
	
	private void writeDataLocationsMultipleTargetFiles(BufferedWriter fileWriter, String category, String sourceName, List<String> targetNames, String typeSetAndDocID)
		throws IOException {
	writeDataLocations1(fileWriter, category, sourceName,  typeSetAndDocID);
    	fileWriter.write("% Name of the " + category + " target file" + NL);
    	String targetNamesString = "";
    	targetNamesString = Utility.stringListStringWithoutBracketsWithSpecifiedSeparator(targetNames, ",");
	fileWriter.write(propertyValueString(MTFilesConfig.getTargetInputFileNamesProperty(category), targetNamesString));
	writeDataLocations2(fileWriter, category, sourceName, typeSetAndDocID);
}

	private void writeDevTestDataLocations(BufferedWriter fileWriter) throws IOException {
	    	if(mtPipelineTestDataLocations.hasDevTargetNames()){
		    writeDataLocationsMultipleTargetFiles(fileWriter, MTConfigFile.DEV_CATEGORY, mtPipelineTestDataLocations.getDevSourceName(),
				mtPipelineTestDataLocations.getDevTargetNames(), "test2006");
		}
	    	else{  
	    	    writeDataLocations(fileWriter, MTConfigFile.DEV_CATEGORY, mtPipelineTestDataLocations.getDevSourceName(),
	    		mtPipelineTestDataLocations.getDevTargetName(), "test2006");
	    	}	    
				
		if(mtPipelineTestDataLocations.hasTestTargetNames()){
		    writeDataLocationsMultipleTargetFiles(fileWriter, MTConfigFile.TEST_CATEGORY, mtPipelineTestDataLocations.getTestSourceName(),
				mtPipelineTestDataLocations.getTestTargetNames(), "test2007");
		}
		else{    
        		writeDataLocations(fileWriter, MTConfigFile.TEST_CATEGORY, mtPipelineTestDataLocations.getTestSourceName(),
        				mtPipelineTestDataLocations.getTestTargetName(), "test2007");
		}	
	}

	private void writeTaggerConfiguration(BufferedWriter fileWriter) throws IOException {
		fileWriter.write("% Tagger configuration" + NL);
		fileWriter.write(propertyValueString(MTTaggerConfig.TAG_SOURCE_FILE_PROPERTY, TRUE));
		fileWriter.write(propertyValueString(MTTaggerConfig.TAG_TARGET_FILE_PROPERTY, TRUE));
		fileWriter.write(propertyValueString(MTTaggerConfig.TARGET_TAGGER_MODEL_NAME_PROPERTY, TRUE));
		fileWriter.write(propertyValueString(MTTaggerConfig.TNT_TAGGER_ROOT_DIR_PROPERTY, tntTaggerRootDir));
		fileWriter
				.write(propertyValueString(EnrichedSourceFileCreator.SOURCE_ENRICHMENT_SCHEME_PROPERTY, PlainSourceFileCreator.SOURCE_ENRICHMENT_SCHEME_NAME));
		fileWriter.write(propertyValueString(MTTaggerConfig.USE_UNIVERSAL_TAGSET_FOR_TAGGING_SOURCE_PROPERTY, TRUE));
		fileWriter.write(propertyValueString(MTTaggerConfig.USE_UNIVERSAL_TAGSET_FOR_TAGGING_TARGET_PROPERTY, TRUE));
		fileWriter.write(propertyValueString(MTTaggerConfig.UNIVERSAL_TAGSET_MAPS_DIR_PROPERTY, universalTagSetMapsDir));
	}
	
	private void writeSegmenterConfiguration(BufferedWriter fileWriter) throws IOException {
	    fileWriter.write(propertyValueString(SegmenterConfig.SEGMENTATION_MODEL_SPECIFICATION_STRING_POPERTY, SegmenterConfig.CHINESE_TREEBANK_SEGMENTATION_MODEL_STRING));
	}

	private void writeCoreContextLabelConfiguration(BufferedWriter fileWriter) throws IOException {
		fileWriter.write(propertyValueString(EbitgChartBuilderCoreContextLabeled.CoreContextLabelTableFileNameConfigurationProperty, NULL));
		fileWriter.write(propertyValueString(CCLRuleGapCreater.MaxiumAllowedCoreContextLabelAmbituityRankProperty, NULL));
	}
	
	
	private void writeRunSpecificationConfiguration(BufferedWriter fileWriter) throws IOException {
	    fileWriter.write(propertyValueString(MTFoldersConfig.SPECIFIC_RUN_SUBFOLDER_NAME_PROPERTY, "SomeExperiment/run-1"));
	}
	

	private BoundaryTagLabelingSettings getBoundaryTagLabelingSettings(){
	    return this.testLabelingSettings.getBoundaryTagLabelingSettings();
	}
	
	private void writeSmoothingConfiguration(BufferedWriter fileWriter) throws IOException {
		fileWriter.write("%Smoothing configuration" + NL);
		fileWriter.write(propertyValueString(GrammarCreaterHats.USE_WORD_ENRICHMENT_SMOOTHING_GRAMMAR_FEATURES_PARAMETER, FALSE));
		fileWriter.write(propertyValueString(BoundaryTagLabelingSettings.SOURCE_INSIDE_TAGS_TYPE_PROPERTY, getBoundaryTagLabelingSettings().getSourceInsideTagsTagSidesString()));
		fileWriter.write(propertyValueString(BoundaryTagLabelingSettings.SOURCE_OUTSIDE_TAGS_TYPE_PROPERTY, getBoundaryTagLabelingSettings().getSourceOutsideTagsTagSidesString()));
		fileWriter.write(propertyValueString(BoundaryTagLabelingSettings.TARGET_INSIDE_TAGS_TYPE_PROPERTY, getBoundaryTagLabelingSettings().getTargetInsideTagsTagSidesString()));
		fileWriter.write(propertyValueString(BoundaryTagLabelingSettings.TARGET_OUTSIDE_TAGS_TYPE_PROPERTY, getBoundaryTagLabelingSettings().getTargetOutsideTagsTagSidesString()));
		fileWriter.write("%Locations of the taggers and tagger universal mapping maps" + NL);
		fileWriter.write(propertyValueString(MTTaggerConfig.SOURCE_TO_UNIVERSAL_TAGS_MAPPER_NAME_PROPERTY, "de-tiger.map"));
		fileWriter.write(propertyValueString(MTTaggerConfig.SOURCE_TAGGER_MODEL_NAME_PROPERTY, "negra.tnt"));
		fileWriter.write(propertyValueString(MTTaggerConfig.TARGET_TO_UNIVERSAL_TAGS_MAPPER_NAME_PROPERTY, "en-ptb.map"));
		fileWriter.write(propertyValueString(MTTaggerConfig.TARGET_TAGGER_MODEL_NAME_PROPERTY, "wsj.tnt"));
		fileWriter.write("% Label smoothing setting" + NL);
		fileWriter.write(propertyValueString(LabeledRulesSmoother.LABEL_SMOOTHING_TYPE_PROPERTY, this.labelSmoothingTypeProperty));
		fileWriter.write(propertyValueString(MTGrammarExtractionConfig.USE_SOURCE_SIDE_LABELS_IN_RULE_FILTERING_CONFIG_PROPERTY, FALSE));
		fileWriter.write(propertyValueString(GrammarStructureSettings.WRITE_PLAIN_HIERO_RULES_FOR_SMOOTHING,
				(new Boolean(writePlainHieroRulesForSmoothing)).toString()));
		
		if (writePlainHieroRulesForSmoothing) {
			// fileWriter.write(propertyValueString(GrammarStructureSettings.USE_SEPARATE_FEATURE_FOR_PLAIN_HIERO_RULES_FOR_SMOOTHING,
			// (new
			// Boolean(true)).toString()));
			fileWriter.write(propertyValueString(GrammarStructureSettings.USE_SEPARATE_FEATURE_FOR_PLAIN_HIERO_RULES_FOR_SMOOTHING,
					(new Boolean(false)).toString()));
		}

		fileWriter.write(propertyValueString(GrammarExtractionConstraints.USE_REORDERING_LABEL_EXTENSION_PROPERTY,
				new Boolean(testLabelingSettings.useReorderingLabelExtension()).toString()));

		fileWriter.write(propertyValueString(GrammarExtractionConstraints.REORDER_LABEL_HIERO_RULES_PROPERTY,
				new Boolean(testLabelingSettings.reorderLabelHieroRules()).toString()));
		fileWriter.write(propertyValueString(GrammarExtractionConstraints.REORDER_LABEL_PHRASE_PAIRS_PROPERTY,
				new Boolean(testLabelingSettings.reorderLabelPhrasePairs()).toString()));
		fileWriter.write(propertyValueString(GrammarExtractionConstraints.USE_REORDERING_LABEL_BINARY_FEATURES_PROPERTY, new Boolean(
				testLabelingSettings.useReorderingLabelingBinaryFeatures()).toString()));
		fileWriter.write(propertyValueString(GrammarExtractionConstraints.REORDERING_FEATURE_SET_TYPE_PROPERTY,
				CoarseReorderingFeatureSet.COARSE_REORDERING_FEATURES_SET_NAME));
		fileWriter.write(propertyValueString(GrammarStructureSettings.USE_PACKED_GRAMMARS_PROPERTY, new Boolean(usePackedGrammars).toString()));
		fileWriter.write(propertyValueString(MTDecoderConfig.TUNING_STRATEGY_MODE_PROPERTY, "" + TUNING_STRATEGY_TYPE_PROPERTY_VALUE));
		fileWriter.write(propertyValueString(GrammarExtractionConstraints.REORDER_LABEL_TYPE_PROPERTY,
				testLabelingSettings.getReorderLabelTypeProperty()));
		if(isDoubleReorderingLabel()){
		    fileWriter.write(propertyValueString(MTDecoderConfig.USE_LABEL_SPLITTING_SMOOTHING_PROPERTY_FOR_LABEL_SUBSTITUTION_FEATURES,
				"false"));
		    fileWriter.write(propertyValueString(MTDecoderConfig.USE_LABEL_SPLITTING_SMOOTHING_WITH_ONLY_LABEL_SUBSTITUTION_FEATURES_FOR_SPLITTED_LABELS_PROPERTY,
				"true"));
		}
		fileWriter.write(propertyValueString(MTGrammarExtractionConfig.USE_CANONICAL_FORM_LABELED_RULES_PROPERTY, new Boolean(testLabelingSettings.outputOnlyCanonicalFormLabeledRules()).toString()));
		fileWriter.write(propertyValueString(MTGrammarExtractionConfig.USE_ONLY_HIERO_WEIGHTS_FOR_CANONICAL_FORM_LABELED_RULES_PROPERTY, new Boolean(testLabelingSettings.useOnlyHieroWeightsForCanonicalFormLabeledRules()).toString()));
		fileWriter.write(propertyValueString(MTGrammarExtractionConfig.RESTRICT_LABEL_VARIATION_FOR_CANONICAL_FORM_LABELED_RULES_PROPERTY, new Boolean(testLabelingSettings.restrictLabelVariationForCanonicalFormLabeledRules()).toString()));
		fileWriter.write(propertyValueString(MTGrammarExtractionConfig.NUM_ALLOWED_TYPES_PER_LABEL_FOR_SOURCE_PROPERTY, new Integer(testLabelingSettings.numAllowedTypesPerLabelForSource()).toString()));
		fileWriter.write(propertyValueString(LabeledRulesSmoother.REORDERING_LABEL_RULE_SMOOTHING_PROPERTY, FALSE));
		fileWriter.write(propertyValueString(LabeledRulesSmoother.REORDERING_LABEL_RULE_SMOOTHING_RULE_WEIGHT_PROPERTY, "0.001"));
		fileWriter.write(propertyValueString(LabeledRulesSmoother.STRIP_SOURCE_PART_LABELS_FOR_SMOOTHING_DOUBLE_LABELD_RULES, TRUE));
		fileWriter.write(propertyValueString(MTDecoderConfig.TUNER_TYPE_PROPERTY, tunerTypeString));
		fileWriter.write(propertyValueString(MTDecoderConfig.RERANKING_PROPERTY, testRerankingSettings.getRerankingType()));
		fileWriter.write(propertyValueString(MTDecoderConfig.NBEST_SIZE_FOR_NBEST_MBR_PROPERTY,
				"" + testRerankingSettings.getMaxNumUniqueTranslationsForNBestMBR()));
		fileWriter.write(propertyValueString(MTDecoderConfig.MIRA_REGULARIZATION_PARAMETER_PROPERTY,"0.001"));
		fileWriter.write(propertyValueString(LabelSideSettings.LABEL_SIDE_SETTING_PROPERTY, this.testLabelingSettings.getLabelSideSetting()));
		fileWriter.write(propertyValueString(MTDecoderConfig.SUM_DERIVATIONS_WITH_SAME_YIELD_FOR_NBEST_MBR_PARAMETER, TRUE));
		fileWriter.write(propertyValueString(MTDecoderConfig.MAX_NUM_UNIQUE_TRANSLATIONS_FOR_NBEST_MBR_PROPERTY, "-1"));
		fileWriter.write(propertyValueString(MTDecoderConfig.DISCRIMINATIVE_FEATURE_INITIAL_WEIGHT_PROPERTY, "0.003"));
		fileWriter.write(propertyValueString(MTDecoderConfig.TUNING_INTERPOLATION_COEFFICIENT_PROPERTY, "0.5"));
		fileWriter.write(propertyValueString(GrammarExtractionConstraints.USE_MOSES_HIERO_UNKNOWN_WORD_RULE_VARIANTS_PROPERTY, FALSE));
		
		
		fileWriter.write(propertyValueString(MTDecoderConfig.USE_DOT_CHART_PROPERTY_STRING, TRUE));
		
		if(this.useSoftSyntacticConstraintDecoding){
			fileWriter.write(propertyValueString(MTDecoderConfig.FUZZY_MATCHING_PROPERTY_NAME, TRUE));
			fileWriter.write(propertyValueString(MTDecoderConfig.REMOVE_LABELS_INSIDE_GRAMMAR_TRIE_FOR_MORE_EFFICIENT_FUZZY_MATCHING_PROPERTY_NAME, FALSE));
			fileWriter.write(propertyValueString(MTDecoderConfig.EXPLORE_ALL_DISTINCT_LABLED_RULE_VERSIONS_IN_CUBE_PRUNING_INITIALIZATION_PROPERTY_NAME, FALSE));
			fileWriter.write(propertyValueString(MTDecoderConfig.USE_SHUFFLING_TO_RANDOMIZE_RULE_ORDER_OF_ADDING_RULES_TO_INITIAL_CUBE_PRUNING_QUEUE_PROPERTY_NAME, TRUE));
			fileWriter.write(propertyValueString(MTDecoderConfig.EXPLORE_ALL_POSSIBLE_LABEL_SUBSTITUTIONS_FOR_ALL_RULES_IN_CUBE_PRUNING_INITIALIZATION_PROPERTY_NAME, FALSE));
			fileWriter.write(propertyValueString(MTDecoderConfig.SEPARATE_CUBE_PRUNING_STATES_MATCHING_SUBSTITUTIONS_PROPERTY_NAME, FALSE));
			fileWriter.write(propertyValueString(MTDecoderConfig.EXPLORE_ALL_LABELS_FOR_GLUE_RULES_IN_CUBE_PRUNING_INITIALIZATION_PROPERTY_NAME, FALSE));
			fileWriter.write(propertyValueString(MTDecoderConfig.MAX_NUMBER_ALTERNATIVE_LABELED_VERSIONS_PER_LANGUAGE_MODEL_STATE, "1"));
			fileWriter.write(propertyValueString(MTDecoderConfig.TUNING_NBEST_SIZE_PROPERTY, "300"));
			
		}else{
		    fileWriter.write(propertyValueString(MTDecoderConfig.FUZZY_MATCHING_PROPERTY_NAME, FALSE));
		}
		
		if(this.use_rule_application_features){
		    fileWriter.write(propertyValueString(MTDecoderConfig.RULE_APPLICATION_FEATURE_PROPERTY, TRUE));
		}
		
		if(this.useLabelSubstitutionFeatures){
			fileWriter.write(propertyValueString(MTDecoderConfig.LABEL_SUBSTITUTION_FEATURE_PROPERTY, TRUE));
			fileWriter.write(propertyValueString(MTDecoderConfig.SPARSE_LABEL_SUBSTITUTION_FEATURE_PROPERTY, FALSE));
		}
		// Don't use forest mira, it is still an experimental option in Joshua for mira tuning
		fileWriter.write(propertyValueString(MTDecoderConfig.USE_FOREST_MIRA_JOSHUA_PROPERTY, FALSE));
	}

	private boolean isDoubleReorderingLabel(){	    
	    String reorderLabelTypeProperty = testLabelingSettings.getReorderLabelTypeProperty();;
	    	   
	    if(reorderLabelTypeProperty.equals(PhraseCentricPlusParentRelativeReorderingLabel.getLabelTypeName())){
		return true;
	    }
	    else{
		return false;
	    }
	}
	
	private static String getRootOutputFolderName(String testName, String codeRootDir) {
		return codeRootDir + TESTS_OUTPUT_FOLDER_NAME + "/" + testName + "/";
	}

	private static String getTestOutputRootDir(String configFileName, String testName) {
		try {
			ConfigFile configFile = new ConfigFile(configFileName);
			String codeRootDir = getCodeRootDir(configFile);
			return getRootOutputFolderName(testName, codeRootDir);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Error: The required config file named " + configFileName + " was not found");
			return null;
		}
	}

	private static String getSystemResultsFolder(TestIdentifierPair testIdentifierPair, String testOutputRootDir) {
		return testOutputRootDir + "/" + testIdentifierPair.getSystemName() + "/";
	}

	public static String getMertGrammarLocation(String configFileName, TestIdentifierPair testIdentifierPair) {
		String testOutputRootDir = getTestOutputRootDir(configFileName, testIdentifierPair.getTestName());
		String result = MTFoldersConfig.getCategoryIntermediateResultsFolderName(MTFoldersConfig.MERT_FOLDER,
				getSystemResultsFolder(testIdentifierPair, testOutputRootDir))
				+ MERT_GRAMMAR_NAME;
		return result;
	}

	public static String getTestRootOutputDir(String configFileName, TestIdentifierPair testIdentifierPair) {
		String testOutputRootDir = getTestOutputRootDir(configFileName, testIdentifierPair.getTestName());
		return testOutputRootDir;
	}

	public static String getTestGrammarLocation(String configFileName, TestIdentifierPair testIdentifierPair) {
		String testOutputRootDir = getTestRootOutputDir(configFileName, testIdentifierPair);
		String result = MTFoldersConfig.getCategoryIntermediateResultsFolderName(MTConfigFile.TEST_CATEGORY,
				getSystemResultsFolder(testIdentifierPair, testOutputRootDir))
				+ TEST_GRAMMAR_NAME;
		return result;
	}

	public static String getTargetParseOytputFileName(String configFileName, TestIdentifierPair testIdentifierPair) {

		MTConfigFile mtConfigFile = MTConfigFile.createMtConfigFile(configFileName, testIdentifierPair.getSystemName());
		return ParserInterface.getTargetParseOutputFileName(mtConfigFile);
	}

	public static String getTargetParseGrammarInputFileName(String configFileName, TestIdentifierPair testIdentifierPair) {

		MTConfigFile mtConfigFile = MTConfigFile.createMtConfigFile(configFileName, testIdentifierPair.getSystemName());
		return ParserInterface.getTargetParseSubFilePath(mtConfigFile);
	}

	private static String getMTSystemTypeProperty(boolean useMoses) {
		if (useMoses) {
			return MosesSystem.getSystemTypeName();
		} else {
			return JoshuaSystem.getSystemTypeName();
		}
	}

	private static boolean usePackedGrammars(TestLabelingSettings testReorderingLabelingSettings, boolean useMoses) {
		if (!useMoses) {
			if (testReorderingLabelingSettings.useReorderingLabelingBinaryFeatures()) {
				return true;
			}
		}
		return false;
	}

	public static MTPipelineTestConfigFileCreater createMTPipelineTestConfigFileCreater(String configFileName, String resultFileName, List<String> systemNames,
			String testName, TestLabelingSettings testReorderingLabelingSettings, TestRerankingSettings testRerankingSettings, TUNER_TYPE tunerType,
			boolean parseTargetFile, int trainDataLength, String testDataLocationsConfigFile, boolean useMoses,  boolean useSoftSyntacticConstraintDecoding,boolean useLabelSubstitutionFeatures,
			SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations,boolean performTuning,boolean use_rule_application_features) {
		try {
			System.out.println("Working Directory = " + System.getProperty("user.dir"));
			ConfigFile configFile = new ConfigFile(configFileName);
			String codeRootDir = getCodeRootDir(configFile);

			boolean usePackedGrammars = usePackedGrammars(testReorderingLabelingSettings, useMoses);
			System.out.println("MTPipelineTestConfigFileCreater.testDataLocationsConfigFile: " + testDataLocationsConfigFile);
			return new MTPipelineTestConfigFileCreater(resultFileName, Utility.stringListStringWithoutBrackets(systemNames), getJDKHomeDir(configFile),
					getScriptsDir(configFile), getJoshuaHomeDir(configFile), getMosesHomeDir(configFile), getSrilmBinDir(configFile),
					getCharniakParserRootDir(configFile), getTnTTaggerRootDir(configFile),getStanfordSegmenterRootDir(configFile), getUniversalTagSetsMapDir(configFile), codeRootDir,
					getRootOutputFolderName(testName, codeRootDir), MTDecoderConfig.getTunerTypeString(tunerType), parseTargetFile,
					trainDataLength, MTPipelineTestDataLocations.createMTPipelineTestDataLocations(testDataLocationsConfigFile), usePackedGrammars,
					getMTSystemTypeProperty(useMoses), testReorderingLabelingSettings.writePlainHieroRulesForSmoothing(),
					testReorderingLabelingSettings.getLabelSmoothingTypeProperty(), testReorderingLabelingSettings, testRerankingSettings,useSoftSyntacticConstraintDecoding,
					useLabelSubstitutionFeatures,sourceAndTargetLanguageAbbreviations,
					performTuning,use_rule_application_features);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Error: The required config file named " + configFileName + " was not found");
			return null;
		}
	}

}
