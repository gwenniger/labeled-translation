package mt_pipeline.mtPipelineTest;

import util.XMLTagging;


public class TestIdentifierPair {

	private static final String SYSTEM_NAME_PROPERTY = "System";
	private static final String TEST_NAME_PROPERTY = "TestName";
	private static final String TEST_PROPERTY = "Test";
	private final String systemName;
	private final String testName;

	private TestIdentifierPair(String systemName, String testName) {
		this.systemName = systemName;
		this.testName = testName;
	}

	public static TestIdentifierPair createTestIdentifierPair(String systemName, String testName) {
		return new TestIdentifierPair(systemName, testName);
	}

	public String getSystemName() {
		return systemName;
	}

	public String getTestName() {
		return this.testName;
	}
	
	private String getWrappedSystemName()
	{
		return XMLTagging.getTagSandwidchedString(SYSTEM_NAME_PROPERTY, getSystemName());
	}
	
	private String getWrappedTestName()
	{
		return XMLTagging.getTagSandwidchedString(TEST_NAME_PROPERTY,getTestName());
	}
	
	public String createTestDescription()
	{
		String result = "";
		result += XMLTagging.getTagSandwidchedString(TEST_PROPERTY, getWrappedSystemName() + "," + getWrappedTestName());
		return result;
	}
}
