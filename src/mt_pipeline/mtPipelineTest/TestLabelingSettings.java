package mt_pipeline.mtPipelineTest;

import grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagLabelingSettings;
import grammarExtraction.chiangGrammarExtraction.LabelSideSettings;
import grammarExtraction.chiangGrammarExtraction.ReorderLabelingFlags;
import grammarExtraction.grammarExtractionFoundation.LabelingSettings.CanonicalFormSettings;
import grammarExtraction.labelSmoothing.LabeledRulesSmoother;
import grammarExtraction.reorderingLabeling.HATComplexityTypeReorderingLabel;
import grammarExtraction.reorderingLabeling.HatLabelRefinedPhraseCentricReorderingLabel;
import grammarExtraction.reorderingLabeling.PhraseCentricPlusParentRelativeReorderingLabel;
import reorderingLabeling.CoarseParentRelativeReorderingLabel;
import reorderingLabeling.CoarsePhraseCentricReorderingLabel;
import reorderingLabeling.FineParentRelativeReorderingLabel;
import reorderingLabeling.FinePhraseCentricReorderingLabelBasic;
import reorderingLabeling.ThreeCaseOnlyPhraseCentricReorderingLabel;

public class TestLabelingSettings {
    
    	private static final int NUM_ALLOWED_TYPES_PER_LABEL_FORS_OURCE = 5;
    
	ReorderLabelingFlags reorderLabelingFlags;
	private final boolean useReordingLabelingBinaryFeatures;
	private final String reorderLabelTypeProperty;
	private final boolean writePlainHieroRulesForSmoothing;
	private final String labelSmoothingTypeProperty;
	private final String labelSideSetting;
	private final CanonicalFormSettings canonicalFormSettings;
	private final BoundaryTagLabelingSettings boundaryTagLabelingSettings;

	private TestLabelingSettings(ReorderLabelingFlags reorderLabelingFlags, boolean useReordingLabelingBinaryFeatures, String reorderLabelTypeProperty,
			boolean writePlainHieroRulesForSmoothing, String labelSmoothingTypeProperty,String labelSideSetting,
			CanonicalFormSettings canonicalFormSettings,BoundaryTagLabelingSettings boundaryTagLabelingSettings) {
		this.reorderLabelingFlags = reorderLabelingFlags;
		this.useReordingLabelingBinaryFeatures = useReordingLabelingBinaryFeatures;
		this.reorderLabelTypeProperty = reorderLabelTypeProperty;
		this.writePlainHieroRulesForSmoothing = writePlainHieroRulesForSmoothing;
		this.labelSmoothingTypeProperty = labelSmoothingTypeProperty;
		this.labelSideSetting = labelSideSetting;
		this.canonicalFormSettings = canonicalFormSettings;
		this.boundaryTagLabelingSettings = boundaryTagLabelingSettings;
	}

	public static TestLabelingSettings createNoReorderingLabelingSettings(BoundaryTagLabelingSettings boundaryTagLabelingSettings, boolean outputOnlyCanonicalFormLabeledRules) {
	    	CanonicalFormSettings canonicalFormSettings = null;
	    	if(outputOnlyCanonicalFormLabeledRules){
	    	   canonicalFormSettings = CanonicalFormSettings.createCanonicalFormSettingsCannonicalFormLabelingDefault();
	    	}
	    	else{
	    	    canonicalFormSettings = CanonicalFormSettings.createCanonicalFormSettingsNonCanonicalFormUsed();
	    	}
	    
	    
		return new TestLabelingSettings(ReorderLabelingFlags.createNoReorderLabelingFlags(), false, "NONE", false, LabeledRulesSmoother.NO_LABEL_SMOOTHING,LabelSideSettings.LABEL_BOTH_SIDES_STRING,canonicalFormSettings,boundaryTagLabelingSettings);
	}

	public static TestLabelingSettings createTestReorderingLabelingSettings(ReorderLabelingFlags reorderLabelingFlags,
			boolean useReordingLabelingBinaryFeatures, String reorderLabelTypeProperty, boolean writePlainHieroRulesForSmoothing,
			String labelSmoothingTypeProperty,BoundaryTagLabelingSettings boundaryTagLabelingSettings) {
		return new TestLabelingSettings(ReorderLabelingFlags.createReorderLabelEverythingReorderLabelingFlags(), useReordingLabelingBinaryFeatures, reorderLabelTypeProperty,
				writePlainHieroRulesForSmoothing, labelSmoothingTypeProperty,LabelSideSettings.LABEL_BOTH_SIDES_STRING,CanonicalFormSettings.createCanonicalFormSettingsNonCanonicalFormUsed(),boundaryTagLabelingSettings);
	}

	public static TestLabelingSettings createPureHieroRuleFallbackTestReorderingLabelingSettings(BoundaryTagLabelingSettings boundaryTagLabelingSettings) {
		return new TestLabelingSettings(ReorderLabelingFlags.createReorderLabelEverythingReorderLabelingFlags(), false, FinePhraseCentricReorderingLabelBasic.getLabelTypeName(), true,
				LabeledRulesSmoother.FULL_LABEL_SMOOTHING,LabelSideSettings.LABEL_BOTH_SIDES_STRING,CanonicalFormSettings.createCanonicalFormSettingsNonCanonicalFormUsed(),boundaryTagLabelingSettings);
	}
	
	
	public static TestLabelingSettings createBasicTestReorderingLabelingSettings(boolean  outputOnlyCanonicalFormLabeledRules,boolean useOnlyHieroWeightsForCanonicalFormLabeledRules,BoundaryTagLabelingSettings boundaryTagLabelingSettings) {
		return new TestLabelingSettings(ReorderLabelingFlags.createReorderLabelEverythingReorderLabelingFlags(), false, FinePhraseCentricReorderingLabelBasic.getLabelTypeName(), false,
				getLabelSmoothingType(useOnlyHieroWeightsForCanonicalFormLabeledRules),LabelSideSettings.LABEL_BOTH_SIDES_STRING,CanonicalFormSettings.createCanonicalFormSettings(outputOnlyCanonicalFormLabeledRules, useOnlyHieroWeightsForCanonicalFormLabeledRules, getLabelSmoothingTypeT(useOnlyHieroWeightsForCanonicalFormLabeledRules),false,-1),boundaryTagLabelingSettings);
	}
	
	private static String getLabelSmoothingType(boolean useOnlyHieroWeightsForCanonicalFormLabeledRules){
		String labelSmoothingType;
	    	if(useOnlyHieroWeightsForCanonicalFormLabeledRules){
	    	    labelSmoothingType = LabeledRulesSmoother.NO_LABEL_SMOOTHING;
	    	}
	    	else{
	    	    labelSmoothingType = LabeledRulesSmoother.FULL_LABEL_SMOOTHING;
	    	}    
	    	return labelSmoothingType;
	}
	
	private static LabeledRulesSmoother.LABEL_SMOOTHING_TYPE getLabelSmoothingTypeT(boolean useOnlyHieroWeightsForCanonicalFormLabeledRules){
	    LabeledRulesSmoother.LABEL_SMOOTHING_TYPE labelSmoothingType;
	    	if(useOnlyHieroWeightsForCanonicalFormLabeledRules){
	    	    labelSmoothingType = LabeledRulesSmoother.LABEL_SMOOTHING_TYPE.NO_LABEL_SMOOTHING;
	    	}
	    	else{
	    	    labelSmoothingType = LabeledRulesSmoother.LABEL_SMOOTHING_TYPE.FULL_LABEL_SMOOTHING;
	    	}    
	    	return labelSmoothingType;
	}
	
	
	public static TestLabelingSettings createTestReorderingLabelingSettingsFineParentRelativeLabelWithLimitedLabelTypesPerSource(boolean  outputOnlyCanonicalFormLabeledRules, boolean useOnlyHieroWeightsForCanonicalFormLabeledRules,BoundaryTagLabelingSettings boundaryTagLabelingSettings) {
	    	return new TestLabelingSettings(ReorderLabelingFlags.createReorderLabelEverythingReorderLabelingFlags(), false, FineParentRelativeReorderingLabel.getLabelTypeName(), false,
				getLabelSmoothingType(useOnlyHieroWeightsForCanonicalFormLabeledRules),LabelSideSettings.LABEL_BOTH_SIDES_STRING,CanonicalFormSettings.createCanonicalFormSettings(outputOnlyCanonicalFormLabeledRules, useOnlyHieroWeightsForCanonicalFormLabeledRules, getLabelSmoothingTypeT(useOnlyHieroWeightsForCanonicalFormLabeledRules), true,NUM_ALLOWED_TYPES_PER_LABEL_FORS_OURCE),boundaryTagLabelingSettings);
	}
	
	public static TestLabelingSettings createTestReorderingLabelingSettingsFineParentRelativeLabel(boolean  outputOnlyCanonicalFormLabeledRules, boolean useOnlyHieroWeightsForCanonicalFormLabeledRules,BoundaryTagLabelingSettings boundaryTagLabelingSettings) {
	    	return new TestLabelingSettings(ReorderLabelingFlags.createReorderLabelEverythingReorderLabelingFlags(), false, FineParentRelativeReorderingLabel.getLabelTypeName(), false,
				getLabelSmoothingType(useOnlyHieroWeightsForCanonicalFormLabeledRules),LabelSideSettings.LABEL_BOTH_SIDES_STRING,CanonicalFormSettings.createCanonicalFormSettings(outputOnlyCanonicalFormLabeledRules, useOnlyHieroWeightsForCanonicalFormLabeledRules, getLabelSmoothingTypeT(useOnlyHieroWeightsForCanonicalFormLabeledRules), false,-1),boundaryTagLabelingSettings);
	}
	
	public static TestLabelingSettings createTestReorderingLabelingSettingsCoarsePhraseCentricLabel(boolean  outputOnlyCanonicalFormLabeledRules, boolean useOnlyHieroWeightsForCanonicalFormLabeledRules,BoundaryTagLabelingSettings boundaryTagLabelingSettings) {
	    	return new TestLabelingSettings(ReorderLabelingFlags.createReorderLabelEverythingReorderLabelingFlags(), false, CoarsePhraseCentricReorderingLabel.getLabelTypeName(), false,
				getLabelSmoothingType(useOnlyHieroWeightsForCanonicalFormLabeledRules),LabelSideSettings.LABEL_BOTH_SIDES_STRING,CanonicalFormSettings.createCanonicalFormSettings(outputOnlyCanonicalFormLabeledRules, useOnlyHieroWeightsForCanonicalFormLabeledRules, getLabelSmoothingTypeT(useOnlyHieroWeightsForCanonicalFormLabeledRules), false,-1),boundaryTagLabelingSettings);
	}
	
	
	public static TestLabelingSettings createTestReorderingLabelingSettingsCoarseParentRelativeLabel(boolean  outputOnlyCanonicalFormLabeledRules, boolean useOnlyHieroWeightsForCanonicalFormLabeledRules,BoundaryTagLabelingSettings boundaryTagLabelingSettings) {
	    	return new TestLabelingSettings(ReorderLabelingFlags.createReorderLabelEverythingReorderLabelingFlags(), false, CoarseParentRelativeReorderingLabel.getLabelTypeName(), false,
				getLabelSmoothingType(useOnlyHieroWeightsForCanonicalFormLabeledRules),LabelSideSettings.LABEL_BOTH_SIDES_STRING,CanonicalFormSettings.createCanonicalFormSettings(outputOnlyCanonicalFormLabeledRules, useOnlyHieroWeightsForCanonicalFormLabeledRules, getLabelSmoothingTypeT(useOnlyHieroWeightsForCanonicalFormLabeledRules), false,-1),boundaryTagLabelingSettings);
	}
	
	public static TestLabelingSettings createTestReorderingLabelingSettingsCoarseParentRelativeLabelWithLimitedLabelTypesPerSource(boolean  outputOnlyCanonicalFormLabeledRules, boolean useOnlyHieroWeightsForCanonicalFormLabeledRules,BoundaryTagLabelingSettings boundaryTagLabelingSettings) {
	    	return new TestLabelingSettings(ReorderLabelingFlags.createReorderLabelEverythingReorderLabelingFlags(), false, CoarseParentRelativeReorderingLabel.getLabelTypeName(), false,
				getLabelSmoothingType(useOnlyHieroWeightsForCanonicalFormLabeledRules),LabelSideSettings.LABEL_BOTH_SIDES_STRING,CanonicalFormSettings.createCanonicalFormSettings(outputOnlyCanonicalFormLabeledRules, useOnlyHieroWeightsForCanonicalFormLabeledRules, getLabelSmoothingTypeT(useOnlyHieroWeightsForCanonicalFormLabeledRules), true,NUM_ALLOWED_TYPES_PER_LABEL_FORS_OURCE),boundaryTagLabelingSettings);
	}
	
	
	public static TestLabelingSettings createTestReorderingLabelingSettingsHatAndParentRelativeLabel(boolean  outputOnlyCanonicalFormLabeledRules, boolean useOnlyHieroWeightsForCanonicalFormLabeledRules,BoundaryTagLabelingSettings boundaryTagLabelingSettings) {
	    	return new TestLabelingSettings(ReorderLabelingFlags.createReorderLabelEverythingReorderLabelingFlags(), false, PhraseCentricPlusParentRelativeReorderingLabel.getLabelTypeName(), false,
				getLabelSmoothingType(useOnlyHieroWeightsForCanonicalFormLabeledRules),LabelSideSettings.LABEL_BOTH_SIDES_STRING,CanonicalFormSettings.createCanonicalFormSettings(outputOnlyCanonicalFormLabeledRules, useOnlyHieroWeightsForCanonicalFormLabeledRules, getLabelSmoothingTypeT(useOnlyHieroWeightsForCanonicalFormLabeledRules),false,-1),boundaryTagLabelingSettings);
	}
	
	public static TestLabelingSettings createTestReorderingLabelingSettingsHatComplexityTypeReorderingLabel(boolean  outputOnlyCanonicalFormLabeledRules, boolean useOnlyHieroWeightsForCanonicalFormLabeledRules,BoundaryTagLabelingSettings boundaryTagLabelingSettings) {
	    	return new TestLabelingSettings(ReorderLabelingFlags.createReorderLabelEverythingReorderLabelingFlags(), false, HATComplexityTypeReorderingLabel.getLabelTypeName(), false,
				getLabelSmoothingType(useOnlyHieroWeightsForCanonicalFormLabeledRules),LabelSideSettings.LABEL_BOTH_SIDES_STRING,CanonicalFormSettings.createCanonicalFormSettings(outputOnlyCanonicalFormLabeledRules, useOnlyHieroWeightsForCanonicalFormLabeledRules, getLabelSmoothingTypeT(useOnlyHieroWeightsForCanonicalFormLabeledRules),false,-1),boundaryTagLabelingSettings);
	}
	
	public static TestLabelingSettings createBasicTestReorderingLabelingSettingsOnlySourceSideLabeled(BoundaryTagLabelingSettings boundaryTagLabelingSettings) {
		return new TestLabelingSettings(ReorderLabelingFlags.createReorderLabelEverythingReorderLabelingFlags(), false, FinePhraseCentricReorderingLabelBasic.getLabelTypeName(), false,
				LabeledRulesSmoother.FULL_LABEL_SMOOTHING,LabelSideSettings.LABEL_SOURCE_SIDE_STRING,CanonicalFormSettings.createCanonicalFormSettingsNonCanonicalFormUsed(),boundaryTagLabelingSettings);
	}
	
	public static TestLabelingSettings createBasicTestReorderingLabelingSettingsOnlyTargetSideLabeled(BoundaryTagLabelingSettings boundaryTagLabelingSettings) {
		return new TestLabelingSettings(ReorderLabelingFlags.createReorderLabelEverythingReorderLabelingFlags(), false, FinePhraseCentricReorderingLabelBasic.getLabelTypeName(), false,
				LabeledRulesSmoother.FULL_LABEL_SMOOTHING,LabelSideSettings.LABEL_TARGET_SIDE_STRING,CanonicalFormSettings.createCanonicalFormSettingsNonCanonicalFormUsed(),boundaryTagLabelingSettings);
	}
	
	public static TestLabelingSettings createTestReorderingLabelingSettingsReorderLabelOnlyAbstractRules(BoundaryTagLabelingSettings boundaryTagLabelingSettings) {
		return new TestLabelingSettings(ReorderLabelingFlags.createReorderLabelAbstractRulesOnlyReorderLabelingFlags(), false, ThreeCaseOnlyPhraseCentricReorderingLabel.getLabelTypeName(), false,
				LabeledRulesSmoother.FULL_LABEL_SMOOTHING,LabelSideSettings.LABEL_BOTH_SIDES_STRING,CanonicalFormSettings.createCanonicalFormSettingsNonCanonicalFormUsed(),boundaryTagLabelingSettings);
		//return new TestReorderingLabelingSettings(ReorderLabelingFlags.createReorderLabelingFlags(true, true, true), false, ThreeCaseOnlyPhraseCentricReorderingLabel.getLabelTypeName(), false,
		//		LabeledRulesSmoother.FULL_LABEL_SMOOTHING);
	}

	public static TestLabelingSettings createBasicTestReorderingLabelingSettingsWithBinaryFeatures(BoundaryTagLabelingSettings boundaryTagLabelingSettings) {
		return new TestLabelingSettings(ReorderLabelingFlags.createReorderLabelEverythingReorderLabelingFlags(), true, FinePhraseCentricReorderingLabelBasic.getLabelTypeName(), false,
				LabeledRulesSmoother.FULL_LABEL_SMOOTHING,LabelSideSettings.LABEL_BOTH_SIDES_STRING,CanonicalFormSettings.createCanonicalFormSettingsNonCanonicalFormUsed(),boundaryTagLabelingSettings);
	}

	public static TestLabelingSettings createHatRefinedTestReorderingLabelingSettings(BoundaryTagLabelingSettings boundaryTagLabelingSettings) {
		return new TestLabelingSettings(ReorderLabelingFlags.createReorderLabelEverythingReorderLabelingFlags(), true, HatLabelRefinedPhraseCentricReorderingLabel.getLabelTypeName(), false,
				LabeledRulesSmoother.FULL_LABEL_SMOOTHING,LabelSideSettings.LABEL_BOTH_SIDES_STRING,CanonicalFormSettings.createCanonicalFormSettingsNonCanonicalFormUsed(),boundaryTagLabelingSettings);
	}

	public boolean useReorderingLabelExtension() {
		return reorderLabelingFlags.useReorderingLabelExtension();
	}

	public boolean reorderLabelHieroRules() {
		return reorderLabelingFlags.reorderLabelHieroRules();
	}

	public boolean reorderLabelPhrasePairs() {
		return reorderLabelingFlags.reorderLabelPhrasePairs();
	}

	public boolean useReorderingLabelingBinaryFeatures() {
		return useReordingLabelingBinaryFeatures;
	}

	public String getReorderLabelTypeProperty() {
		return reorderLabelTypeProperty;
	}

	public boolean writePlainHieroRulesForSmoothing() {
		return this.writePlainHieroRulesForSmoothing;
	}

	public String getLabelSmoothingTypeProperty() {
		return this.labelSmoothingTypeProperty;
	}

	public String getLabelSideSetting() {
		return labelSideSetting;
	}

	public boolean outputOnlyCanonicalFormLabeledRules() {
		return canonicalFormSettings.useCanonicalFormLabeledRules();
	}
	
	public boolean useOnlyHieroWeightsForCanonicalFormLabeledRules() {
		return canonicalFormSettings.useOnlyHieroWeightsForCanonicalFormLabeledRules();
	}
	
	public boolean restrictLabelVariationForCanonicalFormLabeledRules(){
	    return canonicalFormSettings.restrictLabelVariationForCanonicalFormLabeledRules();
	}
	
	public int numAllowedTypesPerLabelForSource(){
	    return canonicalFormSettings.numAllowedTypesPerLabelForSource();
	}

	public BoundaryTagLabelingSettings getBoundaryTagLabelingSettings() {
	    return boundaryTagLabelingSettings;
	}    

}
