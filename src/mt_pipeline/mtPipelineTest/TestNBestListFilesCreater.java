package mt_pipeline.mtPipelineTest;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import mt_pipeline.evaluation.TarGzFileLineInterator;
import util.FileUtil;
import util.LinuxTarInterface;

public class TestNBestListFilesCreater {
    private static String TEST_FOLDER_PATH = "./testNBestLists/";
    private static String TEST_FILE_PATH = TEST_FOLDER_PATH + "output.nbest.TUNED.run1";
    public static final int NUM_SENTENCES = 3;
    public static final int NUM_CANDIDATES_PER_SENTENCE = 2;
    
    public static String getTestNBestListTarGzFilePath(){
	return LinuxTarInterface.getTarGzPackedFilePath(TEST_FILE_PATH);
    }

    private static String LINE_1_CONTENTS = "0 ||| ich hoffe von ganzem herzen , und ich muß sagen , daß hier ausdrücklich die möglichkeit bieten , wenn dieses dokument in die verträge aufgenommen werden kann - in die zukunft . ||| "
	    + "LabelSubstitution_MATCH=7.000 LabelSubstitution_NOMATCH=4.000 LabelSubstitution_[X-ATOMIC]_substitutes_[X-ATOMIC]=1.000 LabelSubstitution_[X-ATOMIC]_substitutes_[X-MONO]=2.000 LabelSubstitution_[X-BITT]_substitutes_[X-ATOMIC]=1.000 LabelSubstitution_[X-BITT]_substitutes_[X-BITT]=1.000 LabelSubstitution_[X-MONO]_substitutes_[X-MONO]=5.000 LabelSubstitution_[X-MONO]_substitutes_[X]=5.000 LabelSubstitution_[X-PET]_substitutes_[X-BITT]=1.000 "
	    + "lm_0=-43.995 tm_pt_0=-7.725 tm_pt_1=-34.478 tm_pt_10=-7.000 tm_pt_11=-1.000 tm_pt_13=-22.000 tm_pt_14=-32.000 tm_pt_15=-5.527 tm_pt_2=-24.533 tm_pt_3=-11.974 tm_pt_4=-92.713 tm_pt_5=-21.000 tm_pt_6=-5.000 tm_pt_7=-2.000 tm_pt_8=-23.000 tm_pt_9=-8.000 WordPenalty=-14.766 ||| -4.693";

    private static String LINE_2_CONTENTS = "0 ||| ich hoffe von ganzem herzen , und ich muß sagen , das hier ausdrücklich die möglichkeit bieten , entsteht , wenn das dokument in die verträge aufgenommen werden kann irgendwann in der zukunft . ||| "
	    + "LabelSubstitution_MATCH=7.000 LabelSubstitution_NOMATCH=3.000 LabelSubstitution_[X-ATOMIC]_substitutes_[X-ATOMIC]=1.000 LabelSubstitution_[X-ATOMIC]_substitutes_[X-MONO]=2.000 LabelSubstitution_[X-BITT]_substitutes_[X-ATOMIC]=1.000 LabelSubstitution_[X-BITT]_substitutes_[X]=1.000 LabelSubstitution_[X-MONO]_substitutes_[X-MONO]=6.000 LabelSubstitution_[X-MONO]_substitutes_[X]=4.000 LabelSubstitution_[X-PET]_substitutes_[X]=1.000 "
	    + "lm_0=-48.955 tm_pt_0=-8.437 tm_pt_1=-38.147 tm_pt_10=-8.000 tm_pt_13=-23.000 tm_pt_14=-34.000 tm_pt_15=-5.513 tm_pt_2=-25.443 tm_pt_3=-6.447 tm_pt_4=-92.243 tm_pt_5=-22.000 tm_pt_6=-6.000 tm_pt_7=-2.000 tm_pt_8=-24.000 tm_pt_9=-7.000 WordPenalty=-15.635 ||| -4.708";

    private static String LINE_3_CONTENTS = "1 ||| sollte dies nicht zustande kommen , dann sollten wir uns nicht wundern , wenn die öffentlichkeit beweist skeptisch gegenüber europa oder sogar abgelehnt wird . ||| "
	    + "LabelSubstitution_MATCH=7.000 LabelSubstitution_[X-ATOMIC]_substitutes_[X-ATOMIC]=3.000 LabelSubstitution_[X-ATOMIC]_substitutes_[X]=2.000 LabelSubstitution_[X-MONO]_substitutes_[X-MONO]=4.000 LabelSubstitution_[X-MONO]_substitutes_[X]=3.000 "
	    + "lm_0=-42.681 tm_pt_0=-8.094 tm_pt_1=-22.781 tm_pt_10=-7.000 tm_pt_13=-19.000 tm_pt_14=-25.000 tm_pt_15=-4.304 tm_pt_2=-26.087 tm_pt_3=-10.719 tm_pt_4=-74.491 tm_pt_5=-17.000 tm_pt_6=-5.000 tm_pt_7=-2.000 tm_pt_8=-19.000 tm_pt_9=-6.000 WordPenalty=-11.726 ||| -4.600";

    private static String LINE_4_CONTENTS = "1 ||| sollte dies nicht eingetreten , sollten wir nicht wundern , wenn die öffentlichkeit zeigt skeptisch in bezug auf europa , oder sogar abgelehnt wird . ||| "
	    + "LabelSubstitution_MATCH=8.000 LabelSubstitution_NOMATCH=1.000 LabelSubstitution_[X-ATOMIC]_substitutes_[X-ATOMIC]=3.000 LabelSubstitution_[X-MONO]_substitutes_[X-ATOMIC]=1.000 LabelSubstitution_[X-MONO]_substitutes_[X-MONO]=5.000 LabelSubstitution_[X-MONO]_substitutes_[X]=3.000 "
	    + "lm_0=-47.818 tm_pt_0=-6.844 tm_pt_1=-21.453 tm_pt_10=-5.000 tm_pt_13=-17.000 tm_pt_14=-25.000 tm_pt_15=-3.086 tm_pt_2=-23.029 tm_pt_3=-10.551 tm_pt_4=-78.126 tm_pt_5=-15.000 tm_pt_6=-3.000 tm_pt_7=-2.000 tm_pt_8=-17.000 tm_pt_9=-3.000 WordPenalty=-11.726 ||| -4.614";

    private static String LINE_5_CONTENTS = "2 ||| die regierungskonferenz - , um ein drittel betrifft - über die reform der europäischen institutionen ist von entscheidender bedeutung für uns im parlament . ||| "
	    + "LabelSubstitution_MATCH=6.000 LabelSubstitution_NOMATCH=1.000 LabelSubstitution_[X-ATOMIC]_substitutes_[X-ATOMIC]=1.000 LabelSubstitution_[X-ATOMIC]_substitutes_[X-MONO]=1.000 LabelSubstitution_[X-MONO]_substitutes_[X-MONO]=5.000 LabelSubstitution_[X-MONO]_substitutes_[X]=4.000 "
	    + "lm_0=-38.903 tm_pt_0=-5.001 tm_pt_1=-16.231 tm_pt_10=-6.000 tm_pt_13=-17.000 tm_pt_14=-24.000 tm_pt_15=-2.379 tm_pt_2=-10.300 tm_pt_3=-8.317 tm_pt_4=-66.978 tm_pt_5=-15.000 tm_pt_6=-4.000 tm_pt_7=-2.000 tm_pt_8=-17.000 tm_pt_9=-4.000 WordPenalty=-11.292 ||| -3.380";
    private static String LINE_6_CONTENTS = "2 ||| die regierungskonferenz - ein drittes thema - über die reform der europäischen institutionen für uns im parlament auch von entscheidender bedeutung ist . ||| "
	    + "LabelSubstitution_MATCH=9.000 LabelSubstitution_[X-ATOMIC]_substitutes_[X-ATOMIC]=3.000 LabelSubstitution_[X-BITT]_substitutes_[X-BITT]=1.000 LabelSubstitution_[X-MONO]_substitutes_[X-MONO]=5.000 LabelSubstitution_[X-MONO]_substitutes_[X]=4.000 lm_0=-37.799"
	    + " tm_pt_0=-7.020 tm_pt_1=-15.345 tm_pt_10=-6.000 tm_pt_11=-1.000 tm_pt_13=-18.000 tm_pt_14=-23.000 tm_pt_15=-0.528 tm_pt_2=-8.638 tm_pt_3=-10.284 tm_pt_4=-73.649 tm_pt_5=-17.000 tm_pt_6=-4.000 tm_pt_7=-2.000 tm_pt_8=-19.000 tm_pt_9=-6.000 WordPenalty=-10.857 ||| -3.401";

    private static List<String> NBEST_LIST_LINES_LIST = Arrays.asList(LINE_1_CONTENTS,
	    LINE_2_CONTENTS, LINE_3_CONTENTS, LINE_4_CONTENTS, LINE_5_CONTENTS, LINE_6_CONTENTS);

    private static void createTestFile() {
	FileUtil.createFolderIfNotExisting(TEST_FOLDER_PATH);
	BufferedWriter outputWriter = null;
	try {
	    outputWriter = new BufferedWriter(new FileWriter(TEST_FILE_PATH));
	    for (String line : NBEST_LIST_LINES_LIST) {
		outputWriter.write(line + "\n");
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	} finally {
	    FileUtil.closeCloseableIfNotNull(outputWriter);
	}
    }
    
    public static void createTestTarGzFile() {
	File fileToDelete = new File(getTestNBestListTarGzFilePath());
	fileToDelete.delete();
	
	createTestFile();
	LinuxTarInterface.packTarGzFile(TEST_FILE_PATH);
	fileToDelete = new File(TEST_FILE_PATH);
	fileToDelete.delete();
    }

    public static void main(String[] args) {
	createTestTarGzFile();
    }

}
