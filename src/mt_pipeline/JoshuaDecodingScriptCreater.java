package mt_pipeline;

import java.io.BufferedWriter;
import java.io.IOException;

import util.FileUtil;

public class JoshuaDecodingScriptCreater {

    private static String NL = "\n";
    // Static content of the script
    private static String HEAD_CONTENTS_STATIC = "#!/bin/bash" + NL + "#" + NL
	    + "# Joshua decoder invocation script." + NL + "#" + NL
	    + "# This script takes care of passing arguments to Java and to the" + NL
	    + "# Joshua decoder.  Usage:" + NL + "#" + NL
	    + "# ./decoder [-m memory] [Joshua arguments]" + NL + "#" + NL
	    + "# The default amount of memory is 4gb." + NL + NL + "NUM_ARGS=0" + NL
	    + "E_OPTERROR=1";
    private static String TAIL_CONTENTS_STATIC = "if [[ $1 == \"-m\" ]]; then"
	    + NL
	    + "        mem=$2"
	    + NL
	    + NL
	    + "    shift"
	    + NL
	    + "    shift"
	    + NL
	    + "fi"
	    + NL
	    + NL
	    + "set -u"
	    + NL
	    + NL
	    + "exec java -Xmx${mem} \\"
	    + NL
	    + "        -cp $JOSHUA/class:$JOSHUA/thrax/bin/thrax.jar:$JOSHUA/lib/berkeleylm.jar:$JOSHUA/lib/junit-4.10.jar \\"
	    + NL + "        -Dfile.encoding=utf8 \\" + NL
	    + "        -Djava.util.logging.config.file=${JOSHUA}/logging.properties \\" + NL
	    + "        -Djava.library.path=$JOSHUA/lib \\" + NL
	    + "        joshua.decoder.JoshuaDecoder \"$@\"";

    // Fields
    private final String memorySpecificationString;
    private final BufferedWriter outputWriter;

    private JoshuaDecodingScriptCreater(String memorySpecificationString,
	    BufferedWriter outputWriter) {
	this.memorySpecificationString = memorySpecificationString;
	this.outputWriter = outputWriter;
    }

    private String memorySpecificationPart() {
	String result = NL
		+ "#<DYNAMICALLY GENERATED PART>"
		+ NL
		+ "## memory usage - automatically generated based on experiment config file settings"
		+ NL + "mem=" + memorySpecificationString + NL + "#</DYNAMICALLY GENERATED PART>"
		+ NL + NL;
	return result;
    }

    public void writeScript() {
	try {
	    outputWriter.write(HEAD_CONTENTS_STATIC);
	    outputWriter.write(memorySpecificationPart());
	    outputWriter.write(TAIL_CONTENTS_STATIC);
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} finally {
	    FileUtil.closeCloseableIfNotNull(outputWriter);
	}

    }

}
