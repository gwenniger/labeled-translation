package mt_pipeline;

import java.util.ArrayList;
import java.util.List;

import bitg.Pair;
import mt_pipeline.mt_config.MTConfigFile;
import mt_pipeline.mt_config.MTDecoderConfig;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;

public class DecoderFilesCreaterBase extends MTComponentCreater {
	private static Double BASIC_FEATURE_INITIAL_WEIGHT = 0.5;
	protected static final String NL = "\n";
	
	protected final SystemIdentity systemIdentity;
	
	public DecoderFilesCreaterBase(MTConfigFile theConfig, SystemIdentity systemIdentity) {
		super(theConfig);
		this.systemIdentity = systemIdentity;
	}
	
	public List<Pair<String, Double>> getAllOrderedFeatureNamesWithWeights() {
		List<Pair<String, Double>> result = new ArrayList<Pair<String, Double>>();

		double discriminativeFeatureInitialWeight = theConfig.decoderConfig.getDiscriminativeFeatureInitialWeight();

		for (String featureName : systemIdentity.getOrderedFeatureNames()) {
			if (isReorderingFeature(featureName)) {
				result.add(new Pair<String, Double>(featureName, discriminativeFeatureInitialWeight));
			} else {
				result.add(new Pair<String, Double>(featureName, BASIC_FEATURE_INITIAL_WEIGHT));
			}
		}
		return result;
	}

	public List<Pair<String, Double>> geBasicOrderedFeatureNamesWithWeights() {
		List<Pair<String, Double>> result = new ArrayList<Pair<String, Double>>();

		for (String featureName : systemIdentity.getOrderedDenseFeatureNames()) {
			result.add(new Pair<String, Double>(featureName, BASIC_FEATURE_INITIAL_WEIGHT));
		}
		return result;
	}
	

	private boolean isReorderingFeature(String featureName) {
		return featureName.contains("RF-");
	}

	public int getTuningStrategyMode() {
		return this.theConfig.decoderConfig.getTuningStrategyMode();

	}

	public double getTuningInterpolationCoefficient() {
		return this.theConfig.decoderConfig.getTuningInterpolationCoefficient();
	}

	public String getNBestFormatPropertyString() {
		return MTDecoderConfig.NBEST_FORMAT_PROPERTY_STRING;
	}

	protected String getLanguageModelFilePath() {
		return theConfig.filesConfig.getBinaryLanguageModelFilePath();
		}
	
}
