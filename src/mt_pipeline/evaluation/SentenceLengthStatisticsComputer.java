package mt_pipeline.evaluation;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.io.LineIterator;
import util.LatexStyle;


public class SentenceLengthStatisticsComputer {

    private static final int NUMBER_DECIMALS = 2;
    private static NumberFormat NUMBER_FORMAT = createNumberFormat();

    private static final String NL = "\n";

    private final LineIterator lineIterator;
    private final List<Integer> sentenceLengths;

    private static NumberFormat createNumberFormat() {
	NumberFormat result = NumberFormat.getInstance();
	result.setMaximumFractionDigits(NUMBER_DECIMALS);
	return result;
    }

    private SentenceLengthStatisticsComputer(LineIterator lineIterator,
	    List<Integer> sentenceLengths) {
	this.lineIterator = lineIterator;
	this.sentenceLengths = sentenceLengths;
    }

    public static SentenceLengthStatisticsComputer createSentenceLengthStatisticsComputer(
	    String inputFilePath) {
	LineIterator lineIterator;
	try {
	    lineIterator = org.apache.commons.io.FileUtils.lineIterator(new File(inputFilePath),
		    "UTF8");
	    SentenceLengthStatisticsComputer result = new SentenceLengthStatisticsComputer(
		    lineIterator, new ArrayList<Integer>());
	    result.processLines();
	    return result;
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private List<String> getWordsFromLine(String line) {
	return Arrays.asList(line.split(" "));
    }

    private void processLines() {
	while (lineIterator.hasNext()) {
	    String line = lineIterator.nextLine();
	    List<String> words = getWordsFromLine(line);
	    sentenceLengths.add(words.size());
	}
    }

    public int getNumSentences() {
	return sentenceLengths.size();
    }

    private int getTotalWordCount() {
	int result = 0;
	for (int length : sentenceLengths) {
	    result += length;
	}
	return result;
    }

    private List<Double> getSentenceLengthsAsDoubleList() {
	List<Double> result = new ArrayList<Double>();
	for (Integer sentenceLength : sentenceLengths) {
	    result.add(new Double(sentenceLength));
	}
	return result;
    }

    private DoubleListStatistics getSentenceLengthDoubleListStatistics() {
	return DoubleListStatistics.createDoublesListStatistics(getSentenceLengthsAsDoubleList());
    }

    private String getStatisticsHeaderString() {
	String result = "";
	result += "NumSentences" + LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR + "NumWords"
		+ LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR;
	result += "Mean/Std Sentence Length";
	return result;
    }

    public String getLengthStatisticsString() {
	String result = "";
	result += getTotalWordCount() + LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR;
	result += NUMBER_FORMAT.format(getSentenceLengthDoubleListStatistics().getMean())
		+ LatexStyle.LATEX_PLUS_MIN_SYMBOL
		+ NUMBER_FORMAT.format(getSentenceLengthDoubleListStatistics().getStd());
	return result;
    }

    private String getStatisticsString() {
	String result = "";
	result += getNumSentences() + LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR
		+ getLengthStatisticsString();
	return result;
    }

    public String getStatisticsTable() {
	return getStatisticsHeaderString() + NL + getStatisticsString();
    }

    public static void main(String[] args) {
	if (args.length != 1) {
	    System.out.println("Usage: corpusStatisticsComputer INPUT_FILE_PATH");
	    System.exit(1);
	}
	SentenceLengthStatisticsComputer corpusStatisticsComputer = createSentenceLengthStatisticsComputer(args[0]);
	System.out.println(corpusStatisticsComputer.getStatisticsTable());
    }

}
