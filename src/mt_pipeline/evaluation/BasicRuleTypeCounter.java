package mt_pipeline.evaluation;

import junit.framework.Assert;

public class BasicRuleTypeCounter {
    private final static String HIERO_RULES_AND_HIERO_GLUE_RULES_FEATURE_STRING = "tm_pt_5";
    private final static String HIERO_GLUE_RULES_FEATURE_STRING = "tm_pt_6";
    private final static String START_AND_END_RULE_FEATURE_STRING = "tm_pt_7";
    private final static String TOTAL_RULES_FEATURE_STRING = "tm_pt_8";
    private final static String OOV_PENALTY_FEATURE_STRING = "OOVPenalty";

    private static final boolean COMPENSATE_FOR_OOV_PRODUCTIONS = true;

    /**
     * This is the set of all rules that is used with an X on the LHS, basically
     * in case of Hiero grammars all rules except Start/End rules
     */
    private int numHieroRulesAndHieroGlueRules;

    /** This is the number of glue rules that is used */
    private int numHieroGlueRules;

    /**
     * The number of start and end rules. This is kept to enable sanity checks
     * mostly.
     */
    private int numStartAndEndRules;

    /** The total number of rules that is used */
    private int totalNumRules;

    /** The total number of unknown word (OOV) productions */
    private int totalNumberUnknownWordProductions;

    private BasicRuleTypeCounter() {
	this.numHieroRulesAndHieroGlueRules = -1;
	this.numHieroGlueRules = -1;
	this.totalNumRules = -1;
	this.numStartAndEndRules = -1;
	this.totalNumberUnknownWordProductions = 0;
    }

    public static BasicRuleTypeCounter createBasicRuleTypeCounter() {
	return new BasicRuleTypeCounter();
    }

    /**
     * This method computes the number of substitutions to glue rules (as
     * opposed to the number of glue rules itself), which is equal to the number
     * of glue rules
     * 
     * @return
     */
    public int getNumGlueRuleSubstitutions() {
	performSanityChecks();
	return numHieroGlueRules;
    }

    /**
     * This method computes a correction for unknown word rule productions. In
     * non-Hiero grammars, for each unknown word that is produced, a
     * pre-terminal word is first applied to produce the unknown word symbol
     * UnknownWord. But in Hiero, this is not necessary, since "X" is the
     * default symbol and therefore can directly produce unknown words. To keep
     * the counts for Hiero grammars an non-Hiero grammars (with soft matching)
     * comparable, we correct for the missing UnknownWord producing unary rules
     * by adding them as additional matching substitutions.
     * 
     * @return
     */
    private int getMissingUnknowWordUnaryRuleSubstitionCorrection() {
	return totalNumberUnknownWordProductions;
    }

    /**
     * This method computes the number of matching substitutions, not counting
     * substitutions to glue rules. For this reason the number is computed by
     * substracting twice the number of glue rules from the total number of
     * (Hiero rules + Hiero glue rules). Note: with "Hiero" rules we mean in
     * proper phrase pairs and lexicalized rules with gaps.
     * 
     * @return
     */
    public int getNumMatchingSubstitutions() {
	performSanityChecks();
	int result = numHieroRulesAndHieroGlueRules - (2 * numHieroGlueRules);
	if (COMPENSATE_FOR_OOV_PRODUCTIONS) {
	    result += getMissingUnknowWordUnaryRuleSubstitionCorrection();
	}
	return result;
    }

    private int getNonNegativeFeatureCount(String featureString) {
	String countString = featureString.split("=")[1];
	double featureValue = Double.parseDouble(countString);
	return Math.abs((int) featureValue);
    }

    public void updateRuleTypeCounts(String featureString) {
	int count = getNonNegativeFeatureCount(featureString);
	if (consitutesHieroRulesAndGlueRulesFeatureString(featureString)) {
	    setNumHieroRulesAndHieroGlueRules(count);

	} else if (consitutesGlueRulesFeatureString(featureString)) {
	    setNumHieroGlueRules(count);
	} else if (consitutesTotalRulesFeatureString(featureString)) {
	    setTotalNumRules(count);
	} else if (consitutesStartAndEndRulesFeature(featureString)) {
	    setNumberStartAndEndRules(count);
	}

	else if (consitutesOOVPenaltyFeatureString(featureString)) {
	    setTotalNumberUnknownWordProdcutions(count);
	}
    }

    private void assertAllValuesSet() {
	Assert.assertTrue(numHieroRulesAndHieroGlueRules >= 0);
	Assert.assertTrue(numHieroGlueRules >= 0);
	Assert.assertTrue(totalNumRules >= 0);
	Assert.assertTrue(numStartAndEndRules >= 0);
    }

    private void assertTotalNumberAddsUp() {
	int totalComputedFromParts = numHieroRulesAndHieroGlueRules + numStartAndEndRules;
	Assert.assertEquals(totalComputedFromParts, totalNumRules);
    }

    public void performSanityChecks() {
	assertAllValuesSet();
	assertTotalNumberAddsUp();
    }

    private void setNumHieroRulesAndHieroGlueRules(int numHieroRulesAndHieroGlueRules) {
	this.numHieroRulesAndHieroGlueRules = numHieroRulesAndHieroGlueRules;
    }

    private void setNumHieroGlueRules(int numHieroGlueRules) {
	this.numHieroGlueRules = numHieroGlueRules;
    }

    private void setTotalNumRules(int totalNumRules) {
	this.totalNumRules = totalNumRules;
    }

    private void setNumberStartAndEndRules(int numStartAndEndRules) {
	this.numStartAndEndRules = numStartAndEndRules;
    }

    private void setTotalNumberUnknownWordProdcutions(int totalNumUnknownWordProductions) {
	this.totalNumberUnknownWordProductions = totalNumUnknownWordProductions;
    }

    public static boolean isFeatureOfInterest(String featureString) {
	return consitutesGlueRulesFeatureString(featureString)
		|| consitutesHieroRulesAndGlueRulesFeatureString(featureString)
		|| consitutesTotalRulesFeatureString(featureString)
		|| consitutesStartAndEndRulesFeature(featureString)
		|| consitutesOOVPenaltyFeatureString(featureString);
    }

    private static boolean consitutesTotalRulesFeatureString(String featureString) {
	return featureString.contains(TOTAL_RULES_FEATURE_STRING);
    }

    private static boolean consitutesHieroRulesAndGlueRulesFeatureString(String featureString) {
	return featureString.contains(HIERO_RULES_AND_HIERO_GLUE_RULES_FEATURE_STRING);
    }

    private static boolean consitutesGlueRulesFeatureString(String featureString) {
	return featureString.contains(HIERO_GLUE_RULES_FEATURE_STRING);
    }

    private static boolean consitutesStartAndEndRulesFeature(String featureString) {
	return featureString.contains(START_AND_END_RULE_FEATURE_STRING);
    }

    private static boolean consitutesOOVPenaltyFeatureString(String featureString) {
	return featureString.contains(OOV_PENALTY_FEATURE_STRING);
    }

}
