package mt_pipeline.evaluation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import mt_pipeline.evaluation.CorpusStatisticsComputer.PairedFilesCorpusStatisticsComputer;
import mt_pipeline.evaluation.CorpusStatisticsComputer.SingleFileCorpusStatisticsComputer;
import org.apache.commons.io.LineIterator;
import util.FileUtil;
import util.LatexStyle;

public class MultiCorpusLengthStatisticsTableComputer {
    private static String NL = "\n";
    private final LineIterator lineIterator;
    private final CorpusStatisticsComputerCreater corpusStatisticsComputerCreater;

    private MultiCorpusLengthStatisticsTableComputer(LineIterator lineIterator,
	    CorpusStatisticsComputerCreater corpusStatisticsComputerCreater) {
	this.lineIterator = lineIterator;
	this.corpusStatisticsComputerCreater = corpusStatisticsComputerCreater;
    }

    public static CorpusStatisticsComputerCreater createCorpusStatisticsComputerCreater(
	    boolean usePairedCorpusFiles) {
	if (usePairedCorpusFiles) {
	    return PairedFilesCorpusStatisticsComputer
		    .createPairedFilesCorpusStatisticsComputerCreater();
	} else {
	    return SingleFileCorpusStatisticsComputer
		    .createSingleFileCorpusStatisticsComputerCreater();
	}
    }

    public static MultiCorpusLengthStatisticsTableComputer createMultiCorpusLengthStatisticsTableComputer(
	    String inputFilePath, boolean usePairedCorpusFiles) {
	LineIterator lineIterator;
	try {
	    lineIterator = org.apache.commons.io.FileUtils.lineIterator(new File(inputFilePath),
		    "UTF8");
	    MultiCorpusLengthStatisticsTableComputer result = new MultiCorpusLengthStatisticsTableComputer(
		    lineIterator, createCorpusStatisticsComputerCreater(usePairedCorpusFiles));
	    return result;
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private CorpusStatisticsComputer getCorpusStatisticsComputer(String configFileLine) {
	return corpusStatisticsComputerCreater.createCorpusStatisticsComputer(configFileLine);
    }

    private String getDataRowString(String configFileLine) {
	String result = "";
	CorpusStatisticsComputer corpusStatisticsComputer = getCorpusStatisticsComputer(configFileLine);
	result += corpusStatisticsComputer.getStatisticsString(configFileLine)
		+ LatexStyle.LATEX_END_OF_LINE_STRING + NL;
	return result;
    }

    public String getStatisticsTableHeaderString() {
	String result = "";
	result += LatexStyle.TABLE_STAR_BEGIN + NL;
	result += "\\centering" + NL;
	result += "\\lapbox[\\width]{-1.5cm}{" + NL;
	result += "\\resizebox{1.2\\textwidth}{!}{%" + NL;
	result += corpusStatisticsComputerCreater.getStatisticsTableHeaderString();
	return result;
    }

    public static String getStatisticsTableFooterString() {
	String result = "";
	result += LatexStyle.TABULAR_END + NL;
	result += "}}" + NL;
	result += LatexStyle.TABLE_STAR_END;
	return result;
    }

    public String getDocumentHeaderString() {
	String result = "";
	result += LatexStyle.standardDocumentSpecificaionString();
	result += LatexStyle.usePackage("adjustbox");
	result += NL + NL;
	result += LatexStyle.BEGIN_DOCUMENT + NL;
	return result;
    }

    public String getDocumentFooterString() {
	String result = "";
	result += LatexStyle.END_DOCUMENT + NL;
	return result;

    }

    private String computeStatisticsTable() {
	String result = getDocumentHeaderString();
	result += getStatisticsTableHeaderString() + NL;
	result += LatexStyle.HLINE + NL;

	while (lineIterator.hasNext()) {
	    String configFileLine = lineIterator.nextLine();
	    result += getDataRowString(configFileLine);
	    result += LatexStyle.HLINE + NL;
	}
	result += getStatisticsTableFooterString();
	result += getDocumentFooterString();
	return result;
    }

    public void computeAndWriteStatisticsTable(String outputFilePath) {
	BufferedWriter outputWriter = null;
	try {
	    outputWriter = new BufferedWriter(new FileWriter(outputFilePath));
	    outputWriter.write(computeStatisticsTable());
	} catch (IOException e) {
	    e.printStackTrace();
	} finally {
	    FileUtil.closeCloseableIfNotNull(outputWriter);
	}
    }

    public static void main(String[] args) {
	if (args.length != 3) {
	    String usageString = "Usage: MultiCorpusLengthStatisticsTableComputer ConfigFilePath OUTPUT_FILE_PATH USE_PAIRED_FILE_PATHS"
		    + NL;
	    usageString += NL + "ConfigFilePath is the path of a config file, such that";
	    usageString += NL + "every line of the config file has the format:";
	    usageString += NL + "DataDescriptionString" + CorpusStatisticsComputer.CONFIG_SEPARATOR
		    + "SourceFilePath" + CorpusStatisticsComputer.CONFIG_SEPARATOR
		    + "TargetFilePath" + NL + NL;
	    usageString += "Or:" + NL;
	    usageString += NL + "DataDescriptionString" + CorpusStatisticsComputer.CONFIG_SEPARATOR
		    + "SourceFilePath" + NL;
	    usageString += "In case USE_PAIRED_FILE_PATHS is set to false";
	    System.out.println(usageString);
	    System.exit(1);
	}
	MultiCorpusLengthStatisticsTableComputer multiCorpusLengthStatisticsTableComputer = MultiCorpusLengthStatisticsTableComputer
		.createMultiCorpusLengthStatisticsTableComputer(args[0],
			Boolean.parseBoolean(args[2]));
	multiCorpusLengthStatisticsTableComputer.computeAndWriteStatisticsTable(args[1]);
    }

}
