package mt_pipeline.evaluation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;
import mt_pipeline.evaluation.MultEvalSystemScores.SystemScoresBasic;
import mt_pipeline.evaluation.MultEvalSystemScores.SystemScoresWithBeer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import util.Pair;

public class MultiEvalSignificanceResultsTableCreater<T extends MultEvalSystemScores> {

    private static final int NUM_RESULT_DECIMALS = 2;
    private static final String FIRST_LINE_RESULS_TABLE_PREFIX = "n=";
    
    private final boolean useBeerMetric;
    boolean outputLengthStatistics;
    
    public MultiEvalSignificanceResultsTableCreater(boolean useBeerMetric,boolean outputLengthStatistics){
	this.useBeerMetric = useBeerMetric;
	this.outputLengthStatistics = outputLengthStatistics;
    }       

    public static MultiEvalResultsTable<SystemScoresBasic> createMultiEvalSignificanceResultsTableBasic(
	    List<SystemScoresBasic> systemScoresList, int numDecimals,boolean outputLengthStatistics) {
	Map<String, Integer> systemNameToSystemIndexMap = new HashMap<String, Integer>();
	for (int i = 0; i < systemScoresList.size(); i++) {
	    systemNameToSystemIndexMap.put(systemScoresList.get(i).getSystemName(), i);
	}
	return   MultiEvalResultsTable.createMultiEvalResultsTableBasic(systemNameToSystemIndexMap, systemScoresList, MultiEvalResultsTable.createResultNumberFormat(numDecimals),outputLengthStatistics);
    }
    
    public static MultiEvalResultsTable<SystemScoresWithBeer> createMultiEvalSignificanceResultsTableWithBeer(
	    List<SystemScoresWithBeer> systemScoresList, int numDecimals,boolean outputLengthStatistics) {
	Map<String, Integer> systemNameToSystemIndexMap = new HashMap<String, Integer>();
	for (int i = 0; i < systemScoresList.size(); i++) {
	    systemNameToSystemIndexMap.put(systemScoresList.get(i).getSystemName(), i);
	}
	return   MultiEvalResultsTable.createMultiEvalResultsTableWithBeer(systemNameToSystemIndexMap, systemScoresList, MultiEvalResultsTable.createResultNumberFormat(numDecimals),outputLengthStatistics);
    }

    private List<T> getSystemScoresListFromLineIterator(
	    LineIterator resultsFileLineIterator) {
	List<T> result = new ArrayList<T>();

	boolean resultTableHeaderRead = false;

	// Read lines until header line that indicates start of results table
	// has been read
	while (resultsFileLineIterator.hasNext() && (!resultTableHeaderRead)) {
	    String line = resultsFileLineIterator.nextLine();
	    if (isFirstLineResultTable(line)) {
		resultTableHeaderRead = true;
	    }
	}

	// Next lines correspond to system scores
	while (resultsFileLineIterator.hasNext()) {
	    String line = resultsFileLineIterator.nextLine();
	    if (line.length() > 0) {
		result.add(getSystemScoresFromResultsLine(line));
	    }
	}

	return result;

    }

    @SuppressWarnings("unchecked")
    public MultiEvalResultsTable<T> createMultiEvalSignificanceResultsTable(
	    String resultFilePath) {
	try {
	    LineIterator resultsFileLineIterator = FileUtils.lineIterator(new File(resultFilePath),
		    "UTF-8");
	    	
	    List<T> systemScoresList = getSystemScoresListFromLineIterator(resultsFileLineIterator);
	    
	    if(useBeerMetric)
	    {
		return (MultiEvalResultsTable<T>) createMultiEvalSignificanceResultsTableWithBeer((List<SystemScoresWithBeer>) systemScoresList, NUM_RESULT_DECIMALS,outputLengthStatistics);
	    }
	    
	    return (MultiEvalResultsTable<T>) createMultiEvalSignificanceResultsTableBasic((List<SystemScoresBasic>) systemScoresList, NUM_RESULT_DECIMALS,outputLengthStatistics);

	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private static boolean isFirstLineResultTable(String line) {
	return line.startsWith(FIRST_LINE_RESULS_TABLE_PREFIX);
    }

    private static String getSystemNameString(String[] tableLineParts) {
	if (tableLineParts.length == 10) {
	    return tableLineParts[0] + tableLineParts[1];
	}
	return tableLineParts[0];
    }

    private static ScoreQuadruple getScoreTripleFromScoreStringPair(Pair<String> scoreStringPair, boolean higherIsBetter) {
	double score = getScore(scoreStringPair);
	double pValue = getPValue(scoreStringPair);
	ScoreQuadruple result = new ScoreQuadruple(score, -1, pValue,higherIsBetter);
	return result;
    }

    
    private int expectedNumberOfScoreStringPairs(){
	if(useBeerMetric){
	    return 5;
	}
	return 4;
    }
    
    private int terMetricNumber(){
	if(useBeerMetric){
	    return 3;
	}
	else{
	    return 2;
	}
    }
    
    private boolean isTerMetric(int metricNumber){
	return metricNumber == terMetricNumber();
    }
    
    private boolean higherScoreIsBetter(int metricNumber){
	return !isTerMetric(metricNumber);
    }
    
    @SuppressWarnings("unchecked")
    private T getSystemScoresFromResultsLine(String resultsLine) {
	String[] tableLineParts = resultsLine.split("(\\s)+");
	String systemName = getSystemNameString(tableLineParts);
	List<Pair<String>> scoreStringPairs = getScoreStringPairs(tableLineParts);
	
	
	Assert.assertEquals(expectedNumberOfScoreStringPairs(), scoreStringPairs.size());
	List<ScoreQuadruple> scoreTriplesList = new ArrayList<ScoreQuadruple>();
	int metricNumber = 0;
	for (Pair<String> scoreStringPair : scoreStringPairs) {
	    scoreTriplesList.add(getScoreTripleFromScoreStringPair(scoreStringPair,higherScoreIsBetter(metricNumber)));
	    metricNumber++;
	}

	if(useBeerMetric)	{
	    return (T) SystemScoresWithBeer.createSystemScores(systemName, scoreTriplesList.get(0),
			scoreTriplesList.get(1), scoreTriplesList.get(2), scoreTriplesList.get(3),
			scoreTriplesList.get(4),outputLengthStatistics);    
	}
	else{
	    return (T) SystemScoresBasic.createSystemScores(systemName, scoreTriplesList.get(0),
			scoreTriplesList.get(1), scoreTriplesList.get(2), scoreTriplesList.get(3),outputLengthStatistics);
	}
	    
	
    }

    private static boolean evenNumberTableLineParts(String[] tableLineParts){
	return (tableLineParts.length % 2) == 0;
    }
    
    
    private static List<Pair<String>> getScoreStringPairs(String[] tableLineParts) {
	System.out.println("getScoreStringPairs...");
	List<Pair<String>> result = new ArrayList<Pair<String>>();
	for (String tableLinePart : tableLineParts) {
	    System.out.println("tableLinePart: " + tableLinePart);
	}

	int startIndex = 1;
	// If there is an even number of table line parts, then 2 instead of 1 elements should be skipped
	if (evenNumberTableLineParts(tableLineParts)) {
	    startIndex = 2;
	}

	for (int i = startIndex; i < tableLineParts.length; i += 2) {
	    System.out.println("i:" + i);
	    result.add(new Pair<String>(tableLineParts[1], tableLineParts[i + 1]));
	}
	return result;
    }

    private static double getScore(Pair<String> scoreStringPair) {
	return Double.parseDouble(scoreStringPair.getFirst());
    }

    private static double getPValue(Pair<String> scoreStringPair) {
	String[] subScoreParts = scoreStringPair.getSecond().split("/");
	String pValueString = subScoreParts[subScoreParts.length - 1];
	pValueString = pValueString.substring(0, pValueString.length() - 1);
	if (pValueString.equals("-")) {
	    return -1;
	}
	return Double.parseDouble(pValueString);
    }

}
