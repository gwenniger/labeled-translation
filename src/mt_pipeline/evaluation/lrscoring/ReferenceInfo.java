package mt_pipeline.evaluation.lrscoring;

import java.util.List;

public class ReferenceInfo {

    private final String sourceFilePath;
    // There may be multpiple reference/reference-alignment pairs
    private final List<OutputAlignmentPair> refOutputAlignmentPairs;

    private ReferenceInfo(String sourceFilePath, List<OutputAlignmentPair> refOutputAlignmentPairs) {
	this.sourceFilePath = sourceFilePath;
	this.refOutputAlignmentPairs = refOutputAlignmentPairs;
    }

    public static ReferenceInfo createReferenceInfo(String sourceFilePath,
	    List<OutputAlignmentPair> refOutputAlignmentPairs) {
	return new ReferenceInfo(sourceFilePath, refOutputAlignmentPairs);
    }

    public List<OutputAlignmentPair> getRefOutputAlignmentPairs() {
	return refOutputAlignmentPairs;
    }

    public String getSourceFilePath() {
	return sourceFilePath;
    }

    public static class OutputAlignmentPair {
	private final String outputFilePath;
	private final String alignmentOutputFilePath;

	OutputAlignmentPair(String outputFilePath, String alignmentOutputFilePath) {
	    this.outputFilePath = outputFilePath;
	    this.alignmentOutputFilePath = alignmentOutputFilePath;
	}

	public String getAlignmentOutputFilePath() {
	    return alignmentOutputFilePath;
	}

	public String getOutputFilePath() {
	    return outputFilePath;
	}
    }

}