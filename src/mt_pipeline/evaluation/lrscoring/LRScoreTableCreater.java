package mt_pipeline.evaluation.lrscoring;

import java.util.List;

import mt_pipeline.evaluation.lrscoring.LRScorerInterface.LRScoreConfigFile;

public class LRScoreTableCreater {

    private final SystemLRScoresListCreater devSystemLRScoresListCreater;
    private final SystemLRScoresListCreater testSystemLRScoresListCreater;

    LRScoreTableCreater(SystemLRScoresListCreater devSystemLRScoresListCreater,
	    SystemLRScoresListCreater testSystemLRScoresListCreater) {
	this.devSystemLRScoresListCreater = devSystemLRScoresListCreater;
	this.testSystemLRScoresListCreater = testSystemLRScoresListCreater;
    }

    public static LRScoreTableCreater createLrScoreTableCreater(String devScoringConfigFilePath,
	    String testScoringConfigFilePath) {
	LRScoreConfigFile lrScoreConfigFileDev = LRScoreConfigFile
		.createLRScoreConfigFile(devScoringConfigFilePath);
	LRScoreConfigFile lrScoreConfigFileTest = LRScoreConfigFile
		.createLRScoreConfigFile(testScoringConfigFilePath);
	LRScorerInterface lrScorerInterface = LRScorerInterface
		.createLRLrScorerInterface(lrScoreConfigFileTest);
	ReferenceInfo referenceInfoDev = lrScoreConfigFileDev.createReferenceInfo();
	ReferenceInfo referenceInfoTest = lrScoreConfigFileTest.createReferenceInfo();
	String resultRootFolderPath = lrScoreConfigFileTest.getResultRootFolderPath();
	List<String> systemNameList = lrScoreConfigFileTest.getSystemNameList();

	SystemLRScoresListCreater devSystemLRScoresListCreater = SystemLRScoresListCreater
		.createDevSystemLRScoresListCreater(referenceInfoDev, lrScorerInterface,
			resultRootFolderPath, systemNameList);
	SystemLRScoresListCreater testSystemLRScoresListCreater = SystemLRScoresListCreater
		.createTestSystemLRScoresListCreater(referenceInfoTest, lrScorerInterface,
			resultRootFolderPath, systemNameList);
	return new LRScoreTableCreater(devSystemLRScoresListCreater, testSystemLRScoresListCreater);
    }

    public SystemScoresTable createSystemLRScoresTable() {
	List<SystemScores> devScores = devSystemLRScoresListCreater.computeSystemScoresList();
	List<SystemScores> testScores = testSystemLRScoresListCreater.computeSystemScoresList();
	return SystemScoresTable.createSystemScoresTable(devScores, testScores, true);
    }

    public void createAndShowSystemLRScoreTable() {
	SystemScoresTable systemScoresTable = createSystemLRScoresTable();
	System.out.println(systemScoresTable.toStringSpecial(true));
    }

    public static void main(String[] args) {
	if (args.length != 2) {
	    System.out
		    .println("Usage : LRSCoreTableCreater CONFIGFILEPATH_DEV CONFIGFILEPATH_TEST");
	    System.exit(2);
	}
	LRScoreTableCreater lrScoreTableCreater = LRScoreTableCreater.createLrScoreTableCreater(
		args[0], args[1]);
	lrScoreTableCreater.createAndShowSystemLRScoreTable();
    }

}
