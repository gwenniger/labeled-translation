package mt_pipeline.evaluation.lrscoring;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.List;

import mt_pipeline.evaluation.ImprovementStrings;
import mt_pipeline.evaluation.lrscoring.ImprovedSentenceScoresComputer.DoublePair;
import junit.framework.Assert;

public class SystemScoresTable {

    private static String PLUS_MINUS_STRING = "$\\pm$";
    private static final int NUM_DECIMALS_DISPLAYED = 2;
    NumberFormat numberFormat = createNumberFormat();
    private final SystemScoresList devScoresList;
    private final SystemScoresList testScoresList;

    private SystemScoresTable(SystemScoresList devScores, SystemScoresList testScores) {
	this.devScoresList = devScores;
	this.testScoresList = testScores;
    }

    public static SystemScoresTable createSystemScoresTable(List<SystemScores> devScores,
	    List<SystemScores> testScores, boolean higerIsBetter) {
	return new SystemScoresTable(SystemScoresList.createSystemScoresList(devScores,
		higerIsBetter), SystemScoresList.createSystemScoresList(testScores, higerIsBetter));
    }

    private static NumberFormat createNumberFormat() {
	NumberFormat result = NumberFormat.getInstance();
	result.setMinimumFractionDigits(NUM_DECIMALS_DISPLAYED);
	result.setMaximumFractionDigits(NUM_DECIMALS_DISPLAYED);
	result.setRoundingMode(RoundingMode.HALF_EVEN);
	return result;
    }

    private static NumberFormat createNoDecimalsNumberFormat() {
	NumberFormat result = NumberFormat.getInstance();
	result.setMinimumFractionDigits(0);
	result.setMaximumFractionDigits(0);
	return result;
    }

    private String resultTableHeader() {
	return "SystemName\tMeanAndStdDevScore\tMeanAndStdTestScore\tDevScoreSignificanceInformation\tTestScoreSignificanceInformation";
    }

    public String getSystemName(int systemIndex) {
	return this.devScoresList.get(systemIndex).getSystemName();
    }

    private int maxSystemNameLength() {
	int result = 0;
	for (SystemScores scores : devScoresList.getSystemScoresList()) {
	    result = Math.max(result, scores.getSystemName().length());
	}
	return result;
    }

    private String createFormattedSystemName(String systemName) {
	String result = systemName;
	int extraSpacesRequired = maxSystemNameLength() - systemName.length();
	for (int i = 0; i < extraSpacesRequired; i++) {
	    result += " ";
	}
	return result;
    }

    private String betterTransltationString(SystemScores systemScores) {
	NumberFormat numberFormat = createNoDecimalsNumberFormat();
	DoublePair betterTranslationCountsPair = systemScores.getBetterPerformingSystemsPair();
	String betterTranslationsString = " base-up:"
		+ numberFormat.format(betterTranslationCountsPair.getFirst()) + "_sys-up:"
		+ numberFormat.format(betterTranslationCountsPair.getSecond());
	return betterTranslationsString;
    }

    private String getSignificanceInformationString(SystemScores systemScores, boolean isBaseline) {
	String result = "";
	if (!isBaseline) {

	    result += betterTransltationString(systemScores) + "_pvalue:"
		    + numberFormat.format(systemScores.getNullHypothesisProbability());
	}

	return result;

    }

    public String getImprovementString(SystemScores systemScores) {
	String result;
	if (systemScores.significantImprovementNinetyNinePercentConfidence()) {
	    result = ImprovementStrings.NINETY_NINE_PERCENT_CONFIDENCE_IMPROVEMENT_STRING;
	} else if (systemScores.significantImprovementNinetyFivePercentConfidence()) {
	    result = ImprovementStrings.NINETY_FIVE_PERCENT_CONFIDENCE_IMPROVEMENT_STRING;
	} else if (systemScores.significantWorseningNinetyNinePercentConfidence()) {
	    result = ImprovementStrings.NINETY_NINE_PERCENT_CONFIDENCE_WORSENING_STRING;
	} else if (systemScores.significantWorseningNinetyFivePercentConfidence()) {
	    result = ImprovementStrings.NINETY_FIVE_PERCENT_CONFIDENCE_WORSENING_STRING;
	} else {
	    result = "      ";
	}

	return result;
    }

    
    public String getMeanAndStandardDeviationStringWithoutImprovementString(SystemScores scores,
	    boolean multiplyScoresWithFactorHundred, boolean showStandardDeviation) {

	double mean = scores.getMeanSystemScore();
	double stdev = scores.getStandardDeviationSystemScore();

	if (multiplyScoresWithFactorHundred) {
	    mean *= 100;
	    stdev *= 100;
	}

	String result = "" + numberFormat.format(mean);
	if (showStandardDeviation) {
	    result += PLUS_MINUS_STRING + numberFormat.format(stdev);
	}
	return result;
    }
    
    public String getMeanAndStandardDeviationString(SystemScores scores,
	    boolean multiplyScoresWithFactorHundred, boolean showStandardDeviation) {
	String result = getMeanAndStandardDeviationStringWithoutImprovementString(scores, multiplyScoresWithFactorHundred, showStandardDeviation);
	result += getImprovementString(scores);
	return result;
    }

    private boolean isBaseline(int index) {
	return index == 0;
    }

    public String getMeanAndStandardDeviationStringDev(int systemIndex,
	    boolean multiplyScoresWithFactorHundred, boolean showStandardDeviation) {
	SystemScores devScores = devScoresList.get(systemIndex);
	return getMeanAndStandardDeviationString(devScores,
		multiplyScoresWithFactorHundred, showStandardDeviation);
    }

    public String getMeanAndStandardDeviationStringTest(int systemIndex,
	    boolean multiplyScoresWithFactorHundred, boolean showStandardDeviation) {
	SystemScores testScores = getTestScoresList().get(systemIndex);
	return getMeanAndStandardDeviationString(testScores, 
		multiplyScoresWithFactorHundred, showStandardDeviation);
    }

    public String toStringSpecial(boolean multiplyScoresWithFactorHundred) {
	Assert.assertEquals(devScoresList.size(), getTestScoresList().size());
	String result = "";
	result += resultTableHeader() + "\n";
	for (int systemIndex = 0; systemIndex < devScoresList.size(); systemIndex++) {
	    SystemScores devScores = devScoresList.get(systemIndex);
	    SystemScores testScores = getTestScoresList().get(systemIndex);
	    Assert.assertEquals(devScores.getSystemName(), testScores.getSystemName());
	    result += createFormattedSystemName(devScores.getSystemName())
		    + "\t"
		    + getMeanAndStandardDeviationStringDev(systemIndex,
			    multiplyScoresWithFactorHundred, true)
		    + "\t"
		    + getMeanAndStandardDeviationStringTest(systemIndex,
			    multiplyScoresWithFactorHundred, true) + "\t"
		    + getSignificanceInformationString(devScores, isBaseline(systemIndex)) + "\t"
		    + getSignificanceInformationString(testScores, isBaseline(systemIndex)) + "\n";
	}
	return result;
    }

    public String toString() {
	return toStringSpecial(false);
    }

    public int getNumDevSystems() {
	return this.devScoresList.size();
    }

    public int getNumTestSystems() {
	return this.getTestScoresList().size();
    }

    public SystemScoresList getDevScoresList() {
   	return devScoresList;
       }
    
    public SystemScoresList getTestScoresList() {
	return testScoresList;
    }

}
