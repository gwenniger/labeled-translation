package mt_pipeline.evaluation.lrscoring;

import java.util.ArrayList;
import java.util.List;

public class SystemLRScoresListCreater {

    private static final String DEV = "dev";
    private static final String TEST = "test";

    private final ReferenceInfo referenceInfo;
    private final LRScorerInterface lrScorerInterface;
    private final List<String> systemResultFolderPaths;

    private SystemLRScoresListCreater(ReferenceInfo referenceInfo,
	    LRScorerInterface lrScorerInterface, List<String> systemResultFolderPaths) {
	this.referenceInfo = referenceInfo;
	this.lrScorerInterface = lrScorerInterface;
	this.systemResultFolderPaths = systemResultFolderPaths;
    }

    private static String createFullResultFolderPath(String resultRootFolderPath,
	    String systemName, String resultType) {
	return resultRootFolderPath + systemName + "/" + resultType + "/";
    }

    private static List<String> createSystemResultFolderPathsFromSystemNameList(
	    String resultRootFolderPath, List<String> systemNameList, String resultType) {
	List<String> result = new ArrayList<String>();
	for (String systemName : systemNameList) {
	    result.add(createFullResultFolderPath(resultRootFolderPath, systemName, resultType));
	}
	return result;
    }

    public static SystemLRScoresListCreater createDevSystemLRScoresListCreater(
	    ReferenceInfo referenceInfo, LRScorerInterface lrScorerInterface,
	    String resultRootFolderPath, List<String> systemNameList) {
	return new SystemLRScoresListCreater(referenceInfo, lrScorerInterface,
		createSystemResultFolderPathsFromSystemNameList(resultRootFolderPath,
			systemNameList, DEV));
    }

    public static SystemLRScoresListCreater createTestSystemLRScoresListCreater(
	    ReferenceInfo referenceInfo, LRScorerInterface lrScorerInterface,
	    String resultRootFolderPath, List<String> systemNameList) {
	return new SystemLRScoresListCreater(referenceInfo, lrScorerInterface,
		createSystemResultFolderPathsFromSystemNameList(resultRootFolderPath,
			systemNameList, TEST));
    }

    
    public List<SystemScores> computeSystemScoresList() {
	List<SystemScores> result = new ArrayList<SystemScores>();
	for (String resultFilesFolderPath : systemResultFolderPaths) {
	    SystemLRScoresCreater systemLRScoresCreater = SystemLRScoresCreater
		    .createSystemLRScoresCreater(referenceInfo, resultFilesFolderPath,
			    lrScorerInterface);
	    result.add(systemLRScoresCreater.computeSystemScores());
	}

	return result;
    }

}
