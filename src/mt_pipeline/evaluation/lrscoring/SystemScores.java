package mt_pipeline.evaluation.lrscoring;

import java.util.ArrayList;
import java.util.List;

import mt_pipeline.evaluation.lrscoring.ImprovedSentenceScoresComputer.DoublePair;
import mt_pipeline.evaluation.lrscoring.ImprovedSentenceScoresComputer.ImprovementSignificanceInformation;

public class SystemScores {
    private final String systemName;
    private final List<EvaluationScores> multipleRunSystemScoresList;
    private ImprovementSignificanceInformation significanceInformationNinentyFivePercentConfidence = null;
    private ImprovementSignificanceInformation significanceInformationNinentyNinePercentConfidence = null;

    private SystemScores(String systemName, List<EvaluationScores> multipleRunSystemScoresList) {
	this.systemName = systemName;
	this.multipleRunSystemScoresList = multipleRunSystemScoresList;
    }

    public static SystemScores createSystemScores(String systemName,
	    List<EvaluationScores> multipleRunSystemScoresList) {
	return new SystemScores(systemName, multipleRunSystemScoresList);
    }

    public void computeImprovementSignificanceInformation(SystemScores baselineScores,
	    boolean higerIsBetter) {
	ImprovedSentenceScoresComputer improvementSignificanceComputer = ImprovedSentenceScoresComputer
		.createImprovedSentenceScoresComputer(baselineScores, this, higerIsBetter);

	this.significanceInformationNinentyFivePercentConfidence = improvementSignificanceComputer
		.getImprovementSignificanceInformation(0.05);
	this.significanceInformationNinentyNinePercentConfidence = improvementSignificanceComputer
		.getImprovementSignificanceInformation(0.01);

    }

    public String getSystemName() {
	return systemName;
    }

    public List<EvaluationScores> getSystemScoresList() {
	return multipleRunSystemScoresList;
    }

    private List<Double> getRunScoresList() {
	List<Double> runScores = new ArrayList<Double>();
	for (EvaluationScores evaluationScores : multipleRunSystemScoresList) {
	    runScores.add(evaluationScores.getTotalScore());
	}
	return runScores;
    }

    public double getMeanSystemScore() {

	MeanAndStandardDeviationComputer meanAndStandardDeviationComputer = MeanAndStandardDeviationComputer
		.createMeanAndStandardDeviationComputer(getRunScoresList());
	return meanAndStandardDeviationComputer.computeMeanValue();
    }

    public double getStandardDeviationSystemScore() {

	MeanAndStandardDeviationComputer meanAndStandardDeviationComputer = MeanAndStandardDeviationComputer
		.createMeanAndStandardDeviationComputer(getRunScoresList());
	return meanAndStandardDeviationComputer.computeStandardDeviation();
    }

    public double getSentenceLevelScore(int sentenceIndex, int runIndex) {
	return this.multipleRunSystemScoresList.get(runIndex).getSentenceLevelScore(sentenceIndex);
    }

    public int getNumberOfRuns() {
	return this.multipleRunSystemScoresList.size();
    }

    public int getNumberOfSentences() {
	return this.multipleRunSystemScoresList.get(0).getNumSentenceLevelScores();
    }

    public boolean significantImprovementNinetyFivePercentConfidence() {
	return (this.significanceInformationNinentyFivePercentConfidence != null)
		&& (this.significanceInformationNinentyFivePercentConfidence
			.isSignificantImprovementOverBaseline());
    }

    public boolean significantWorseningNinetyFivePercentConfidence() {
	return (this.significanceInformationNinentyFivePercentConfidence != null)
		&& (this.significanceInformationNinentyFivePercentConfidence
			.isSignificantWorseningOverBaseline());
    }

    public boolean significantImprovementNinetyNinePercentConfidence() {
	return (this.significanceInformationNinentyNinePercentConfidence != null)
		&& (this.significanceInformationNinentyNinePercentConfidence
			.isSignificantImprovementOverBaseline());
    }

    public boolean significantWorseningNinetyNinePercentConfidence() {
	return (this.significanceInformationNinentyNinePercentConfidence != null)
		&& (this.significanceInformationNinentyNinePercentConfidence
			.isSignificantWorseningOverBaseline());
    }

    /**
     * Return the probability of the null hypothesis for the sign test of
     * comparing this system scores with the baseline system scores
     * 
     * @return
     */
    public double getNullHypothesisProbability() {
	return this.significanceInformationNinentyFivePercentConfidence
		.getNullHypothesisProbability();
    }

    public DoublePair getBetterPerformingSystemsPair() {
	return this.significanceInformationNinentyFivePercentConfidence
		.getBetterPerformingSystemCountsAcrossRuns();
    }

}
