package mt_pipeline.evaluation.lrscoring;

import java.util.List;

public class SystemScoresList {

    private final List<SystemScores> systemScoresList;

    private SystemScoresList(List<SystemScores> systemScoresList) {
	this.systemScoresList = systemScoresList;
    }

    private void computeImprovementSignificanceInformation(boolean higherIsBetter) {
	for (SystemScores systemScores : getNonBaselineSystemScores()) {
	    systemScores.computeImprovementSignificanceInformation(getBaselineScores(),
		    higherIsBetter);
	}
    }

    public static SystemScoresList createSystemScoresList(List<SystemScores> systemScoresList,
	    boolean higherIsBetter) {
	SystemScoresList result = new SystemScoresList(systemScoresList);
	result.computeImprovementSignificanceInformation(higherIsBetter);
	return result;
    }

    public int size() {
	return systemScoresList.size();
    }

    public SystemScores get(int index) {
	return systemScoresList.get(index);
    }

    public SystemScores getBaselineScores() {
	return get(0);
    }

    private List<SystemScores> getNonBaselineSystemScores() {
	return systemScoresList.subList(1, systemScoresList.size());
    }

    public List<SystemScores> getSystemScoresList() {
	return systemScoresList;
    }
}
