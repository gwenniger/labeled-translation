package mt_pipeline.evaluation.lrscoring;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import mt_pipeline.evaluation.lrscoring.ReferenceInfo.OutputAlignmentPair;
import junit.framework.Assert;
import util.FileUtil;

public class SystemLRScoresCreater {

    private final List<OutputAlignmentPair> testOutputAlignmentPairs;

    private ReferenceInfo referenceInfo;

    private final LRScorerInterface lrScorerInterface;

    private SystemLRScoresCreater(List<OutputAlignmentPair> testOutputAlignmentPairs,
	    ReferenceInfo referenceInfo, LRScorerInterface lrScorerInterface) {
	this.testOutputAlignmentPairs = testOutputAlignmentPairs;
	this.referenceInfo = referenceInfo;
	this.lrScorerInterface = lrScorerInterface;
	Assert.assertFalse(testOutputAlignmentPairs.isEmpty());
    }

    public static SystemLRScoresCreater createSystemLRScoresCreater(ReferenceInfo referenceInfo,
	    String resultFilesFolderPath, LRScorerInterface lrScorerInterface) {
	OutputAlignmentPairsConstructor outputAlignmentPairsConstructor = OutputAlignmentPairsConstructor
		.createOutputAlignmentPairsConstructor(resultFilesFolderPath);

	return new SystemLRScoresCreater(
		outputAlignmentPairsConstructor.getOutputAlignmentPairsFromResultFolder(),
		referenceInfo, lrScorerInterface);
    }

    private String getSystemName() {
	String firstOutputFilePath = this.testOutputAlignmentPairs.get(0).getOutputFilePath();
	String grandParentFilePath = new File(new File(firstOutputFilePath).getParent()).getParent();
	return new File(grandParentFilePath).getName();
    }

    public SystemScores computeSystemScores() {
	List<EvaluationScores> multipleRunSystemScoresList = new ArrayList<EvaluationScores>();
	for (OutputAlignmentPair outputAlignmentPair : this.testOutputAlignmentPairs) {

	    EvaluationScores evaluationScores = lrScorerInterface.computeLRReorderingOnlyScore(
		    outputAlignmentPair, referenceInfo);
	    multipleRunSystemScoresList.add(evaluationScores);
	}

	return SystemScores.createSystemScores(getSystemName(), multipleRunSystemScoresList);

    }

    protected static int getRunNumber(File file) {
	String numberString = (file.getName().split(".run"))[1];
	return Integer.parseInt(numberString);
    }

    private static class OutputAlignmentPairsConstructor {
	private static final String OUTPUT_FILE_PREFIX = "output.1best.TUNED.run";
	private static final String OUTPUT_ALIGNMENT_FILE_PREFIX = "output.1best.TUNED.alignment.run";

	private final String resultFolderPath;

	private OutputAlignmentPairsConstructor(String resultFolderPath) {
	    this.resultFolderPath = resultFolderPath;
	}

	public static OutputAlignmentPairsConstructor createOutputAlignmentPairsConstructor(
		String resultFolderPath) {
	    return new OutputAlignmentPairsConstructor(resultFolderPath);
	}

	private boolean isOutputFile(File file) {
	    return file.getName().startsWith(OUTPUT_FILE_PREFIX);
	}

	private boolean isOutputAlignmentFile(File file) {
	    return file.getName().startsWith(OUTPUT_ALIGNMENT_FILE_PREFIX);
	}

	private static void sortFilesByRunNumber(List<File> fileList) {
	    fileList.sort(new OutputFileComparator());
	}

	private List<File> getOutputFilesSortedByRunNumber(List<File> allFilesInFolder) {
	    List<File> result = new ArrayList<File>();

	    for (File file : allFilesInFolder) {
		if (isOutputFile(file)) {
		    result.add(file);
		}
	    }
	    sortFilesByRunNumber(result);
	    return result;
	}

	private List<File> getOutputAlignmentFilesSortedByRunNumber(List<File> allFilesInFolder) {
	    List<File> result = new ArrayList<File>();

	    for (File file : allFilesInFolder) {
		if (isOutputAlignmentFile(file)) {
		    result.add(file);
		}
	    }
	    sortFilesByRunNumber(result);
	    return result;
	}

	public List<OutputAlignmentPair> getOutputAlignmentPairsFromResultFolder() {
	    List<OutputAlignmentPair> result = new ArrayList<OutputAlignmentPair>();
	    System.out.println("resultFolderPath: "  +resultFolderPath);
	    List<File> allFilesInFolder = FileUtil.getAllFilesInFolder(new File(resultFolderPath),
		    false);
	    Assert.assertFalse(allFilesInFolder.isEmpty());
	    List<File> outputFilesSortedByRunNumber = getOutputFilesSortedByRunNumber(allFilesInFolder);
	    List<File> outputAlignmentFilesSortedByRunNumber = getOutputAlignmentFilesSortedByRunNumber(allFilesInFolder);
	    Assert.assertEquals(outputFilesSortedByRunNumber.size(),
		    outputAlignmentFilesSortedByRunNumber.size());
	    Assert.assertFalse(outputAlignmentFilesSortedByRunNumber.isEmpty());
	    for (int i = 0; i < outputFilesSortedByRunNumber.size(); i++) {
		File outputFile = outputFilesSortedByRunNumber.get(i);
		File outputAlignmentFile = outputAlignmentFilesSortedByRunNumber.get(i);
		Assert.assertEquals(i + 1, getRunNumber(outputFile));
		Assert.assertEquals(i + 1, getRunNumber(outputAlignmentFile));
		result.add(new OutputAlignmentPair(outputFile.getAbsolutePath(),
			outputAlignmentFile.getAbsolutePath()));
	    }
	    Assert.assertFalse(result.isEmpty());
	    return result;
	}

    }

    private static class OutputFileComparator implements Comparator<File> {

	@Override
	public int compare(File file1, File file2) {
	    Integer file1RunIndex = SystemLRScoresCreater.getRunNumber(file1);
	    Integer file2RunIndex = SystemLRScoresCreater.getRunNumber(file2);
	    return Integer.compare(file1RunIndex, file2RunIndex);
	}
    }

}
