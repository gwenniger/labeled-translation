package mt_pipeline.evaluation.lrscoring;

import java.util.List;

public class MeanAndStandardDeviationComputer {

    private final List<Double> values;

    private MeanAndStandardDeviationComputer(List<Double> values) {
	this.values = values;
    }

    public static MeanAndStandardDeviationComputer createMeanAndStandardDeviationComputer(
	    List<Double> values) {
	return new MeanAndStandardDeviationComputer(values);
    }

    private int getNumValues() {
	return values.size();
    }

    public double computeMeanValue() {
	double total = 0;
	for (Double value : values) {
	    total += value;
	}
	return (total / getNumValues());
    }

    private double computeSquaredDifference(double val1, double val2) {
	return (val1 - val2) * (val1 - val2);
    }

    private double computeSumSquaredDifferencesWithMean() {
	double result = 0;
	double mean = computeMeanValue();
	for (Double value : values) {
	    result += computeSquaredDifference(value, mean);
	}
	return result;
    }

    public double computeStandardDeviation() {
	// Be ware that we compute the sample standard deviation, which is
	// normalized by n-1 rather than n
	double result = Math.sqrt((computeSumSquaredDifferencesWithMean() / (getNumValues() - 1)));
	return result;
    }
}
