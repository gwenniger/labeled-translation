package mt_pipeline.evaluation.lrscoring;

import junit.framework.Assert;

import org.apache.commons.math3.util.ArithmeticUtils;

public class SignTest {

    /**
     * This is the method we actually need to use when we want to reject the null 
     * hypothesis "Both systems translate the best translation with equal probability of 0.5"
     * This is also known as the "Two-Sided" sign test
     * @param n
     * @param k
     * @return
     */
    public static double computeProbabilityAnySystemBetterForUpToKSentencesGivenTwoSystemsEqual(
	    int n, int k) {
	int kMin = Math.min(k, n - k);
	return 2 * computeProbabilityOneSpecificSystemBetterForUpToKSentencesGivenTwoSystemsEqual(n, kMin);
    }

    public static double computeProbabilityOneSpecificSystemBetterForUpToKSentencesGivenTwoSystemsEqual(
	    int n, int k) {
	double result = 0;

	for (int i = 0; i <= k; i++) {
	    Assert.assertTrue(i <= n);
	    // System.out.println("totalNumSentences: " + totalNumSentences
	    // + " i: " + i);
	    double binomialCoefficientLog = ArithmeticUtils.binomialCoefficientLog(n, i);
	    // System.out.println("binomialCoefficientLog: " +
	    // binomialCoefficientLog);
	    //double exponentLogProb = Math.log(Math.pow(0.5, n));
	    double alternative = n * Math.log(0.5); // equivalent to
	    // exponentLogProb
	    // System.out.println("expenentLogProg:" + exponentLogProb +
	    // " alternative: "
	    // + alternative);

//	    double logProbToAdd = binomialCoefficientLog + exponentLogProb;
	    double logProbToAdd = binomialCoefficientLog + alternative;
	    double probToAdd = Math.pow(Math.E, logProbToAdd);
	    //System.out.println("probToAdd: " + probToAdd);
	    result += probToAdd;
	    // System.out.println("result: " + result);
	}

	// System.out.println("final result:" + result);
	return result;
    }
}
