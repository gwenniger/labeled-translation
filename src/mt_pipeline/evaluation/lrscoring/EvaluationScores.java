package mt_pipeline.evaluation.lrscoring;

import java.util.List;

public class EvaluationScores {
    private final double totalScore;
    private final List<Double> sentenceLevelScores;

    private EvaluationScores(double totalScore, List<Double> sentenceLevelScores) {
	this.totalScore = totalScore;
	this.sentenceLevelScores = sentenceLevelScores;
    }

    public static EvaluationScores createEvaluationScores(double totalScore,
	    List<Double> sentenceLevelScores) {
	return new EvaluationScores(totalScore, sentenceLevelScores);
    }

    public double getTotalScore() {
	return totalScore;
    }

    public double getSentenceLevelScore(int sentenceIndex) {
	return this.sentenceLevelScores.get(sentenceIndex);
    }

    public int getNumSentenceLevelScores() {
	return this.sentenceLevelScores.size();
    }

    public String toString() {
	String result = "";
	int index = 0;
	result += "<Sentence level scores>" + "\n";
	for (Double score : sentenceLevelScores) {
	    result += "sentence " + index + " score: " + score + "\n";
	    index++;
	}
	result += "</Sentence level scores>" + "\n";
	result += "\nTotal score:\n" + totalScore;
	return result;
    }
}
