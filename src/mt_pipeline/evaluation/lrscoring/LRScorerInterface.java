package mt_pipeline.evaluation.lrscoring;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import util.ConfigFile;
import util.LinuxInteractor;
import mt_pipeline.evaluation.lrscoring.ReferenceInfo.OutputAlignmentPair;

public class LRScorerInterface {
    private static final String LR_SCORE_BIN_DIR = "build";
    private static final String LR_SCORE_MAIN_PROGRAM_NAME = "lrscore";

    private static final String TEST_TRANSLATION_INDICATOR = "-t";
    private static final String REF_TRANSLATION_INDICATOR = "-r";
    private static final String TEST_TRANSLATION_ALIGNMENT_INDICATOR = "-a";
    private static final String REF_TRANSLATION_ALIGNMENT_INDICATOR = "-A";
    private static final String SORUCE_FILE_INDICATOR = "-f";
    private static final String SCORER_TYPE_INDICATOR = "-s";
    private static final String OUTPUT_FILE_PATH_INDICATOR = "-o";
    private static final String KENDALL = "KENDALL";

    private static final String ARGSEP = " ";
    private static final String VERBOSITY_TYPE_INDICATOR = "-v";
    private static final int VERBOSITY_LEVEL = 1;

    private static final String TEMP_RESULT_FILE_NAME = "lrScoreOutput.temp";
    private static final String LR_SCORES_HEADER = "Sentence Number and SCORES:";
    private static final String LR_SCORES_TOTALS_HEADER = "TOTALS";

    private final String lrScoreRootDir;
    private final String lrScoreWorkDir;

    private LRScorerInterface(String lrScoreRootDir, String lrScoreWorkDir) {
	this.lrScoreRootDir = lrScoreRootDir;
	this.lrScoreWorkDir = lrScoreWorkDir;
    }

    public static LRScorerInterface createLrScorerInterface(String lrScoreRootDir,
	    String lrScoreWorkDir) {
	return new LRScorerInterface(lrScoreRootDir, lrScoreWorkDir);
    }

    public static LRScorerInterface createLRLrScorerInterface(LRScoreConfigFile lrScoreConfigFile) {
	return LRScorerInterface.createLrScorerInterface(lrScoreConfigFile.getLRScoreRootDir(),
		lrScoreConfigFile.getLRScoreWorkDir());
    }

    private String lrScoreBaseCommand() {
	return lrScoreRootDir + LR_SCORE_BIN_DIR + "/" + LR_SCORE_MAIN_PROGRAM_NAME;
    }

    private String testTranslationArgument(String testOutputFilePath) {
	return TEST_TRANSLATION_INDICATOR + ARGSEP + testOutputFilePath;
    }

    private String refTranslationArgument(String refOutputFilePath) {
	return REF_TRANSLATION_INDICATOR + ARGSEP + refOutputFilePath;
    }

    private String testTranslationAlignmentArgument(String testAlignmentOutputFilePath) {
	return TEST_TRANSLATION_ALIGNMENT_INDICATOR + ARGSEP + testAlignmentOutputFilePath;
    }

    private String refTranslationAlignmentArgument(String refAlignmentOutputFilePath) {
	return REF_TRANSLATION_ALIGNMENT_INDICATOR + ARGSEP + refAlignmentOutputFilePath;
    }

    private String sourceFileArgument(String sourceFilePath) {
	return SORUCE_FILE_INDICATOR + ARGSEP + sourceFilePath;
    }

    private String scorerTypeString() {
	return SCORER_TYPE_INDICATOR + ARGSEP + KENDALL;
    }

    private String outputArgumentString(String outputFilePath) {
	return OUTPUT_FILE_PATH_INDICATOR + ARGSEP + outputFilePath;
    }

    private String verbosityString() {
	return VERBOSITY_TYPE_INDICATOR + ARGSEP + VERBOSITY_LEVEL;
    }

    private String tempResultFilePath() {
	return lrScoreWorkDir + TEMP_RESULT_FILE_NAME;
    }

    private String lrReorderingOnlyScoreCommand(String testOutputFilePath,
	    String refOutputFilePath, String testAlignmentOutputFilePath,
	    String refAlignmentOutputFilePath, String sourceFilePath, String outputFilePath) {
	// ./build/lrscore -t test/simpleTest -r test/simpleRef -a
	// test/simpleTestAlign -A test/simpleRefAlign -f test/simpleSource -v 1
	// -s KENDALL -o test/simpleOutK -v 1

	return lrScoreBaseCommand() + ARGSEP + testTranslationArgument(testOutputFilePath) + ARGSEP
		+ refTranslationArgument(refOutputFilePath) + ARGSEP
		+ testTranslationAlignmentArgument(testAlignmentOutputFilePath) + ARGSEP
		+ refTranslationAlignmentArgument(refAlignmentOutputFilePath) + ARGSEP
		+ sourceFileArgument(sourceFilePath) + ARGSEP + scorerTypeString() + ARGSEP
		+ outputArgumentString(outputFilePath) + ARGSEP + verbosityString();

    }

    public EvaluationScores computeLRReorderingOnlyScore(
	    OutputAlignmentPair testOutputAlignmentPair, ReferenceInfo referenceInfo) {

	OutputAlignmentPair firstOutputAlignmentPair = referenceInfo.getRefOutputAlignmentPairs()
		.get(0);

	String testOutputFilePath = testOutputAlignmentPair.getOutputFilePath();
	String testAlignmentOutputFilePath = testOutputAlignmentPair.getAlignmentOutputFilePath();
	String refOutputFilePath = firstOutputAlignmentPair.getOutputFilePath();
	String refAlignmentOutputFilePath = firstOutputAlignmentPair.getAlignmentOutputFilePath();

	String outputFilePath = tempResultFilePath();
	String lrReorderingOnlyScoreCommand = lrReorderingOnlyScoreCommand(testOutputFilePath,
		refOutputFilePath, testAlignmentOutputFilePath, refAlignmentOutputFilePath,
		referenceInfo.getSourceFilePath(), outputFilePath);

	LinuxInteractor.executeExtendedCommandDisplayOutput(lrReorderingOnlyScoreCommand);
	return readLRScoresFromResultFile();
    }

    private boolean isLRScoresHeader(String line) {
	return line.startsWith(LR_SCORES_HEADER);
    }

    private boolean isLRScoresTotalsHeader(String line) {
	return line.startsWith(LR_SCORES_TOTALS_HEADER);
    }

    private void readLinesUntilScoresBegin(LineIterator it) {
	// Read lines until score header is found
	boolean scoreHeaderRead = false;
	while ((!scoreHeaderRead) && it.hasNext()) {
	    String line = it.nextLine();
	    if (isLRScoresHeader(line)) {
		scoreHeaderRead = true;
	    }
	}
    }

    private List<Double> readSentenceScoresUntilEmptyLineFound(LineIterator it) {
	List<Double> sentenceScores = new ArrayList<Double>();
	boolean lineIsEmpty = false;
	while ((!lineIsEmpty) && it.hasNext()) {
	    String line = it.nextLine();
	    if (line.isEmpty()) {
		lineIsEmpty = true;
	    } else {
		String[] parts = line.split("\\s");
		Double score = Double.parseDouble(parts[1]);
		sentenceScores.add(score);
	    }
	}
	return sentenceScores;
    }

    private void readLinesUntilTotalsHeaderRead(LineIterator it) {
	boolean totalsHeaderRead = false;
	while ((!totalsHeaderRead) && it.hasNext()) {
	    String line = it.nextLine();
	    if (isLRScoresTotalsHeader(line)) {
		totalsHeaderRead = true;
	    }
	}
    }

    private EvaluationScores readLRScoresFromResultFile() {
	LineIterator it = null;
	try {
	    it = FileUtils.lineIterator(new File(tempResultFilePath()), "UTF-8");

	    readLinesUntilScoresBegin(it);
	    List<Double> sentenceLevelScores = readSentenceScoresUntilEmptyLineFound(it);
	    readLinesUntilTotalsHeaderRead(it);
	    Assert.assertTrue(it.hasNext());
	    double totalScore = Double.parseDouble(it.nextLine());
	    return EvaluationScores.createEvaluationScores(totalScore, sentenceLevelScores);

	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	} finally {
	    if (it != null) {
		it.close();
	    }
	}
    }

    public static class LRScoreConfigFile extends ConfigFile {

	private static final String MULTEVAL_SCRIPT_SYSTEM_SPECIFICATION_PREFIX = "--hyps-";

	private static final String LR_SCORE_ROOT_DIR_PROPERTY = "lrScoreRootDir";
	private static final String LR_SCORE_WORK_DIR_PROPERTY = "lrScoreWorkDir";
	private static final String TEST_OUTPUT_FILE_PATH_PROPERTY = "testOutputFilePath";
	private static final String REF_OUTPUT_FILE_PATH_PROPERTY = "refOutputFilePath";
	private static final String TEST_ALIGNMENT_OUTPUT_FILE_PATH_PROPERTY = "testAlignmentOutputFilePath";
	private static final String REF_ALIGNMENT_OUTPUT_FILE_PATH_PROPERTY = "refAlignmentOutputFilePath";
	private static final String SOURCE_FILE_PATH_PROPERTY = "sourceFilePath";

	private static final String RESULT_ROOT_FOLDER_PATH_PROPERYY = "resultRootFolderPath";
	private static final String USED_MULTEVAL_EVALUATION_SCRIPT_FILE_PATH = "multevalEvaluationScriptFile";

	public LRScoreConfigFile(String inputFile) throws FileNotFoundException {
	    super(inputFile);
	}

	public static LRScoreConfigFile createLRScoreConfigFile(String configFilePath) {
	    try {
		return new LRScoreConfigFile(configFilePath);
	    } catch (FileNotFoundException e) {
		e.printStackTrace();
		throw new RuntimeException(e);
	    }
	}

	public String getResultRootFolderPath() {
	    return getValueWithPresenceCheck(RESULT_ROOT_FOLDER_PATH_PROPERYY);
	}

	private boolean isSystemSpecificationLine(String line) {
	    boolean result = (line.trim()).startsWith(MULTEVAL_SCRIPT_SYSTEM_SPECIFICATION_PREFIX);
	    // System.out.println("isSystemSpecificationLine? : " + line + " :"
	    // + result);
	    return result;
	}

	private String extractSystemNameFromSystemNameSpecificationLine(
		String systemNameSpecificationLine) {
	    String[] parts = systemNameSpecificationLine.split("/");
	    String systemNamePart = parts[parts.length - 3];
	    String result = systemNamePart.trim();
	    System.out.println("System name: " + result);
	    return result;
	}

	public List<String> extractSystemNameListFromMultevalScript(String multevalScriptFilePath) {
	    try {
		List<String> result = new ArrayList<String>();
		LineIterator lineIterator = FileUtils.lineIterator(
			new File(multevalScriptFilePath), "UTF8");
		while (lineIterator.hasNext()) {
		    String line = lineIterator.nextLine();
		    if (isSystemSpecificationLine(line)) {
			result.add(extractSystemNameFromSystemNameSpecificationLine(line));
		    }
		}
		return result;

	    } catch (IOException e) {
		e.printStackTrace();
		throw new RuntimeException(e);
	    }

	}

	public List<String> getSystemNameList() {
	    String multevalScriptFilePath = getValueWithPresenceCheck(USED_MULTEVAL_EVALUATION_SCRIPT_FILE_PATH);
	    return extractSystemNameListFromMultevalScript(multevalScriptFilePath);
	}

	public ReferenceInfo createReferenceInfo() {
	    OutputAlignmentPair firstOutputAlignmentPair = new OutputAlignmentPair(
		    getRefOutputFilePath(), getRefAlignmentOutputFilePath());
	    return ReferenceInfo.createReferenceInfo(getSourceFilePath(),
		    Collections.singletonList(firstOutputAlignmentPair));
	}

	public String getLRScoreRootDir() {
	    return getValueWithPresenceCheck(LR_SCORE_ROOT_DIR_PROPERTY);
	}

	public String getLRScoreWorkDir() {
	    return getValueWithPresenceCheck(LR_SCORE_WORK_DIR_PROPERTY);
	}

	private String getRefOutputFilePath() {
	    return getValueWithPresenceCheck(REF_OUTPUT_FILE_PATH_PROPERTY);
	}

	private String getTestOutputFilePath() {
	    return getValueWithPresenceCheck(TEST_OUTPUT_FILE_PATH_PROPERTY);
	}

	public String getTestAlignmentOutputFilePath() {
	    return getValueWithPresenceCheck(TEST_ALIGNMENT_OUTPUT_FILE_PATH_PROPERTY);
	}

	public OutputAlignmentPair getTestOutputAlignmentPair() {
	    return new OutputAlignmentPair(getTestOutputFilePath(),
		    getTestAlignmentOutputFilePath());
	}

	public String getRefAlignmentOutputFilePath() {
	    return getValueWithPresenceCheck(REF_ALIGNMENT_OUTPUT_FILE_PATH_PROPERTY);
	}

	@Override
	public String getSourceFilePath() {
	    return getValueWithPresenceCheck(SOURCE_FILE_PATH_PROPERTY);
	}
    }

    private static void computLRScoreForLRScoreConfig(LRScoreConfigFile lrScoreConfigFile) {
	LRScorerInterface lrScorerInterface = createLrScorerInterface(
		lrScoreConfigFile.getLRScoreRootDir(), lrScoreConfigFile.getLRScoreWorkDir());
	EvaluationScores scores = lrScorerInterface.computeLRReorderingOnlyScore(
		lrScoreConfigFile.getTestOutputAlignmentPair(),
		lrScoreConfigFile.createReferenceInfo());
	System.out.println("Scores: " + scores);
    }

    public static void main(String[] args) {

	if (args.length != 1) {
	    System.out.println("Usage: LRSCoreInterface CONFIG");
	    System.exit(1);
	}

	computLRScoreForLRScoreConfig(LRScoreConfigFile.createLRScoreConfigFile(args[0]));

    }
}
