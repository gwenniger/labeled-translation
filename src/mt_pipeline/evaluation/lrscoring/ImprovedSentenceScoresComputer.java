package mt_pipeline.evaluation.lrscoring;

import junit.framework.Assert;
import util.Pair;

public class ImprovedSentenceScoresComputer {

    private final DoublePair betterPerformingSystemCountsAcrossRuns;
    private final double probabilityOneSystemBetterForUpToKSentences;

    private ImprovedSentenceScoresComputer(DoublePair betterPerformingSystemCountsAcrossRun,
	    double probabilityOneSystemBetterForUpToKSentences) {
	this.betterPerformingSystemCountsAcrossRuns = betterPerformingSystemCountsAcrossRun;
	this.probabilityOneSystemBetterForUpToKSentences = probabilityOneSystemBetterForUpToKSentences;
	Assert.assertTrue(probabilityOneSystemBetterForUpToKSentences >= 0);
    }

    private static double computNumberOfTies(SystemScores baselineScores,
	    DoublePair betterPerformingSystemCountsAcrossRuns) {
	return baselineScores.getNumberOfSentences()
		- betterPerformingSystemCountsAcrossRuns.getSumComponents();
    }

    public static ImprovedSentenceScoresComputer createImprovedSentenceScoresComputer(
	    SystemScores baselineScores, SystemScores secondSystemSores, boolean higerIsBetter) {
	SufficientStatisticsComputer sufficientStatisticsComputer = new SufficientStatisticsComputer(
		baselineScores, secondSystemSores, higerIsBetter);
	DoublePair betterPerformingSystemCountsAcrossRuns = sufficientStatisticsComputer
		.computeBetterPerformingSystemCountPairsAcrossRuns();

	double numberOfTies = computNumberOfTies(baselineScores,
		betterPerformingSystemCountsAcrossRuns);
	int halfNumberOfTiesRoundedDown = (int) (numberOfTies / 2);
	// Add the ties, half of them to each system
	betterPerformingSystemCountsAcrossRuns.add(new DoublePair(new Double(
		halfNumberOfTiesRoundedDown), new Double(halfNumberOfTiesRoundedDown)));

	double totalNumberOfSentencesWithDifferentlyPerformingSystems = betterPerformingSystemCountsAcrossRuns
		.getFirst() + betterPerformingSystemCountsAcrossRuns.getSecond();
	// round the number of sentences improved down
	double leastPerformingSystemBetterSentences = Math.min(
		betterPerformingSystemCountsAcrossRuns.getFirst(),
		betterPerformingSystemCountsAcrossRuns.getSecond());
	int k = (int) (Math.ceil(leastPerformingSystemBetterSentences));
	int n = (int) (Math.floor(totalNumberOfSentencesWithDifferentlyPerformingSystems));

	double probabilityOneSystemBetterForUpToKSentences = SignTest
		.computeProbabilityAnySystemBetterForUpToKSentencesGivenTwoSystemsEqual(n, k);
	return new ImprovedSentenceScoresComputer(betterPerformingSystemCountsAcrossRuns,
		probabilityOneSystemBetterForUpToKSentences);

    }

    private boolean significantDifference(double pLevel) {
	Assert.assertTrue(probabilityOneSystemBetterForUpToKSentences >= 0);
	return probabilityOneSystemBetterForUpToKSentences <= pLevel;
    }

    public boolean significantImprovement(double pLevel) {
	return significantDifference(pLevel)
		&& (betterPerformingSystemCountsAcrossRuns.getFirst() < betterPerformingSystemCountsAcrossRuns
			.getSecond());
    }

    public boolean significantWorsening(double pLevel) {
	return significantDifference(pLevel)
		&& (betterPerformingSystemCountsAcrossRuns.getFirst() > betterPerformingSystemCountsAcrossRuns
			.getSecond());
    }

    public ImprovementSignificanceInformation getImprovementSignificanceInformation(double pLevel) {
	return new ImprovementSignificanceInformation(betterPerformingSystemCountsAcrossRuns,
		significantImprovement(pLevel), significantWorsening(pLevel),
		probabilityOneSystemBetterForUpToKSentences);
    }

    public static class ImprovementSignificanceInformation {
	private final DoublePair betterPerformingSystemCountsAcrossRuns;
	private final double nullHypothesisProbability;
	private final boolean significantImprovementOverBaseline;
	private final boolean significantWorseningOverBaseline;

	private ImprovementSignificanceInformation(
		DoublePair betterPerformingSystemCountsAcrossRuns,
		boolean significantImprovementOverBaseline,
		boolean significantWorseningOverBaseline, double nullHypothesisProbability) {
	    this.betterPerformingSystemCountsAcrossRuns = betterPerformingSystemCountsAcrossRuns;
	    this.significantImprovementOverBaseline = significantImprovementOverBaseline;
	    this.significantWorseningOverBaseline = significantWorseningOverBaseline;
	    this.nullHypothesisProbability = nullHypothesisProbability;
	}

	public boolean isSignificantWorseningOverBaseline() {
	    return significantWorseningOverBaseline;
	}

	public boolean isSignificantImprovementOverBaseline() {
	    return significantImprovementOverBaseline;
	}

	public double getNullHypothesisProbability() {
	    return nullHypothesisProbability;
	}

	public DoublePair getBetterPerformingSystemCountsAcrossRuns() {
	    return betterPerformingSystemCountsAcrossRuns;
	}
    }

    private static class SufficientStatisticsComputer {
	private final SystemScores baselineScores;
	private final SystemScores secondSystemSores;
	private final boolean higerIsBetter;

	private SufficientStatisticsComputer(SystemScores baselineScores,
		SystemScores secondSystemSores, boolean higerIsBetter) {
	    this.baselineScores = baselineScores;
	    this.secondSystemSores = secondSystemSores;
	    this.higerIsBetter = higerIsBetter;
	}

	private DoublePair secondSystemOverBaselineImprovementForSentenceRunCombination(
		int sentenceNumber, int baselineRunIndex, int secondSystemRunIndex) {
	    double baselineSentenceScore = baselineScores.getSentenceLevelScore(sentenceNumber,
		    baselineRunIndex);
	    double secondSystemSentenceScore = secondSystemSores.getSentenceLevelScore(
		    sentenceNumber, secondSystemRunIndex);

	    boolean baseleineIsBetter = (baselineSentenceScore > secondSystemSentenceScore);
	    boolean secondSystemIsBetter = (baselineSentenceScore < secondSystemSentenceScore);

	    if (!higerIsBetter) {
		secondSystemIsBetter = !secondSystemIsBetter;
		baseleineIsBetter = !baseleineIsBetter;
	    }

	    if (baseleineIsBetter) {
		return new DoublePair(1.0, 0.0);
	    } else if (secondSystemIsBetter) {
		return new DoublePair(0.0, 1.0);
	    } else {
		return new DoublePair(0.0, 0.0);
	    }
	}

	private DoublePair secondSystemOverBaselineImprovementForSentenceAcrossRuns(
		int sentenceNumber) {
	    DoublePair result = new DoublePair(0.0, 0.0);

	    int numPairs = baselineScores.getNumberOfRuns() * secondSystemSores.getNumberOfRuns();
	    double weightPerPair = 1.0 / numPairs;
	    for (int baselineRunIndex = 0; baselineRunIndex < baselineScores.getNumberOfRuns(); baselineRunIndex++) {
		for (int secondSystemRunIndex = 0; secondSystemRunIndex < secondSystemSores
			.getNumberOfRuns(); secondSystemRunIndex++) {
		    DoublePair doublePairToAdd = secondSystemOverBaselineImprovementForSentenceRunCombination(
			    sentenceNumber, baselineRunIndex, secondSystemRunIndex);
		    doublePairToAdd.multiplyWithScalar(weightPerPair);
		    result.add(doublePairToAdd);
		}
	    }
	    return result;
	}

	private DoublePair computeBetterPerformingSystemCountPairsAcrossRuns() {
	    DoublePair result = new DoublePair(0.0, 0.0);
	    for (int sentenceNumber = 0; sentenceNumber < this.baselineScores
		    .getNumberOfSentences(); sentenceNumber++) {
		result.add(secondSystemOverBaselineImprovementForSentenceAcrossRuns(sentenceNumber));
	    }
	    return result;
	}

    }

    public static class DoublePair extends Pair<Double> {

	public DoublePair(Double first, Double second) {
	    super(first, second);
	}

	private void multiplyWithScalar(double factor) {
	    this.setFirst(factor * this.getFirst());
	    this.setSecond(factor * this.getSecond());
	}

	private void add(DoublePair secondPair) {
	    this.setFirst(this.getFirst() + secondPair.getFirst());
	    this.setSecond(this.getSecond() + secondPair.getSecond());
	}

	public double getSumComponents() {
	    return this.getFirst().doubleValue() + this.getSecond().doubleValue();
	}
    }

}
