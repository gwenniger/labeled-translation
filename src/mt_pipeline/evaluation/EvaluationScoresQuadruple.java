package mt_pipeline.evaluation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;
import util.Pair;

import mt_pipeline.mt_config.MTConfigFile;

public class EvaluationScoresQuadruple {
	protected final double devBaseScore, devTunedScore, testBaseScore, testTunedScore;

	private EvaluationScoresQuadruple(double devBaseScore, double devTunedScore, double testBaseScore, double testTunedScore) {
		this.devBaseScore = devBaseScore;
		this.devTunedScore = devTunedScore;
		this.testBaseScore = testBaseScore;
		this.testTunedScore = testTunedScore;
	}

	public static EvaluationScoresQuadruple createNistScores(MTConfigFile theConfig) {
		double devBaseScore, devTunedScore, testBaseScore, testTunedScore;
		try {
			devBaseScore = getNistFromResultFile(theConfig.filesConfig.getResultPathForCategory(false, MTConfigFile.DEV_CATEGORY));
			devTunedScore = getNistFromResultFile(theConfig.filesConfig.getResultPathForCategory(true, MTConfigFile.DEV_CATEGORY));
			testBaseScore = getNistFromResultFile(theConfig.filesConfig.getResultPathForCategory(false, MTConfigFile.TEST_CATEGORY));
			testTunedScore = getNistFromResultFile(theConfig.filesConfig.getResultPathForCategory(true, MTConfigFile.TEST_CATEGORY));

			EvaluationScoresQuadruple result = new EvaluationScoresQuadruple(devBaseScore, devTunedScore, testBaseScore, testTunedScore);
			return result;
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public static EvaluationScoresQuadruple createBleuScores(MTConfigFile theConfig) {
		double devBaseScore, devTunedScore, testBaseScore, testTunedScore;
		try {
			devBaseScore = getBleuFromResultFile(theConfig.filesConfig.getResultPathForCategory(false, MTConfigFile.DEV_CATEGORY));
			devTunedScore = getBleuFromResultFile(theConfig.filesConfig.getResultPathForCategory(true, MTConfigFile.DEV_CATEGORY));
			testBaseScore = getBleuFromResultFile(theConfig.filesConfig.getResultPathForCategory(false, MTConfigFile.TEST_CATEGORY));
			testTunedScore = getBleuFromResultFile(theConfig.filesConfig.getResultPathForCategory(true, MTConfigFile.TEST_CATEGORY));

			EvaluationScoresQuadruple result = new EvaluationScoresQuadruple(devBaseScore, devTunedScore, testBaseScore, testTunedScore);
			return result;
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public static boolean isDouble(String s) {
		try {
			Double.parseDouble(s);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	private static Pair<Double> getNistAndBleuFromResultLine(String resultLine) {
		String[] parts = resultLine.split(" ");
		List<Double> numbers = new ArrayList<Double>();

		for (String part : parts) {
			if (isDouble(part)) {
				numbers.add(Double.parseDouble(part));
			}
		}
		Assert.assertEquals(2, numbers.size());

		Pair<Double> result = new Pair<Double>(numbers.get(0), numbers.get(1));

		return result;
	}

	private static Pair<Double> getNistAndBleuLineFromResultFile(String resultFileName) throws IOException {
		BufferedReader inputReader = new BufferedReader(new FileReader(resultFileName));

		try {
			String line = null;
			while ((line = inputReader.readLine()) != null) {
				if (line.startsWith("NIST score = ")) {

					return getNistAndBleuFromResultLine(line);
				}
			}

			throw new RuntimeException("Exception: No valid result line could be found");
		} finally {
			inputReader.close();
		}
	}

	public static double getNistFromResultFile(String resultFileName) throws IOException {
		return getNistAndBleuLineFromResultFile(resultFileName).getFirst();
	}

	public static double getBleuFromResultFile(String resultFileName) throws IOException {
		return getNistAndBleuLineFromResultFile(resultFileName).getSecond();
	}

}
