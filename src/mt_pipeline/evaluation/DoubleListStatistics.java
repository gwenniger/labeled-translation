package mt_pipeline.evaluation;

import java.util.Collections;
import java.util.List;
import util.MeanStdComputation;

public class DoubleListStatistics {
    private final double mean;
    private final double std;
    private final double min;
    private final double max;

    private DoubleListStatistics(double mean, double std, double min, double max) {
	this.mean = mean;
	this.std = std;
	this.min = min;
	this.max = max;
    }

    static DoubleListStatistics createDoublesListStatistics(List<Double> values) {
	double mean = MeanStdComputation.computeDoublesMean(values);
	double std = MeanStdComputation.computeDoublesStd(values);
	double min = Collections.min(values);
	double max = Collections.max(values);
	return new DoubleListStatistics(mean, std, min, max);
    }

    public double getMean() {
	return mean;
    }

    public double getStd() {
	return std;
    }

    public double getMin() {
	return min;
    }

    public double getMax() {
	return max;
    }

}