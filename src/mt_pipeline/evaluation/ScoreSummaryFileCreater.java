package mt_pipeline.evaluation;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mt_pipeline.MTPipeline;
import mt_pipeline.mt_config.MTConfigFile;

public class ScoreSummaryFileCreater {
	private static String BasicDataLabel = "No Tuning";
	private static String TunedDataLabel = "Tuned";
	private static String DevSetLabel = "DevSet";
	private static String TestSetLabel = "TestSet";
	private static String LatexBreakSymbol = "\\\\";
	
	final List<MTConfigFile> pipelineConfigFiles;
	
	private ScoreSummaryFileCreater(List<MTConfigFile> pipelineConfigFiles) {
		this.pipelineConfigFiles = pipelineConfigFiles;
	}

	public static ScoreSummaryFileCreater createScoreSummaryFileCreater(List<MTPipeline> pipelines)
	{
		return new ScoreSummaryFileCreater(getConfigFilesForPipelines(pipelines));
	}


	private static List<MTConfigFile> getConfigFilesForPipelines(List<MTPipeline> pipelines)
	{
		 List<MTConfigFile> result = new ArrayList<MTConfigFile>();
		 for(MTPipeline pipeline : pipelines)
		 {
			 result.add(pipeline.getTheConfig());
		 }
		 
		 return result;
	}
	

	private String createScoresTable(EvaluationScoresQuadruple scores, String category) {
		String result =

		"\\begin{center}" + "\n" + "\t\\begin{tabular}{ | l | l | l |}" + "\n" + "\\multicolumn{3}{|c|}{" + category + "} " + LatexBreakSymbol + "\n" + "\t\\hline" + "\n"
				+ "\tData & " + BasicDataLabel + " & " + TunedDataLabel + LatexBreakSymbol + "\n" + "\t" + DevSetLabel + " & " + scores.devBaseScore + " & "
				+ scores.devTunedScore + LatexBreakSymbol + "\n" + "\t" + TestSetLabel + " & " + scores.testBaseScore + " & " + scores.testTunedScore + LatexBreakSymbol + "\n"
				+ "\t\\hline" + "\n" + "\t\\end{tabular}" + "\n" + "\\end{center}" + "\n";

		return result;
	}

	private String createNistScoresTable(MTConfigFile theConfig) {
		return createScoresTable(EvaluationScoresQuadruple.createNistScores(theConfig), "Nist Scores");
	}

	private String createBleuScoresTable(MTConfigFile theConfig) {
		return createScoresTable(EvaluationScoresQuadruple.createBleuScores(theConfig), "Bleu Scores");
	}

	private void writeResultsForSystem(BufferedWriter outputWriter,MTConfigFile theConfig) throws IOException
	{
		outputWriter.write("System name: " + theConfig.getSystemName() + "\n\n");
		outputWriter.write(createNistScoresTable(theConfig) + "\n\n\n");
		outputWriter.write(createBleuScoresTable(theConfig) + "\n");
		outputWriter.write("\n");
	}
	
	private void writeLatexDocumentHeader(BufferedWriter outputWriter) throws IOException
	{
		String latexDocumentHeader = "\\documentclass[a4paper,10pt]{article}\n" + 
				 "\\begin{document}";
		outputWriter.write(latexDocumentHeader);
	}
	
	private void writeLatexDocumentFooter(BufferedWriter outputWriter) throws IOException
	{
		String latexDocumentFooter = "\\end{document}";
		outputWriter.write(latexDocumentFooter);
	}
	
	public void writeSummarizedResults() {
		try {
			BufferedWriter outputWriter = new BufferedWriter(new java.io.FileWriter(this.pipelineConfigFiles.get(0).filesConfig.getResultsSummarayFilePath()));
			writeLatexDocumentHeader(outputWriter);
			for(MTConfigFile pipelineConfig : this.pipelineConfigFiles)
			{
				writeResultsForSystem(outputWriter, pipelineConfig);
			}
			writeLatexDocumentFooter(outputWriter);
			outputWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
