package mt_pipeline.evaluation;

public class EvaluationFilePathsTriple {

	private static final String GML_SUFFIX = ".gml";
	private final String sourceFilePath;
	private final String resultFilePath;
	private final String referenceFilePath;

	private EvaluationFilePathsTriple(String sourceFilePath, String resultFilePath, String referenceFilePath) {
		this.sourceFilePath = sourceFilePath;
		this.referenceFilePath = referenceFilePath;
		this.resultFilePath = resultFilePath;
	}

	public static EvaluationFilePathsTriple createEvaluationFilePathsTriple(String sourceFilePath, String resultFilePath, String referenceFilePath) {
		return new EvaluationFilePathsTriple(sourceFilePath, resultFilePath, referenceFilePath);
	}

	public static EvaluationFilePathsTriple createEvaluationFilePathsTripleGML(String sourceFilePath, String resultFilePath, String referenceFilePath) {
		EvaluationFilePathsTriple intermediateResult = new EvaluationFilePathsTriple(sourceFilePath, resultFilePath, referenceFilePath);
		return intermediateResult.createEvaluationFilePathsTripleGML();
	}

	public EvaluationFilePathsTriple createEvaluationFilePathsTripleGML() {
		return new EvaluationFilePathsTriple(this.sourceFilePath + GML_SUFFIX, resultFilePath + GML_SUFFIX, referenceFilePath + GML_SUFFIX);
	}

	public String getSourceFilePath() {
		return this.sourceFilePath;
	}

	public String getReferenceFilePath() {
		return this.referenceFilePath;
	}

	public String getResultFilePath() {
		return this.resultFilePath;
	}

}
