package mt_pipeline.evaluation;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import util.ConfigFile;
import util.FileUtil;

public class PureMultiEvalResultsTableDevAndTest extends MultiEvalResultsTableDevAndTest {

    protected PureMultiEvalResultsTableDevAndTest(MultiEvalResultsTable multiEvalResultsTableDev,
	    MultiEvalResultsTable multiEvalResultsTableTest) {
	super(multiEvalResultsTableDev, multiEvalResultsTableTest);
    }

    public static PureMultiEvalResultsTableDevAndTest createPureMultiEvalResultsTableDevAndTest(
	    String resultFilePathDev, String resultFilePathTest,boolean useBeerMetric,boolean outputLengthStatistics) {
	MultiEvalResultsTable multiEvalResultsTableDev = MultiEvalResultsTable
		.createCompleteMultiEvalResultsTable(resultFilePathDev,useBeerMetric,outputLengthStatistics);
	MultiEvalResultsTable multiEvalResultsTableTest = MultiEvalResultsTable
		.createCompleteMultiEvalResultsTable(resultFilePathTest,useBeerMetric,outputLengthStatistics);
	return new PureMultiEvalResultsTableDevAndTest(multiEvalResultsTableDev,
		multiEvalResultsTableTest);
    }
    
    public static void createAndWriteResultsTableForConfig(String configFilePath,boolean showStandardDeviation) {

	ConfigFile theConfig;
	try {
	    theConfig = new ConfigFile(configFilePath);
	    boolean useBeerMetric = Boolean.parseBoolean(theConfig.getValueWithPresenceCheck(MultiEvalAndLRScoreTableDevAndTest.USE_BEER_METRIC_PROPERTY_STRING));
	    boolean outputLengthStatistics = Boolean.parseBoolean(theConfig.getValueWithPresenceCheck(MultiEvalAndLRScoreTableDevAndTest.OUTPUT_LENGTH_STATISTICS_PROPERTY_STRING));
	    MultiEvalResultsTableDevAndTest multiEvalResultsTableDevAndTest = PureMultiEvalResultsTableDevAndTest
		    .createPureMultiEvalResultsTableDevAndTest(getDevResultFilePath(theConfig),
			    getTestResultFilePath(theConfig),useBeerMetric,outputLengthStatistics);
	    multiEvalResultsTableDevAndTest.writeScoresToFile(getOutputResultFilePath(theConfig),showStandardDeviation);
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}

    }
    
    @Override
    protected String tableHeader() {
  	String result = "\\begin{tabular}{|l|l|l|l|l|l|l|l|}"
  		+ NL
  		+ "\\hline"
  		+ NL
  		+ "{\\multirow{3}{*}{\\minitab[c]{ System  Name }}}  &  \\multicolumn{6}{c|}{German-English}  \\\\"
  		+ NL + "\\cline{2-7}" + NL
  		+ "&   \\multicolumn{3}{c|}{DEV}  &  \\multicolumn{3}{c|}{TEST}\\\\" + NL
  		+ "\\cline{2-7}" + NL
  		+ "&    BLEU & METEOR & TER &  BLEU & METEOR & TER    \\\\  \\hline" + NL;
  	return result;
      }
    
    @Override
    public void writeScoresToFile(String outputFilePath, boolean showStandardDeviation) {
	BufferedWriter outputWriter = null;
	try {
	    outputWriter = new BufferedWriter(new FileWriter(outputFilePath));
	    outputWriter.write(tableHeader());
	    for (int i = 0; i < multiEvalResultsTableDev.numSystems(); i++) {
		outputWriter.write(this.multiEvalResultsTableDev.getSystemName(i) + " & "
			+ this.multiEvalResultsTableDev.getSystemScoresString(i,showStandardDeviation) + " & "
			+ this.multiEvalResultsTableTest.getSystemScoresString(i,showStandardDeviation) + " \\\\" + NL);
	    }
	    outputWriter.write(tableFooter());
	} catch (IOException e) {
	    e.printStackTrace();
	} finally {
	    FileUtil.closeCloseableIfNotNull(outputWriter);
	}

    }

    public static void main(String[] args) {
	/*
	 * String resultFilePathDev =
	 * "./multEvalTableCreaterTestData/allOutput-multeval-EnDe-dev.txt";
	 * String resultFilePathTest =
	 * "./multEvalTableCreaterTestData/allOutput-multeval-EnDe-test.txt";
	 * MultiEvalResultsTableDevAndTest multiEvalResultsTableDevAndTest =
	 * MultiEvalResultsTableDevAndTest
	 * .createMultiEvalResultsTableDevAndTest(resultFilePathDev,
	 * resultFilePathTest); multiEvalResultsTableDevAndTest.printScores();
	 */
	if (args.length != 1) {
	    System.out.println("Usage : createResultsTable CONFIG_FILE_PATH");
	    System.exit(1);
	}
	createAndWriteResultsTableForConfig(args[0],true);

    }

}
