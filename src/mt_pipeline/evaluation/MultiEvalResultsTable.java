package mt_pipeline.evaluation;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import junit.framework.Assert;
import mt_pipeline.evaluation.MultEvalSystemScores.SystemScoresBasic;
import mt_pipeline.evaluation.MultEvalSystemScores.SystemScoresWithBeer;

public class  MultiEvalResultsTable <T extends  MultEvalSystemScores> {
    static double SIGNIFICANCE_THRESHOLD_P_VALUE = 0.05;
    protected final NumberFormat resultNumberFormat;

    private final Map<String, Integer> systemNameToSystemIndexMap;
    private final List<T> systemScoresList;
    
    private final boolean useBeerMetric;
    private final boolean outputLengthStatistics; 
    

    private MultiEvalResultsTable(Map<String, Integer> systemNameToSystemIndexMap,
	    List<T> systemScoresList, NumberFormat resultNumberFormat,boolean useBeerMetric,
	    boolean outputLengthStatistics) {
	this.systemNameToSystemIndexMap = systemNameToSystemIndexMap;
	this.systemScoresList = systemScoresList;
	this.resultNumberFormat = resultNumberFormat;
	this.useBeerMetric = useBeerMetric;
	this.outputLengthStatistics = outputLengthStatistics;
    }

    public static MultiEvalResultsTable<SystemScoresBasic>
    createMultiEvalResultsTableBasic(Map<String, Integer> systemNameToSystemIndexMap,
	    List<SystemScoresBasic> systemScoresList, NumberFormat resultNumberFormat, boolean outputLengthStatistics){
	return new MultiEvalResultsTable<SystemScoresBasic>(systemNameToSystemIndexMap, systemScoresList, resultNumberFormat, false,outputLengthStatistics);
    }
    
    public static MultiEvalResultsTable<SystemScoresWithBeer>
    createMultiEvalResultsTableWithBeer(Map<String, Integer> systemNameToSystemIndexMap,
	    List<SystemScoresWithBeer> systemScoresList, NumberFormat resultNumberFormat,boolean outputLengthStatistics){
	return new MultiEvalResultsTable<SystemScoresWithBeer>(systemNameToSystemIndexMap, systemScoresList, resultNumberFormat, true,outputLengthStatistics);
    }
    
    
    public static MultiEvalResultsTable<SystemScoresBasic> createMultiEvalResultsTableBasic(boolean outputLengthStatistics) {
	return new MultiEvalResultsTable<SystemScoresBasic>(new HashMap<String, Integer>(),
		new ArrayList<SystemScoresBasic>(), createResultNumberFormat(2),false,outputLengthStatistics);
    }
    
    public static MultiEvalResultsTable<SystemScoresWithBeer> createMultiEvalResultsTableWithBeer(boolean outputLengthStatistics) {
	return new MultiEvalResultsTable<SystemScoresWithBeer>(new HashMap<String, Integer>(),
		new ArrayList<SystemScoresWithBeer>(), createResultNumberFormat(2),true,outputLengthStatistics);
    }

    public static MultiEvalResultsTable <?> createCompleteMultiEvalResultsTable(String resultFilePath,boolean useBeerMetric,boolean outputLengthStatistics)
    {
	if(useBeerMetric){
	    return createCompleteMultiEvalResultsTableWithBeer(resultFilePath,outputLengthStatistics);
	}
	else{
	    return createCompleteMultiEvalResultsTableBasic(resultFilePath,outputLengthStatistics);
	}
    }
    
    public static MultiEvalResultsTable<SystemScoresBasic> createCompleteMultiEvalResultsTableBasic(String resultFilePath,boolean outputLengthStatistics) {
	// Create table containing scores and standard deviations original
	// scores
	MultiEvalResultsTable<SystemScoresBasic> scoreAndStandardDeviationsTable = MultiEvalScoreAndStandardDeviationResultsTableCreater
		.createMultiEvalResultsTableScoreAndStandardDeviationBasic(resultFilePath,outputLengthStatistics);
	// Create table containing re-sampling significance test scores with
	// p-Values
	
	MultiEvalSignificanceResultsTableCreater<SystemScoresBasic> multiEvalSignificanceResultsTableCreater =
		new MultiEvalSignificanceResultsTableCreater<SystemScoresBasic>(false,outputLengthStatistics);
	
	MultiEvalResultsTable<SystemScoresBasic> scoreAndPValueTable = multiEvalSignificanceResultsTableCreater
		.createMultiEvalSignificanceResultsTable(resultFilePath);
	Assert.assertEquals(scoreAndStandardDeviationsTable.numSystems(),
		scoreAndPValueTable.numSystems());

	// Copy the p-values from the re-sampling scores table to the first
	// table
	for (int i = 0; i < scoreAndStandardDeviationsTable.numSystems(); i++) {
	    scoreAndStandardDeviationsTable.getSystemScoresForIndex(i)
		    .addPValuesFromSecondSystemScores(
			    scoreAndPValueTable.getSystemScoresForIndex(i));
	}
	// Return the original scores table which is now enriched with the
	// p-values
	return scoreAndStandardDeviationsTable;
    }

    
    public static MultiEvalResultsTable<SystemScoresWithBeer> createCompleteMultiEvalResultsTableWithBeer(String resultFilePath,boolean outputLengthStatistics) {
	// Create table containing scores and standard deviations original
	// scores
	MultiEvalResultsTable<SystemScoresWithBeer> scoreAndStandardDeviationsTable = MultiEvalScoreAndStandardDeviationResultsTableCreater
		.createMultiEvalResultsTableScoreAndStandardDeviationWithBeer(resultFilePath,outputLengthStatistics);
	// Create table containing re-sampling significance test scores with
	// p-Values
	
	MultiEvalSignificanceResultsTableCreater<SystemScoresWithBeer> multiEvalSignificanceResultsTableCreater =
		new MultiEvalSignificanceResultsTableCreater<SystemScoresWithBeer>(true,outputLengthStatistics);
	
	MultiEvalResultsTable<SystemScoresWithBeer> scoreAndPValueTable = multiEvalSignificanceResultsTableCreater
		.createMultiEvalSignificanceResultsTable(resultFilePath);
	Assert.assertEquals(scoreAndStandardDeviationsTable.numSystems(),
		scoreAndPValueTable.numSystems());

	// Copy the p-values from the re-sampling scores table to the first
	// table
	for (int i = 0; i < scoreAndStandardDeviationsTable.numSystems(); i++) {
	    scoreAndStandardDeviationsTable.getSystemScoresForIndex(i)
		    .addPValuesFromSecondSystemScores(
			    scoreAndPValueTable.getSystemScoresForIndex(i));
	}
	// Return the original scores table which is now enriched with the
	// p-values
	return scoreAndStandardDeviationsTable;
    }

    
    public static NumberFormat createResultNumberFormat(int numDecimals) {
	NumberFormat result = NumberFormat.getInstance();
	result.setRoundingMode(RoundingMode.HALF_EVEN);
	result.setMaximumFractionDigits(numDecimals);
	result.setMinimumFractionDigits(numDecimals);
	return result;
    }

    public MultEvalSystemScores getBaselineScores() {
	return this.systemScoresList.get(0);
    }

    public int numSystems() {
	return this.systemScoresList.size();
    }

    public boolean hasSystemScoresEntryForSystemName(String systemName) {
	return this.systemNameToSystemIndexMap.containsKey(systemName);
    }

    public int getSystemIndexForSystemName(String systemName) {
	if (hasSystemScoresEntryForSystemName(systemName)) {
	    return this.systemNameToSystemIndexMap.get(systemName);
	}
	return -1;
    }

    public int addSystemScoresAndReturnListIndex(T systemScores) {
	int systemIndex = numSystems();
	this.systemScoresList.add(systemScores);
	this.systemNameToSystemIndexMap.put(systemScores.getSystemName(), systemIndex);
	return systemIndex;
    }

    public T getSystemScoresForIndex(int index) {
	return this.systemScoresList.get(index);
    }

    public T getSystemScoresForSystemName(String systemName) {
	return this.systemScoresList.get(systemNameToSystemIndexMap.get(systemName));
    }

    public boolean allScoresAreSet() {
	for (MultEvalSystemScores systemScores : this.systemScoresList) {
	    if (!systemScores.allScoresAreSet()) {
		System.out.println("Failure: not all scores are set");
		return false;
	    }
	}
	System.out.println("Succes: all scores are set");
	return true;

    }
    
    
    @SuppressWarnings("unchecked")
    private T createSystemScores(String systemName){
	if(useBeerMetric){
	    return (T) SystemScoresWithBeer.createSystemScores(systemName,outputLengthStatistics);
	}
	else{
	    return (T) SystemScoresBasic.createSystemScores(systemName,outputLengthStatistics);
	}	   
    }    

    private int addSystemScoresForSystemName(String systemName) {
	Assert.assertFalse(this.hasSystemScoresEntryForSystemName(systemName));
	T systemScores = createSystemScores(systemName);
	return addSystemScoresAndReturnListIndex(systemScores);
    }

    public MultEvalSystemScores getOrAddAndGetSystemScoresForSystemName(String systemName) {
	if (this.hasSystemScoresEntryForSystemName(systemName)) {
	    return this.getSystemScoresForSystemName(systemName);
	}
	return this.getSystemScoresForIndex(addSystemScoresForSystemName(systemName));
    }

    public String getSystemName(int systemIndex) {
	return this.systemScoresList.get(systemIndex).getSystemName();
    }

    public String getSystemScoresString(int systemIndex, boolean showStandardDeviation) {
	return this.getSystemScoresForIndex(systemIndex).getScoresString(
		SIGNIFICANCE_THRESHOLD_P_VALUE, this.resultNumberFormat, getBaselineScores(),
		showStandardDeviation);
    }
    
    public String getSystemScoresStringIncludingLength(int systemIndex, boolean showStandardDeviation) {
  	return this.getSystemScoresForIndex(systemIndex).getScoresStringLengthOnly(
  		SIGNIFICANCE_THRESHOLD_P_VALUE, this.resultNumberFormat, getBaselineScores(),
  		showStandardDeviation);
      }
    
    public String getSystemScoresStringLengthOnly(int systemIndex, boolean showStandardDeviation) {
  	return this.getSystemScoresForIndex(systemIndex).getScoresStringLengthOnly(
  		SIGNIFICANCE_THRESHOLD_P_VALUE, this.resultNumberFormat, getBaselineScores(),
  		showStandardDeviation);
      }

    public void printScores(boolean showStandardDeviation) {
	System.out.println("\n\n\nScores Table:");
	for (int i = 0; i < numSystems(); i++) {
	    System.out.println(this.getSystemName(i) + " & "
		    + this.getSystemScoresString(i, showStandardDeviation));
	}
    }       

    public static void main(String[] args) {
	String path = "./multEvalTableCreaterTestData/allOutput-multeval-DeEn-Test.txt";
	MultiEvalResultsTable<SystemScoresBasic> multiEvalResultsTable = MultiEvalResultsTable
		.createCompleteMultiEvalResultsTableBasic(path,false);
	multiEvalResultsTable.printScores(true);
    }

}
