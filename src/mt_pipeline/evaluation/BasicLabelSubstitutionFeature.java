package mt_pipeline.evaluation;

import grammarExtraction.chiangGrammarExtraction.ChiangRuleCreater;

public abstract class BasicLabelSubstitutionFeature {
    private static final String LABEL_SUBSTITUTION_FEATURE_MARKER = "_substitutes_";

    private final String substitutionLabel;
    private final String substitutionToLabel;

    public BasicLabelSubstitutionFeature(String substitutionLabel, String substitutionToLabel) {
	this.substitutionLabel = substitutionLabel;
	this.substitutionToLabel = substitutionToLabel;
    }

    public static String getLabelFromLabelPart(String labelPart) {
	return labelPart.substring(labelPart.indexOf("[") + 1, labelPart.indexOf("]"));
    }

    public static double getWeightFromSecondLabelPart(String secondlabelPart) {
	String[] parts = secondlabelPart.split(" ");
	return Double.parseDouble(parts[1]);
    }

    public static double getFeatureValueFromSecondLabelPart(String secondlabelPart) {
	String[] parts = secondlabelPart.split("=");
	return Double.parseDouble(parts[1]);
    }

    public static boolean consitutesLabelSubstitutionFeatureString(String string) {
	return string.contains(LABEL_SUBSTITUTION_FEATURE_MARKER);
    }

    public String getSubstitutionLabel() {
	return substitutionLabel;
    }

    public String getSubstitutionToLabel() {
	return substitutionToLabel;
    }

    public boolean isGlueRuleSubstitutionFeature() {
	return this.substitutionToLabel.equals(ChiangRuleCreater.ChiangLabel);
    }

    public boolean isMatchFeature() {
	return this.substitutionLabel.equals(this.substitutionToLabel);
    }

    public boolean isNoMatchFeature() {
	return !isMatchFeature();
    }

    public static class BasicLabelSubstitutionFeatureWithWeight extends
	    BasicLabelSubstitutionFeature {
	private final double featureWeight;

	public BasicLabelSubstitutionFeatureWithWeight(String substitutionLabel,
		String substitutionToLabel, double featureWeight) {
	    super(substitutionLabel, substitutionToLabel);
	    this.featureWeight = featureWeight;
	}

	public static BasicLabelSubstitutionFeatureWithWeight createBasicLabelSubstitutionFeatureWithWeight(
		String labelSubstitutionFeatureString) {
	    String[] firstSplitParts = labelSubstitutionFeatureString
		    .split(LABEL_SUBSTITUTION_FEATURE_MARKER);
	    String firstLabel = getLabelFromLabelPart(firstSplitParts[0]);
	    String secondLabel = getLabelFromLabelPart(firstSplitParts[1]);
	    double weight = getWeightFromSecondLabelPart(firstSplitParts[1]);
	    return new BasicLabelSubstitutionFeatureWithWeight(firstLabel, secondLabel, weight);
	}

	public double getFeatureWeight() {
	    return featureWeight;
	}

    }

    public static class BasicLabelSubstitutionFeatureWithValue extends
	    BasicLabelSubstitutionFeature {
	private final double featureValue;

	public BasicLabelSubstitutionFeatureWithValue(String substitutionLabel,
		String substitutionToLabel, double featureValue) {
	    super(substitutionLabel, substitutionToLabel);
	    this.featureValue = featureValue;
	}

	public static BasicLabelSubstitutionFeatureWithValue createBasicLabelSubstitutionFeatureWithWeight(
		String labelSubstitutionFeatureString) {
	    String[] firstSplitParts = labelSubstitutionFeatureString
		    .split(LABEL_SUBSTITUTION_FEATURE_MARKER);
	    String firstLabel = getLabelFromLabelPart(firstSplitParts[0]);
	    String secondLabel = getLabelFromLabelPart(firstSplitParts[1]);
	    double featureValue = getFeatureValueFromSecondLabelPart(firstSplitParts[1]);
	    return new BasicLabelSubstitutionFeatureWithValue(firstLabel, secondLabel, featureValue);

	}

	public double getFeatureValue() {
	    return featureValue;
	}

    }

}
