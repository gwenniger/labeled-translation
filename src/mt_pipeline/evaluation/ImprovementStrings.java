package mt_pipeline.evaluation;

public class ImprovementStrings {
    public static final String NINETY_FIVE_PERCENT_CONFIDENCE_IMPROVEMENT_STRING = "\\SI";
    public static final String NINETY_NINE_PERCENT_CONFIDENCE_IMPROVEMENT_STRING = "\\SISI";
    public static final String NINETY_FIVE_PERCENT_CONFIDENCE_WORSENING_STRING = "\\SW";
    public static final String NINETY_NINE_PERCENT_CONFIDENCE_WORSENING_STRING = "\\SWSW";

}
