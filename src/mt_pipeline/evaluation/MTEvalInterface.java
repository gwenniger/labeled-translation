package mt_pipeline.evaluation;

import util.LinuxInteractor;

public class MTEvalInterface {

	private static final String MT_EVAL_PROGRAM_NAME = "mteval-v11b.perl";

	private final EvaluationFilePathsTriple evaluationFilePathsTriple;
	private final String mtEvalDir;
	private final String outputScoreFilePath;

	private MTEvalInterface(EvaluationFilePathsTriple evaluationFilePathsTriple, String mtEvalDir, String outputScoreFilePath) {
		this.evaluationFilePathsTriple = evaluationFilePathsTriple;
		this.mtEvalDir = mtEvalDir;
		this.outputScoreFilePath = outputScoreFilePath;
	}

	public static MTEvalInterface createMtEvalInterfaceForGMLFiles(String sourceFilePathGML, String resultFilePathGML, String referenceFilePathGML,
			String mtEvalDir, String outputScoreFilePath) {
		return new MTEvalInterface(EvaluationFilePathsTriple.createEvaluationFilePathsTriple(sourceFilePathGML, resultFilePathGML, referenceFilePathGML),
				mtEvalDir, outputScoreFilePath);
	}

	public static EvaluationFilePathsTriple createMtEvalInterfaceForPlainFilesAndGetEvaluationFilePathsTripleGML(String sourceFilePath, String resultFilePath,
			String referenceFilePath) {
		EvaluationFilePathsTriple evaluationFilePathsTriple = EvaluationFilePathsTriple.createEvaluationFilePathsTriple(sourceFilePath, resultFilePath,
				referenceFilePath);
		EvaluationFilePathsTriple evaluationFilePathsTripleGML = evaluationFilePathsTriple.createEvaluationFilePathsTripleGML();
		GmlWrapper.createGMLFilesForPlainFiles(evaluationFilePathsTriple, evaluationFilePathsTripleGML);
		return evaluationFilePathsTripleGML;
	}

	public static MTEvalInterface createMtEvalInterfaceForPlainFiles(String sourceFilePath, String resultFilePath, String referenceFilePath, String mtEvalDir,
			String outputScoreFilePath) {
		EvaluationFilePathsTriple evaluationFilePathsTripleGML = createMtEvalInterfaceForPlainFilesAndGetEvaluationFilePathsTripleGML(sourceFilePath,
				resultFilePath, referenceFilePath);
		return new MTEvalInterface(evaluationFilePathsTripleGML, mtEvalDir, outputScoreFilePath);
	}

	public void computeBleuAndNist() {
		String evaluationCommand = mtEvalDir + MT_EVAL_PROGRAM_NAME + " -r " + evaluationFilePathsTriple.getReferenceFilePath() + " -t "
				+ evaluationFilePathsTriple.getResultFilePath() + " -s " + evaluationFilePathsTriple.getSourceFilePath() + " -c >" + outputScoreFilePath;
		LinuxInteractor.executeCommand(evaluationCommand, true);
	}

}
