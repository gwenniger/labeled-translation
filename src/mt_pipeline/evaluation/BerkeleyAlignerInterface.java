package mt_pipeline.evaluation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mt_pipeline.preAndPostProcessing.SubFileCreater;
import util.ConfigFile;
import util.FileStatistics;
import util.FileUtil;
import util.LinuxInteractor;
import datapreparation.LanguageAbbreviationPair;

public class BerkeleyAlignerInterface {

    private static final String NL = "\n";

    private static final String TEMP_TEST_SENTENCES_DIR = "test-temp-dir";
    private static final String BERKELEY_ALIGNER_CONFIG_FILE_NAME = "BerkeleyAlignerTestConfigig.txt";
    private static final String SOURCE_ABBREVIATION_PROPERTY = "sourceAbbreviation";
    private static final String TARGET_ABBREVIATION_PROPERTY = "targetAbbreviation";
    private static final String OUTPUT_DIR_NAME = "alignerOutput";
    private static final String ALIGNMENT_SUFFIX = ".alignment";
    private static final String RUN_SUFFIX = ".run";
    private static final String ALIGNMENT_RESULT_FILEN_NAME = "training.align";
    private static final String TUNED_OUTPUT_RUN_FILE_PREFIX = "output.1best.TUNED.run";

    private final String workDirPath;
    private final String berkeleyAlignerRootDirPath;
    private final String modelFolderPath;
    private final List<String> sourceFilePaths;
    private final List<String> targetFilePaths;
    private final LanguageAbbreviationPair languageAbbreviationPair;

    private BerkeleyAlignerInterface(String workDirPath, String berkeleyAlignerRootDirPath,
	    String modelFolderPath, List<String> sourceFilePaths, List<String> targetFilePaths,
	    LanguageAbbreviationPair languageAbbreviationPair) {
	this.workDirPath = workDirPath;
	this.berkeleyAlignerRootDirPath = berkeleyAlignerRootDirPath;
	this.modelFolderPath = modelFolderPath;
	this.sourceFilePaths = sourceFilePaths;
	this.targetFilePaths = targetFilePaths;
	this.languageAbbreviationPair = languageAbbreviationPair;
    }

    public static BerkeleyAlignerInterface createBerkeleyAlignerInterface(String configFilePath) {
	BerleleyAlignerInterfaceConfig theConfig;
	try {
	    theConfig = new BerleleyAlignerInterfaceConfig(configFilePath);
	    LanguageAbbreviationPair languageAbbreviationPair = LanguageAbbreviationPair
		    .createLanguageAbbreviationPair(theConfig.getSourceAbbreviation(),
			    theConfig.getTargetAbbreviation());
	    theConfig.showTargetFilePaths();
	    return new BerkeleyAlignerInterface(theConfig.getWorkDir(),
		    theConfig.getBerkeleyAlignerRootDir(), theConfig.getModelFolderPath(),
		    theConfig.getSourceFilePaths(), theConfig.getTargetFilePaths(),
		    languageAbbreviationPair);
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private void prepareForRunningAligner() {
	createTempTestDir();
	createBerkeleyAlignerConfigCreater().writeConfig();
    }

    private String berkeleyAlignerCommand() {
	return berkeleyAlignerRootDirPath + "./align" + " " + configFilePath();
    }

    private void alignTestData() {
	FileUtil.removeDirectory(new File(outputDirPath()));
	prepareForRunningAligner();
	LinuxInteractor.executeExtendedCommandDisplayOutput(berkeleyAlignerCommand());
	writeDeConcatenatedAlignmentOutputFiles();
    }

    private BerkeleyAlignerConfigCreater createBerkeleyAlignerConfigCreater() {
	BufferedWriter outputWriter;
	try {
	    outputWriter = new BufferedWriter(new FileWriter(configFilePath()));
	    return new BerkeleyAlignerConfigCreater(modelFolderPath, outputWriter,
		    tempTestDirPath(), languageAbbreviationPair, outputDirPath());
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}

    }

    private String configFilePath() {
	return workDirPath + "/" + BERKELEY_ALIGNER_CONFIG_FILE_NAME;
    }

    private String tempTestDirPathWithoutLastSlash() {
	return workDirPath + "/" + TEMP_TEST_SENTENCES_DIR;
    }

    private String tempTestDirPath() {
	return tempTestDirPathWithoutLastSlash() + "/";
    }

    private String outputDirPath() {
	return workDirPath + "/" + OUTPUT_DIR_NAME + "/";
    }

    private String alignmentResultFilePath() {
	return outputDirPath() + ALIGNMENT_RESULT_FILEN_NAME;
    }

    private String tempTestSourcePath() {
	return tempTestDirPath() + "test." + languageAbbreviationPair.getSourceAbbreviation();
    }

    private String tempTestTargetPath() {
	return tempTestDirPath() + "test." + languageAbbreviationPair.getTargetAbbreviation();
    }

    private static void createFileConcatenated(List<String> filesToConcatenate,
	    String outputFilePath) {
	FileUtil.copyFileNative(filesToConcatenate.get(0), outputFilePath, true);
	for (int i = 1; i < filesToConcatenate.size(); i++) {
	    FileUtil.appendFile(outputFilePath, filesToConcatenate.get(i));
	}

    }

    private void createTestSourceFileConcatenated() {
	createFileConcatenated(sourceFilePaths, tempTestSourcePath());
    }

    private void createTestTargetFileConcatenated() {
	createFileConcatenated(targetFilePaths, tempTestTargetPath());
    }

    private String getAlignmentOutputFilePathForTargetFilePath(String targetFilePath) {
	if (targetFilePath.contains(RUN_SUFFIX)) {
	    int runSuffixBeginIndex = targetFilePath.lastIndexOf(RUN_SUFFIX);
	    String runSuffix = targetFilePath.substring(runSuffixBeginIndex);
	    String result = targetFilePath.substring(0, runSuffixBeginIndex);
	    result += ALIGNMENT_SUFFIX;
	    result += runSuffix;
	    return result;
	} else if (targetFilePath.endsWith("." + languageAbbreviationPair.getSourceAbbreviation())
		|| targetFilePath.endsWith("." + languageAbbreviationPair.getTargetAbbreviation())) {
	    int languageSuffixIndex = targetFilePath.lastIndexOf(".");
	    return targetFilePath.substring(0, languageSuffixIndex) + ALIGNMENT_SUFFIX;
	}
	return targetFilePath + ALIGNMENT_SUFFIX;

    }

    private void writeDeConcatenatedAlignmentOutputFiles() {
	try {
	    int startLine = 0;
	    for (String targetFilePath : targetFilePaths) {
		int numLinesToCopy;

		numLinesToCopy = FileStatistics.countLines(targetFilePath);

		SubFileCreater.createSubFile(alignmentResultFilePath(),
			getAlignmentOutputFilePathForTargetFilePath(targetFilePath),
			numLinesToCopy, startLine);
		startLine += numLinesToCopy;
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private void createTempTestDir() {
	FileUtil.createFolderIfNotExisting(workDirPath);
	FileUtil.createFolderIfNotExisting(tempTestDirPath());
	System.out.println("Copy files...");
	createTestSourceFileConcatenated();
	createTestTargetFileConcatenated();
    }

    private static class BerkeleyAlignerConfigCreater {
	private final String LOAD_PARAMS_DIR_PAR = "loadParamsDir";
	private final String FOREIGN_SUFFIX_PAR = "foreignSuffix";
	private final String ENGLISH_SUFFIX_PAR = "englishSuffix";

	private final BufferedWriter outputWriter;
	private final String modelFolderPath;
	private final String testSentencesFolderPath;
	private final LanguageAbbreviationPair languageAbbreviationPair;
	private final String outputDirPath;

	private BerkeleyAlignerConfigCreater(String modelFolderPath, BufferedWriter outputWriter,
		String testSentencesFolderPath, LanguageAbbreviationPair languageAbbreviationPair,
		String outputDirPath) {
	    this.modelFolderPath = modelFolderPath;
	    this.outputWriter = outputWriter;
	    this.testSentencesFolderPath = testSentencesFolderPath;
	    this.languageAbbreviationPair = languageAbbreviationPair;
	    this.outputDirPath = outputDirPath;
	}

	public void writeConfig() {
	    try {
		outputWriter.write(loadParamsDirSpecification() + NL);
		outputWriter.write(trainConfig() + NL);
		outputWriter.write(executionConfig() + NL);
		outputWriter.write(languageDataConfig() + NL);
		outputWriter.write(evaluationConfig() + NL);
	    } catch (IOException e) {
		e.printStackTrace();
		throw new RuntimeException(e);
	    } finally {
		FileUtil.closeCloseableIfNotNull(outputWriter);
	    }
	}

	private String loadParamsDirSpecification() {
	    return LOAD_PARAMS_DIR_PAR + " " + modelFolderPath;
	}

	private String trainConfig() {
	    String result = "##########################################" + NL
		    + "# Training: Defines the training regimen #" + NL
		    + "##########################################" + NL
		    + "forwardModels   MODEL1 HMM" + NL + "reverseModels   MODEL1 HMM" + NL
		    + "mode    JOINT JOINT" + NL + "iters   0 0";
	    return result;
	}

	private String getExecutionDir() {
	    return "execDir" + "  " + outputDirPath;
	}

	private String executionConfig() {
	    String result = "###############################################" + NL
		    + "# Execution: Controls output and program flow #" + NL
		    + "###############################################" + NL + NL +

		    getExecutionDir() + NL + "create" + NL + "saveParams      true" + NL
		    + "numThreads      2" + NL + "msPerLine       100000000" + NL + "alignTraining";
	    return result;
	}

	private String foreignSuffixConfig() {
	    return FOREIGN_SUFFIX_PAR + "   " + languageAbbreviationPair.getSourceAbbreviation();
	}

	private String englishSuffixConfig() {
	    return ENGLISH_SUFFIX_PAR + "   " + languageAbbreviationPair.getTargetAbbreviation();
	}

	private String languageDataConfig() {
	    String result = "";
	    result += "#################"
		    + NL
		    + "# Language/Data #"
		    + NL
		    + "#################"
		    + NL
		    + foreignSuffixConfig()
		    + NL
		    + englishSuffixConfig()
		    + NL
		    + "lowercase"
		    + NL
		    +

		    "# Choose the training sources, which can either be directories or files that list files/directories"
		    + NL
		    + "# Note that training on the test set does not peek at the correct answers (no cheating)"
		    + NL + "trainSources  " + testSentencesFolderPath + NL + "sentences       MAX"
		    + NL +

		    "# The test sources must have hand alignments for all sentence pairs" + NL
		    + "testSources" + NL + "maxTestSentences        MAX" + NL
		    + "offsetTestSentences     0";
	    return result;
	}

	private String evaluationConfig() {

	    String result = "##############" + NL + "# Evaluation #" + NL + "##############" + NL
		    + "competitiveThresholding";
	    return result;

	}
    }

    private static class BerleleyAlignerInterfaceConfig extends ConfigFile {
	private static String BERKELEY_ALIGNER_ROOT_DIR_PATH_PROPERTY = "berkeleyAlignerRootDirPath";
	private static String MODEL_FOLDER_PATH_PROPERTY = "modelFolderPath";
	private static String SOURCE_FILE_PATHS_PROPERTY = "sourceFilePaths";
	private static String SOURCE_FILE_PATH_PROPERTY = "sourceFilePath";
	private static String TARGET_FILE_PATHS_PROPERTY = "targetFilePaths";
	private static final String WORK_DIR_PROPERTY = "berkeleyAlignerInterfaceWorkDir";
	private static final String RESULT_TYPE_PROPERTY = "resultType";
	private static final String DEV_STRING = "dev";
	private static final String TEST_STRING = "test";
	

	private static enum RESULT_TYPE {
	    DEV, TEST,BOTH
	};

	private static String ALIGN_TUNED_RUN_OUTPUT_FILES_FROM_FOLDER_PROPERTY = "alignTunedRunOutputFilesFromFolder";
	private static String REF_TARGET_FILE_PATH_PROPERTY = "refTargetFilePath";
	private static String TUNED_OUTPUT_FILES_FOLDER = "tunedOutputFilesFolder";

	public BerleleyAlignerInterfaceConfig(String inputFile) throws FileNotFoundException {
	    super(inputFile);
	}

	private String getWorkDir() {
	    return getValueWithPresenceCheck(WORK_DIR_PROPERTY);
	}

	private RESULT_TYPE getResultType() {
	    String resultTypeString = getValueWithPresenceCheck(RESULT_TYPE_PROPERTY);
	    if (resultTypeString.equalsIgnoreCase(DEV_STRING)) {
		return RESULT_TYPE.DEV;
	    } else if (resultTypeString.equalsIgnoreCase(TEST_STRING)) {
		return RESULT_TYPE.TEST;
	    }
	    else if (resultTypeString.equalsIgnoreCase(TEST_STRING)) {
		return RESULT_TYPE.TEST;
	    }
	    else {
		throw new RuntimeException("Property " + RESULT_TYPE_PROPERTY
			+ " has invalid value. Value ust be \"dev\" or \"test\"");
	    }
	}

	private boolean processDevResultFiles() {
	    return getResultType().equals(RESULT_TYPE.DEV);
	}

	private boolean processTestResultFiles() {
	    return getResultType().equals(RESULT_TYPE.TEST);
	}

	private String getBerkeleyAlignerRootDir() {
	    return getValueWithPresenceCheck(BERKELEY_ALIGNER_ROOT_DIR_PATH_PROPERTY);
	}

	private String getModelFolderPath() {
	    return getValueWithPresenceCheck(MODEL_FOLDER_PATH_PROPERTY);
	}

	private boolean alignTunedRunOutputFilesFromFolder() {
	    return getBooleanWithPresenceCheck(ALIGN_TUNED_RUN_OUTPUT_FILES_FROM_FOLDER_PROPERTY);
	}

	private List<String> construcSourceFilePathsFromTargetFilePaths() {
	    List<String> result = new ArrayList<String>();
	    for (int i = 0; i < getTargetFilePaths().size(); i++) {
		result.add(getSourceFilePath());
	    }
	    return result;
	}

	private List<String> getSourceFilePathsDirectly() {
	    return getMultiValueParmeterAsListWithPresenceCheck(SOURCE_FILE_PATHS_PROPERTY);
	}

	@Override
	public String getSourceFilePath() {
	    return getValueWithPresenceCheck(SOURCE_FILE_PATH_PROPERTY);
	}

	private List<String> getSourceFilePaths() {
	    if (alignTunedRunOutputFilesFromFolder()) {
		return construcSourceFilePathsFromTargetFilePaths();
	    } else {
		return getSourceFilePathsDirectly();
	    }
	}

	private List<String> getTargetFilePathsDirectly() {

	    return getMultiValueParmeterAsListWithPresenceCheck(TARGET_FILE_PATHS_PROPERTY);
	}

	private String getTunedOutputFilesFolderPath() {
	    return getValueWithPresenceCheck(TUNED_OUTPUT_FILES_FOLDER);
	}

	private String getRefTargetFilePath() {
	    return getValueWithPresenceCheck(REF_TARGET_FILE_PATH_PROPERTY);
	}

	private List<String> getTargetFilePaths() {

	    if (alignTunedRunOutputFilesFromFolder()) {
		List<String> result = new ArrayList<String>();
		result.addAll(getAllTunedRunOutputFilePaths(getTunedOutputFilesFolderPath()));
		result.add(getRefTargetFilePath());
		return result;
	    } else {
		return getTargetFilePathsDirectly();
	    }
	}

	private void showTargetFilePaths() {
	    System.out.println("<target file paths>");
	    for (String targetFilePath : getTargetFilePaths()) {
		System.out.println("target file path: " + targetFilePath);
	    }
	    System.out.println("</target file paths>");
	}

	private String getSourceAbbreviation() {
	    return getValueWithPresenceCheck(SOURCE_ABBREVIATION_PROPERTY);
	}

	private String getTargetAbbreviation() {
	    return getValueWithPresenceCheck(TARGET_ABBREVIATION_PROPERTY);
	}

	private boolean isTunedOutputFile(File file) {
	    return file.getName().contains(TUNED_OUTPUT_RUN_FILE_PREFIX);
	}

	private boolean parentFolderIsOfRightResultType(File file) {
	    if (processDevResultFiles()) {
		return file.getParentFile().getName().equals(DEV_STRING);

	    } else if (processTestResultFiles()) {
		return file.getParentFile().getName().equals(TEST_STRING);
	    }
	    return false;
	}

	private List<String> getAllTunedRunOutputFilePaths(String resultsFolderPath) {
	    List<String> result = new ArrayList<String>();
	    for (File file : FileUtil.getAllFilesInFolder(new File(resultsFolderPath), true)) {

		if (parentFolderIsOfRightResultType(file)) {
		    if (isTunedOutputFile(file)) {
			result.add(file.getAbsolutePath());
		    }
		}
	    }
	    return result;
	}
    }

    public static void main(String[] args) {
	if (args.length != 1) {
	    System.out.println("Usage: BerkeleyAlignerInterface ConfigFile");
	    System.exit(1);
	}

	BerkeleyAlignerInterface berkeleyAlignerInterface = BerkeleyAlignerInterface
		.createBerkeleyAlignerInterface(args[0]);
	berkeleyAlignerInterface.alignTestData();
    }
}