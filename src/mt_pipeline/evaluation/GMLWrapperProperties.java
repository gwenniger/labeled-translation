package mt_pipeline.evaluation;

/**
 * A class that stores the properties used when generating the GML wrap
 * 
 * @author gemaille
 * 
 */
public class GMLWrapperProperties {

	protected final String setid, sourceLanguage, targetLanguage, docid, typeID;

	protected GMLWrapperProperties(String setid, String sourceLanguage, String targetLanguage, String docid, String typeID) {
		this.setid = setid;
		this.sourceLanguage = sourceLanguage;
		this.targetLanguage = targetLanguage;
		this.docid = docid;
		this.typeID = typeID; // src,ref,tst
	}

}
