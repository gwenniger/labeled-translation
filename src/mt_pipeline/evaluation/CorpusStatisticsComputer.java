package mt_pipeline.evaluation;

import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;
import util.LatexStyle;

public abstract class CorpusStatisticsComputer implements CorpusStatisticsComputerCreater {
    private static String NL = "\n";
    public static String CONFIG_SEPARATOR = ",";
    protected final SentenceLengthStatisticsComputer sourceSentencesStatisticsComputer;

    protected CorpusStatisticsComputer(
	    SentenceLengthStatisticsComputer sourceSentencesStatisticsComputer) {
	this.sourceSentencesStatisticsComputer = sourceSentencesStatisticsComputer;
    }

    protected abstract String getStatisticsString(String configFileLine);

    public abstract String getStatisticsTableHeaderString();

    private List<String> getConfigFileLineParts(String configFileLine) {
	System.out.println("configFileLine:" + configFileLine);
	List<String> result = Arrays.asList(configFileLine.split(CONFIG_SEPARATOR));
	return result;
    }

    protected String getDataDescriptionString(String configFileLine) {
	return getConfigFileLineParts(configFileLine).get(0);
    }

    protected String getSourceFilePath(String configFileLine) {
	return getConfigFileLineParts(configFileLine).get(1);
    }

    protected String getTargetFilePath(String configFileLine) {
	List<String> parts = getConfigFileLineParts(configFileLine);
	Assert.assertEquals(3, parts.size());
	return parts.get(2);
    }

    protected String resultsTableLineDataDescriptionString(String configFileLine) {
	String dataDescriptionString = getDataDescriptionString(configFileLine);
	String result = dataDescriptionString + LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR;
	return result;
    }

    protected static String getOneSideHeaderPart(String side) {
	String result = "Num " + side + " words" + LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR;
	result += "Mean/Std " + side + " sentence length";
	return result;
    }

    protected String getStatisticsTableHeaderStringFirstPart(int noColumns) {
	String result = LatexStyle.beginTabularCommandWithLColumnSpecification(noColumns) + NL;
	result += LatexStyle.HLINE + NL;
	result += "DataDescriptor" + LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR;
	result += "NumSentences" + LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR
		+ getOneSideHeaderPart("source");
	return result;
    }

    public static class SingleFileCorpusStatisticsComputer extends CorpusStatisticsComputer {

	protected SingleFileCorpusStatisticsComputer(
		SentenceLengthStatisticsComputer sourceSentencesStatisticsComputer) {
	    super(sourceSentencesStatisticsComputer);
	}

	public static SingleFileCorpusStatisticsComputer createSingleFileCorpusStatisticsComputer(
		String sourceFilePath) {
	    SentenceLengthStatisticsComputer sourceSentencesStatisticsComputer = SentenceLengthStatisticsComputer
		    .createSentenceLengthStatisticsComputer(sourceFilePath);
	    return new SingleFileCorpusStatisticsComputer(sourceSentencesStatisticsComputer);
	}

	public static CorpusStatisticsComputerCreater createSingleFileCorpusStatisticsComputerCreater() {
	    return new SingleFileCorpusStatisticsComputer(null);
	}

	public String getStatisticsString(String configFileLine) {

	    String result = resultsTableLineDataDescriptionString(configFileLine);
	    result += sourceSentencesStatisticsComputer.getNumSentences()
		    + LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR;
	    result += sourceSentencesStatisticsComputer.getLengthStatisticsString();
	    return result;
	}

	@Override
	public CorpusStatisticsComputer createCorpusStatisticsComputer(String configLine) {
	    return createSingleFileCorpusStatisticsComputer(getSourceFilePath(configLine));
	}

	@Override
	public String getStatisticsTableHeaderString() {
	    String result = getStatisticsTableHeaderStringFirstPart(4)
		    + LatexStyle.LATEX_END_OF_LINE_STRING;
	    return result;
	}
    }

    public static class PairedFilesCorpusStatisticsComputer extends CorpusStatisticsComputer {
	protected final SentenceLengthStatisticsComputer targetSentencesStatisticsComputer;

	protected PairedFilesCorpusStatisticsComputer(
		SentenceLengthStatisticsComputer sourceSentencesStatisticsComputer,
		SentenceLengthStatisticsComputer targetSentencesStatisticsComputer) {
	    super(sourceSentencesStatisticsComputer);
	    this.targetSentencesStatisticsComputer = targetSentencesStatisticsComputer;
	}

	public static PairedFilesCorpusStatisticsComputer createPairedFilesCorpusStatisticsComputer(
		String sourceFilePath, String targetFilePath) {
	    SentenceLengthStatisticsComputer sourceSentencesStatisticsComputer = SentenceLengthStatisticsComputer
		    .createSentenceLengthStatisticsComputer(sourceFilePath);
	    SentenceLengthStatisticsComputer targetSentencesStatisticsComputer = SentenceLengthStatisticsComputer
		    .createSentenceLengthStatisticsComputer(targetFilePath);
	    return new PairedFilesCorpusStatisticsComputer(sourceSentencesStatisticsComputer,
		    targetSentencesStatisticsComputer);
	}

	public static CorpusStatisticsComputerCreater createPairedFilesCorpusStatisticsComputerCreater() {
	    return new PairedFilesCorpusStatisticsComputer(null, null);
	}

	@Override
	public CorpusStatisticsComputer createCorpusStatisticsComputer(String configLine) {
	    return createPairedFilesCorpusStatisticsComputer(getSourceFilePath(configLine),
		    getTargetFilePath(configLine));
	}

	@Override
	public String getStatisticsString(String configFileLine) {
	    String result = resultsTableLineDataDescriptionString(configFileLine);
	    result += sourceSentencesStatisticsComputer.getNumSentences()
		    + LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR;
	    result += sourceSentencesStatisticsComputer.getLengthStatisticsString()
		    + LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR;
	    result += targetSentencesStatisticsComputer.getLengthStatisticsString();
	    return result;
	}

	@Override
	public String getStatisticsTableHeaderString() {
	    String result = getStatisticsTableHeaderStringFirstPart(6)
		    + LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR + getOneSideHeaderPart("target")
		    + LatexStyle.LATEX_END_OF_LINE_STRING;
	    return result;
	}

    }

}
