package mt_pipeline.evaluation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mt_pipeline.evaluation.EvaluationFilePathsTriple;
import mt_pipeline.preAndPostProcessing.FileConverter;

/**
 * This class is used to wrap translation files with the GML XM-like tag format.
 * This is nescessary in order to use the NIST Bleu score evaluation program.
 * 
 * @author gemaille
 * 
 */
public class GmlWrapper {

    protected final List<SystemInput> systemInputs;
    protected final BufferedWriter outputWriter;

    private static final String SOURCE_LANGUAGE = "source-language";
    private static final String TARGET_LANGUAGE = "target-language";
    private static final String DEFAULT_SET_AND_DOC_ID = "doc_id_default";
    public static final String TEST_TYPE = "tst";
    public static final String REFERENCE_TYPE = "ref";
    public static final String SOURCE_TYPE = "src";

    private final GMLWrapperProperties gmlWrapperProperties;

    private GmlWrapper(List<SystemInput> systemInputs, BufferedWriter outputWriter,
	    GMLWrapperProperties gmlWrapperProperties) {
	this.systemInputs = systemInputs;
	this.outputWriter = outputWriter;
	this.gmlWrapperProperties = gmlWrapperProperties;
    }

    public static GmlWrapper createGMLWrapper(String inputFileName, String outputFileName,
	    String setid, String sourceLanguage, String targetLanguage, String docid, String sysID)
	    throws IOException {
	GMLWrapperProperties gmlWrapperProperties = new GMLWrapperProperties(setid, sourceLanguage,
		targetLanguage, docid, sysID);

	BufferedReader inputReader = FileConverter.createInputReader(inputFileName);
	SystemInput systemInput = new SystemInput(inputReader, docid, sysID);
	return new GmlWrapper(Collections.singletonList(systemInput),
		FileConverter.createOutputWriter(outputFileName), gmlWrapperProperties);
    }

    private static List<SystemInput> createRefSystemInputsList(List<String> refInputFileNames,
	    String docID) throws IOException {
	List<SystemInput> result = new ArrayList<SystemInput>();
	for (int i = 0; i < refInputFileNames.size(); i++) {
	    BufferedReader inputReader = FileConverter.createInputReader(refInputFileNames.get(i));
	    String sysID = "ref" + (i + 1);
	    SystemInput systemInput = new SystemInput(inputReader, docID, sysID);
	    result.add(systemInput);
	}
	return result;

    }

    public static GmlWrapper createRefGMLWrapper(List<String> refInputFileNames,
	    String outputFileName, String setid, String sourceLanguage, String targetLanguage,
	    String docid) throws IOException {
	GMLWrapperProperties gmlWrapperProperties = new GMLWrapperProperties(setid, sourceLanguage,
		targetLanguage, docid,"ref");

	List<SystemInput> systemInputs = createRefSystemInputsList(refInputFileNames, docid);
	return new GmlWrapper(systemInputs, FileConverter.createOutputWriter(outputFileName),
		gmlWrapperProperties);
    }

    protected void performFileOperation() throws IOException {
	createGMLWrappedFile();
    }

    private static String segWrappedLine(String line) {
	return "<seg>" + line + "</seg>";
    }

    private String getSet() {
	return gmlWrapperProperties.typeID + "set";
    }

    private String getGMLPreamble() {
	String result = "";
	result = "<" + getSet() + " setid=\"" + gmlWrapperProperties.setid + "\" srclang=\""
		+ gmlWrapperProperties.sourceLanguage + "\" trglang=\""
		+ gmlWrapperProperties.targetLanguage + "\">\n";
	return result;
    }

    private String getGMLPostamble() {
	String result = "";
	result += "</" + getSet() + ">\n";
	return result;
    }

    private void createGMLWrappedFile() throws IOException {
	outputWriter.write(getGMLPreamble());

	for (SystemInput systemInput : this.systemInputs) {
	    outputWriter.write(systemInput.getDocPreamble());
	    String line;
	    while ((line = systemInput.getInputReader().readLine()) != null) {
		// System.out.println("createGMLWrappedFile() - line:" + line);
		outputWriter.write(segWrappedLine(line) + "\n");
	    }
	    outputWriter.write(systemInput.getDocPostamble());
	}
	outputWriter.write(getGMLPostamble());
	outputWriter.close();
	System.out.println("Completed gml wrapping...");

    }

    public static void createGMLWrappedFile(String inputFileName, String outputFileName,
	    String sourceLanguage, String targetLanguage, String setAndDocID, String typeID) {
	try {
	    GmlWrapper gmlWrapper = GmlWrapper.createGMLWrapper(inputFileName, outputFileName,
		    setAndDocID, sourceLanguage, targetLanguage, setAndDocID, typeID);
	    gmlWrapper.performFileOperation();
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }
    
    public static void createGMLWrappedFile(List<String> inputFileNames, String outputFileName,
	    String sourceLanguage, String targetLanguage, String setAndDocID) {
	try {
	    GmlWrapper gmlWrapper = GmlWrapper.createRefGMLWrapper(inputFileNames, outputFileName,
		    setAndDocID, sourceLanguage, targetLanguage, setAndDocID);
	    gmlWrapper.performFileOperation();
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    public static void createGMLFilesForPlainFiles(
	    EvaluationFilePathsTriple plainEvaluationFilePathsTriple,
	    EvaluationFilePathsTriple evaluationFilePathsTripleGML) {
	createGMLWrappedFile(plainEvaluationFilePathsTriple.getSourceFilePath(),
		evaluationFilePathsTripleGML.getSourceFilePath(), SOURCE_LANGUAGE, TARGET_LANGUAGE,
		DEFAULT_SET_AND_DOC_ID, SOURCE_TYPE);
	createGMLWrappedFile(plainEvaluationFilePathsTriple.getReferenceFilePath(),
		evaluationFilePathsTripleGML.getReferenceFilePath(), SOURCE_LANGUAGE,
		TARGET_LANGUAGE, DEFAULT_SET_AND_DOC_ID, REFERENCE_TYPE);
	createGMLWrappedFile(plainEvaluationFilePathsTriple.getResultFilePath(),
		evaluationFilePathsTripleGML.getResultFilePath(), SOURCE_LANGUAGE, TARGET_LANGUAGE,
		DEFAULT_SET_AND_DOC_ID, TEST_TYPE);
    }

    private static class SystemInput {
	private final BufferedReader inputReader;
	private final String docID;
	private final String systemID;

	private SystemInput(BufferedReader inputReader, String docID, String systemID) {
	    this.inputReader = inputReader;
	    this.docID = docID;
	    this.systemID = systemID;
	}

	public BufferedReader getInputReader() {
	    return inputReader;
	}

	private String getDocPreamble() {
	    String result = "";
	    result += "<DOC docid=\"" + docID + "\" sysid=\"" + systemID + "\">\n";
	    return result;
	}

	private String getDocPostamble() {
	    String result = "";
	    result = "</DOC>\n";
	    return result;
	}

    }
}
