package mt_pipeline.evaluation;

import junit.framework.Assert;

public class SentenceNumberScoresTriple {
	private final int sentenceNumber;
	private final double bleuScore;
	private final double nistScore;

	private SentenceNumberScoresTriple(int sentenceNumber, double bleuScore, double nistScore) {
		this.sentenceNumber = sentenceNumber;
		this.bleuScore = bleuScore;
		this.nistScore = nistScore;
	}

	public static SentenceNumberScoresTriple createSentenceNumberScoresTriple(int sentenceNumber, double bleuScore, double nistScore) {
		return new SentenceNumberScoresTriple(sentenceNumber, bleuScore, nistScore);
	}

	public static SentenceNumberScoresTriple createSentenceNumberScoresTripleFromString(String sentenceNumberScoresTripleString) {
		String[] parts = sentenceNumberScoresTripleString.split("\\s");
		Assert.assertEquals(3, parts.length);
		return new SentenceNumberScoresTriple(Integer.parseInt(parts[0]), Double.parseDouble(parts[1]), Double.parseDouble(parts[2]));
	}

	public static void checkCompatibility(SentenceNumberScoresTriple baselineScoreTriple, SentenceNumberScoresTriple otherScoreTriple) {
		if (baselineScoreTriple.getSentenceNumber() != otherScoreTriple.getSentenceNumber()) {
			throw new RuntimeException("Error: Incompatible SentenceNumberScoreTriple instances: different sentence numbers: " + baselineScoreTriple.getSentenceNumber() + " v.s. "
					+ otherScoreTriple.getSentenceNumber());
		}
	}

	public static SentenceNumberScoresTriple getSentenceNumberScoresTripleDifference(SentenceNumberScoresTriple baselineScoreTriple, SentenceNumberScoresTriple otherScoreTriple) {
		checkCompatibility(baselineScoreTriple, otherScoreTriple);
		double bleuScoreDifference = otherScoreTriple.bleuScore - baselineScoreTriple.bleuScore;
		double nistScoreDifference = otherScoreTriple.nistScore - baselineScoreTriple.nistScore;

		return new SentenceNumberScoresTriple(baselineScoreTriple.getSentenceNumber(), bleuScoreDifference, nistScoreDifference);
	}

	public int getSentenceNumber() {
		return this.sentenceNumber;
	}

	public double getBleuScore() {
		return this.bleuScore;
	}

	public double getNistScore() {
		return this.nistScore;
	}

}
