package mt_pipeline.evaluation;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import util.ConfigFile;
import util.FileUtil;
import util.LatexStyle;
import mt_pipeline.evaluation.lrscoring.SystemScores;
import mt_pipeline.evaluation.lrscoring.SystemScoresTable;

/**
 * This class computes a table of Multeval and LRScores using two different baselines 
 * (for example Hiero and SAMT). This means statistical significance is computed 
 * and added two the table for both baselines. 
 * 
 * @author gemaille
 *
 */
public class MultiEvalAndLRScoreTableDevAndTestTwoBaselines {
    public static final List<String> MULTIPLE_BASELINE_SUFFIX_LETTER_LIST = Arrays.asList("h", "s");
    public static String NL = "\n";

    private final MultiEvalAndLRScoreTableDevAndTest firstBaselineTable;
    private final MultiEvalAndLRScoreTableDevAndTest secondBaselineTable;

    private MultiEvalAndLRScoreTableDevAndTestTwoBaselines(
	    MultiEvalAndLRScoreTableDevAndTest firstBaselineTable,
	    MultiEvalAndLRScoreTableDevAndTest secondBaselineTable) {
	this.firstBaselineTable = firstBaselineTable;
	this.secondBaselineTable = secondBaselineTable;
    }

    public static MultiEvalAndLRScoreTableDevAndTestTwoBaselines createMultiEvalAndLRScoreTableDevAndTestTwoBaselines(
	    String configFilePathFirstBaseline, String configFilePathSecondBaseline,
	    boolean showStandardDeviation) {
	MultiEvalAndLRScoreTableDevAndTest firstBaselineTable = MultiEvalAndLRScoreTableDevAndTest
		.createResultsTableForConfig(configFilePathFirstBaseline, showStandardDeviation);
	MultiEvalAndLRScoreTableDevAndTest secondBaselineTable = MultiEvalAndLRScoreTableDevAndTest
		.createResultsTableForConfig(configFilePathSecondBaseline, showStandardDeviation);
	return new MultiEvalAndLRScoreTableDevAndTestTwoBaselines(firstBaselineTable,
		secondBaselineTable);
    }

    private SystemScoresTable getFirstSystemLRScoresTable() {
	return firstBaselineTable.getLrSystemScoresTable();
    }

    private SystemScoresTable getSecondSystemLRScoresTable() {
	return secondBaselineTable.getLrSystemScoresTable();
    }

    protected String improvementNewCommandStrings() {
	String result = NL + "\\newcommand{\\SI}{$^{\\blacktriangle}$}" + NL
		+ "\\newcommand{\\SISI}{$^{\\blacktriangle\\blacktriangle}$}" + NL
		+ "\\newcommand{\\SW}{{$^{\\blacktriangledown}$}}" + NL
		+ "\\newcommand{\\SWSW}{{$^{\\blacktriangledown\\blacktriangledown}$}}" + NL
		+ "\\newcommand{\\SIh}{$^{\\blacktriangle H}$}" + NL
		+ "\\newcommand{\\SISIh}{$^{\\blacktriangle\\blacktriangle H}$}" + NL
		+ "\\newcommand{\\SWh}{{$^{\\blacktriangledown H}$}}" + NL
		+ "\\newcommand{\\SWSWh}{{$^{\\blacktriangledown\\blacktriangledown H}$}}" + NL
		+ "\\newcommand{\\SIs}{$^{\\blacktriangle S}$}" + NL
		+ "\\newcommand{\\SISIs}{$^{\\blacktriangle\\blacktriangle S}$}" + NL
		+ "\\newcommand{\\SWs}{{$^{\\blacktriangledown S}$}}" + NL
		+ "\\newcommand{\\SWSWs}{{$^{\\blacktriangledown\\blacktriangledown S}$}}" + NL;
	return result;
    }

    protected String documentHeader() {
	return LatexStyle.standardDocumentSpecificaionString()
		+ LatexStyle.usePackageWithOptions("inputenc", "utf8") 
		+ LatexStyle.usePackage("multirow") 
		+ LatexStyle.usePackage("adjustbox") 
		+ LatexStyle.usePackageWithOptions("geometry", "margin=1in")
		+ LatexStyle.usePackage("amssymb")
		+ "% Symbols for indicating significal improvement / worsening of the results with respect to the baseline results"
		+ improvementNewCommandStrings()
		+ "% See : http://www.latex-community.org/forum/viewtopic.php?f=45&t=20438 " + NL
		+ "\\newcommand{\\minitab}[2][l]{\\begin{tabular}{#1}#2\\end{tabular}}" + NL
		+LatexStyle.beginDocumentLine();

    }

    public void writeScoresToFile(String outputFilePath, boolean showStandardDeviation) {
	BufferedWriter outputWriter = null;
	try {
	    outputWriter = new BufferedWriter(new FileWriter(outputFilePath));
	    outputWriter.write(documentHeader());
	    outputWriter.write(firstBaselineTable.tableHeader());

	    MultEvalTwoBaselineSystemScoreCreater twoBaselineSystemScoreCreaterDev = new MultEvalTwoBaselineSystemScoreCreater(
		    this.firstBaselineTable.multiEvalResultsTableDev,
		    this.secondBaselineTable.multiEvalResultsTableDev, showStandardDeviation,
		    firstBaselineTable.outputLengthStatistics);

	    MultEvalTwoBaselineSystemScoreCreater twoBaselineSystemScoreCreaterTest = new MultEvalTwoBaselineSystemScoreCreater(
		    this.firstBaselineTable.multiEvalResultsTableTest,
		    this.secondBaselineTable.multiEvalResultsTableTest, showStandardDeviation,
		    firstBaselineTable.outputLengthStatistics);

	    LRScoreTwoBaselineScoreCreater lrScoreTwoBaselineScoreCreater = new LRScoreTwoBaselineScoreCreater(
		    getFirstSystemLRScoresTable(), getSecondSystemLRScoresTable());

	    for (int systemIndex = 0; systemIndex < firstBaselineTable.multiEvalResultsTableDev
		    .numSystems(); systemIndex++) {

		// Replace lower dash with gives problems in Latex
		String systemName = getFirstSystemLRScoresTable().getSystemName(systemIndex)
			.replace("_", "-");

		String lrScoreStringDev = lrScoreTwoBaselineScoreCreater
			.getMeanAndStandardDeviationStringDev(systemIndex, true,
				showStandardDeviation);
		String lrScoreStringTest = lrScoreTwoBaselineScoreCreater
			.getMeanAndStandardDeviationStringTest(systemIndex, true,
				showStandardDeviation);

		String outputString = systemName
			+ " & "
			+ twoBaselineSystemScoreCreaterDev.getSystemScoreString(lrScoreStringDev,
				systemIndex)

			+ " & "
			+ twoBaselineSystemScoreCreaterTest.getSystemScoreString(lrScoreStringTest,
				systemIndex);
		outputString += " \\\\" + NL;

		outputWriter.write(outputString);
	    }
	    outputWriter.write(firstBaselineTable.tableFooter());
	    outputWriter.write(firstBaselineTable.documentTailer());
	} catch (IOException e) {
	    e.printStackTrace();
	} finally {
	    FileUtil.closeCloseableIfNotNull(outputWriter);
	}
    }

    public static void createAndWriteResultsTableForConfig(String configFilePathFirstBaseline,
	    String configFilePathSecondBaseline, boolean showStandardDeviation) {
	ConfigFile configFirstBaseline;
	try {
	    configFirstBaseline = new ConfigFile(configFilePathFirstBaseline);
	    MultiEvalAndLRScoreTableDevAndTestTwoBaselines multiEvalAndLRScoreTableDevAndTestTwoBaselines = MultiEvalAndLRScoreTableDevAndTestTwoBaselines
		    .createMultiEvalAndLRScoreTableDevAndTestTwoBaselines(
			    configFilePathFirstBaseline, configFilePathSecondBaseline,
			    showStandardDeviation);
	    multiEvalAndLRScoreTableDevAndTestTwoBaselines.writeScoresToFile(
		    MultiEvalResultsTableDevAndTest.getOutputResultFilePath(configFirstBaseline),
		    showStandardDeviation);
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	}
    }

    /**
     * For system two, the two baselines are reversed in order. This function
     * computes the index in baseline two that corresponds to the system with
     * systemIndex for baseline one.
     * 
     * @param systemIndex
     * @return
     */
    protected static int getSystemTwoIndexForSystemOneIndex(int systemIndex) {
	if (systemIndex == 0) {
	    return 1;
	} else if (systemIndex == 1) {
	    return 0;
	}
	// Only the first two systems are affected
	else {
	    return systemIndex;
	}
    }

    public static class MultEvalTwoBaselineSystemScoreCreater {
	private final MultiEvalResultsTable<?> multiEvalResultsTableFirstBaseline;
	private final MultiEvalResultsTable<?> multiEvalResultsTableSecondBaseline;
	private final boolean showStandardDeviation;
	private final boolean outputLengthStatistics;

	private MultEvalTwoBaselineSystemScoreCreater(
		MultiEvalResultsTable<?> multiEvalResultsTableFirstBaseline,
		MultiEvalResultsTable<?> multiEvalResultsTableSecondBaseline,
		boolean showStandardDeviation, boolean outputLengthStatistics) {
	    this.multiEvalResultsTableFirstBaseline = multiEvalResultsTableFirstBaseline;
	    this.multiEvalResultsTableSecondBaseline = multiEvalResultsTableSecondBaseline;
	    this.showStandardDeviation = showStandardDeviation;
	    this.outputLengthStatistics = outputLengthStatistics;
	}

	private MulteEvalSystemScoresMultipleBaselines getMulteEvalSystemScoresMultipleBaselinesForIndex(
		int systemIndex) {

	    MultEvalSystemScores systemScoresRelativeToFirstBaseline = multiEvalResultsTableFirstBaseline
		    .getSystemScoresForIndex(systemIndex);

	    MultEvalSystemScores systemScoresRelativeToSecondBaseline = multiEvalResultsTableSecondBaseline
		    .getSystemScoresForIndex(getSystemTwoIndexForSystemOneIndex(systemIndex));
	    MultEvalSystemScores firstBaselineSystemScores = multiEvalResultsTableFirstBaseline
		    .getBaselineScores();
	    MultEvalSystemScores secondBaselineSystemScores = multiEvalResultsTableSecondBaseline
		    .getBaselineScores();

	    MulteEvalSystemScoresMultipleBaselines result = MulteEvalSystemScoresMultipleBaselines
		    .createSystemScoresTwoBaselines(systemScoresRelativeToFirstBaseline,
			    systemScoresRelativeToSecondBaseline, firstBaselineSystemScores,
			    secondBaselineSystemScores);
	    return result;
	}

	public String getSystemScoreString(String lrScoreString, int systemIndex) {
	    String result = "";
	    result += getMultEvalSystemScoresString(systemIndex, showStandardDeviation) + " & "
		    + lrScoreString;
	    if (outputLengthStatistics) {
		result += " & "
			+ getMultEvalSystemScoresStringLengthOnly(systemIndex,
				showStandardDeviation);
	    }
	    return result;
	}

	public String getMultEvalSystemScoresString(int systemIndex, boolean showStandardDeviation) {
	    return this.getMulteEvalSystemScoresMultipleBaselinesForIndex(systemIndex)
		    .getScoresString(MultiEvalResultsTable.SIGNIFICANCE_THRESHOLD_P_VALUE,
			    multiEvalResultsTableFirstBaseline.resultNumberFormat,
			    showStandardDeviation);
	}

	public String getMultEvalSystemScoresStringIncludingLength(int systemIndex,
		boolean showStandardDeviation) {
	    return this.getMulteEvalSystemScoresMultipleBaselinesForIndex(systemIndex)
		    .getScoresStringIncludingLength(
			    MultiEvalResultsTable.SIGNIFICANCE_THRESHOLD_P_VALUE,
			    multiEvalResultsTableFirstBaseline.resultNumberFormat,
			    showStandardDeviation);
	}

	public String getMultEvalSystemScoresStringLengthOnly(int systemIndex,
		boolean showStandardDeviation) {
	    return this.getMulteEvalSystemScoresMultipleBaselinesForIndex(systemIndex)
		    .getScoresStringLengthOnly(
			    MultiEvalResultsTable.SIGNIFICANCE_THRESHOLD_P_VALUE,
			    multiEvalResultsTableFirstBaseline.resultNumberFormat,
			    showStandardDeviation);
	}

    }

    public static class LRScoreTwoBaselineScoreCreater {
	private final SystemScoresTable firstBaselineLRSystemScoresTable;
	private final SystemScoresTable secondBaselineLRSystemScoresTable;

	private LRScoreTwoBaselineScoreCreater(SystemScoresTable firstBaselineLRSystemScoresTable,
		SystemScoresTable secondBaselineLRSystemScoresTable) {
	    this.firstBaselineLRSystemScoresTable = firstBaselineLRSystemScoresTable;
	    this.secondBaselineLRSystemScoresTable = secondBaselineLRSystemScoresTable;
	}

	public String getImprovementStringWithLetter(SystemScoresTable systemScoresTable,
		SystemScores systemScores, int baselineIndex) {
	    String result = systemScoresTable.getImprovementString(systemScores);
	    if (result.trim().length() > 0) {
		result = " " + result;
		result += MULTIPLE_BASELINE_SUFFIX_LETTER_LIST.get(baselineIndex);
	    }

	    return result;
	}

	public String getMeanAndStandardDeviationStringTest(int systemIndex,
		boolean multiplyScoresWithFactorHundred, boolean showStandardDeviation) {
	    SystemScores testScoresBaselineOne = firstBaselineLRSystemScoresTable
		    .getTestScoresList().get(systemIndex);
	    SystemScores testScoresBaselineTwo = secondBaselineLRSystemScoresTable
		    .getTestScoresList().get(getSystemTwoIndexForSystemOneIndex(systemIndex));

	    String result = firstBaselineLRSystemScoresTable
		    .getMeanAndStandardDeviationStringWithoutImprovementString(
			    testScoresBaselineOne, multiplyScoresWithFactorHundred,
			    showStandardDeviation);

	    result += getImprovementStringWithLetter(firstBaselineLRSystemScoresTable,
		    testScoresBaselineOne, 0);
	    result += getImprovementStringWithLetter(secondBaselineLRSystemScoresTable,
		    testScoresBaselineTwo, 1);
	    return result;
	}

	public String getMeanAndStandardDeviationStringDev(int systemIndex,
		boolean multiplyScoresWithFactorHundred, boolean showStandardDeviation) {
	    SystemScores devScoresBaselineOne = firstBaselineLRSystemScoresTable.getDevScoresList()
		    .get(systemIndex);
	    SystemScores devScoresBaselineTwo = secondBaselineLRSystemScoresTable
		    .getDevScoresList().get(getSystemTwoIndexForSystemOneIndex(systemIndex));

	    String result = firstBaselineLRSystemScoresTable
		    .getMeanAndStandardDeviationStringWithoutImprovementString(
			    devScoresBaselineOne, multiplyScoresWithFactorHundred,
			    showStandardDeviation);

	    result += getImprovementStringWithLetter(firstBaselineLRSystemScoresTable,
		    devScoresBaselineOne, 0);
	    result += getImprovementStringWithLetter(secondBaselineLRSystemScoresTable,
		    devScoresBaselineTwo, 1);
	    return result;
	}

    }

    public static void main(String[] args) {

	if (args.length != 3) {
	    System.out
		    .println("Usage : createResultsTable CONFIG_FILE_PATH_FIRST_BASELINE CONFIG_FILE_PATH_SECOND_BASELINE SHOWSTANDARD_DEVIATION_FLAG");
	    System.exit(1);
	}
	createAndWriteResultsTableForConfig(args[0], args[1], Boolean.parseBoolean(args[2]));

    }

}
