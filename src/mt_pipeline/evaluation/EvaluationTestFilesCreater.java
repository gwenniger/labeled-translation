package mt_pipeline.evaluation;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import util.Utility;

import alignment.ArtificialSentenceCreater;

public class EvaluationTestFilesCreater {

	private final static EvaluationTestSentenceCreater SOURCE_FILE_CREATER = new SourceSentenceCreater();
	private final static EvaluationTestSentenceCreater REFERENCE_FILE_CREATER = new ReferenceSentenceCreater();

	private final EvaluationFilePathsTriple evaluationFilePathsTriple;

	private final int testSize;
	private final int sentenceLenth;
	private final EvaluationTestSentenceCreater resultSentencesCreater;

	private EvaluationTestFilesCreater(EvaluationFilePathsTriple evaluationFilePathsTriple, int sentenceLenth, int testSize,
			EvaluationTestSentenceCreater resultSentencesCreater) {
		this.evaluationFilePathsTriple = evaluationFilePathsTriple;
		this.sentenceLenth = sentenceLenth;
		this.testSize = testSize;
		this.resultSentencesCreater = resultSentencesCreater;
	}

	public static EvaluationTestFilesCreater createEvaluationTestFilesCreaterCorrectEvenSentences(EvaluationFilePathsTriple evaluationFilePathsTriple,
			int sentenceLenth, int testSize) {
		return new EvaluationTestFilesCreater(evaluationFilePathsTriple, sentenceLenth, testSize, new EvenSentencesCorrectTestSentenceCreater());
	}

	public static EvaluationTestFilesCreater createEvaluationTestFilesCreaterCorrectOddSentences(EvaluationFilePathsTriple evaluationFilePathsTriple,
			int sentenceLenth, int testSize) {
		return new EvaluationTestFilesCreater(evaluationFilePathsTriple, sentenceLenth, testSize, new OddSentencesCorrectTestSentenceCreater());
	}

	public static EvaluationTestFilesCreater createEvaluationTestFilesCreaterAllCorrectSentences(EvaluationFilePathsTriple evaluationFilePathsTriple,
			int sentenceLenth, int testSize) {
		return new EvaluationTestFilesCreater(evaluationFilePathsTriple, sentenceLenth, testSize, REFERENCE_FILE_CREATER);
	}

	private void writeFile(EvaluationTestSentenceCreater evaluationTestSentenceCreater, String fileName) {
		try {
			BufferedWriter outputWriter = new BufferedWriter(new FileWriter(fileName));
			for (int i = 1; i <= testSize; i++) {
				List<String> sentence = evaluationTestSentenceCreater.createSentence(i, sentenceLenth);
				outputWriter.write(Utility.stringListStringWithoutBrackets(sentence) + "\n");
			}
			outputWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void writeFiles() {
		writeFile(SOURCE_FILE_CREATER, evaluationFilePathsTriple.getSourceFilePath());
		writeFile(REFERENCE_FILE_CREATER, evaluationFilePathsTriple.getReferenceFilePath());
		writeFile(resultSentencesCreater, evaluationFilePathsTriple.getResultFilePath());
	}

	private static interface EvaluationTestSentenceCreater {
		public List<String> createSentence(int sentencNumber, int sentenceLength);
	}

	private static class ReferenceSentenceCreater implements EvaluationTestSentenceCreater {
		@Override
		public List<String> createSentence(int sentencNumber, int sentenceLength) {
			return ArtificialSentenceCreater.createArtificialSentence(ArtificialSentenceCreater.TARGET_WORD, sentenceLength, sentencNumber);
		}
	}

	private static class SourceSentenceCreater implements EvaluationTestSentenceCreater {
		@Override
		public List<String> createSentence(int sentencNumber, int sentenceLength) {
			return ArtificialSentenceCreater.createArtificialSentence(ArtificialSentenceCreater.SOURCE_WORD, sentenceLength, sentencNumber);
		}
	}

	private static class OddSentencesCorrectTestSentenceCreater implements EvaluationTestSentenceCreater {
		@Override
		public List<String> createSentence(int sentencNumber, int sentenceLength) {

			if (Utility.isEven(sentencNumber)) {
				return ArtificialSentenceCreater.createArtificialSentence(ArtificialSentenceCreater.ERROR_WORD, sentenceLength, sentencNumber);
			} else {
				return ArtificialSentenceCreater.createArtificialSentence(ArtificialSentenceCreater.TARGET_WORD, sentenceLength, sentencNumber);
			}
		}
	}

	private static class EvenSentencesCorrectTestSentenceCreater implements EvaluationTestSentenceCreater {
		@Override
		public List<String> createSentence(int sentencNumber, int sentenceLength) {

			if (Utility.isEven(sentencNumber)) {
				return ArtificialSentenceCreater.createArtificialSentence(ArtificialSentenceCreater.TARGET_WORD, sentenceLength, sentencNumber);
			} else {
				return ArtificialSentenceCreater.createArtificialSentence(ArtificialSentenceCreater.ERROR_WORD, sentenceLength, sentencNumber);

			}
		}
	}

}
