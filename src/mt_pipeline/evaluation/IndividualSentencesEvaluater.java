package mt_pipeline.evaluation;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import util.FileStatistics;
import util.SubFileGenerator;
import util.Utility;

public class IndividualSentencesEvaluater {

	private final EvaluationFilePathsTriple originalEvaluationFilePathsTriple;
	private final String mtEvalDir;
	private final String outputScoreFilePath;

	private IndividualSentencesEvaluater(EvaluationFilePathsTriple originalEvaluationFilePathsTriple, String mtEvalDir, String outputScoreFilePath) {
		this.originalEvaluationFilePathsTriple = originalEvaluationFilePathsTriple;
		this.mtEvalDir = mtEvalDir;
		this.outputScoreFilePath = outputScoreFilePath;
	}

	public static IndividualSentencesEvaluater createIndividualSentencesEvaluater(String sourceFilePathGML, String resultFilePathGML, String referenceFilePathGML, String mtEvalDir,
			String outputScoreFilePath) {
		return new IndividualSentencesEvaluater(EvaluationFilePathsTriple.createEvaluationFilePathsTriple(sourceFilePathGML, resultFilePathGML, referenceFilePathGML), mtEvalDir, outputScoreFilePath);
	}

	public static IndividualSentencesEvaluater createIndividualSentencesEvaluaterForPlainFiles(String sourceFilePath, String resultFilePath, String referenceFilePath, String mtEvalDir,
			String outputScoreFilePath) {
		EvaluationFilePathsTriple evaluationFilePathsTripleGML = MTEvalInterface
				.createMtEvalInterfaceForPlainFilesAndGetEvaluationFilePathsTripleGML(sourceFilePath, resultFilePath, referenceFilePath);
		return new IndividualSentencesEvaluater(evaluationFilePathsTripleGML, mtEvalDir, outputScoreFilePath);
	}

	private List<Integer> getGmlWrapperHeaderLines() {
		return Arrays.asList(1, 2);
	}

	private List<Integer> getGmlWrapperTailerLines(String fileName) {
		try {
			int noLines = FileStatistics.countLines(fileName);
			return Arrays.asList((noLines - 1), noLines);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	private String lineNumSuffix(int lineNum) {
		return ".Line" + lineNum;
	}

	public String getResultSubFileName(String originalFileName, int lineNum) {
		return originalFileName + lineNumSuffix(lineNum);
	}

	private void getOneLineGMLFile(String originalGMLFileName, int lineNum) {
		HashSet<Integer> acceptableLinesSet = new HashSet<Integer>();
		acceptableLinesSet.add(lineNum + 2);
		acceptableLinesSet.addAll(getGmlWrapperHeaderLines());
		acceptableLinesSet.addAll(getGmlWrapperTailerLines(originalGMLFileName));
		String resultFileName = getResultSubFileName(originalGMLFileName, lineNum);
		System.out.println("getOneLineGMLFile - resultFileName: " + resultFileName);
		SubFileGenerator subfileGenerator = new SubFileGenerator(originalGMLFileName, resultFileName, acceptableLinesSet);
		subfileGenerator.generateSubFile();
	}

	private void createOneLineSrcFile(int lineNum) {
		getOneLineGMLFile(originalEvaluationFilePathsTriple.getSourceFilePath(), lineNum);
	}

	private void createOneLineRefFile(int lineNum) {
		getOneLineGMLFile(originalEvaluationFilePathsTriple.getReferenceFilePath(), lineNum);
	}

	private void createOneLineDecodeResultSubFileGML(int lineNum) {

		getOneLineGMLFile(originalEvaluationFilePathsTriple.getResultFilePath(), lineNum);
	}

	private int getNumResultLines() {
		try {
			return FileStatistics.countLines(this.originalEvaluationFilePathsTriple.getResultFilePath()) - 4;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private EvaluationFilePathsTriple getOneLineEvaluationFilePathTriple(int lineNum) {
		return EvaluationFilePathsTriple
				.createEvaluationFilePathsTriple(getResultSubFileName(this.originalEvaluationFilePathsTriple.getSourceFilePath(), lineNum),
						getResultSubFileName(this.originalEvaluationFilePathsTriple.getResultFilePath(), lineNum),
						getResultSubFileName(this.originalEvaluationFilePathsTriple.getReferenceFilePath(), lineNum));
	}

	private String getOneLineOutputFileName(int lineNum) {
		return this.outputScoreFilePath + this.lineNumSuffix(lineNum);
	}

	private void wrapEvaluationLineInSeparateFiles(int lineNum) {
		createOneLineDecodeResultSubFileGML(lineNum);
		createOneLineRefFile(lineNum);
		createOneLineSrcFile(lineNum);
	}

	private void deleteTemporaryOneLineFiles(EvaluationFilePathsTriple oneLineEvalationFilePathTriple, String resultFileName) {
		Utility.deleteFile(resultFileName);
		Utility.deleteFile(oneLineEvalationFilePathTriple.getSourceFilePath());
		Utility.deleteFile(oneLineEvalationFilePathTriple.getResultFilePath());
		Utility.deleteFile(oneLineEvalationFilePathTriple.getReferenceFilePath());
	}

	private String getScoresString(SentenceNumberScoresTriple sentenceNumberScoresTriple) {
		return sentenceNumberScoresTriple.getSentenceNumber() + " " + sentenceNumberScoresTriple.getBleuScore() + " " + sentenceNumberScoresTriple.getNistScore();
	}

	private void writeSentenceIndividualScores(List<SentenceNumberScoresTriple> sentenceIndividualScores) {
		try {
			BufferedWriter outputWriter = new BufferedWriter(new FileWriter(this.outputScoreFilePath));

			outputWriter.write("SentenceNumber: BleuScore: NistScore:  \n");
			for (int sentenceNumber = 1; sentenceNumber <= sentenceIndividualScores.size(); sentenceNumber++) {
				outputWriter.write(getScoresString(sentenceIndividualScores.get(sentenceNumber - 1)));
				if (sentenceNumber < sentenceIndividualScores.size()) {
					outputWriter.write("\n");
				}
			}
			outputWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void createSentencIndividualScoresFile() {

		List<SentenceNumberScoresTriple> sentenceIndividualScores = new ArrayList<SentenceNumberScoresTriple>();

		for (int sentenceNumber = 1; sentenceNumber <= getNumResultLines(); sentenceNumber++) {
			wrapEvaluationLineInSeparateFiles(sentenceNumber);
			EvaluationFilePathsTriple oneLineEvalationFilePathTriple = getOneLineEvaluationFilePathTriple(sentenceNumber);
			String resultFileName = getOneLineOutputFileName(sentenceNumber);
			MTEvalInterface mtEvalInterface = MTEvalInterface.createMtEvalInterfaceForGMLFiles(oneLineEvalationFilePathTriple.getSourceFilePath(), oneLineEvalationFilePathTriple.getResultFilePath(),
					oneLineEvalationFilePathTriple.getReferenceFilePath(), mtEvalDir, resultFileName);
			System.out.println("mtEvalDir: " + mtEvalDir);
			mtEvalInterface.computeBleuAndNist();
			try {

				sentenceIndividualScores.add(SentenceNumberScoresTriple.createSentenceNumberScoresTriple(sentenceNumber, EvaluationScoresQuadruple.getBleuFromResultFile(resultFileName),
						EvaluationScoresQuadruple.getNistFromResultFile(resultFileName)));
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
			deleteTemporaryOneLineFiles(oneLineEvalationFilePathTriple, resultFileName);
			writeSentenceIndividualScores(sentenceIndividualScores);
		}
	}
}
