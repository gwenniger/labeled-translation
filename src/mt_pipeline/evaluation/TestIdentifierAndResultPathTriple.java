package mt_pipeline.evaluation;

import mt_pipeline.mtPipelineTest.TestIdentifierPair;

public class TestIdentifierAndResultPathTriple {
	private final TestIdentifierPair testIdentifierPair;
	private final String resultPath;

	private TestIdentifierAndResultPathTriple(TestIdentifierPair testIdentifierPair, String resultPath) {
		this.testIdentifierPair = testIdentifierPair;
		this.resultPath = resultPath;
	}

	public static TestIdentifierAndResultPathTriple createTestIdentifierAndResultPathTriple(TestIdentifierPair testIdentifierPair, String resultPath) {
		return new TestIdentifierAndResultPathTriple(testIdentifierPair, resultPath);
	}

	public static TestIdentifierAndResultPathTriple createTestIdentifierAndResultPathTriple(String systemName, String testName, String resultPath) {
		return new TestIdentifierAndResultPathTriple(TestIdentifierPair.createTestIdentifierPair(systemName, testName), resultPath);
	}

	public String getResultPath() {
		return this.resultPath;
	}

	public TestIdentifierPair getTestIdentifierPair() {
		return this.testIdentifierPair;
	}

}
