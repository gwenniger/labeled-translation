package mt_pipeline.evaluation;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import junit.framework.Assert;
import util.ConfigFile;

public class MultipleLanguagePairMultipleRunFilePathsCreater {
    private static String LANGUAGE_PAIRS_KEY = "languagePairs";

    private final List<String> languagePairsList;
    private final Map<String, Map<String, List<String>>> languagePairRunFilePathsListsMaps;

    private MultipleLanguagePairMultipleRunFilePathsCreater(List<String> languagePairsList,
	    Map<String, Map<String, List<String>>> languagePairRunWeightFilePathsListsMaps) {
	this.languagePairsList = languagePairsList;
	this.languagePairRunFilePathsListsMaps = languagePairRunWeightFilePathsListsMaps;
    }

    public static MultipleLanguagePairMultipleRunFilePathsCreater createMultipleLanguagePairMultipleRunFilePathsCreater(
	    String configFilePath, List<String> subcategorizationKeysList,
	    List<String> filePathsSuffixesList) {
	try {
	    ConfigFile theConfig = new ConfigFile(configFilePath);
	    List<String> languagePairsList = getLanguagePairsListFromConfig(theConfig);
	    Map<String, Map<String, List<String>>> languagePairRunWeightFilePathsListsMaps = createLanguagePairRunConfigPathsListMaps(
		    languagePairsList, theConfig, subcategorizationKeysList, filePathsSuffixesList);

	    return new MultipleLanguagePairMultipleRunFilePathsCreater(languagePairsList,
		    languagePairRunWeightFilePathsListsMaps);
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private static List<String> getLanguagePairsListFromConfig(ConfigFile theConfig) {
	return theConfig.getMultiValueParmeterAsListWithPresenceCheck(LANGUAGE_PAIRS_KEY);
    }

    private static Map<String, List<String>> createLanguagePairRunConfigPathsListMap(
	    List<String> languagePairsList, ConfigFile theConfig, String filePathsSuffix) {
	Map<String, List<String>> languagePairRunConfigPathsListsMap = new HashMap<String, List<String>>();
	for (String languagePairString : languagePairsList) {
	    String languagePairConfigFilePathsKey = languagePairString + filePathsSuffix;
	    List<String> languagePairConfigFilePaths = theConfig
		    .getMultiValueParmeterAsListWithPresenceCheck(languagePairConfigFilePathsKey);
	    languagePairRunConfigPathsListsMap.put(languagePairString, languagePairConfigFilePaths);
	}
	return languagePairRunConfigPathsListsMap;
    }

    private static Map<String, Map<String, List<String>>> createLanguagePairRunConfigPathsListMaps(
	    List<String> languagePairsList, ConfigFile theConfig,
	    List<String> subcategorizationKeysList, List<String> filePathsSuffixesList) {
	Map<String, Map<String, List<String>>> result = new HashMap<String, Map<String, List<String>>>();
	Assert.assertEquals(subcategorizationKeysList.size(), filePathsSuffixesList.size());
	for (int i = 0; i < filePathsSuffixesList.size(); i++) {
	    String subcategorizationKey = subcategorizationKeysList.get(i);
	    String filePathsSuffix = filePathsSuffixesList.get(i);
	    result.put(
		    subcategorizationKey,
		    createLanguagePairRunConfigPathsListMap(languagePairsList, theConfig,
			    filePathsSuffix));
	}
	return result;

    }

    public List<String> getLanguagePairsList() {
	return languagePairsList;
    }

    //private Map<String, List<String>> getLanguagePairRunFilePathsListsMap(String subcategorizationKey) {
	//return this.languagePairRunFilePathsListsMaps.get(subcategorizationKey);
    //}

    public List<String> getFilePathsForSubcategorizatLanguagePairCombination(
	    String subcategorizationString, String languagePairString) {
	return this.languagePairRunFilePathsListsMaps.get(subcategorizationString).get(
		languagePairString);
    }
}
