package mt_pipeline.evaluation;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import mt_pipeline.mtPipelineTest.TestIdentifierPair;

public class ImprovementOrderedSentenceNumbersComputer {

	private final SystemSentenceScores baselineSystemScores;
	private final List<SystemSentenceScores> otherSystemsScores;
	private final List<OrderedSentenceScoresDifferences> orderedSentenceScoresDifferences;

	private ImprovementOrderedSentenceNumbersComputer(SystemSentenceScores baselineSystemScores, List<SystemSentenceScores> otherSystemsScores,
			List<OrderedSentenceScoresDifferences> orderedSenteceScoresDifferences) {
		this.baselineSystemScores = baselineSystemScores;
		this.otherSystemsScores = otherSystemsScores;
		this.orderedSentenceScoresDifferences = orderedSenteceScoresDifferences;
	}

	public static ImprovementOrderedSentenceNumbersComputer createImprovementOrderedSentenceNumbersComputer(String baselineResultsFileName, String otherSystemResultsFileName) {
		SystemSentenceScores baselineSentenceScores = SystemSentenceScores.createSystemSentenceScores(TestIdentifierPair.createTestIdentifierPair("BASELINE", "test1"), baselineResultsFileName);
		SystemSentenceScores otherSystemSentenceScores = SystemSentenceScores.createSystemSentenceScores(TestIdentifierPair.createTestIdentifierPair("OTHERSYSTEM", "test1"),
				otherSystemResultsFileName);
		List<SystemSentenceScores> otherSystemsScoresList = Collections.singletonList(otherSystemSentenceScores);
		return new ImprovementOrderedSentenceNumbersComputer(baselineSentenceScores, otherSystemsScoresList, computeSentenceScoresDifferences(baselineSentenceScores, otherSystemsScoresList));
	}

	private static List<SystemSentenceScores> createOtherSystemScores(List<TestIdentifierAndResultPathTriple> otherSystemResults) {
		List<SystemSentenceScores> result = new ArrayList<SystemSentenceScores>();
		for (TestIdentifierAndResultPathTriple otherSystemResult : otherSystemResults) {
			result.add(SystemSentenceScores.createSystemSentenceScores(otherSystemResult.getTestIdentifierPair(), otherSystemResult.getResultPath()));
		}
		return result;
	}

	public static ImprovementOrderedSentenceNumbersComputer createImprovementOrderedSentenceNumbersComputer(TestIdentifierAndResultPathTriple baselineSystemResults,
			List<TestIdentifierAndResultPathTriple> otherSystemResults) {
		SystemSentenceScores baselineSentenceScores = SystemSentenceScores.createSystemSentenceScores(baselineSystemResults.getTestIdentifierPair(), baselineSystemResults.getResultPath());
		List<SystemSentenceScores> otherSystemsScores = createOtherSystemScores(otherSystemResults);
		return new ImprovementOrderedSentenceNumbersComputer(baselineSentenceScores, otherSystemsScores, computeSentenceScoresDifferences(baselineSentenceScores, otherSystemsScores));
	}

	private static List<OrderedSentenceScoresDifferences> computeSentenceScoresDifferences(SystemSentenceScores baselineSystemScores, List<SystemSentenceScores> otherSystemsScores) {
		List<OrderedSentenceScoresDifferences> result = new ArrayList<OrderedSentenceScoresDifferences>();

		for (SystemSentenceScores otherSystemScores : otherSystemsScores) {
			result.add(OrderedSentenceScoresDifferences.createOrderedSentenceScoresDifferences(baselineSystemScores, otherSystemScores));
		}
		return result;
	}

	public List<OrderedSentenceScoresDifferences> getOrderedSentenceScoreDifferences() {
		return this.orderedSentenceScoresDifferences;
	}

	public void writeImprovementScoresToFile(String fileName) {
		try {
			BufferedWriter outputWriter = new BufferedWriter(new FileWriter(fileName));
			List<OrderedSentenceScoresDifferences> orderedSentenceScoresDifferencesList = getOrderedSentenceScoreDifferences();

			for (int scoreIndex = 0; scoreIndex < orderedSentenceScoresDifferencesList.get(0).getNumScores(); scoreIndex++) {
				for (OrderedSentenceScoresDifferences orderedSentencesScoreDifferences : orderedSentenceScoresDifferencesList) {
					outputWriter.write(orderedSentencesScoreDifferences.getImprovementStringForIndex(scoreIndex) + " ||| ");
				}
				outputWriter.write("\n");
			}
			outputWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public SystemSentenceScores getBaselineSystemScores() {
		return this.baselineSystemScores;
	}

	public List<SystemSentenceScores> getOtherSystemsScores() {
		return this.otherSystemsScores;
	}

	public int getNumOtherSystems() {
		return this.orderedSentenceScoresDifferences.size();
	}

	public int getNumScores() {
		return this.baselineSystemScores.getNumScores();
	}

	public String getImprovementScoreStringForSystemAndIndex(int systemIndex, int improvementElementIndex) {
		return this.orderedSentenceScoresDifferences.get(systemIndex).getImprovementStringForIndex(improvementElementIndex);
	}

	public String getOtherSystemTestDescription(int systemIndex) {
		return this.otherSystemsScores.get(systemIndex).getTestIdentifierPair().createTestDescription();
	}

	public String getBaselineTestDescription() {
		return this.baselineSystemScores.getTestIdentifierPair().createTestDescription();
	}

}
