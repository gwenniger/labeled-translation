package mt_pipeline.evaluation;

import java.text.NumberFormat;

import junit.framework.Assert;
import mt_pipeline.evaluation.MultiEvalResultsTableSystemsResultsCollecter.ResultEntry;

public class ScoreQuadruple {
    private static final double NINETY_FIVE_PERCENT_CONFIDENCE_MAX_P_VALUE = 0.05;
    private static final double NINETY_NINE_PERCENT_CONFIDENCE_MAX_P_VALUE = 0.01;
    private double mean;
    private double stdv;
    /**
     * The p-value for for significance of change of the value relative to the
     * baseline value
     */
    private double pValue;
    // Whether a higher score means a better score (mostly true except for TER)
    private final boolean higherIsBetter;

    ScoreQuadruple(double mean, double stdv, double pValue, boolean higherIsBetter) {
	this.mean = mean;
	this.stdv = stdv;
	this.pValue = pValue;
	this.higherIsBetter = higherIsBetter;
    }

    public static ScoreQuadruple createBleuScoreQuadruple() {
	return new ScoreQuadruple(-1, -1, -1, true);
    }

    public static ScoreQuadruple createMeteorScoreQuadruple() {
	return new ScoreQuadruple(-1, -1, -1, true);
    }

    public static ScoreQuadruple createBeerScoreQuadruple() {
	return new ScoreQuadruple(-1, -1, -1, true);
    }

    public static ScoreQuadruple createTerScoreQuadruple() {
	return new ScoreQuadruple(-1, -1, -1, false);
    }

    public static ScoreQuadruple createLengthScoreQuadruple() {
	return new ScoreQuadruple(-1, -1, -1, true);
    }

    public void addPValueFromSecondScoreTriple(ScoreQuadruple scoreTriple) {
	this.pValue = scoreTriple.pValue;
    }

    public double getMean() {
	return mean;
    }

    public double getStdv() {
	return stdv;
    }

    public double getPValue() {
	return pValue;
    }

    public boolean meanAndStandardDeviationAreSet() {
	return (mean >= 0) && (stdv >= 0);
    }

    public void updateScorePairForResultEntry(ResultEntry resultEntry) {
	if (resultEntry.isAverageScorpeType()) {
	    this.mean = resultEntry.getScore();
	} else if (resultEntry.isStandardDeviationScorpeType()) {
	    this.stdv = resultEntry.getScore();
	} else {
	    throw new RuntimeException("Wrong result entry metric name: "
		    + resultEntry.getScoreTypeName());
	}
    }

    private boolean differenceWithBaselineIsSignificant(double significanceThresholdPValue) {
	// A value is significant if it is smaller or equal than the
	// threshold.
	// Negative values are used to indicate undefined values (for the
	// baseline)
	// which are not in any case to be marked as significant
	return (this.pValue >= 0) && (this.pValue <= significanceThresholdPValue);
    }

    private String significantImprovementMarkerString() {

	if (differenceWithBaselineIsSignificant(NINETY_NINE_PERCENT_CONFIDENCE_MAX_P_VALUE)) {
	    return ImprovementStrings.NINETY_NINE_PERCENT_CONFIDENCE_IMPROVEMENT_STRING;
	} else if (differenceWithBaselineIsSignificant(NINETY_FIVE_PERCENT_CONFIDENCE_MAX_P_VALUE)) {
	    return ImprovementStrings.NINETY_FIVE_PERCENT_CONFIDENCE_IMPROVEMENT_STRING;
	}
	return "";
    }

    private String significantWorseningMarkerString() {

	if (differenceWithBaselineIsSignificant(NINETY_NINE_PERCENT_CONFIDENCE_MAX_P_VALUE)) {
	    return ImprovementStrings.NINETY_NINE_PERCENT_CONFIDENCE_WORSENING_STRING;
	} else if (differenceWithBaselineIsSignificant(NINETY_FIVE_PERCENT_CONFIDENCE_MAX_P_VALUE)) {
	    return ImprovementStrings.NINETY_FIVE_PERCENT_CONFIDENCE_WORSENING_STRING;
	}
	return "";
    }  

    private boolean resultImrproved(ScoreQuadruple baselineScore) {
	boolean result;
	if (higherIsBetter) {
	    result = this.mean > baselineScore.mean;
	} else {
	    result = this.mean < baselineScore.mean;
	}
	// System.out.println(" resultImrproved - this.mean:" + this.mean
	// + " baselineScore.mean: " + baselineScore.mean + "result: " +
	// result);
	return result;
    }

    private boolean resultWorsened(ScoreQuadruple baselineScore) {
	boolean result;
	if (higherIsBetter) {
	    result = this.mean < baselineScore.mean;
	} else {
	    result = this.mean > baselineScore.mean;
	}
	// System.out.println(" resultWorsened - this.mean:" + this.mean +
	// " baselineScore.mean: "
	// + baselineScore.mean + "result: " + result);
	return result;
    }

    private String improvementMarkerString(boolean resultImproved, boolean resultWorsened) {
	Assert.assertFalse(resultImproved && resultWorsened);

	if (resultImproved) {
	    return significantImprovementMarkerString();
	} else if (resultWorsened) {
	    return significantWorseningMarkerString();
	}

	return "";

    }

    public String getScorePairString(NumberFormat numberFormat,
	    boolean showStandardDeviation) {
	String result = "" + numberFormat.format(mean);

	if (showStandardDeviation) {
	    result += "$\\pm$";
	    result += numberFormat.format(stdv);
	}
  	return result;
      }
    
    public String getScorePairStringWithImprovementMarking(NumberFormat numberFormat,
	    ScoreQuadruple baselineScore, boolean showStandardDeviation) {
	String result = getScorePairString(numberFormat, showStandardDeviation);
	result += getImprovementMarkerString(baselineScore);
	return result;
    }

    public String getImprovementMarkerString(ScoreQuadruple baselineScore) {
	return improvementMarkerString(resultImrproved(baselineScore),
		resultWorsened(baselineScore));
    }
}