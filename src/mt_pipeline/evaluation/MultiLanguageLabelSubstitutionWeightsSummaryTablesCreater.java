package mt_pipeline.evaluation;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.NumberFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import util.ConfigFile;
import util.FileUtil;
import util.LatexStyle;
import util.Pair;

public class MultiLanguageLabelSubstitutionWeightsSummaryTablesCreater {


    private static String NL = "\n";
    private static String CAPTION_TEXT_KEY = "mainTableCaptionText";
    private static String MATCH_TABLE_CAPTION_TEXT = "Match Feature Weights.";
    private static String NOMATCH_TABLE_CAPTION_TEXT = "NoMatch Feature Weights.";
    private static String SUMMARY_TABLE_CAPTION_TEXT = "Summary feature weight values.";

    private static final NumberFormat NUMBER_FORMAT = LabelSubstitutionFeatureWeightTableConstructer
	    .createNumberFormat();
    private static final NumberFormat NUMBER_FORMAT_NO_E = LabelSubstitutionFeatureWeightTableConstructer
	    .createNumberFormatNoE();

    private final List<String> languagePairsList;
    private final Map<String, LabelSubstitutionFeatureWeightTableConstructer> labelSubstitutionFeatureWeightTableConstructorMap;
    private final String mainTableCaptionText;

    private MultiLanguageLabelSubstitutionWeightsSummaryTablesCreater(
	    List<String> languagePairsList,
	    Map<String, LabelSubstitutionFeatureWeightTableConstructer> labelSubstitutionFeatureWeightTableConstructorMap,
	    String mainTableCaptionText) {
	this.languagePairsList = languagePairsList;
	this.labelSubstitutionFeatureWeightTableConstructorMap = labelSubstitutionFeatureWeightTableConstructorMap;
	this.mainTableCaptionText = mainTableCaptionText;
    }

    public static MultiLanguageLabelSubstitutionWeightsSummaryTablesCreater createMultiLanguageLabelSubstitutionWeightsSummaryTablesCreater(
	    String configFilePath) {
	LabelSubstitutionFeatureWeightTableConstructerCreater labelSubstitutionFeatureWeightTableConstructerCreater = LabelSubstitutionFeatureWeightTableConstructerCreater
		.createLabelSubstitutionFeatureWeightTableConstructerCreater(configFilePath);
	return new MultiLanguageLabelSubstitutionWeightsSummaryTablesCreater(
		labelSubstitutionFeatureWeightTableConstructerCreater.getLanguagePairsList(),
		labelSubstitutionFeatureWeightTableConstructerCreater
			.getLabelSubstitutionFeatureWeightTableConstructerMap(),
		labelSubstitutionFeatureWeightTableConstructerCreater.getMainTableCaptionText());

    }

    private String getLatexMeanStdString(DoubleListStatistics doubleListStatistics) {
	return NUMBER_FORMAT.format(doubleListStatistics.getMean()) + LatexStyle.LATEX_PLUS_MIN_SYMBOL
		+ NUMBER_FORMAT.format(doubleListStatistics.getStd());
    }

    private String getLanguagePairLinePrefix(String languagePair) {
	return languagePair + LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR;
    }

    private String createMeansStandardDeviationsDifferencesAndRankTableLine(String languagePair) {
	System.out.println("createMeansStandardDeviationsDifferencesAndRankTableLine - "
		+ languagePair);

	String result = getLanguagePairLinePrefix(languagePair);
	LabelSubstitutionFeatureWeightTableConstructer featueWeightsConstructor = this.labelSubstitutionFeatureWeightTableConstructorMap
		.get(languagePair);

	DoubleListStatistics matchingFeatureStatistics = featueWeightsConstructor
		.getCompensatedMatchingFeaturesStatistics();
	result += getLatexMeanStdString(matchingFeatureStatistics);
	DoubleListStatistics mismatchingFeatureStatistics = featueWeightsConstructor
		.getCompensatedMisMatchingFeaturesStatistics();
	result += LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR
		+ getLatexMeanStdString(mismatchingFeatureStatistics);
	double difference = matchingFeatureStatistics.getMean()
		- mismatchingFeatureStatistics.getMean();
	result += LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR + "\\centerColumn{"
		+ NUMBER_FORMAT.format(difference) + "}";

	Pair<Double> meanSelfSubstitutionPreferenceRankAndStd = LabelSelfSubstitutionPreferenceRankComputer
		.createLabelSelfSubstitutionPreferenceRankComputer(featueWeightsConstructor)
		.computeGlobalAvarageSubstitutionPreferenceRankAndStd();
	result += LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR
		+ NUMBER_FORMAT_NO_E.format(meanSelfSubstitutionPreferenceRankAndStd.getFirst())
		+ LatexStyle.LATEX_PLUS_MIN_SYMBOL
		+ NUMBER_FORMAT.format(meanSelfSubstitutionPreferenceRankAndStd.getSecond());
	result += LatexStyle.LATEX_END_OF_LINE_STRING;
	return result;
    }

    private String createMeanStdMinMaxLine(String languagePair,
	    DoubleListStatistics doubleListStatistics) {
	String result = getLanguagePairLinePrefix(languagePair);
	result += getLatexMeanStdString(doubleListStatistics);
	result += LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR
		+ NUMBER_FORMAT.format(doubleListStatistics.getMin());
	result += LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR
		+ NUMBER_FORMAT.format(doubleListStatistics.getMax());
	result += LatexStyle.LATEX_END_OF_LINE_STRING;
	return result;
    }

    public String createMeanStdMinMaxTableHead() {
	String result = "\\begin{subtable}[b]{0.80\\textwidth}" + "\\begin{tabular}{|l|l|l|l|}"
		+ NL + "\\hline" + NL + "Language Pair & Mean/Std & Min & Max \\\\" + NL
		+ "\\hline";
	return result;
    }

    public String createMeanStdMinMaxTableFooter(String captionText) {
	String result = "\\hline" + NL + "\\end{tabular}" + NL + "\\caption{" + captionText + "}"
		+ NL + "\\end{subtable}";
	return result;
    }

    public String createMeansStandardDeviationsAndDifferencesTableHead() {
	String result = "\\begin{subtable}[b]{0.80\\textwidth}" + NL + "\\scalebox{0.9}{"
		+ "\\begin{tabular}{|C{1.5cm}|l|l|L{2.6cm}|C{2.3cm}|}" + NL + "\\hline"
		+ "\\NameEntry{Language Pair} &  \\multirow{3}{*}{Mean/Std Match} & "
		+ "\\multirow{3}{*}{Mean/Std NoMatch} & "
		+ "\\NameEntry{$Mean(Match) -$  $Mean(NoMatch)$} & "
		+ "\\NameEntry{Mean/Std SSP-Rank}  \\\\" + NL + "& & & & \\\\" + NL
		+ "& & & & \\\\ \\hline";
	return result;
    }

    private String createMeansStandardDeviationsAndDifferencesTableFooter() {
	String result = "\\hline" + NL + "\\end{tabular}}" + "\\caption{"
		+ SUMMARY_TABLE_CAPTION_TEXT + "}" + NL + "\\end{subtable}";
	return result;
    }

    private DoubleListStatistics getDoubleListStatistics(String languagePair, boolean matching) {
	LabelSubstitutionFeatureWeightTableConstructer labelSubstitutionFeatureWeightTableConstructer = this.labelSubstitutionFeatureWeightTableConstructorMap
		.get(languagePair);
	if (matching) {
	    return labelSubstitutionFeatureWeightTableConstructer
		    .getCompensatedMatchingFeaturesStatistics();
	} else {
	    return labelSubstitutionFeatureWeightTableConstructer
		    .getCompensatedMisMatchingFeaturesStatistics();
	}
    }

    private void createMeanStandardDeviationMinMaxTable(Writer writer, boolean matching,
	    String captionText) throws IOException {
	writer.write(createMeanStdMinMaxTableHead() + NL);
	for (String languagePair : this.languagePairsList) {
	    writer.write(createMeanStdMinMaxLine(languagePair,
		    getDoubleListStatistics(languagePair, matching))
		    + NL);
	}
	writer.write(createMeanStdMinMaxTableFooter(captionText));
    }

    public void createMeanStandardDeviationMinMaxSubTableMatch(Writer writer) throws IOException {
	createMeanStandardDeviationMinMaxTable(writer, true, MATCH_TABLE_CAPTION_TEXT);
    }

    public void createMeanStandardDeviationMinMaxSubTableNoMatch(Writer writer) throws IOException {
	createMeanStandardDeviationMinMaxTable(writer, false, NOMATCH_TABLE_CAPTION_TEXT);
    }

    public void createMeansStandardDeviationsAndDifferencesSubTable(Writer writer)
	    throws IOException {
	writer.write(createMeansStandardDeviationsAndDifferencesTableHead() + NL);
	for (String languagePair : this.languagePairsList) {
	    writer.write(createMeansStandardDeviationsDifferencesAndRankTableLine(languagePair)
		    + NL);
	}
	writer.write(createMeansStandardDeviationsAndDifferencesTableFooter());
    }

    private String getTableHeader() {
	return "\\begin{table*}[t]";
    }

    private String getTableFooter() {
	return NL + "\\caption{" + mainTableCaptionText + "}" + NL + "\\end{table*}";
    }

    private void writeSummarizedLabelSubstitutionFeatureWeightsStatistics(Writer writer)
	    throws IOException {
	writer.write(getTableHeader() + NL);
	writer.write(NL + NL);
	createMeanStandardDeviationMinMaxSubTableMatch(writer);
	writer.write(NL + NL);
	createMeanStandardDeviationMinMaxSubTableNoMatch(writer);
	writer.write(NL + NL);
	createMeansStandardDeviationsAndDifferencesSubTable(writer);
	writer.write(getTableFooter());
    }

    public void writeToStandardOutput() {
	try {
	    BufferedWriter log = new BufferedWriter(new OutputStreamWriter(System.out));
	    // log.write("This will be printed on stdout!\n");
	    writeSummarizedLabelSubstitutionFeatureWeightsStatistics(log);
	    log.flush();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    private static String createLatexDocumentHeader() {
	String result = "\\documentclass[a4paper,10pt]{article}"
		+ NL
		+ "\\usepackage[utf8]{inputenc}"
		+ NL
		+ "\\usepackage{multirow}"
		+ NL
		+ "\\usepackage{varwidth}"
		+ NL
		+ "\\usepackage{adjustbox}"
		+ NL
		+ "\\usepackage{subcaption}"
		+ NL
		+ "\\usepackage{array}"
		+ NL
		+ "\\usepackage{adjustbox}"
		+ NL
		+ "\\newcolumntype{L}[1]{>{\\raggedright\\let\\newline\\\\\\arraybackslash\\hspace{0pt}}m{#1}}"
		+ NL
		+ "\\newcolumntype{C}[1]{>{\\centering\\let\\newline\\\\\\arraybackslash\\hspace{0pt}}m{#1}}"
		+ NL
		+ "\\newcolumntype{R}[1]{>{\\raggedleft\\let\\newline\\\\\\arraybackslash\\hspace{0pt}}m{#1}}"
		+ NL +

		"% See: http://tex.stackexchange.com/questions/63746/newline-in-multirow-environment"
		+ NL + "\\newcommand\\NameEntry[1]{%" + NL + " \\multirow{3}*{%" + NL
		+ "  \\begin{varwidth}{5em}% --- or minipage, if you prefer a fixed width" + NL
		+ "  \\flushleft #1%" + NL + "  \\end{varwidth}}}" + NL +

		"\\newcommand{\\centerColumn}[1]{\\multicolumn{1}{c|}{#1}}" + NL +

		"\\title{Summarized Label Substitution Feature Weights}" + NL + "\\author{"
		+ LabelSubstitutionFeatureWeightTableConstructer.AUTHOR_NAME_STRING + "}" + NL
		+ "\\begin{document}" + NL + "\\maketitle" + NL + NL;
	return result;
    }

    private static String createLatexDocumentFooter() {
	return NL + NL + NL + NL + "\\end{document}";
    }

    public void writeToOutputWriter(BufferedWriter outputWriter) {
	try {
	    writeSummarizedLabelSubstitutionFeatureWeightsStatistics(outputWriter);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    private static class LabelSubstitutionFeatureWeightTableConstructerCreater {
	private static String CONFIG_FILE_PATHS_SUFFIX = "-ConfigFilePaths";
	private static String TEST_SUFFIX = "TEST";

	private final MultipleLanguagePairMultipleRunFilePathsCreater multipleLanguagePairMultipleRunFilePathsCreater;
	private final String mainTableCaptionText;

	private LabelSubstitutionFeatureWeightTableConstructerCreater(
		final MultipleLanguagePairMultipleRunFilePathsCreater multipleLanguagePairMultipleRunFilePathsCreater,
		String mainTableCaptionText) {
	    this.multipleLanguagePairMultipleRunFilePathsCreater = multipleLanguagePairMultipleRunFilePathsCreater;
	    this.mainTableCaptionText = mainTableCaptionText;
	}

	public static LabelSubstitutionFeatureWeightTableConstructerCreater createLabelSubstitutionFeatureWeightTableConstructerCreater(
		String configFilePath) {
	    try {
		ConfigFile theConfig = new ConfigFile(configFilePath);
		MultipleLanguagePairMultipleRunFilePathsCreater multipleLanguagePairMultipleRunFilePathsCreater = createMultipleLanguagePairMultipleRunFilePathsCreater(configFilePath);

		return new LabelSubstitutionFeatureWeightTableConstructerCreater(multipleLanguagePairMultipleRunFilePathsCreater, getMainTableCaptionText(theConfig));
	    } catch (FileNotFoundException e) {
		e.printStackTrace();
		throw new RuntimeException(e);
	    }

	}

	private static MultipleLanguagePairMultipleRunFilePathsCreater createMultipleLanguagePairMultipleRunFilePathsCreater(

	String configFilePath) {
	    List<String> subcategorizationKeysList = Collections.singletonList(TEST_SUFFIX);
	    List<String> filePathsSuffixesList = Collections
		    .singletonList(CONFIG_FILE_PATHS_SUFFIX);
	    return MultipleLanguagePairMultipleRunFilePathsCreater
		    .createMultipleLanguagePairMultipleRunFilePathsCreater(configFilePath,
			    subcategorizationKeysList, filePathsSuffixesList);
	}

	private static String getMainTableCaptionText(ConfigFile theConfig) {
	    return theConfig.getValueWithPresenceCheck(CAPTION_TEXT_KEY);
	}

	private List<String> getWeightFilePathsForLanguagePairString(String languagePairString) {
	    return multipleLanguagePairMultipleRunFilePathsCreater.getFilePathsForSubcategorizatLanguagePairCombination(TEST_SUFFIX, languagePairString);
	}

	private LabelSubstitutionFeatureWeightTableConstructer createLabelSubstitutionFeatureWeightTableConstructer(
		String languagePairString) {
	    List<String> weightFilePaths = getWeightFilePathsForLanguagePairString(languagePairString);
	    LabelSubstitutionFeatureWeightTableConstructer labelSubstitutionFeatureWeightTableConstructer = LabelSubstitutionFeatureWeightTableConstructer
		    .createLabelSubstitutionFeatureWeightTableConstructer(weightFilePaths);
	    labelSubstitutionFeatureWeightTableConstructer.addWeightsFromFileLineIterators();
	    return labelSubstitutionFeatureWeightTableConstructer;

	}

	public Map<String, LabelSubstitutionFeatureWeightTableConstructer> getLabelSubstitutionFeatureWeightTableConstructerMap() {
	    Map<String, LabelSubstitutionFeatureWeightTableConstructer> result = new HashMap<String, LabelSubstitutionFeatureWeightTableConstructer>();
	    for (String languagePairString : multipleLanguagePairMultipleRunFilePathsCreater.getLanguagePairsList()) {
		result.put(languagePairString,
			createLabelSubstitutionFeatureWeightTableConstructer(languagePairString));
	    }
	    return result;
	}

	public List<String> getLanguagePairsList() {
	    return multipleLanguagePairMultipleRunFilePathsCreater.getLanguagePairsList();
	}

	public String getMainTableCaptionText() {
	    return mainTableCaptionText;
	}
    }

    public static void main(String[] args) {
	if (args.length != 3) {
	    for (String arg : args) {
		System.out.println(arg);
	    }
	    System.out
		    .println("Usage:  multiLanguageLabelSubstitutionWeightsSummaryTablesCreater ConfigFilePathParentRelative ConfigFilePathPhraseCentric ResultFilePath");
	    System.exit(1);
	}

	BufferedWriter outputWriter = null;
	try {
	    outputWriter = new BufferedWriter(new FileWriter(args[2]));

	    outputWriter.write(MultiLanguageLabelSubstitutionWeightsSummaryTablesCreater
		    .createLatexDocumentHeader());
	    MultiLanguageLabelSubstitutionWeightsSummaryTablesCreater multiLanguageLabelSubstitutionWeightsSummaryTablesCreaterParentRelative = createMultiLanguageLabelSubstitutionWeightsSummaryTablesCreater(args[0]);
	    multiLanguageLabelSubstitutionWeightsSummaryTablesCreaterParentRelative
		    .writeToStandardOutput();
	    multiLanguageLabelSubstitutionWeightsSummaryTablesCreaterParentRelative
		    .writeToOutputWriter(outputWriter);

	    MultiLanguageLabelSubstitutionWeightsSummaryTablesCreater multiLanguageLabelSubstitutionWeightsSummaryTablesCreaterPhraseCentric = createMultiLanguageLabelSubstitutionWeightsSummaryTablesCreater(args[1]);
	    multiLanguageLabelSubstitutionWeightsSummaryTablesCreaterPhraseCentric
		    .writeToStandardOutput();

	    multiLanguageLabelSubstitutionWeightsSummaryTablesCreaterPhraseCentric
		    .writeToOutputWriter(outputWriter);

	    outputWriter.write(NL + NL + NL
		    + LabelSelfSubstitutionPreferenceRankComputer.getMetricExplanationString());

	    outputWriter.write(MultiLanguageLabelSubstitutionWeightsSummaryTablesCreater
		    .createLatexDocumentFooter());
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} finally {
	    FileUtil.closeCloseableIfNotNull(outputWriter);
	}

    }

}
