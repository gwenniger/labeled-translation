package mt_pipeline.evaluation;

import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import bitg.Pair;

public class NBestListFeaturesAnalyzer {

    private static final String NUM_TOTAL_RULES_EXCEPT_START_AND_END_FEATURE_NAME = "tm_pt_5";
    private static final String NUM_GLUE_RULES_FEATURE_NAME = "tm_pt_6";
    private static final String NUM_PURELY_LEXICAL_RULES = "tm_pt_9";

    private static final String NUM_TOTAL_RULES_EXCEPT_START_AND_END_FEATURE_NAME_MEANINGFULL = "Num-total-rules-except-start-and-end";
    private static final String NUM_GLUE_RULES_FEATURE_NAME_MEANINGFULL = "Num-glue-rules";
    private static final String NUM_PURELY_LEXICAL_RULES_MEANINGFULL = "Num-purely-lexical-rules";

    private static final String NUM_SYNTACTIC_RULES = "NumSyntacticRules";

    private final List<Map<String, Double>> sentencesFeaturesValuesMaps;
    private final List<Map<String, Double>> derivedFeaturesValuesMaps;

    private final String numTotalRulesExceptStartAndEndFeatureName;
    private final String numGlueRulesFeatureName;
    private final String numLexicalRulesFeatureName;

    private NBestListFeaturesAnalyzer(List<Map<String, Double>> sentencesFeaturesValuesMaps,
	    List<Map<String, Double>> derivedFeaturesValuesMaps,
	    String numTotalRulesExceptStartAndEndFeatureName, String numGlueRulesFeatureName,
	    String numLexicalRulesFeatureName) {
	this.sentencesFeaturesValuesMaps = sentencesFeaturesValuesMaps;
	this.derivedFeaturesValuesMaps = derivedFeaturesValuesMaps;
	this.numTotalRulesExceptStartAndEndFeatureName = numTotalRulesExceptStartAndEndFeatureName;
	this.numGlueRulesFeatureName = numGlueRulesFeatureName;
	this.numLexicalRulesFeatureName = numLexicalRulesFeatureName;
    }

    public static NBestListFeaturesAnalyzer createNBestListFeaturesAnalyzer(String nBestListFilePath) {
	FeatureValuesMatrixCreater featureValuesMatrixCreater = FeatureValuesMatrixCreater
		.createFeatureValuesMatrixCreater(nBestListFilePath);
	List<Map<String, Double>> derivedFeaturesValuesMaps = new ArrayList<Map<String, Double>>();
	NBestListFeaturesAnalyzer result = new NBestListFeaturesAnalyzer(
		featureValuesMatrixCreater.getSentencesFeatureValueMaps(),
		derivedFeaturesValuesMaps, NUM_TOTAL_RULES_EXCEPT_START_AND_END_FEATURE_NAME,
		NUM_GLUE_RULES_FEATURE_NAME, NUM_PURELY_LEXICAL_RULES);
	result.fillDerivedFeaturesValuesMaps();
	return result;
    }

    private void fillDerivedFeaturesValuesMaps() {
	int sentenceNum = 0;
	for (Map<String, Double> featuresValuesMap : this.sentencesFeaturesValuesMaps) {
	    System.out.println("Computing derived features for sentence: " + sentenceNum);
	    Map<String, Double> derivedFeaturesValuesMap = new HashMap<String, Double>();
	    
	    double numSyntacticRules = computeNumSyntacticRules(featuresValuesMap);
	    System.out.println("num Syntactic rules: " + numSyntacticRules);
	    derivedFeaturesValuesMap.put(NUM_SYNTACTIC_RULES,
		    numSyntacticRules);
	    this.derivedFeaturesValuesMaps.add(derivedFeaturesValuesMap);
	    sentenceNum++;
	}
    }

    private double computeTotalOverAllSentences(String featureName,List<Map<String, Double>> featuresValuesMaps) {
	double total = 0;
	for (Map<String, Double> featureValuesMap : featuresValuesMaps) {
	    if (featureValuesMap.containsKey(featureName)) {
		total += featureValuesMap.get(featureName);
	    }
	}
	return total;
    }

    private int getNumSentences() {
	return this.sentencesFeaturesValuesMaps.size();
    }

    private double computeMean(String featureName,List<Map<String, Double>> featuresValuesMaps) {
	return computeTotalOverAllSentences(featureName,featuresValuesMaps) / getNumSentences();
    }

    private static Set<String> getAllPresentFeatureNames(
	    List<Map<String, Double>> featuresValuesMaps) {
	Set<String> result = new HashSet<String>();
	for (Map<String, Double> sentenceFeaturesValuesMap : featuresValuesMaps) {
	    result.addAll(sentenceFeaturesValuesMap.keySet());
	}
	return result;
    }

    private Set<String> getAllPresentSentenceFeatureNames() {
	return getAllPresentFeatureNames(sentencesFeaturesValuesMaps);
    }

    private Set<String> getAllPresentDerivedFeatureNames() {
	return getAllPresentFeatureNames(derivedFeaturesValuesMaps);
    }

    private String getSentenceFeaturesMeansString() {
	String result = "";
	for (String featureName : getAllPresentSentenceFeatureNames()) {
	    result += "\n" + getMeaningfullFeatureName(featureName) + " mean value: "
		    + computeMean(featureName,sentencesFeaturesValuesMaps);
	}
	return result;
    }

    
    private String getSelectedFeaturesMeansString() {
	String result = "";
	for (String featureName : Arrays.asList(this.numTotalRulesExceptStartAndEndFeatureName, this.numGlueRulesFeatureName, this.numLexicalRulesFeatureName)) {
	    result += "\n" + getMeaningfullFeatureName(featureName) + " mean value: " + computeMean(featureName,sentencesFeaturesValuesMaps);
	}
	return result;
    }
    
    private String getDerivedFeaturesMeansString() {
	String result = "";
	for (String featureName : getAllPresentDerivedFeatureNames()) {
	    result += "\n" + featureName + " mean value: " + computeMean(featureName,derivedFeaturesValuesMaps);
	}
	return result;
    }
    
    

    public void showFeatureMeans() {
	System.out.println(getSentenceFeaturesMeansString());
	System.out.println(getSelectedFeaturesMeansString());
	System.out.println(getDerivedFeaturesMeansString());
    }

    private double getTotalNumRuleExceptStartAndEndRule(Map<String, Double> featuresValuesMap) {
	return featuresValuesMap.get(numTotalRulesExceptStartAndEndFeatureName);
    }

    private double getNumGlueRules(Map<String, Double> featuresValuesMap) {
	return featuresValuesMap.get(numGlueRulesFeatureName);
    }

    private double getNumPurelyLexicalRules(Map<String, Double> featuresValuesMap) {
	if (!featuresValuesMap.containsKey(numLexicalRulesFeatureName)) {
	    return 0;
	}
	return featuresValuesMap.get(numLexicalRulesFeatureName);
    }

    private boolean isTotalNumRuleExceptStartAndEndRuleFeatureName(String featureName) {
	return featureName.equals(numTotalRulesExceptStartAndEndFeatureName);
    }

    private boolean isNumGlueRulesFeatureName(String featureName) {
	return featureName.equals(numGlueRulesFeatureName);
    }

    private boolean isNumPurelyLexicalRulesFeatureName(String featureName) {
	return featureName.equals(numLexicalRulesFeatureName);
    }

    private String getMeaningfullFeatureName(String featureName) {
	if (isTotalNumRuleExceptStartAndEndRuleFeatureName(featureName)) {
	    return NUM_TOTAL_RULES_EXCEPT_START_AND_END_FEATURE_NAME_MEANINGFULL;
	} else if (isNumGlueRulesFeatureName(featureName)) {
	    return NUM_GLUE_RULES_FEATURE_NAME_MEANINGFULL;
	} else if (isNumPurelyLexicalRulesFeatureName(featureName)) {
	    return NUM_PURELY_LEXICAL_RULES_MEANINGFULL;
	} else {
	    return featureName;
	}
    }

    private double computeNumSyntacticRules(Map<String, Double> featuresValuesMap) {
	return getTotalNumRuleExceptStartAndEndRule(featuresValuesMap)
		- getNumGlueRules(featuresValuesMap) - getNumPurelyLexicalRules(featuresValuesMap);
    }

    private static class FeatureValuesMatrixCreater {
	final LineIterator nBestListIterator;

	private FeatureValuesMatrixCreater(LineIterator nBestListIterator) {
	    this.nBestListIterator = nBestListIterator;
	}

	private static FeatureValuesMatrixCreater createFeatureValuesMatrixCreater(
		String nBestListPath) {
	    try {
		return new FeatureValuesMatrixCreater(FileUtils.lineIterator(
			new File(nBestListPath), "UTF-8"));
	    } catch (IOException e) {
		e.printStackTrace();
		throw new RuntimeException(e);
	    }
	}

	private List<Pair<String, Double>> getFeaturesFromNBestListCandidateString(
		String nBestListCandidateString) {
	    List<Pair<String, Double>> result = new ArrayList<Pair<String, Double>>();
	    String[] parts = JoshuaStyle.spitOnRuleSeparator(nBestListCandidateString);
	    String featuresPart = parts[2];

	    String[] featureComponents = featuresPart.split(" ");
	    for (int i = 0; i < featureComponents.length; i++) {
		String[] subParts = featureComponents[i].split("=");
		Double featureValue = Double.parseDouble(subParts[1]);
		result.add(new Pair<String, Double>(subParts[0], featureValue));
	    }
	    return result;
	}

	private Map<String, Double> getFeaturesMapFromNBestListCandidateString(
		String nBestListCandidateString) {
	    Map<String, Double> result = new HashMap<String, Double>();

	    for (Pair<String, Double> featureNameValuePair : getFeaturesFromNBestListCandidateString(nBestListCandidateString)) {
		result.put(featureNameValuePair.first, featureNameValuePair.last);
	    }
	    return result;
	}

	private int getSentenceNumberFromNBestListString(String nBestListCandidateString) {
	    String[] parts = JoshuaStyle.spitOnRuleSeparator(nBestListCandidateString);
	    return Integer.parseInt(parts[0]);
	}

	private List<Map<String, Double>> getSentencesFeatureValueMaps() {
	    List<Map<String, Double>> result = new ArrayList<Map<String, Double>>();

	    int lastSentenceNumber = -1;
	    while (nBestListIterator.hasNext()) {
		String nBestListCandidateString = nBestListIterator.nextLine();
		int sentenceNumber = getSentenceNumberFromNBestListString(nBestListCandidateString);
		if (sentenceNumber != lastSentenceNumber) {
		    result.add(getFeaturesMapFromNBestListCandidateString(nBestListCandidateString));
		    lastSentenceNumber = sentenceNumber;
		}
	    }
	    return result;
	}
    }

    public static void main(String[] args) {

	if (args.length != 1) {
	    System.out.println("Usage: nBestListFeaturesAnalyzer NBestListFilePath");
	    System.exit(1);
	}

	NBestListFeaturesAnalyzer nBestListFeaturesAnalyzer = NBestListFeaturesAnalyzer
		.createNBestListFeaturesAnalyzer(args[0]);
	nBestListFeaturesAnalyzer.showFeatureMeans();
    }

}
