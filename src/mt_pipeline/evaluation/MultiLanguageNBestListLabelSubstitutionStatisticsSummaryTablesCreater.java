package mt_pipeline.evaluation;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mt_pipeline.evaluation.SentencesNBestListLabelSubstitutionStatisticsComputer.NBestListsLabelSubstitutionstatistics;
import util.ConfigFile;
import util.FileUtil;
import util.LatexStyle;

public class MultiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater {
    private static final NumberFormat NUMBER_FORMAT = LabelSubstitutionFeatureWeightTableConstructer
	    .createNumberFormat();
    private static final NumberFormat NUMBER_FORMAT_NO_E = LabelSubstitutionFeatureWeightTableConstructer
	    .createNumberFormatNoE();

    private final NbestListSubstitutionStatisticsData nBestListSubstitutionStatisticsData;

    private MultiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater(
	    NbestListSubstitutionStatisticsData nBestListSubstitutionStatisticsData) {
	this.nBestListSubstitutionStatisticsData = nBestListSubstitutionStatisticsData;
    }

    public static final MultiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater createMultiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater(
	    String configFilePath, boolean useDevData, boolean useTestData) {
	return new MultiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater(
		NbestListSubstitutionStatisticsData.createNbestListSubstitutionStatisticsData(
			configFilePath, useDevData, useTestData));
    }

    public String createDevTableRow(String languagePairString, String rowDescriptionString,
	    NBestListsLabelSubstitutionstatisticsView nBestListsLabelSubstitutionstatisticsView) {
	String result = rowDescriptionString + LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR;
	NBestListsLabelSubstitutionstatistics devNBestListsLabelSubstitutionstatistics = nBestListSubstitutionStatisticsData
		.getDevSentencesNBestListLabelSubstitutionStatisticsComputer(languagePairString);
	result += nBestListsLabelSubstitutionstatisticsView
		.getLatexStyleSubstitutionStatistics(devNBestListsLabelSubstitutionstatistics)
		+ LatexStyle.LATEX_END_OF_LINE_STRING + "\n";
	return result;
    }

    public String createTestTableRow(String languagePairString, String rowDescriptionString,
	    NBestListsLabelSubstitutionstatisticsView nBestListsLabelSubstitutionstatisticsView) {

	String result = rowDescriptionString + LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR;
	NBestListsLabelSubstitutionstatistics testNBestListsLabelSubstitutionstatistics = nBestListSubstitutionStatisticsData
		.getTestSentencesNBestListLabelSubstitutionStatisticsComputer(languagePairString);
	result += nBestListsLabelSubstitutionstatisticsView
		.getLatexStyleSubstitutionStatistics(testNBestListsLabelSubstitutionstatistics)
		+ LatexStyle.LATEX_END_OF_LINE_STRING + "\n";
	return result;
    }

    private String createTableString(
	    NBestListsLabelSubstitutionstatisticsView nBestListsLabelSubstitutionstatisticsView) {
	String result = "";
	for (String languagePairString : nBestListSubstitutionStatisticsData.getLanguagePairsList()) {
	    result += createDevTableRow(languagePairString, languagePairString + "-dev",
		    nBestListsLabelSubstitutionstatisticsView) + "\n";
	    result += createTestTableRow(languagePairString, languagePairString + "-test",
		    nBestListsLabelSubstitutionstatisticsView) + "\n";
	}

	return result;
    }

    private String createTableStringMeanOnly() {
	return createTableString(new NBestListsLabelSubstitutionstatisticsMeanOnlyView());
    }

    private String createTableStringMeanAndStd() {
	return createTableString(new NBestListsLabelSubstitutionstatisticsMeanAndStd());
    }

    private String tablesHeader() {
	return nBestListSubstitutionStatisticsData.getSystemDescriptionString() + "\n\n";
    }

    private String tablesFooter() {
	return "\n========================================================================\n\n";
    }

    public void writeTablesToWriter(Writer writer) throws IOException {
	writer.write(tablesHeader());
	writer.write("Mean only table\n");
	writer.write(createTableStringMeanOnly());
	writer.write("\n\nMean and std table\n");
	writer.write(createTableStringMeanAndStd());
	writer.write(tablesFooter());
    }

    public List<String> getLanguagePairsList() {
	return this.nBestListSubstitutionStatisticsData.getLanguagePairsList();
    }

    public String getSystemDescriptionString() {
	return this.nBestListSubstitutionStatisticsData.getSystemDescriptionString();
    }

    protected static abstract class NBestListsLabelSubstitutionstatisticsView {

	protected abstract String getLatexStyleSubstitutionStatistics(
		NBestListsLabelSubstitutionstatistics nBestListsLabelSubstitutionstatistics);

    }

    protected static class NBestListsLabelSubstitutionstatisticsMeanAndStd extends
	    NBestListsLabelSubstitutionstatisticsView {

	protected NBestListsLabelSubstitutionstatisticsMeanAndStd() {
	}

	@Override
	protected String getLatexStyleSubstitutionStatistics(
		NBestListsLabelSubstitutionstatistics nBestListsLabelSubstitutionstatistics) {
	    return getLatexStyleSubstitutionStatisticsMeanAndStd(nBestListsLabelSubstitutionstatistics);
	}

	private String getLatexStyleSubstitutionStatisticsMeanAndStd(
		NBestListsLabelSubstitutionstatistics nBestListsLabelSubstitutionstatistics) {
	    String result = "";
	    result += NUMBER_FORMAT_NO_E.format(nBestListsLabelSubstitutionstatistics
		    .getMeanMatchSubstitutionsFraction())
		    + LatexStyle.LATEX_PLUS_MIN_SYMBOL
		    + NUMBER_FORMAT.format(nBestListsLabelSubstitutionstatistics
			    .getStdMatchSubstittuionsFraction())
		    + LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR
		    + NUMBER_FORMAT_NO_E.format(nBestListsLabelSubstitutionstatistics
			    .getMeanNoMatchSubstitutionsFraction())
		    + LatexStyle.LATEX_PLUS_MIN_SYMBOL
		    + NUMBER_FORMAT.format(nBestListsLabelSubstitutionstatistics
			    .getStdNoMatchSubstittuionsFraction())
		    + LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR
		    + NUMBER_FORMAT_NO_E.format(nBestListsLabelSubstitutionstatistics
			    .getMeanGlueRuleFraction())
		    + LatexStyle.LATEX_PLUS_MIN_SYMBOL
		    + NUMBER_FORMAT.format(nBestListsLabelSubstitutionstatistics
			    .getStdGlueRuleFraction());
	    return result;
	}
    }

    protected static class NBestListsLabelSubstitutionstatisticsMeanOnlyView extends
	    NBestListsLabelSubstitutionstatisticsView {

	protected NBestListsLabelSubstitutionstatisticsMeanOnlyView() {
	}

	@Override
	protected String getLatexStyleSubstitutionStatistics(
		NBestListsLabelSubstitutionstatistics nBestListsLabelSubstitutionstatistics) {
	    return getLatexStyleSubstitutionStatisticsMeanOnly(nBestListsLabelSubstitutionstatistics);
	}

	private String getLatexStyleSubstitutionStatisticsMeanOnly(
		NBestListsLabelSubstitutionstatistics nBestListsLabelSubstitutionstatistics) {
	    String result = "";
	    result += NUMBER_FORMAT_NO_E.format(nBestListsLabelSubstitutionstatistics
		    .getMeanMatchSubstitutionsFraction())
		    + LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR
		    + NUMBER_FORMAT_NO_E.format(nBestListsLabelSubstitutionstatistics
			    .getMeanNoMatchSubstitutionsFraction())
		    + LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR
		    + NUMBER_FORMAT_NO_E.format(nBestListsLabelSubstitutionstatistics
			    .getMeanGlueRuleFraction());

	    return result;
	}

    }

    private static class NbestListSubstitutionStatisticsData {
	private static final String TEST_CONFIG_FILE_PATHS_SUFFIX = "-TestNBestListFilePaths";
	private static final String DEV_CONFIG_FILE_PATHS_SUFFIX = "-DevNBestListFilePaths";
	private static final String TEST_KEY = "TEST";
	private static final String DEV_KEY = "DEV";
	private static final String USE_ONLY_FIRST_CANDIDATE_IN_NBEST_LIST_PROPERTY = "useOnlyFirstCandidateInNBestList";
	private static final String SYSTEM_DESCRIPTION_STRING_PROPERTY = "systemDescriptionString";
	private static final String IS_SOFT_MATCHING_SYSTEM_PROPERTY = "isSoftMatchingSystem";

	private static final List<String> SUBCATEGORIZATION_KEY_LIST = Arrays.asList(DEV_KEY,
		TEST_KEY);
	private static final List<String> FILE_PATH_SUFFIXES_LIST = Arrays.asList(
		DEV_CONFIG_FILE_PATHS_SUFFIX, TEST_CONFIG_FILE_PATHS_SUFFIX);

	private final List<String> languagePairsList;
	private final Map<String, NBestListsLabelSubstitutionstatistics> devNbestListSubstitutionStatisticsData;
	private final Map<String, NBestListsLabelSubstitutionstatistics> testNbestListSubstitutionStatisticsData;
	private final String systemDescriptionString;

	private NbestListSubstitutionStatisticsData(
		List<String> languagePairsList,
		Map<String, NBestListsLabelSubstitutionstatistics> devNbestListSubstitutionStatisticsData,
		Map<String, NBestListsLabelSubstitutionstatistics> testNbestListSubstitutionStatisticsData,
		String systemDescriptionString) {
	    this.languagePairsList = languagePairsList;
	    this.devNbestListSubstitutionStatisticsData = devNbestListSubstitutionStatisticsData;
	    this.testNbestListSubstitutionStatisticsData = testNbestListSubstitutionStatisticsData;
	    this.systemDescriptionString = systemDescriptionString;
	}

	public static NbestListSubstitutionStatisticsData createNbestListSubstitutionStatisticsData(
		String configFilePath, boolean useDevData, boolean useTestData) {
	    MultipleLanguagePairMultipleRunFilePathsCreater multipleLanguagePairMultipleRunFilePathsCreater = MultipleLanguagePairMultipleRunFilePathsCreater
		    .createMultipleLanguagePairMultipleRunFilePathsCreater(configFilePath,
			    SUBCATEGORIZATION_KEY_LIST, FILE_PATH_SUFFIXES_LIST);

	    ConfigFile theConfig;
	    try {
		theConfig = new ConfigFile(configFilePath);
		boolean useOnlyFirstCandidateInNBestList = theConfig
			.getBooleanWithPresenceCheck(USE_ONLY_FIRST_CANDIDATE_IN_NBEST_LIST_PROPERTY);

		String systemDescriptionString = theConfig
			.getValueWithPresenceCheck(SYSTEM_DESCRIPTION_STRING_PROPERTY);

		boolean isSoftMatchingSystem = theConfig
			.getBooleanWithPresenceCheck(IS_SOFT_MATCHING_SYSTEM_PROPERTY);

		Map<String, NBestListsLabelSubstitutionstatistics> devNbestListSubstitutionStatisticsData = null;
		Map<String, NBestListsLabelSubstitutionstatistics> testNbestListSubstitutionStatisticsData = null;

		if (!(useDevData || useTestData)) {
		    throw new RuntimeException(
			    "Error: NbestListSubstitutionStatisticsData must use either dev or test data, or both");
		}

		if (useDevData) {
		    devNbestListSubstitutionStatisticsData = createnNbestListSubstitutionStatisticsDataMap(
			    multipleLanguagePairMultipleRunFilePathsCreater, DEV_KEY,
			    useOnlyFirstCandidateInNBestList, isSoftMatchingSystem);
		}
		if (useTestData) {
		    testNbestListSubstitutionStatisticsData = createnNbestListSubstitutionStatisticsDataMap(
			    multipleLanguagePairMultipleRunFilePathsCreater, TEST_KEY,
			    useOnlyFirstCandidateInNBestList, isSoftMatchingSystem);
		}

		return new NbestListSubstitutionStatisticsData(
			multipleLanguagePairMultipleRunFilePathsCreater.getLanguagePairsList(),
			devNbestListSubstitutionStatisticsData,
			testNbestListSubstitutionStatisticsData, systemDescriptionString);
	    } catch (FileNotFoundException e) {
		e.printStackTrace();
		throw new RuntimeException(e);
	    }

	}

	private static Map<String, NBestListsLabelSubstitutionstatistics> createnNbestListSubstitutionStatisticsDataMap(
		MultipleLanguagePairMultipleRunFilePathsCreater multipleLanguagePairMultipleRunFilePathsCreater,
		String subCategory, boolean useOnlyFirstCandidateInNBestList,
		boolean isSoftMatchingSystem) {
	    Map<String, NBestListsLabelSubstitutionstatistics> result = new HashMap<String, NBestListsLabelSubstitutionstatistics>();

	    int fileNum = 0;

	    for (String languagePairString : multipleLanguagePairMultipleRunFilePathsCreater
		    .getLanguagePairsList()) {

		List<String> nBestListFilePaths = multipleLanguagePairMultipleRunFilePathsCreater
			.getFilePathsForSubcategorizatLanguagePairCombination(subCategory,
				languagePairString);
		SentencesNBestListLabelSubstitutionStatisticsComputer sentencesNBestListLabelSubstitutionStatisticsComputer = SentencesNBestListLabelSubstitutionStatisticsComputer
			.createSentencesNBestListLabelSubstitutionStatisticsComputer(
				useOnlyFirstCandidateInNBestList, isSoftMatchingSystem);
		for (String nBestListFilePath : nBestListFilePaths) {
		    sentencesNBestListLabelSubstitutionStatisticsComputer
			    .addStatisticsFromTarGzFile(nBestListFilePath);
		    fileNum++;
		    int totalFiles = nBestListFilePaths.size()
			    * multipleLanguagePairMultipleRunFilePathsCreater
				    .getLanguagePairsList().size();
		    double percentageCompleted = (100 * (((double) fileNum) / totalFiles));
		    System.out.println("Completed " + percentageCompleted
			    + "% of processing data for " + subCategory);
		}
		result.put(languagePairString,
			sentencesNBestListLabelSubstitutionStatisticsComputer
				.getNBestListsLabelSubstitutionstatistics());
	    }

	    return result;
	}

	public List<String> getLanguagePairsList() {
	    return languagePairsList;
	}

	public NBestListsLabelSubstitutionstatistics getDevSentencesNBestListLabelSubstitutionStatisticsComputer(
		String languagePairString) {
	    return this.devNbestListSubstitutionStatisticsData.get(languagePairString);
	}

	public NBestListsLabelSubstitutionstatistics getTestSentencesNBestListLabelSubstitutionStatisticsComputer(
		String languagePairString) {
	    return this.testNbestListSubstitutionStatisticsData.get(languagePairString);
	}

	public String getSystemDescriptionString() {
	    return systemDescriptionString;
	}

    }

    public static void createAndWriteTablesForConfig(String configFilePath, Writer outputWriter)
	    throws IOException {
	MultiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater multiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater = MultiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater
		.createMultiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater(
			configFilePath, true, true);
	System.out.println(multiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater
		.createTableStringMeanAndStd());
	System.out.println("\n\n");
	System.out.println(multiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater
		.createTableStringMeanOnly());

	multiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater
		.writeTablesToWriter(outputWriter);

    }

    public static void createAndWriteTablesForMetaConfig(String metaConfigFilePath,
	    String writerOutputFilePath) throws IOException {

	ConfigFile metaConfig = MultiLanguageMultiSystemNBestListLabelSubstitutionStatisticsTableCreater
		.createConfigFile(metaConfigFilePath);

	for (String configFilePath : MultiLanguageMultiSystemNBestListLabelSubstitutionStatisticsTableCreater
		.getConfigFilePathsFromMetaConfig(metaConfig)) {
	    BufferedWriter outputWriter = null;
	    try {
		// Create writer in "append" mode
		outputWriter = new BufferedWriter(new FileWriter(writerOutputFilePath, true));

		createAndWriteTablesForConfig(configFilePath, outputWriter);

	    } catch (IOException e) {
		e.printStackTrace();
	    } finally {
		FileUtil.closeCloseableIfNotNull(outputWriter);
	    }
	}

    }

    private static void writeEmtpyFile(String filePath) {
	BufferedWriter outputWriter = null;
	try {
	    outputWriter = new BufferedWriter(new FileWriter(filePath));
	    outputWriter.close();

	} catch (IOException e) {
	    e.printStackTrace();
	} finally {
	    FileUtil.closeCloseableIfNotNull(outputWriter);
	}
    }

    public static void main(String[] args) {
	if (args.length != 2) {
	    System.out
		    .println("usage: multiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater META_CONFIGFILEPATH OUTPUTFILEPATH");
	    System.exit(1);
	}
	String metaConfigFilePath = args[0];
	String outputFilePath = args[1];
	writeEmtpyFile(outputFilePath);
	try {
	    createAndWriteTablesForMetaConfig(metaConfigFilePath, outputFilePath);
	} catch (IOException e) {
	    e.printStackTrace();
	}

    }

}
