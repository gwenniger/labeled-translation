package mt_pipeline.evaluation;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import util.ConfigFile;
import util.FileUtil;
import util.LatexStyle;
import mt_pipeline.evaluation.MultiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater.NBestListsLabelSubstitutionstatisticsView;
import mt_pipeline.evaluation.MultiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater.NBestListsLabelSubstitutionstatisticsMeanOnlyView;
import mt_pipeline.evaluation.MultiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater.NBestListsLabelSubstitutionstatisticsMeanAndStd;

public class MultiLanguageMultiSystemNBestListLabelSubstitutionStatisticsTableCreater {
    private static String NL = "\n";
    private static final String CONFIG_FILE_PATHS_LIST_PROPERTY = "configFilePathsList";
    private static final String WRITE_DEV_RESULTS_PROPERTY = "writeDevResults";
    private static final String WRITE_TEST_RESULTS_PROPERTY = "writeTestResults";
    private static String MATCH_FRACTION_LABEL = "Match";
    private static String NOMATCH_FRACTION_LABEL = "NoMatch";
    private static String GLUE_RULE_FRACTION_LABEL = "Glue Rule";

    private final List<MultiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater> systemStatisticsCreaters;
    private final boolean writeDevResults;
    private final boolean writeTestResults;

    private MultiLanguageMultiSystemNBestListLabelSubstitutionStatisticsTableCreater(
	    List<MultiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater> systemStatisticsCreaters,
	    boolean writeDevResults, boolean writeTestResults) {
	this.systemStatisticsCreaters = systemStatisticsCreaters;
	this.writeDevResults = writeDevResults;
	this.writeTestResults = writeTestResults;
    }

    public static MultiLanguageMultiSystemNBestListLabelSubstitutionStatisticsTableCreater createMultiLanguageMultiSystemNBestListLabelSubstitutionStatisticsTableCreater(
	    String metaConfigFilePath) {
	ConfigFile metaConfig = createConfigFile(metaConfigFilePath);
	boolean writeDevResults = writeDevResults(metaConfig);
	boolean writeTestResults = writeTestResults(metaConfig);
	return new MultiLanguageMultiSystemNBestListLabelSubstitutionStatisticsTableCreater(
		createSystemStatisticCreaters(metaConfig, writeDevResults, writeTestResults),
		writeDevResults, writeTestResults);
    }

    private static List<MultiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater> createSystemStatisticCreaters(
	    ConfigFile metaConfig, boolean useDevData, boolean useTestData) {

	List<MultiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater> result = new ArrayList<MultiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater>();

	for (String configFilePath : getConfigFilePathsFromMetaConfig(metaConfig)) {
	    MultiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater multiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater = MultiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater
		    .createMultiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater(
			    configFilePath, useDevData, useTestData);
	    result.add(multiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater);
	}
	return result;
    }

    public static List<String> getConfigFilePathsFromMetaConfig(ConfigFile theConfig) {
	return theConfig
		.getMultiValueParmeterAsListWithPresenceCheck(CONFIG_FILE_PATHS_LIST_PROPERTY);
    }

    public static boolean writeDevResults(ConfigFile theConfig) {
	return theConfig.getBooleanWithPresenceCheck(WRITE_DEV_RESULTS_PROPERTY);
    }

    public static boolean writeTestResults(ConfigFile theConfig) {
	return theConfig.getBooleanWithPresenceCheck(WRITE_TEST_RESULTS_PROPERTY);
    }

    public static ConfigFile createConfigFile(String metaConfigFilePath) {
	try {
	    ConfigFile theConfig = new ConfigFile(metaConfigFilePath);
	    return theConfig;
	} catch (FileNotFoundException e) {
	    throw new RuntimeException(e);
	}
    }

    private List<String> getLanguagePairsList() {
	Assert.assertTrue(!systemStatisticsCreaters.isEmpty());
	return systemStatisticsCreaters.get(0).getLanguagePairsList();
    }

    private String tableHeader() {
	String result = LatexStyle.TABLE_STAR_BEGIN + "[tbh]" + NL;
	result += LatexStyle.CENTERING + NL;
	result += "\\resizebox{0.90" + LatexStyle.TEXT_WIDTH + "}{!}{%" + NL;
	result += LatexStyle.beginTabularCommandWithLColumnSpecification(4) + NL;

	result += LatexStyle.HLINE + NL;
	result += "{" + LatexStyle.multiRow(1, "*", "\\minitab[c]{ System  Name }") + "} "
		+ LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR + MATCH_FRACTION_LABEL
		+ LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR + NOMATCH_FRACTION_LABEL
		+ LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR + GLUE_RULE_FRACTION_LABEL
		+ LatexStyle.LATEX_END_OF_LINE_STRING + NL;
	return result;
    }

    private String tableFooter() {

	String result = LatexStyle.TABULAR_END + NL + "}" + NL + LatexStyle.TABLE_STAR_END + NL;
	return result;
    }

    private String languagePairSubtableHeader(String languagePairString) {
	String result = LatexStyle.HLINE + NL;
	result += LatexStyle.LATEX_TABLE_COLUMN_SEPARATOR
		+ LatexStyle.multiColumn(3, "c|", languagePairString)
		+ LatexStyle.LATEX_END_OF_LINE_STRING + NL;
	result += LatexStyle.cLine(2, 4) + NL;
	return result;
    }

    private String languagePairSubtableFooter() {
	return LatexStyle.HLINE + NL;
    }

    private String systemDescriptionStringDev(
	    MultiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater systemStatisticsCreater) {
	String result = systemStatisticsCreater.getSystemDescriptionString();
	if (writeDevResults && writeTestResults) {
	    result += "-dev";
	}
	return result;
    }

    private String systemDescriptionStringTest(
	    MultiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater systemStatisticsCreater) {
	String result = systemStatisticsCreater.getSystemDescriptionString();
	if (writeDevResults && writeTestResults) {
	    result += "-test";
	}
	return result;
    }

    private String createTable(
	    NBestListsLabelSubstitutionstatisticsView nBestListsLabelSubstitutionstatisticsView) {
	String result = "";
	result += tableHeader();

	for (String languagePairString : getLanguagePairsList()) {
	    result += languagePairSubtableHeader(languagePairString);
	    for (MultiLanguageNBestListLabelSubstitutionStatisticsSummaryTablesCreater systemStatisticsCreater : systemStatisticsCreaters) {
		if (writeDevResults) {
		    result += systemStatisticsCreater.createDevTableRow(languagePairString,
			    systemDescriptionStringDev(systemStatisticsCreater),
			    nBestListsLabelSubstitutionstatisticsView);
		}
		if (writeTestResults) {
		    result += systemStatisticsCreater.createTestTableRow(languagePairString,
			    systemDescriptionStringTest(systemStatisticsCreater),
			    nBestListsLabelSubstitutionstatisticsView);
		}
	    }
	    result += languagePairSubtableFooter();
	}
	result += tableFooter();

	return result;

    }

    public String createTableMeanOnly() {
	return createTable(new NBestListsLabelSubstitutionstatisticsMeanOnlyView());
    }

    public String createTableMeanAndStd() {
	return createTable(new NBestListsLabelSubstitutionstatisticsMeanAndStd());
    }

    public void writeTables(String outputFilePath) {
	BufferedWriter outputWriter = null;
	try {
	    outputWriter = new BufferedWriter(new FileWriter(outputFilePath));
	    outputWriter.write(createTableMeanOnly());
	    outputWriter.write(NL + NL + NL);
	    outputWriter.write(createTableMeanAndStd());
	} catch (IOException e) {
	    e.printStackTrace();
	} finally {
	    FileUtil.closeCloseableIfNotNull(outputWriter);
	}
    }

    public static void main(String[] args) {
	if (args.length != 2) {
	    System.out
		    .println("usage:  multiLanguageMultiSystemNBestListLabelSubstitutionStatisticsTableCreater META_CONFIGFILEPATH OUTPUTFILEPATH");
	    System.exit(1);
	}
	String metaConfigFilePath = args[0];
	String outputFilePath = args[1];
	MultiLanguageMultiSystemNBestListLabelSubstitutionStatisticsTableCreater multiLanguageMultiSystemNBestListLabelSubstitutionStatisticsTableCreater = MultiLanguageMultiSystemNBestListLabelSubstitutionStatisticsTableCreater
		.createMultiLanguageMultiSystemNBestListLabelSubstitutionStatisticsTableCreater(metaConfigFilePath);
	multiLanguageMultiSystemNBestListLabelSubstitutionStatisticsTableCreater
		.writeTables(outputFilePath);
    }
}
