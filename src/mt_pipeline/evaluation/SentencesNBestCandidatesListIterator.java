package mt_pipeline.evaluation;

import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.io.FileUtils;

public class SentencesNBestCandidatesListIterator implements Iterator<List<String>> {

    private final Iterator<String> nBestListFileLinesIterator;
    private List<String> nextTranslationNBestList;
    private String nextLine;
    private int currentTranslationNumber = -1;

    private SentencesNBestCandidatesListIterator(Iterator<String> nBestListFileLinesIterator) {
	this.nBestListFileLinesIterator = nBestListFileLinesIterator;
	this.initialize();
    }

    public static SentencesNBestCandidatesListIterator createSentencesNBestCandidatesListIteratorTarGzFile(
	    String nBestListTarGzFilePath) {
	TarGzFileLineInterator nBestListTargzFileLineIterator = TarGzFileLineInterator
		.createTarGzFileLineInterator(nBestListTarGzFilePath);

	SentencesNBestCandidatesListIterator result = new SentencesNBestCandidatesListIterator(
		nBestListTargzFileLineIterator);
	return result;

    }

    @SuppressWarnings("unchecked")
    public static SentencesNBestCandidatesListIterator createSentencesNBestCandidatesListIterator(
	    String nBestListFilePath) {
	Iterator<String> lineIterator;
	try {
	    lineIterator = FileUtils.lineIterator(new File(nBestListFilePath), "UTF-8");
	    SentencesNBestCandidatesListIterator result = new SentencesNBestCandidatesListIterator(
		    lineIterator);
	    return result;
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}

    }

    private int getNextLineTranslationNumber() {
	//System.out.println("line: " + nextLine);
	String translationNumberPart = JoshuaStyle.spitOnRuleSeparator(nextLine)[0];
	return Integer.parseInt(translationNumberPart);
    }


    private void readNextLineTarGzFileLinesIterator() {
	if (nBestListFileLinesIterator.hasNext()) {
	    nextLine = nBestListFileLinesIterator.next();
	} else {
	    nextLine = null;
	}
    }

    private void initializeNextLineAndTranslationNumber() {
	readNextLineTarGzFileLinesIterator();
	if (nextLine == null) {
	    throw new RuntimeException(
		    "SentencesNBestCandidatesListIterator created with TarGzFile line iterator for empty file");
	}
	currentTranslationNumber = getNextLineTranslationNumber();

    }

    private void initialize() {
	initializeNextLineAndTranslationNumber();
	obtainNextNBestListFromTarGzLineIterator();
    }

    private boolean nextLineTranslationNumberIsCurrentTranslationNumber() {
	return getNextLineTranslationNumber() == currentTranslationNumber;
    }

    private void obtainNextNBestListFromTarGzLineIterator() {
	if (nextLine == null) {
	    nextTranslationNBestList = null;
	    return;
	}

	nextTranslationNBestList = new ArrayList<String>();
	while ((nextLine != null) && nextLineTranslationNumberIsCurrentTranslationNumber()) {
	    nextTranslationNBestList.add(nextLine);
	    readNextLineTarGzFileLinesIterator();
	}

	if (nextLine != null) {
	    currentTranslationNumber = getNextLineTranslationNumber();
	}
    }

    @Override
    public boolean hasNext() {
	return (nextTranslationNBestList != null);
    }

    @Override
    public List<String> next() {
	List<String> result = nextTranslationNBestList;
	obtainNextNBestListFromTarGzLineIterator();
	return result;
    }

    @Override
    public void remove() {
	throw new RuntimeException("not implemented");
    }

    public static void main(String[] args) {
	if (args.length != 1) {
	    System.out.println("usage:  sentencesNBestCandidatesListIterator TARGZ_FILE_PATH");
	    System.exit(1);
	}

	SentencesNBestCandidatesListIterator sentencesNBestCandidatesListIterator = createSentencesNBestCandidatesListIteratorTarGzFile((args[0]));
	int sentenceNumber = 0;
	while (sentencesNBestCandidatesListIterator.hasNext() && (sentenceNumber < 2)) {
	    // System.out.println(tarGzFileLineInterator.next());
	    List<String> nBestList = sentencesNBestCandidatesListIterator.next();
	    System.out.println(">>>>>>> Lines for sentence nuber " + sentenceNumber
		    + " <<<<<<<<<<<<");
	    if (sentenceNumber == 1) {
		for (String nBestListLine : nBestList) {
		    System.out.println(nBestListLine);
		}
	    }
	    System.out
		    .println("=====================================================================");

	    sentenceNumber++;
	}
	System.out.println("counted " + sentenceNumber + " different sentences in NBest list");
    }

}
