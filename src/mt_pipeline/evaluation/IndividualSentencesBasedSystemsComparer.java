package mt_pipeline.evaluation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import util.FileUtil;

import mt_pipeline.mtPipelineTest.TestIdentifierPair;
import mt_pipeline.mt_config.MTConfigFile;
import mt_pipeline.mt_config.MTFilesConfig;
import mt_pipeline.mt_config.MTFoldersConfig;

public class IndividualSentencesBasedSystemsComparer {

	private static final String SCORE_SEPARATOR = " ||| ";
	private final ImprovementOrderedSentenceNumbersComputer devNoTuningImprovementComputer;
	private final ImprovementOrderedSentenceNumbersComputer devTunedImprovementComputer;
	private final ImprovementOrderedSentenceNumbersComputer testNoTuningImprovementComputer;
	private final ImprovementOrderedSentenceNumbersComputer testTunedImprovementComputer;

	private IndividualSentencesBasedSystemsComparer(ImprovementOrderedSentenceNumbersComputer devNoTuningImprovementComputer, ImprovementOrderedSentenceNumbersComputer devTunedImprovementComputer,
			ImprovementOrderedSentenceNumbersComputer testNoTuningImprovementComputer, ImprovementOrderedSentenceNumbersComputer testTunedImprovementComputer) {
		this.devNoTuningImprovementComputer = devNoTuningImprovementComputer;
		this.devTunedImprovementComputer = devTunedImprovementComputer;
		this.testNoTuningImprovementComputer = testNoTuningImprovementComputer;
		this.testTunedImprovementComputer = testTunedImprovementComputer;
	}

	private String getImprovementStringForSystemIndex(int systemIndex, int improvementElementIndex) {
		String result = "";
		result += devNoTuningImprovementComputer.getImprovementScoreStringForSystemAndIndex(systemIndex, improvementElementIndex) + SCORE_SEPARATOR ;
		result += devTunedImprovementComputer.getImprovementScoreStringForSystemAndIndex(systemIndex, improvementElementIndex) + SCORE_SEPARATOR;
		result += testNoTuningImprovementComputer.getImprovementScoreStringForSystemAndIndex(systemIndex, improvementElementIndex) + SCORE_SEPARATOR;
		result += testTunedImprovementComputer.getImprovementScoreStringForSystemAndIndex(systemIndex, improvementElementIndex) + SCORE_SEPARATOR;
		return result;
	}

	private ImprovementOrderedSentenceNumbersComputer getAllRepresentableSentenceNumbersComputer() {
		return this.devNoTuningImprovementComputer;
	}

	private int getNumOtherSystems() {
		return getAllRepresentableSentenceNumbersComputer().getNumOtherSystems();
	}

	private int getNumScores() {
		return getAllRepresentableSentenceNumbersComputer().getNumScores();
	}

	private String getTestImprovementsDescription(int systemIndex) {
		return getAllRepresentableSentenceNumbersComputer().getOtherSystemTestDescription(systemIndex) + " relative to baseline sytem "
				+ getAllRepresentableSentenceNumbersComputer().getBaselineTestDescription();
	}

	private String createSystemImprovementScoresString(int systemIndex) {
		String result = "Improvement scores for system: " + getTestImprovementsDescription(systemIndex) + "\n\n";
		result += "DevNoTuning" + SCORE_SEPARATOR + "DevTuned" + SCORE_SEPARATOR + "TestNoTuning" +  SCORE_SEPARATOR + "TestTuned" + "\n";
		for (int improvementElementIndex = 0; improvementElementIndex < getNumScores(); improvementElementIndex++) {
			result += getImprovementStringForSystemIndex(systemIndex, improvementElementIndex) + "\n";
		}
		result += "\n\n";
		return result;
	}

	private static List<TestIdentifierPair> getTestIdentifiersForTestRootFolder(String testRootFolderPath) {
		List<TestIdentifierPair> result = new ArrayList<TestIdentifierPair>();
		File testRootFolder = new File(testRootFolderPath);

		for (File folder : FileUtil.getAllFoldersInFolder(testRootFolder)) {
			if (!folder.getName().equals(MTFoldersConfig.DATA_NAME)) {
				TestIdentifierPair testIdentifierPair = TestIdentifierPair.createTestIdentifierPair(folder.getName(), testRootFolder.getName());
				result.add(testIdentifierPair);
			}
		}
		return result;

	}

	private static TestIdentifierAndResultPathTriple getTestIdentifierAndResultPathTripleForSystemAndSettings(String testRootFolder, TestIdentifierPair testIdentifierPair, String category,
			boolean useTunedParameters) {
		String systemRootFolder = testRootFolder + testIdentifierPair.getSystemName() + "/";
		String resultPath = MTFilesConfig.getSentencesIndividualScoresResultPathForCategory(systemRootFolder, useTunedParameters, category);
		return TestIdentifierAndResultPathTriple.createTestIdentifierAndResultPathTriple(testIdentifierPair, resultPath);
	}

	private static ImprovementOrderedSentenceNumbersComputer createImprovementOrderedSentenceNumbersComputer(String baselineContainingTestRootFolderPath, String otherSystemsTestRootFolderPath,
			String category, boolean useTunedParameters, String baselineName) {
		TestIdentifierAndResultPathTriple baseLineTriple = null;
		List<TestIdentifierAndResultPathTriple> otherSystemsList = new ArrayList<TestIdentifierAndResultPathTriple>();

		for (TestIdentifierPair testIdentifierPair : getTestIdentifiersForTestRootFolder(baselineContainingTestRootFolderPath)) {
			System.out.println("System name: " + testIdentifierPair.getSystemName());
			if (testIdentifierPair.getSystemName().equals(baselineName)) {
				baseLineTriple = getTestIdentifierAndResultPathTripleForSystemAndSettings(baselineContainingTestRootFolderPath, testIdentifierPair, category, useTunedParameters);
			} else {
				otherSystemsList.add(getTestIdentifierAndResultPathTripleForSystemAndSettings(baselineContainingTestRootFolderPath, testIdentifierPair, category, useTunedParameters));
			}
		}

		for (TestIdentifierPair testIdentifierPair : getTestIdentifiersForTestRootFolder(otherSystemsTestRootFolderPath)) {
			otherSystemsList.add(getTestIdentifierAndResultPathTripleForSystemAndSettings(baselineContainingTestRootFolderPath, testIdentifierPair, category, useTunedParameters));
		}

		if (baseLineTriple == null) {
			throw new RuntimeException("No baseline system has been found with the name: " + baselineName);
		}

		return ImprovementOrderedSentenceNumbersComputer.createImprovementOrderedSentenceNumbersComputer(baseLineTriple, otherSystemsList);
	}

	public static IndividualSentencesBasedSystemsComparer createIndividualSentencesBasedSystemsComparer(String baselineContainingTestRootFolderPath, String otherSystemsTestRootFolderPath,
			String baselineName) {
		ImprovementOrderedSentenceNumbersComputer devNoTuningImprovementComputer = createImprovementOrderedSentenceNumbersComputer(baselineContainingTestRootFolderPath,
				otherSystemsTestRootFolderPath, MTConfigFile.DEV_CATEGORY, false, baselineName);
		ImprovementOrderedSentenceNumbersComputer devTunedImprovementComputer = createImprovementOrderedSentenceNumbersComputer(baselineContainingTestRootFolderPath, otherSystemsTestRootFolderPath,
				MTConfigFile.DEV_CATEGORY, true, baselineName);
		ImprovementOrderedSentenceNumbersComputer testNoTuningImprovementComputer = createImprovementOrderedSentenceNumbersComputer(baselineContainingTestRootFolderPath,
				otherSystemsTestRootFolderPath, MTConfigFile.TEST_CATEGORY, false, baselineName);
		ImprovementOrderedSentenceNumbersComputer testTunedImprovementComputer = createImprovementOrderedSentenceNumbersComputer(baselineContainingTestRootFolderPath, otherSystemsTestRootFolderPath,
				MTConfigFile.TEST_CATEGORY, true, baselineName);

		return new IndividualSentencesBasedSystemsComparer(devNoTuningImprovementComputer, devTunedImprovementComputer, testNoTuningImprovementComputer, testTunedImprovementComputer);
	}

	public void writeSentenceBasedSystemComparisionsToFile(String fileName) {
		try {
			System.out.println("Writing system based comparsion to " + fileName);
			BufferedWriter outputWriter = new BufferedWriter(new FileWriter(fileName));
			for (int systemIndex = 0; systemIndex < this.getNumOtherSystems(); systemIndex++) {
				outputWriter.write(createSystemImprovementScoresString(systemIndex));
			}
			outputWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
