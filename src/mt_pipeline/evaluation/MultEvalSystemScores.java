package mt_pipeline.evaluation;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;
import mt_pipeline.evaluation.MultiEvalResultsTableSystemsResultsCollecter.ResultEntry;

public abstract class MultEvalSystemScores {

    protected final ScoreQuadruple bleu;
    protected final ScoreQuadruple meteor;
    protected final ScoreQuadruple ter;
    protected final ScoreQuadruple length;
    protected final String systemName;
    protected final boolean outputLengthStatistics;

    protected MultEvalSystemScores(ScoreQuadruple bleu, ScoreQuadruple meteor, ScoreQuadruple ter,
	    ScoreQuadruple length, String systemName, boolean outputLengthStatistics) {
	this.bleu = bleu;
	this.meteor = meteor;
	this.ter = ter;
	this.length = length;
	this.systemName = systemName;
	this.outputLengthStatistics = outputLengthStatistics;
    }

    protected void addPValuesFromSecondSystemScoresBasicMetrics(
	    MultEvalSystemScores systemScoresWithPValues) {
	this.bleu.addPValueFromSecondScoreTriple(systemScoresWithPValues.bleu);
	this.meteor.addPValueFromSecondScoreTriple(systemScoresWithPValues.meteor);
	this.ter.addPValueFromSecondScoreTriple(systemScoresWithPValues.ter);
	this.length.addPValueFromSecondScoreTriple(systemScoresWithPValues.length);
    }

    protected ScoreQuadruple getScorePairForResultEntryStandardSet(ResultEntry resultEntry) {
	if (resultEntry.isBleuEntry()) {
	    return bleu;
	} else if (resultEntry.isMeteorEntry()) {
	    return meteor;
	} else if (resultEntry.isTerEntry()) {
	    return ter;
	} else if (resultEntry.isLengthEntry()) {
	    return length;
	} else {
	    return null;
	}
    }

    protected abstract ScoreQuadruple getScorePairForResultEntry(ResultEntry resultEntry);

    public void updateSystemScoresForResultEntry(ResultEntry resultEntry) {
	ScoreQuadruple scorePairToChange = getScorePairForResultEntry(resultEntry);
	scorePairToChange.updateScorePairForResultEntry(resultEntry);
    }

    public boolean allScoresAreSet() {
	return (bleu.meanAndStandardDeviationAreSet() && meteor.meanAndStandardDeviationAreSet() && ter
		.meanAndStandardDeviationAreSet());
    }

    public abstract List<ScoreQuadruple> getOrderdScoreTriplesList();

    /*
     * @Override public String getScoresString(double
     * significanceThresholdPValue, NumberFormat numberFormat, SystemScores
     * baselineScores, boolean showStandardDeviation) { return
     * bleu.getScorePairStringWithImprovementMarking(numberFormat,
     * baselineScores.bleu, true, showStandardDeviation) + " & " +
     * meteor.getScorePairStringWithImprovementMarking(numberFormat,
     * baselineScores.meteor, true, showStandardDeviation) + " & " +
     * ter.getScorePairStringWithImprovementMarking(numberFormat,
     * baselineScores.ter, false, showStandardDeviation); }
     */

    protected List<ScoreQuadruple> getScoreTriplesExceptLength() {
	return getOrderdScoreTriplesList().subList(0, getOrderdScoreTriplesList().size() - 1);
    }

    // protected boolean higherIsBetter(ScoreTriple scoreTriple){
    // if(scoreTriple.)
    // }

    public static String getScoreString(List<ScoreQuadruple> scoreQuadruples,
	    List<ScoreQuadruple> baselineScoreQuadruples, double significanceThresholdPValue,
	    NumberFormat numberFormat, boolean showStandardDeviation) {
	String result = "";

	for (int i = 0; i < scoreQuadruples.size(); i++) {
	    ScoreQuadruple scoreTriple = scoreQuadruples.get(i);
	    ScoreQuadruple baselineScoreTriple = baselineScoreQuadruples.get(i);

	    result += scoreTriple.getScorePairStringWithImprovementMarking(numberFormat,
		    baselineScoreTriple, showStandardDeviation);
	    if (i < scoreQuadruples.size() - 1) {
		result += " & ";
	    }
	}
	return result;

    }

    public String getScoresString(double significanceThresholdPValue, NumberFormat numberFormat,
	    MultEvalSystemScores baselineScores, boolean showStandardDeviation) {
	return getScoreString(getScoreTriplesExceptLength(),
		baselineScores.getScoreTriplesExceptLength(), significanceThresholdPValue,
		numberFormat, showStandardDeviation);
    }

    public String getScoresStringIncludingLength(double significanceThresholdPValue,
	    NumberFormat numberFormat, MultEvalSystemScores baselineScores,
	    boolean showStandardDeviation) {
	return getScoreString(getOrderdScoreTriplesList(),
		baselineScores.getOrderdScoreTriplesList(), significanceThresholdPValue,
		numberFormat, showStandardDeviation);
    }

    public String getScoresStringLengthOnly(double significanceThresholdPValue,
	    NumberFormat numberFormat, MultEvalSystemScores baselineScores,
	    boolean showStandardDeviation) {
	String result = "";
	int index = getOrderdScoreTriplesList().size() - 1;
	ScoreQuadruple scoreTriple = getOrderdScoreTriplesList().get(index);
	ScoreQuadruple baselineScoreTriple = baselineScores.getOrderdScoreTriplesList().get(index);
	result += scoreTriple.getScorePairStringWithImprovementMarking(numberFormat,
		baselineScoreTriple, showStandardDeviation);
	return result;
    }

    public String getSystemName() {
	return systemName;
    }

    public static class SystemScoresBasic extends MultEvalSystemScores {

	protected SystemScoresBasic(ScoreQuadruple bleu, ScoreQuadruple meteor, ScoreQuadruple ter,
		ScoreQuadruple length, String systemName, boolean outputLengthStatistics) {
	    super(bleu, meteor, ter, length, systemName, outputLengthStatistics);
	}

	public static SystemScoresBasic createSystemScores(String systemName, ScoreQuadruple bleu,
		ScoreQuadruple meteor, ScoreQuadruple ter, ScoreQuadruple length,
		boolean outputLengthStatistics) {
	    return new SystemScoresBasic(bleu, meteor, ter, length, systemName,
		    outputLengthStatistics);
	}

	public static SystemScoresBasic createSystemScores(String systemName,
		boolean outputLengthStatistics) {
	    return new SystemScoresBasic(ScoreQuadruple.createBleuScoreQuadruple(),
		    ScoreQuadruple.createMeteorScoreQuadruple(),
		    ScoreQuadruple.createTerScoreQuadruple(),
		    ScoreQuadruple.createLengthScoreQuadruple(), systemName, outputLengthStatistics);
	}

	@Override
	protected ScoreQuadruple getScorePairForResultEntry(ResultEntry resultEntry) {
	    ScoreQuadruple result = getScorePairForResultEntryStandardSet(resultEntry);
	    if (result != null) {
		return result;
	    } else {
		throw new RuntimeException("Wrong result entry metric name: "
			+ resultEntry.getMetricName());
	    }
	}

	@Override
	public List<ScoreQuadruple> getOrderdScoreTriplesList() {
	    return Arrays.asList(bleu, meteor, ter, length);
	}

	protected void addPValuesFromSecondSystemScores(MultEvalSystemScores systemScoresWithPValues) {
	    addPValuesFromSecondSystemScoresBasicMetrics(systemScoresWithPValues);
	}
    }

    protected static class SystemScoresWithBeer extends MultEvalSystemScores {

	protected final ScoreQuadruple beer;

	protected SystemScoresWithBeer(ScoreQuadruple bleu, ScoreQuadruple meteor,
		ScoreQuadruple beer, ScoreQuadruple ter, ScoreQuadruple length, String systemName,
		boolean outputLengthStatistics) {
	    super(bleu, meteor, ter, length, systemName, outputLengthStatistics);
	    this.beer = beer;
	}

	public static SystemScoresWithBeer createSystemScores(String systemName,
		ScoreQuadruple bleu, ScoreQuadruple meteor, ScoreQuadruple beer,
		ScoreQuadruple ter, ScoreQuadruple length, boolean outputLengthStatistics) {
	    return new SystemScoresWithBeer(bleu, meteor, beer, ter, length, systemName,
		    outputLengthStatistics);
	}

	public static SystemScoresWithBeer createSystemScores(String systemName,
		boolean outputLengthStatistics) {
	    return new SystemScoresWithBeer(ScoreQuadruple.createBleuScoreQuadruple(),
		    ScoreQuadruple.createMeteorScoreQuadruple(),
		    ScoreQuadruple.createBeerScoreQuadruple(),
		    ScoreQuadruple.createTerScoreQuadruple(),
		    ScoreQuadruple.createLengthScoreQuadruple(), systemName, outputLengthStatistics);
	}

	@Override
	protected ScoreQuadruple getScorePairForResultEntry(ResultEntry resultEntry) {
	    ScoreQuadruple result = getScorePairForResultEntryStandardSet(resultEntry);
	    if (result != null) {
		return result;
	    } else {
		if (resultEntry.isBeerEntry()) {
		    return beer;
		} else {

		    throw new RuntimeException("Wrong result entry metric name: "
			    + resultEntry.getMetricName());
		}
	    }
	}

	@Override
	public List<ScoreQuadruple> getOrderdScoreTriplesList() {
	    return Arrays.asList(bleu, meteor, beer, ter, length);
	}

	protected void addPValuesFromSecondSystemScores(SystemScoresWithBeer systemScoresWithPValues) {
	    addPValuesFromSecondSystemScoresBasicMetrics(systemScoresWithPValues);
	    this.beer.addPValueFromSecondScoreTriple(systemScoresWithPValues.beer);

	}
    }
}
