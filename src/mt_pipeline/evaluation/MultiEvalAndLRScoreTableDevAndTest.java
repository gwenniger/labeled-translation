package mt_pipeline.evaluation;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import junit.framework.Assert;
import mt_pipeline.evaluation.lrscoring.LRScoreTableCreater;
import mt_pipeline.evaluation.lrscoring.SystemScoresTable;
import util.ConfigFile;
import util.FileUtil;

public class MultiEvalAndLRScoreTableDevAndTest extends MultiEvalResultsTableDevAndTest {

    public static final String USE_BEER_METRIC_PROPERTY_STRING = "useBeerMetric";
    public static final String OUTPUT_LENGTH_STATISTICS_PROPERTY_STRING = "outputLengthStatistics";
    
    private static final String LR_SCORE_DEV_SCORES_COMUTATION_CONFIG_FILE_PATH_PROPERTY = "lrScoreDevScoresComputationConfigFilePath";
    private static final String LR_SCORE_TEST_SCORES_COMPUTATION_CONFIG_FILE_PATH_PROPERTY = "lrScoreTestScoresComputationConfigFilePath";

    private final SystemScoresTable lrSystemScoresTable;
    private final boolean useBeerMetric;
    protected final boolean outputLengthStatistics; 

    protected MultiEvalAndLRScoreTableDevAndTest(MultiEvalResultsTable<?> multiEvalResultsTableDev,
	    MultiEvalResultsTable<?> multiEvalResultsTableTest, SystemScoresTable lrSystemScoresTable,
	    boolean useBeerMetric,boolean outputLengthStatistics) {
	super(multiEvalResultsTableDev, multiEvalResultsTableTest);
	this.lrSystemScoresTable = lrSystemScoresTable;
	this.useBeerMetric = useBeerMetric;
	this.outputLengthStatistics = outputLengthStatistics;
	Assert.assertEquals(multiEvalResultsTableTest.numSystems(),
		lrSystemScoresTable.getNumTestSystems());
    }

    public static MultiEvalAndLRScoreTableDevAndTest createMultiEvalAndLRScoreTableDevAndTest(
	    String resultFilePathDev, String resultFilePathTest, String devScoringConfigFilePath,
	    String testScoringConfigFilePath, boolean useBeerMetric,boolean outputLengthStatistics) {
	MultiEvalResultsTable<?> multiEvalResultsTableDev = MultiEvalResultsTable
		.createCompleteMultiEvalResultsTable(resultFilePathDev,useBeerMetric,outputLengthStatistics);
	MultiEvalResultsTable<?> multiEvalResultsTableTest = MultiEvalResultsTable
		.createCompleteMultiEvalResultsTable(resultFilePathTest,useBeerMetric,outputLengthStatistics);

	LRScoreTableCreater lrScoreTableCreater = LRScoreTableCreater.createLrScoreTableCreater(
		devScoringConfigFilePath, testScoringConfigFilePath);
	SystemScoresTable lrSystemScoresTable = lrScoreTableCreater.createSystemLRScoresTable();

	return new MultiEvalAndLRScoreTableDevAndTest(multiEvalResultsTableDev,
		multiEvalResultsTableTest, lrSystemScoresTable,useBeerMetric,outputLengthStatistics);
    }

    protected String improvementNewCommandStrings(){
	String result =  NL + "\\newcommand{\\SI}{$^{\\blacktriangle}$}" + NL
	+ "\\newcommand{\\SISI}{$^{\\blacktriangle\\blacktriangle}$}" + NL
	+ "\\newcommand{\\SW}{{$^{\\blacktriangledown}$}}" + NL
	+ "\\newcommand{\\SWSW}{{$^{\\blacktriangledown\\blacktriangledown}$}}" + NL; 
	return result;
    }
    
    protected String documentHeader() {
	return "\\documentclass[a4paper,10pt]{article}"
		+ NL
		+ "\\usepackage[utf8]{inputenc}"
		+ NL
		+ "\\usepackage{multirow}"
		+ NL
		+ "\\usepackage{adjustbox}"
		+ NL
		+ "\\usepackage[margin=1in]{geometry}"
		+ NL
		+ "\\usepackage{amssymb}"
		+ NL
		+		 
		"% Symbols for indicating significal improvement / worsening of the results with respect to the baseline results"
		+ improvementNewCommandStrings() +
		"% See : http://www.latex-community.org/forum/viewtopic.php?f=45&t=20438 " + NL
		+ "\\newcommand{\\minitab}[2][l]{\\begin{tabular}{#1}#2\\end{tabular}}" + NL
		 + "\\begin{document}" + NL;

    }

    protected String documentTailer() {
	return "}" + NL + "\\end{table*}" + NL + NL +
		 "\\end{document}";
    }

    
    private int noMetrics(){
	int result = 4;
	
	if(useBeerMetric){
	    result++;
	}
	if(outputLengthStatistics){
	    result++;
	}
    	return result;
    }
    
    private int noTableColumns(){
	return noMetrics() * 2 + 1;	    	
    }
    
    private String systemColumnHeaderPart(){
	String result = "BLEU & METEOR & ";
	if(useBeerMetric){
	    result += "BEER & ";  
	}
	result += "TER & KRS";
	if(outputLengthStatistics){
	    result += " & LENGTH";
	}
	return result;
    }
    
    private String systemColumnHeaderLine(){	
	    return  "&    " + systemColumnHeaderPart() + " & " + systemColumnHeaderPart() + "   \\\\  \\hline" + NL;		    
    }
    
    private String tabularColumnString(){
	String result = "|";
	for(int i = 0; i < noTableColumns(); i++){
	    result += "l|";
	}
	return result;
    }
    
    @Override
    protected String tableHeader() {
	String result = "\\begin{table*}"
		+ NL
		+ "\\resizebox{1.0\\textwidth}{!}{%"
		+ NL
		+ "\\begin{tabular}{" + tabularColumnString() + "}" + NL 
		+ "\\hline"
		+ NL
		+ "{\\multirow{3}{*}{\\minitab[c]{ System  Name }}}  &  \\multicolumn{" + (noTableColumns() -1) + "}{c|}{SourceLanguage-TargetLanguage}  \\\\"
		+ NL + "\\cline{2-" + noTableColumns() + "}" + NL
		+ "&   \\multicolumn{" + noMetrics() + "}{c|}{DEV}  &  \\multicolumn{" + noMetrics() + "}{c|}{TEST}\\\\" + NL
		+ "\\cline{2-" + noTableColumns() + "}" + NL
		+ systemColumnHeaderLine();
	return result;
    }

    
    private String getSystemScoreString(MultiEvalResultsTable<?> multiEvalResultsTable,String lrScoreString, 
	    int systemIndex,boolean showStandardDeviation){
	String result = "";		
	result +=  multiEvalResultsTable.getSystemScoresString(systemIndex,showStandardDeviation)
		+ " & "+ lrScoreString;
	if(outputLengthStatistics){
	    result+= " & " + multiEvalResultsTable.getSystemScoresStringLengthOnly(systemIndex,
		showStandardDeviation);
	}
	return result;
    }
    
    @Override
    public void writeScoresToFile(String outputFilePath, boolean showStandardDeviation) {
	BufferedWriter outputWriter = null;
	try {
	    outputWriter = new BufferedWriter(new FileWriter(outputFilePath));
	    outputWriter.write(documentHeader());
	    outputWriter.write(tableHeader());
	    for (int systemIndex = 0; systemIndex < multiEvalResultsTableDev.numSystems(); systemIndex++) {
		
		 // Replace lower dash with gives problems in Latex
		String systemName = getLrSystemScoresTable().getSystemName(systemIndex).replace("_", "-");
		
		String lrScoreStringDev = getLrSystemScoresTable().getMeanAndStandardDeviationStringDev(systemIndex,
				true, showStandardDeviation);
		String lrScoreStringTest = getLrSystemScoresTable().getMeanAndStandardDeviationStringTest(systemIndex,
			true, showStandardDeviation);
		
		String outputString = systemName
			+ " & "
			+ getSystemScoreString(multiEvalResultsTableDev, lrScoreStringDev, systemIndex, showStandardDeviation)  		
			+ " & " +
			getSystemScoreString(multiEvalResultsTableTest, lrScoreStringTest, systemIndex, showStandardDeviation);						
			outputString	+= " \\\\" + NL;
		
		outputWriter.write(outputString);
	    }
	    outputWriter.write(tableFooter());
	    outputWriter.write(documentTailer());
	} catch (IOException e) {
	    e.printStackTrace();
	} finally {
	    FileUtil.closeCloseableIfNotNull(outputWriter);
	}
    }

    private static String getLRScoreDevScoresConfigFilePath(ConfigFile theConfig) {
	return theConfig
		.getValueWithPresenceCheck(LR_SCORE_DEV_SCORES_COMUTATION_CONFIG_FILE_PATH_PROPERTY);
    }

    private static String getLRScoreTestScoresConfigFilePath(ConfigFile theConfig) {
	return theConfig
		.getValueWithPresenceCheck(LR_SCORE_TEST_SCORES_COMPUTATION_CONFIG_FILE_PATH_PROPERTY);
    }

    
    public static MultiEvalAndLRScoreTableDevAndTest createResultsTableForConfig(String configFilePath,
	    boolean showStandardDeviation) {
	ConfigFile theConfig;
	try {
	    theConfig = new ConfigFile(configFilePath);
	    
	    boolean useBeerMetric = Boolean.parseBoolean(theConfig.getValueWithPresenceCheck(USE_BEER_METRIC_PROPERTY_STRING));
	    boolean outputLengthStatistics = Boolean.parseBoolean(theConfig.getValueWithPresenceCheck(OUTPUT_LENGTH_STATISTICS_PROPERTY_STRING));
	    
	    MultiEvalAndLRScoreTableDevAndTest multiEvalResultsTableDevAndTest = MultiEvalAndLRScoreTableDevAndTest
		    .createMultiEvalAndLRScoreTableDevAndTest(getDevResultFilePath(theConfig),
			    getTestResultFilePath(theConfig),
			    getLRScoreDevScoresConfigFilePath(theConfig),
			    getLRScoreTestScoresConfigFilePath(theConfig),useBeerMetric,outputLengthStatistics
			    );
	    return multiEvalResultsTableDevAndTest;
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }
    
    public static void createAndWriteResultsTableForConfig(String configFilePath,
	    boolean showStandardDeviation) {
		ConfigFile theConfig;
		try {
		    theConfig = new ConfigFile(configFilePath);
		    MultiEvalResultsTableDevAndTest multiEvalResultsTableDevAndTest = MultiEvalAndLRScoreTableDevAndTest
			    .createResultsTableForConfig(configFilePath, showStandardDeviation);
		    multiEvalResultsTableDevAndTest.writeScoresToFile(getOutputResultFilePath(theConfig),
			    showStandardDeviation);
		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		}	   
    }

    public static void main(String[] args) {

	if (args.length != 2) {
	    System.out
		    .println("Usage : createResultsTable CONFIG_FILE_PATH SHOWSTANDARD_DEVIATION_FLAG");
	    System.exit(1);
	}
	createAndWriteResultsTableForConfig(args[0], Boolean.parseBoolean(args[1]));

    }

    public SystemScoresTable getLrSystemScoresTable() {
	return lrSystemScoresTable;
    }

}
