package mt_pipeline.evaluation;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


import util.FileUtil;

import mt_pipeline.mtPipelineTest.TestIdentifierPair;

public class SystemSentenceScores {

	private final TestIdentifierPair testIdentifierPair;
	private final List<SentenceNumberScoresTriple> sentenceScores;

	private SystemSentenceScores(TestIdentifierPair testIdentifierPair, List<SentenceNumberScoresTriple> sentenceScores) {
		this.testIdentifierPair = testIdentifierPair;
		this.sentenceScores = sentenceScores;
	}

	public static SystemSentenceScores createSystemSentenceScores(TestIdentifierPair testIdentifierPair, List<SentenceNumberScoresTriple> sentenceScores) {
		return new SystemSentenceScores(testIdentifierPair, sentenceScores);
	}

	public static SystemSentenceScores createSystemSentenceScores(TestIdentifierPair testIdentifierPair, String scoresFileName) {
		return new SystemSentenceScores(testIdentifierPair, getSentenceScoresFromFile(scoresFileName));
	}


	public static List<SentenceNumberScoresTriple> getSentenceScoresFromFile(String scoresFileName) {
		List<SentenceNumberScoresTriple> result = new ArrayList<SentenceNumberScoresTriple>();
		List<String> lines = FileUtil.getSentences(new File(scoresFileName), true);
		for (int lineNum = 1; lineNum < lines.size(); lineNum++) {
			result.add(SentenceNumberScoresTriple.createSentenceNumberScoresTripleFromString(lines.get(lineNum)));
		}
		return result;
	}

	private List<SentenceNumberScoresTriple> getSentenceScores() {
		return this.sentenceScores;
	}

	public SentenceNumberScoresTriple getSentenceScore(int index) {
		return this.sentenceScores.get(index);
	}

	public TestIdentifierPair getTestIdentifierPair() {
		return this.testIdentifierPair;
	}

	public int getNumScores() {
		return this.getSentenceScores().size();
	}

}
