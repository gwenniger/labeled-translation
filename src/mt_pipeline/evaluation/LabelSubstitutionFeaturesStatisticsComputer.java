package mt_pipeline.evaluation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import util.MeanStdComputation;
import util.Pair;

public class LabelSubstitutionFeaturesStatisticsComputer {

    private static String LABEL_SUBSTITUTION_FEATURE_STRING = "LabelSubstitution";
    private static String COARSE_MATCHING_FEATURE_STRING = LABEL_SUBSTITUTION_FEATURE_STRING
	    + "_MATCH";
    private static String COARSE_MISMATCHING_FEATURE_STRING = LABEL_SUBSTITUTION_FEATURE_STRING
	    + "_NOMATCH";
    private static String SUBSTITUTION_SEPARATOR = "_substitutes_";

    private final LineIterator lineIterator;
    private final List<Double> rawMatchingLabelSubstitutionFeatureWeights;
    private final List<Double> rawMisMatchingLabelSubstitutionFeatureWeights;
    private double coarseMatchingFeatureWeight;
    private double coarseMisMatchingFeatureWeight;

    private LabelSubstitutionFeaturesStatisticsComputer(LineIterator lineIterator,
	    List<Double> rawMatchingLabelSubstitutionFeatureWeights,
	    List<Double> rawMisMatchingLabelSubstitutionFeatureWeights) {
	this.lineIterator = lineIterator;
	this.rawMatchingLabelSubstitutionFeatureWeights = rawMatchingLabelSubstitutionFeatureWeights;
	this.rawMisMatchingLabelSubstitutionFeatureWeights = rawMisMatchingLabelSubstitutionFeatureWeights;
    }

    public static LabelSubstitutionFeaturesStatisticsComputer createLabelSubstitutionFeaturesStatisticsComputer(
	    String filePath) {
	LineIterator lineIterator;
	try {
	    lineIterator = FileUtils.lineIterator(new File(filePath), "UTF-8");
	    return new LabelSubstitutionFeaturesStatisticsComputer(lineIterator,
		    new ArrayList<Double>(), new ArrayList<Double>());
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private boolean lineContainsCoarseMatchingFeatureWeight(String line) {
	return line.startsWith(COARSE_MATCHING_FEATURE_STRING);
    }

    private boolean lineContainsCoarseMisMatchingFeatureWeight(String line) {
	return line.startsWith(COARSE_MISMATCHING_FEATURE_STRING);
    }

    private boolean isSpecificLabelSubstitionFeatureString(String line) {
	return line.contains(LABEL_SUBSTITUTION_FEATURE_STRING)
		&& line.contains(SUBSTITUTION_SEPARATOR);
    }

    private Pair<String> getLabelPair(String labelSubstitutionFeatureLine) {
	String firstPart = labelSubstitutionFeatureLine.split(" ")[0];
	String[] labelParts = firstPart.split(SUBSTITUTION_SEPARATOR);

	String firstLabelParts = labelParts[0];
	String firstLabel = firstLabelParts.split("_")[1];
	String secondLabel = labelParts[1];
	return new Pair<String>(firstLabel, secondLabel);
    }

    private boolean labelsMatch(Pair<String> labelPair) {
	return labelPair.getFirst().equals(labelPair.getSecond());
    }

    private double getFeatureWeigth(String featureLine) {
	String valueString = featureLine.split(" ")[1];
	return Double.parseDouble(valueString);
    }

    private void processCoarseMatchingFeatureWeightString(String line) {
	this.coarseMatchingFeatureWeight = getFeatureWeigth(line);
    }

    private void processCoarseMisMatchingFeatureWeightString(String line) {
	this.coarseMisMatchingFeatureWeight = getFeatureWeigth(line);
    }

    private void processSpecificMatchingLabelSubstitutionFeatureWeightString(String featureLine) {
	Double featureWeigth = getFeatureWeigth(featureLine);
	this.rawMatchingLabelSubstitutionFeatureWeights.add(featureWeigth);
    }

    private void processSpecificMisMatchingLabelSubstitutionFeatureWeightString(String featureLine) {
	Double featureWeigth = getFeatureWeigth(featureLine);
	this.rawMisMatchingLabelSubstitutionFeatureWeights.add(featureWeigth);
    }

    private void processSpecificLabelSubstitutionFeatureWeightString(String featureLine) {
	Pair<String> labelPair = getLabelPair(featureLine);
	if (labelsMatch(labelPair)) {
	    processSpecificMatchingLabelSubstitutionFeatureWeightString(featureLine);
	} else {
	    processSpecificMisMatchingLabelSubstitutionFeatureWeightString(featureLine);
	}
    }

    private static List<Double> makeCopyWithAddedScalar(List<Double> arrayToCopy, double scalarToAdd) {
	List<Double> result = new ArrayList<Double>();
	for (Double raqMatchingFeatureWeight : arrayToCopy) {
	    result.add(raqMatchingFeatureWeight + scalarToAdd);
	}
	return result;
    }

    private List<Double> getCoarseFeatureWeightCompensatedSpecificMatchingFeatureWeights() {
	return makeCopyWithAddedScalar(rawMatchingLabelSubstitutionFeatureWeights,
		coarseMatchingFeatureWeight);
    }

    private List<Double> getCoarseFeatureWeightCompensatedSpecificMisMatchingFeatureWeights() {
	return makeCopyWithAddedScalar(rawMisMatchingLabelSubstitutionFeatureWeights,
		coarseMisMatchingFeatureWeight);
    }


    private static String getMeanStdMinMaxString(List<Double> values) {
	DoubleListStatistics doublesListStatistics = DoubleListStatistics.createDoublesListStatistics(values);
	String result = "";
	result += "mean: " + doublesListStatistics.getMean() + " std: "
		+ doublesListStatistics.getStd() + " min: " + doublesListStatistics.getMin() + " max: " + doublesListStatistics.getMax();
	return result;
    }

    private String getSpecificMatchingFeaturesMeanAndStdString() {
	return getMeanStdMinMaxString(getCoarseFeatureWeightCompensatedSpecificMatchingFeatureWeights());
    }

    private String getSpecificMisMatchingFeaturesMeanAndStdString() {
	return getMeanStdMinMaxString(getCoarseFeatureWeightCompensatedSpecificMisMatchingFeatureWeights());
    }

    private void processLines() {
	while (lineIterator.hasNext()) {
	    String line = lineIterator.nextLine();
	    if (!line.equals("")) {
		if (lineContainsCoarseMatchingFeatureWeight(line)) {
		    processCoarseMatchingFeatureWeightString(line);
		} else if (lineContainsCoarseMisMatchingFeatureWeight(line)) {
		    processCoarseMisMatchingFeatureWeightString(line);
		} else if (isSpecificLabelSubstitionFeatureString(line)) {
		    processSpecificLabelSubstitutionFeatureWeightString(line);
		}
	    }
	}
    }

    public void printMeanAndStandardDeviationFeatures() {
	processLines();
	System.out.println("Matching features: " + getSpecificMatchingFeaturesMeanAndStdString());
	System.out.println("Mismatching features: "
		+ getSpecificMisMatchingFeaturesMeanAndStdString());
    }



    public static void main(String[] args) {
	if (args.length != 1) {
	    System.out.println("Usage : labelSubstitutionFeaturesComputer configFilePath");
	    System.exit(1);
	}
	LabelSubstitutionFeaturesStatisticsComputer labelSubstitutionFeaturesStatisticsComputer = createLabelSubstitutionFeaturesStatisticsComputer(args[0]);
	labelSubstitutionFeaturesStatisticsComputer.printMeanAndStandardDeviationFeatures();
    }

}
