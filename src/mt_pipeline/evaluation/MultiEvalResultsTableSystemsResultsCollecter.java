package mt_pipeline.evaluation;

import java.io.File;
import java.io.IOException;

import mt_pipeline.evaluation.MultEvalSystemScores.SystemScoresBasic;
import mt_pipeline.evaluation.MultEvalSystemScores.SystemScoresWithBeer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import junit.framework.Assert;

public class MultiEvalResultsTableSystemsResultsCollecter<T extends MultEvalSystemScores> {

    private static final String RESULT_TAG = "RESULT";
    private static final String RESULTS_BLOCK_END_SIGNAL_LINE_PREFIX = "Top unmatched hypothesis words";

    private final LineIterator resultsFileLineIterator;
    private final MultiEvalResultsTable<T> multiEvalResultsTable;
    

    private MultiEvalResultsTableSystemsResultsCollecter(
	    MultiEvalResultsTable<T> multiEvalResultsTable, LineIterator resultsFileLineIterator) {
	this.multiEvalResultsTable = multiEvalResultsTable;
	this.resultsFileLineIterator = resultsFileLineIterator;
    }

    public static MultiEvalResultsTableSystemsResultsCollecter<?> createMultiEvalResultsTableSystemsResultsCollecter(
	    String resultFilePath,boolean useBeerMetric,boolean outputLengthStatistics) {
	try {
	    LineIterator resultsFileLineIterator = FileUtils.lineIterator(new File(resultFilePath),
		    "UTF-8");
	    if(useBeerMetric){

		    MultiEvalResultsTable<SystemScoresWithBeer> multiEvalResultsTable = MultiEvalResultsTable
			    .createMultiEvalResultsTableWithBeer(outputLengthStatistics);
		    return new MultiEvalResultsTableSystemsResultsCollecter<SystemScoresWithBeer>(multiEvalResultsTable,
			    resultsFileLineIterator);
	    }else{
		    MultiEvalResultsTable<SystemScoresBasic> multiEvalResultsTable = MultiEvalResultsTable
			    .createMultiEvalResultsTableBasic(outputLengthStatistics);
		    return new MultiEvalResultsTableSystemsResultsCollecter<SystemScoresBasic>(multiEvalResultsTable,
			    resultsFileLineIterator);
	    }
	    
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    public MultiEvalResultsTable<T> getResultsTable() {
	return this.multiEvalResultsTable;
    }

    private boolean isResultLine(String line) {
	return line.startsWith(RESULT_TAG);
    }

    private boolean isEntryOfInterest(ResultEntry resultEntry) {

	return (resultEntry.isBleuMeteorBeerTerOrLengthEntry() && resultEntry
		.isAverageOrStandardDeviationScoreType());
    }

    public void updateSystemScoresWithResultEntry(ResultEntry resultEntry) {
	if (isEntryOfInterest(resultEntry)) {
	    MultEvalSystemScores correspondingSystemScoresEntry = this.multiEvalResultsTable
		    .getOrAddAndGetSystemScoresForSystemName(resultEntry.getSystemName());
	    correspondingSystemScoresEntry.updateSystemScoresForResultEntry(resultEntry);
	}
    }

    public void updateSystemScoresWithResultEntryLine(String resultEntryLine) {
	updateSystemScoresWithResultEntry(ResultEntry
		.createResultEntryFromResultLine(resultEntryLine));
    }

    private boolean isEndSignalLine(String line) {
	return line.startsWith(RESULTS_BLOCK_END_SIGNAL_LINE_PREFIX);
    }

    public void extractAllSystemScoresFromInput() {
	try {
	    while (resultsFileLineIterator.hasNext()) {
		String line = resultsFileLineIterator.nextLine();
		System.out.println("line: " + line);
		if (isEndSignalLine(line)) {
		    Assert.assertTrue(this.multiEvalResultsTable.allScoresAreSet());
		    return;
		}
		if (isResultLine(line)) {
		    updateSystemScoresWithResultEntryLine(line);
		}
	    }
	    Assert.assertTrue(this.multiEvalResultsTable.allScoresAreSet());
	} finally {
	    resultsFileLineIterator.close();
	}
    }

    public static class ResultEntry {
	private final String BLEU_NAME = "BLEU";
	private final String METEOR_NAME = "METEOR";
	private final String BEER_NAME = "BEER";
	private final String TER_NAME = "TER";
	private final String LENGTH_NAME = "Length";
	private final String AVG_NAME = "AVG";
	private final String STDDEV_NAME = "STDDEV";

	private final String systemName;
	private final String metricName;
	private final String scoreTypeName;
	private final double score;

	private ResultEntry(String systemName, String metricName, String scoreTypeName, double score) {
	    this.systemName = systemName;
	    this.metricName = metricName;
	    this.scoreTypeName = scoreTypeName;
	    this.score = score;
	}

	public static ResultEntry createResultEntryFromResultLine(String resultLine) {
	    String[] resultParts = resultLine.split(":");
	    Assert.assertTrue(resultParts[0].equals(RESULT_TAG));
	    Assert.assertEquals(5, resultParts.length);

	    String systemName = resultParts[1].trim();
	    String metricName = resultParts[2].trim();
	    String scoreTypeName = resultParts[3].trim();
	    double score = Double.parseDouble(resultParts[4].trim());

	    return new ResultEntry(systemName, metricName, scoreTypeName, score);
	}

	public String getSystemName() {
	    return this.systemName;
	}

	public String getMetricName() {
	    return this.metricName;
	}

	public String getScoreTypeName() {
	    return this.scoreTypeName;
	}

	public double getScore() {
	    return this.score;
	}

	public boolean isBleuEntry() {
	    return metricName.equals(BLEU_NAME);
	}

	public boolean isMeteorEntry() {
	    return metricName.equals(METEOR_NAME);
	}
	
	public boolean isBeerEntry() {
	    return metricName.equals(BEER_NAME);
	}

	public boolean isTerEntry() {
	    return metricName.equals(TER_NAME);
	}

	public boolean isLengthEntry() {
	    return metricName.equals(LENGTH_NAME);
	}

	public boolean isAverageScorpeType() {
	    return this.scoreTypeName.equals(AVG_NAME);
	}

	public boolean isStandardDeviationScorpeType() {
	    return this.scoreTypeName.equals(STDDEV_NAME);
	}

	public boolean isBleuMeteorBeerTerOrLengthEntry() {
	    return (isBleuEntry() || isMeteorEntry() || isBeerEntry() || isTerEntry() || isLengthEntry());
	}

	public boolean isAverageOrStandardDeviationScoreType() {
	    return isAverageScorpeType() || isStandardDeviationScorpeType();
	}

    }

    public int numSystems() {
	return this.multiEvalResultsTable.numSystems();

    }

    public void printScores() {
	this.multiEvalResultsTable.printScores(true);
    }

}
