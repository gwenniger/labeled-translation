package mt_pipeline.evaluation;

import grammarExtraction.chiangGrammarExtraction.ChiangRuleCreater;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import junit.framework.Assert;
import util.MeanStdComputation;
import util.ObjectDoubleValuePair;
import util.Pair;

public class LabelSelfSubstitutionPreferenceRankComputer {

    private static final String NL = "\n";

    private static String METRIC_EXPLANATION_STRING = "Mean Label Self Substitution Preference Rank (\\emph{Mean Label SSP-Rank})"
	    + NL
	    + "is a metric that asseses how often labels prefer to substitute to themselves over all other labels."
	    + NL
	    + "Label Self Substitution Preference Rank (\\emph{Label SSP-Rank}) is computed for a substituted-to label $Y$ "
	    + "by sorting the (label substitution feature) weight $weight(X\\_subst\\_Y)$ for all subsitutions $X$ that substitute $Y$, "
	    + "and determining the position of the self substitution $Y\\_subst\\_Y$ within the list sorted by weight. "
	    + "The rank is 1 iff no other label has higher preference (= label substitution feature weight) of substituting to $Y$ than $Y$ itself. \\\\"
	    + NL
	    + NL
	    + "The Mean Label Self Substitution Preference Rank (Mean Label SSP-Rank) is easily computed by averaging"
	    + " the Label SSP-Rank over all possible  substituted-to labels $Y$. In the computation of "
	    + "the Label SSP-Rank and  Mean Label SSP-Rank, any substitution that involves the glue rule label ($X$) "
	    + "is ignored from the calculation. \\\\"
	    + NL
	    + NL
	    + "Avoiding the trap of considering individual weights in isolation, while at the same time "
	    + "being more precise than just computing the difference between mean weights for Matching and"
	    + " Mismatching substitutions, this metric offers an intuitive, adequately sensitive, and "
	    + "robust way to capture to what extend preference for matching substitutions is general over all used labels.";

    private final LabelSubstitutionFeatureWeightTableConstructer labelSubstitutionFeatureWeightTableConstructer;

    private LabelSelfSubstitutionPreferenceRankComputer(
	    LabelSubstitutionFeatureWeightTableConstructer labelSubstitutionFeatureWeightTableConstructer) {
	this.labelSubstitutionFeatureWeightTableConstructer = labelSubstitutionFeatureWeightTableConstructer;
    }

    public static LabelSelfSubstitutionPreferenceRankComputer createLabelSelfSubstitutionPreferenceRankComputer(
	    LabelSubstitutionFeatureWeightTableConstructer labelSubstitutionFeatureWeightTableConstructer) {
	return new LabelSelfSubstitutionPreferenceRankComputer(
		labelSubstitutionFeatureWeightTableConstructer);
    }

    private int getMatchingSubstitutionIndex(
	    List<ObjectDoubleValuePair<Pair<String>>> substitutionPairWeightObjectList,
	    String substitutionToLabel) {

	Pair<String> matchingSubstitution = new Pair<String>(substitutionToLabel,
		substitutionToLabel);

	int index = 0;
	for (ObjectDoubleValuePair<Pair<String>> objectDoubleValuePair : substitutionPairWeightObjectList) {
	    if (objectDoubleValuePair.getObject().equals(matchingSubstitution)) {
		return index;
	    }
	    index++;
	}
	return -1;
    }

    private int computeSelfSubstitutionPreferenceRank(String substitutionToLabel, int runIndex) {

	Assert.assertFalse(substitutionToLabel.equals(ChiangRuleCreater.ChiangLabel));
	// System.out.println(">>> substitutionToLabel: " +
	// substitutionToLabel);

	List<ObjectDoubleValuePair<Pair<String>>> substitutionPairWeightObjectList = new ArrayList<ObjectDoubleValuePair<Pair<String>>>();

	for (String substitutionFromLabel : labelSubstitutionFeatureWeightTableConstructer
		.getSortedFromLabelsList()) {

	    Assert.assertFalse(substitutionFromLabel.equals(ChiangRuleCreater.ChiangLabel));
	    // System.out.println(">>> substitutionFromLabel: " +
	    // substitutionFromLabel);

	    if (labelSubstitutionFeatureWeightTableConstructer
		    .hasLabelSubstitutionFeatureValueForIteration(substitutionFromLabel,
			    substitutionToLabel, runIndex)) {
		double weight = labelSubstitutionFeatureWeightTableConstructer
			.getCompensatedSubstitutionFeatureValue(substitutionFromLabel,
				substitutionToLabel, runIndex);
		Pair<String> substitutionPair = new Pair<String>(substitutionFromLabel,
			substitutionToLabel);
		substitutionPairWeightObjectList.add(new ObjectDoubleValuePair<Pair<String>>(
			substitutionPair, weight));
	    }
	}
	substitutionPairWeightObjectList.sort(Collections.reverseOrder());
	int result = getMatchingSubstitutionIndex(substitutionPairWeightObjectList,
		substitutionToLabel) + 1;

	if (result > 1) {
	    System.out.println("}}}} runIndex: " + runIndex + " Rank > 1 for substitutionToLabel "
		    + substitutionToLabel);
	    System.out.println("Rank 1 substitution: "
		    + substitutionPairWeightObjectList.get(0).getObject());
	}
	return result;
    }

    public double computeAverageSubstitutionPreferenceRank(String substitutionToLabel) {
	List<Double> rankValues = new ArrayList<Double>();

	for (int runIndex = 1; runIndex <= labelSubstitutionFeatureWeightTableConstructer
		.getNumRuns(); runIndex++) {
	    if (labelSubstitutionFeatureWeightTableConstructer
		    .hasLabelSubstitutionFeatureValueForIteration(substitutionToLabel,
			    substitutionToLabel, runIndex)) {
		rankValues.add(new Double(computeSelfSubstitutionPreferenceRank(
			substitutionToLabel, runIndex)));
	    }
	}
	double result = MeanStdComputation.computeDoublesMean(rankValues);
	System.out.println("Average preference rank " + substitutionToLabel + ": " + result);
	
	return result;
    }

    public Pair<Double> computeGlobalAvarageSubstitutionPreferenceRankAndStd() {
	List<Double> averageRankValues = new ArrayList<Double>();

	for (String substitutionToLabel : labelSubstitutionFeatureWeightTableConstructer
		.getSortedToLabelsList()) {
	    if (labelSubstitutionFeatureWeightTableConstructer.hasLabelSubstitutionFeatureValue(
		    substitutionToLabel, substitutionToLabel)) {
		averageRankValues
			.add(computeAverageSubstitutionPreferenceRank(substitutionToLabel));
	    }
	}
	return new Pair<Double>(MeanStdComputation.computeDoublesMean(averageRankValues),
		MeanStdComputation.computeDoublesStd(averageRankValues));
    }

    public static String getMetricExplanationString() {
	return METRIC_EXPLANATION_STRING;
    }

}
