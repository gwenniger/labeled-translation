package mt_pipeline.evaluation;

import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import util.MeanStdComputation;
import mt_pipeline.evaluation.BasicLabelSubstitutionFeature.BasicLabelSubstitutionFeatureWithValue;
import junit.framework.Assert;

public class SentencesNBestListLabelSubstitutionStatisticsComputer {
    private final boolean useOnlyFirstCandidateInNBestList;
    private final boolean isSoftMatchingSystem;
    private final NBestListsLabelSubstitutionstatistics nBestListsLabelSubstitutionstatistics;

    private SentencesNBestListLabelSubstitutionStatisticsComputer(
	    boolean useOnlyFirstCandidateInNBestList, boolean isSoftMatchingSystem,
	    NBestListsLabelSubstitutionstatistics nBestListsLabelSubstitutionstatistics) {
	this.useOnlyFirstCandidateInNBestList = useOnlyFirstCandidateInNBestList;
	this.isSoftMatchingSystem = isSoftMatchingSystem;
	this.nBestListsLabelSubstitutionstatistics = nBestListsLabelSubstitutionstatistics;
    }

    public static SentencesNBestListLabelSubstitutionStatisticsComputer createSentencesNBestListLabelSubstitutionStatisticsComputer(
	    boolean useOnlyFirstCandidateInNBestList, boolean isSoftMatchingSystem) {

	return new SentencesNBestListLabelSubstitutionStatisticsComputer(
		useOnlyFirstCandidateInNBestList, isSoftMatchingSystem,
		NBestListsLabelSubstitutionstatistics.createNBestListsLabelSubstitutionstatistics());
    }

    private String getFeaturesPartString(String line) {
	return JoshuaStyle.spitOnRuleSeparator(line)[2];
    }

    private List<String> getFeatureStrings(String candidateString) {
	String featurePartString = getFeaturesPartString(candidateString);
	List<String> result = Arrays.asList(featurePartString.split(" "));
	return result;
    }

    public void addStatisticsFromTarGzFile(String nBestListTarGzFilePath) {
	SentencesNBestCandidatesListIterator sentencesNBestCandidatesListIterator = SentencesNBestCandidatesListIterator
		.createSentencesNBestCandidatesListIteratorTarGzFile(nBestListTarGzFilePath);
	processNBestLists(sentencesNBestCandidatesListIterator);
    }

    private void processNBestLists(
	    SentencesNBestCandidatesListIterator sentencesNBestCandidatesListIterator) {
	if (nBestListsLabelSubstitutionstatistics.getNumSentences() <= 0) {
	    processRunNBestLists(sentencesNBestCandidatesListIterator,
		    new NBestListsLabelSubstitutionstatisticsUpdaterFirstRun(
			    nBestListsLabelSubstitutionstatistics));
	} else {
	    processRunNBestLists(sentencesNBestCandidatesListIterator,
		    new NBestListsLabelSubstitutionstatisticsUpdaterAdditionalRun(
			    nBestListsLabelSubstitutionstatistics));
	}

    }

    private boolean isHieroBaselineSystem() {
	return !isSoftMatchingSystem;
    }

    private void updateSentenceLabelSubstitutionStatistics(
	    SentenceLabelSubstitutionstatistics sentenceLabelSubstitutionstatistics,
	    String candidateString) {
	List<String> featureStrings = getFeatureStrings(candidateString);

	BasicRuleTypeCounter basicRuleTypeCounter = BasicRuleTypeCounter
		.createBasicRuleTypeCounter();

	for (String featureString : featureStrings) {
	    if (isSoftMatchingSystem
		    && BasicLabelSubstitutionFeature
			    .consitutesLabelSubstitutionFeatureString(featureString)) {
		BasicLabelSubstitutionFeatureWithValue basicLabelSubstitutionFeatureWithValue = BasicLabelSubstitutionFeatureWithValue
			.createBasicLabelSubstitutionFeatureWithWeight(featureString);
		sentenceLabelSubstitutionstatistics
			.addBasicLabelSubstitutionFeatureWithValue(basicLabelSubstitutionFeatureWithValue);
	    } else if (isHieroBaselineSystem()) {
		if (BasicRuleTypeCounter.isFeatureOfInterest(featureString)) {
		    basicRuleTypeCounter.updateRuleTypeCounts(featureString);
		}
	    }
	}
	if (isHieroBaselineSystem()) {
	    sentenceLabelSubstitutionstatistics
		    .addCountsFromBasicRuleTypeCounter(basicRuleTypeCounter);
	}
    }

    /**
     * Method that processes the nBest lists from first runs or additional runs
     * beyond the first, using Strategy (the argument
     * nBestListsLabelSubstitutionstatisticsUpdater determines the exact
     * updating behavior)
     * 
     * @param sentencesNBestCandidatesListIterator
     * @param nBestListsLabelSubstitutionstatisticsUpdater
     */
    private void processRunNBestLists(
	    SentencesNBestCandidatesListIterator sentencesNBestCandidatesListIterator,
	    NBestListsLabelSubstitutionstatisticsUpdater nBestListsLabelSubstitutionstatisticsUpdater) {
	int sentenceNumber = 0;
	while (sentencesNBestCandidatesListIterator.hasNext()) {
	    List<String> nBestList = sentencesNBestCandidatesListIterator.next();
	    SentenceLabelSubstitutionstatistics sentenceLabelSubstitutionstatistics = nBestListsLabelSubstitutionstatisticsUpdater
		    .getOrCreateSentenceLabelSubstitutionstatistics(sentenceNumber);
	    if (useOnlyFirstCandidateInNBestList) {
		String candidateString = nBestList.get(0);
		updateSentenceLabelSubstitutionStatistics(sentenceLabelSubstitutionstatistics,
			candidateString);
	    } else {
		for (String candidateString : nBestList) {
		    updateSentenceLabelSubstitutionStatistics(sentenceLabelSubstitutionstatistics,
			    candidateString);
		}
	    }
	    nBestListsLabelSubstitutionstatisticsUpdater
		    .updateWithSentenceLabelSubstitutionstatistics(sentenceLabelSubstitutionstatistics);
	    sentenceNumber++;
	}
    }

    public void reportStatistics(String nBestListTarGzFilePath) {
	nBestListsLabelSubstitutionstatistics.printStatistics();
	nBestListsLabelSubstitutionstatistics.printSummarizedStatistics();
    }

    public NBestListsLabelSubstitutionstatistics getNBestListsLabelSubstitutionstatistics() {
	return this.nBestListsLabelSubstitutionstatistics;
    }

    protected static abstract class NBestListsLabelSubstitutionstatisticsUpdater {
	protected final NBestListsLabelSubstitutionstatistics nBestListsLabelSubstitutionstatistics;

	protected NBestListsLabelSubstitutionstatisticsUpdater(
		NBestListsLabelSubstitutionstatistics nBestListsLabelSubstitutionstatistics) {
	    this.nBestListsLabelSubstitutionstatistics = nBestListsLabelSubstitutionstatistics;
	}

	public abstract SentenceLabelSubstitutionstatistics getOrCreateSentenceLabelSubstitutionstatistics(
		int sentenceNumber);

	public abstract void updateWithSentenceLabelSubstitutionstatistics(
		SentenceLabelSubstitutionstatistics sentenceLabelSubstitutionstatistics);
    }

    protected static class NBestListsLabelSubstitutionstatisticsUpdaterFirstRun extends
	    NBestListsLabelSubstitutionstatisticsUpdater {

	protected NBestListsLabelSubstitutionstatisticsUpdaterFirstRun(
		NBestListsLabelSubstitutionstatistics nBestListsLabelSubstitutionstatistics) {
	    super(nBestListsLabelSubstitutionstatistics);
	}

	@Override
	public SentenceLabelSubstitutionstatistics getOrCreateSentenceLabelSubstitutionstatistics(
		int sentenceNumber) {
	    return new SentenceLabelSubstitutionstatistics();
	}

	@Override
	public void updateWithSentenceLabelSubstitutionstatistics(
		SentenceLabelSubstitutionstatistics sentenceLabelSubstitutionstatistics) {
	    this.nBestListsLabelSubstitutionstatistics
		    .addSentenceLabelSubstitutionstatistics(sentenceLabelSubstitutionstatistics);

	}

    }

    protected static class NBestListsLabelSubstitutionstatisticsUpdaterAdditionalRun extends
	    NBestListsLabelSubstitutionstatisticsUpdater {

	protected NBestListsLabelSubstitutionstatisticsUpdaterAdditionalRun(
		NBestListsLabelSubstitutionstatistics nBestListsLabelSubstitutionstatistics) {
	    super(nBestListsLabelSubstitutionstatistics);
	}

	@Override
	public SentenceLabelSubstitutionstatistics getOrCreateSentenceLabelSubstitutionstatistics(
		int sentenceNumber) {
	    return this.nBestListsLabelSubstitutionstatistics
		    .getSentenceLabelSubstitutionstatistics(sentenceNumber);
	}

	@Override
	public void updateWithSentenceLabelSubstitutionstatistics(
		SentenceLabelSubstitutionstatistics sentenceLabelSubstitutionstatistics) {
	}

    }

    private static class SentenceLabelSubstitutionstatistics {
	private int hieroRuleToGlueRuleSubstitutionCount;
	private int matchSubstitutionsCount;
	private int noMatchSubstitutionsCount;

	private SentenceLabelSubstitutionstatistics() {
	    this.hieroRuleToGlueRuleSubstitutionCount = 0;
	    this.matchSubstitutionsCount = 0;
	    this.noMatchSubstitutionsCount = 0;
	}

	public void addBasicLabelSubstitutionFeatureWithValue(
		BasicLabelSubstitutionFeatureWithValue basicLabelSubstitutionFeatureWithValue) {
	    int featureCount = (int) basicLabelSubstitutionFeatureWithValue.getFeatureValue();

	    if (basicLabelSubstitutionFeatureWithValue.isGlueRuleSubstitutionFeature()) {
		addToGlueRuleSubstitutionCount(featureCount);
	    } else if (basicLabelSubstitutionFeatureWithValue.isMatchFeature()) {
		addMatchSubstitutionCount(featureCount);
	    } else {
		Assert.assertTrue(basicLabelSubstitutionFeatureWithValue.isNoMatchFeature());
		addNoMatchSubstitutionCount(featureCount);
	    }
	}

	public void addCountsFromBasicRuleTypeCounter(BasicRuleTypeCounter basicRuleTypeCounter) {
	    addToGlueRuleSubstitutionCount(basicRuleTypeCounter.getNumGlueRuleSubstitutions());
	    addMatchSubstitutionCount(basicRuleTypeCounter.getNumMatchingSubstitutions());
	}

	private void addToGlueRuleSubstitutionCount(int count) {
	    this.hieroRuleToGlueRuleSubstitutionCount += count;
	}

	private void addMatchSubstitutionCount(int count) {
	    this.matchSubstitutionsCount += count;
	}

	private void addNoMatchSubstitutionCount(int count) {
	    this.noMatchSubstitutionsCount += count;
	}

	/**
	 * Returns the number of substitutions of Hiero rules to glue rules
	 * 
	 * @return
	 */
	public int getHieroRuleToGlueRuleSubstitutionCount() {
	    return hieroRuleToGlueRuleSubstitutionCount;
	}

	public int getMatchSubstitutionCount() {
	    return matchSubstitutionsCount;
	}

	public int getNoMatchSubstitutionCount() {
	    return noMatchSubstitutionsCount;
	}

	private int getTotalRuleApplicationCount() {
	    return getHieroRuleToGlueRuleSubstitutionCount() + getNoMatchSubstitutionCount()
		    + getMatchSubstitutionCount();
	}

	public double getGlueRuleFraction() {
	    return (double) (getHieroRuleToGlueRuleSubstitutionCount())
		    / getTotalRuleApplicationCount();
	}

	public double getMatchSubstitutionFraction() {
	    return (double) (getMatchSubstitutionCount()) / getTotalRuleApplicationCount();
	}

	public double getNoMatchSubstitutionFraction() {
	    return (double) (getNoMatchSubstitutionCount()) / getTotalRuleApplicationCount();
	}

	public String toString() {
	    String result = "<SentenceLabelSubstitutionstatistics> ";
	    result += "## Fractions: glueRuleFrac: " + getGlueRuleFraction() + " "
		    + " matchSubstFrac: " + getMatchSubstitutionFraction() + " "
		    + " noMatchSubsFrac: " + getNoMatchSubstitutionFraction();

	    result += "## Counts: glueRuleCount: " + getHieroRuleToGlueRuleSubstitutionCount()
		    + " " + " matchSubstCount: " + getMatchSubstitutionCount() + " "
		    + " noMatchSubsCount: " + getNoMatchSubstitutionCount();
	    result += " </SentenceLabelSubstitutionstatistics>";
	    return result;
	}

    }

    public static class NBestListsLabelSubstitutionstatistics {
	private final List<SentenceLabelSubstitutionstatistics> sentenceLabelSubstitutionstatisticsList;

	private NBestListsLabelSubstitutionstatistics(
		List<SentenceLabelSubstitutionstatistics> sentenceLabelSubstitutionstatistics) {
	    this.sentenceLabelSubstitutionstatisticsList = sentenceLabelSubstitutionstatistics;
	}

	public static NBestListsLabelSubstitutionstatistics createNBestListsLabelSubstitutionstatistics() {
	    return new NBestListsLabelSubstitutionstatistics(
		    new ArrayList<SentenceLabelSubstitutionstatistics>());
	}

	private List<Double> getGlueRuleFractions() {
	    List<Double> result = new ArrayList<Double>();
	    for (SentenceLabelSubstitutionstatistics sentenceLabelSubstitutionstatistics : sentenceLabelSubstitutionstatisticsList) {
		result.add(sentenceLabelSubstitutionstatistics.getGlueRuleFraction());
	    }
	    return result;
	}

	private List<Double> getMatchSubstitutionFractions() {
	    List<Double> result = new ArrayList<Double>();
	    for (SentenceLabelSubstitutionstatistics sentenceLabelSubstitutionstatistics : sentenceLabelSubstitutionstatisticsList) {
		result.add(sentenceLabelSubstitutionstatistics.getMatchSubstitutionFraction());
	    }
	    return result;
	}

	private List<Double> getNoMatchSubstitutionFractions() {
	    List<Double> result = new ArrayList<Double>();
	    for (SentenceLabelSubstitutionstatistics sentenceLabelSubstitutionstatistics : sentenceLabelSubstitutionstatisticsList) {
		result.add(sentenceLabelSubstitutionstatistics.getNoMatchSubstitutionFraction());
	    }
	    return result;
	}

	public double getMeanGlueRuleFraction() {
	    return MeanStdComputation.computeDoublesMean(getGlueRuleFractions());
	}

	public double getStdGlueRuleFraction() {
	    return MeanStdComputation.computeDoublesStd(getGlueRuleFractions());
	}

	public double getMeanMatchSubstitutionsFraction() {
	    return MeanStdComputation.computeDoublesMean(getMatchSubstitutionFractions());
	}

	public double getStdMatchSubstittuionsFraction() {
	    return MeanStdComputation.computeDoublesStd(getMatchSubstitutionFractions());
	}

	public double getMeanNoMatchSubstitutionsFraction() {
	    return MeanStdComputation.computeDoublesMean(getNoMatchSubstitutionFractions());
	}

	public double getStdNoMatchSubstittuionsFraction() {
	    return MeanStdComputation.computeDoublesStd(getNoMatchSubstitutionFractions());
	}

	private int getNumSentences() {
	    return sentenceLabelSubstitutionstatisticsList.size();
	}

	public void addSentenceLabelSubstitutionstatistics(
		SentenceLabelSubstitutionstatistics sentenceLabelSubstitutionstatistics) {
	    this.sentenceLabelSubstitutionstatisticsList.add(sentenceLabelSubstitutionstatistics);
	}

	protected SentenceLabelSubstitutionstatistics getSentenceLabelSubstitutionstatistics(
		int sentenceNumber) {
	    return this.sentenceLabelSubstitutionstatisticsList.get(sentenceNumber);
	}

	public void printStatistics() {
	    int sentencNum = 0;
	    for (SentenceLabelSubstitutionstatistics sentenceLabelSubstitutiontatistics : sentenceLabelSubstitutionstatisticsList) {
		System.out.println("Sentence " + sentencNum + " statistics: "
			+ sentenceLabelSubstitutiontatistics);
		sentencNum++;
	    }
	}

	public void printSummarizedStatistics() {
	    String result = "Aggregated fractional statistics, aggregated over runs, then sentences: \n";
	    result += "mean/std glueRuleFrac: " + getMeanGlueRuleFraction() + " \\pm "
		    + getStdGlueRuleFraction() + "\n";
	    result += "mean/std matchSubstFrac: " + getMeanMatchSubstitutionsFraction() + " \\pm "
		    + getStdMatchSubstittuionsFraction() + "\n";
	    result += "mean/std noMatchSubstFrac: " + getMeanNoMatchSubstitutionsFraction()
		    + " \\pm " + getStdNoMatchSubstittuionsFraction() + "\n";
	    System.out.println(result);
	}

    }

    public static void main(String[] args) {
	if (args.length != 1) {
	    System.out
		    .println("usage:  sentencesNBestListLabelSubstitutionStatisticsComputer TARGZ_FILE_PATH");
	    System.exit(1);
	}
	SentencesNBestListLabelSubstitutionStatisticsComputer sentencesNBestListLabelSubstitutionStatisticsComputer = SentencesNBestListLabelSubstitutionStatisticsComputer
		.createSentencesNBestListLabelSubstitutionStatisticsComputer(false, false);
	sentencesNBestListLabelSubstitutionStatisticsComputer.addStatisticsFromTarGzFile(args[0]);
	// sentencesNBestListLabelSubstitutionStatisticsComputer.addStatisticsFromTarGzFile(args[0]);
	sentencesNBestListLabelSubstitutionStatisticsComputer.reportStatistics(args[0]);

    }

}
