package mt_pipeline.evaluation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

public class OOVFromOutputRemover {

    private static final String GENERIC_OOV_MARKER = "OOV";
    private static final String TOKEN_SEPARATOR = " ";

    private final BufferedWriter outputWriter;
    private final LineIterator inputFileLineIterator;
    private final boolean keepGenericOVVMarker;

    private OOVFromOutputRemover(BufferedWriter outputWriter, LineIterator inputFileLineIterator,
	    boolean keepGenericOVVMarker) {
	this.outputWriter = outputWriter;
	this.inputFileLineIterator = inputFileLineIterator;
	this.keepGenericOVVMarker = keepGenericOVVMarker;

    }

    public static OOVFromOutputRemover createOOVFromOutputRemover(String inputFileName,
	    String outputFileName, boolean keepGenericOVVMarker) {

	try {
	    BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFileName));
	    LineIterator inputFileLineIterator = FileUtils.lineIterator(new File(inputFileName),
		    "UTF-8");
	    return new OOVFromOutputRemover(outputWriter, inputFileLineIterator,
		    keepGenericOVVMarker);
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private String produceReformattedOOV() {
	if (keepGenericOVVMarker) {
	    return GENERIC_OOV_MARKER;
	}
	return "";
    }

    private boolean isOOV(String tokenString) {
	return tokenString.endsWith(GENERIC_OOV_MARKER);
    }

    private String produceReformattedToken(String tokenString) {
	if (isOOV(tokenString)) {
	    return produceReformattedOOV();
	}
	return tokenString;
    }

    private boolean writeReformattedToken(String originalToken, boolean earlierTokenWritten)
	    throws IOException {
	String reformattedToken = produceReformattedToken(originalToken);
	if (!reformattedToken.isEmpty()) {
	    if (earlierTokenWritten) {
		outputWriter.write(TOKEN_SEPARATOR);
	    }
	    outputWriter.write(reformattedToken);
	    return true;
	}
	return earlierTokenWritten;
    }

    private void produceOutputFileWithAdaptedOOVs() {
	try {
	    while (inputFileLineIterator.hasNext()) {
		String line = inputFileLineIterator.nextLine();
		String[] tokens = line.split(TOKEN_SEPARATOR);

		boolean earlierTokenWritten = false;

		if (tokens.length > 0) {
		    earlierTokenWritten = writeReformattedToken(tokens[0], earlierTokenWritten);
		}

		for (int i = 1; i < tokens.length; i++) {
		    earlierTokenWritten = writeReformattedToken(tokens[i], earlierTokenWritten);
		}
		outputWriter.write("\n");
	    }
	    System.out.println("Closing output writer");
	    outputWriter.close();
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    public static void main(String[] args) {

	if (args.length != 3) {
	    System.out
		    .print("Usage: oovFromOutputRemover INPUT_FILE_PATH OUTPUT_FILE_PATH KEEP_GENERIC_OOV_MARKER");
	}
	OOVFromOutputRemover oovFromOutputRemover = createOOVFromOutputRemover(args[0], args[1],
		Boolean.parseBoolean(args[2]));
	oovFromOutputRemover.produceOutputFileWithAdaptedOOVs();
    }

}
