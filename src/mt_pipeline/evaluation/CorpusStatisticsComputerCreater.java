package mt_pipeline.evaluation;

public interface CorpusStatisticsComputerCreater {
    public CorpusStatisticsComputer createCorpusStatisticsComputer(String configLine);
    public String getStatisticsTableHeaderString();
}
