package mt_pipeline.evaluation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import util.XMLTagging;

import junit.framework.Assert;

public class OrderedSentenceScoresDifferences {

	private final List<SentenceNumberScoresTriple> sentenceScoresDifferences;
	private final List<SentenceNumberScoresTriple> sentenceScoresDifferencesOrderedByBleuImprovement;
	private final List<SentenceNumberScoresTriple> sentenceScoresDifferencesOrderedByNistImprovement;

	private OrderedSentenceScoresDifferences(List<SentenceNumberScoresTriple> sentenceScoresDifferences, List<SentenceNumberScoresTriple> sentenceScoresDifferencesOrderedByBleuImprovement,
			List<SentenceNumberScoresTriple> sentenceScoresDifferencesOrderedByNistImprovement) {
		this.sentenceScoresDifferences = sentenceScoresDifferences;
		this.sentenceScoresDifferencesOrderedByBleuImprovement = sentenceScoresDifferencesOrderedByBleuImprovement;
		this.sentenceScoresDifferencesOrderedByNistImprovement = sentenceScoresDifferencesOrderedByNistImprovement;
	}

	public static OrderedSentenceScoresDifferences createOrderedSentenceScoresDifferences(SystemSentenceScores baselineScores, SystemSentenceScores otherSystemScores) {
		List<SentenceNumberScoresTriple> sentenceScoresDifferences = getSentenceScoresDifferences(baselineScores, otherSystemScores);
		List<SentenceNumberScoresTriple> sentenceScoresDifferencesOrderedByBleuImprovement = new ArrayList<SentenceNumberScoresTriple>(sentenceScoresDifferences);
		sentenceScoresDifferencesOrderedByBleuImprovement.sort(Collections.reverseOrder(new BleuBasedSentenceScoresComparater()));
		List<SentenceNumberScoresTriple> sentenceScoresDifferencesOrderedByNistImprovement = new ArrayList<SentenceNumberScoresTriple>(sentenceScoresDifferences);
		sentenceScoresDifferencesOrderedByNistImprovement.sort(Collections.reverseOrder(new NistBasedSentenceScoresComparater()));
		return new OrderedSentenceScoresDifferences(sentenceScoresDifferences, sentenceScoresDifferencesOrderedByBleuImprovement, sentenceScoresDifferencesOrderedByNistImprovement);
	}

	public static List<SentenceNumberScoresTriple> getSentenceScoresDifferences(SystemSentenceScores baselineScores, SystemSentenceScores otherSystemScores) {
		Assert.assertEquals(baselineScores.getNumScores(), otherSystemScores.getNumScores());
		List<SentenceNumberScoresTriple> result = new ArrayList<SentenceNumberScoresTriple>();
		for (int scoreNum = 0; scoreNum < baselineScores.getNumScores(); scoreNum++) {

			SentenceNumberScoresTriple baselineScoreTriple = baselineScores.getSentenceScore(scoreNum);
			SentenceNumberScoresTriple otherScoreTriple = otherSystemScores.getSentenceScore(scoreNum);
			result.add(SentenceNumberScoresTriple.getSentenceNumberScoresTripleDifference(baselineScoreTriple, otherScoreTriple));
		}
		return result;
	}

	private static class BleuBasedSentenceScoresComparater implements Comparator<SentenceNumberScoresTriple> {

		@Override
		public int compare(SentenceNumberScoresTriple o1, SentenceNumberScoresTriple o2) {

			if (o1.getBleuScore() == o2.getBleuScore()) {
				return o2.getSentenceNumber() - o1.getSentenceNumber();
			} else if (o1.getBleuScore() > o2.getBleuScore()) {
				return 1;
			} else {
				return -1;
			}

		}
	}

	private static class NistBasedSentenceScoresComparater implements Comparator<SentenceNumberScoresTriple> {
		@Override
		public int compare(SentenceNumberScoresTriple o1, SentenceNumberScoresTriple o2) {

			if (o1.getNistScore() == o2.getNistScore()) {
				return o2.getSentenceNumber() - o1.getSentenceNumber();
			} else if (o1.getNistScore() > o2.getNistScore()) {
				return 1;
			} else {
				return -1;
			}
		}
	}

	public List<SentenceNumberScoresTriple> getSentenceScoresDifferences() {
		return this.sentenceScoresDifferences;
	}

	public List<SentenceNumberScoresTriple> getSentenceScoresDifferencesOrderedByBleuImprovement() {
		return this.sentenceScoresDifferencesOrderedByBleuImprovement;
	}

	public List<SentenceNumberScoresTriple> getSentenceScoresDifferencesOrderedByNistImprovement() {
		return this.sentenceScoresDifferencesOrderedByNistImprovement;
	}

	private String getCategoryImprovementString(String category, double improvement, int sentenceNumber) {
		return XMLTagging.getTagSandwidchedString("ScoreImprovment", "Sentence:" + sentenceNumber + "," + category + "-improvment:" + improvement);
	}

	public String getImprovementStringForIndex(int index) {

		String result = "";
		SentenceNumberScoresTriple bleuScoresOrderedImprovmentTriple = this.sentenceScoresDifferencesOrderedByBleuImprovement.get(index);
		SentenceNumberScoresTriple nistScoresOrderedImprovmentTriple = this.sentenceScoresDifferencesOrderedByNistImprovement.get(index);
		result += getCategoryImprovementString("BLEU", bleuScoresOrderedImprovmentTriple.getBleuScore(), bleuScoresOrderedImprovmentTriple.getSentenceNumber());
		result += " ";
		result += getCategoryImprovementString("NIST", nistScoresOrderedImprovmentTriple.getNistScore(), nistScoresOrderedImprovmentTriple.getSentenceNumber());
		return result;
	}

	public int getNumScores() {
		return this.sentenceScoresDifferences.size();
	}
}
