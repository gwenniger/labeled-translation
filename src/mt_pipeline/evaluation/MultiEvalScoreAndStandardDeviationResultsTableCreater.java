package mt_pipeline.evaluation;

import mt_pipeline.evaluation.MultEvalSystemScores.SystemScoresBasic;
import mt_pipeline.evaluation.MultEvalSystemScores.SystemScoresWithBeer;


public class MultiEvalScoreAndStandardDeviationResultsTableCreater {

    public static MultiEvalResultsTable<SystemScoresBasic> createMultiEvalResultsTableScoreAndStandardDeviationBasic(
	    String resultFilePath,boolean outputLengthStatistics) {
	@SuppressWarnings("unchecked")
	MultiEvalResultsTableSystemsResultsCollecter<SystemScoresBasic> resultsCollector = (MultiEvalResultsTableSystemsResultsCollecter<SystemScoresBasic>) MultiEvalResultsTableSystemsResultsCollecter
		.createMultiEvalResultsTableSystemsResultsCollecter(resultFilePath,false,outputLengthStatistics);
	resultsCollector.extractAllSystemScoresFromInput();
	return resultsCollector.getResultsTable();

    }

    public static MultiEvalResultsTable<SystemScoresWithBeer> createMultiEvalResultsTableScoreAndStandardDeviationWithBeer(
	    String resultFilePath,boolean outputLengthStatistics) {
	@SuppressWarnings("unchecked")
	MultiEvalResultsTableSystemsResultsCollecter<SystemScoresWithBeer> resultsCollector = (MultiEvalResultsTableSystemsResultsCollecter<SystemScoresWithBeer>) MultiEvalResultsTableSystemsResultsCollecter
		.createMultiEvalResultsTableSystemsResultsCollecter(resultFilePath,true,outputLengthStatistics);
	resultsCollector.extractAllSystemScoresFromInput();
	return resultsCollector.getResultsTable();

    }
    
    public static void main(String[] args) {
	String path = "./multEvalTableCreaterTestData/allOutput-multeval-DeEn-Test.txt";
	MultiEvalResultsTable<SystemScoresBasic> multiEvalResultsTable = MultiEvalScoreAndStandardDeviationResultsTableCreater
		.createMultiEvalResultsTableScoreAndStandardDeviationBasic(path,false);
	multiEvalResultsTable.printScores(true);
    }

}
