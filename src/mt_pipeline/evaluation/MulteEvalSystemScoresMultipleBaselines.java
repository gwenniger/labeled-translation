package mt_pipeline.evaluation;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;

/**
 * This class takes care of computing score strings for MultEval metric scores
 * (BLEU, METEOR, BEER, TER, LENGTH) when there are multiple baselines.
 * @author gemaille
 *
 */
public class MulteEvalSystemScoresMultipleBaselines {
    private List<MultEvalSystemScores> systemScoresRelativeToMultipleBaselines;
    private List<MultEvalSystemScores> baselinesSystemScores;
    private final List<String> multipleBaselineSuffixLetterList = MultiEvalAndLRScoreTableDevAndTestTwoBaselines.MULTIPLE_BASELINE_SUFFIX_LETTER_LIST;

    private MulteEvalSystemScoresMultipleBaselines(
	    List<MultEvalSystemScores> systemScoresListMultipleBaselines,
	    List<MultEvalSystemScores> baselinesSystemScores) {
	this.systemScoresRelativeToMultipleBaselines = systemScoresListMultipleBaselines;
	this.baselinesSystemScores = baselinesSystemScores;
    }

    public static MulteEvalSystemScoresMultipleBaselines createSystemScoresTwoBaselines(
	    MultEvalSystemScores systemScoresRelativeToFirstBaseline,
	    MultEvalSystemScores systemScoresRelativeToSecondBaseline,
	    MultEvalSystemScores firstBaselineSystemScores,
	    MultEvalSystemScores secondBaselineSystemScores) {
	return new MulteEvalSystemScoresMultipleBaselines(Arrays.asList(
		systemScoresRelativeToFirstBaseline, systemScoresRelativeToSecondBaseline),
		Arrays.asList(firstBaselineSystemScores, secondBaselineSystemScores));
    }

    public static <E> List<E> getLastElementSublist(List<E> list) {
	List<E> result = list.subList(list.size() - 1, list.size());
	return result;
    }

    protected List<List<ScoreQuadruple>> getScoreTriplesLists() {
	List<List<ScoreQuadruple>> result = new ArrayList<List<ScoreQuadruple>>();
	for (MultEvalSystemScores multEvalSystemScores : this.systemScoresRelativeToMultipleBaselines) {
	    result.add(multEvalSystemScores.getOrderdScoreTriplesList());
	}
	return result;
    }

    protected List<List<ScoreQuadruple>> getScoreTriplesExceptLengthLists() {
	List<List<ScoreQuadruple>> result = new ArrayList<List<ScoreQuadruple>>();
	for (MultEvalSystemScores multEvalSystemScores : this.systemScoresRelativeToMultipleBaselines) {
	    result.add(multEvalSystemScores.getOrderdScoreTriplesList().subList(0,
		    multEvalSystemScores.getOrderdScoreTriplesList().size() - 1));
	}
	return result;
    }

    protected List<List<ScoreQuadruple>> getScoreTriplesLengthOnlyLists() {
	List<List<ScoreQuadruple>> result = new ArrayList<List<ScoreQuadruple>>();
	for (MultEvalSystemScores multEvalSystemScores : this.systemScoresRelativeToMultipleBaselines) {
	    result.add(getLastElementSublist(multEvalSystemScores.getOrderdScoreTriplesList()));
	}
	return result;
    }

    protected List<List<ScoreQuadruple>> getBaselineScoreTriplesLists() {
	List<List<ScoreQuadruple>> result = new ArrayList<List<ScoreQuadruple>>();
	for (MultEvalSystemScores multEvalSystemScores : this.baselinesSystemScores) {
	    result.add(multEvalSystemScores.getOrderdScoreTriplesList());
	}
	return result;
    }

    protected List<List<ScoreQuadruple>> getBaselineScoreTriplesExceptLengthLists() {
	List<List<ScoreQuadruple>> result = new ArrayList<List<ScoreQuadruple>>();
	for (MultEvalSystemScores multEvalSystemScores : this.baselinesSystemScores) {
	    result.add(multEvalSystemScores.getOrderdScoreTriplesList().subList(0,
		    multEvalSystemScores.getOrderdScoreTriplesList().size() - 1));
	}
	return result;
    }

    protected List<List<ScoreQuadruple>> getBaselineScoreTriplesLengthOnlyLists() {
	List<List<ScoreQuadruple>> result = new ArrayList<List<ScoreQuadruple>>();
	for (MultEvalSystemScores multEvalSystemScores : this.baselinesSystemScores) {
	    result.add(getLastElementSublist(multEvalSystemScores.getOrderdScoreTriplesList()));
	}
	return result;
    }

    public static List<ScoreQuadruple> getMultipleBaselineScoreQuadruplesListForIndex(
	    List<List<ScoreQuadruple>> scoreTriplesLists, int index) {
	List<ScoreQuadruple> result = new ArrayList<ScoreQuadruple>();
	for (List<ScoreQuadruple> scoreQuadruples : scoreTriplesLists) {
	    result.add(scoreQuadruples.get(index));
	}
	return result;
    }

    public String getLetteredImprovementMarkerString(ScoreQuadruple scoreQuadruple,
	    ScoreQuadruple baselineScoreQuadruple, int baselineIndex) {
	String result = scoreQuadruple.getImprovementMarkerString(baselineScoreQuadruple);
	if (result.length() > 0) {
	    result = " \\ " + result;
	    result += this.multipleBaselineSuffixLetterList.get(baselineIndex);
	}
	return result;

    }

    public String getMultipleBaselineImprovementMarkingString(
	    List<List<ScoreQuadruple>> scoreTriplesLists,
	    List<List<ScoreQuadruple>> baselineScoreTriplesLists, int index) {
	String result = "";
	List<ScoreQuadruple> multipleScoresRelativeToBaseline = getMultipleBaselineScoreQuadruplesListForIndex(
		scoreTriplesLists, index);
	List<ScoreQuadruple> multipleBaselineScores = getMultipleBaselineScoreQuadruplesListForIndex(
		baselineScoreTriplesLists, index);
	Assert.assertEquals(multipleScoresRelativeToBaseline.size(), multipleBaselineScores.size());

	for (int i = 0; i < multipleScoresRelativeToBaseline.size(); i++) {
	    ScoreQuadruple scoreQuadruple = multipleScoresRelativeToBaseline.get(i);
	    ScoreQuadruple baselineScoreQuadruple = multipleBaselineScores.get(i);

	    result += getLetteredImprovementMarkerString(scoreQuadruple, baselineScoreQuadruple, i);
	}

	return result;
    }

    public String getScoresString(List<List<ScoreQuadruple>> scoreTriplesLists,
	    List<List<ScoreQuadruple>> baselineScoreTriplesLists,
	    double significanceThresholdPValue, NumberFormat numberFormat,
	    boolean showStandardDeviation) {
	String result = "";

	List<ScoreQuadruple> firstBaselineScoreQuadruplesList = scoreTriplesLists.get(0);

	for (int i = 0; i < firstBaselineScoreQuadruplesList.size(); i++) {
	    ScoreQuadruple scoreTriple = firstBaselineScoreQuadruplesList.get(i);
	    result += scoreTriple.getScorePairString(numberFormat, showStandardDeviation);
	    result += getMultipleBaselineImprovementMarkingString(scoreTriplesLists,
		    baselineScoreTriplesLists, i);
	    if (i < firstBaselineScoreQuadruplesList.size() - 1) {
		result += " & ";
	    }
	}
	return result;
    }

    public String getScoresString(double significanceThresholdPValue, NumberFormat numberFormat,
	    boolean showStandardDeviation) {
	return getScoresString(getScoreTriplesExceptLengthLists(),
		getBaselineScoreTriplesExceptLengthLists(), significanceThresholdPValue,
		numberFormat, showStandardDeviation);
    }

    public String getScoresStringIncludingLength(double significanceThresholdPValue,
	    NumberFormat numberFormat, boolean showStandardDeviation) {
	return getScoresString(getScoreTriplesLists(), getBaselineScoreTriplesLists(),
		significanceThresholdPValue, numberFormat, showStandardDeviation);
    }

    public String getScoresStringLengthOnly(double significanceThresholdPValue,
	    NumberFormat numberFormat, boolean showStandardDeviation) {
	return getScoresString(getScoreTriplesLengthOnlyLists(),
		getBaselineScoreTriplesLengthOnlyLists(), significanceThresholdPValue,
		numberFormat, showStandardDeviation);
    }
}
