package mt_pipeline.evaluation;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import junit.framework.Assert;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import util.LinuxTarInterface;

public class TarGzFileLineInterator implements Iterator<String> {



    private final String unpackedTempFilePath;
    private LineIterator lineIterator;

    private TarGzFileLineInterator(String unpackedTempFilePath, LineIterator lineIterator) {
	this.unpackedTempFilePath = unpackedTempFilePath;
	this.lineIterator = lineIterator;
    }

    public static TarGzFileLineInterator createTarGzFileLineInterator(String tarGzFilePath) {
	try {
	    LinuxTarInterface.unpackTarGzFile(tarGzFilePath);
	    LineIterator lineIterator = FileUtils.lineIterator(new File(
		    LinuxTarInterface.getUnpackedFilePath(tarGzFilePath)), "UTF-8");
	    return new TarGzFileLineInterator(LinuxTarInterface.getUnpackedFilePath(tarGzFilePath), lineIterator);
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }





 
    @Override
    public boolean hasNext() {
	boolean hasNext = false;
	if (lineIterator != null) {
	    hasNext = lineIterator.hasNext();

	    if (!hasNext) {
		File fileToDelete = new File(this.unpackedTempFilePath);
		System.out.println("deleting temporary file " + this.unpackedTempFilePath);
		boolean deletingSuccesfull = fileToDelete.delete();
		Assert.assertTrue(deletingSuccesfull);
		// closing the line iterator is essential to prevent a - possibly big - memory leak
		lineIterator.close();
		lineIterator = null;
	    }
	}
	return hasNext;
    }

    @Override
    public String next() {
	return lineIterator.nextLine();
    }

    @Override
    public void remove() {
	throw new RuntimeException("not implemented");

    }

    public static void main(String[] args) {
	if (args.length != 1) {
	    System.out.println("usage: targzFileLineIterator TARGZ_FILE_PATH");
	    System.exit(1);
	}

	TarGzFileLineInterator tarGzFileLineInterator = createTarGzFileLineInterator(args[0]);
	int lineNumbers = 0;
	while (tarGzFileLineInterator.hasNext()) {
	    // System.out.println(tarGzFileLineInterator.next());
	    tarGzFileLineInterator.next();
	    lineNumbers++;
	}
	System.out.println("counted " + lineNumbers + " lines");
    }

}
