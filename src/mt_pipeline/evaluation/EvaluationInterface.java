package mt_pipeline.evaluation;

import mt_pipeline.MTComponentCreater;
import mt_pipeline.mt_config.MTConfigFile;

public class EvaluationInterface extends MTComponentCreater {

	protected EvaluationInterface(MTConfigFile theConfig) {
		super(theConfig);
	}

	public static EvaluationInterface createEvaluationInterface(MTConfigFile theConfig) {
		return new EvaluationInterface(theConfig);
	}

	private void wrapDecodeResultInGML(boolean useTunedParameters, String category) {
		GmlWrapper.createGMLWrappedFile(theConfig.filesConfig.getOneBestOutputFilePath(category, useTunedParameters), theConfig.filesConfig.getOneBestOutputFilePathGML(category, useTunedParameters),
				theConfig.filesConfig.getSourceLanguageAbbreviation(), theConfig.filesConfig.getTargetLanguageAbbreviation(), theConfig.filesConfig.getSetAndDocID(category), GmlWrapper.TEST_TYPE);
	}

	private void computeBleuAndNist(boolean useTunedParameters, String category) {
		MTEvalInterface mtEvalInterface = MTEvalInterface.createMtEvalInterfaceForGMLFiles(theConfig.filesConfig.getGMLSrcFileName(category),
				theConfig.filesConfig.getOneBestOutputFilePathGML(category, useTunedParameters), theConfig.filesConfig.getGMLRefFileName(category), theConfig.foldersConfig.getScriptsDir(),
				theConfig.filesConfig.getResultPathForCategory(useTunedParameters, category));
		mtEvalInterface.computeBleuAndNist();
	}

	private void computeSentenceIndividualScores(boolean useTunedParameters, String category) {
		IndividualSentencesEvaluater individualSentencesEvaluater = IndividualSentencesEvaluater.createIndividualSentencesEvaluater(theConfig.filesConfig.getGMLSrcFileName(category),
				theConfig.filesConfig.getOneBestOutputFilePathGML(category, useTunedParameters), theConfig.filesConfig.getGMLRefFileName(category), theConfig.foldersConfig.getScriptsDir(),
				theConfig.filesConfig.getSentencesIndividualScoresResultPathForCategory(useTunedParameters, category));
		individualSentencesEvaluater.createSentencIndividualScoresFile();
	}

	private void performEvaluation(boolean useTunedParameters, String category) {
		wrapDecodeResultInGML(useTunedParameters, category);
		// wrapDecodeResultLinesInGMLInSeperateFiles(useTunedParameters,
		// category);
		computeBleuAndNist(useTunedParameters, category);
		//computeSentenceIndividualScores(useTunedParameters, category);
	}

	public void performEvaluation() {
	    	if(MTConfigFile.performTuning(theConfig)){  
	    	    performEvaluation(true, MTConfigFile.TEST_CATEGORY);
	    	    performEvaluation(true, MTConfigFile.DEV_CATEGORY);
	    	}    
		performEvaluation(false, MTConfigFile.DEV_CATEGORY);
		performEvaluation(false, MTConfigFile.TEST_CATEGORY);
	}

}
