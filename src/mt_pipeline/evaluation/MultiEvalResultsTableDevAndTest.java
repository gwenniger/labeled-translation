package mt_pipeline.evaluation;

import org.junit.Assert;

import util.ConfigFile;

public abstract class MultiEvalResultsTableDevAndTest {
    protected final String NL = "\n";
    private static final String DEV_RESULT_FILE_PATH_PROPERTY = "devResultFilePath";
    private static final String TEST_RESULT_FILE_PATH_PROPERTY = "testResultFilePath";
    private static final String OUTPUT_FILE_PATH_PROPERTY = "outputFilePath";
    protected final MultiEvalResultsTable multiEvalResultsTableDev;
    protected final MultiEvalResultsTable multiEvalResultsTableTest;

    protected MultiEvalResultsTableDevAndTest(MultiEvalResultsTable multiEvalResultsTableDev,
	    MultiEvalResultsTable multiEvalResultsTableTest) {
	this.multiEvalResultsTableDev = multiEvalResultsTableDev;
	this.multiEvalResultsTableTest = multiEvalResultsTableTest;
	Assert.assertEquals(multiEvalResultsTableDev.numSystems(), this.multiEvalResultsTableTest.numSystems());
    }

    public void printScores( boolean showStandardDeviation) {
	System.out.println("\n\n\nScores Table:");
	for (int i = 0; i < multiEvalResultsTableDev.numSystems(); i++) {
	    System.out.println(this.multiEvalResultsTableDev.getSystemName(i) + " & "
		    + this.multiEvalResultsTableDev.getSystemScoresString(i,showStandardDeviation) + " & "
		    + this.multiEvalResultsTableTest.getSystemScoresString(i,showStandardDeviation) + " \\\\");
	}
    }

    protected abstract String tableHeader();

    protected String tableFooter() {
	return "\\hline" + NL + "\\end{tabular}" + NL;
    }

    public abstract void writeScoresToFile(String outputFilePath, boolean showStandardDeviation);

    protected static String getDevResultFilePath(ConfigFile theConfig) {
	return theConfig.getValueWithPresenceCheck(DEV_RESULT_FILE_PATH_PROPERTY);
    }

    protected static String getTestResultFilePath(ConfigFile theConfig) {
	return theConfig.getValueWithPresenceCheck(TEST_RESULT_FILE_PATH_PROPERTY);
    }

    protected static String getOutputResultFilePath(ConfigFile theConfig) {
	return theConfig.getValueWithPresenceCheck(OUTPUT_FILE_PATH_PROPERTY);
    }

    
}
