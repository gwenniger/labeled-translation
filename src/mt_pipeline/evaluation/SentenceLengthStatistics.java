package mt_pipeline.evaluation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import util.FileUtil;
import util.Pair;

public class SentenceLengthStatistics {
    private static final String NL = "\n";

    private final LineIterator inputerator;
    private final Map<Integer, Integer> lengthFrequenciesMap;

    private SentenceLengthStatistics(LineIterator inputIterator,
	    Map<Integer, Integer> lengthCountsMap) {
	this.inputerator = inputIterator;
	this.lengthFrequenciesMap = lengthCountsMap;
    }

    public static SentenceLengthStatistics createSentenceLengthStatistics(String inputFilePath) {
	LineIterator inputIterator;
	try {
	    inputIterator = FileUtils.lineIterator(new File(inputFilePath), "UTF-8");
	    SentenceLengthStatistics result = new SentenceLengthStatistics(inputIterator,
		    new HashMap<Integer, Integer>());
	    result.collectStatistics();
	    return result;
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private int countNumberOfWords(String sentence) {
	String[] words = sentence.split("\\s");
	return words.length;
    }

    private void addWordCount(int wordCount) {
	if (lengthFrequenciesMap.containsKey(wordCount)) {
	    int currentLengthFrequency = lengthFrequenciesMap.get(wordCount);
	    currentLengthFrequency++;
	    lengthFrequenciesMap.put(wordCount, currentLengthFrequency);
	} else {
	    lengthFrequenciesMap.put(wordCount, 1);
	}
    }

    public void collectStatistics() {
	while (inputerator.hasNext()) {
	    String sentence = inputerator.nextLine();
	    int wordCount = countNumberOfWords(sentence);
	    addWordCount(wordCount);
	}
	inputerator.close();
    }

    private List<Pair<Integer>> getLengthFrequenciesOrderedByLengthAscending() {
	List<Pair<Integer>> result = new ArrayList<Pair<Integer>>();
	for (Entry<Integer, Integer> entry : lengthFrequenciesMap.entrySet()) {
	    result.add(new Pair<Integer>(entry.getKey(), entry.getValue()));
	}

	/// http://www.baeldung.com/java-8-sort-lambda
	//Sort using function reference
	result.sort(Comparator.comparing(Pair::getFirst));

	// http://stackoverflow.com/questions/25172595/comparator-reversed-not-compiles-using-lambda
	// Alternative with explicit lambda
	//result.sort(Comparator.comparing((Pair<Integer> pair) -> pair.getFirst()));		
	return result;
    }

    private List<Pair<Integer>> getLengthFrequenciesOrderedByLengthAscendingInsertZeroEntriesForGaps() {
	List<Pair<Integer>> lengthFrequencies = getLengthFrequenciesOrderedByLengthAscending();
	List<Pair<Integer>> result = new ArrayList<Pair<Integer>>();
		
	int previousLength = -1;
	for (Pair<Integer> lengthFrequencyPair : lengthFrequencies) {
	    int nextLength = lengthFrequencyPair.getFirst();
	    // Insert count 0 entries for gaps
	    for (int i = previousLength + 1; i < nextLength; i++) {
		result.add(new Pair<Integer>(i, 0));
	    }
	    result.add(lengthFrequencyPair);
	    previousLength = nextLength;
	}

	return result;
    }

    private List<Integer> getLengthsOrderedAscending() {
	List<Integer> result = new ArrayList<Integer>();
	List<Pair<Integer>> lengthFrequencies = getLengthFrequenciesOrderedByLengthAscending();

	for (Pair<Integer> lengthFrequencyPair : lengthFrequencies) {
	    int length = lengthFrequencyPair.getFirst();
	    int frequency = lengthFrequencyPair.getSecond();
	    for (int i = 0; i < frequency; i++) {
		result.add(length);
	    }
	}
	return result;
    }

    private List<Double> convertIntegerListToDoubleList(List<Integer> integerList) {
	List<Double> result = new ArrayList<Double>();
	for (Integer integer : integerList) {
	    result.add(new Double(integer));
	}
	return result;
    }

    private double computeAverageLength() {
	DoubleListStatistics doubleListStatistics = DoubleListStatistics
		.createDoublesListStatistics(convertIntegerListToDoubleList(getLengthsOrderedAscending()));
	return doubleListStatistics.getMean();
    }

    private double computeStdLength() {
	DoubleListStatistics doubleListStatistics = DoubleListStatistics
		.createDoublesListStatistics(convertIntegerListToDoubleList(getLengthsOrderedAscending()));
	return doubleListStatistics.getStd();
    }

    private int computeMinLength() {
	return Collections.min(getLengthsOrderedAscending());
    }

    private int computeMaxLength() {
	return Collections.max(getLengthsOrderedAscending());
    }

    private String lengthFrequencyString(int length, int frequency) {
	return length + "," + frequency + "\n";
    }

    private String getStatisticsSummaryString() {
	String result = "";
	result += "min_length:," + computeMinLength() + NL;
	result += "max_length:," + computeMaxLength() + NL;
	result += "average_length:," + computeAverageLength() + NL;
	result += "standard_deviation_length:," + computeStdLength() + NL;
	return result;
    }

    public String getLengthStatisticsString() {
	String result = getStatisticsSummaryString() + NL;
	result += "length:,frequency:" + NL;
	List<Pair<Integer>> orderedLengthFrequenciesList = getLengthFrequenciesOrderedByLengthAscendingInsertZeroEntriesForGaps();
	for (Pair<Integer> lengthFrequencyPair : orderedLengthFrequenciesList) {
	    int length = lengthFrequencyPair.getFirst();
	    int frequency = lengthFrequencyPair.getSecond();
	    result += lengthFrequencyString(length, frequency);
	}
	return result;
    }

    private void writeSentenceLengthStatistics(String outputFilePath) {
	BufferedWriter outputWriter = null;
	try {
	    outputWriter = new BufferedWriter(new FileWriter(outputFilePath));
	    outputWriter.write(getLengthStatisticsString());
	} catch (IOException e) {
	    throw new RuntimeException(e);
	} finally {
	    FileUtil.closeCloseableIfNotNull(outputWriter);
	}
    }

    public static void main(String[] args) {
	if (args.length != 2) {
	    System.out
		    .println("usage: sentenceLengthStatistics INPUT_FILE_PATH OUTPUT_STATISTICS_FILE_PATH ");
	    System.exit(1);
	}
	SentenceLengthStatistics sentenceLengthStatistics = createSentenceLengthStatistics(args[0]);
	System.out.println(sentenceLengthStatistics.getLengthStatisticsString());
	sentenceLengthStatistics.writeSentenceLengthStatistics(args[1]);
    }

}
