package mt_pipeline.evaluation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import junit.framework.Assert;
import mt_pipeline.evaluation.BasicLabelSubstitutionFeature.BasicLabelSubstitutionFeatureWithWeight;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import util.ConfigFile;
import util.FileUtil;
import util.MeanStdComputation;

public class LabelSubstitutionFeatureWeightTableConstructer {
    private static String LATEX_COLUMN_SEPARATOR = " & ";
    private static String LATEX_NEWLINE_CODE = "\\\\";
    private static String LATEX_HLINE_CODE = "\\hline";

    private static String RAW_SCORES_TABLE_CAPTION_TEXT = "Raw label substiution feature weights. "
	    + "These weights are somewhat deceptive,"
	    + "\nas they are trained in conjunction with a coarse MATCH/NO\\_MATCH feature which claims a large part of the weight in practice.";
    private static String COMPENSATED_SCORES_TABLE_CAPTION_TEXT = "Compensated label substiution feature weights. These scores are corrected by adding the MATCH feature"
	    + "\nweight for matching substitutions and NO\\_MATCH weights for mismatching substitutions. \n"
	    + "Notice how in general these compensated weights are positive when a label matches to itself, and negative otherwise,"
	    + "\nas  would be expected.";
    private static String WEIGHT_FILE_PATHS_PROPERTY = "weightFilePathsList";
    protected static String AUTHOR_NAME_STRING = "Gideon Maillette de Buy Wenniger";
    private static String DOCUMENT_TITLE_STRING = "Label Substitution Feature Tables";

    private static String LABEL_SUBSTITUTION_MATCH_FEATURE_MARKER = "LabelSubstitution_MATCH";
    private static String LABEL_SUBSTITUTION_NOMATCH_FEATURE_MARKER = "LabelSubstitution_NOMATCH";

    private static final NumberFormat NUMBER_FORMAT = createNumberFormat();

    private static final String SUBSTITUTED_LABEL_STRING = "Substituted label";
    private static final String SUBSTITUTED_TO_LABEL_STRING = "Substituted to label";
    private static final String WEIGHTS_TABLE_OUTPUT_FILE_PATH_PROPERTY = "weightsTableOutputFilePath";

    private final List<LineIterator> weightFileLineIterators;
    private final Map<String, Map<String, Map<Integer,Double>>> labelSubsitutionFeatureWeightsMap;
    private final List<Double> matchWeights;
    private final List<Double> noMatchWeights;

    public static NumberFormat createNumberFormat() {
	NumberFormat numberFormat = new DecimalFormat("0.#E0");
	numberFormat.setMinimumFractionDigits(1);
	numberFormat.setMaximumFractionDigits(1);
	return numberFormat;
    }
    
    public static NumberFormat createNumberFormatNoE() {
 	NumberFormat numberFormat = new DecimalFormat("0.##");
 	numberFormat.setMinimumFractionDigits(2);
 	return numberFormat;
     }

    private LabelSubstitutionFeatureWeightTableConstructer(
	    List<LineIterator> weightFileLineIterators,
	    Map<String, Map<String, Map<Integer,Double>>> labelSubsitutionFeatureWeightsMap,
	    List<Double> matchWeights,List<Double> noMatchWeights) {
	this.weightFileLineIterators = weightFileLineIterators;
	this.labelSubsitutionFeatureWeightsMap = labelSubsitutionFeatureWeightsMap;
	this.matchWeights = matchWeights;
	this.noMatchWeights = noMatchWeights;
    }

    public static List<LineIterator> createWeightFileLineIterators(List<String> weightFilePaths)
	    throws IOException {
	List<LineIterator> result = new ArrayList<LineIterator>();
	for (String weightFilePath : weightFilePaths) {
	    result.add(FileUtils.lineIterator(new File(weightFilePath), "UTF8"));
	}
	return result;
    }

    public static LabelSubstitutionFeatureWeightTableConstructer createLabelSubstitutionFeatureWeightTableConstructer(
	    List<String> weightFilePaths) {
	try {
	    List<LineIterator> weightFileLineIterators = createWeightFileLineIterators(weightFilePaths);
	    Map<String, Map<String, Map<Integer,Double>>> labelSubsitutionFeatureWeightsMap = new HashMap<String, Map<String, Map<Integer,Double>>>();
	    return new LabelSubstitutionFeatureWeightTableConstructer(weightFileLineIterators,
		    labelSubsitutionFeatureWeightsMap, new ArrayList<Double>(),new ArrayList<Double>());
	} catch (IOException e) {
	    throw new RuntimeException(e);
	}
    }
    
    public double getNumRuns(){
	return this.matchWeights.size();
    }

    public double getMeanCoarseMatchWeigth(){
	return MeanStdComputation.computeDoublesMean(matchWeights);
    }
    
    public double getMeanCoarseNoMatchWeigth(){
	return MeanStdComputation.computeDoublesMean(noMatchWeights);
    }
    
    private void addToOuterMap(String featureLabelOuterMap) {
	// System.out.println("addToOuterMap: " + featureLabelOuterMap);
	Map<String, Map<Integer,Double>> elementToAdd = new HashMap<String, Map<Integer,Double>>();
	labelSubsitutionFeatureWeightsMap.put(featureLabelOuterMap, elementToAdd);
    }

    private boolean outerMapContains(String featureLabelOuterMap) {
	return this.labelSubsitutionFeatureWeightsMap.containsKey(featureLabelOuterMap);
    }

    private Map<String, Map<Integer,Double>> getFromOuterMap(String featureLabelOuterMap) {
	return labelSubsitutionFeatureWeightsMap.get(featureLabelOuterMap);
    }

    private boolean innerMapContains(String featureLabelOuterMap, String featureLabelInnerMap) {
	return this.getFromOuterMap(featureLabelOuterMap).containsKey(featureLabelInnerMap);
    }
    
    private boolean innerMapContainsForIterationNumber(String featureLabelOuterMap, String featureLabelInnerMap, int iterationNumber) {
   	return this.getFromOuterMap(featureLabelOuterMap).get(featureLabelInnerMap).containsKey(iterationNumber);
       }


    private void addToInnerMap(String featureLabelOuterMap, String featureLabelInnerMap) {
	// System.out.println("addToInnerMap: " + featureLabelOuterMap + "|" +
	// featureLabelInnerMap);
	getFromOuterMap(featureLabelOuterMap).put(featureLabelInnerMap, new HashMap<Integer,Double>());
    }

    private Map<Integer,Double> getFromInnerMap(String featureLabelOuterMap, String featureLabelInnerMap) {
	Map<String, Map<Integer,Double>> innerMap = getFromOuterMap(featureLabelOuterMap);
	Assert.assertNotNull(innerMap);
	Map<Integer,Double> result = innerMap.get(featureLabelInnerMap);
	if (result == null) {
	    System.out.println("WARNING : getFromInnerMap(" + featureLabelOuterMap + ","
		    + featureLabelInnerMap + ") returns null");
	}
	return result;
    }

    private void addValueToInnerMap(String featureLabelOuterMap, String featureLabelInnerMap,
	    Integer runIndex, Double value) {
	getFromInnerMap(featureLabelOuterMap, featureLabelInnerMap).put(runIndex,value);
    }

    private void addValue(String featureLabelOuterMap, String featureLabelInnerMap, Integer runIndex, Double value) {
	if (!outerMapContains(featureLabelOuterMap)) {
	    addToOuterMap(featureLabelOuterMap);
	}
	if (!innerMapContains(featureLabelOuterMap, featureLabelInnerMap)) {
	    addToInnerMap(featureLabelOuterMap, featureLabelInnerMap);

	}
	addValueToInnerMap(featureLabelOuterMap, featureLabelInnerMap,runIndex, value);
    }

    private void addBasicLabelSubstitutionFeature(
	    BasicLabelSubstitutionFeatureWithWeight basicLabelSubsitututionFeature,  Integer runIndex) {
	addValue(basicLabelSubsitututionFeature.getSubstitutionLabel(),
		basicLabelSubsitututionFeature.getSubstitutionToLabel(),runIndex,
		basicLabelSubsitututionFeature.getFeatureWeight());
    }

    private static boolean constitutesMatchFeatureString(String string) {
	return string.contains(LABEL_SUBSTITUTION_MATCH_FEATURE_MARKER);
    }

    private static boolean constitutesNoMatchFeatureString(String string) {
	return string.contains(LABEL_SUBSTITUTION_NOMATCH_FEATURE_MARKER);
    }

    private double getMatchOrNoMatchFeatureWeight(String matchOrNoMatchFeatureString) {
	String[] featureParts = matchOrNoMatchFeatureString.split(" ");
	return Double.parseDouble(featureParts[1]);
    }

    private void addWeightsFromFileLineIterator(LineIterator lineIterator, Integer runIndex) {
	String line = null;
	while (lineIterator.hasNext()) {
	    line = lineIterator.nextLine();
	    if (BasicLabelSubstitutionFeature.consitutesLabelSubstitutionFeatureString(line)) {
		BasicLabelSubstitutionFeatureWithWeight basicLabelSubsitututionFeature = BasicLabelSubstitutionFeatureWithWeight
			.createBasicLabelSubstitutionFeatureWithWeight(line);
		// System.out.println("label substitution feature containing line: "
		// + line);
		addBasicLabelSubstitutionFeature(basicLabelSubsitututionFeature,runIndex);
	    } else if (constitutesMatchFeatureString(line)) {
		this.matchWeights.add(getMatchOrNoMatchFeatureWeight(line));
	    } else if (constitutesNoMatchFeatureString(line)) {
		this.noMatchWeights.add(getMatchOrNoMatchFeatureWeight(line));
	    }

	}
    }

    public void addWeightsFromFileLineIterators() {
	int runIndex = 1;
	for (LineIterator lineIterator : this.weightFileLineIterators) {
	    System.out.println("Adding weight files from file iterator " + runIndex);
	    addWeightsFromFileLineIterator(lineIterator,runIndex);
	    runIndex++;
	}
    }

    private double getMeanValue(List<Double> valuesList) {
	return util.MeanStdComputation.computeDoublesMean(valuesList);
    }

    public boolean hasLabelSubstitutionFeatureValue(String subtitutionLabel,
	    String substitutionToLabel) {
	if (!outerMapContains(subtitutionLabel)) {
	    return false;
	}
	if (!innerMapContains(subtitutionLabel, substitutionToLabel)) {
	    return false;
	}
	return true;
    }
    
    public boolean hasLabelSubstitutionFeatureValueForIteration(String subtitutionLabel,
	    String substitutionToLabel, int iterationNumber) {
	if (!outerMapContains(subtitutionLabel)) {
	    return false;
	}
	if (!innerMapContains(subtitutionLabel, substitutionToLabel)) {
	    return false;
	}
	if(!innerMapContainsForIterationNumber(subtitutionLabel, substitutionToLabel, iterationNumber)){
	    return false;
	}
	
	return true;
    }

    private double getMeanSubstitutionFeatureValue(String subtitutionLabel,
	    String substitutionToLabel) {
	List<Double> values = new ArrayList<Double>();
	values.addAll(getFromInnerMap(subtitutionLabel, substitutionToLabel).values());
	return getMeanValue(values);
    }
    
    private List<Double> getCompensatedSubstitutionFeatureValues(String subtitutionLabel,
	    String substitutionToLabel,List<Double> compensationValuesList) {
	List<Double> result = new ArrayList<Double>();
	Map<Integer,Double> substitutionFeatureValues = getFromInnerMap(subtitutionLabel, substitutionToLabel);
	
	for(Integer runIndex : substitutionFeatureValues.keySet()){
	    result.add(substitutionFeatureValues.get(runIndex) + compensationValuesList.get(runIndex - 1));
	}
	return result;
    }
    
    private List<Double> getCompensatedSubstitutionFeatureValues(String subtitutionLabel,
	    String substitutionToLabel){
	if(isMatchFeature(subtitutionLabel,substitutionToLabel)){
	    return getCompensatedSubstitutionFeatureValues(subtitutionLabel, substitutionToLabel, matchWeights);
	}
	else{
	    return getCompensatedSubstitutionFeatureValues(subtitutionLabel, substitutionToLabel, noMatchWeights);
	}
    } 
    
    private double getCompensatedSubstitutionFeatureValue(String subtitutionLabel,
	    String substitutionToLabel,int runIndex,List<Double> compensationValuesList) {
	
	Map<Integer,Double> substitutionFeatureValues = getFromInnerMap(subtitutionLabel, substitutionToLabel);
	
	double result = substitutionFeatureValues.get(runIndex) + compensationValuesList.get(runIndex - 1);
	return result;
    }
    
    public double getCompensatedSubstitutionFeatureValue(String subtitutionLabel,
	    String substitutionToLabel,int runIndex){
	if(isMatchFeature(subtitutionLabel,substitutionToLabel)){
	    return getCompensatedSubstitutionFeatureValue(subtitutionLabel, substitutionToLabel,runIndex, matchWeights);
	}
	else{
	    return getCompensatedSubstitutionFeatureValue(subtitutionLabel, substitutionToLabel,runIndex, noMatchWeights);
	}
    }    
    

    private boolean isMatchFeature(String fromLabel, String toLabel) {
	return fromLabel.equals(toLabel);
    }

    protected List<String> getSortedFromLabelsList() {
	List<String> fromLabelsList = new ArrayList<String>(
		this.labelSubsitutionFeatureWeightsMap.keySet());
	fromLabelsList.sort(null);
	return fromLabelsList;
    }

    protected List<String> getSortedToLabelsList() {
	Set<String> toLabelsSet = new HashSet<String>();
	for (String fromLabel : this.labelSubsitutionFeatureWeightsMap.keySet()) {
	    toLabelsSet.addAll(getFromOuterMap(fromLabel).keySet());
	}
	List<String> toLabelsList = new ArrayList<String>(toLabelsSet);
	toLabelsList.sort(null);
	return toLabelsList;
    }

    public void writeTable(FeatureWeightComputer featureWeightComputer) {

	System.out.print("                    ");
	for (String toLabel : getSortedToLabelsList()) {
	    System.out.print(toLabel + "    ");
	}
	System.out.print("\n");

	for (String fromLabel : getSortedFromLabelsList()) {
	    System.out.print(fromLabel + " || ");
	    for (String toLabel : getSortedToLabelsList()) {

		if (hasLabelSubstitutionFeatureValue(fromLabel, toLabel)) {
		    double weight = featureWeightComputer.getWeight(fromLabel, toLabel);
		    System.out.print(NUMBER_FORMAT.format(weight) + "    ");
		} else {
		    System.out.print("----" + "    ");
		}

	    }
	    System.out.println("\n");
	}
    }

    public void writeLatexTableHeader(Writer writer) throws IOException {
	String header = "\\begin{table*}[t]" + "\n" + "\\resizebox{1.0\\textwidth}{!}{%" + "\n"
		+ "\\begin{tabular}{|";
	for (int i = 0; i < getSortedToLabelsList().size() + 1; i++) {
	    header += "l|";
	}
	header += "}\n";
	header += LATEX_HLINE_CODE;
	writer.write("\n" + header);
    }

    public void writeLatexTableFooter(Writer writer, String captionText) throws IOException {
	writer.write("\n" + "\n" + "\\end{tabular}\n}\n" + latexCaptionString(captionText)
		+ "\\end{table*}");
    }

    private String latexBoldFormatString(String string) {
	return "{\\bf " + string + "}";
    }

    public int getNumToLabels() {
	return getSortedToLabelsList().size();
    }

    public void writeColumnHeaders(Writer writer) throws IOException {
	writer.write("\n" + "\\multirow{2}{*}{" + SUBSTITUTED_LABEL_STRING + "} &  \\multicolumn{"
		+ getNumToLabels() + "}{c|}{" + SUBSTITUTED_TO_LABEL_STRING + "} "
		+ LATEX_NEWLINE_CODE);
	writer.write("\n" + "\\cline{2 - " + getNumToLabels() + "}");

    }

    public String latexCaptionString(String captionText) {
	return "\\caption{" + captionText + "}\n";
    }

    private List<Double> getCompensatedMatchingFeaturesList(){
	MatchFeatureCompensatedWeightComputer matchFeatureCompensatedWeightComputer = new MatchFeatureCompensatedWeightComputer(this);
	List<Double> result = new ArrayList<Double>();
	for (String fromLabel : getSortedFromLabelsList()) {
	    for (String toLabel : getSortedToLabelsList()) {
		if(hasLabelSubstitutionFeatureValue(fromLabel,toLabel)){	
		    if(isMatchFeature(fromLabel, toLabel) ){
		    result.add(matchFeatureCompensatedWeightComputer.getWeight(fromLabel, toLabel));
		    }
		}
	    }
	}
	return result;
    }
    
    private List<Double> getCompensatedMisMatchingFeaturesList(){
 	MatchFeatureCompensatedWeightComputer matchFeatureCompensatedWeightComputer = new MatchFeatureCompensatedWeightComputer(this);
 	List<Double> result = new ArrayList<Double>();
 	for (String fromLabel : getSortedFromLabelsList()) {
 	    for (String toLabel : getSortedToLabelsList()) {
 		
 		if(hasLabelSubstitutionFeatureValue(fromLabel, toLabel)){    
 		    if(!isMatchFeature(fromLabel, toLabel)){
 			result.add(matchFeatureCompensatedWeightComputer.getWeight(fromLabel, toLabel));
 		    }
 		}
 	    }
 	}
 	return result;
     }
    
    public DoubleListStatistics getCompensatedMatchingFeaturesStatistics(){
	return DoubleListStatistics.createDoublesListStatistics(getCompensatedMatchingFeaturesList());
    }
   
    public DoubleListStatistics getCompensatedMisMatchingFeaturesStatistics(){
   	return DoubleListStatistics.createDoublesListStatistics(getCompensatedMisMatchingFeaturesList());
       }
    
    public void writeLatexTable(FeatureWeightComputer featureWeightComputer, String captionText,
	    Writer writer) throws IOException {

	writeLatexTableHeader(writer);
	writeColumnHeaders(writer);

	for (String toLabel : getSortedToLabelsList()) {
	    writer.write(LATEX_COLUMN_SEPARATOR + toLabel);
	}
	writer.write(LATEX_NEWLINE_CODE + "\n" + LATEX_HLINE_CODE + "\n");

	for (String fromLabel : getSortedFromLabelsList()) {
	    writer.write(fromLabel);
	    for (String toLabel : getSortedToLabelsList()) {

		if (hasLabelSubstitutionFeatureValue(fromLabel, toLabel)) {
		    double weight = featureWeightComputer.getWeight(fromLabel, toLabel);
		    String latexNumberString = NUMBER_FORMAT.format(weight);
		    if (weight > 0) {
			latexNumberString = latexBoldFormatString(latexNumberString);
		    }
		    writer.write(LATEX_COLUMN_SEPARATOR + latexNumberString);
		} else {
		    writer.write(LATEX_COLUMN_SEPARATOR + "-");
		}

	    }
	    writer.write(LATEX_NEWLINE_CODE + "\n");
	    writer.write(LATEX_HLINE_CODE + "\n");
	}
	writeLatexTableFooter(writer, captionText);
    }

    public void writeMatchingWeights(Writer writer) throws IOException {
	writer.write("\n\n (Mean) Coarse Match Weight: " + NUMBER_FORMAT.format(getMeanCoarseMatchWeigth()) + LATEX_NEWLINE_CODE
		+ "\n");
	writer.write("(Mean) Coarse No Match Weight: " + NUMBER_FORMAT.format(getMeanCoarseNoMatchWeigth()) + LATEX_NEWLINE_CODE
		+ "\n");
    }
    
    private void writeCompensatedFeaturesStatistics(DoubleListStatistics doubleListStatistics, Writer writer) throws IOException{
	writer.write("\n\n Mean : " + NUMBER_FORMAT.format(doubleListStatistics.getMean()) + LATEX_NEWLINE_CODE);
	writer.write("\n Standard Deviation : " + NUMBER_FORMAT.format(doubleListStatistics.getStd()) + LATEX_NEWLINE_CODE);
	writer.write("\n Min : " + NUMBER_FORMAT.format(doubleListStatistics.getMin()) + LATEX_NEWLINE_CODE);
	writer.write("\n Max : " + NUMBER_FORMAT.format(doubleListStatistics.getMax()) + LATEX_NEWLINE_CODE
		+ "\n");
    }
    
    public void writeCompensatedMatchingFeaturesStatistics(Writer writer) throws IOException
    {
	writer.write("\n Compensated Match features Statistics:" + LATEX_NEWLINE_CODE
		+ "\n");
	writeCompensatedFeaturesStatistics(getCompensatedMatchingFeaturesStatistics(), writer);
    }
    
    public void writeCompensatedMisMatchingFeaturesStatistics(Writer writer) throws IOException
    {
	writer.write("\n Compensated NoMatch features Statistics:" + LATEX_NEWLINE_CODE
		+ "\n");
	writeCompensatedFeaturesStatistics(getCompensatedMisMatchingFeaturesStatistics(), writer);
    }
    
    public void writeDifferenceMeanMatchAndMismatchWeights(Writer writer) throws IOException
    {
	double meanMatchWeight = getCompensatedMatchingFeaturesStatistics().getMean();
	double meanMismatchWeight = getCompensatedMisMatchingFeaturesStatistics().getMean();
	double difference = meanMatchWeight - meanMismatchWeight;
	
	writer.write("\n avg(Match) - avg(NoMatch): " + NUMBER_FORMAT.format(difference)  + LATEX_NEWLINE_CODE
		+ "\n");
    }
    

    public void writeRawWeightsTable() {
	writeTable(new BasicWeightComputer(this));
    }

    public void writeMatchingFeatureCompensatedWeightsTable() {
	writeTable(new MatchFeatureCompensatedWeightComputer(this));
    }

    public void createAndWriteTable(Writer outputWriter) throws IOException {
	addWeightsFromFileLineIterators();
	writeMatchingWeights(outputWriter);
	System.out.println("Raw weights table: \n\n");
	writeRawWeightsTable();
	System.out.println("Matching feature compensated weights table: \n\n");
	writeMatchingFeatureCompensatedWeightsTable();

	System.out.println("\n\n\n");
	writeLatexTable(new BasicWeightComputer(this), RAW_SCORES_TABLE_CAPTION_TEXT, outputWriter);

	
	// Write mean, std, min and max value of compensated feature weights
	writeCompensatedMatchingFeaturesStatistics(outputWriter);
	writeCompensatedMisMatchingFeaturesStatistics(outputWriter);
	writeDifferenceMeanMatchAndMismatchWeights(outputWriter);
	
	System.out.println("\n\n\n");
	writeLatexTable(new MatchFeatureCompensatedWeightComputer(this),
		COMPENSATED_SCORES_TABLE_CAPTION_TEXT, outputWriter);
    }

    

    protected static abstract class FeatureWeightComputer {
	protected final LabelSubstitutionFeatureWeightTableConstructer labelSubstitutionFeatureWeightTableConstructer;

	protected FeatureWeightComputer(
		LabelSubstitutionFeatureWeightTableConstructer labelSubstitutionFeatureWeightTableConstructer) {
	    this.labelSubstitutionFeatureWeightTableConstructer = labelSubstitutionFeatureWeightTableConstructer;
	}

	protected abstract double getWeight(String fromLabel, String toLabel);
    }

    private static class BasicWeightComputer extends FeatureWeightComputer {

	protected BasicWeightComputer(
		LabelSubstitutionFeatureWeightTableConstructer labelSubstitutionFeatureWeightTableConstructer) {
	    super(labelSubstitutionFeatureWeightTableConstructer);
	}

	@Override
	protected double getWeight(String fromLabel, String toLabel) {
	    return this.labelSubstitutionFeatureWeightTableConstructer
		    .getMeanSubstitutionFeatureValue(fromLabel, toLabel);
	}

    }

    private static class MatchFeatureCompensatedWeightComputer extends FeatureWeightComputer {

	protected MatchFeatureCompensatedWeightComputer(
		LabelSubstitutionFeatureWeightTableConstructer labelSubstitutionFeatureWeightTableConstructer) {
	    super(labelSubstitutionFeatureWeightTableConstructer);
	}

	@Override
	protected double getWeight(String fromLabel, String toLabel) {
	    List<Double> compensatedFeatureValues = this.labelSubstitutionFeatureWeightTableConstructer.getCompensatedSubstitutionFeatureValues(fromLabel, toLabel);
	    return MeanStdComputation.computeDoublesMean(compensatedFeatureValues);
	}

    }

    public static void writeLatexDocumentHeader(Writer writer, String authorName, String title)
	    throws IOException {
	writer.write("\\documentclass[a4paper,10pt]{article}" + "\n"
		+ "\\usepackage[utf8]{inputenc}" + "\n" + "\\usepackage{multirow}" + "\n"
		+ "\\usepackage{adjustbox}" + "\n" + "\n" +

		"\\title{" + title + "}" + "\n" + "\\author{" + authorName + "}" + "\n"
		+ "\\begin{document}" + "\n" + "\\maketitle" + "\n");
    }

    private static void writeLatexDocumentFooter(Writer writer) throws IOException {
	writer.write("\n\n\\end{document}");
    }

    private static void writeLatexTableDocument(
	    LabelSubstitutionFeatureWeightTableConstructer labelSubstitutionFeatureWeightTableConstructer,
	    Writer outputWriter) throws IOException {
	writeLatexDocumentHeader(outputWriter, AUTHOR_NAME_STRING, DOCUMENT_TITLE_STRING);
	labelSubstitutionFeatureWeightTableConstructer.createAndWriteTable(outputWriter);
	writeLatexDocumentFooter(outputWriter);

    }

    private static void createTableDocument(String configFilePath) {
	ConfigFile theConfig;
	BufferedWriter outputWriter = null;
	try {
	    theConfig = new ConfigFile(configFilePath);
	    List<String> weightFilePaths = theConfig
		    .getMultiValueParmeterAsListWithPresenceCheck(WEIGHT_FILE_PATHS_PROPERTY);
	    String outputFilePath = theConfig
		    .getValueWithPresenceCheck(WEIGHTS_TABLE_OUTPUT_FILE_PATH_PROPERTY);
	    LabelSubstitutionFeatureWeightTableConstructer labelSubstitutionFeatureWeightTableConstructer = LabelSubstitutionFeatureWeightTableConstructer
		    .createLabelSubstitutionFeatureWeightTableConstructer(weightFilePaths);
	    outputWriter = new BufferedWriter(new FileWriter(outputFilePath));
	    writeLatexTableDocument(labelSubstitutionFeatureWeightTableConstructer, outputWriter);

	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	} finally {
	    FileUtil.closeCloseableIfNotNull(outputWriter);
	}

    }
    
    

    public static void main(String[] args) {
	if (args.length != 1) {
	    System.out
		    .println("Usage: labelSubstitutionFeatureWeightTableConstructor CONFIG_FILE_PATH");
	    System.exit(1);
	}
	createTableDocument(args[0]);

    }
}
