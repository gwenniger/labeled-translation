package mt_pipeline;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import mt_pipeline.mt_config.MTConfigFile;

public class JoshuaSystem extends MTSystem {
	public static String SYSTEM_TYPE = "Joshua";

	protected JoshuaSystem(MTConfigFile theConfig, DecoderConfigCreater decoderConfigCreater, SystemIdentity systemIdentity) {
		super(theConfig, decoderConfigCreater, systemIdentity);
	}

	public static JoshuaSystem createJoshuaSystem(MTConfigFile theConfig, String unknownWordsLabel, SystemIdentity systemIdentity) {
		return new JoshuaSystem(theConfig, JoshuaConfigCreater.createJoshuaConfigCreater(theConfig, unknownWordsLabel), systemIdentity);
	}

	public static String getSystemTypeName() {
		return SYSTEM_TYPE;
	}

}
