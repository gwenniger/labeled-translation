package mt_pipeline;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import mt_pipeline.grammarExtraction.ExternalProcessGrammarCreaterSAMT;
import mt_pipeline.mt_config.MTConfigFile;

public class MTPipelineHATsSAMT extends MTPipelineHatsComplexLabels {
	public static final String SystemName = "JoshuaHatsSAMT";

	protected MTPipelineHATsSAMT() {
	};

	protected MTPipelineHATsSAMT(MTConfigFile theConfig, String configFilePath, int noModelParameters, boolean extractGrammarsAsExternalProcess, MTSystem mtSystem) {
		super(theConfig, configFilePath, noModelParameters, extractGrammarsAsExternalProcess, mtSystem);
	}

	public static MTPipeline createMTPipelineHatsSAMT(String configFilePath) throws RuntimeException {
		MTConfigFile theConfig = MTConfigFile.createMtConfigFile(configFilePath, SystemName);
		SystemIdentity systemIdentity = SystemIdentity.createSystemIdentity(theConfig);
		MTSystem mtSystem = MTSystem.createMTSystem(theConfig,systemIdentity);
		return new MTPipelineHATsSAMT(MTConfigFile.createMtConfigFile(configFilePath, SystemName), configFilePath, mtSystem.noModelParameters(theConfig,SystemName),
				MTPipeline.extractGrammarsAsExternalProcess(configFilePath), mtSystem);
	}

	@Override
	protected void extractGrammars() {
		long startTime = System.currentTimeMillis();

		if (extractGrammarAsExternalProcess()) {
			ExternalProcessGrammarCreaterSAMT.extractSAMTGrammarsAsExternalProcess(getConfigFilePath());
		} else {
			GrammarCreaterHats.extractFilteredHatsSAMTGrammarsPrallel(getTheConfig(),getSystemIdentity());
		}
		long passedTime = (System.currentTimeMillis() - startTime) / 1000;
		System.out.println(">>> HATs grammar extraction took: " + passedTime + "seconds");
	}

	@Override
	public MTPipeline createMTPipeline(String configFilePath) throws RuntimeException {
		return createMTPipelineHatsSAMT(configFilePath);
	}

}
