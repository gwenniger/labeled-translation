package mt_pipeline;

import grammarExtraction.chiangGrammarExtraction.HieroGlueGrammarCreater;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import mt_pipeline.mt_config.MTConfigFile;

public class MTPipelineJoshuaPure extends MTPipeline {
	protected static final String SystemName = "JoshuaPureHiero";

	protected MTPipelineJoshuaPure() {
	};

	private MTPipelineJoshuaPure(MTConfigFile theConfig, String configFilePath, int noModelParameters, MTSystem mtSystem) {
		super(theConfig, configFilePath, noModelParameters, false, mtSystem);
	}

	public static MTPipeline createMTPipelineJoshuaPure(String configFilePath) throws RuntimeException {
		MTConfigFile theConfig = MTConfigFile.createMtConfigFile(configFilePath, SystemName);
		SystemIdentity systemIdentity = SystemIdentity.createSystemIdentity(theConfig);
		MTSystem mtSystem = JoshuaSystem.createMTSystem(theConfig,systemIdentity);
		return new MTPipelineJoshuaPure(theConfig, configFilePath, 3, mtSystem);
	}

	@Override
	protected void extractGrammars() {
		MTDataFilesCreater.createJoshuaPrefixTable(getTheConfig(),getSystemIdentity());
		GrammarCreaterJoshuaPure.extractFilteredPureJoshuaGrammarsPrallel(getTheConfig());
	}

	@Override
	public MTPipeline createMTPipeline(String configFilePath) throws RuntimeException {
		return createMTPipelineJoshuaPure(configFilePath);
	}

	@Override
	protected String getUnknownWordsLabel() {
		return HieroGlueGrammarCreater.ChiangLabel;
	}

}