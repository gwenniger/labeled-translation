package mt_pipeline;

import mt_pipeline.mt_config.MTConfigFile;

public class JoshuaDecoderCommandCreater extends DecoderCommandCreater {

	protected JoshuaDecoderCommandCreater(MTConfigFile theConfig) {
		super(theConfig);
	}
	
	public static JoshuaDecoderCommandCreater createJoshuaDecoderCommandCreater(MTConfigFile theConfig)
	{
		return new JoshuaDecoderCommandCreater(theConfig);
	}

	@Override
	public String getDecodeCommand(String category, String configFilePath, boolean useTunedParameters) {
		return joshuaDecodeCommand(category, configFilePath, useTunedParameters);
	}

	private String joshuaDecodeCommand(String category, String configFilePath, boolean useTunedParameters) {
		String result = "";
		result += theConfig.decoderConfig.joshuaDecoderCallWithConfigFileSpecification(configFilePath) + " < " + theConfig.filesConfig.getEnrichedSourceFilePath(category)
				+ DecoderInterface.joshuaDecodeOutputSpecification(theConfig, category, useTunedParameters);
		// result += theConfig.baseJoshuaJavaCall() + " -Djava.library.path="+
		// theConfig.getJoshuaLibDir() + " joshua.decoder.JoshuaDecoder " +
		// theConfig.getJoshuaTestConfigFileName() + " " +
		// theConfig.getSourcePreprocessedFileName(MTConfigFile.TestCategory)
		// +"  " + theConfig.getDecoderOutputFilePath(category);
		return result;
	}

}
