package mt_pipeline;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import mt_pipeline.mt_config.MTConfigFile;

public abstract class DecoderConfigCreater extends DecoderFilesCreaterBase {

	public DecoderConfigCreater(MTConfigFile theConfig, SystemIdentity systemIdentity) {
		super(theConfig,systemIdentity);
	}

	public abstract void createTunedTestConfig(MTConfigFile theConfig); 
	public abstract void writeMertConfigFile(MTConfigFile theConfig);
	public abstract void writeTestConfigFile(MTConfigFile theConfig); 
	protected abstract String getSystemTypeName();
	
	protected String getMainHeader() {
		String result = "";
		result += NL + "######################################################################";
		result += NL + "################ THIS IS AN AUTOMATICALLY GENERATED ##################";
		result += NL + "################        "+ getSystemTypeName() + " CONFIG FILE          ##################";
		result += NL + "######################################################################";
		result += NL + NL;
		return result;
	}

	protected String getStaticHeader() {
		String result = NL;
		result += NL + "#######################################################";
		result += NL + "####### Begin Static part Configuration file ##########";
		result += NL + "#######################################################";
		return result;
	}

	protected String getDynamicHeader() {
		String result = NL;
		result += NL + "#######################################################";
		result += NL + "####### Begin Dynamic part Configuration file #########";
		result += NL + "#######################################################";
		return result;
	}
	
}
