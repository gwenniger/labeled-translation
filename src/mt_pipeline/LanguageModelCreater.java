package mt_pipeline;

import util.FileUtil;
import util.LinuxInteractor;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import mt_pipeline.mt_config.MTConfigFile;
import mt_pipeline.mt_config.MTDecoderConfig;
import mt_pipeline.mt_config.MTFilesConfig;
import mt_pipeline.mt_config.MTFoldersConfig;

public class LanguageModelCreater extends MTComponentCreater {

	private final SystemIdentity systemIdentity;

	private LanguageModelCreater(MTConfigFile theConfig, SystemIdentity systemIdentity) {
		super(theConfig);
		this.systemIdentity = systemIdentity;
	}

	private static LanguageModelCreater createLanguageModelCreater(MTConfigFile theConfig, SystemIdentity systemIdentity) {
		return new LanguageModelCreater(theConfig, systemIdentity);
	}

	public static void createLM(MTConfigFile theConfig, SystemIdentity systemIdentity) {
		LanguageModelCreater lmc = createLanguageModelCreater(theConfig, systemIdentity);
		lmc.extractArpaLM();
	}

	protected String getSrilmBinDir() {
		if (theConfig.hasValue(MTFoldersConfig.SRILM_BIN_DIR_PROPERTY)) {
			return theConfig.getValue(MTFoldersConfig.SRILM_BIN_DIR_PROPERTY);
		}
		throw new RuntimeException("getSrilmBindDir() : " + MTFoldersConfig.SRILM_BIN_DIR_PROPERTY + " unspecified in config file");
	}

	protected String getLMInputFile() {
		if (theConfig.hasValue(MTFilesConfig.LM_INPUT_FILE_PROPERTY)) {
			return theConfig.getValue(MTFilesConfig.LM_INPUT_FILE_PROPERTY);
		}

		throw new RuntimeException("getLMInputFile() :  lmInputFile unspecified in config file" +
		"\nplease specify language model input file with " + MTFilesConfig.LM_INPUT_FILE_PROPERTY +
		"\nor alternatively specify path to externally generated language model with " + MTFilesConfig.LM_FILE_EXTERNALLY_GENERATED_PATH_PROPERTY);
	}

	private String getKenLMHome() {
		if (systemIdentity.isJoshuaSystem()) {
			return theConfig.decoderConfig.getJoshuaHomeDir() + "ext/kenlm/build/bin/";
		} else if (systemIdentity.isMosesSystem()) {
			return theConfig.decoderConfig.getMosesHomeDir() + "bin/";
		} else {
			throw new RuntimeException("Erroc : LanguageModelCreater.getKenLMHome() : unknown system type (neither Moses nor Joshua)");
		}
	}

	
	private String smoothingParamter(){
	    if(theConfig.decoderConfig.getLanguageModelOrder() > 1){
		return "  -kndiscount";
	    }
	    return "";
	}
	
	
	private boolean useKenLM(){
	    return theConfig.decoderConfig.getLanguageModelName().equals(MTDecoderConfig.KENLM_NAME);
	}
	
	private void extractArpaLMKenLM(){
		String command = getSrilmBinDir() + "ngram-count -interpolate -order " + theConfig.decoderConfig.getLanguageModelOrder() +smoothingParamter() + " -text " + this.getLMInputFile() + " -lm " + this.theConfig.filesConfig.getArpaLanguageModeFilePath();
		LinuxInteractor.executeCommand(command, true);
	}
	
	
	private void extractArpaLM() {
		// String srilmCommand = getSrilmBinDir() +
		// "ngram-count -interpolate -order 3  -kndiscount -text " +
		// this.trainFileName +" -lm " + this.resultFileName;
		// String[] commandAndArgs = new String[]{getSrilmBinDir() +
		// "ngram-count", "-interpolate", "-order 3", "-kndiscount", "-text",
		// this.trainFileName ," -lm ", this.resultFileName};
		// String[] commandAndArgs = new String[]{getSrilmBinDir() +
		// "ngram-count", " -interpolate -order 3 -kndiscount -text " +
		// this.trainFileName + " -lm " + this.resultFileName};
		// String srilmCommand = getSrilmBinDir() + "ngram-count";
		// String[] commandAndArgs = new String[]{this.scriptsDir +
		// "buildBinaryLM.sh",this.joshuaHome, this.srilmHome,
		// this.trainFileName, this.resultFileName};
		// executeExternalCommand(command);
	    	extractArpaLMKenLM();

	    	String command;
		if(useKenLM()){
		    command = this.getKenLMHome() + "build_binary " + this.theConfig.filesConfig.getArpaLanguageModeFilePath() + " " + this.theConfig.filesConfig.getBinaryLanguageModelFilePath();
		    LinuxInteractor.executeCommand(command, true);
		}
		else{
		    // If we have a unigram language model, we just use it in the raw form, since the 
		    // KenLM build binary will crash on it
		    FileUtil.copyFileNative(this.theConfig.filesConfig.getArpaLanguageModeFilePath(),this.theConfig.filesConfig.getBinaryLanguageModelFilePath(), true);
		}
	}

}
