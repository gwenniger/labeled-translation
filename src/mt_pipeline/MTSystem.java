package mt_pipeline;

import grammarExtraction.chiangGrammarExtraction.RuleFeaturesBasic;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.samtGrammarExtraction.LabeledRuleFeaturesSet;
import grammarExtraction.samtGrammarExtraction.RuleFeaturesStandard;
import mt_pipeline.mt_config.MTConfigFile;

public abstract class MTSystem {
	protected final MTConfigFile theConfig;
	protected final DecoderConfigCreater decoderConfigCreater;
	protected final SystemIdentity systemIdentity;

	protected MTSystem(MTConfigFile theConfig, DecoderConfigCreater decoderConfigCreater, SystemIdentity systemIdentity) {
		this.theConfig = theConfig;
		this.decoderConfigCreater = decoderConfigCreater;
		this.systemIdentity = systemIdentity;
	}

	public static MTSystem createMTSystem(MTConfigFile theConfig,SystemIdentity systemIdentity) {
		return theConfig.systemSpecificConfig.createMTSystem(theConfig, systemIdentity.getUnknownWordsLabel(),systemIdentity);
	}

	public void createTunedTestConfig(MTConfigFile theConfig) {
		decoderConfigCreater.createTunedTestConfig(theConfig);
	}

	public void writeDecodingConfigFiles() {
		decoderConfigCreater.writeMertConfigFile(theConfig);
		decoderConfigCreater.writeTestConfigFile(theConfig);
	}

	public SystemIdentity getSystemIdentity() {
		return this.systemIdentity;
	}
	
	public int noModelParameters(MTConfigFile theConfig, String systemIdentity) {
		int noModelParameters = RuleFeaturesBasic.getNumLexicalPlusPhraseProbabilityFeatures(theConfig,systemIdentity);

		if (RuleFeaturesStandard.useExtraSAMTFeatures(theConfig)) {
			noModelParameters += LabeledRuleFeaturesSet.noExtraLabeledRuleFeatures();
		}
		return noModelParameters;
	}

}
