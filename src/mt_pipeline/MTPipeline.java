package mt_pipeline;

import grammarExtraction.grammarExtractionFoundation.SystemIdentity;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import util.ConfigFile;
import util.FileUtil;
import alignmentStatistics.MultiThreadCorpusDataGenerator;
import mt_pipeline.mt_config.MTConfigFile;
import mt_pipeline.parserInterface.MetaDataAnnotatorInterface;
import mt_pipeline.parserInterface.MetaDataAnnotatorProcess;
import mt_pipeline.parserInterface.ParserInterface;
import mt_pipeline.preAndPostProcessing.EnrichedSourceFileCreatorFactory;
import mt_pipeline.taggerInterface.TnTTaggerInterface;
import mt_pipeline.evaluation.EvaluationInterface;
import mt_pipeline.evaluation.ScoreSummaryFileCreater;

public abstract class MTPipeline implements MTPipelineCreater {
	public static final String EXECUTION_SCHEME_PROPERTY = "executionScheme";
	public static final String EXECUTE_EVERYTHING_STRING = "executeEverything";
	public static final String EXECUTE_MERT_AND_EVALUATION = "executeMertAndEvaluation";
	public static final String EXECUTE_EVALUATION_ONLY = "executeEvaluationOnly";
	public static final String EXECUTE_RERANKING_AND_SCORING_ONLY = "executeRerankingAndScoringOnly";
	private static final String EXECUTE_SCORING_ONLY = "executeScoringOnly";
	private static final String EXECUTE_GRAMMAR_EXTRACTION_AS_EXTERNAL_PROCESS_PROPERTY = "executeGrammarExtractionAsExternalProcess";
	private static final List<String> ExecutionOptions = Arrays.asList(
			EXECUTE_EVERYTHING_STRING, EXECUTE_MERT_AND_EVALUATION,
			EXECUTE_EVALUATION_ONLY, EXECUTE_RERANKING_AND_SCORING_ONLY,
			EXECUTE_SCORING_ONLY);

	private final String configFilePath;
	private final MTConfigFile theConfig;
	private final int noModelParameters;
	private final boolean extractGrammarsAsExternalProcess;
	private final MTSystem mtSystem;


	protected MTPipeline() {
		this.configFilePath = null;
		this.theConfig = null;
		this.noModelParameters = -1;
		this.extractGrammarsAsExternalProcess = false;
		this.mtSystem = null;
	};

	protected MTPipeline(MTConfigFile theConfig, String configFilePath,
			int noModelParameters, boolean extractGrammarsAsExternalProcess,
			MTSystem mtSystem) {
		this.theConfig = theConfig;
		this.configFilePath = configFilePath;
		this.noModelParameters = noModelParameters;
		this.extractGrammarsAsExternalProcess = extractGrammarsAsExternalProcess;
		this.mtSystem = mtSystem;
		theConfig.checkLanguageAbbreviationConsistency();
	}

	private void runTuning() {
		// if (this.mtSystem.getSystemIdentity().isJoshuaSystem()) {
		DecoderInterface joshuaDecoderInterface = DecoderInterface
				.createDecoderInterface(getTheConfig(), mtSystem);
		joshuaDecoderInterface.runTuning();
		// }
	}

	private void runDecodingAndEvaluation() {
		DecoderInterface joshuaDecoderInterface = DecoderInterface
				.createDecoderInterface(getTheConfig(), mtSystem);
		// Create a Tuned Joshua Config for the TestSet
		if (MTConfigFile.performTuning(theConfig)) {
			mtSystem.createTunedTestConfig(getTheConfig());
		}
		// Perform the decoding of the test set
		joshuaDecoderInterface.decodeDevAndTestSentences((MTConfigFile.performTuning(theConfig)));
		/*
		 * try { Thread.sleep(10000); } catch (InterruptedException e) {
		 * e.printStackTrace(); }
		 */
		  
		 runRerankingAndScoringOnly();
	}

	private void runRerankingAndScoringOnly() {
		// Create result folders in case these have been renamed/removed
		FileUtil.createFolderIfNotExisting(theConfig.foldersConfig
				.getResultsFolderCategory(MTConfigFile.DEV_CATEGORY));
		FileUtil.createFolderIfNotExisting(theConfig.foldersConfig
				.getResultsFolderCategory(MTConfigFile.TEST_CATEGORY));
		DecoderInterface joshuaDecoderInterface = DecoderInterface
				.createDecoderInterface(getTheConfig(), mtSystem);
		joshuaDecoderInterface.rerankDevAndTestSentences();
		runScoringOnly();
	}

	private void runScoringOnly() {
		// Perform evaluation
		EvaluationInterface evaluationInterface = EvaluationInterface
				.createEvaluationInterface(getTheConfig());
		evaluationInterface.performEvaluation();
	}

	private void removeAnyOldMertAndTestFolderAndResultFilesAndCreateEmptyFolders() {
		// Remove the old mert and results directory, so we will not get no
		// problems with old files

		if (theConfig.extractTestGrammar()) {
			// We only remove the test directory if we are going to (re) extract
			// the test grammar
			FileUtil.removeDirectory(new File(getTheConfig().foldersConfig
					.getIntermediateTestFolder()));
		}
		if (theConfig.extractDevGrammar()) {
			// We only remove the mert directory if we are going to (re) extract
			// the dev grammar
			FileUtil.removeDirectory(new File(getTheConfig().foldersConfig
					.getMertFolder()));
		}
		FileUtil.removeDirectory(new File(getTheConfig().foldersConfig
				.getResultsBaseFolder()));
		
		saveUsedConfig();
		
		// Recreate the mert and test folders
		FileUtil.createFolderIfNotExisting(getTheConfig().foldersConfig
				.getMertFolder());
		FileUtil.createFolderIfNotExisting(getTheConfig().foldersConfig
				.getIntermediateTestFolder());
	}

	private List<MetaDataAnnotatorInterface<? extends MetaDataAnnotatorProcess<?>>> createMetaDataAnnotators() {
		List<MetaDataAnnotatorInterface<? extends MetaDataAnnotatorProcess<?>>> result = new ArrayList<MetaDataAnnotatorInterface<? extends MetaDataAnnotatorProcess<?>>>();
		result.addAll(ParserInterface.createParsers(theConfig));
		result.addAll(TnTTaggerInterface.createTaggers(theConfig));
		return result;
	}

	private void runDataCreation() {

		removeAnyOldMertAndTestFolderAndResultFilesAndCreateEmptyFolders();

		MTDataFilesCreater mtDataFilesCreater = new MTDataFilesCreater(
				getTheConfig(), createMetaDataAnnotators(),
				EnrichedSourceFileCreatorFactory
						.getSourceSubFileCreaterBasedOnConfig(getTheConfig()),
				mtSystem);
		mtDataFilesCreater.createSharedDataFiles();
		extractGrammars();
	}

	protected abstract void extractGrammars();

	private void runMertAndDecodingAndEvaluation() {
		MTDataFilesCreater mtDataFilesCreater = new MTDataFilesCreater(
				getTheConfig(), createMetaDataAnnotators(),
				EnrichedSourceFileCreatorFactory
						.getSourceSubFileCreaterBasedOnConfig(getTheConfig()),
				mtSystem);
		// Moved this inside the mert/evaluation step to assure these files are
		// generated anew whenever
		// that step is ran
		mtDataFilesCreater
				.createSystemSpecificFilesMertAndEvaluation(noModelParameters);
		runTuning();
		runDecodingAndEvaluation();
	}

	private void runEverything() {
		runDataCreation();
		runMertAndDecodingAndEvaluation();
	}

	private void checkValidityExecutionScheme(String executionScheme) {
		if (!ExecutionOptions.contains(executionScheme)) {
			System.out
					.println("\n>>>Make sure you chose a valid parameter for executionScheme");
			System.out.println("Valid values are:");
			for (String executionString : ExecutionOptions) {
				System.out.println("executionScheme = " + executionString);
			}
			System.exit(1);

		}
	}

	private void saveUsedConfig() {
		FileUtil.createFolderIfNotExisting(getTheConfig().foldersConfig
				.getSystemOutputFolderName());
		FileUtil.createFolderIfNotExisting(getTheConfig().foldersConfig.getResultsBaseFolder());
		System.out.println(">>>Saving config to : " + getTheConfig().filesConfig.getSavedConfigFilePath());
		FileUtil.copyFile(configFilePath,
				getTheConfig().filesConfig.getSavedConfigFilePath());
	}

	protected String getConfigFilePath() {
		return this.configFilePath;
	}

	private void executePipeline() {

		String executionScheme = getTheConfig()
				.getStringIfPresentOrReturnDefault(EXECUTION_SCHEME_PROPERTY,
						"");

		checkValidityExecutionScheme(executionScheme);


		if (executionScheme.equals(EXECUTE_EVERYTHING_STRING)) {
			runEverything();
		} 
		else 
		{    
		    saveUsedConfig();
		    if (executionScheme.equals(EXECUTE_MERT_AND_EVALUATION)) {
			runMertAndDecodingAndEvaluation();
		    } else if (executionScheme.equals(EXECUTE_EVALUATION_ONLY)) {
			runDecodingAndEvaluation();
		    } else if (executionScheme.equals(EXECUTE_RERANKING_AND_SCORING_ONLY)) {
			runRerankingAndScoringOnly();
		    } else if (executionScheme.equals(EXECUTE_SCORING_ONLY)) {
			runScoringOnly();
		    } else {
			System.out
					.println("No execution call available for this execution scheme");
		    }
		}    
	}

	public static boolean runPipelinesForConfig(String configFileName) {
		List<MTPipeline> mtPipelines = createMTPipelinesListFromConfigFile(configFileName);
		executePipelines(mtPipelines);
		try {
		    if(MTConfigFile.performTuning(new ConfigFile(configFileName))){    
		        writeSummarizedResults(mtPipelines);
		    }
		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		    throw new RuntimeException(e);
		}    
		return true;
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			for (String arg : args) {
				System.out.println("arg: " + arg);
			}

			System.out.println("Usage: java -jar mtPipeline ConfigFileName");
			System.exit(1);
		}
		String configFileName = args[0];
		runPipelinesForConfig(configFileName);
		System.exit(0);
	}

	private static void writeSummarizedResults(List<MTPipeline> mtPipelines) {
		ScoreSummaryFileCreater scoreSummaryFileCreater = ScoreSummaryFileCreater
				.createScoreSummaryFileCreater(mtPipelines);
		scoreSummaryFileCreater.writeSummarizedResults();
	}

	private static void executePipelines(List<MTPipeline> mtPipelines) {
		for (MTPipeline pipeline : mtPipelines) {
			pipeline.executePipeline();
		}
	}

	private static Map<String, MTPipelineCreater> createMTPipelineCreaterMap() {
		Map<String, MTPipelineCreater> result = new HashMap<String, MTPipelineCreater>();
		result.put(MTPipelineJoshuaPure.SystemName, new MTPipelineJoshuaPure());
		result.put(MTPipelineHatsHiero.SystemName, new MTPipelineHatsHiero());
		result.put(MTPipelineHATsSAMT.SystemName, new MTPipelineHATsSAMT());
		result.put(MTPipelineHatsCCL.SystemName, new MTPipelineHatsCCL());
		result.put(MTPipelineHatsBoundaryTagLabeled.SystemName,
				new MTPipelineHatsBoundaryTagLabeled());
		return result;
	}

	private static List<MTPipeline> createMTPipelinesListFromConfigFile(
			String configFileName) {
		List<MTPipeline> result = new ArrayList<MTPipeline>();

		try {
			ConfigFile theConfig = new ConfigFile(configFileName);

			List<String> systemNames = theConfig
					.getMultiValueParmeterAsList(MTConfigFile.SYSTEM_NAMES_PROPERTY);
			Map<String, MTPipelineCreater> mtPipelineCreaterMap = createMTPipelineCreaterMap();

			if (systemNames != null) {
				for (String systemName : systemNames) {
					if (mtPipelineCreaterMap.containsKey(systemName)) {
						result.add(mtPipelineCreaterMap.get(systemName)
								.createMTPipeline(configFileName));
					} else {
						printUnknownSystemMessage(systemName);
						throw new RuntimeException(
								"Error: Unrecognized system name");
					}
				}
			} else {
				throw new RuntimeException("Please make sure the parameter "
						+ MTConfigFile.SYSTEM_NAMES_PROPERTY
						+ " specifies a valid list of sytem names");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return result;
	}

	private static void printUnknownSystemMessage(String systemName) {
		System.out
				.println("The system named \"" + systemName + "\" is unknows");
		System.out
				.println("Please make sure the list \"systems\" in the configuration file contains only elements from the following known systems list:");
		for (String knownSystemName : knownSystemNames()) {
			System.out.println(knownSystemName);
		}
	}

	private static List<String> knownSystemNames() {
		List<String> result = new ArrayList<String>();
		result.addAll(createMTPipelineCreaterMap().keySet());
		return result;
	}

	public static <T extends Callable<T>> List<T> performThreadComputation(
			List<T> threads, int numThreads) {
		List<Future<T>> registeredTasks = new ArrayList<Future<T>>();
		ExecutorService executorService = Executors
				.newFixedThreadPool(numThreads);

		for (T tret : threads) {
			Future<T> executingTask = executorService.submit(tret);
			registeredTasks.add(executingTask);
		}
		// Wait until all extractor threads to finish
		List<T> results = MultiThreadCorpusDataGenerator
				.joinThreads(registeredTasks);
		
		// See: http://stackoverflow.com/questions/20057497/program-does-not-terminate-immediately-when-all-executorservice-tasks-are-done
		try {
			executorService.shutdown();
			boolean terminated = executorService.awaitTermination(1, TimeUnit.SECONDS);
			System.out.println("terminated: " + terminated);			
		} catch (InterruptedException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
		System.out.println("\n\n Threads finished \n\n");

		return results;
	}

	protected String getUnknownWordsLabel() {
		return this.mtSystem.getSystemIdentity().getUnknownWordsLabel();
	}

	public MTConfigFile getTheConfig() {
		return theConfig;
	}

	public boolean extractGrammarAsExternalProcess() {
		return this.extractGrammarsAsExternalProcess;
	}

	public static boolean extractGrammarsAsExternalProcess(String configFilePath) {
		ConfigFile theConfig;
		try {
			theConfig = new ConfigFile(configFilePath);
			return Boolean
					.parseBoolean(theConfig
							.getStringIfPresentOrReturnDefault(
									EXECUTE_GRAMMAR_EXTRACTION_AS_EXTERNAL_PROCESS_PROPERTY,
									"true"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	public SystemIdentity getSystemIdentity() {
		return this.mtSystem.getSystemIdentity();
	}

}
