package mt_pipeline;

import util.Pair;
import grammarExtraction.minimalPhrasePairExtraction.MultiThreadGrammarExtractorMinimalPhrasePairs;
import mt_pipeline.mt_config.MTConfigFile;
import mt_pipeline.parserInterface.ParserInterface;
import coreContextLabeling.CoreContextLabelsTable;
import coreContextLabeling.EbitgChartBuilderCoreContextLabeled;
import alignment.ConfigFileCreater;

public class CoreContextLabelsTableExtracter {
	private static final String CoreContextLabelsTableExtracterConfigFileName = "CoreContextLabelsExtracterConfigFile.txt";

	public static String writeCoreContextLabelsTableExtractionConfigFileAndReturnPath(MTConfigFile configFile) {

		// We use the same data file that are used for rule extraction also for
		// extraction of the core-context-labels-table
		// The alternative is to use all the data, but this has the disadvantage
		// of being somewhat unclear and introducing
		// core context labels that are never needed, but nevertheless
		// potentially take the place (in rank) of other labels that are needed
		String trainCorpusLocation = configFile.foldersConfig.getCategoryDataFolderName(MTConfigFile.TRAIN_CATEGORY);
		String configFilePath = configFile.foldersConfig.getResourcesFolder() + CoreContextLabelsTableExtracterConfigFileName;
		String sourceFileName = configFile.filesConfig.getSourceSubFileName();
		String targetFileName = configFile.filesConfig.getTargetSubFileName();
		String alignmentFileName = configFile.filesConfig.getAlignmentsSubFileName();
		ConfigFileCreater configFileCreater = ConfigFileCreater.createConfigFileCreater(trainCorpusLocation, sourceFileName, targetFileName, alignmentFileName, configFilePath);

		addCoreContextLabelTableExtractionRequiredConfigurationProperties(configFile, configFileCreater);

		configFileCreater.writeConfigFile();
		return configFilePath;
	}

	private static void addCoreContextLabelTableExtractionRequiredConfigurationProperties(MTConfigFile configFile, ConfigFileCreater configFileCreater) {
		String coreContextLabelTableFileName = CoreContextLabelsTable.getCoreContextLabelsTableFileName(configFile);
		configFileCreater.addProperty(EbitgChartBuilderCoreContextLabeled.CoreContextLabelTableFileNameConfigurationProperty, coreContextLabelTableFileName);
		Pair<String> coreContextLabelProbabilitySettingPropertyPair = configFile.getConfigPairWithPresenceCheck(CoreContextLabelsTable.CoreContextLabelProbabilitySettingProperty);
		configFileCreater.addProperty(coreContextLabelProbabilitySettingPropertyPair.getFirst(), coreContextLabelProbabilitySettingPropertyPair.getSecond());
		Pair<String> coreContextLabelRelativeEntropyWeightPropertyPair = configFile.getConfigPairWithPresenceCheck(CoreContextLabelsTable.RelativeEntropyWeightCoreContextLabelsProperty);
		configFileCreater.addProperty(coreContextLabelRelativeEntropyWeightPropertyPair.getFirst(), coreContextLabelRelativeEntropyWeightPropertyPair.getSecond());
	}

	public static void extractCoreContextLabelsTableIfNotPresent(MTConfigFile configFile) {
		int numThreads = configFile.decoderConfig.getNumParallelGrammarExtractors();
		String coreContextLabelTableFileName = CoreContextLabelsTable.getCoreContextLabelsTableFileName(configFile);
		if ( MTComponentCreater.fileNameAlreadyExists(coreContextLabelTableFileName)) {
			System.out.println("Info: A Core Context Labels Table file already exist with the name " + coreContextLabelTableFileName + " using that file");
			return;
		}
		String configFilePath = writeCoreContextLabelsTableExtractionConfigFileAndReturnPath(configFile);
		System.out.println("Extract core context label multi-thread ...");
		MultiThreadGrammarExtractorMinimalPhrasePairs.extractCoreContextLabelsTableMultiThread(configFilePath, numThreads);
	}

}
