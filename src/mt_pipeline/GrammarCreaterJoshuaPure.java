package mt_pipeline;

import grammarExtraction.chiangGrammarExtraction.GlueFeaturesCreaterJoshuaPure;
import grammarExtraction.chiangGrammarExtraction.HieroGlueGrammarCreater;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.samtGrammarExtraction.GlueGrammarCreater;

import java.util.ArrayList;
import java.util.List;

import util.LinuxInteractor;
import mt_pipeline.mt_config.MTConfigFile;

public class GrammarCreaterJoshuaPure extends GrammarCreater {

	private GrammarCreaterJoshuaPure(MTConfigFile theConfig, String grammarOutputPath, String category,SystemIdentity systemIdentity) {
		super(theConfig, grammarOutputPath, category,systemIdentity);
	}

	public static GrammarCreaterJoshuaPure createTestGrammarCreater(MTConfigFile theConfig,SystemIdentity systemIdentity) {
		return new GrammarCreaterJoshuaPure(theConfig, theConfig.filesConfig.getFilteredGrammarPathTest(), MTConfigFile.TEST_CATEGORY,systemIdentity);
	}

	public static GrammarCreaterJoshuaPure createDevGrammarCreater(MTConfigFile theConfig,SystemIdentity systemIdentity) {
		return new GrammarCreaterJoshuaPure(theConfig, theConfig.filesConfig.getFilteredGrammarPathMert(), MTConfigFile.DEV_CATEGORY,systemIdentity);
	}

	/**
	 * Create a prefix table that is later used to extract a (filtered) Joshua grammar for the test set
	 */
	public void createPrefixTable() {
		// Beware that here we assume the train files have already been
		// lowercased and tokenized. In general this does not have to be true.
		String command = theConfig.decoderConfig.baseJoshuaJavaCall() + " joshua.corpus.suffix_array.Compile " + theConfig.filesConfig.getEnrichedSourceFilePath(MTConfigFile.TRAIN_CATEGORY) + " "
				+ theConfig.filesConfig.getTargetSubFilePathTrain() + " " + theConfig.filesConfig.getAlignmentsSubFilePath() + " "
				+ theConfig.foldersConfig.getSuffixArrayFolder();
		LinuxInteractor.executeCommand(command, true);
	}

	/**
	 * Create a pure Joshua grammar, using the extracted prefix table
	 */
	private void extractSortedFilteredJoshuaGrammar(String sortedGrammarPath, String category, int noThreads) {
		System.out.println("extractSortedFilteredGrammar(" + sortedGrammarPath + "," + category + ") called");
		String rawGrammarPath = sortedGrammarPath + ".raw ";
		String command = theConfig.decoderConfig.baseJoshuaJavaCall() + " joshua.prefix_tree.ExtractRules " + theConfig.foldersConfig.getSuffixArrayFolder() + " " + rawGrammarPath
				+ theConfig.filesConfig.getEnrichedSourceFilePath(category);
		System.out.println("command: " + command);
		LinuxInteractor.executeExtendedCommandDisplayOutput(command);

		sortGrammar(rawGrammarPath, sortedGrammarPath, theConfig.foldersConfig.getIntermediateOutputFolderName(), noThreads);
	}

	protected void extractSortedFilteredGrammar() {
		extractSortedFilteredJoshuaGrammar(grammarOutputPath, category, theConfig.decoderConfig.getNumParallelGrammarExtractors());
	}

	public static void extractFilteredPureJoshuaGrammarsPrallel(MTConfigFile theConfig) {
		SystemIdentity systemIdentity = SystemIdentity.createSystemIdentity(theConfig);
		checkDevAndMertFolderExist(theConfig);
		GrammarCreater testGrammarExtractor = GrammarCreaterJoshuaPure.createTestGrammarCreater(theConfig,systemIdentity);
		GrammarCreater devGrammarCreater = GrammarCreaterJoshuaPure.createDevGrammarCreater(theConfig,systemIdentity);
		List<GrammarCreater> grammarCreaters = new ArrayList<GrammarCreater>();
		grammarCreaters.add(devGrammarCreater);
		grammarCreaters.add(testGrammarExtractor);

		// Extract the filtered grammar for Test and Mert in parallel
		MTPipeline.performThreadComputation(grammarCreaters, 2);

		GlueFeaturesCreaterJoshuaPure glueFeaturesCreaterJoshuaPure = GlueFeaturesCreaterJoshuaPure.createFeaturesCreaterJoshuaPure(systemIdentity);
		GlueGrammarCreater glueGrammarCreater = HieroGlueGrammarCreater.createHieroGlueGrammarCreater(true,
				systemIdentity);
		glueGrammarCreater.writeGlueGrammar(null, theConfig.filesConfig.getGlueGrammarPathMert(),glueFeaturesCreaterJoshuaPure);
		glueGrammarCreater.writeGlueGrammar(null, theConfig.filesConfig.getGlueGrammarPathTest(),glueFeaturesCreaterJoshuaPure);

	}
}
