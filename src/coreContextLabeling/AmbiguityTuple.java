package coreContextLabeling;

public interface AmbiguityTuple {

	public double getAmbiguityScore();
}
