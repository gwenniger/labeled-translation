package coreContextLabeling;

import grammarExtraction.translationRules.TranslationEquivalenceItem;

public class SourceTargetAmbiguityTuple implements AmbiguityTuple {

	final TranslationEquivalenceItem sourceTargetSideEquivalenceItem;
	final double sourceSideEntropy;
	final double sourceTargetRuleProbability;
	final double relativeEntropyWeight;

	public SourceTargetAmbiguityTuple(TranslationEquivalenceItem sourceTargetSideEquivalenceItem, double sourceSideEntropy,
			double sourceTargetSideRuleProbability, double relativeEntropyWeight) {
		this.sourceTargetSideEquivalenceItem = sourceTargetSideEquivalenceItem;
		this.sourceSideEntropy = sourceSideEntropy;
		this.sourceTargetRuleProbability = sourceTargetSideRuleProbability;
		this.relativeEntropyWeight = relativeEntropyWeight;
	}

	@Override
	public double getAmbiguityScore() {
		return CoreContextLabel.getAmbiguityScore(this.sourceSideEntropy, this.sourceTargetRuleProbability, relativeEntropyWeight);
	}

}
