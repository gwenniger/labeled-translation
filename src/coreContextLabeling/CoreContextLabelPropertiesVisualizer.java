package coreContextLabeling;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

import tsg.TSNodeLabel;
import viewer.BasicDrawing;
import viewer.ExtraTreeNodePropertiesVisualizer;
import viewer.ExtraTreeNodePropertiesVisualizerFactory;
import viewer.NodeRenderPosition;
import viewer.TreePairPanel;
import viewer.TreeVisualizer;

public class CoreContextLabelPropertiesVisualizer implements ExtraTreeNodePropertiesVisualizer, ExtraTreeNodePropertiesVisualizerFactory {

	private final TreePairPanel thePanel;

	private CoreContextLabelPropertiesVisualizer(TreePairPanel thePanel) {
		this.thePanel = thePanel;
	}

	public static CoreContextLabelPropertiesVisualizer createCoreContextLabelPropertiesVisualizer(TreePairPanel thePanel) {
		return new CoreContextLabelPropertiesVisualizer(thePanel);
	}

	public static CoreContextLabelPropertiesVisualizer creaContextLabelPropertiesVisualizerFactory() {
		return new CoreContextLabelPropertiesVisualizer(null);
	}

	private void drawCoreContextLabelAmbiguityRank(Graphics2D g2, TSNodeLabel node, NodeRenderPosition nodePosition, int yOffset, int extraXOffSet) {
		Font savedFont = g2.getFont();
		g2.setFont(thePanel.getPanelSizeProperties().smallFont());
		String ambiguityRankString = getCoreContextLabelAmbiguityRankString(node);
		g2.drawString(ambiguityRankString, (int) (nodePosition.getXMiddle() + extraXOffSet + (nodePosition.getSymbolBulletDiameter() * 0.50)),
				(int) (nodePosition.getBulletYMiddle() + yOffset + (nodePosition.getSymbolBulletDiameter() * 0.50)));
		g2.setFont(savedFont);

	}

	private void drawCoreContextLabeIfPresent(Graphics2D g2, TSNodeLabel node, NodeRenderPosition nodePosition, int yOffset) {
		String coreContextLabelString;
		if ((coreContextLabelString = getCoreContextLabelString(node)) != "") {
			g2.setColor(TreeVisualizer.getColorForVarNum(getCoreContextLabelString(node), thePanel.getColorTable()));
			int varNum = TreeVisualizer.getVarNum(coreContextLabelString, thePanel.getVarNumTable());
			int extraXOffSet = (int) nodePosition.getSymbolBulletDiameter();
			BasicDrawing.drawNodeSymbolBullet(((int) (nodePosition.getSymbolBulletDiameter() * 0.75)), nodePosition.getXMiddle() + extraXOffSet,
					nodePosition.getBulletYMiddle() + yOffset, varNum, thePanel.getMaxVarNum(), g2);
			g2.setColor(Color.BLACK);
			drawCoreContextLabelAmbiguityRank(g2, node, nodePosition, yOffset, extraXOffSet);
		}
	}

	public static String getCoreContextLabelString(TSNodeLabel node) {

		String result = node.getExtraLabel(TreeRepresentationCoreContextLabeled.CoreContextLabelProperty);

		if (result == null) {
			result = "";
		}
		return result;
	}

	public static String getCoreContextLabelAmbiguityRankString(TSNodeLabel node) {

		String result = node.getExtraLabel(TreeRepresentationCoreContextLabeled.CoreContextLabelAmbiguityRankProperty);

		if (result == null) {
			result = "";
		}
		return result;
	}

	@Override
	public void drawExtraTreeNodeProperties(Graphics2D g2, TSNodeLabel node, NodeRenderPosition nodePosition, int yOffset) {
		drawCoreContextLabeIfPresent(g2, node, nodePosition, yOffset);
		
	}

	@Override
	public ExtraTreeNodePropertiesVisualizer createExtraTreeNodePropertiesVisualizer(TreePairPanel thePanel) {
		return createCoreContextLabelPropertiesVisualizer(thePanel);
	}
}
