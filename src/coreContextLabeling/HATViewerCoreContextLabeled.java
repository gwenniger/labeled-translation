package coreContextLabeling;

import hat_database.HATSelectionPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ChangeEvent;

import coreContextLabeling.CoreContextLabelsTable;
import coreContextLabeling.EbitgChartBuilderCoreContextLabeled;
import alignment.Alignment;
import alignmentStatistics.EbitgTreeStatisticsComputation;
import alignmentStatistics.SymmetricTreeStatisticsComputation;
import tsg.TSNodeLabel;
import util.ConfigCacher;
import util.ConfigFile;
import util.InputReading;
import util.Pair2;
import util.Serialization;
import viewer.LabeledAlignmentPanel;
import viewer_io.SpinnerPanel;
import viewer.TreePairPanel;
import viewer_io.ButtonIOComponent;
import viewer_io.ButtonIOComponentTable;
import viewer_io.CheckBoxTable;
import viewer_io.EbitgCheckBoxTable;
import viewer_io.ExporterPDF;
import viewer_io.MenuInteraction;
import viewer_io.TreePairBankLoader;
import viewer_io.ViewerInputLoading;
import static util.ConfigCacher.createConfigCacher;
import static extended_bitg.HATPairPanel.createHATPairPanel;
import extended_bitg.EbitgChartBuilderBasic;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeState;
import extended_bitg.EbitgTreeState.TreeType;
import extended_bitg.HATViewer;
import extended_bitg.HATInputPreparation;
import extended_bitg.HATPairPanel;
import extended_bitg.NumberKeeper;
import extended_bitg.SafeGUIAffectingTaskRunner;

// SCROLLBARS INFO: 
//http://www.javakb.com/Uwe/Forum.aspx/java-gui/4544/Added-JTree-to-JScrollPane-but-no-Hori-Scrollbars

@SuppressWarnings("serial")

public class HATViewerCoreContextLabeled extends HATViewer {
	public static final int MAX_ALLOWED_INFERENCES_PER_NODE = 100000;
	// private static final boolean USE_GREEDY_NULL_BINDING = true;

	public static final String SourceKeyString = "SourcetComponent";
	public static final String TargetKeyString = "TargetComponent";
	public static final String AlignmentKeyString = "AlignmentComponent";
	public static final String ConfigKeyString = "ConfigComponent";

	/*
	 * public static final String sourceInitial = "don't want it"; public static
	 * final String targetInitial = "ne le veux pas";Che public static final
	 * String alignmentInitial = "0-0 0-3 1-2 2-1";
	 */
	/*
	 * public static final String sourceInitial = "s1 s2 s3 s4"; public static
	 * final String targetInitial = "t1 t2 t3 t4 t5"; public static final String
	 * alignmentInitial = "0-0 1-1 1-2 1-3 2-4 3-4";
	 */

	public static final String sourceInitial = "the man walks fast to his car";
	public static final String targetInitial = "de man loopt snel naar zijn auto";
	public static final String alignmentInitial = "0-0 1-1 2-2 3-3 4-4 5-5 6-6";

	protected final String HatSpinnerLabel = "HAT :";

	public static final boolean atomicPartsMustAllShareSameAlignmentChunk = true;
	public final EbitgTreeStatisticsComputation treeStatisticsComputation = new SymmetricTreeStatisticsComputation(atomicPartsMustAllShareSameAlignmentChunk);

	JTabbedPane tabbedPane;
	HATSelectionPanel hatSelectionPanel;

	// A NumberKeeper attribute: keeps the number of the current
	// source-target-alignment triple,
	// being its position in the original corpus from which it was extracted
	NumberKeeper numberKeeper;
	MenuInteraction menuInteraction;

	protected final SpinnerPanel alignmentTripleSpinnerPanel;

	// We keep a variable that locks the value of the alignments for the
	// spinner, while
	// computation of a set of HATs is taking place, to avoid inconsistency
	// between computed
	// trees and the alignments that are used to draw lines between those trees
	private boolean alignmentsLocked = false;

	private CoreContextLabelsTable coreContextLabelingTable;

	public static TSNodeLabel initialSentence() {
		try {
			return new TSNodeLabel("(S (V-H load) (NP (D a) (N-H corpus)))");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static HATViewerCoreContextLabeled createHATViewer() {

		EbitgCheckBoxTable checkBoxTable = EbitgCheckBoxTable.createEbitgCheckBoxTable();

		ConfigCacher configCacher = createConfigCacher("HATViewerCachedConfig");

		// Set up the drawing area.
		HATPairPanel treePairPanel = createHATPairPanel(null, checkBoxTable, CoreContextLabelPropertiesVisualizer.creaContextLabelPropertiesVisualizerFactory());
		treePairPanel.setBackground(Color.white);
		treePairPanel.setFocusable(false);

		LabeledAlignmentPanel alignmentPanel = LabeledAlignmentPanel.createLabeledAlignmentPanel(checkBoxTable);
		alignmentPanel.initializePanel();
		HATViewerCoreContextLabeled HATViewer = new HATViewerCoreContextLabeled(ButtonIOComponentTable.createButtonIOComponentTable(), alignmentPanel,
				configCacher, treePairPanel, checkBoxTable);

		configCacher.registerEntry("configFileLocation", "!!!./configFile.txt", HATViewer.getConfigFileField());
		configCacher.updateRegisteredTextFields();

		// alignmentPanel.setPreferredSize(new Dimension(800,600));

		JScrollPane scroller = new JScrollPane(alignmentPanel, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scroller.setPreferredSize(new Dimension(1800, 170));

		JLabel corpusSetSizeReport = new JLabel("of: 0");
		corpusSetSizeReport.setFocusable(false);

		HATViewer.rightControlPanel.add(HATViewer.alignmentTripleSpinnerPanel);

		HATViewer.componentsPanel.add(scroller, BorderLayout.NORTH);

		// Get the default toolkit
		// Toolkit toolkit = Toolkit.getDefaultToolkit();
		// Get the current screen size
		// Dimension screenSize = toolkit.getScreenSize();

		Dimension mainPanelPrefferdSize = HATViewer.getPreferredSize();
		HATViewer.treeScroller.setPreferredSize(new Dimension(mainPanelPrefferdSize.width, mainPanelPrefferdSize.height - 340));

		HATViewer.tabbedPane = new JTabbedPane();
		HATViewer.tabbedPane.addTab("HATs visualization", HATViewer);
		addHATSelectionPanel(HATViewer);

		System.out.println("HATViewer creator returns");

		return HATViewer;
	}

	public JTextField getSourceField() {
		return getIoComponentsTable().getComponentByName(SourceKeyString).getTextField();
	}

	public JTextField getTargetField() {
		return getIoComponentsTable().getComponentByName(TargetKeyString).getTextField();
	}

	public JTextField getAlignmentsField() {
		return getIoComponentsTable().getComponentByName(AlignmentKeyString).getTextField();
	}

	public JTextField getConfigFileField() {
		return getIoComponentsTable().getComponentByName(ConfigKeyString).getTextField();
	}

	public static void addHATSelectionPanel(HATViewerCoreContextLabeled HATViewer) {
		HATViewer.hatSelectionPanel = new HATSelectionPanel(HATViewer.getConfigFileField(), HATViewer.getConfigCacher());
		HATViewer.tabbedPane.addTab("HAT Selection", HATViewer.hatSelectionPanel);
	}

	public HATViewerCoreContextLabeled(ButtonIOComponentTable buttonIOComponentTable, LabeledAlignmentPanel alignmentPanel, ConfigCacher configCacher,
			TreePairPanel treePairPanel, CheckBoxTable checkBoxTable) {
		super(buttonIOComponentTable, alignmentPanel, configCacher, treePairPanel, checkBoxTable);
		this.alignmentTripleSpinnerPanel = SpinnerPanel.createSpinnerPanel(this, "Alignment Triple: ");
		this.menuInteraction = createMenuInteraction(this);
	}

	protected String getSpinnerLabel() {
		return HatSpinnerLabel;
	}

	@Override
	protected void createInputFields() {
		java.awt.Font f = new java.awt.Font("Arial", Font.BOLD, 18);
		JTextField alignmentField = new JTextField(alignmentInitial);
		alignmentField.setFont(f);

		JTextField sourceField = new JTextField(sourceInitial);
		sourceField.setFont(f);

		JTextField targetField = new JTextField(targetInitial);
		targetField.setFont(f);

		JTextField configFileField = new JTextField("configFile.txt");
		targetField.setFont(f);

		JButton button1 = new JButton("Compute Ebitg trees");
		button1.setMnemonic('E');

		JButton configFileCreateButton = new JButton("Make Configfile for Database");
		button1.setMnemonic('E');

		JButton button2 = new JButton("Load Everything from ConfigFile");
		getIoComponentsTable().addComponent(HATViewer.SourceKeyString, new ButtonIOComponent(button1, sourceField, null, new JLabel("SourceString:")));
		getIoComponentsTable().addComponent(HATViewer.TargetKeyString, new ButtonIOComponent(null, targetField, null, new JLabel("TargetString:")));
		getIoComponentsTable().addComponent(HATViewer.AlignmentKeyString,
				new ButtonIOComponent(configFileCreateButton, alignmentField, null, new JLabel("Alignment:")));
		getIoComponentsTable().addComponent(HATViewer.ConfigKeyString,
				new ButtonIOComponent(button2, configFileField, null, new JLabel("ConfigFile location:")));
	}

	private static void createButtonActionListeners(final HATViewer treeViewer) {
		treeViewer.getIoComponentsTable().get(0).getTheButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((HATViewer) treeViewer).actionPerformed(e);
			}
		});

		treeViewer.getIoComponentsTable().get(2).getTheButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				treeViewer.createConfigFileForDB();
			}
		});

		treeViewer.getIoComponentsTable().get(3).getTheButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				treeViewer.loadEveryThingFromConfigFile();
			}
		});
	}

	/**
	 * Create the GUI and show it. For thread safety, this method should be
	 * invoked from the event-dispatching thread.
	 */
	public static HATViewerCoreContextLabeled createAndShowGUI() {
		// Create and set up the window.
		JFrame mainFrame = new JFrame("Ebitg Tree Viewer");
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


		final HATViewerCoreContextLabeled treeViewer = createHATViewer();
		treeViewer.hatsDatabaseBuilderPanel.setContainingFrame(mainFrame);
		Dimension area = treeViewer.treePanel.getArea();
		System.out.println("Area: " + area.width + " " + area.height);
		mainFrame.setMinimumSize(new Dimension(area.width + 5, area.height + 5));
		treeViewer.setOpaque(true); // content panes must be opaque
		mainFrame.setContentPane(treeViewer.tabbedPane);
		// frame.setContentPane(treeViewer);

		mainFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				treeViewer.doBeforeClosing();
			}
		});

		// Menu
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("File");
		treeViewer.menuInteraction.addItemsToMenu(menu);

		createButtonActionListeners(treeViewer);

		for (JMenuItem item : treeViewer.menuInteraction.getMenuItems()) {
			System.out.println("item :" + item);
			menu.add(item);
		}

		menuBar.add(menu);
		mainFrame.setJMenuBar(menuBar);

		// Display the window.
		mainFrame.pack();
		mainFrame.setVisible(true);

		return treeViewer;
	}

	private void loadCoreContextLabelsFromConfig() {
		try {
			ConfigFile theConfig = new ConfigFile(getConfigFilePath());
			if (theConfig.hasValue(EbitgChartBuilderCoreContextLabeled.CoreContextLabelTableFileNameConfigurationProperty)) {
				coreContextLabelingTable = Serialization.unSerializeOBject(theConfig
						.getValue(EbitgChartBuilderCoreContextLabeled.CoreContextLabelTableFileNameConfigurationProperty));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private String getConfigFilePath() {
		return this.getIoComponentsTable().get(3).getTextfieldContents();
	}

	private void loadSentencesAndAlignmentsAndSourceParses() {
		//ViewerInputLoading.loadAlignmentTriplesAndParsesFromConfig(getConfigFilePath(), this.getAlignmentPanel(), this.alignmentTripleSpinnerPanel);
		ViewerInputLoading.loadAlignmentTriplesAndParsesFromConfig(getConfigFilePath(), this, this.alignmentTripleSpinnerPanel);
	}

	public void loadEveryThingFromConfigFile() {
		System.out.println(">>> Load everything from configFile...");
		loadSentencesAndAlignmentsAndSourceParses();
		this.numberKeeper = NumberKeeper.getNumberKeeperFromConfig(getConfigFilePath());
		loadCoreContextLabelsFromConfig();
	}

	private String getSourceString() {
		return this.getSourceField().getText();
	}

	private String getTargetString() {
		return this.getTargetField().getText();
	}

	public String getAlignmentString() {
		return this.getAlignmentsField().getText();
	}

	/**
	 * 
	 * @param hatInputPreparation
	 * @param tg
	 * @return True if all went well and false if an error occurred in checking
	 *         the consistency of the generated trees
	 */
	public boolean displayComputedHATs(HATInputPreparation hatInputPreparation,
			EbitgTreeGrower<? extends EbitgLexicalizedInference, ? extends EbitgTreeState<?, ?>> tg) {
		File f = new File("tempITGTrees.txt");

		this.loadTreebank(f, true, hatInputPreparation.getSourceLeafDescriptions(), hatInputPreparation.getTargetLeafDescriptions(), getAlignmentString());

		int numTrees = this.treePanel.getNumTrees();
		this.treeSpinnerPanel.setSpinnerSize(numTrees);

		return this.determineAndPrintProperties(tg, hatInputPreparation.getSourceString(), hatInputPreparation.getTargetString());

	}

	private void setMaxNumLabels() {
		if (showExtraLabels() && (this.getAlignmentPanel().getSourceParse() == null)) {
			this.treePanel.setMaxNumLabels(1);
		} else {
			this.treePanel.setMaxNumLabels(2);
		}
	}

	public void setShowAlignmentSets(boolean value) {
		((EbitgCheckBoxTable) this.checkBoxTable).getShowAlignmentSetCheckBox().setSelected(value);
	}

	public void setNoColors(boolean value) {
		((EbitgCheckBoxTable) this.checkBoxTable).getNoColorsCheckBox().setSelected(value);
	}

	private boolean showExtraLabels() {
		return ((EbitgCheckBoxTable) this.checkBoxTable).showExtraLabels();
	}

	public HATInputPreparation createHatInputPreparationFromInputFields() {
		return HATInputPreparation.createHATInputPreparation(this.getSourceString(), this.getTargetString(), this.getAlignmentString(),
				((EbitgCheckBoxTable) this.checkBoxTable).useSymbolsForWords());
	}

	private boolean hasCoreContextLabelingTable() {
		return (this.coreContextLabelingTable != null);
	}

	private EbitgTreeGrower<? extends EbitgLexicalizedInference, ? extends EbitgTreeState<?, ?>> createTreeGrower(HATInputPreparation hatInputPreparation,
			TSNodeLabel sourceParse) {
		if (hasCoreContextLabelingTable()) {
			return EbitgChartBuilderCoreContextLabeled.buildAndWriteTreePairsForAlignment(MAX_ALLOWED_INFERENCES_PER_NODE, hatInputPreparation
					.getSourceString(), hatInputPreparation.getTargetString(), hatInputPreparation.getAlignmentString(), getAlignmentPanel().getCCGLabeler(),
					((EbitgCheckBoxTable) this.checkBoxTable).computeAllInducedHATs(), ((EbitgCheckBoxTable) this.checkBoxTable).useGreedyNullBinding(),
					coreContextLabelingTable);
		} else {
			// assert(alignmentPanel.hasParses());
			// assert(alignmentPanel.getCCGLabeler().hasLabelChart());
			return EbitgChartBuilderBasic.buildAndWriteTreePairsForAlignment(MAX_ALLOWED_INFERENCES_PER_NODE, hatInputPreparation.getSourceString(),
					hatInputPreparation.getTargetString(), hatInputPreparation.getAlignmentString(), getAlignmentPanel().getCCGLabeler(),
					((EbitgCheckBoxTable) this.checkBoxTable).computeAllInducedHATs(), ((EbitgCheckBoxTable) this.checkBoxTable).useGreedyNullBinding());
		}
	}

	public boolean computeHATsThreadSafe() {
		SafeGUIAffectingTaskRunner<HATViewer, Object, Boolean> taskRunner = SafeGUIAffectingTaskRunner.createEbitgViewerHatsComputer(this);
		return SafeGUIAffectingTaskRunner.executeThreadSafe(taskRunner);
	}

	public boolean computeHATs() {
		alignmentsLocked = true;
		System.out.println(">>> Produce ITG trees for permutation...");

		System.out.println(" this.checkBoxTable : " + this.checkBoxTable);
		HATInputPreparation hatInputPreparation = createHatInputPreparationFromInputFields();

		this.getAlignmentPanel().loadFromStrings(hatInputPreparation.getSourceString(), hatInputPreparation.getTargetString(),
				hatInputPreparation.getAlignmentString());

		this.getAlignmentPanel().initWithoutSentenceLouding();

		setMaxNumLabels();

		TSNodeLabel sourceParse = null;

		setMaxNumLabels();

		Alignment alignments = Alignment.createAlignment(hatInputPreparation.getAlignmentString());
		((HATPairPanel) this.treePanel).setAlignments(alignments.getAlignmentPairs());

		System.out.println(">>> Prepared input:");
		System.out.println("sourceString: " + hatInputPreparation.getSourceString());
		System.out.println("targetString: " + hatInputPreparation.getTargetString());
		System.out.println("alignment: " + hatInputPreparation.getAlignmentString());

		EbitgTreeGrower<? extends EbitgLexicalizedInference, ? extends EbitgTreeState<?, ?>> tg = createTreeGrower(hatInputPreparation, sourceParse);

		if (tg != null) {

			// Print the set of minimal phrase pairs
			// MinimalPhrasePairExtractor mppe = new
			// MinimalPhrasePairExtractor(tg.getChart());
			// mppe.printMinimalPhrasePairInferences();

			alignmentsLocked = false;
			return displayComputedHATs(hatInputPreparation, tg);
		}

		alignmentsLocked = false;

		// If the parsing failed we do not consider it as an error of the
		// visualization
		return true;

	}

	/**
	 * Method that loads the HAT treebank, sets the leaf description words and
	 * the alignments that belong with the set of HATs
	 * 
	 * @param treebankFile
	 * @param resetSpinner
	 * @param leafDescriptionWords1
	 * @param leafDescriptionWords2
	 * @param alignmentString
	 * @return
	 */
	protected void loadTreebank(File treebankFile, boolean resetSpinner, List<String> leafDescriptionWords1, List<String> leafDescriptionWords2,
			String alignmentString) {
		TreePairBankLoader.loadTreePairBank(treebankFile, this, resetSpinner);
		setTreeLeafDescriptions(leafDescriptionWords1, leafDescriptionWords2);

		System.out.println("HATViewer.loadTreebank....");
		// FIXME this should not be done here
		// List<AlignmentPair> alignments =
		// InputReading.constructAlignmentPairs(alignmentString);
		// ((HATPairPanel)this.treePanel).setAlignments(alignments);
		return;
	}

	protected void setTreeLeafDescriptions(List<String> leafDescriptionWords1, List<String> leafDescriptionWords2) {
		((TreePairPanel) this.treePanel).setFirstTreeLeafDescriptionWords(leafDescriptionWords1);
		((TreePairPanel) this.treePanel).setSecondTreeLeafDescriptionWords(leafDescriptionWords2);
	}

	/**
	 * Method that determines the consistency of the source and target string
	 * with both the original and the twin tree
	 * 
	 * @param sourceString
	 * @param targetString
	 * @return A pair of a boolean and a String. The boolean is true provided
	 *         that both trees are consistent, the String is a String that
	 *         contains the consistency properties as a description"
	 */
	public Pair2<Boolean, String> determineConsistency(String sourceString, String targetString) {

		boolean firstTreeConsistent = ((HATPairPanel) this.treePanel).firstTreeConsistentWithString(sourceString);
		boolean secondTreeConsistent = ((HATPairPanel) this.treePanel).secondTreeConsistentWithString(targetString);

		return new Pair2<Boolean, String>((firstTreeConsistent && secondTreeConsistent), "\nOriginalTreeConsistent: " + firstTreeConsistent
				+ " twinTreeConsistent: " + secondTreeConsistent);
	}

	public boolean determineAndPrintProperties(EbitgTreeGrower<? extends EbitgLexicalizedInference, ? extends EbitgTreeState<?, ?>> tg, String sourceString,
			String targetString) {

		System.out.println("HATViewer: DeterminAndPrintProperties");
		int sourceLength = InputReading.determineNumWordsString(sourceString);
		int targetLength = InputReading.determineNumWordsString(targetString);

		System.out.println("EbitViewer : targetString" + targetString);

		int maxSourceLengthAtomicFragment = 1;

		int numberTranslationEquivalentNodesInChart = treeStatisticsComputation.determineNumberTranslationEquivalentNodesInChart(tg.getChart());
		int maxBranchingFactor = treeStatisticsComputation.determineMaximalOccuringBranching(tg, maxSourceLengthAtomicFragment);
		int maxExtraAlignments = treeStatisticsComputation.determineMaxNumberOfExtraAlignmentsInTree(tg);

		// tg.getChart().printChartContents();

		TreeType treeType = treeStatisticsComputation.determineTreeType(tg, maxSourceLengthAtomicFragment);

		Pair2<Boolean, String> consistencyInformation = this.determineConsistency(sourceString, targetString);

		// Test: Check that the trees are consistent

		if (!consistencyInformation.getFirst()) {
			System.out.println("HATViewer : Tree" + getCurrentAlignmentTripleNumber() + " is inconsistent");
			// System.exit(0);
		}

		String outputString = "";

		if (this.numberKeeper != null && (!this.numberKeeper.isEmpty())) {
			outputString = "Alignment Triple number:\t" + getCurrentAlignmentTripleNumber();
		}
		outputString += "  sourceLength: " + sourceLength + "   targetLength: " + targetLength + "\nmaxBranchingFacor: " + maxBranchingFactor
				+ "   maxExtraAlignments: " + maxExtraAlignments + "\n#TranslationEquivalentNodes: " + numberTranslationEquivalentNodesInChart + " treeType: "
				+ treeType.toString() + consistencyInformation.getSecond();

		if (!consistencyInformation.getFirst()) {
			System.out.println("Inconsistency detected in trees: ");
			System.out.println(outputString);
		}

		this.setOutputFieldText(outputString);

		return consistencyInformation.getFirst();
	}

	public int getCurrentAlignmentTripleNumber() {
		return this.numberKeeper.getCurrentNumber();
	}

	// Deal with scrolling trough the sentences
	public void stateChanged(ChangeEvent e) {
		System.out.println("HATViewer.stateChanged() called ...");

		System.out.println("(this.alignmentTripleSpinnerPanel.getSpinner()" + this.alignmentTripleSpinnerPanel.getSpinner());
		System.out.println("(this.treeSpinnerPanel.getSpinner()" + this.treeSpinnerPanel.getSpinner());
		System.out.println("e.getSource()" + e.getSource());

		if (e.getSource().equals(this.alignmentTripleSpinnerPanel.getSpinner()) && (!this.alignmentsLocked)) {
			loadCurrentAlingmentTripleSpinnerValue();
		} else if (e.getSource().equals(this.treeSpinnerPanel.getSpinner())) {
			System.out.println("HATViewer ... treeSpinnerPanel block entered");
			Integer n = treeSpinnerPanel.getSpinnerValue() - 1;
			System.out.println("Tree number: " + n);
			int treeToLoad = this.treePanel.goToSentence(n);
			System.out.println("Tree to be loaded: " + treeToLoad);
			this.treePanel.init();

		}

	}

	public void loadCurrentAlingmentTripleSpinnerValue() {
		Integer number = this.alignmentTripleSpinnerPanel.getSpinnerValue();
		loadAlignmentTriple(number);
		this.numberKeeper.goToIndex(number - 1);

		System.out.println("numbeKeeper goes to index: " + (number - 1));
		// System.out.println("this.numberKeeper.getCurrentNumber() : " +
		// this.numberKeeper.getCurrentNumber());
	}

	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);

		if (useSymbolsChanged(e) || showNamePlatesChanged(e) || computeHATsButtonPressed(e) || showEtraLabelsChanged(e)) {
			// Displaying words/symbols changed, recompute the HATs
			System.out.println("Recompute the HATs ");
			this.computeHATs();
		}

	}

	private boolean useSymbolsChanged(ActionEvent e) {
		return e.getSource().equals(((EbitgCheckBoxTable) this.checkBoxTable).getUseSymbolsCheckBox());
	}

	private boolean showNamePlatesChanged(ActionEvent e) {
		return e.getSource().equals(((EbitgCheckBoxTable) this.checkBoxTable).getShowNamePlateCheckBox());
	}

	private boolean computeHATsButtonPressed(ActionEvent e) {
		return e.getSource().equals(this.getIoComponentsTable().get(0).getTheButton());
	}

	private boolean showEtraLabelsChanged(ActionEvent e) {
		return e.getSource().equals(((EbitgCheckBoxTable) this.checkBoxTable).getShowExtraLabelsCheckBox());
	}

	public void loadAlignmentTriple(int number) {
		int index = number - 1;

		this.numberKeeper.goToIndex(number - 1);
		getAlignmentPanel().goToSentence(index);
		// statusBar.setText(treebankComments.get(index));
		getAlignmentPanel().init();

		String sourceSentence = getAlignmentPanel().getSourceSentence();
		String targetSentence = getAlignmentPanel().getTargetSentence();
		String alignment = getAlignmentPanel().getAlignment();

		this.getSourceField().setText(sourceSentence);
		this.getTargetField().setText(targetSentence);
		this.getAlignmentsField().setText(alignment);

	}

	public void loadAlignmentTripleThreadSafe(int number) {
		SafeGUIAffectingTaskRunner<HATViewer, Integer, Boolean> taskRunner = SafeGUIAffectingTaskRunner.createEbitgViewerAlignmentTripleLoader(this, number);
		SafeGUIAffectingTaskRunner.executeThreadSafe(taskRunner);
	}

	public int getMaxAlignmentSpinnerValue() {
		return this.alignmentTripleSpinnerPanel.getMaxSpinnerValue();
	}

	public void startRunnableThreadSafe(Runnable r) {
		// This is necessary since repaint is not thread safe,
		// see http://stevensrmiller.com/wordpress/?p=567
		javax.swing.SwingUtilities.invokeLater(r);
	}

	public void exportAlignmentToFile(String fileName) {
		ExporterPDF.exportToPDF(new File(fileName), this.getAlignmentPanel());
	}

	public void exportHATToFile(String fileName) {
		ExporterPDF.exportToPDF(new File(fileName), this.treePanel);
	}

	public static void main(String[] args) {
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
