package coreContextLabeling;

import java.util.ArrayList;
import java.util.List;
import bitg.Pair;
import extended_bitg.EbitgChartEntry;
import extended_bitg.EbitgLexicalizedInference;
import grammarExtraction.minimalPhrasePairExtraction.MinimalPhrasePairExtractor;
import grammarExtraction.translationRules.TranslationEquivalenceItem;

public class CoreContextLabeler 
{
	private final	CoreContextLabelsTable coreContextLabelsTable;
	private final ChartCoreContextLabelsTable chartCoreContextLabelsTable;
	
	private CoreContextLabeler(ChartCoreContextLabelsTable chartCoreContextLabelsTable, CoreContextLabelsTable coreContextLabelsTable)
	{
		this.chartCoreContextLabelsTable = chartCoreContextLabelsTable;
		this.coreContextLabelsTable = coreContextLabelsTable;
	}
	
	public static CoreContextLabeler createCoreContextLabeler(int sourceLength, CoreContextLabelsTable coreContextLabelsTable)
	{
		ChartCoreContextLabelsTable chartCoreContextLabelsTable = ChartCoreContextLabelsTable.createChartCoreContextLabelsTable(sourceLength);
		return new CoreContextLabeler(chartCoreContextLabelsTable, coreContextLabelsTable);
	}

	
	public CoreContextLabel getCoreContextLabelForInferenceFromTable(EbitgLexicalizedInference ebitgLexicalizedInference)
	{
		return this.chartCoreContextLabelsTable.getCoreContextLabelForInference(ebitgLexicalizedInference);
	}
	
	public CoreContextLabel getCoreContextLabelForChartEntryFromTable(EbitgChartEntry chartEntry)
	{
		return this.chartCoreContextLabelsTable.getCoreContextLabelForChartEntry(chartEntry);
	}
	
	/*
	private IntegerKeyRuleRepresentation createKeyRuleRepresentationSource(EbitgLexicalizedInference ebitgLexicalizedInference){
		List<String> sourceWords = ebitgLexicalizedInference.generatedSourceWords();
		IntegerKeyRuleRepresentation sourceRuleRepresentation = IntegerKeyRuleRepresentation.createIntegerKeyRuleRepresentationWithAddedChiangLabel(sourceWords,coreContextLabelsTable.getWordKeyMappingTable());
		return sourceRuleRepresentation;
	}*/
	
	/*
	private IntegerKeyRuleRepresentation createKeyRuleRepresentationTarget(EbitgLexicalizedInference ebitgLexicalizedInference){
		List<String> targetWords = ebitgLexicalizedInference.getGeneratedTargetWords();
		IntegerKeyRuleRepresentation targetRuleRepresentation = IntegerKeyRuleRepresentation.createIntegerKeyRuleRepresentationWithAddedChiangLabel(targetWords, translationRuleProbabilityTable.getWordKeyMappingTable());
		return targetRuleRepresentation;
	}*/
	
	
	private CoreContextLabel getCoreContextLabelFromTrie(EbitgLexicalizedInference ebitgLexicalizedInference)
	{
		List<String> sourceWords = ebitgLexicalizedInference.generatedSourceWords();
		List<String> targetWords = ebitgLexicalizedInference.getGeneratedTargetWords();
		//System.out.println("getCoreContextLabelFromTrie -  sourceSideRuleString: " +  sourceSideRuleString +  "  targetSideRuleString: " +  targetSideRuleString);
		//IntegerKeyRuleRepresentation sourceRuleRepresentation = createKeyRuleRepresentationSource(ebitgLexicalizedInference); 
		//System.out.println("getCoreContextLabelFromTrie sourceSideRuleString: " + sourceSideRuleString + " sourceSideRuleRepresentation : " + sourceRuleRepresentation);
		//double uncertaintyScore = coreContextLabelsTable.getRulePhraseDisambiguationScore(sourceRuleRepresentation);
		//CoreContextLabel result = CoreContextLabel.createCoreContextLabel(sourceWords, targetWords, uncertaintyScore, coreContextLabelsTable.getWordKeyMappingTable());
		TranslationEquivalenceItem translationEquivalenceItem = TranslationEquivalenceItem.createXLabelExtendedTranslationEquivalenceItem(sourceWords, targetWords,this.coreContextLabelsTable.getWordKeyMappingTable());
		CoreContextLabel result = coreContextLabelsTable.getCoreContextLabel(translationEquivalenceItem);
		return result;
	}
	
	
	private List<Pair<EbitgChartEntry,CoreContextLabel>> computeChartEntryCoreContextLablPairsForMinimalPhrasePairs(EbitgChartBuilderCoreContextLabeled chartBuilder)
	{
		List<Pair<EbitgChartEntry,CoreContextLabel>> result = new ArrayList<Pair<EbitgChartEntry,CoreContextLabel>>();
		MinimalPhrasePairExtractor<EbitgLexicalizedInferenceCoreContextLabeled,EbitgTreeStateCoreContextLabeled> minimalPhrasePairExtractor = MinimalPhrasePairExtractor.createMinimalPhrasePairExtractor(chartBuilder);
		List<EbitgLexicalizedInference> minimalPhrasePairInferences = minimalPhrasePairExtractor.getMimimalPhrasePairInferences();
		
		for(EbitgLexicalizedInference inference : minimalPhrasePairInferences)
		{
			CoreContextLabel coreContextLabel = getCoreContextLabelFromTrie(inference);
			EbitgChartEntry  chartEntry = inference.getContainingChartEntry();
			result.add(new Pair<EbitgChartEntry, CoreContextLabel>(chartEntry, coreContextLabel));
		}
		return result;
	}
	
	public void fillCoreContextLabelsTableUsingChartBuilder(EbitgChartBuilderCoreContextLabeled chartBuilder)
	{
		List<Pair<EbitgChartEntry,CoreContextLabel>> labeldMinimalPhrasePairChartEntries = computeChartEntryCoreContextLablPairsForMinimalPhrasePairs(chartBuilder);
		this.chartCoreContextLabelsTable.fillCoreContextLabelsTableUsingLabeldMinimalPhrasePairChartEntries(labeldMinimalPhrasePairChartEntries);
	}
}
