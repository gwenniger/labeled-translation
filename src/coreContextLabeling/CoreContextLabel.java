package coreContextLabeling;

import java.util.List;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.SourceTargetEquivalencePair;
import grammarExtraction.translationRules.TranslationEquivalenceItem;

public class CoreContextLabel extends TranslationEquivalenceItem implements AmbiguityTuple{

	private static final long serialVersionUID = 1L;
	private final double sourceSideEntropy;
	private final double ruleProbability;
	private final int amiguityScoreRank;
	private final double relativeEntropyWeight;

	private CoreContextLabel(SourceTargetEquivalencePair sourceTargetEquivalencePair, double sourceSideEntropy, double ruleProbability, double relativeEntropyWeight, int amiguityScoreRank) {
		super(sourceTargetEquivalencePair);
		this.sourceSideEntropy = sourceSideEntropy;
		this.ruleProbability = ruleProbability;
		this.amiguityScoreRank = amiguityScoreRank;
		this.relativeEntropyWeight = relativeEntropyWeight;
	}

	public static CoreContextLabel createCoreContextLabel(TranslationEquivalenceItem translationEquivalenceItem, double sourceSideEntropy, double ruleProbability, double relativeEntropyWeight,
			int amiguityScoreRank) {
		return new CoreContextLabel(translationEquivalenceItem.getSourceTargetEquivalencePair(), sourceSideEntropy, ruleProbability, relativeEntropyWeight, amiguityScoreRank);
	}

	public static CoreContextLabel createCoreContextLabel(List<String> sourceSideWords, List<String> targetSideWords, double sourceSideEntropy, double ruleProbability,
			WordKeyMappingTable wordKeyMappingTable, int relativeEntropyWeight, int amiguityScoreRank) {
		SourceTargetEquivalencePair sourceTargetEquivalencePair = SourceTargetEquivalencePair.createSourceTargetEquivalencePair(sourceSideWords, targetSideWords, wordKeyMappingTable);
		return new CoreContextLabel(sourceTargetEquivalencePair, sourceSideEntropy, ruleProbability, relativeEntropyWeight, amiguityScoreRank);
	}

	public static double getAmbiguityScore(double conditionSideEntropy, double conditionSideProbability, double relativeEntropyWeight) {
		double logConditionSideProbability = Math.log(conditionSideProbability);
		// Entropy will become bigger for higher ambiguity,
		// logConditionSideProbability will become lower for lower probabilities
		double totalWeight = 1 + relativeEntropyWeight;
		double entropyWeight = relativeEntropyWeight / totalWeight;
		double probabilityWeight = 1 / totalWeight;

		return (probabilityWeight * logConditionSideProbability) + (entropyWeight * conditionSideEntropy);
	}

	public double getAmbiguityScore() {

		return getAmbiguityScore(this.sourceSideEntropy, this.ruleProbability, this.relativeEntropyWeight);
	}

	public boolean equals(Object ruleObject) {
		return super.equals(ruleObject);
	}

	public int getAmbiguityScoreRank() {
		return this.amiguityScoreRank;
	}

	public double getSourceSideEntropy() {
		return this.sourceSideEntropy;
	}

	public double getSourceSideLogProbability() {
		return Math.log(this.ruleProbability);
	}

	public String toString() {
		return "<CCL>" + this.sourceTargetEquivalencePair.getSourceSideRepresentation().toString() + this.sourceTargetEquivalencePair.getTargetSideRepresentation().toString() + " ||| "
				+ "ambiguityScore: " + this.getAmbiguityScore() + "</CCL>";
	}

}
