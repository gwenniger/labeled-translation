package coreContextLabeling;

import java.util.List;
import bitg.Pair;
import extended_bitg.EbitgChartEntry;
import extended_bitg.EbitgLexicalizedInference;
import util.Span;

public class ChartCoreContextLabelsTable {
	private final CoreContextLabel[][] chartCoreContextLabels;

	private ChartCoreContextLabelsTable(CoreContextLabel[][] chartCoreContextLabels) {
		this.chartCoreContextLabels = chartCoreContextLabels;
	}

	public static ChartCoreContextLabelsTable createChartCoreContextLabelsTable(int sourceLength) {
		CoreContextLabel[][] chartCoreContextLabels = new CoreContextLabel[sourceLength][sourceLength];
		return new ChartCoreContextLabelsTable(chartCoreContextLabels);
	}

	public static ChartCoreContextLabelsTable createCoreContextLabelsTable(CoreContextLabel[][] chartCoreContextLabels) {
		return new ChartCoreContextLabelsTable(chartCoreContextLabels);
	}

	public CoreContextLabel getCoreContextLabel(int i, int j) {
		CoreContextLabel result = this.chartCoreContextLabels[i][j];
		// System.out.println("getCoreContextLabel(" + i + ","+ j + ") result: "
		// + result);
		return result;
	}

	protected CoreContextLabel getCoreContextLabelForChartEntry(EbitgChartEntry chartEntry) {
		// System.out.println("getCoreContextLabelForChartEntry - chartEntry:" +
		// chartEntry);
		return getCoreContextLabel(chartEntry.getI(), chartEntry.getJ());
	}

	public CoreContextLabel getCoreContextLabelForInference(EbitgLexicalizedInference ebitgLexicalizedInference) {
		assert (ebitgLexicalizedInference != null);
		return getCoreContextLabelForChartEntry(ebitgLexicalizedInference.getContainingChartEntry());
	}

	private void fillMimimalPhrasePairEntries(List<Pair<EbitgChartEntry, CoreContextLabel>> labeledMinimalPhrasePairChartEntries) {
		for (Pair<EbitgChartEntry, CoreContextLabel> chartEntryCoreContextLabelPair : labeledMinimalPhrasePairChartEntries) {
			EbitgChartEntry chartEntry = chartEntryCoreContextLabelPair.first;
			CoreContextLabel coreContextLabel = chartEntryCoreContextLabelPair.last;
			this.chartCoreContextLabels[chartEntry.getI()][chartEntry.getJ()] = coreContextLabel;
		}
	}

	private int getSourceLength() {
		return this.chartCoreContextLabels.length;
	}

	private CoreContextLabel getHighestUncertaintyLabelForTwoNotNullLabels(CoreContextLabel label1, CoreContextLabel label2) {
		if (label1.getAmbiguityScore() > label2.getAmbiguityScore()) {
			return label1;
		} else {
			return label2;
		}
	}

	private boolean bothLabelsAreNull(CoreContextLabel label1, CoreContextLabel label2) {
		return (label1 == null) && (label2 == null);
	}

	private boolean bothLabelsAreNotNull(CoreContextLabel label1, CoreContextLabel label2) {
		return (label1 != null) && (label2 != null);
	}

	private CoreContextLabel getHighestUncertaintyLabel(CoreContextLabel label1, CoreContextLabel label2) {
		if (bothLabelsAreNotNull(label1, label2)) {
			return getHighestUncertaintyLabelForTwoNotNullLabels(label1, label2);
		} else if (bothLabelsAreNull(label1, label2)) {
			return null;
		} else if (label1 == null) {
			assert (label1 == null);
			return label2;
		} else {
			assert (label2 == null);
			return label1;
		}
	}

	/**
	 * This method computes the CoreContextLabels for the non-minimal chart
	 * entries, after the minimal entries have been added first.
	 * 
	 * This method uses the dynamic programming fact that for any entry in the
	 * table, the label can be computed by taking the labels of the two entries
	 * it covers. Keep in mind with this that entries may contain null values as
	 * well, this is no problem, and also these entries do not correspond
	 * directly to chart entries that have inferences (they may but do not have
	 * to). This makes that the algorithm works, by taking the maximum label
	 * over the two (overlapping) left and right sub entries, without requiring
	 * a third loop to go over split points or anything.
	 * 
	 * Crucially, we do not need to loop over different splits. Instead this
	 * algorithm works based on the fact that for every entry in the label
	 * chart, there are two sub entries that are 1 smaller ( a left and a right
	 * one). Now based on the labels of these sub entries (which may contain
	 * nulls) we can directly compute the label for the bigger chart entry. You
	 * can actually proof that this is correct by "copmlete induction" - it is a
	 * dynamic programming algorithm in the end. The helper method
	 * getHighestUncertaintyLabel is also vital in this construction. It allows
	 * computing the maximum uncertainty label over two labels, even if one ore
	 * two are null. Hence we can assign maximum ambiguity labels to table
	 * entries, even though they do not necessarily have derivations in the
	 * chart. This is necessary so that starting from bottom up, we always have
	 * labels for the two sub entries of any entry in the label table (while
	 * these may be null) . Without this the algorithm, which needs only two
	 * loops, would not work.
	 */
	public void fillNonMinimalEntries() {
		for (int spanLength = 2; spanLength <= getSourceLength(); spanLength++) {
			for (int spanBegin = 0; spanBegin < getSourceLength() - spanLength + 1; spanBegin++) {
				int i = spanBegin;
				int j = spanBegin + spanLength - 1;

				if (this.chartCoreContextLabels[i][j] == null) {
					CoreContextLabel oldBest = null;
					Span leftSpanOneSmallerThanFullSpan = new Span(i, j - 1);
					Span rightSpanOneSmallerThanFullSpan = new Span(i + 1, j);
					CoreContextLabel leftSubSpanLabel = this.getCoreContextLabel(leftSpanOneSmallerThanFullSpan.getFirst(),
							leftSpanOneSmallerThanFullSpan.getSecond());
					CoreContextLabel rightSubSpanLabel = this.getCoreContextLabel(rightSpanOneSmallerThanFullSpan.getFirst(),
							rightSpanOneSmallerThanFullSpan.getSecond());
					CoreContextLabel leftRightBest = getHighestUncertaintyLabel(leftSubSpanLabel, rightSubSpanLabel);
					oldBest = getHighestUncertaintyLabel(leftRightBest, oldBest);

					this.chartCoreContextLabels[i][j] = oldBest;
				}
			}
		}

	}

	@SuppressWarnings("unused")
	/**
	 * Method mostly made for testing
	 * @param labeledMinimalPhrasePairChartEntries
	 * @return
	 */
	private CoreContextLabel getMostAmbiguousCoreContextLabel(List<Pair<EbitgChartEntry, CoreContextLabel>> labeledMinimalPhrasePairChartEntries) {
		CoreContextLabel result = null;
		if (labeledMinimalPhrasePairChartEntries.size() > 0) {
			result = labeledMinimalPhrasePairChartEntries.get(0).last;

			for (Pair<EbitgChartEntry, CoreContextLabel> pair : labeledMinimalPhrasePairChartEntries) {
				CoreContextLabel coreContextLabel = pair.last;
				if (coreContextLabel.getAmbiguityScore() > result.getAmbiguityScore()) {
					result = coreContextLabel;
				}
			}
		}
		return result;
	}

	public void fillCoreContextLabelsTableUsingLabeldMinimalPhrasePairChartEntries(
			List<Pair<EbitgChartEntry, CoreContextLabel>> labeledMinimalPhrasePairChartEntries) {
		fillMimimalPhrasePairEntries(labeledMinimalPhrasePairChartEntries);
		fillNonMinimalEntries();

		// int arrayLength = chartCoreContextLabels.length;
		// CoreContextLabel topLabel = chartCoreContextLabels[0][arrayLength
		// -1];
		// Assert.assertEquals(getMostAmbiguousCoreContextLabel(labeledMinimalPhrasePairChartEntries),topLabel);
	}

	public void printTable() {
		for (int spanLength = 1; spanLength <= getSourceLength(); spanLength++) {
			for (int spanBegin = 0; spanBegin < getSourceLength() - spanLength + 1; spanBegin++) {
				int i = spanBegin;
				int j = spanBegin + spanLength - 1;
				System.out.println("chartCoreContextLabels[" + i + "][" + j + "] " + this.chartCoreContextLabels[i][j]);
			}
		}
	}
}
