package coreContextLabeling;

import grammarExtraction.IntegerKeyRuleRepresentation;
import grammarExtraction.translationRuleTrie.RuleCountsTable;

public class SourceSideAmbiguityTuple implements AmbiguityTuple {

	final IntegerKeyRuleRepresentation sourceSideRepresentation;
	final RuleCountsTable ruleCountsTable;
	final double sourceSideEntropy;
	final double sourceSideRuleProbability;
	final double relativeEntropyWeight;

	public SourceSideAmbiguityTuple(IntegerKeyRuleRepresentation sourceSideRepresentation, RuleCountsTable ruleCountsTable, double sourceSideEntropy,
			double sourceSideRuleProbability,double relativeEntropyWeight) {
		this.sourceSideRepresentation = sourceSideRepresentation;
		this.ruleCountsTable = ruleCountsTable;
		this.sourceSideEntropy = sourceSideEntropy;
		this.sourceSideRuleProbability = sourceSideRuleProbability;
		this.relativeEntropyWeight = relativeEntropyWeight;
	}

	public double getAmbiguityScore() {
		return CoreContextLabel.getAmbiguityScore(this.sourceSideEntropy, this.sourceSideRuleProbability,this.relativeEntropyWeight);
	}
}
