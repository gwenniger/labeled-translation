package coreContextLabeling;

import java.text.NumberFormat;
import java.util.ArrayList;

import extended_bitg.EbitgInference;
import extended_bitg.EbitgNode;
import extended_bitg.EbitgTreeState;
import extended_bitg.EbitgTreeStateChartBacking;
import extended_bitg.TreeRepresentation;

public class EbitgTreeStateCoreContextLabeled extends EbitgTreeState<EbitgLexicalizedInferenceCoreContextLabeled, EbitgTreeStateCoreContextLabeled> {
	protected String coreContextLabelString;
	private static NumberFormat ShortNumberFormat = createNumberFormat();

	protected EbitgTreeStateCoreContextLabeled(
			EbitgTreeStateChartBacking<EbitgLexicalizedInferenceCoreContextLabeled, EbitgTreeStateCoreContextLabeled> treeStateChartBacking, EbitgNode node,
			ArrayList<EbitgInference> inferences, EbitgTreeStateCoreContextLabeled parent, int parentRelativeChildNumber, int parentRelativePermutationGroup,
			String nodeID, String nodeLabel, boolean hasMoreInferences) {
		super(treeStateChartBacking, node, inferences, parent, parentRelativeChildNumber, parentRelativePermutationGroup, nodeID, nodeLabel, hasMoreInferences);
	}

	@Override
	public EbitgTreeStateCoreContextLabeled createEbitgTreeState(
			EbitgTreeStateChartBacking<EbitgLexicalizedInferenceCoreContextLabeled, EbitgTreeStateCoreContextLabeled> treeStateChartBacking, EbitgNode node,
			ArrayList<EbitgInference> inferences, EbitgTreeStateCoreContextLabeled parent, int parentRelativeChildNumber, int parentRelativePermutationGroup) {
		return createEbitgTreeStateCoreContextLabeled(treeStateChartBacking, node, inferences, parent, parentRelativeChildNumber,
				parentRelativePermutationGroup);
	}

	public static EbitgTreeStateCoreContextLabeled createEbitgTreeStateCoreContextLabeled(
			EbitgTreeStateChartBacking<EbitgLexicalizedInferenceCoreContextLabeled, EbitgTreeStateCoreContextLabeled> treeStateChartBacking, EbitgNode node,
			ArrayList<EbitgInference> inferences, EbitgTreeStateCoreContextLabeled parent, int parentRelativeChildNumber, int parentRelativePermutationGroup) {
		boolean hasMoreInferences = true;
		EbitgTreeStateCoreContextLabeled state = new EbitgTreeStateCoreContextLabeled(treeStateChartBacking, node, inferences, parent,
				parentRelativeChildNumber, parentRelativePermutationGroup, computeNodeIDFromParent(parent, parentRelativeChildNumber),
				computeNodeLabelFromParent(parent, parentRelativePermutationGroup), hasMoreInferences);
		return state;
	}

	private static NumberFormat createNumberFormat()
	{
		NumberFormat result = NumberFormat.getInstance();
		result.setMaximumFractionDigits(3);
		return result;
	}
	
	@Override
	protected void collectInformationForExtraLabels() {
		System.out.println("EbitgTreeStateCoreContextLabeled.collectInformationForExtraLabels() - EbitgTreeStateCoreContextLabeled: " + this);
		System.out.println("this.getChosenInfernce()" + this.getChosenInfernce());

		if (!this.getChosenInfernce().isComplete()) {
			coreContextLabelString = "";
		} else if (this.isMinimalPhrasePairState()) {
			coreContextLabelString = this.getNodeLabel();
		} else {
			coreContextLabelString = getCoreContextLabelChildState().coreContextLabelString;
		}
	}

	private EbitgTreeStateCoreContextLabeled getCoreContextLabelChildState() {
		return this.getChildTreeState(getCoreContextLabelChildIndex());
	}

	private int getCoreContextLabelChildIndex() {
		int index = 0;
		assert (getChildInferences().size() > 0);
		for (EbitgLexicalizedInferenceCoreContextLabeled childInference : getChildInferences()) {
			System.out.println("getCoreContextLabel(): " + getCoreContextLabel());
			System.out.println("childInference.getCoreContextLabel(): " + childInference.getCoreContextLabel());

			if (getCoreContextLabel() == childInference.getCoreContextLabel()) {
				return index;
			}
			index++;
		}
		throw new RuntimeException("Error : Trying to find the core contex label child index for the wrong type of tree state");
	}

	private CoreContextLabel getCoreContextLabel() {
		return this.getChosenInfernce().getCoreContextLabel();
	}

	public String getCoreContextLabelString() {
		return this.coreContextLabelString;
	}

	public String getCoreContextLabelAmbiguityInfoString() {
		CoreContextLabel coreContextLabel = this.getCoreContextLabel();
		if (coreContextLabel != null) {
			return "" + coreContextLabel.getAmbiguityScoreRank()+  "|" + ShortNumberFormat.format(coreContextLabel.getSourceSideEntropy()) + "|" + ShortNumberFormat.format(coreContextLabel.getSourceSideLogProbability());
		}
		return "";
	}

	@Override
	protected TreeRepresentation<EbitgLexicalizedInferenceCoreContextLabeled, EbitgTreeStateCoreContextLabeled> createTreeRepresentation(
			EbitgTreeStateCoreContextLabeled containingState)

	{
		return TreeRepresentationCoreContextLabeled.createEbitgTreeStateTreeRepresentation(containingState);
	}

}
