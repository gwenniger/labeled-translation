package coreContextLabeling;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import util.ConfigFile;
import util.Serialization;

import bitg.Pair;

import grammarExtraction.IntegerKeyRuleRepresentation;
import grammarExtraction.IntegerKeyRuleRepresentationWithLabel;
import grammarExtraction.grammarExtractionFoundation.ConcurrencySettings;
import grammarExtraction.translationRuleTrie.CountItem;
import grammarExtraction.translationRuleTrie.RuleCountsTable;
import grammarExtraction.translationRuleTrie.TranslationRuleProbabilityTable;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.TranslationEquivalenceItem;

public class CoreContextLabelsTable implements Serializable {
	public static final String RelativeEntropyWeightCoreContextLabelsProperty = "relativeEntropyWeightCoreContextLabels";
	public static final String CoreContextLabelProbabilitySettingProperty = "coreContextLabelProbabilitySetting";
	public static final String CoreContextLabelSourceProbabilitySetting = "source";
	public static final String CoreContextLabelSourceTargetProbabilitySetting = "source-target";
	public static final List<String> CoreContextLabelProbabilitySettings = Arrays.asList(CoreContextLabelSourceProbabilitySetting, CoreContextLabelSourceTargetProbabilitySetting);
	private static final Pattern WhiteSpacePattern = Pattern.compile("\\s+");
	private static final long serialVersionUID = 1L;
	private final ConcurrentHashMap<TranslationEquivalenceItem, CoreContextLabel> coreContextLabelsTable;
	private final WordKeyMappingTable wordKeyMappingTable;

	private CoreContextLabelsTable(ConcurrentHashMap<TranslationEquivalenceItem, CoreContextLabel> coreContextLabelsTable, WordKeyMappingTable wordKeyMappingTable) {
		this.coreContextLabelsTable = coreContextLabelsTable;
		this.wordKeyMappingTable = wordKeyMappingTable;
	}

	public static CoreContextLabelsTable createCoreContextLabelsTableGettingProbabilitySettingFromConfig(TranslationRuleProbabilityTable minimalPhrasePairRuleTable, int noConcurrentThreads,
			ConfigFile theConfig) {
		String probabilitySettingsString = getProbabilitySettingString(theConfig);
		String entropyWeightString = theConfig.getValueWithPresenceCheck(RelativeEntropyWeightCoreContextLabelsProperty);
		double relativeEntropyWeight = Double.parseDouble(entropyWeightString);
		if (probabilitySettingsString.equals(CoreContextLabelSourceProbabilitySetting)) {
			return new CoreContextLabelsTable(computeCoreContexLabelsTableUsingSourceSideProbability(minimalPhrasePairRuleTable, noConcurrentThreads, relativeEntropyWeight),
					minimalPhrasePairRuleTable.getWordKeyMappingTable());
		} else if (probabilitySettingsString.equals(CoreContextLabelSourceTargetProbabilitySetting)) {
			return new CoreContextLabelsTable(computeCoreContexLabelsTableUsingSourceTargetPairProbability(minimalPhrasePairRuleTable, noConcurrentThreads, relativeEntropyWeight),
					minimalPhrasePairRuleTable.getWordKeyMappingTable());
		} else {
			// This should never be reached, since the method
			// getProbabilitySettingString takes care of assuring proper values
			// are received
			throw new RuntimeException("Error in CoreContextLabelsTable createCoreContextLabelsTableGettingProbabilitySettingFromConfig");
		}
	}

	public static CoreContextLabelsTable createCoreContextLabelsTableUsingSourceSideProbability(TranslationRuleProbabilityTable minimalPhrasePairRuleTable, int noConcurrentThreads,
			int relativeEntropyWeight) {
		return new CoreContextLabelsTable(computeCoreContexLabelsTableUsingSourceSideProbability(minimalPhrasePairRuleTable, noConcurrentThreads, relativeEntropyWeight),
				minimalPhrasePairRuleTable.getWordKeyMappingTable());
	}

	public static CoreContextLabelsTable createCoreContextLabelsTableUsingSourceTargetPairProbability(TranslationRuleProbabilityTable minimalPhrasePairRuleTable, int noConcurrentThreads,
			int relativeEntropyWeight) {
		return new CoreContextLabelsTable(computeCoreContexLabelsTableUsingSourceTargetPairProbability(minimalPhrasePairRuleTable, noConcurrentThreads, relativeEntropyWeight),
				minimalPhrasePairRuleTable.getWordKeyMappingTable());
	}

	public CoreContextLabel getCoreContextLabel(TranslationEquivalenceItem translationEquivalenceItem) {
		CoreContextLabel result = this.coreContextLabelsTable.get(translationEquivalenceItem);
		if (result != null) {
			return result;
		} else {
			throw new RuntimeException("Core contex label null for : " + translationEquivalenceItem);
		}
	}

	private static List<SourceSideAmbiguityTuple> getSourceSideAmbiguityTuples(TranslationRuleProbabilityTable minimalPhrasePairRuleTable, double relativeEntropyWeight) {
		List<SourceSideAmbiguityTuple> result = new ArrayList<SourceSideAmbiguityTuple>();

		Enumeration<Pair<IntegerKeyRuleRepresentationWithLabel, RuleCountsTable>> targetGivenSourceRuleCountsTableEnumeration = minimalPhrasePairRuleTable
				.getTargetGivenSourceRuleCountTableEnumeration();

		while (targetGivenSourceRuleCountsTableEnumeration.hasMoreElements()) {
			Pair<IntegerKeyRuleRepresentationWithLabel, RuleCountsTable> sourceLabelTablePair = targetGivenSourceRuleCountsTableEnumeration.nextElement();
			IntegerKeyRuleRepresentationWithLabel sourceSideRepresentation = sourceLabelTablePair.first;

			double sourceSideEntropy = minimalPhrasePairRuleTable.getSourceToTargetRulePhraseEntropy(sourceSideRepresentation);
			double sourceSideRuleProbability = minimalPhrasePairRuleTable.getSourceSideRuleProbability(sourceSideRepresentation);

			result.add(new SourceSideAmbiguityTuple(sourceSideRepresentation, sourceLabelTablePair.last, sourceSideEntropy, sourceSideRuleProbability, relativeEntropyWeight));
		}
		return result;
	}

	private static List<SourceSideAmbiguityTuple> getSourceSideAmbiguityTuplesOrderedByAmbiguityDescending(TranslationRuleProbabilityTable minimalPhrasePairRuleTable, double relativeEntropyWeight) {
		List<SourceSideAmbiguityTuple> result = getSourceSideAmbiguityTuples(minimalPhrasePairRuleTable, relativeEntropyWeight);
		result.sort(Collections.reverseOrder(new AmbiguityTupleComparator()));
		return result;
	}

	private static List<SourceTargetAmbiguityTuple> getSourceTargetSideAmbiguityTuples(TranslationRuleProbabilityTable minimalPhrasePairRuleTable, double relativeEntropyWeight) {
		List<SourceTargetAmbiguityTuple> result = new ArrayList<SourceTargetAmbiguityTuple>();

		Enumeration<Pair<IntegerKeyRuleRepresentationWithLabel, RuleCountsTable>> targetGivenSourceRuleCountsTableEnumeration = minimalPhrasePairRuleTable
				.getTargetGivenSourceRuleCountTableEnumeration();

		while (targetGivenSourceRuleCountsTableEnumeration.hasMoreElements()) {
			Pair<IntegerKeyRuleRepresentationWithLabel, RuleCountsTable> sourceLabelTablePair = targetGivenSourceRuleCountsTableEnumeration.nextElement();
			IntegerKeyRuleRepresentationWithLabel sourceSideRepresentation = sourceLabelTablePair.first;
			RuleCountsTable ruleCountsTable = sourceLabelTablePair.last;

			double sourceSideEntropy = minimalPhrasePairRuleTable.getSourceToTargetRulePhraseEntropy(sourceSideRepresentation);

			for (CountItem countItem : ruleCountsTable.getAllCountItems()) {
				IntegerKeyRuleRepresentationWithLabel targetSideRepresentation = countItem.getLabel().restoreRuleRepresentation();
				TranslationEquivalenceItem sourceTargetPair = TranslationEquivalenceItem.createTranslationEquivalenceItem(sourceSideRepresentation, targetSideRepresentation);
				double sourceTargetRuleProbability = minimalPhrasePairRuleTable.getSourceTargetPairProbability(sourceSideRepresentation, targetSideRepresentation);
				result.add(new SourceTargetAmbiguityTuple(sourceTargetPair, sourceSideEntropy, sourceTargetRuleProbability, relativeEntropyWeight));
			}
		}
		return result;
	}

	private static List<SourceTargetAmbiguityTuple> getSourceTargetAmbiguityTuplesOrderedByAmbiguityDescending(TranslationRuleProbabilityTable minimalPhrasePairRuleTable, double relativeEntropyWeight) {
		List<SourceTargetAmbiguityTuple> result = getSourceTargetSideAmbiguityTuples(minimalPhrasePairRuleTable, relativeEntropyWeight);
		result.sort(Collections.reverseOrder(new AmbiguityTupleComparator()));
		return result;
	}

	private static ConcurrentHashMap<TranslationEquivalenceItem, CoreContextLabel> computeCoreContexLabelsTableUsingSourceSideProbability(TranslationRuleProbabilityTable minimalPhrasePairRuleTable,
			int noConcurrentThreads, double relativeEntropyWeight) {
		ConcurrentHashMap<TranslationEquivalenceItem, CoreContextLabel> coreContextLabelsTable = new ConcurrentHashMap<TranslationEquivalenceItem, CoreContextLabel>(
				ConcurrencySettings.ConcurrentHashMapInitialCapacity, ConcurrencySettings.ConcurrentHashMapLoadFactor, ConcurrencySettings.getConcurrencyLevel(noConcurrentThreads));

		List<SourceSideAmbiguityTuple> sourceSideAmgiguityTuplesOrderedByAmbiguityDescending = getSourceSideAmbiguityTuplesOrderedByAmbiguityDescending(minimalPhrasePairRuleTable,
				relativeEntropyWeight);

		int ambiguityRanking = 0;
		for (SourceSideAmbiguityTuple sourceSideAmbiguityTuple : sourceSideAmgiguityTuplesOrderedByAmbiguityDescending) {

			Enumeration<IntegerKeyRuleRepresentation> targetSidesEnumeration = sourceSideAmbiguityTuple.ruleCountsTable.getUnlabeledConditionedSideRuleCountsTableConditionedSidesEnumeration();
			while (targetSidesEnumeration.hasMoreElements()) {
				IntegerKeyRuleRepresentation targetSideRepresentation = targetSidesEnumeration.nextElement();
				TranslationEquivalenceItem translationEquivalenceItem = TranslationEquivalenceItem.createTranslationEquivalenceItem(sourceSideAmbiguityTuple.sourceSideRepresentation,
						targetSideRepresentation);
				CoreContextLabel coreContextLabel = CoreContextLabel.createCoreContextLabel(translationEquivalenceItem, sourceSideAmbiguityTuple.sourceSideEntropy,
						sourceSideAmbiguityTuple.sourceSideRuleProbability, relativeEntropyWeight, ambiguityRanking);
				// Add the CoreContextLabel to the table
				// System.out.println("adding key-value pair to coreContextTable");
				coreContextLabelsTable.put(translationEquivalenceItem, coreContextLabel);
				ambiguityRanking++;
			}
		}

		// printCoreContextLabelsTableEntries();
		return coreContextLabelsTable;
	}

	private static ConcurrentHashMap<TranslationEquivalenceItem, CoreContextLabel> computeCoreContexLabelsTableUsingSourceTargetPairProbability(
			TranslationRuleProbabilityTable minimalPhrasePairRuleTable, int noConcurrentThreads, double relativeEntropyWeight) {
		ConcurrentHashMap<TranslationEquivalenceItem, CoreContextLabel> coreContextLabelsTable = new ConcurrentHashMap<TranslationEquivalenceItem, CoreContextLabel>(
				ConcurrencySettings.ConcurrentHashMapInitialCapacity, ConcurrencySettings.ConcurrentHashMapLoadFactor, ConcurrencySettings.getConcurrencyLevel(noConcurrentThreads));

		List<SourceTargetAmbiguityTuple> sourceTargetAmgiguityTuplesOrderedByAmbiguityDescending = getSourceTargetAmbiguityTuplesOrderedByAmbiguityDescending(minimalPhrasePairRuleTable,
				relativeEntropyWeight);

		int ambiguityRanking = 0;
		for (SourceTargetAmbiguityTuple sourceTargetAmbiguityTuple : sourceTargetAmgiguityTuplesOrderedByAmbiguityDescending) {

			CoreContextLabel coreContextLabel = CoreContextLabel.createCoreContextLabel(sourceTargetAmbiguityTuple.sourceTargetSideEquivalenceItem, sourceTargetAmbiguityTuple.sourceSideEntropy,
					sourceTargetAmbiguityTuple.sourceTargetRuleProbability, relativeEntropyWeight, ambiguityRanking);
			// Add the CoreContextLabel to the table
			// System.out.println("adding key-value pair to coreContextTable");
			coreContextLabelsTable.put(sourceTargetAmbiguityTuple.sourceTargetSideEquivalenceItem, coreContextLabel);
			ambiguityRanking++;
		}
		// printCoreContextLabelsTableEntries();
		return coreContextLabelsTable;
	}

	public void printCoreContextLabelsTableEntries() {
		for (Entry<TranslationEquivalenceItem, CoreContextLabel> entry : coreContextLabelsTable.entrySet()) {
			System.out.println("CoreContextLabelsTableEntry: \n" + entry);
		}
	}

	public WordKeyMappingTable getWordKeyMappingTable() {
		return this.wordKeyMappingTable;
	}

	private String replaceWhiteSpaceWithUnderScores(String string) {
		Matcher matcher = WhiteSpacePattern.matcher(string);
		return matcher.replaceAll("_");
	}

	public String getCoreContextLabelWordString(CoreContextLabel coreContexLabel) {
		String result = "<CCL>";
		result += coreContexLabel.getSourceSideRepresentation().getStringRepresentation(wordKeyMappingTable) + "___"
				+ coreContexLabel.getTargetSideRepresentation().getStringRepresentation(wordKeyMappingTable);
		result += "</CCL>";
		result = replaceWhiteSpaceWithUnderScores(result);
		return result;
	}

	public static String getCoreContextLabelsTableFileName(ConfigFile configFile) {
		return configFile.getValueWithPresenceCheck(EbitgChartBuilderCoreContextLabeled.CoreContextLabelTableFileNameConfigurationProperty);
	}

	public static CoreContextLabelsTable getCoreContextLabelsTableFromSerializedFile(String fileName) {
		return Serialization.unSerializeOBject(fileName);
	}

	private static String getProbabilitySettingString(ConfigFile theConfig) {
		String result = theConfig.getValueWithPresenceCheck(CoreContextLabelProbabilitySettingProperty);
		if (CoreContextLabelProbabilitySettings.contains(result)) {
			return result;
		} else {
			String errorMessage = "\nError : unrecognized value for required parameter " + CoreContextLabelProbabilitySettingProperty + "\n";
			errorMessage += "Pleas choose : \"" + CoreContextLabelSourceProbabilitySetting + "\" for using (only) the source probability in determing the ambiguity score " + " of CoreContextLabels\n";
			errorMessage += "Alternatively choose : \"" + CoreContextLabelSourceTargetProbabilitySetting + "\" for using the source-target pair probability in determing the ambiguity score "
					+ " of CoreContextLabels";
			throw new RuntimeException(errorMessage);
		}
	}

}