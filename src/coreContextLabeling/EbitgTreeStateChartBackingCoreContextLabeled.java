package coreContextLabeling;

import hat_lexicalization.Lexicalizer;
import extended_bitg.EbitgChartBuilder;
import extended_bitg.EbitgTreeStateChartBacking;

public class EbitgTreeStateChartBackingCoreContextLabeled extends EbitgTreeStateChartBacking<EbitgLexicalizedInferenceCoreContextLabeled,EbitgTreeStateCoreContextLabeled>
{
	protected EbitgTreeStateChartBackingCoreContextLabeled(
			EbitgChartBuilder<EbitgLexicalizedInferenceCoreContextLabeled,EbitgTreeStateCoreContextLabeled> chartBuilder,
			Lexicalizer lexicalizer) 
	{
		super(chartBuilder, lexicalizer);
	}

	public static EbitgTreeStateChartBackingCoreContextLabeled  createBasicEbitgTreeStateChartBackingCoreContextLabeled(EbitgChartBuilder<EbitgLexicalizedInferenceCoreContextLabeled,EbitgTreeStateCoreContextLabeled> chartBuilder)
	{
		return new EbitgTreeStateChartBackingCoreContextLabeled(chartBuilder, chartBuilder.getLexicalizer());
	}
}
