package coreContextLabeling;

import java.util.ArrayList;
import java.util.List;

import util.XMLTagging;

import extended_bitg.TreeRepresentation;

public class TreeRepresentationCoreContextLabeled extends TreeRepresentation<EbitgLexicalizedInferenceCoreContextLabeled, EbitgTreeStateCoreContextLabeled> {

	public static String CoreContextLabelProperty = "CCL";
	public static String CoreContextLabelAmbiguityRankProperty = "CCLAmbiguityRank";

	

	
	protected TreeRepresentationCoreContextLabeled(EbitgTreeStateCoreContextLabeled containingState, String treeString, String twinTreeString) {
		super(containingState, treeString, twinTreeString);
	}

	public static TreeRepresentationCoreContextLabeled createEbitgTreeStateTreeRepresentation(EbitgTreeStateCoreContextLabeled containingState) {
		return new TreeRepresentationCoreContextLabeled(containingState, null, null);
	}


	
	private static String getCoreContextLabelLeftTag() {
		return XMLTagging.getLeftXMLTag(CoreContextLabelProperty);
	}

	private static String getCoreContextLabelRightTag() {
		return XMLTagging.getRightXMLTag(CoreContextLabelProperty);
	}

	private static String getCoreContextLabeAmbiguityRanklLeftTag() {
		return XMLTagging.getLeftXMLTag(CoreContextLabelAmbiguityRankProperty);
	}

	private static String getCoreContextLabelAmbiguityRankRightTag() {
		return XMLTagging.getRightXMLTag(CoreContextLabelAmbiguityRankProperty);
	}

	private String getCoreContextLabelAmbiguityInfoString() {
		String ambiguityScoreString = getCoreContextLabeAmbiguityRanklLeftTag() + this.getContainingState().getCoreContextLabelAmbiguityInfoString()
				+ getCoreContextLabelAmbiguityRankRightTag();
		return ambiguityScoreString;
	}

	private String getCoreContextLabelString() {
		String coreContextLabelString = this.getContainingState().getCoreContextLabelString();
		return getCoreContextLabelLeftTag() + "@" + coreContextLabelString + "@" + getCoreContextLabelRightTag();
	}

	@Override
	protected List<String> getAdditionalExtraLabels() {
		List<String> result = new ArrayList<String>();
		String coreContextLabelString = getCoreContextLabelString();
		result.add(coreContextLabelString);
		if (coreContextLabelString.length() > 0) {
			result.add(getCoreContextLabelAmbiguityInfoString());
		}
		return result;
	}

}
