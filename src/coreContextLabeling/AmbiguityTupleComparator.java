package coreContextLabeling;

import java.util.Comparator;

public class AmbiguityTupleComparator implements Comparator<AmbiguityTuple> {
	@Override
	public int compare(AmbiguityTuple o1, AmbiguityTuple o2) {
		return Double.compare(o1.getAmbiguityScore(), o2.getAmbiguityScore());
	}
}
