package coreContextLabeling;

import java.util.List;

import ccgLabeling.CCGLabels;
import coreContextLabeling.CoreContextLabel;
import extended_bitg.EbitgChartEntry;
import extended_bitg.EbitgInferenceLexicalization;
import extended_bitg.EbitgInferencePermutationProperties;
import extended_bitg.EbitgInferenceTargetProperties;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgNode;

public class EbitgLexicalizedInferenceCoreContextLabeled extends EbitgLexicalizedInference
{

	private final CoreContextLabel coreContextLabel;
	
	
	public EbitgLexicalizedInferenceCoreContextLabeled(
			EbitgInferenceTargetProperties targetProperties,
			List<EbitgNode> nodes, List<EbitgChartEntry> nodeChartEntries,
			boolean inverted, InferenceType inferenceType,
			EbitgChartEntry containingChartEntry,
			EbitgInferenceLexicalization lexicalization,
			EbitgInferencePermutationProperties permutationProperties,
			CCGLabels ccgLabels,
			CoreContextLabel coreContextLabel) {
		super(targetProperties, nodes, nodeChartEntries, inverted, inferenceType,
				containingChartEntry, lexicalization, permutationProperties, ccgLabels);
		this.coreContextLabel = coreContextLabel;
	}

	
	
	public static EbitgLexicalizedInferenceCoreContextLabeled createEbitgLexicalizedInferenceCoreContextLabeled(EbitgLexicalizedInference ebitgLexicalizedInference, CoreContextLabel coreContextLabe)
	{
		return new EbitgLexicalizedInferenceCoreContextLabeled(ebitgLexicalizedInference.getTargetProperties(), ebitgLexicalizedInference.getNodes(), ebitgLexicalizedInference.getNodeChartEntries(), ebitgLexicalizedInference.isInverted(), ebitgLexicalizedInference.getInferenceType(), ebitgLexicalizedInference.getContainingChartEntry(), ebitgLexicalizedInference.getLexicalization(), ebitgLexicalizedInference.getPermutationProperties(),ebitgLexicalizedInference.getCCGLabels(), coreContextLabe);
	}
	
	
	
	public CoreContextLabel getCoreContextLabel()
	{
		return this.coreContextLabel;
	}
	
	
}
