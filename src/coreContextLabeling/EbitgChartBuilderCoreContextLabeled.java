package coreContextLabeling;

import alignment.AlignmentStringTriple;
import ccgLabeling.CCGLabeler;
import coreContextLabeling.CoreContextLabel;
import hat_lexicalization.Lexicalizer;
import extended_bitg.EbitgChart;
import extended_bitg.EbitgChartBuilder;
import extended_bitg.EbitgChartFiller;
import extended_bitg.EbitgChartInitializer;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgLexicalizedInferenceCacher;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.HATComplexityTypeComputer;

public class EbitgChartBuilderCoreContextLabeled extends EbitgChartBuilder<EbitgLexicalizedInferenceCoreContextLabeled, EbitgTreeStateCoreContextLabeled> {
	final CoreContextLabeler coreContextLabeler;
	final public static String CoreContextLabelTableFileNameConfigurationProperty = "coreContextLabelTableFileName";

	protected EbitgChartBuilderCoreContextLabeled(Lexicalizer lexicalizer, EbitgLexicalizedInferenceCacher<EbitgLexicalizedInferenceCoreContextLabeled> lexicalizedInferenceCacher,
			EbitgChart theChart, EbitgChartInitializer chartInitializer, EbitgChartFiller chartFiller, boolean useCaching, HATComplexityTypeComputer hatComplexityTypeComputer, CoreContextLabeler coreContextLabeler) {
		super(lexicalizer, lexicalizedInferenceCacher, theChart, chartInitializer, chartFiller, useCaching, hatComplexityTypeComputer);
		this.coreContextLabeler = coreContextLabeler;
	}

	/**
	 * Factory method that creates the chart builder for an alignment triple < sourceString, targetString, alignmentString>
	 * 
	 * @param sourceString
	 *            The source string
	 * @param targetString
	 *            The target string
	 * @param alignmentString
	 *            The alignment string e.g. '0-0 0-1 1-2 2-3'
	 * @param maxAllowedInferencesPerNode
	 *            A Control parameter that specifies the maximum allowed number of inferences per node, to stop chart building in cases there is too much
	 *            ambiguity which would lead the ChartBuilder to go out of memory
	 * @param lexicalzer
	 *            : The Lexicalizer, which determines if the HATs chart is build compactly or with an explict representation of the nulls
	 * @return The ChartBuilder that is constructed for the input alignment triple
	 */

	private static EbitgChartBuilderCoreContextLabeled createEbitgChartBuilderCoreContextLabeled(String sourceString, String targetString, String alignmentString, int maxAllowedInferencesPerNode,
			boolean useGreedyNullBinding, Lexicalizer lexicalizer, boolean useCaching, CoreContextLabelsTable coreContextLabelsTable) {
		// System.out.println(" EbitgChartBuilderCoreContextLabeled" + lexicalizer.getChartInternalAlignmentChunking());
		EbitgChart theChart = createInitialChart(lexicalizer);
		EbitgChartInitializer chartInitializer = EbitgChartInitializer.createChartInitializer(theChart, lexicalizer, useGreedyNullBinding);
		EbitgChartFiller chartFiller = new EbitgChartFiller(maxAllowedInferencesPerNode, theChart);
		chartInitializer.initializeChart();

		EbitgLexicalizedInferenceCacher<EbitgLexicalizedInferenceCoreContextLabeled> lexicalizedInferenceCacher = EbitgLexicalizedInferenceCacher.createEbitgLexicalizedInferenceCacher();
		CoreContextLabeler coreContextLabeler = CoreContextLabeler.createCoreContextLabeler(lexicalizer.getSourceLength(), coreContextLabelsTable);
		HATComplexityTypeComputer hatComplexityTypeComputer = HATComplexityTypeComputer.createHAComplexityTypeComputer(theChart);
		EbitgChartBuilderCoreContextLabeled theBuilder = new EbitgChartBuilderCoreContextLabeled(lexicalizer, lexicalizedInferenceCacher, theChart, chartInitializer, chartFiller, useCaching,
			hatComplexityTypeComputer,coreContextLabeler);
		theBuilder.checkChartSizeConsistency();
		theBuilder.findDerivationsAlignment();
		theBuilder.coreContextLabeler.fillCoreContextLabelsTableUsingChartBuilder(theBuilder);

		return theBuilder;
	}

	public static EbitgChartBuilderCoreContextLabeled createEbitgChartBuilderCoreContextLabeled(String sourceString, String targetString, String alignmentString, int maxAllowedInferencesPerNode,
			boolean useGreedyNullBinding, boolean useCaching, CoreContextLabelsTable coreContextLabelsTable, CCGLabeler ccgLabeler) {
		Lexicalizer lexicalizer = Lexicalizer.createCompactLexicalizer(alignmentString, sourceString, targetString, ccgLabeler);
		return createEbitgChartBuilderCoreContextLabeled(sourceString, targetString, alignmentString, maxAllowedInferencesPerNode, useGreedyNullBinding, lexicalizer, useCaching,
				coreContextLabelsTable);
	}

	@Override
	protected EbitgLexicalizedInferenceCoreContextLabeled createLabelEnrichedInferenceFromLexicalizedInference(EbitgLexicalizedInference ebitgLexicalizedInference) {
		if (ebitgLexicalizedInference != null) {
			CoreContextLabel coreContextLabel = coreContextLabeler.getCoreContextLabelForInferenceFromTable(ebitgLexicalizedInference);
			return EbitgLexicalizedInferenceCoreContextLabeled.createEbitgLexicalizedInferenceCoreContextLabeled(ebitgLexicalizedInference, coreContextLabel);
		}
		return null;
	}

	public CoreContextLabeler getCoreContextLabeler() {
		return this.coreContextLabeler;
	}

	@Override
	public EbitgTreeGrower<EbitgLexicalizedInferenceCoreContextLabeled, EbitgTreeStateCoreContextLabeled> createEbitgTreeGrower() {
		return EbitgTreeGrowerCoreContextLabeled.createEbitgTreeGrowerCoreContextLabeled(this);
	}

	public static EbitgTreeGrower<EbitgLexicalizedInferenceCoreContextLabeled, EbitgTreeStateCoreContextLabeled> buildAndWriteTreePairsForAlignment(int maxAllowedInferencesPerNode,
			String sourceString, String targetString, String alignmentString, CCGLabeler ccgLabeler, boolean writeAllTrees, boolean useGreedyNullBinding, CoreContextLabelsTable coreContextLabelsTable) {
		AlignmentStringTriple trimmedAlignmentTriple = AlignmentStringTriple.createTrimmedAlignmentStringTriple(sourceString, targetString, alignmentString);
		EbitgChartBuilder<EbitgLexicalizedInferenceCoreContextLabeled, EbitgTreeStateCoreContextLabeled> builder = createEbitgChartBuilderCoreContextLabeled(trimmedAlignmentTriple.getSourceString(),
				trimmedAlignmentTriple.getTargetString(), trimmedAlignmentTriple.getAlignmentString(), maxAllowedInferencesPerNode, useGreedyNullBinding, true, coreContextLabelsTable, ccgLabeler);
		return buildAndWriteTreePairsForAlignment(builder, writeAllTrees);
	}

}
