<DOC docid="HKLCap_0001A_Empowering_section" language="English">
<p id=1>
<seg id=1> (Cap 1, section 64) </seg>
</p>
<p id=2>
<seg id=2> [14 November 1969] </seg>
</p>
<p id=3>
<seg id=3> (L.N. 164 of 1969) </seg>
</p>
<p id=4>
<seg id=4> Note:	Under s. 1(2) of 26 of 1998, the amendment to this enactment by that Ordinance is deemed to have come into operation on 1 July 1997. </seg>
<seg id=5> The aforesaid s. 1(2) is subject to article 12 of the Hong Kong Bill of Rights set out in Part II of Cap 383. </seg>
</p>
</DOC>
<DOC docid="HKLCap_0001A_rule_0001" language="English">
<p id=1>
<seg id=1> These rules may be cited as the Administrative Appeals Rules. </seg>
</p>
</DOC>
<DOC docid="HKLCap_0001A_rule_0002" language="English">
<p id=1>
<seg id=1> Remarks: </seg>
</p>
<p id=2>
<seg id=2> Amendments retroactively made - see 26 of 1998 s. 44 </seg>
</p>
<p id=3>
<seg id=3> In these rules, unless the context otherwise requires- </seg>
</p>
<p id=4>
<seg id=4> "appeal" (上訴) means- </seg>
</p>
<p id=5>
<seg id=5> (a)	an appeal to the Chief Executive in Council otherwise than by way of petition; and </seg>
</p>
<p id=6>
<seg id=6> (b)	an objection to the Chief Executive in Council other than an objection in writing;  (26 of 1998 s. 44) </seg>
</p>
<p id=7>
<seg id=7> "applicant" (申請人) means the person who initiates an appeal; </seg>
</p>
<p id=8>
<seg id=8> "committee" (委員會) means a committee of members of the Executive Council appointed under paragraph (1) of rule 9; </seg>
</p>
<p id=9>
<seg id=9> "Ordinance" (條例) means any Ordinance by which an appeal is provided; </seg>
</p>
<p id=10>
<seg id=10> "respondent" (答辯人) means any public officer who, or the head of any Government department which, is concerned in an appeal. </seg>
</p>
</DOC>
<DOC docid="HKLCap_0001A_rule_0003" language="English">
<p id=1>
<seg id=1> No appeal shall lie if proceedings in respect of the matter of the appeal have already been taken or initiated before a court. </seg>
</p>
</DOC>
<DOC docid="HKLCap_0001A_rule_0004" language="English">
<p id=1>
<seg id=1> Remarks: </seg>
</p>
<p id=2>
<seg id=2> Amendments retroactively made - see 26 of 1998 s. 44 </seg>
</p>
<p id=3>
<seg id=3> An applicant shall submit for the consideration of the Chief Executive in Council a written memorandum, setting out the grounds of appeal upon which he relies, within thirty days after the notification to him of the decision against which he wishes to appeal. </seg>
</p>
<p id=4>
<seg id=4> (26 of 1998 s. 44) </seg>
</p>
</DOC>
<DOC docid="HKLCap_0001A_rule_0005" language="English">
<p id=1>
<seg id=1> A memorandum of appeal shall be written in the English or Chinese language and delivered to the Clerk to the Executive Council who shall forward a copy thereof to the respondent. </seg>
</p>
<p id=2>
<seg id=2> (14 of 1994 s. 24) </seg>
</p>
</DOC>
<DOC docid="HKLCap_0001A_rule_0006" language="English">
<p id=1>
<seg id=1> The Clerk to the Executive Council shall give the applicant not less than seven days' notice of the hearing of the appeal, and shall furnish the applicant with a copy of these rules. </seg>
</p>
<p id=2>
<seg id=2> (14 of 1994 s. 24) </seg>
</p>
</DOC>
<DOC docid="HKLCap_0001A_rule_0007" language="English">
<p id=1>
<seg id=1> Remarks: </seg>
</p>
<p id=2>
<seg id=2> Amendments retroactively made - see 26 of 1998 s. 44 </seg>
</p>
<p id=3>
<seg id=3> The applicant may, if he so desires, be present at the hearing of the appeal and be heard either in person or by counsel or solicitor: </seg>
</p>
<p id=4>
<seg id=4> Provided that if he elects to be heard by his counsel or solicitor he shall not himself also be heard save by special leave of the Chief Executive in Council or of a committee appointed under rule 9 for the purpose of hearing such appeal. </seg>
</p>
<p id=5>
<seg id=5> (26 of 1998 s. 44) </seg>
</p>
</DOC>
<DOC docid="HKLCap_0001A_rule_0008" language="English">
<p id=1>
<seg id=1> Remarks: </seg>
</p>
<p id=2>
<seg id=2> Amendments retroactively made - see 26 of 1998 s. 44 </seg>
</p>
<p id=3>
<seg id=3> The respondent may, if he so desires, be present at the hearing of the appeal and be heard either in person or by counsel or solicitor: </seg>
</p>
<p id=4>
<seg id=4> Provided that if he elects to be heard by his counsel or solicitor he shall not himself be heard except by special leave of the Chief Executive in Council or of a committee appointed under rule 9 for the purpose of hearing such appeal. </seg>
</p>
<p id=5>
<seg id=5> (26 of 1998 s. 44) </seg>
</p>
</DOC>
<DOC docid="HKLCap_0001A_rule_0009" language="English">
<p id=1>
<seg id=1> Remarks: </seg>
</p>
<p id=2>
<seg id=2> Amendments retroactively made - see 26 of 1998 s. 44 </seg>
</p>
<p id=3>
<seg id=3> (1)	The Chief Executive in Council may appoint a committee of not less than two members of the Executive Council for the purpose of hearing- </seg>
</p>
<p id=4>
<seg id=4> (a)	any appeal or class of appeals; </seg>
</p>
<p id=5>
<seg id=5> (b)	any appeal by way of petition or any class of such appeals; or </seg>
</p>
<p id=6>
<seg id=6> (c)	any objection in writing to the Chief Executive in Council or any class of such objections. </seg>
</p>
<p id=7>
<seg id=7> (2)	The Chief Executive in Council may in his discretion refer any appeal or class of appeals, any appeal by way of petition or any class of such appeals, or any objection in writing or any class of such objections to the committee for such purpose. </seg>
</p>
<p id=8>
<seg id=8> (26 of 1998 s. 44) </seg>
</p>
</DOC>
<DOC docid="HKLCap_0001A_rule_0010" language="English">
<p id=1>
<seg id=1> Remarks: </seg>
</p>
<p id=2>
<seg id=2> Amendments retroactively made - see 26 of 1998 s. 44 </seg>
</p>
<p id=3>
<seg id=3> (1)	A committee shall hear any matter referred to it under paragraph (2) of rule 9 and shall advise the Chief Executive in Council as to the decision that should be made thereon: </seg>
</p>
<p id=4>
<seg id=4> Provided that the Chief Executive in Council shall not be bound to accept the advice of such committee.   </seg>
</p>
<p id=5>
<seg id=5> (2)	Every hearing by a committee shall be in camera. </seg>
</p>
<p id=6>
<seg id=6> (26 of 1998 s. 44) </seg>
</p>
</DOC>
<DOC docid="HKLCap_0001A_rule_0011" language="English">
<p id=1>
<seg id=1> Rules 7 and 8 shall apply to any appeal by way of petition, or objection in writing, which is referred to a committee, in same manner as to an appeal. </seg>
</p>
</DOC>
<DOC docid="HKLCap_0001A_rule_0012" language="English">
<p id=1>
<seg id=1> Remarks: </seg>
</p>
<p id=2>
<seg id=2> Amendments retroactively made - see 26 of 1998 s. 44 </seg>
</p>
<p id=3>
<seg id=3> The applicant and the respondent shall be informed of the decision of the Chief Executive in Council by the Clerk to the Executive Council in writing within fourteen days after the determination of the appeal or within such other time or in such other manner as the Chief Executive in Council may specify. </seg>
</p>
<p id=4>
<seg id=4> (14 of 1994 s. 24; 26 of 1998 s. 44) </seg>
</p>
</DOC>
<DOC docid="HKLCap_0001A_rule_0013" language="English">
<p id=1>
<seg id=1> Remarks: </seg>
</p>
<p id=2>
<seg id=2> Amendments retroactively made - see 26 of 1998 s. 44 </seg>
</p>
<p id=3>
<seg id=3> (1)	In any appeal the Chief Executive in Council may in his discretion direct a case to be stated for the opinion of the Court of Appeal on any question of law involved in any appeal submitted to him. </seg>
</p>
<p id=4>
<seg id=4> (2)	The terms of any such case shall be agreed upon by the parties concerned, or, in the event of their failure to agree, shall be settled by the Court of Appeal. </seg>
</p>
<p id=5>
<seg id=5> (3)	The Court of Appeal shall hear and determine the question of law arising on any case stated as aforesaid, and shall remit the matter to the Chief Executive in Council who shall give effect by order to the finding of the court. </seg>
<seg id=6> The costs of such hearing before the Court of Appeal shall be in the discretion of the Court of Appeal. </seg>
</p>
<p id=6>
<seg id=7> (4)	Any party to the appeal shall be entitled to be heard by counsel or in person on the hearing before the Court of Appeal of any case so stated. </seg>
</p>
<p id=7>
<seg id=8> (92 of 1975 s. 59; 26 of 1998 s. 44) </seg>
</p>
</DOC>
