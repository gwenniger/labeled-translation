#!/bin/bash


# Method to copy results to a directory for evaluation with multeval

#Usage:

# ./copyResultsToEvaluationDirectory.sh /home/UserName/tests/testResults/reorderingLabeled/Joshua-HieroParentRelativeReorderingLabeled-ZhEn-MultiUNPlusHongKong-XinhuaLM-Basic-JOSHUA-MIRA-7Million-SoftConstraintDecodingAndMatching-MultipleDevReferences-MTATestSet-RerunSingleOOVProducingRule/ results-RerunNoLabelsInTrieOptimizedCubePruning-FixedRelabelingRuleInTriesBug-PopLimit2000 /home/UserName/tests/testResults/allSystemsNBestLists/zh-en/experiments-efficientFuzzyMatchingPaper/hiero-ParentRelativeLabeled-SoftConstraintDecoding-LSF-BASIC-RerunNoLabelsInTrieOptimizedCubePruning-FixedRelabelingRuleInTriesBug-PopLimit2000/
 
 



RESULTS_SOURCE_ROOT_DIR=$1
RESULTS_SUB_DIR_PREFIX=$2
EVALUATION_ROOT_DIR=$3


echo "Make results sub directories"
mkdir ${EVALUATION_ROOT_DIR}dev
mkdir ${EVALUATION_ROOT_DIR}test



RESULTS_BASE_DIR=${RESULTS_SOURCE_ROOT_DIR}JoshuaHatsHiero/

for i in `seq 1 3`;
do
   echo "Copy results from run" $i
   mkdir ${EVALUATION_ROOT_DIR}dev/run${i}
   mkdir ${EVALUATION_ROOT_DIR}test/run${i}
   RUN_SUB_DIR=${RESULTS_BASE_DIR}${RESULTS_SUB_DIR_PREFIX}-run${i}

   echo "RUN_SUB_DIR:"  ${RUN_SUB_DIR}

   # Copy the dev and test result files and the experimental config file
   cp ${RUN_SUB_DIR}/dev/*  ${EVALUATION_ROOT_DIR}dev/run${i}
   cp ${RUN_SUB_DIR}/test/*  ${EVALUATION_ROOT_DIR}test/run${i}
   cp ${RUN_SUB_DIR}/experimentConfigFile.txt  ${EVALUATION_ROOT_DIR}test/run${i}

   # copy the final joshua config file
   TUNER_SUB_DIR=${RESULTS_BASE_DIR}intermediate-results/mert/${RESULTS_SUB_DIR_PREFIX}-run${i}/tuner-work-dir/
   cp ${TUNER_SUB_DIR}joshua.config.final  ${EVALUATION_ROOT_DIR}test/run${i}

   echo "Copy output.NBEST.tuned file to .run"${i} "file"
   cp ${EVALUATION_ROOT_DIR}dev/run${i}/output.1best.TUNED   ${EVALUATION_ROOT_DIR}dev/output.1best.TUNED.run${i}
   cp ${EVALUATION_ROOT_DIR}test/run${i}/output.1best.TUNED  ${EVALUATION_ROOT_DIR}test/output.1best.TUNED.run${i}
done    
