 #!/bin/bash

# USAGE ./moveResults.sh SOURCE_ROOT_DIR TARGET_ROOT_DIR RESULTS_SUBDIR_NAME
# e.g.: 
# ./moveResults.sh /home/UserName/tests/testResults/reorderingLabeled/Joshua-HieroParentRelativeReorderingLabeled-ZhEn-MultiUNPlusHongKong-XinhuaLM-Basic-JOSHUA-MIRA-7Million-SoftConstraintDecodingAndMatching-MultipleDevReferences-MTATestSet-RerunSingleOOVProducingRule-run3/ /home/UserName/tests/testResults/reorderingLabeled/Joshua-HieroParentRelativeReorderingLabeled-ZhEn-MultiUNPlusHongKong-XinhuaLM-Basic-JOSHUA-MIRA-7Million-SoftConstraintDecodingAndMatching-MultipleDevReferences-MTATestSet-RerunSingleOOVProducingRule/ results-RerunNoLabelsInTrieOptimizedCubePruning-FixedRelabelingRuleInTriesBug-PopLimit2000-run3



SOURCE_ROOT_DIR=$1
TARGET_ROOT_DIR=$2
RESULTS_SUBDIR_NAME=$3

echo "Move the main results to" $RESULTS_SUBDIR_NAME
cd $SOURCE_ROOT_DIR/JoshuaHatsHiero
mv results $RESULTS_SUBDIR_NAME 
mv experimentConfigFile.txt $RESULTS_SUBDIR_NAME
mv  $RESULTS_SUBDIR_NAME $TARGET_ROOT_DIR/JoshuaHatsHiero/$RESULTS_SUBDIR_NAME


# MERT results
MERT_RESULTS_SUBDIR_PATH=$SOURCE_ROOT_DIR/JoshuaHatsHiero/intermediate-results/mert/$RESULTS_SUBDIR_NAME
mkdir $MERT_RESULTS_SUBDIR_PATH
echo "Move the tuner results to" $MERT_RESULTS_SUBDIR_PATH
cd  $SOURCE_ROOT_DIR/JoshuaHatsHiero/intermediate-results/mert/
mv tuner-work-dir mert*  params.txt decoder_command $MERT_RESULTS_SUBDIR_PATH 
mv  $RESULTS_SUBDIR_NAME $TARGET_ROOT_DIR/JoshuaHatsHiero/intermediate-results/mert/$RESULTS_SUBDIR_NAME


# Test results
TEST_RESULTS_SUBDIR_PATH=$SOURCE_ROOT_DIR/JoshuaHatsHiero/intermediate-results/test/$RESULTS_SUBDIR_NAME
mkdir $TEST_RESULTS_SUBDIR_PATH
echo "Move the test results to" $TEST_RESULTS_SUBDIR_PATH
cd  $SOURCE_ROOT_DIR/JoshuaHatsHiero/intermediate-results/test/
mv  test_config* output.nbest.*  $TEST_RESULTS_SUBDIR_PATH
mv  $RESULTS_SUBDIR_NAME $TARGET_ROOT_DIR/JoshuaHatsHiero/intermediate-results/test/$RESULTS_SUBDIR_NAME


# Dev results
DEV_RESULTS_SUBDIR_PATH=$SOURCE_ROOT_DIR/JoshuaHatsHiero/intermediate-results/dev/$RESULTS_SUBDIR_NAME
mkdir $DEV_RESULTS_SUBDIR_PATH
echo "Move the dev results to" $DEV_RESULTS_SUBDIR_PATH
cd  $SOURCE_ROOT_DIR/JoshuaHatsHiero/intermediate-results/dev/
mv  output.nbest.*  $DEV_RESULTS_SUBDIR_PATH
mv  $RESULTS_SUBDIR_NAME $TARGET_ROOT_DIR/JoshuaHatsHiero/intermediate-results/dev/$RESULTS_SUBDIR_NAME



#Move the final results file name
mv ${SOURCE_ROOT_DIR}summarizedResults.tex ${TARGET_ROOT_DIR}summarizedResults-${RESULTS_SUBDIR_NAME}.tex

