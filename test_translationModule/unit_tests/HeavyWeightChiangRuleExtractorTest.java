package unit_tests;

import extended_bitg.EbitgChartBuilderBasic;
import extended_bitg.EbitgChartBuilderProperties;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeStateBasic;
import reorderingLabeling.FinePhraseCentricReorderingLabelBasic;
import util.Span;
import grammarExtraction.chiangGrammarExtraction.ChiangGrammarExtractionConstraints;
import grammarExtraction.chiangGrammarExtraction.ChiangRuleCreater;
import grammarExtraction.chiangGrammarExtraction.GapRangeComputing;
import grammarExtraction.chiangGrammarExtraction.HeavyWeightChiangRuleExtractor;
import grammarExtraction.chiangGrammarExtraction.HierarchicalPhraseBasedRuleExtractor;
import grammarExtraction.chiangGrammarExtraction.LabelSideSettings;
import grammarExtraction.chiangGrammarExtraction.ReorderLabelingSettings;
import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.RuleGap;
import grammarExtraction.translationRules.RuleLabel;
import grammarExtraction.translationRules.TranslationRule;
import java.util.List;
import junit.framework.Assert;
import org.junit.Test;
import alignment.AlignmentTriple;

public class HeavyWeightChiangRuleExtractorTest {

	private static final int MaxAllowedInferencesPerNode = 100000;
	private static final ChiangGrammarExtractionConstraints chiangGrammarExtractionConstraints = ChiangGrammarExtractionConstraints.createStandardChiangGrammarExtractionConstraints(true);
	private static final ReorderLabelingSettings REORDER_LABELING_SETTINGS = ReorderLabelingSettings.createNoReorderingLabelingSettings();
	private static final ReorderLabelingSettings  REORDER_LABELING_SETTINGS_HAT_LABELS =ReorderLabelingSettings.createReorderLabelingSettings(FinePhraseCentricReorderingLabelBasic.createReorderingLabelCreater(),true,true,true);
	private static LabelSideSettings LABEL_SIDE_SETTINGS = new LabelSideSettings.LabelBothSides();

	public static TranslationRule<EbitgLexicalizedInference> createWholeSentencePairBaseTranslationRule(HeavyWeightChiangRuleExtractor<EbitgLexicalizedInference, EbitgTreeStateBasic> ruleExtractor) {
		return ruleExtractor.createChiangConstraintsSatisfyingBaseTranslationRule(new Span(0, ruleExtractor.getChartSourceLength() - 1));
	}

	int expectedNumberOfBaseRulesMonotoneAlignment(int monotoneAlignmentLength) {
		return (int) ((((double) monotoneAlignmentLength + 1) / 2) * monotoneAlignmentLength);
	}

	int expectedNumberOfBaseRulesFourPermutationAlignment(int noGroups) {
		int singletonRules = (noGroups * 4);
		return (int) ((((double) noGroups + 1) / 2) * noGroups) + singletonRules;

	}

	int expectedNumberOneGapRulesMonotoneAlignment(int monontoneAlignmentLength) {
		int result = 0;
		int maxParts = Math.min(chiangGrammarExtractionConstraints.getMaxTerminalsPlusNonTerminalsSourceSide(), monontoneAlignmentLength);
		for (int i = 2; i <= maxParts; i++) {
			result += i;
		}

		return result;
	}

	boolean isLegalGapping(int gapOneStart, int gapOneEnd, int gapTwoStart, int gapTwoEnd, int minSummedGapsLength) {
		// System.out.println("Checking isLegalGapping...");

		int gapOneLength = (gapOneEnd - gapOneStart) + 1;
		int gapTwoLength = (gapTwoEnd - gapTwoStart) + 1;
		int totalGapLength = gapOneLength + gapTwoLength;
		int distanceBetweenGaps = gapTwoStart - gapOneEnd - 1;

		return ((gapOneLength >= 1) && (gapTwoLength >= 1) && (distanceBetweenGaps >= 1) && (totalGapLength >= minSummedGapsLength));
	}

	/**
	 * Computes the number of possible two gap alignments for the monotone case in a brute force manner
	 * 
	 * @param monotoneAlignmentLength
	 * @return
	 */
	int expectedNumberTwoGapRulesMonotoneAlignment(int monotoneAlignmentLength, boolean allowDanglingSecondNonTerminal) {
		int result = 0;
		GapRangeComputing gapRangeComputing = GapRangeComputing.createGapRangeComputing(chiangGrammarExtractionConstraints);
		int minSummedGapsLength = gapRangeComputing.minSummedGapsLength(monotoneAlignmentLength, 2);

		for (int gapOneStart = 0; gapOneStart < monotoneAlignmentLength; gapOneStart++) {
			for (int gapOneEnd = gapOneStart + chiangGrammarExtractionConstraints.getMinNonTerminalSpan() - 1; gapOneEnd < monotoneAlignmentLength; gapOneEnd++) {
				for (int gapTwoStart = gapOneEnd + 1; gapTwoStart < monotoneAlignmentLength; gapTwoStart++) {
					for (int gapTwoEnd = gapTwoStart + chiangGrammarExtractionConstraints.getMinNonTerminalSpan() - 1; gapTwoEnd < monotoneAlignmentLength; gapTwoEnd++) {
						if (isLegalGapping(gapOneStart, gapOneEnd, gapTwoStart, gapTwoEnd, minSummedGapsLength)) {
							if (allowDanglingSecondNonTerminal || (!conaintsDanglingSecondNonTerminalForMonotoneAlignment(gapOneStart, gapTwoEnd, monotoneAlignmentLength))) {
								result++;
							}
						}

					}
				}
			}
		}
		return result;
	}

	boolean conaintsDanglingSecondNonTerminalForMonotoneAlignment(int gapOneStart, int gapTwoEnd, int phraseLength) {
		if (gapOneStart == 0) {
			return true;
		} else if (gapTwoEnd == (phraseLength - 1)) {
			return true;
		}
		return false;
	}

	public void hasExpectedNumberElements(List<?> generatedRules, int expected) {
		System.out.println("Expected number of generated rules: " + expected + " actual rules generated " + generatedRules.size());
		Assert.assertEquals(expected, generatedRules.size());
	}

	@Test
	public void testCreateBaseTranslationRulesForPermutation() {
		final int noGroups = 2;
		AlignmentTriple monotoneAlignment = AlignmentTriple.createFourPermutationAlignmentTriple(noGroups);
		EbitgChartBuilderBasic chartBuilder = EbitgChartBuilderBasic.createEbitgChartBuilder(monotoneAlignment.getSourceSentenceAsString(), monotoneAlignment.getTargetSentenceAsString(),
				monotoneAlignment.getAlignmentAsString(), MaxAllowedInferencesPerNode, EbitgChartBuilderProperties.USE_GREEDY_NULL_BINDING, EbitgChartBuilderProperties.USE_CACHING);
		chartBuilder.findDerivationsAlignment();
		HierarchicalPhraseBasedRuleExtractor<EbitgLexicalizedInference> ruleExtractor = HierarchicalPhraseBasedRuleExtractor.createHierarchicalPhraseBasedRulesExtractorChiangPure(chartBuilder,
				chiangGrammarExtractionConstraints, new ChiangRuleCreater(REORDER_LABELING_SETTINGS,LABEL_SIDE_SETTINGS.createRuleLabelCreater()));

		WordKeyMappingTable wordKeyMappingTable = WordKeyMappingTable.createWordKeyMappingTable(monotoneAlignment, 2, 16);
		List<LightWeightPhrasePairRuleSet> ruleSets = ruleExtractor.createLightWeightTranslationRuleSets(wordKeyMappingTable);

		for (LightWeightPhrasePairRuleSet ruleSet : ruleSets) {
			System.out.println(ruleSet);
		}

		hasExpectedNumberElements(ruleSets, expectedNumberOfBaseRulesFourPermutationAlignment(noGroups));
	}

	@Test
	public void testCreateOneGapTranslationRulesForMonotone() {
		final int alignmentLength = 10;
		AlignmentTriple alignment = AlignmentTriple.createMonontoneAlignmentTriple(alignmentLength);
		EbitgChartBuilderBasic chartBuilder = EbitgChartBuilderBasic.createEbitgChartBuilder(alignment.getSourceSentenceAsString(), alignment.getTargetSentenceAsString(),
				alignment.getAlignmentAsString(), MaxAllowedInferencesPerNode, EbitgChartBuilderProperties.USE_GREEDY_NULL_BINDING, EbitgChartBuilderProperties.USE_CACHING);
		chartBuilder.findDerivationsAlignment();
		HeavyWeightChiangRuleExtractor<EbitgLexicalizedInference, EbitgTreeStateBasic> ruleExtractor = HeavyWeightChiangRuleExtractor.createHeavyWeightChiangRuleExtractor(chartBuilder,
				chiangGrammarExtractionConstraints, new ChiangRuleCreater(REORDER_LABELING_SETTINGS,LABEL_SIDE_SETTINGS.createRuleLabelCreater()));

		TranslationRule<EbitgLexicalizedInference> baseTranslationRule = createWholeSentencePairBaseTranslationRule(ruleExtractor);
		System.out.println("BaseTranslationRule: " + baseTranslationRule);
		List<TranslationRule<EbitgLexicalizedInference>> oneGapRules = ruleExtractor.createOneGapRules(baseTranslationRule);

		for (TranslationRule<EbitgLexicalizedInference> rule : oneGapRules) {
			System.out.println(rule);
		}

		hasExpectedNumberElements(oneGapRules, expectedNumberOneGapRulesMonotoneAlignment(alignmentLength));
	}

	public void testCreateTwoGapsTranslationRulesForMonotone(boolean allowDanglingSecondNonTerminal) {
		ChiangGrammarExtractionConstraints chiangGrammarExtractionConstraints = ChiangGrammarExtractionConstraints.createStandardChiangGrammarExtractionConstraints(allowDanglingSecondNonTerminal);
		Assert.assertEquals(allowDanglingSecondNonTerminal, chiangGrammarExtractionConstraints.allowDanglingSecondNonterminal());
		final int alignmentLength = 10;
		AlignmentTriple alignment = AlignmentTriple.createMonontoneAlignmentTriple(alignmentLength);
		HeavyWeightChiangRuleExtractor<EbitgLexicalizedInference, EbitgTreeStateBasic> ruleExtractor = createRuleExtractor(alignment, chiangGrammarExtractionConstraints);
		TranslationRule<EbitgLexicalizedInference> baseTranslationRule = createWholeSentencePairBaseTranslationRule(ruleExtractor);
		List<TranslationRule<EbitgLexicalizedInference>> twoGapRules = ruleExtractor.createTwoGapRulesAndAbstractRules(baseTranslationRule);

		for (TranslationRule<EbitgLexicalizedInference> rule : twoGapRules) {
			System.out.println(rule);
		}

		hasExpectedNumberElements(twoGapRules, expectedNumberTwoGapRulesMonotoneAlignment(alignmentLength, allowDanglingSecondNonTerminal));
	}

	@Test
	public void testCreateTwoGapsTranslationRulesForMonotoneWithDanglingSecondNonterminals() {
		testCreateTwoGapsTranslationRulesForMonotone(true);
	}

	@Test
	public void gapsTest() {
		RuleGap gap1 = new RuleGap(new Span(0, 3), new Span(0, 3), 0, RuleLabel.createBothSidesSameRuleLabel("gap1"));
		Assert.assertTrue(gap1.isOnLeftSourcePhraseBoundary());
		Assert.assertTrue(gap1.isOnLeftTargetPhraseBoundary());
		Assert.assertTrue(gap1.isOnRighttSourcePhraseBoundary(4));
		Assert.assertTrue(gap1.isOnRightTargetPhraseBoundary(4));

		RuleGap gap2 = new RuleGap(new Span(0, 1), new Span(0, 1), 0, RuleLabel.createBothSidesSameRuleLabel("gap2"));
		Assert.assertTrue(gap2.isOnLeftSourcePhraseBoundary());
		Assert.assertTrue(gap2.isOnLeftTargetPhraseBoundary());
		Assert.assertFalse(gap2.isOnRighttSourcePhraseBoundary(4));
		Assert.assertFalse(gap2.isOnRightTargetPhraseBoundary(4));
	}

	@Test
	public void testCreateTwoGapsTranslationRulesForMonotoneWithoutDanglingSecondNonterminals() {
		testCreateTwoGapsTranslationRulesForMonotone(false);
	}

	private static HeavyWeightChiangRuleExtractor<EbitgLexicalizedInference, EbitgTreeStateBasic> createRuleExtractor(AlignmentTriple alignmentTriple,
			ChiangGrammarExtractionConstraints chiangGrammarExtracionConstraints) {
		EbitgChartBuilderBasic chartBuilder = EbitgChartBuilderBasic.createEbitgChartBuilder(alignmentTriple.getSourceSentenceAsString(), alignmentTriple.getTargetSentenceAsString(),
				alignmentTriple.getAlignmentAsString(), MaxAllowedInferencesPerNode, EbitgChartBuilderProperties.USE_GREEDY_NULL_BINDING, EbitgChartBuilderProperties.USE_CACHING);
		chartBuilder.findDerivationsAlignment();
		HeavyWeightChiangRuleExtractor<EbitgLexicalizedInference, EbitgTreeStateBasic> ruleExtractor = HeavyWeightChiangRuleExtractor.createHeavyWeightChiangRuleExtractor(chartBuilder,
				chiangGrammarExtracionConstraints, new ChiangRuleCreater(REORDER_LABELING_SETTINGS,LABEL_SIDE_SETTINGS.createRuleLabelCreater()));
		return ruleExtractor;
	}
	
	private static HeavyWeightChiangRuleExtractor<EbitgLexicalizedInference, EbitgTreeStateBasic> createReorderingLabeledRuleExtractor(AlignmentTriple alignmentTriple,
			ChiangGrammarExtractionConstraints chiangGrammarExtracionConstraints) {
		EbitgChartBuilderBasic chartBuilder = EbitgChartBuilderBasic.createEbitgChartBuilder(alignmentTriple.getSourceSentenceAsString(), alignmentTriple.getTargetSentenceAsString(),
				alignmentTriple.getAlignmentAsString(), MaxAllowedInferencesPerNode, EbitgChartBuilderProperties.USE_GREEDY_NULL_BINDING, EbitgChartBuilderProperties.USE_CACHING);
		chartBuilder.findDerivationsAlignment();
		HeavyWeightChiangRuleExtractor<EbitgLexicalizedInference, EbitgTreeStateBasic> ruleExtractor = HeavyWeightChiangRuleExtractor.createHeavyWeightChiangRuleExtractor(chartBuilder,
				chiangGrammarExtracionConstraints, new ChiangRuleCreater( REORDER_LABELING_SETTINGS_HAT_LABELS,LABEL_SIDE_SETTINGS.createRuleLabelCreater()));
		return ruleExtractor;
	}

	@Test
	public void testCreateTwoGapsTranslationRulesForFourPermutation() {
		final int noGroups = 2;
		AlignmentTriple alignment = AlignmentTriple.createFourPermutationAlignmentTriple(noGroups);
		HeavyWeightChiangRuleExtractor<EbitgLexicalizedInference, EbitgTreeStateBasic> ruleExtractor = createRuleExtractor(alignment, chiangGrammarExtractionConstraints);

		TranslationRule<EbitgLexicalizedInference> baseTranslationRule = createWholeSentencePairBaseTranslationRule(ruleExtractor);
		List<TranslationRule<EbitgLexicalizedInference>> twoGapRules = ruleExtractor.createTwoGapRulesAndAbstractRules(baseTranslationRule);

		for (TranslationRule<EbitgLexicalizedInference> rule : twoGapRules) {
			System.out.println(rule);
		}

		System.out.println(alignment);

		// hasExpectedNumberRules(twoGapRules, expectedNumberTwoGapRulesMonotoneAlignment(alignmentLength));
	}

	@Test
	public void testHalfConnectedSourceAlignment() {
		int noGroups = 5;
		AlignmentTriple alignment = AlignmentTriple.createHalfConnectedSourceAlignmentTriple(noGroups);
		System.out.println(">>>Alignment: " + alignment);
		HeavyWeightChiangRuleExtractor<EbitgLexicalizedInference, EbitgTreeStateBasic> ruleExtractor = createRuleExtractor(alignment, chiangGrammarExtractionConstraints);
		System.out.println("alignment: " + alignment.getAlignmentAsString());
		TranslationRule<EbitgLexicalizedInference> baseTranslationRule = createWholeSentencePairBaseTranslationRule(ruleExtractor);
		System.out.println("baseTranslationRule: " + baseTranslationRule);
		List<TranslationRule<EbitgLexicalizedInference>> twoGapRules = ruleExtractor.createTwoGapRulesAndAbstractRules(baseTranslationRule);

		for (TranslationRule<EbitgLexicalizedInference> rule : twoGapRules) {
			System.out.println(rule);
			System.out.println("Rule.rule.getOutsideGapTargetPositions()" + rule.getRelativeOutsideGapTargetPositions());
			assert (rule.getRelativeOutsideGapTargetPositions().size() > 0);
		}

		// hasExpectedNumberElements(twoGapRules, expectedNumberTwoGapRulesMonotoneAlignment(alignmentLength));
	}
	
	@Test
	public void testFullyConnectedAlignmentWithReorderingLabeling() {
		AlignmentTriple alignment = AlignmentTriple.createFullyConnectedAlignmentTriple(5);
		System.out.println(">>>Alignment: " + alignment);
		HeavyWeightChiangRuleExtractor<EbitgLexicalizedInference, EbitgTreeStateBasic> ruleExtractor = createReorderingLabeledRuleExtractor(alignment, chiangGrammarExtractionConstraints);
		System.out.println("alignment: " + alignment.getAlignmentAsString());
		TranslationRule<EbitgLexicalizedInference> baseTranslationRule = createWholeSentencePairBaseTranslationRule(ruleExtractor);
		System.out.println("<testFullyConnectedAlignmentWithReorderingLabeling>\nBase translation rule:" + baseTranslationRule + "\n</testFullyConnectedAlignmentWithReorderingLabeling>");
		Assert.assertEquals("X-ATOMIC", baseTranslationRule.getRuleLabel().getSourceSideLabel());
		Assert.assertEquals("X-ATOMIC", baseTranslationRule.getRuleLabel().getTargetSideLabel());
	}

}
