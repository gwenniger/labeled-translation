package unit_tests;

import grammarExtraction.samtGrammarExtraction.UnknownWordRulesCreatorPosTags;
import junit.framework.Assert;

import org.junit.Test;

public class UnknownWordRulesCreatorPosTagsTest {

	@Test(expected=AssertionError.class)
	public void testAssertionsEnabled() 
	{
		assert(false);
	}
	
	@Test
	public void test() {
		Assert.assertFalse(UnknownWordRulesCreatorPosTags.isPosTag("WHNP:WP+VBP+DT"));
		Assert.assertTrue(UnknownWordRulesCreatorPosTags.isPosTag("S"));
		Assert.assertTrue(UnknownWordRulesCreatorPosTags.isPosTag("PP"));
		Assert.assertFalse(UnknownWordRulesCreatorPosTags.isPosTag("_COMMA_...TO"));
		Assert.assertTrue(UnknownWordRulesCreatorPosTags.isPosTag("_COMMA_"));
	}

}
