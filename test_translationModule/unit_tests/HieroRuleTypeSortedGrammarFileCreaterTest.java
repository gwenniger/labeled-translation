package unit_tests;

import grammarExtraction.grammarCombining.HieroRuleTypeSortedGrammarFileCreater;
import java.util.Arrays;
import java.util.List;
import junit.framework.Assert;
import org.junit.Test;
import util.FileUtil;

public class HieroRuleTypeSortedGrammarFileCreaterTest {

    private static String TEST_FOLDER_PATH = "./GrammarCombinerTestFolder/";
    private static String TEST_GRAMMAR_FILE_PATH = TEST_FOLDER_PATH + "TestGrammarUnsorted.txt";
    private static String REFERENCE_FILE_PATH = TEST_FOLDER_PATH + "ReferenceGrammarSorted.txt";
    private static String RESULT_FILE_PATH = TEST_FOLDER_PATH + "PreambledGrammarSorted.txt";

    private static String RULE1 = "[VERB:.] ||| von der [NOUN:NOUN,1] abzusetzen [.:.,2] ||| be taken off the [NOUN:NOUN,1] [.:.,2] ||| -0.0 1.0413927 1.0543576 -0.0 4.0355897 1.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 4.0 2.4324255 ";
    private static String RULE2 = "[VERB:VERB] ||| kann sich nicht [VERB:VERB,1] ||| cannot [VERB:VERB,1] ||| -0.0 0.30103 0.30103 -0.0 3.285994 1.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 1.0 1.4560941 ";
    private static String RULE3 = "[VERB:VERB] ||| kann sich nicht ||| cannot ||| -0.0 0.30103 0.30103 -0.0 3.0813472 1.0 0.0 0.0 1.0 1.0 0.0 0.0 0.0 1.0 1.0 1.0 ";
    private static String RULE4 = "[DET:NOUN] ||| die [NOUN:ADP,1] beide programme ||| the [NOUN:ADP,1] both programmes ||| -0.0 0.20411998 0.53147894 -0.0 4.5584683 1.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 3.0 2.629166 ";
    private static String RULE5 = "[ADJ:.] ||| [ADJ:ADJ,1] abwegigkeit [.:.,2] ||| [ADJ:ADJ,1] shift [.:.,2] ||| -0.0 -0.0 -0.0 -0.0 3.1598785 1.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 1.0 1.1798865 ";

    private static String RULE1_EXPECTED_RESULT = "[X] ||| von der [X,1] abzusetzen [X,2] ||| be taken off the [X,1] [X,2] ||| "
	    + HieroRuleTypeSortedGrammarFileCreater.TYPE_SORTED_GRAMMAR_UNLABELED_PREAMBLE_SEPARATOR
	    + "[VERB:.] ||| von der [NOUN:NOUN,1] abzusetzen [.:.,2] ||| be taken off the [NOUN:NOUN,1] [.:.,2] ||| -0.0 1.0413927 1.0543576 -0.0 4.0355897 1.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 4.0 2.4324255 ";
    private static String RULE2_EXPECTED_RESULT = "[X] ||| kann sich nicht [X,1] ||| cannot [X,1] ||| "
	    + HieroRuleTypeSortedGrammarFileCreater.TYPE_SORTED_GRAMMAR_UNLABELED_PREAMBLE_SEPARATOR
	    + "[VERB:VERB] ||| kann sich nicht [VERB:VERB,1] ||| cannot [VERB:VERB,1] ||| -0.0 0.30103 0.30103 -0.0 3.285994 1.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 1.0 1.4560941 ";
    private static String RULE3_EXPECTED_RESULT = "[X] ||| kann sich nicht ||| cannot ||| "
	    + HieroRuleTypeSortedGrammarFileCreater.TYPE_SORTED_GRAMMAR_UNLABELED_PREAMBLE_SEPARATOR
	    + "[VERB:VERB] ||| kann sich nicht ||| cannot ||| -0.0 0.30103 0.30103 -0.0 3.0813472 1.0 0.0 0.0 1.0 1.0 0.0 0.0 0.0 1.0 1.0 1.0 ";
    private static String RULE4_EXPECTED_RESULT = "[X] ||| die [X,1] beide programme ||| the [X,1] both programmes ||| "
	    + HieroRuleTypeSortedGrammarFileCreater.TYPE_SORTED_GRAMMAR_UNLABELED_PREAMBLE_SEPARATOR
	    + "[DET:NOUN] ||| die [NOUN:ADP,1] beide programme ||| the [NOUN:ADP,1] both programmes ||| -0.0 0.20411998 0.53147894 -0.0 4.5584683 1.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 3.0 2.629166 ";

    private static String RULE5_EXPECTED_RESULT = "[X] ||| [X,1] abwegigkeit [X,2] ||| [X,1] shift [X,2] ||| "
	    + HieroRuleTypeSortedGrammarFileCreater.TYPE_SORTED_GRAMMAR_UNLABELED_PREAMBLE_SEPARATOR
	    + "[ADJ:.] ||| [ADJ:ADJ,1] abwegigkeit [.:.,2] ||| [ADJ:ADJ,1] shift [.:.,2] ||| -0.0 -0.0 -0.0 -0.0 3.1598785 1.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 1.0 1.1798865 ";

    private static List<String> INPUT_TEST_FILE_LINES_LIST = Arrays.asList(RULE1, RULE2, RULE3,
	    RULE4, RULE5);

    private static List<String> EXPECTED_OUTPUT_TEST_FILE_LINES_LIST = Arrays.asList(
	    RULE4_EXPECTED_RESULT, RULE3_EXPECTED_RESULT, RULE2_EXPECTED_RESULT,
	    RULE1_EXPECTED_RESULT, RULE5_EXPECTED_RESULT);

    public static void createTestGrammar() {
	util.FileUtil.writeLinesListToFile(INPUT_TEST_FILE_LINES_LIST, TEST_GRAMMAR_FILE_PATH);
    }

    public static void createReferemceResultFile() {
	util.FileUtil.writeLinesListToFile(EXPECTED_OUTPUT_TEST_FILE_LINES_LIST,
		REFERENCE_FILE_PATH);
    }

    public void setup() {
	FileUtil.createFolderIfNotExisting(TEST_FOLDER_PATH);
	createTestGrammar();
	createReferemceResultFile();
    }

    @Test
    public void testCreateSortedOutputFile() {
	setup();
	HieroRuleTypeSortedGrammarFileCreater hieroRuleTypeSortedGrammarFileCreater = HieroRuleTypeSortedGrammarFileCreater
		.createHieroRuleTypeSortedGrammarFileCreater(TEST_GRAMMAR_FILE_PATH,
			RESULT_FILE_PATH);
	hieroRuleTypeSortedGrammarFileCreater.createSortedOutputFile();
	Assert.assertTrue(FileUtil.filesContainSameLinesInSameOrder(REFERENCE_FILE_PATH,
		RESULT_FILE_PATH, false));
    }

}
