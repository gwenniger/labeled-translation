package unit_tests;

import static org.junit.Assert.*;
import grammarExtraction.grammarComparison.StringListsComparer;

import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

public class StringListsComparerTest {

    @Test
    public void test() {
	List<String> firstList = Arrays.asList("D", "B", "C", "A","Z");
	List<String> secondList = Arrays.asList("G", "C", "E", "F", "B");

	StringListsComparer stringListsComparer = StringListsComparer.createStringListsComparer(
		firstList, secondList);
	stringListsComparer.computeAndPrintStringPresenceInformation();
	Assert.assertEquals(Arrays.asList("A","D","Z"), stringListsComparer.getStringsOnlyPresentInFirstList());
	Assert.assertEquals(Arrays.asList("E","F","G"), stringListsComparer.getStringsOnlyPresentInSecondList());
	Assert.assertEquals(Arrays.asList("B","C"), stringListsComparer.getStringsPresentInBothLists());

    }

}
