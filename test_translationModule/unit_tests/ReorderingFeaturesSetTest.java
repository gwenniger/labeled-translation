package unit_tests;

import integration_tests.MTPipelineTestJoshua;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import junit.framework.Assert;
import extended_bitg.EbitgLexicalizedInference;
import grammarExtraction.chiangGrammarExtraction.ChiangRuleExtracter;
import grammarExtraction.chiangGrammarExtraction.HierarchicalPhraseBasedRuleExtractor;
import grammarExtraction.chiangGrammarExtraction.LabelSideSettings;
import grammarExtraction.chiangGrammarExtraction.ReorderLabelingSettings;
import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.labelSmoothing.SourceOrTargetLabeledRulesSmoother;
import grammarExtraction.phraseProbabilityWeights.EmptyPhraseProbabilityWeights;
import grammarExtraction.phraseProbabilityWeights.FeatureValueFormatterJoshua;
import grammarExtraction.phraseProbabilityWeights.LabelSmoothingWeights;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsJoshua;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsPair;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsSet;
import grammarExtraction.phraseProbabilityWeights.PhraseProbabilityWeightsSetJoshua;
import grammarExtraction.phraseProbabilityWeights.SmoothingWeights;
import reorderingLabeling.FinePhraseCentricReorderingLabelBasic;
import grammarExtraction.reorderingLabeling.MultiDimensionalDiscreteCaseFeature;
import grammarExtraction.reorderingLabeling.FineHatReorderingFeaturesSet;
import grammarExtraction.reorderingLabeling.ReorderingFeatureSetCreater;
import grammarExtraction.reorderingLabeling.RuleFeaturesReordering;
import grammarExtraction.samtGrammarExtraction.SAMTGrammarExtractionConstraints;
import grammarExtraction.translationRuleTrie.ProbabilityRepresentation;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.LightWeightTranslationRule;
import grammarExtraction.translationRules.TranslationRuleGapInformation;
import grammarExtraction.translationRules.TranslationRuleSignatureTriple;
import mt_pipeline.MTPipelineHatsHiero;
import mt_pipeline.mt_config.MTConfigFile;
import org.junit.Test;
import util.Pair;
import alignment.AlignmentStringTriple;
import alignment.AlignmentTriple;
import ccgLabeling.LabelGenerator.UnaryCategoryHandlerType;

public class ReorderingFeaturesSetTest {
	private static final boolean USE_REORDERING_LABEL_EXTENSION = true;
	private static final boolean REORDER_LABEL_HIERO_RULES = true;
	private static final boolean REORDER_LABEL_PHRASE_PAIRS = true;

	private final static int MaxAllowedInferencesPerNode = 100000;
	private final static ChiangRuleExtracter CHIANG_RULE_EXTRACTER = createChiangRuleExtracter();
	private double ARIBTRARY_TEST_WEIGHT_VALUE = 0.6;

	private static ChiangRuleExtracter createChiangRuleExtracter() {
		SAMTGrammarExtractionConstraints chiangGrammarExtractionConstraints = SAMTGrammarExtractionConstraints.createSAMTGrammarExtractionConstraints(SystemIdentity.STANDARD_MAX_SOURCE_AND_TARGET_LENGTH,
				true,true, true, true, true, true,
				ReorderLabelingSettings.createReorderLabelingSettings(FinePhraseCentricReorderingLabelBasic.createReorderingLabelCreater(), true,true, true),
				LabelSideSettings.createLabelSideSettingsBothSides(), UnaryCategoryHandlerType.BOTTOM);
		return ChiangRuleExtracter.createChiangRuleExtracter(chiangGrammarExtractionConstraints, MaxAllowedInferencesPerNode, true, true);
	}

	private static HierarchicalPhraseBasedRuleExtractor<EbitgLexicalizedInference> createRuleExtractorHieroReorderinLabeled(
			AlignmentStringTriple alignmentTriple) {

		return CHIANG_RULE_EXTRACTER.createTranslationRuleExtractor(alignmentTriple);
	}

	private List<LightWeightTranslationRule> getAllRules(List<LightWeightPhrasePairRuleSet> ruleSets) {
		List<LightWeightTranslationRule> result = new ArrayList<LightWeightTranslationRule>();
		for (LightWeightPhrasePairRuleSet set : ruleSets) {
			result.addAll(set.getRules());
		}
		return result;
	}

	private int getNumberOfGaps(TranslationRuleSignatureTriple translationRuleSignatureTriple, WordKeyMappingTable wordKeyMappingTable) {
		TranslationRuleGapInformation translationRuleGapInformation = TranslationRuleGapInformation.createTranslationRuleGapInformation(
				translationRuleSignatureTriple, wordKeyMappingTable);
		return translationRuleGapInformation.getSourceGapKeys(wordKeyMappingTable).size();
	}

	private boolean translationRuleSignatureTripleHasOneGap(TranslationRuleSignatureTriple translationRuleSignatureTriple,
			WordKeyMappingTable wordKeyMappingTable) {
		return getNumberOfGaps(translationRuleSignatureTriple, wordKeyMappingTable) == 1;
	}

	private boolean translationRuleSignatureTripleHasTwoGaps(TranslationRuleSignatureTriple translationRuleSignatureTriple,
			WordKeyMappingTable wordKeyMappingTable) {
		return getNumberOfGaps(translationRuleSignatureTriple, wordKeyMappingTable) == 2;
	}

	private void assertReorderingFeatureHasRealValue(MultiDimensionalDiscreteCaseFeature feature) {
		Assert.assertTrue(!feature.isNullFeature());
	}

	private void assertReorderingFeatureHasNullValue(MultiDimensionalDiscreteCaseFeature feature) {
		Assert.assertTrue(feature.isNullFeature());
	}

	private void assertOneGapReorderingFeatureConsistentWithTranslationRule(TranslationRuleSignatureTriple translationRuleSignatureTriple,
			WordKeyMappingTable wordKeyMappingTable, FineHatReorderingFeaturesSet reorderingFeaturesSet) {
		if (translationRuleSignatureTripleHasOneGap(translationRuleSignatureTriple, wordKeyMappingTable)) {
			assertReorderingFeatureHasRealValue(reorderingFeaturesSet.getOneGapRuleFeature());
		} else {
			assertReorderingFeatureHasNullValue(reorderingFeaturesSet.getOneGapRuleFeature());
		}
	}

	private void assertTwoGapReorderingFeaturesConsistentWithTranslationRule(TranslationRuleSignatureTriple translationRuleSignatureTriple,
			WordKeyMappingTable wordKeyMappingTable, FineHatReorderingFeaturesSet reorderingFeaturesSet) {
		if (translationRuleSignatureTripleHasTwoGaps(translationRuleSignatureTriple, wordKeyMappingTable)) {
			assertReorderingFeatureHasRealValue(reorderingFeaturesSet.getTwoGapRuleFeatureBasic());
			assertReorderingFeatureHasRealValue(reorderingFeaturesSet.getTwoGapRuleFeatureWithReorderingInformation());
			assertReorderingFeatureHasRealValue(reorderingFeaturesSet.getSeperateGapFeatureFirstGapBasic());
			assertReorderingFeatureHasRealValue(reorderingFeaturesSet.getSeperateGapFeatureSecondGapBasic());
			assertReorderingFeatureHasRealValue(reorderingFeaturesSet.getSeperateGapFeatureFirstGapWithReorderingInformation());
			assertReorderingFeatureHasRealValue(reorderingFeaturesSet.getSeperateGapFeatureSecondGapWithReorderingInformation());
		} else {
			assertReorderingFeatureHasNullValue(reorderingFeaturesSet.getTwoGapRuleFeatureBasic());
			assertReorderingFeatureHasNullValue(reorderingFeaturesSet.getTwoGapRuleFeatureWithReorderingInformation());
			assertReorderingFeatureHasNullValue(reorderingFeaturesSet.getSeperateGapFeatureFirstGapBasic());
			assertReorderingFeatureHasNullValue(reorderingFeaturesSet.getSeperateGapFeatureSecondGapBasic());
			assertReorderingFeatureHasNullValue(reorderingFeaturesSet.getSeperateGapFeatureFirstGapWithReorderingInformation());
			assertReorderingFeatureHasNullValue(reorderingFeaturesSet.getSeperateGapFeatureSecondGapWithReorderingInformation());
		}
	}

	// @Test
	public void testCreateFineHatReorderingFeaturesSet() {
		ReorderLabelingSettings reorderLabelingSettings = ReorderLabelingSettings.createReorderLabelingSettings(
				FinePhraseCentricReorderingLabelBasic.createReorderingLabelCreater(), USE_REORDERING_LABEL_EXTENSION,REORDER_LABEL_HIERO_RULES, REORDER_LABEL_PHRASE_PAIRS);

		String sourceString = "je ne le veux pas";
		String targetString = "i dont want it";
		String alignmentString = "0-0 1-1 2-3 3-2 4-1";
		AlignmentTriple alignment = AlignmentTriple.createAlignmentTriple(sourceString, targetString, alignmentString);

		System.out.println("Aligment:" + alignment);

		AlignmentStringTriple alignmentStringTriple = AlignmentStringTriple.createTrimmedAlignmentStringTriple(sourceString, targetString, alignmentString);
		HierarchicalPhraseBasedRuleExtractor<?> hieararchicalPhraseBasedRuleExtractor = createRuleExtractorHieroReorderinLabeled(alignmentStringTriple);
		WordKeyMappingTable wordKeyMappingTable = WordKeyMappingTable.createWordKeyMappingTable(alignment, 2, 16);
		List<LightWeightPhrasePairRuleSet> ruleSets = hieararchicalPhraseBasedRuleExtractor.createLightWeightTranslationRuleSets(wordKeyMappingTable);

		List<LightWeightTranslationRule> allRules = getAllRules(ruleSets);
		for (LightWeightTranslationRule rule : allRules) {
			TranslationRuleSignatureTriple translationRuleSignatureTriple = rule.getTranslationRuleSignatureTriple();

			System.out.println("\nJushua Rule representation: " + rule.getJoshuaRuleRepresentation(wordKeyMappingTable));
			FineHatReorderingFeaturesSet reorderingFeaturesSet = FineHatReorderingFeaturesSet.createFineHatReorderingFeaturesSet(
					translationRuleSignatureTriple, wordKeyMappingTable, reorderLabelingSettings,new FeatureValueFormatterJoshua());
			System.out.println("Translation rule Signature triple: " + translationRuleSignatureTriple);
			String compressedReorderingFeaturesString = reorderingFeaturesSet.getCompressedReorderingFeaturesString();
			System.out.println("Compressed reordering Features String: " + compressedReorderingFeaturesString);
			System.out.println("Dense reordering Features String:\n" + reorderingFeaturesSet.getDenseLabeledReorderingFeaturesString());
			System.out.println("Dense reordering Features String from CompressedString:\n"
					+ FineHatReorderingFeaturesSet.getDenseReorderingFeaturesStringFromCompressedReorderingFeaturesString(compressedReorderingFeaturesString));

			assertOneGapReorderingFeatureConsistentWithTranslationRule(translationRuleSignatureTriple, wordKeyMappingTable, reorderingFeaturesSet);
			assertTwoGapReorderingFeaturesConsistentWithTranslationRule(translationRuleSignatureTriple, wordKeyMappingTable, reorderingFeaturesSet);
		}
	}

	private PhraseProbabilityWeightsSet createArbitraryPhraseProbabilityWeightSet() {

		util.Pair<Double> phraseProbabilityPair = new util.Pair<Double>(ARIBTRARY_TEST_WEIGHT_VALUE, ARIBTRARY_TEST_WEIGHT_VALUE);

		List<util.Pair<Double>> pairsList = Arrays.asList(phraseProbabilityPair, phraseProbabilityPair, phraseProbabilityPair);
		Assert.assertTrue(pairsList.size() > 0);
		SmoothingWeights labelSmoothingWeights = LabelSmoothingWeights.createLabelSmoothingWeights(pairsList,
				SourceOrTargetLabeledRulesSmoother.getSmoothingLabelPrefixStringsSourceOrTarget());
		Assert.assertTrue(labelSmoothingWeights.getExtraSmoothingWeights().size() > 0);
		PhraseProbabilityWeightsJoshua phraseProbabilityWeightsJoshua = PhraseProbabilityWeightsJoshua.createPhraseProbabilityWeightsJoshuaForTesting(
				PhraseProbabilityWeightsPair.createPhraseProbabilityWeightsPair(ARIBTRARY_TEST_WEIGHT_VALUE, ARIBTRARY_TEST_WEIGHT_VALUE, "Basic"),
				labelSmoothingWeights,new Pair<Double>(-1.0,-1.0));

		Assert.assertTrue(phraseProbabilityWeightsJoshua.getExtraSmoothingWeights().size() > 0);
		PhraseProbabilityWeightsSetJoshua arbitraryWeightsSet = PhraseProbabilityWeightsSetJoshua.createPhraseProbabilityWeightsSetJoshua(
				phraseProbabilityWeightsJoshua, new EmptyPhraseProbabilityWeights());

		return arbitraryWeightsSet;
	}

	private static MTConfigFile createHieorWithReorderingLabelsConfigFile() {
		return MTConfigFile.createMtConfigFile(MTPipelineTestJoshua.createBasicHieroWithReorderingConfigFileEnFr(), MTPipelineHatsHiero.SystemName);
	}

	private boolean isBoolean(String s) {
		return (s.equals("true") || s.equals("false"));
	}

	private boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private boolean isFloating(String s) {
		try {
			Double.parseDouble(s);
			return !(isInteger(s));
		} catch (Exception e) {
			return false;
		}
	}

	private boolean isUnpackedProbabilityString(String unpackedString) {
		return (unpackedString.toLowerCase()).contains("probability") || (unpackedString.toLowerCase()).contains("weight");
	}

	private String getValuePart(String labeledValueString) {
		return labeledValueString.split("=")[1];
	}

	private boolean consistentBooleanRepresentations(String compressedBooleanString, String unpackedBooleanString) {
		String booleanPartString = getValuePart(unpackedBooleanString);
		if (Boolean.parseBoolean(compressedBooleanString)) {
			return (Integer.parseInt(booleanPartString) == 1);
		} else {
			return (Integer.parseInt(booleanPartString) == 0);
		}
	}

	private boolean consistentProbabilityRepresentations(String compressedFloatString, String unpackedFloatString) {
		String floatPartString = getValuePart(unpackedFloatString);
		return Math.abs(Double.parseDouble(floatPartString)
				- ProbabilityRepresentation.getJoshuaLogProbabilityRepresentation(Double.parseDouble(compressedFloatString))) < 0.0001;
	}

	private boolean consistentFloatingRepresentations(String compressedFloatString, String unpackedFloatString) {
		String floatPartString = getValuePart(unpackedFloatString);
		return Math.abs(Double.parseDouble(floatPartString) - Double.parseDouble(compressedFloatString)) < 0.0001;
	}

	private boolean consistentIntegerRepresentations(String compressedIntegerString, String unpackedIntegerString) {
		String integerPartString = getValuePart(unpackedIntegerString);
		return (Integer.parseInt(compressedIntegerString) == Integer.parseInt(integerPartString));
	}

	private void assertCorrectMapping(String compressedString, String unpackedFormatString) {
		List<String> compressedPartsList = Arrays.asList(compressedString.split(" "));
		List<String> unpackedPartsList = Arrays.asList(unpackedFormatString.split(" "));

		int index = 0;
		for (String compressedPart : compressedPartsList) {
			String unpackedPart = unpackedPartsList.get(index);
			System.out.println("compressed Part: " + compressedPart + " unpackedPart: " + unpackedPart);
			if (isBoolean(compressedPart)) {
				Assert.assertTrue(consistentBooleanRepresentations(compressedPart, unpackedPart));

			} else if (isInteger(compressedPart)) {
				Assert.assertTrue(consistentIntegerRepresentations(compressedPart, unpackedPart));

			} else if (isFloating(compressedPart)) {
				if (isUnpackedProbabilityString(unpackedPart)) {
					Assert.assertTrue(consistentProbabilityRepresentations(compressedPart, unpackedPart));
				} else {
					Assert.assertTrue(consistentFloatingRepresentations(compressedPart, unpackedPart));
				}

			}
			index++;
		}
	}

	/**
	 * A method that tests that RuleFeaturesReordering does not change if we
	 * compress it and write the compressed representation as a String and then
	 * reconstruct RuleFeaturesReordering from that String. Note that currently
	 * we use the method getLabeledFeaturesRepresentation() - a String
	 * representation - as a proxy for equality. To be more precise we should
	 * recursively define equals method for RuleFeaturesReordering and all its
	 * sub-objects
	 * 
	 * @param reorderingInformationExtendedFeatures
	 * @param hieroReorderingConfigFile
	 */
	private void assertRuleFeaturesReorderingDoesNotChangeFromCompressionAndUnCompression(RuleFeaturesReordering reorderingInformationExtendedFeatures,
			MTConfigFile hieroReorderingConfigFile) {
		String originalLabeledFeaturesRepresentation = reorderingInformationExtendedFeatures.getSparseLabeledFeaturesRepresentation();
		System.out.println("reorderingInformationExtendedFeatures.getLabeledFeaturesRepresentation() :" + originalLabeledFeaturesRepresentation);
		System.out.println();
		String compressedRepresentationString = reorderingInformationExtendedFeatures.getCompressedFeaturesRepresentation();
		System.out.println("reorderingInformationExtendedFeatures.getLabeledAndCompressedFeaturesRepresentation() :" + compressedRepresentationString);
		SystemIdentity systemIdentity = SystemIdentity.createSystemIdentity(hieroReorderingConfigFile);
		RuleFeaturesReordering reorderingInformationExtendedFeatures2 = RuleFeaturesReordering
				.createRuleFeaturesReorderingFromCompressedUnlabeledFeaturesString(compressedRepresentationString, systemIdentity);

		String unpackedRepresentationString = reorderingInformationExtendedFeatures2.getSparseLabeledFeaturesRepresentation();
		System.out.println("final format: " + unpackedRepresentationString);
		assertCorrectMapping(compressedRepresentationString, unpackedRepresentationString);
		Assert.assertEquals(originalLabeledFeaturesRepresentation, unpackedRepresentationString);

	}

	@Test
	public void testReorderingInformationExtendedFeautres() {
		ReorderLabelingSettings reorderLabelingSettings = ReorderLabelingSettings.createReorderLabelingSettings(
				FinePhraseCentricReorderingLabelBasic.createReorderingLabelCreater(), USE_REORDERING_LABEL_EXTENSION,REORDER_LABEL_HIERO_RULES, REORDER_LABEL_PHRASE_PAIRS);

		String sourceString = "je ne le veux pas";
		String targetString = "i dont want it";
		String alignmentString = "0-0 1-1 2-3 3-2 4-1";
		AlignmentTriple alignment = AlignmentTriple.createAlignmentTriple(sourceString, targetString, alignmentString);

		System.out.println("Aligment:" + alignment);

		AlignmentStringTriple alignmentStringTriple = AlignmentStringTriple.createTrimmedAlignmentStringTriple(sourceString, targetString, alignmentString);
		HierarchicalPhraseBasedRuleExtractor<?> hieararchicalPhraseBasedRuleExtractor = createRuleExtractorHieroReorderinLabeled(alignmentStringTriple);
		WordKeyMappingTable wordKeyMappingTable = WordKeyMappingTable.createWordKeyMappingTable(alignment, 2, 16);
		List<LightWeightPhrasePairRuleSet> ruleSets = hieararchicalPhraseBasedRuleExtractor.createLightWeightTranslationRuleSets(wordKeyMappingTable);

		List<LightWeightTranslationRule> allRules = getAllRules(ruleSets);
		MTConfigFile hieroReorderingConfigFile = createHieorWithReorderingLabelsConfigFile();
		
		for (LightWeightTranslationRule rule : allRules) {
			TranslationRuleSignatureTriple translationRuleSignatureTriple = rule.getTranslationRuleSignatureTriple();

			System.out.println("ExcreateHieorWithReorderingLabelsConfigFiletra smoothing weights: " + createArbitraryPhraseProbabilityWeightSet().getExtraSmoothingWeights());
			RuleFeaturesReordering reorderingInformationExtendedFeatures = RuleFeaturesReordering.createRuleFeaturesReordering(ARIBTRARY_TEST_WEIGHT_VALUE,
					createArbitraryPhraseProbabilityWeightSet(), ARIBTRARY_TEST_WEIGHT_VALUE, ARIBTRARY_TEST_WEIGHT_VALUE, ARIBTRARY_TEST_WEIGHT_VALUE, false,
					false, false, translationRuleSignatureTriple, wordKeyMappingTable, ReorderingFeatureSetCreater.createHatFineReorderingFeatureSetCreater(reorderLabelingSettings,new FeatureValueFormatterJoshua()));

			assertRuleFeaturesReorderingDoesNotChangeFromCompressionAndUnCompression(reorderingInformationExtendedFeatures, hieroReorderingConfigFile);

		}
	}
}
