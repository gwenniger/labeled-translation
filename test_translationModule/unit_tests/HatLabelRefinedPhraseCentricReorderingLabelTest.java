package unit_tests;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import junit.framework.Assert;
import grammarExtraction.reorderingLabeling.HatLabelRefinedPhraseCentricReorderingLabel;
import org.junit.Test;
import reorderingLabeling.ReorderingLabel;
import reorderingLabeling.ReorderingLabelCreater;

public class HatLabelRefinedPhraseCentricReorderingLabelTest {

	@Test
	public void testGetHatLabelForLabelContainingString() {

		String reorderingLabelContainingString = "HAT-[1,2,3]";
		ReorderingLabelCreater reorderingLabelCreater = HatLabelRefinedPhraseCentricReorderingLabel
				.createReorderingLabelCreater();
		ReorderingLabel reorderingLabel = reorderingLabelCreater
				.getReorderingLabelForReorderingLabelContainingString(reorderingLabelContainingString);
		System.out.println(reorderingLabel);
		Assert.assertEquals("ReorderingLabel: "
				+ reorderingLabelContainingString, reorderingLabel.toString());
	}

	@Test
	public void testGetHatLabelForHatSetPermutation() {
		List<SortedSet<Integer>> sortedSourceSetPermutation = new ArrayList<>();
		SortedSet<Integer> set1 = new TreeSet<Integer>();
		set1.add(0);
		SortedSet<Integer> set2 = new TreeSet<Integer>();
		set2.add(1);
		SortedSet<Integer> set3 = new TreeSet<Integer>();
		set3.add(0);
		sortedSourceSetPermutation.add(set1);
		sortedSourceSetPermutation.add(set2);
		sortedSourceSetPermutation.add(set3);

		HatLabelRefinedPhraseCentricReorderingLabel hatLabelRefinedPhraseCentricReorderingLabel = new HatLabelRefinedPhraseCentricReorderingLabel();

		ReorderingLabel reorderingLabel = hatLabelRefinedPhraseCentricReorderingLabel
				.getHatLabelForHatSetPermutation(sortedSourceSetPermutation);

		System.out.println(reorderingLabel);
		Assert.assertEquals("ReorderingLabel: HAT-[1]" + "",
				reorderingLabel.toString());

	}

}
