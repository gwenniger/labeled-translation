package unit_tests;

import java.io.File;
import java.util.List;
import junit.framework.Assert;
import mt_pipeline.evaluation.EvaluationFilePathsTriple;
import mt_pipeline.evaluation.EvaluationTestFilesCreater;
import mt_pipeline.evaluation.IndividualSentencesEvaluater;
import mt_pipeline.evaluation.SentenceNumberScoresTriple;
import org.junit.Test;
import util.FileUtil;
import util.Utility;

public class IndividualSentencesEvaluaterTest {

	private static final String TEST_REF_NAME = "MTEvalInterfaceTest.ref.txt";
	private static final String TEST_SRC_NAME = "MTEvalInterfaceTest.src.txt";
	private static final String TEST_TST_NAME = "MTEvalInterfaceTest.tst.txt";
	public static final String OUTPUT_SCORE_FILE_PATH_EVEN_CORRECT = "MTEvalInterfaceTest.testResultIndividualSentences-EvenCorrect.txt";
	public static final String OUTPUT_SCORE_FILE_PATH_ODD_CORRECT = "MTEvalInterfaceTest.testResultIndividualSentences-OddCorrect.txt";
	private static final int SENTENCE_LENGTH = 10;
	private static final int TEST_SIZE = 100;

	public void assertEvenBleuScoresAreValue(String resultFileName, double value) {
		List<String> lines = FileUtil.getSentences(new File(resultFileName), true);
		for (int i = 1; i < lines.size(); i++) {
			SentenceNumberScoresTriple sentenceNumberScoresTriple = SentenceNumberScoresTriple.createSentenceNumberScoresTripleFromString(lines.get(i));
			if (Utility.isEven(i)) {
				Assert.assertEquals(value, sentenceNumberScoresTriple.getBleuScore());
			}
		}
	}

	public void assertOddBleuScoresAreValue(String resultFileName, double value) {
		List<String> lines = FileUtil.getSentences(new File(resultFileName), true);
		for (int i = 1; i < lines.size(); i++) {
			SentenceNumberScoresTriple sentenceNumberScoresTriple = SentenceNumberScoresTriple.createSentenceNumberScoresTripleFromString(lines.get(i));
			if (!Utility.isEven(i)) {
				Assert.assertEquals(value, sentenceNumberScoresTriple.getBleuScore());
			}
		}
	}

	private void assertEvenBleuScoresAreOne(String resultFileName) {
		assertEvenBleuScoresAreValue(resultFileName, 1.0);
	}

	private void assertEvenBleuScoresAreZero(String resultFileName) {
		assertEvenBleuScoresAreValue(resultFileName, 0);
	}

	private void assertOddBleuScoresAreOne(String resultFileName) {
		assertOddBleuScoresAreValue(resultFileName, 1.0);
	}

	private void assertOddBleuScoresAreZero(String resultFileName) {
		assertOddBleuScoresAreValue(resultFileName, 0);
	}

	private void assertLineIndividualBleuScoresAreLineSpecificAndConsistentWithEvenCorrectData(String outputFileName) {
		assertEvenBleuScoresAreOne(outputFileName);
		assertOddBleuScoresAreZero(outputFileName);
	}

	private void assertLineIndividualBleuScoresAreLineSpecificAndConsistentWithOddCorrectData(String outputFileName) {
		assertOddBleuScoresAreOne(outputFileName);
		assertEvenBleuScoresAreZero(outputFileName);
	}

	public static void createIndividualScoresFileCorrectEvenSentences() {
		EvaluationFilePathsTriple evaluationFilePathsTriple = EvaluationFilePathsTriple.createEvaluationFilePathsTriple(TEST_SRC_NAME, TEST_TST_NAME, TEST_REF_NAME);
		EvaluationTestFilesCreater evaluationTestFilesCreater = EvaluationTestFilesCreater.createEvaluationTestFilesCreaterCorrectEvenSentences(evaluationFilePathsTriple, SENTENCE_LENGTH, TEST_SIZE);
		evaluationTestFilesCreater.writeFiles();
		IndividualSentencesEvaluater individualSentencesEvaluater = IndividualSentencesEvaluater.createIndividualSentencesEvaluaterForPlainFiles(TEST_SRC_NAME, TEST_TST_NAME, TEST_REF_NAME,
				MTEvalInterfaceTest.getMTEvalDir(), OUTPUT_SCORE_FILE_PATH_EVEN_CORRECT);
		individualSentencesEvaluater.createSentencIndividualScoresFile();
	}

	public static void createIndividualScoresFileCorrectOddSentences() {
		EvaluationFilePathsTriple evaluationFilePathsTriple = EvaluationFilePathsTriple.createEvaluationFilePathsTriple(TEST_SRC_NAME, TEST_TST_NAME, TEST_REF_NAME);
		EvaluationTestFilesCreater evaluationTestFilesCreater = EvaluationTestFilesCreater.createEvaluationTestFilesCreaterCorrectOddSentences(evaluationFilePathsTriple, SENTENCE_LENGTH, TEST_SIZE);
		evaluationTestFilesCreater.writeFiles();
		IndividualSentencesEvaluater individualSentencesEvaluater = IndividualSentencesEvaluater.createIndividualSentencesEvaluaterForPlainFiles(TEST_SRC_NAME, TEST_TST_NAME, TEST_REF_NAME,
				MTEvalInterfaceTest.getMTEvalDir(), OUTPUT_SCORE_FILE_PATH_ODD_CORRECT);
		individualSentencesEvaluater.createSentencIndividualScoresFile();
	}

	@Test
	public void test() {
		createIndividualScoresFileCorrectEvenSentences();
		assertLineIndividualBleuScoresAreLineSpecificAndConsistentWithEvenCorrectData(OUTPUT_SCORE_FILE_PATH_EVEN_CORRECT);
		createIndividualScoresFileCorrectOddSentences();
		assertLineIndividualBleuScoresAreLineSpecificAndConsistentWithOddCorrectData(OUTPUT_SCORE_FILE_PATH_ODD_CORRECT);
	}

}
