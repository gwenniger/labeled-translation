package unit_tests;

import grammarExtraction.grammarCombining.GrammarCombiner;
import java.util.Arrays;
import java.util.List;
import junit.framework.Assert;
import org.junit.Test;
import util.FileUtil;

public class GrammarCombinerTest {

    private static String TEST_FOLDER_PATH = "./GrammarCombinerTestFolder/";
    private static String TEST_GRAMMAR_FILE_PATH_SYNTAX = TEST_FOLDER_PATH
	    + "TestGrammarSyntaxUnsorted.txt";
    private static String TEST_GRAMMAR_FILE_PATH_HATS = TEST_FOLDER_PATH
	    + "TestGrammarHATsUnsorted.txt";
    private static String REFERENCE_FILE_PATH = TEST_FOLDER_PATH + "ReferenceGrammarSorted.txt";
    private static String RESULT_FILE_PATH = TEST_FOLDER_PATH + "CombinedGrammarSorted.txt";

    private static String GRAMMAR_ONE_RULE1 = "[UnknownWordPreNonterminal] ||| [UnknownWord,1] ||| [UnknownWord,1] ||| 9.0 9.0 9.0 9.0 -0.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 0.0 2.7182817 ";
    private static String GRAMMAR_ONE_RULE2 = "[X-SyntacticLabel:X-SyntacticLabel] ||| 为 ||| ｋ１５ ||| 0.30103 0.30103 -0.0 -0.0 0.60206 1.0 0.0 0.0 1.0 1.0 0.0 0.0 0.0 1.0 1.0 1.0 ";
    private static String GRAMMAR_ONE_RULE3 = "[X-SyntacticLabel:X-SyntacticLabel] ||| 为 ||| ｋ１４ ||| 0.30103 0.30103 -0.0 -0.0 0.60206 1.0 0.0 0.0 1.0 1.0 0.0 0.0 0.0 1.0 1.0 1.0 ";
    private static String GRAMMAR_ONE_RULE4 = "[DET:NOUN] ||| die [NOUN:ADP,1] beide programme ||| the [NOUN:ADP,1] both programmes ||| -0.0 0.20411998 0.53147894 -0.0 4.5584683 1.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 3.0 2.629166 ";
    private static String GRAMMAR_ONE_RULE5 = "[ADJ:.] ||| [ADJ:ADJ,1] abwegigkeit [.:.,2] ||| [ADJ:ADJ,1] shift [.:.,2] ||| -0.0 -0.0 -0.0 -0.0 3.1598785 1.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 1.0 1.1798865 ";

    // Note that we put the rules 1 and 2 deliberately in inverted order with respect to the same rules for grammar one
    // this is to test robustness with respect to sorting (the standard linux sort method cannot distinguish these rules
    // so it will mess up the sorting for them)
    private static String GRAMMAR_TWO_RULE1 = "[UnknownWordPreNonterminal] ||| [UnknownWord,1] ||| [UnknownWord,1] ||| 9.0 9.0 9.0 9.0 -0.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 0.0 2.7182817 ";
    private static String GRAMMAR_TWO_RULE2 = "[ATOMIC] ||| 为 ||| ｋ１４ ||| 0.30103 0.30103 -0.0 -0.0 0.60206 1.0 0.0 0.0 1.0 1.0 0.0 0.0 0.0 1.0 1.0 1.0 ";
    private static String GRAMMAR_TWO_RULE3 = "[ATOMIC] ||| 为 ||| ｋ１５ ||| 0.30103 0.30103 -0.0 -0.0 0.60206 1.0 0.0 0.0 1.0 1.0 0.0 0.0 0.0 1.0 1.0 1.0 ";
    private static String GRAMMAR_TWO_RULE4 = "[MONO] ||| die [MONO,1] beide programme ||| the [MONO,1] both programmes ||| -0.0 0.20411998 0.53147894 -0.0 4.5584683 1.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 3.0 2.629166 ";
    private static String GRAMMAR_TWO_RULE5 = "[MONO] ||| [INV,1] abwegigkeit [MONO,2] ||| [INV,1] shift [MONO,2] ||| -0.0 -0.0 -0.0 -0.0 3.1598785 1.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 1.0 1.1798865 ";

    private static String RULE1_EXPECTED_RESULT = "[ADJ:.<<>>MONO] ||| [ADJ:ADJ<<>>INV,1] abwegigkeit [.:.<<>>MONO,2] ||| [ADJ:ADJ<<>>INV,1] shift [.:.<<>>MONO,2] ||| -0.0 -0.0 -0.0 -0.0 3.1598785 1.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 1.0 1.1798865 ";
    private static String RULE2_EXPECTED_RESULT = "[DET:NOUN<<>>MONO] ||| die [NOUN:ADP<<>>MONO,1] beide programme ||| the [NOUN:ADP<<>>MONO,1] both programmes ||| -0.0 0.20411998 0.53147894 -0.0 4.5584683 1.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 3.0 2.629166 ";
    private static String RULE3_EXPECTED_RESULT = "[UnknownWordPreNonterminal] ||| [UnknownWord,1] ||| [UnknownWord,1] ||| 9.0 9.0 9.0 9.0 -0.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 0.0 2.7182817 ";    	      
    private static String RULE4_EXPECTED_RESULT = "[X-SyntacticLabel:X-SyntacticLabel<<>>ATOMIC] ||| 为 ||| ｋ１４ ||| 0.30103 0.30103 -0.0 -0.0 0.60206 1.0 0.0 0.0 1.0 1.0 0.0 0.0 0.0 1.0 1.0 1.0 ";
    private static String RULE5_EXPECTED_RESULT = "[X-SyntacticLabel:X-SyntacticLabel<<>>ATOMIC] ||| 为 ||| ｋ１５ ||| 0.30103 0.30103 -0.0 -0.0 0.60206 1.0 0.0 0.0 1.0 1.0 0.0 0.0 0.0 1.0 1.0 1.0 ";
     

    private static List<String> GRAMMAR_ONE_LINES_LIST = Arrays.asList(GRAMMAR_ONE_RULE1, GRAMMAR_ONE_RULE2, GRAMMAR_ONE_RULE3,
	    GRAMMAR_ONE_RULE4, GRAMMAR_ONE_RULE5);

    private static List<String> GRAMMAR_TWO_LINES_LIST = Arrays.asList(GRAMMAR_TWO_RULE1,
	    GRAMMAR_TWO_RULE2, GRAMMAR_TWO_RULE3, GRAMMAR_TWO_RULE4, GRAMMAR_TWO_RULE5);

    private static List<String> EXPECTED_OUTPUT_TEST_FILE_LINES_LIST = Arrays.asList(
	    RULE1_EXPECTED_RESULT, RULE2_EXPECTED_RESULT, RULE3_EXPECTED_RESULT,
	    RULE4_EXPECTED_RESULT, RULE5_EXPECTED_RESULT);

    public static void createTestGrammars() {
	util.FileUtil.writeLinesListToFile(GRAMMAR_ONE_LINES_LIST, TEST_GRAMMAR_FILE_PATH_SYNTAX);
	util.FileUtil.writeLinesListToFile(GRAMMAR_TWO_LINES_LIST, TEST_GRAMMAR_FILE_PATH_HATS);
    }

    public static void createReferemceResultFile() {
	util.FileUtil.writeLinesListToFile(EXPECTED_OUTPUT_TEST_FILE_LINES_LIST,
		REFERENCE_FILE_PATH);
    }

    public void setup() {
	FileUtil.createFolderIfNotExisting(TEST_FOLDER_PATH);
	createTestGrammars();
	createReferemceResultFile();
    }

    private static List<String> getInputGrammarFilePaths(){
	return Arrays.asList(TEST_GRAMMAR_FILE_PATH_SYNTAX,TEST_GRAMMAR_FILE_PATH_HATS);
    }
    
    @Test
    public void testCreateSortedOutputFile() {
	setup();
	GrammarCombiner grammarCombiner = GrammarCombiner.createGrammarCombiner(getInputGrammarFilePaths(),RESULT_FILE_PATH,
		TEST_FOLDER_PATH, false, 0);
	grammarCombiner.createCombinedResultGrammarFile();
	Assert.assertTrue(FileUtil.filesContainSameLinesInSameOrder(REFERENCE_FILE_PATH,
		RESULT_FILE_PATH, false));
    }
}
