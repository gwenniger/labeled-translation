package unit_tests;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;
import mt_pipeline.evaluation.LabelSubstitutionFeatureWeightTableConstructer;

import org.junit.Test;

import util.FileUtil;
import util.Utility;

public class LabelSubstitutionFeatureWeightTableConstructerTest {

    private static String LABEL_SUBSTITUTION_TEST_FILE_NAME = "LabelSubsitutionFeatureWeightTableTestFile.txt";

    private static List<String> TEST_FILE_LINES_LIST = Arrays.asList("tm_pt_15 0.0508761884000771",
	    "#wordpenalty weight", "wordpenalty -0.0167062097777727",
	    "#out of vocubalary penalty weight", "oovpenalty -0.902341358054368",
	    "LabelSubstitution_[X-E.F.M.]_substitutes_[X-L.B.M.] 0.0276074958005188",
	    "LabelSubstitution_MATCH 0.00770013539209968",
	    "LabelSubstitution_[X-L.B.I.]_substitutes_[X] -0.014868739147863",
	    "LabelSubstitution_[X-E.F.D.]_substitutes_[X-L.B.I.] -0.00282738137606799",
	    "LabelSubstitution_[X-R.B.M.]_substitutes_[X-L.B.I.] -0.00164667479233101",
	    "LabelSubstitution_[X-R.B.I.]_substitutes_[X] 0.00906304174684495",
	    "LabelSubstitution_[X-R.B.M.]_substitutes_[X-R.B.I.] 0.0252893822483697",
	    "LabelSubstitution_[X-L.B.M.]_substitutes_[X] -0.0127914382606376",
	    "LabelSubstitution_[X-E.F.D.]_substitutes_[X] -0.000570184463675186",
	    "LabelSubstitution_[X-E.F.I.]_substitutes_[X-E.F.I.] 0.0020679557348538",
	    "LabelSubstitution_[X-TOP.]_substitutes_[X-L.B.I.] -8.87541417380217e-05",
	    "LabelSubstitution_[X-R.B.M.]_substitutes_[X-E.F.D.] 0.0325897759847532",
	    "LabelSubstitution_[X-L.B.I.]_substitutes_[X-E.F.D.] 0.00415575610339024",
	    "LabelSubstitution_[X-R.B.M.]_substitutes_[X-L.B.M.] -0.00159216345017924",
	    "LabelSubstitution_[X-L.B.M.]_substitutes_[X-E.F.I.] 0.0103906305480808",
	    "LabelSubstitution_[X-E.F.M.]_substitutes_[X-E.F.I.] -0.0210286804522494",
	    "LabelSubstitution_[X-TOP.]_substitutes_[X-E.F.D.] 0.00303959369950226",
	    "LabelSubstitution_[X-L.B.M.]_substitutes_[X-E.F.D.] 2.07862713749626e-05",
	    "LabelSubstitution_[X-E.F.D.]_substitutes_[X-E.F.M.] 0.00237284593601265",
	    "LabelSubstitution_[X-E.F.I.]_substitutes_[X-L.B.I.] 0.00357371643590489",
	    "LabelSubstitution_[X-R.B.I.]_substitutes_[X-E.F.D.] 0.0023832962969433",
	    "LabelSubstitution_[X-L.B.I.]_substitutes_[X-E.F.I.] 0.00538326212955578",
	    "LabelSubstitution_[X-R.B.M.]_substitutes_[X-E.F.I.] -0.00835843445016005",
	    "LabelSubstitution_[X-L.B.I.]_substitutes_[X-R.B.M.] -0.00560269514890923",
	    "LabelSubstitution_[X-E.F.D.]_substitutes_[X-E.F.I.] 0.00560106189626085",
	    "LabelSubstitution_[X-E.F.D.]_substitutes_[X-R.B.I.] 0.00709789496817377",
	    "LabelSubstitution_[X-L.B.I.]_substitutes_[X-L.B.M.] 0.0114081592281203",
	    "LabelSubstitution_[X-TOP.]_substitutes_[X] 0.00138965240187448",
	    "LabelSubstitution_[X-E.F.M.]_substitutes_[X-E.F.M.] -0.00637535873264192",
	    "LabelSubstitution_[X-L.B.M.]_substitutes_[X-L.B.M.] -0.00675442070820868",
	    "LabelSubstitution_[X-L.B.I.]_substitutes_[X-L.B.I.] 0.00970276102101804",
	    "LabelSubstitution_[X-L.B.I.]_substitutes_[X-E.F.M.] -0.010076235553314",
	    "LabelSubstitution_[X-E.F.I.]_substitutes_[X-R.B.I.] 0.00406779378218431",
	    "LabelSubstitution_[X-E.F.M.]_substitutes_[X-R.B.I.] -0.0292240475195679",
	    "LabelSubstitution_[X-R.B.I.]_substitutes_[X-R.B.I.] 0.0119588109725284",
	    "LabelSubstitution_[X-R.B.I.]_substitutes_[X-E.F.I.] 0.0256674863765186",
	    "LabelSubstitution_[X-E.F.D.]_substitutes_[X-E.F.D.] 0.0053684400547693",
	    "LabelSubstitution_[X-R.B.I.]_substitutes_[X-E.F.M.] -0.0138367199556046",
	    "LabelSubstitution_[X-L.B.I.]_substitutes_[X-R.B.I.] -0.000576222586436173",
	    "LabelSubstitution_[X-E.F.I.]_substitutes_[X-R.B.M.] 0.000765046240741757",
	    "LabelSubstitution_[X-R.B.I.]_substitutes_[X-L.B.M.] -0.00326503168535993",
	    "LabelSubstitution_[X-E.F.M.]_substitutes_[X-L.B.I.] -0.0141723349547053",
	    "LabelSubstitution_[X-R.B.M.]_substitutes_[X-R.B.M.] -0.00826812663078989",
	    "LabelSubstitution_[X-TOP.]_substitutes_[X-R.B.I.] -0.000228736419401705",
	    "LabelSubstitution_[X-TOP.]_substitutes_[X-E.F.I.] 0.000305192772886029",
	    "LabelSubstitution_[X-L.B.M.]_substitutes_[X-R.B.I.] 0.00946824804488099",
	    "LabelSubstitution_[X-L.B.M.]_substitutes_[X-L.B.I.] -0.00340980172650146",
	    "LabelSubstitution_[X-R.B.I.]_substitutes_[X-R.B.M.] -0.00898382285335068",
	    "LabelSubstitution_[X-R.B.I.]_substitutes_[X-L.B.I.] -0.00714093670150154",
	    "LabelSubstitution_[X-E.F.M.]_substitutes_[X-R.B.M.] -0.0107243544125359",
	    "LabelSubstitution_[X-E.F.I.]_substitutes_[X] -0.0107585176371059",
	    "LabelSubstitution_[X-R.B.M.]_substitutes_[X] 0.0312636485148343",
	    "LabelSubstitution_[X-E.F.I.]_substitutes_[X-E.F.M.] -0.0243477445560521",
	    "LabelSubstitution_[X-R.B.M.]_substitutes_[X-E.F.M.] -0.0204103776639318",
	    "LabelSubstitution_[X-TOP.]_substitutes_[X-E.F.M.] -0.00221978683067819",
	    "LabelSubstitution_[X-E.F.D.]_substitutes_[X-R.B.M.] 0.00184809291217182",
	    "LabelSubstitution_[X-L.B.M.]_substitutes_[X-R.B.M.] -0.0124316314741886",
	    "LabelSubstitution_[X-E.F.D.]_substitutes_[X-L.B.M.] -0.00151128674384638",
	    "LabelSubstitution_NOMATCH -0.0311196029993037",
	    "LabelSubstitution_[X-E.F.M.]_substitutes_[X] 0.00433654366346196",
	    "LabelSubstitution_[X-TOP.]_substitutes_[X-L.B.M.] 0.00368641086864352",
	    "LabelSubstitution_[X-E.F.I.]_substitutes_[X-E.F.D.] 0.0038245373783356",
	    "LabelSubstitution_[X-L.B.M.]_substitutes_[X-E.F.M.] -0.00404179682085624",
	    "LabelSubstitution_[X-E.F.M.]_substitutes_[X-E.F.D.] -0.0105261291174213",
	    "LabelSubstitution_[X-E.F.I.]_substitutes_[X-L.B.M.] -0.00309228758757914",
	    "LabelSubstitution_[X-TOP.]_substitutes_[X-R.B.M.] 3.33715268388603e-06");

    public static void createTestFile() {
	util.FileUtil.writeLinesListToFile(TEST_FILE_LINES_LIST, LABEL_SUBSTITUTION_TEST_FILE_NAME);
    }

    @Test
    public void test() {
	  BufferedWriter log; 
	createTestFile();
	LabelSubstitutionFeatureWeightTableConstructer labelSubstitutionFeatureWeightTableConstructer = LabelSubstitutionFeatureWeightTableConstructer
		.createLabelSubstitutionFeatureWeightTableConstructer(Arrays
			.asList(LABEL_SUBSTITUTION_TEST_FILE_NAME));
	  log = new BufferedWriter(new OutputStreamWriter(System.out));
	try {
	    labelSubstitutionFeatureWeightTableConstructer.createAndWriteTable(log);
	} catch (IOException e) {	 
	    e.printStackTrace();
	    Assert.fail();
	}
	finally{
	    FileUtil.closeCloseableIfNotNull(log);
	}

    }

}
