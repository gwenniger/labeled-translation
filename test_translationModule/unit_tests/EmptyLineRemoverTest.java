package unit_tests;

import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import junit.framework.Assert;
import util.EmptyLineRemover;
import util.FileUtil;

public class EmptyLineRemoverTest {

    private static String TEST_FOLDER_PATH = "./WMT-DataPreparation-TestFolder/";
    private static String TEST_FILE_PATH = TEST_FOLDER_PATH + "testFileWithEmptlyLines.txt";
    private static String TEST_OUTPUT_FILE_PATH = TEST_FOLDER_PATH + "testOutputFile.txt";
    private final static List<String> TEST_LINES = Arrays.asList("line1", "", "lin3", "", "line5");
    private final static List<String> EXPECTED_RESULT_LINES = Arrays.asList("line1", "lin3",
	    "line5");

    private void setup() {
	FileUtil.createFolderIfNotExisting(TEST_FOLDER_PATH);
	FileUtil.writeLinesListToFile(TEST_LINES, TEST_FILE_PATH);
    }

    @Test
    public void test() {
	setup();
	// EmptyLineRemover.removeEmptyLinesInPlace(TEST_FILE_PATH);
	EmptyLineRemover.removeEmptyLines(TEST_FILE_PATH, TEST_OUTPUT_FILE_PATH);
	List<String> resultFileStrings = FileUtil.getLinesInFile(TEST_OUTPUT_FILE_PATH, false);
	Assert.assertEquals(EXPECTED_RESULT_LINES, resultFileStrings);
    }

}
