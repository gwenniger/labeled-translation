package unit_tests;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import junit.framework.Assert;
import integration_tests.MTPipelineTestJoshua;
import mt_pipeline.mtPipelineTest.TestRerankingSettings;
import org.junit.Test;
import util.FileUtil;

public class GrammarSortingNoRulesLostTest extends MTPipelineTestJoshua {

    private static String RULE1 = "[UnknownWordPreNonterminal] ||| [UnknownWord,1] ||| [UnknownWord,1] ||| 9.0 9.0 9.0 9.0 -0.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 0.0 2.7182817 "; 
    private static String RULE2 = "[X-SyntacticLabel:X-SyntacticLabel] ||| 为 ||| ｋ１４ ||| 0.30103 0.30103 -0.0 -0.0 0.60206 1.0 0.0 0.0 1.0 1.0 0.0 0.0 0.0 1.0 1.0 1.0 "; 
    private static String RULE3 = "[X-SyntacticLabel:X-SyntacticLabel] ||| 为 ||| ｋ１５ ||| 0.30103 0.30103 -0.0 -0.0 0.60206 1.0 0.0 0.0 1.0 1.0 0.0 0.0 0.0 1.0 1.0 1.0 "; 
    // The produced grammars end with an empty line, so we add it at the end
    private static Set<String> EXPECTED_RULES_SET = new HashSet<String>(Arrays.asList(RULE1,RULE2,RULE3,""));
    
    private static String DEV_GRAMMAR_FILE_PATH = "./mtPipelineTestsResults/JoshuaHatsBoundaryTagLabeled-TargetInsideTags-ZhEn/JoshuaHatsBoundaryTagLabeled/intermediate-results/mert/de-en-grammar-mert";
    private static String TEST_GRAMMAR_FILE_PATH = "./mtPipelineTestsResults/JoshuaHatsBoundaryTagLabeled-TargetInsideTags-ZhEn/JoshuaHatsBoundaryTagLabeled/intermediate-results/test/de-en-grammar";

    
    /**
     * In this test we test that no rules are lost during sorting.
     * There was a bug were one expected rule disappeared when from the 
     * created grammar. After a long debugging process, it was established 
     * that this rule was still present up to the raw (unsorted) form of the grammar,
     * but was lost in the last step during sorting of the grammar.
     * 
     * We were using the option "-u" when using the linux sort program: 
     * 
     *  -u, --unique
     *         with -c, check for strict ordering; without -c, output only the first of an equal run
     * 
     * This option has the effect of keeping only unique versions of every line. 
     * But somehow (because of a bug?) the lines
     * [X-SyntacticLabel:X-SyntacticLabel] ||| 为 ||| ｋ１４ ||| 0.30103 0.30103 -0.0 -0.0 0.60206 1.0 0.0 0.0 1.0 1.0 0.0 0.0 0.0 1.0 1.0 1.0
     * and
     * [X-SyntacticLabel:X-SyntacticLabel] ||| 为 ||| ｋ１５ ||| 0.30103 0.30103 -0.0 -0.0 0.60206 1.0 0.0 0.0 1.0 1.0 0.0 0.0 0.0 1.0 1.0 1.0
     * are apparently considered identical to the sort program, which is not what we want.
     * 
     * Hence we should remove the -u option when sorting grammars.
     * Interestingly, this bug seems to have only turned up recently it seems, possibly it was only 
     * introduced in a new version of sort?
     */
    @Test
    public void testNoGrammarRulesAreLostDuringGrammarSorting(){
	testHieroPipelineWithCoarseInsidePhraseBoundaryPosTagsTarget(TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_DEBUGGING3,
		"ZhEn", TestRerankingSettings.createNoRerankingSettings(),
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_ZH_EN, false,true);
	Set<String> devGrammarRules = new HashSet<String>(FileUtil.getSentences(new File(DEV_GRAMMAR_FILE_PATH), true));
	Set<String> testGrammarRules = new HashSet<String>(FileUtil.getSentences(new File(TEST_GRAMMAR_FILE_PATH), true));
	
	// The dev and test grammar should be the same here, both should contain exactly the rules in the EXPECTED_RULES_SET	
	// We compare sets instead of lists, because the relevant rules:
	// 1. [X-SyntacticLabel:X-SyntacticLabel] ||| 为 ||| ｋ１４ ||| 0.30103 0.30103 -0.0 -0.0 0.60206 1.0 0.0 0.0 1.0 1.0 0.0 0.0 0.0 1.0 1.0 1.0
	// 2. [X-SyntacticLabel:X-SyntacticLabel] ||| 为 ||| ｋ１５ ||| 0.30103 0.30103 -0.0 -0.0 0.60206 1.0 0.0 0.0 1.0 1.0 0.0 0.0 0.0 1.0 1.0 1.0
	// are not distinguishable for sort, so we can not predict their order of appearance beforehand
	//
	System.out.println("Expected rules Set: \n" + EXPECTED_RULES_SET);
	System.out.println("DEV rules set: \n" + devGrammarRules);
	Assert.assertEquals(EXPECTED_RULES_SET, devGrammarRules);
	
	System.out.println("Expected rules Set: \n" + EXPECTED_RULES_SET);
	System.out.println("TEST rules set: \n" + testGrammarRules);
	Assert.assertEquals(EXPECTED_RULES_SET, testGrammarRules);
	
	
    }
    

}
