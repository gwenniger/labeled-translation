package unit_tests;

import static org.junit.Assert.*;
import junit.framework.Assert;
import integration_tests.MTPipelineTest;
import mt_pipeline.evaluation.TarGzFileLineInterator;
import mt_pipeline.mtPipelineTest.TestNBestListFilesCreater;

import org.junit.Test;

public class TarGzFileLineInteratorTest {

    @Test
    public void test() {
	TestNBestListFilesCreater.createTestTarGzFile();

	TarGzFileLineInterator tarGzFileLineInterator = TarGzFileLineInterator
		.createTarGzFileLineInterator(TestNBestListFilesCreater
			.getTestNBestListTarGzFilePath());
	int countedLines = 0;
	while (tarGzFileLineInterator.hasNext()) {
	    String line = tarGzFileLineInterator.next();
	    System.out.println("line: " + line);
	    countedLines++;
	}
	Assert.assertEquals(6, countedLines);
    }

}
