package unit_tests;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import integration_tests.MTPipelineTest;

import org.junit.Test;

import util.ConfigFile;
import datapreparation.SegmenterConfig;
import datapreparation.StanfordSegmenterInterface;

public class TestTypeCommasInfluencesStanfordSegmenter {

    public static String MAIN_CONFIG_FILE_NAME = MTPipelineTest.MT_PIPELINE_TEST_CONFIG_FILE_GENERATION_CONFIG_FILE_NAME;
    private static String STANFORD_SEGMENTER_TEST_CONFIG_FILE_NAME = "StanfordsegmenterTestConfig.txt";
    private static String STANFORD_SEGMENTER_TEST_INPUT_FILE_NAME = "StanfordsegmenterTestInputFile.txt";
    private static String STANFORD_SEGMENTER_TEST_OUTPUT_FILE_NAME = "StanfordsegmenterTestInputFile.segmented.txt";

    private static String STANFORD_SEGMENTER_TEST_LINE1 = "仅在二十九、三十两日,孟买 降雨量达两百四十三公厘。";
    private static String STANFORD_SEGMENTER_TEST_LINE2 = "仅在二十九、三十两日、孟买 降雨量达两百四十三公厘。";
    private static String STANFORD_SEGMENTER_TEST_LINE3 = "仅在二十九,三十两日,孟买 降雨量达两百四十三公厘。";

    private static String configFileString(String key, String value) {
	return key + " = " + value + "\n";
    }

    private static void createTempStanfordSegmenterConfigFile() {
	try {
	    BufferedWriter outputWriter = new BufferedWriter(new FileWriter(
		    STANFORD_SEGMENTER_TEST_CONFIG_FILE_NAME));
	    ConfigFile configFile = new ConfigFile(MAIN_CONFIG_FILE_NAME);

	    String stanfordSegmenterLocation = configFile
		    .getValueWithPresenceCheck(SegmenterConfig.STANFORD_SEGMENTER_ROOT_DIR_LOCATION);
	    outputWriter
		    .write(configFileString(SegmenterConfig.STANFORD_SEGMENTER_ROOT_DIR_LOCATION,
			    stanfordSegmenterLocation));
	    outputWriter
		    .write(configFileString(SegmenterConfig.NUM_SEGMENTER_THREADS_PROPERTY, "1"));
	    outputWriter.write(configFileString(SegmenterConfig.SEGMENTER_INPUT_FILE_PATH_PROPERTY,
		    STANFORD_SEGMENTER_TEST_INPUT_FILE_NAME));
	    outputWriter.close();

	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private static void createTempStanfordSegmenterTestInputFile(String testLine) {
	try {
	    BufferedWriter outputWriter = new BufferedWriter(new FileWriter(
		    STANFORD_SEGMENTER_TEST_INPUT_FILE_NAME));
	    outputWriter.write(testLine);
	    outputWriter.close();

	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private static String readStanfordSegmenterProducedOutputFromFile() {
	try {
	    BufferedReader inputReader = new BufferedReader(new FileReader(
		    STANFORD_SEGMENTER_TEST_OUTPUT_FILE_NAME));
	    String resultLine = inputReader.readLine();
	    inputReader.close();
	    return resultLine;
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private static String getSegmenterResultForInputLine(String inputLine) {
	createTempStanfordSegmenterTestInputFile(inputLine);
	createTempStanfordSegmenterConfigFile();
	StanfordSegmenterInterface stanfordSegmenterInterface = StanfordSegmenterInterface
		.createStanfordSegmenterInterface(STANFORD_SEGMENTER_TEST_CONFIG_FILE_NAME);
	stanfordSegmenterInterface.createResultFilesMutltiThreaded();
	String resultLine = readStanfordSegmenterProducedOutputFromFile();
	return resultLine;
    }

    @Test
    public void test() {
	
	String resultLine = getSegmenterResultForInputLine(STANFORD_SEGMENTER_TEST_LINE1);
	String resultLine2 = getSegmenterResultForInputLine(STANFORD_SEGMENTER_TEST_LINE2);
	String resultLine3 = getSegmenterResultForInputLine(STANFORD_SEGMENTER_TEST_LINE3);
	
	System.out.println("Segmentation line: " + STANFORD_SEGMENTER_TEST_LINE1);
	System.out.println("result line1: " + resultLine);
	System.out.println("\n\nSegmentation line: " + STANFORD_SEGMENTER_TEST_LINE2);
	System.out.println("result line2: " + resultLine2);
	System.out.println("\n\nSegmentation line: " + STANFORD_SEGMENTER_TEST_LINE3);
	System.out.println("result line3: " + resultLine3);
	
	
    }
}
