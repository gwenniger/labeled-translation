package unit_tests;

import util.Span;
import junit.framework.Assert;

import mt_pipeline.preAndPostProcessing.ExtraBracketsRemover;

import org.junit.Test;

public class ExtraBracketsRemoverTest {
	
	  @Test(expected=AssertionError.class)
	public void testAssertionsEnabled() {
		assert (false);
	}

	@Test
	public void test() {
		Span span1 = new Span(0,10);
		Span span2 = new Span(1,10);
		Span span3 = new Span(5,11);
		Assert.assertTrue(span1.contains(span2));
		Assert.assertFalse(span1.contains(span3));
		Assert.assertTrue(ExtraBracketsRemover.containsOnlyBracketsAndWhiteSpaceCharacters("(( )"));
		Assert.assertFalse(ExtraBracketsRemover.containsOnlyBracketsAndWhiteSpaceCharacters("(()(b"));
		Assert.assertEquals("((a)(b))",ExtraBracketsRemover.removeExtraBrackets("(((a)(b)))"));
		Assert.assertEquals("((a)(b))",ExtraBracketsRemover.removeExtraBrackets("(  ((a)(b)))"));
		Assert.assertEquals("((a) (b))",ExtraBracketsRemover.removeExtraBrackets("(  ((a) (b)))"));
		Assert.assertEquals("((a)((b)c))",ExtraBracketsRemover.removeExtraBrackets("((a)((b)c))"));
	}

}
