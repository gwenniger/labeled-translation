package unit_tests;

import junit.framework.Assert;

import mt_pipeline.taggerInterface.TnTFormatToTagLineFormatConverter;

import org.junit.Test;

public class TnTFormatToTagLineFormatConverterTest {


	  @Test(expected=AssertionError.class)
	  public void testAssertionsEnabled() 
	  {
	    assert(false);
	  }
	
	
	@Test
	public void test() {
		assert(TnTFormatToTagLineFormatConverter.isCommentLine("%%"));
		Assert.assertFalse(TnTFormatToTagLineFormatConverter.isCommentLine("has"));
		Assert.assertFalse(TnTFormatToTagLineFormatConverter.isEmptyLine("has"));
	}

}
