package unit_tests;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import junit.framework.Assert;
import grammarExtraction.chiangGrammarExtraction.LabelSideSettings;
import grammarExtraction.chiangGrammarExtraction.ReorderLabelingSettings;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.labelSmoothing.EmptyAlternativelyLabeledRuleSetsListCreater;
import grammarExtraction.labelSmoothing.NoLabelSmoothingRulesSmoother;
import grammarExtraction.samtGrammarExtraction.ComplexGlueGrammarCreater;
import grammarExtraction.samtGrammarExtraction.GapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator;
import grammarExtraction.samtGrammarExtraction.LabelProbabilityEstimatorSAMT;
import grammarExtraction.samtGrammarExtraction.SAMTGrammarExtractionConstraints;
import grammarExtraction.translationRuleTrie.FeatureEnrichedGrammarRuleCreator;
import grammarExtraction.translationRuleTrie.LabelProbabilityEstimator;
import grammarExtraction.translationRuleTrie.StandardFeatureEnrichedGrammarRuleCreater;
import grammarExtraction.translationRuleTrie.TranslationRuleProbabilityTable;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

import org.junit.Test;

public class ComplexGlueGrammarCreaterTest {

    private static int NumLabels = 10;

    public static WordKeyMappingTable createTestWordKeyMappingTable() {
	WordKeyMappingTable result = WordKeyMappingTable.createWordKeyMappingTable(16);
	for (int i = 0; i < NumLabels; i++) {
	    result.getKeyForWord("[LABEL" + (i + 1) + "]");
	}

	return result;
    }

    public static boolean fileContainsNoEmptyLines(String fileName) {
	try {
	    BufferedReader inputReader = new BufferedReader(new FileReader(fileName));
	    String line;
	    while ((line = inputReader.readLine()) != null) {
		if (line.isEmpty()) {
		    inputReader.close();
		    return false;
		}
	    }
	    inputReader.close();
	    return true;
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private static boolean collectionContainsSubstring(List<String> collection, String subString) {
	for (String element : collection) {
	    if (element.contains(subString)) {
		return true;
	    }
	}
	return false;
    }

    private int noGlueRuleWeights(String glueRuleString) {
	String[] parts = glueRuleString.split(" \\|\\|\\| ");
	String weightsPart = parts[parts.length - 1].trim();
	System.out.println("weightsPart: " + weightsPart);
	String[] weights = weightsPart.split(" ");
	return weights.length;
    }

    @Test
    public void testComplexGlueGrammarCreater() {
	SystemIdentity systemIdentity = SystemIdentity.createSAMTSystemIdentyWithoutSmoothing();
	FeatureEnrichedGrammarRuleCreator featureEnrichedGrammarRuleCreator = StandardFeatureEnrichedGrammarRuleCreater
		.createStandardFeatureEnrichedGrammarRuleCreater(null, null, null);
	ComplexGlueGrammarCreater complexGlueGrammarCreater = ComplexGlueGrammarCreater
		.createComplexGlueGrammarCreater(false, systemIdentity);
	List<String> complexGlueRules = complexGlueGrammarCreater.createComplexGlueRules(
		createTestWordKeyMappingTable(),
		LabelProbabilityEstimatorSAMT.createLabelProbabilityEstimatorSAMT(false),
		featureEnrichedGrammarRuleCreator);

	for (String complexGlueRuleString : complexGlueRules) {
	    System.out.println("ComplexGlueRule: \"" + complexGlueRuleString + "\"");
	    Assert.assertEquals(16, noGlueRuleWeights(complexGlueRuleString));
	}

	System.out.println(complexGlueRules + "\n\n\n");
	Assert.assertTrue(collectionContainsSubstring(complexGlueRules,
		"[GOAL] ||| [GOAL,1] [LABEL1,2] ||| [GOAL,1] [LABEL1,2] |||"));
	Assert.assertTrue(collectionContainsSubstring(complexGlueRules,
		"[GOAL] ||| [GOAL,1] [LABEL" + NumLabels + ",2] ||| [GOAL,1] [LABEL" + NumLabels
			+ ",2] |||"));
	Assert.assertFalse(collectionContainsSubstring(complexGlueRules,
		"[GOAL] ||| [GOAL,1] [LABEL" + NumLabels + ",2] ||| [GOAL,1] [LABEL" + NumLabels
			+ ",2] ||| 0 0 0"));
	Assert.assertEquals(NumLabels * 1 + 2, complexGlueRules.size());
	complexGlueGrammarCreater.writeGlueGrammar(
		createTestTranslationRuleProbabilityTable(createTestWordKeyMappingTable(),
			LabelProbabilityEstimatorSAMT.createLabelProbabilityEstimatorSAMT(false),
			GapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator.createGapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator(),
			systemIdentity), "ComplexGlueGrammarCreaterTestOutput.txt",
		featureEnrichedGrammarRuleCreator);
	Assert.assertTrue(fileContainsNoEmptyLines("ComplexGlueGrammarCreaterTestOutput.txt"));
    }

    public static TranslationRuleProbabilityTable createTestTranslationRuleProbabilityTable(
	    WordKeyMappingTable wordKeyMappingTable,
	    LabelProbabilityEstimator labelProbabilityEstimator,
	    GapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator gapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator,
	    SystemIdentity systemIdentity) {

	return new TranslationRuleProbabilityTable(null, null, null, wordKeyMappingTable,
		labelProbabilityEstimator,
		gapLabelGivenLeftHandSideLabelConditionalProbabilityEstimator,
		NoLabelSmoothingRulesSmoother
			.createNoLabelSmoothingNoReorderingRulesSmoothingSmoother(
				wordKeyMappingTable, EmptyAlternativelyLabeledRuleSetsListCreater
					.createEmptyAlternativelyLabeledRuleSetsListCreater(),
				EmptyAlternativelyLabeledRuleSetsListCreater
					.createEmptyAlternativelyLabeledRuleSetsListCreater(),
				false),
		SAMTGrammarExtractionConstraints.createSAMTGrammarExtractionConstraints(
			systemIdentity.getMaxSourceAndTargetLength(), false, false, false, false,
			false, false, ReorderLabelingSettings.createNoReorderingLabelingSettings(),
			LabelSideSettings.createLabelSideSettingsBothSides(), null), systemIdentity);
    }

}
