package unit_tests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import ccgLabeling.CCGLabeler;
import coreContextLabeling.AmbiguityTupleComparator;
import coreContextLabeling.CoreContextLabel;
import coreContextLabeling.CoreContextLabeler;
import coreContextLabeling.CoreContextLabelsTable;
import coreContextLabeling.EbitgChartBuilderCoreContextLabeled;
import coreContextLabeling.EbitgLexicalizedInferenceCoreContextLabeled;
import coreContextLabeling.EbitgTreeGrowerCoreContextLabeled;
import coreContextLabeling.EbitgTreeStateCoreContextLabeled;
import util.FileUtil;
import util.Serialization;
import artificalCorpusCreation.ArtificialCoreContextCorpusCreater;
import extended_bitg.EbitgChart;
import extended_bitg.EbitgChartBuilderProperties;
import extended_bitg.EbitgChartEntry;
import extended_bitg.EbitgTreeGrower;
import util.Span;
import grammarExtraction.grammarExtractionFoundation.LabelingSettings.CanonicalFormSettings;
import grammarExtraction.minimalPhrasePairExtraction.MultiThreadGrammarExtractorMinimalPhrasePairs;
import grammarExtraction.translationRuleTrie.LexicalWeightCountItem;
import grammarExtraction.translationRuleTrie.MTRuleTrieNodeRoot;
import grammarExtraction.translationRuleTrie.TranslationRuleProbabilityTable;

public class CoreContextLabeledChartTester {

	//@Test(expected = AssertionError.class)
	public void testAssertionsEnabled() {
		assert (false);
	}

	private void createToyCarTestCorpus(String testCorpusFolderName) {

		System.out.println("createCarTestCorpus");
		FileUtil.createFolderIfNotExisting(testCorpusFolderName);
		try {
			ArtificialCoreContextCorpusCreater.createToyCoreContextArtificialCorpusCreator(testCorpusFolderName);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private TranslationRuleProbabilityTable createTranslationProbabilityTable(String testCorpusFolderName, String serializationFileName) {
		String configFilePath = testCorpusFolderName + "configFile.txt";
		int numThreads = 4;
		String resultFilePath = testCorpusFolderName + ArtificialCoreContextCorpusCreater.MINIMAL_PHRASE_PAIR_GRAMMAR_NAME;
		TranslationRuleProbabilityTable translationRuleProbabilityTable = MultiThreadGrammarExtractorMinimalPhrasePairs.extractGrammarMultiThread(configFilePath, numThreads, resultFilePath);
		return translationRuleProbabilityTable;
	}

	private EbitgChartBuilderCoreContextLabeled setupCoreContextLabeledToyChart() {
		String sourceString = "the man walks to his car";
		String targetString = "de man loopt naar zijn auto";
		String alignmentString = "0-0 1-1 2-2 3-3 4-4 5-5";

		String testCorpusFolderName = "./testCorpusToyCar/";
		String serializationFileName = ArtificialCoreContextCorpusCreater.getCoreContextTableSerializationFilePath(testCorpusFolderName);
		createToyCarTestCorpus(testCorpusFolderName);
		createTranslationProbabilityTable(testCorpusFolderName, serializationFileName);
		CoreContextLabelsTable coreContextLabelsTable = Serialization.unSerializeOBject(serializationFileName);// CoreContextLabelsTable.createCoreContextLabelsTableUsingSourceSideProbability(translationRuleProbabilityTableDeSerialized,16,1);
		EbitgChartBuilderCoreContextLabeled chartBuilder = EbitgChartBuilderCoreContextLabeled.createEbitgChartBuilderCoreContextLabeled(sourceString, targetString, alignmentString,
				EbitgChartBuilderProperties.MaxAllowedInferencesPerNode, EbitgChartBuilderProperties.USE_GREEDY_NULL_BINDING, EbitgChartBuilderProperties.USE_CACHING, coreContextLabelsTable,
				CCGLabeler.createEmptyCCGLabeler());
		chartBuilder.findDerivationsAlignment();
		return chartBuilder;
	}

	public static List<Span> getAllSubSpans(Span span) {
		List<Span> result = new ArrayList<Span>();
		for (int spanLength = 1; spanLength < span.getSpanLength(); spanLength++) {
			for (int spanBegin = span.getFirst(); spanBegin < span.getSpanLength() - spanLength + 1; spanBegin++) {
				int i = spanBegin;
				int j = spanBegin + spanLength - 1;
				result.add(new Span(i, j));
			}
		}
		return result;
	}

	private static List<EbitgChartEntry> getAllSubSpanEntries(EbitgChart chart, EbitgChartEntry chartEntry) {
		List<EbitgChartEntry> result = new ArrayList<EbitgChartEntry>();

		for (Span span : getAllSubSpans(new Span(chartEntry.getI(), chartEntry.getJ()))) {
			result.add(chart.getChartEntry(span.getFirst(), span.getSecond()));
		}
		return result;
	}

	public static CoreContextLabel getMaxUncertaintyCoreContextLabelFromList(List<CoreContextLabel> coreContextLabels) {
		if (!coreContextLabels.isEmpty()) {
			List<CoreContextLabel> coreContextLabelsSoretedByAmbiguity = new ArrayList<CoreContextLabel>(coreContextLabels);
			Collections.sort(coreContextLabelsSoretedByAmbiguity, new AmbiguityTupleComparator());
			return coreContextLabelsSoretedByAmbiguity.get(0);
		}
		return null;
	}

	private CoreContextLabel getMaxUncertaintyCoreContextLabelAmongChartEntries(List<EbitgChartEntry> chartEntries, CoreContextLabeler coreContextLabeler) {

		List<CoreContextLabel> candidateLabels = new ArrayList<CoreContextLabel>();
		for (EbitgChartEntry chartEntry : chartEntries) {
			CoreContextLabel candidateLabel = coreContextLabeler.getCoreContextLabelForChartEntryFromTable(chartEntry);
			candidateLabels.add(candidateLabel);
		}
		System.out.println("candidateLabels: " + candidateLabels);
		return getMaxUncertaintyCoreContextLabelFromList(candidateLabels);
	}

	// @Test
	public void testCoreContextLabeler(EbitgChartBuilderCoreContextLabeled chartBuilder, CoreContextLabeler coreContextLabeler) {
		EbitgChart chart = chartBuilder.getChart();
		boolean existLabeledChartEntries = false;

		for (int spanLength = 2; spanLength <= chart.getSourceLength(); spanLength++) {
			for (int spanBegin = 0; spanBegin < chart.getSourceLength() - spanLength + 1; spanBegin++) {
				int i = spanBegin;
				int j = spanBegin + spanLength - 1;

				EbitgChartEntry chartEntry = chart.getChartEntry(i, j);
				CoreContextLabel chartEntryLabel = coreContextLabeler.getCoreContextLabelForChartEntryFromTable(chartEntry);
				CoreContextLabel maxUncertaintyLabelSubEntries = getMaxUncertaintyCoreContextLabelAmongChartEntries(getAllSubSpanEntries(chart, chartEntry), coreContextLabeler);

				System.out.println("Chart entry label: " + chartEntryLabel);
				System.out.println("maxUncertaintyLabelSubEntries: " + maxUncertaintyLabelSubEntries);

				if ((chartEntryLabel != null) && (maxUncertaintyLabelSubEntries != null)) {
					existLabeledChartEntries = true;
					Assert.assertTrue(chartEntryLabel.getAmbiguityScore() >= maxUncertaintyLabelSubEntries.getAmbiguityScore());
				}
			}
		}
		Assert.assertTrue(existLabeledChartEntries);

	}

	// @Test
	public void testTrieNodeRootSerialization() {
		String testCorpusFolderName = "./testCorpusToyCar/";
		String serializationFileName = testCorpusFolderName + "trieNodeRoot.serialized";
		createToyCarTestCorpus(testCorpusFolderName);
		TranslationRuleProbabilityTable translationRuleProbabilityTable = createTranslationProbabilityTable(testCorpusFolderName, serializationFileName);

		MTRuleTrieNodeRoot trieNodeRoot = MTRuleTrieNodeRoot.createMTRuleTrieNodeRoot(translationRuleProbabilityTable.getWordKeyMappingTable(), new LexicalWeightCountItem(),CanonicalFormSettings.createCanonicalFormSettingsNonCanonicalFormUsed());
		assert (trieNodeRoot instanceof MTRuleTrieNodeRoot);
		Serialization.serializeOBject(trieNodeRoot, serializationFileName);

		MTRuleTrieNodeRoot trieNodeRootUnserialized = Serialization.unSerializeOBject(serializationFileName);
		System.out.println("Trie Node root original: " + trieNodeRoot);
		System.out.println("Trie Node root un-serialized: " + trieNodeRootUnserialized);
	}

	@Test
	public void testCoreContextLabeler() {
		EbitgChartBuilderCoreContextLabeled chartBuilder = setupCoreContextLabeledToyChart();
		CoreContextLabeler coreContextLabeler = chartBuilder.getCoreContextLabeler();
		testCoreContextLabeler(chartBuilder, coreContextLabeler);
		System.out.println("Tested core context labeler succesfull");
	}

	//@Test
	public void testCoreEbitgTreeGrowerCoreContextLabeled() {
		EbitgChartBuilderCoreContextLabeled chartBuilder = setupCoreContextLabeledToyChart();
		EbitgTreeGrower<EbitgLexicalizedInferenceCoreContextLabeled, EbitgTreeStateCoreContextLabeled> treeGrower = EbitgTreeGrowerCoreContextLabeled.createEbitgTreeGrowerCoreContextLabeled(chartBuilder);
		System.out.println("TreeString: " + treeGrower.getTreeString());

	}
}
