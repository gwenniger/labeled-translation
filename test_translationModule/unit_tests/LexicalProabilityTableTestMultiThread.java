package unit_tests;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import junit.framework.Assert;
import grammarExtraction.translationRuleTrie.ConcurrentLexicalProbabilityTable;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

import org.junit.Test;
import alignment.Alignment;
import alignment.AlignmentTriple;

public class LexicalProabilityTableTestMultiThread implements Callable<LexicalProabilityTableTestMultiThread>
{
	private ConcurrentLexicalProbabilityTable lexicalProbabilityTable;
	
	public LexicalProabilityTableTestMultiThread()
	{
		this.lexicalProbabilityTable = null;
	}
	
	public void setLexicalProabilityTable(ConcurrentLexicalProbabilityTable lexicalProbabilityTable)
	{
		this.lexicalProbabilityTable = lexicalProbabilityTable;
	}
	
	private static List<AlignmentTriple> createSimpleAlignmentTriples()
	{
		List<AlignmentTriple> result = new ArrayList<AlignmentTriple>();
		result.add(AlignmentTriple.createAlignmentTriple("s1 s2 s3", "t1 t2 t3", "0-0 1-1 2-2"));
		result.add(AlignmentTriple.createAlignmentTriple("s1 s2 s3 s4", "t4 t5 t6 t7", "0-0 1-1 2-2 3-3"));
		result.add(AlignmentTriple.createAlignmentTriple("sA sB", "tA tB", "0-0 0-1 1-1"));
		return result;
	}
	
	
	
	private static ConcurrentLexicalProbabilityTable createSimpleEmptyConcurrentLexicalProbabilityTable()
	{
		List<AlignmentTriple> alignmentTriples = createSimpleAlignmentTriples();
		WordKeyMappingTable wordKeyMappingTable = WordKeyMappingTable.createWordKeyMappingTableFromAlignmentTriples(alignmentTriples, 2,16); 
		ConcurrentLexicalProbabilityTable result = ConcurrentLexicalProbabilityTable.createConcurrentLexicalProbabilityTable(wordKeyMappingTable,16);
		return result;
	}
	
	 @Test(expected=AssertionError.class)
	  public void testAssertionsEnabled() 
	  {
	    assert(false);
	  }
	
	 
	private List<LexicalProabilityTableTestMultiThread> performThreadComputation(List<LexicalProabilityTableTestMultiThread> threads, int noThreads)
	{
		List<Future<LexicalProabilityTableTestMultiThread>> registeredTasks = new ArrayList<Future<LexicalProabilityTableTestMultiThread>>();
		ExecutorService executorService = Executors.newFixedThreadPool(noThreads); 
		
		 for(LexicalProabilityTableTestMultiThread tret :  threads)
		 {	 
			 Future<LexicalProabilityTableTestMultiThread> executingTask = executorService.submit(tret);
			 registeredTasks.add(executingTask);
		 }
		 // Wait until all extractor threads to finish
		 List<LexicalProabilityTableTestMultiThread> results = this.joinThreads(registeredTasks);
		 System.out.println("\n\n Threads finished \n\n");
		
		return results;
	}
	 
	public List<LexicalProabilityTableTestMultiThread> joinThreads(List<Future<LexicalProabilityTableTestMultiThread>> executingTasks)
	{
		
		List<LexicalProabilityTableTestMultiThread> results = new ArrayList<LexicalProabilityTableTestMultiThread>();
		 // Wait until all threads finish
		 for(Future<LexicalProabilityTableTestMultiThread> waitForCompletion : executingTasks)
		 {
			 try 
			 {
				 LexicalProabilityTableTestMultiThread aResult = waitForCompletion.get();
				 results.add(aResult);
				 System.out.println("Thread finished");
				 //FIXME need custom executor that can generate custom wrappers of futures
				 //System.out.println("Thread " + thread.getId() + " finished");
			 } catch (InterruptedException e) 
			 {
				 e.printStackTrace();
			 } catch (ExecutionException e) {
				e.printStackTrace();
			}
		 }
		 return results;
		 
	}
	 
	@Test
	public void testWordProbabilitiesMultiThread() 
	{
		ConcurrentLexicalProbabilityTable lexicalProbabilityTable = createSimpleEmptyConcurrentLexicalProbabilityTable();
		List<LexicalProabilityTableTestMultiThread> tasks = new ArrayList<LexicalProabilityTableTestMultiThread>();
		
		int noThreads = 4;
		int noTasks = 100;
		for(int i = 0; i < noTasks; i++)
		{
			LexicalProabilityTableTestMultiThread lptmt = new LexicalProabilityTableTestMultiThread();
			lptmt.setLexicalProabilityTable(lexicalProbabilityTable);
			tasks.add(lptmt);
		}
		
		assert(tasks.size() == noTasks);
		
		performThreadComputation(tasks, noThreads);
		
		lexicalProbabilityTable.computeProbabilitiesFromCounts();
		
		Assert.assertEquals(0.5, lexicalProbabilityTable.getProbability("sA","tA"));
		Assert.assertEquals(0.5, lexicalProbabilityTable.getProbability("sA","tB"));
		Assert.assertEquals(1.0, lexicalProbabilityTable.getProbability("sB","tB"));
		Assert.assertEquals(noTasks,lexicalProbabilityTable.getCount("sB","tB"));
		
		
		List<String> sourceSentence = new ArrayList<String>(Arrays.asList(new String[]{"sA","sB"}));
		List<String> targetSentence = new ArrayList<String>(Arrays.asList(new String[]{"tA","tB"}));
		Alignment alignment = Alignment.createAlignment("0-0 1-1");
		
		Assert.assertEquals(0.5,lexicalProbabilityTable.computeAlignedPhrasePairProbability(sourceSentence, targetSentence, alignment));
	}



	@Override
	public LexicalProabilityTableTestMultiThread call() throws Exception 
	{
		List<AlignmentTriple> alignmentTriples = createSimpleAlignmentTriples();
		this.lexicalProbabilityTable.addAlignmentTriples(alignmentTriples);
		return this;
	}


	
}