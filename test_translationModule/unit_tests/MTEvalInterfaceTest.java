package unit_tests;

import integration_tests.MTPipelineTest;

import java.io.FileNotFoundException;
import java.io.IOException;

import junit.framework.Assert;


import mt_pipeline.evaluation.EvaluationFilePathsTriple;
import mt_pipeline.evaluation.EvaluationScoresQuadruple;
import mt_pipeline.evaluation.EvaluationTestFilesCreater;
import mt_pipeline.evaluation.MTEvalInterface;
import mt_pipeline.mtPipelineTest.MTPipelineTestConfigFileCreater;

import org.junit.Test;

import util.ConfigFile;

public class MTEvalInterfaceTest {

	private static final String TEST_REF_NAME = "MTEvalInterfaceTest.ref.txt";
	private static final String TEST_SRC_NAME = "MTEvalInterfaceTest.src.txt";
	private static final String TEST_TST_NAME = "MTEvalInterfaceTest.tst.txt";
	private static final String OUTPUT_SCORE_FOLE_PATH = "MTEvalInterfaceTest.testResult.txt";
	private static final int SENTENCE_LENGTH = 10;
	private static final int TEST_SIZE = 100;

	public static String getMTEvalDir() {
		try {
			ConfigFile theConfig = new ConfigFile(MTPipelineTest.MT_PIPELINE_TEST_CONFIG_FILE_GENERATION_CONFIG_FILE_NAME);
			return MTPipelineTestConfigFileCreater.getScriptsDir(theConfig);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public void assertBleuScoreIsOne() {
		try {
			Assert.assertEquals(1.0, EvaluationScoresQuadruple.getBleuFromResultFile(OUTPUT_SCORE_FOLE_PATH));
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	public void assertBleuScoreLessThanOne() {
		try {
			Assert.assertTrue(EvaluationScoresQuadruple.getBleuFromResultFile(OUTPUT_SCORE_FOLE_PATH) < 1);
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@Test
	public void testEvenSentencesCorrect() {
		EvaluationFilePathsTriple evaluationFilePathsTriple = EvaluationFilePathsTriple.createEvaluationFilePathsTriple(TEST_SRC_NAME, TEST_TST_NAME,
				TEST_REF_NAME);
		EvaluationTestFilesCreater evaluationTestFilesCreater = EvaluationTestFilesCreater.createEvaluationTestFilesCreaterCorrectEvenSentences(
				evaluationFilePathsTriple, SENTENCE_LENGTH, TEST_SIZE);
		evaluationTestFilesCreater.writeFiles();
		MTEvalInterface mtEvalInterface = MTEvalInterface.createMtEvalInterfaceForPlainFiles(evaluationFilePathsTriple.getSourceFilePath(),
				evaluationFilePathsTriple.getResultFilePath(), evaluationFilePathsTriple.getReferenceFilePath(), getMTEvalDir(), OUTPUT_SCORE_FOLE_PATH);
		mtEvalInterface.computeBleuAndNist();
		assertBleuScoreLessThanOne();

	}

	@Test
	public void testAllSentencesCorrect() {
		EvaluationFilePathsTriple evaluationFilePathsTriple = EvaluationFilePathsTriple.createEvaluationFilePathsTriple(TEST_SRC_NAME, TEST_TST_NAME,
				TEST_REF_NAME);
		EvaluationTestFilesCreater evaluationTestFilesCreater = EvaluationTestFilesCreater.createEvaluationTestFilesCreaterAllCorrectSentences(
				evaluationFilePathsTriple, SENTENCE_LENGTH, TEST_SIZE);
		evaluationTestFilesCreater.writeFiles();
		MTEvalInterface mtEvalInterface = MTEvalInterface.createMtEvalInterfaceForPlainFiles(evaluationFilePathsTriple.getSourceFilePath(),
				evaluationFilePathsTriple.getResultFilePath(), evaluationFilePathsTriple.getReferenceFilePath(), getMTEvalDir(), OUTPUT_SCORE_FOLE_PATH);
		mtEvalInterface.computeBleuAndNist();
		assertBleuScoreIsOne();
	}

}
