package unit_tests;

import java.util.List;
import junit.framework.Assert;
import org.junit.Test;
import extended_bitg.EbitgChartBuilderProperties;
import extended_bitg.EbitgLexicalizedInference;
import grammarExtraction.grammarExtractionFoundation.HeavyWeightPhrasePairRuleSet;
import grammarExtraction.minimalPhrasePairExtraction.HeavyWeightMinimalPhrasePairRuleExtractor;

public class MinimalPhrasePairRuleExtractorTest 
{
	  @Test(expected=AssertionError.class)
	  public void testAssertionsEnabled() 
	  {
	    assert(false);
	  }
	
	
   private int getBaseRuleTargetSpanLength(HeavyWeightPhrasePairRuleSet<EbitgLexicalizedInference> minimalRuleSet)
   {
	   return minimalRuleSet.getRules().get(0).getTargetLength();
   }
   
   private int getBaseRuleSourceSpanLength(HeavyWeightPhrasePairRuleSet<EbitgLexicalizedInference> minimalRuleSet)
   {
	   return minimalRuleSet.getRules().get(0).getSourceLength();
   }
	
   private int getNumberOfRulesInSet(HeavyWeightPhrasePairRuleSet<EbitgLexicalizedInference> minimalRuleSet)
   {
	   return minimalRuleSet.getRules().size();
   }
   
	@Test
	public void testMinimalPhrasePairRuleExtractor() 
	{
		String sourceString = "the main walks to his car";
        String targetString = "de man loopt naar zijn auto";
        String alignmentString = "0-0 1-1 2-2 3-3 4-4 5-5";
        
        HeavyWeightMinimalPhrasePairRuleExtractor<EbitgLexicalizedInference,?> minimalPhrasePairRuleExtracter = HeavyWeightMinimalPhrasePairRuleExtractor.createHeavyWeightMinimalPhrasePairRuleExtractor(sourceString, targetString, alignmentString, EbitgChartBuilderProperties.MaxAllowedInferencesPerNode,EbitgChartBuilderProperties.USE_CACHING);
        List<HeavyWeightPhrasePairRuleSet<EbitgLexicalizedInference>> heavyWightMinimalRules = minimalPhrasePairRuleExtracter.createHeavyWeightMinimalTranslationRules();
        
        int noRules = 0;
        for(HeavyWeightPhrasePairRuleSet<EbitgLexicalizedInference> minimalRuleSet : heavyWightMinimalRules)
        {
        	Assert.assertEquals(1,getNumberOfRulesInSet(minimalRuleSet));
        	Assert.assertEquals(1,getBaseRuleSourceSpanLength(minimalRuleSet));
        	Assert.assertEquals(1,getBaseRuleTargetSpanLength(minimalRuleSet));
        	noRules++;
        }
        
        Assert.assertEquals(6,noRules);
	}	
	
	@Test
	public void testMinimalPhrasePairRuleExtractor2() 
	{
		String sourceString = "the criminal is taken to the police station";
        String targetString = "de boef wordt naar het politiebureau gebracht";
        String alignmentString = "0-0 1-1 2-2 3-6 4-3 5-4 6-5 7-5";
        
        HeavyWeightMinimalPhrasePairRuleExtractor<EbitgLexicalizedInference,?> minimalPhrasePairRuleExtracter = HeavyWeightMinimalPhrasePairRuleExtractor.createHeavyWeightMinimalPhrasePairRuleExtractor(sourceString, targetString, alignmentString, EbitgChartBuilderProperties.MaxAllowedInferencesPerNode,EbitgChartBuilderProperties.USE_CACHING);
        List<HeavyWeightPhrasePairRuleSet<EbitgLexicalizedInference>> heavyWightMinimalRules = minimalPhrasePairRuleExtracter.createHeavyWeightMinimalTranslationRules();
        
        int noRules = 0;
        for(HeavyWeightPhrasePairRuleSet<EbitgLexicalizedInference> minimalRuleSet : heavyWightMinimalRules)
        {
        	Assert.assertEquals(1, getNumberOfRulesInSet(minimalRuleSet));
        	assert(getBaseRuleSourceSpanLength(minimalRuleSet) <= 2);
        	Assert.assertEquals(1,getBaseRuleTargetSpanLength(minimalRuleSet));
        	noRules++;
        }
        
        Assert.assertEquals(7,noRules);
	}	
	
	@Test
	public void testMinimalPhrasePairRuleExtractor3() 
	{
		String sourceString = "s0 s1 s2 s3";
        String targetString = "t0 t1 t2 t3";
        String alignmentString = "0-0 0-1 1-0 1-1 2-2 2-3 3-2 3-3";

        HeavyWeightMinimalPhrasePairRuleExtractor<EbitgLexicalizedInference,?> minimalPhrasePairRuleExtracter = HeavyWeightMinimalPhrasePairRuleExtractor.createHeavyWeightMinimalPhrasePairRuleExtractor(sourceString, targetString, alignmentString, EbitgChartBuilderProperties.MaxAllowedInferencesPerNode,EbitgChartBuilderProperties.USE_CACHING);
        List<HeavyWeightPhrasePairRuleSet<EbitgLexicalizedInference>> heavyWightMinimalRules = minimalPhrasePairRuleExtracter.createHeavyWeightMinimalTranslationRules();
        
        int noRules = 0;
        for(HeavyWeightPhrasePairRuleSet<EbitgLexicalizedInference> minimalRuleSet : heavyWightMinimalRules)
        {
        	Assert.assertEquals(1, getNumberOfRulesInSet(minimalRuleSet));
        	Assert.assertEquals(2, getBaseRuleSourceSpanLength(minimalRuleSet));
        	Assert.assertEquals(2,getBaseRuleTargetSpanLength(minimalRuleSet));
        	noRules++;
        }
        
        Assert.assertEquals(2,noRules);
	}	

	
	@Test
	public void testMinimalPhrasePairRuleExtractor5() 
	{
		String sourceString = "s0 s1 s2 s3 s4";
        String targetString = "t0 t1 t2 t3 t4";
        String alignmentString = "0-0 2-2 4-4";

        HeavyWeightMinimalPhrasePairRuleExtractor<EbitgLexicalizedInference,?> minimalPhrasePairRuleExtracter = HeavyWeightMinimalPhrasePairRuleExtractor.createHeavyWeightMinimalPhrasePairRuleExtractor(sourceString, targetString, alignmentString, EbitgChartBuilderProperties.MaxAllowedInferencesPerNode,EbitgChartBuilderProperties.USE_CACHING);
        List<HeavyWeightPhrasePairRuleSet<EbitgLexicalizedInference>> heavyWightMinimalRules = minimalPhrasePairRuleExtracter.createHeavyWeightMinimalTranslationRules();
        
        int noRules = 0;
        for(HeavyWeightPhrasePairRuleSet<EbitgLexicalizedInference> minimalRuleSet : heavyWightMinimalRules)
        {
        	Assert.assertEquals(1, getNumberOfRulesInSet(minimalRuleSet));
        	Assert.assertEquals(1, getBaseRuleSourceSpanLength(minimalRuleSet));
        	Assert.assertEquals(1,getBaseRuleTargetSpanLength(minimalRuleSet));
        	noRules++;
        }
        
        Assert.assertEquals(3,noRules);
	}	

	
}
