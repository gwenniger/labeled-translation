package unit_tests;

import java.util.List;
import junit.framework.Assert;
import util.Span;
import grammarExtraction.chiangGrammarExtraction.ChiangGrammarExtractionConstraints;
import grammarExtraction.chiangGrammarExtraction.GapRangeComputing;
import org.junit.Test;

import util.Pair;

public class GapRangeComputingTest {

	private static final int MAX_SENTENCE_LENGTH = 40;

	private static GapRangeComputing createTestGapRangeComputing() {
		ChiangGrammarExtractionConstraints chiangGrammarExtractionConstraints = ChiangGrammarExtractionConstraints.createStandardChiangGrammarExtractionConstraints(false);
		return GapRangeComputing.createGapRangeComputing(chiangGrammarExtractionConstraints);
	}

	private void secondSpanBeginsExactlyAfterFirstSpan(Pair<Span> partition) {
		// System.out.println("Partition: " + partition);
		int firstSpanLastIndex = partition.getFirst().getSecond();
		int secondSpanFirstIndex = partition.getSecond().getFirst();
		Assert.assertEquals(firstSpanLastIndex + 1, secondSpanFirstIndex);
	}
	
	private void checkNoPartitionsEqualsSpanLengthMinusOne(Span testSpan,List<Pair<Span>> twoSpanPartionsList) {
		int testSpanLenthMinusOne = testSpan.getSpanLength() - 1;
		int noPartitions = twoSpanPartionsList.size();
		Assert.assertEquals(testSpanLenthMinusOne, noPartitions);
	}
	
	private void checkAllSecondSpansBeginExactlyAfterFirstSpan(List<Pair<Span>> twoSpanPartionsList) {
		for (Pair<Span> partition : twoSpanPartionsList) {
			secondSpanBeginsExactlyAfterFirstSpan(partition);
		}
	}

	@Test
	public void testFindFullyAbstractRulesPermissibleSplitRanges() {
		GapRangeComputing testGapRangeComputing = createTestGapRangeComputing();

		for (int sourceSpanBegin = 0; sourceSpanBegin < MAX_SENTENCE_LENGTH; sourceSpanBegin++) {
			for (int sourceSpanEnd = sourceSpanBegin; sourceSpanEnd < sourceSpanBegin + MAX_SENTENCE_LENGTH; sourceSpanEnd++) {
				Span testSpan = new Span(sourceSpanBegin, sourceSpanEnd);
				List<Pair<Span>> twoSpanPartionsList = testGapRangeComputing.findFullyAbstractRulesPermissibleSplitRanges(testSpan);
			
				checkNoPartitionsEqualsSpanLengthMinusOne(testSpan,twoSpanPartionsList);
				checkAllSecondSpansBeginExactlyAfterFirstSpan(twoSpanPartionsList);

			}
		}
	}

}
