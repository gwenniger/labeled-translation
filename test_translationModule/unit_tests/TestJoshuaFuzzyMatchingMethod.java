package unit_tests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

public class TestJoshuaFuzzyMatchingMethod {

    private static final <T> List<List<T>> unpackAllPossibleTCombinations(List<List<T>> listOfLists) {
	List<List<T>> result = new ArrayList<List<T>>();

	// We need to initially add one empty list to the result for iterative
	// extending
	result.add(new ArrayList<T>());

	if (!(listOfLists instanceof List)) {
	    throw new RuntimeException("Gideon: Not a list!");
	}

	for (int i = 0; i < listOfLists.size(); i++) {

	    List<T> superNodeListI = listOfLists.get(i);

	    // We make a temporary copy of the list to enable extending it
	    // without changing the list we are reading from
	    List<List<T>> extendedResult = new ArrayList<List<T>>();

	    for (T listITChoice : superNodeListI) {

		for (List<T> partialList : result) {
		    List<T> extendedPartialList = new ArrayList<T>(partialList);
		    extendedPartialList.add(listITChoice);
		    extendedResult.add(extendedPartialList);
		}
	    }
	    result = extendedResult;
	    System.out.println("result: " + result);
	}
	return result;
    }

    @Test
    public void test() {
	List<String> list1 = Arrays.asList("a", "b", "c");
	List<String> list2 = Arrays.asList("1", "2");
	List<String> list3 = Arrays.asList("red", "green", "blue", "orange");
	List<List<String>> combinationsListPacked = Arrays.asList(list1, list2, list3);

	List<List<String>> result = unpackAllPossibleTCombinations(combinationsListPacked);
	for (List<String> unpackedList : result) {
	    System.out.println("unpacked list: " + unpackedList);
	}
	int expectedUnpackedResultSize = 3 * 2 * 4;
	Assert.assertEquals(expectedUnpackedResultSize, result.size());

    }

}
