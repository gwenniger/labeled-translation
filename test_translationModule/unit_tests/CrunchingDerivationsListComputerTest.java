package unit_tests;

import grammarExtraction.grammarExtractionFoundation.JoshuaStyle;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import junit.framework.Assert;
import mt_pipeline.evaluation.SentencesNBestCandidatesListIterator;
import mt_pipeline.mbrInterface.CrunchingDerivationsListComputer;

import org.junit.Test;

import util.FileUtil;

public class CrunchingDerivationsListComputerTest {

	private static String CRUNCHING_TEST_INPUT_FILE = "crunchingTestInputFile.txt";
	private static String CRUNCHING_TEST_OUTPUT_FILE = "crunchingTestOutputFile.txt";

	private static void writeInputFile() {

		BufferedWriter outputWriter = null;
		try {
			outputWriter = new BufferedWriter(new FileWriter(CRUNCHING_TEST_INPUT_FILE));
			outputWriter.write("0 ||| first sentence ||| -1 ||| -1" + "\n");
			outputWriter.write("0 ||| first sentence ||| -1 ||| -1" + "\n");
			outputWriter.write("0 ||| first sentence ||| -1 ||| -1" + "\n");
			outputWriter.write("0 ||| second sentence ||| -1 ||| -1" + "\n");
			outputWriter.write("0 ||| second sentence ||| -1 ||| -1" + "\n");
			outputWriter.write("0 ||| second sentence ||| -1 ||| -1" + "\n");
			outputWriter.write("0 ||| second sentence ||| -1 ||| -1" + "\n");
			outputWriter.write("0 ||| second sentence ||| -1 ||| -1" + "\n");
			outputWriter.write("1 ||| last sentence ||| " + Math.log10(0.30) + " ||| " + Math.log10(0.30) + "\n");
			outputWriter.write("1 ||| last sentence ||| -1 ||| -1" + "\n");
			outputWriter.write("1 ||| last sentence ||| -1 ||| -1" + "\n");
			outputWriter.write("1 ||| first sentence ||| -1 ||| -1" + "\n");
			outputWriter.write("1 ||| first sentence ||| -1 ||| -1");
			outputWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void computeResultFile() {

		BufferedWriter outputWriter = null;
		SentencesNBestCandidatesListIterator sentencesNBestCandidatesListIterator;
		try {
			sentencesNBestCandidatesListIterator = SentencesNBestCandidatesListIterator.createSentencesNBestCandidatesListIterator(CRUNCHING_TEST_INPUT_FILE);
			outputWriter = new BufferedWriter(new FileWriter(CRUNCHING_TEST_OUTPUT_FILE));
			CrunchingDerivationsListComputer crunchingDerivationsListComputer = CrunchingDerivationsListComputer.createCrunchingDerivationsListComputer(sentencesNBestCandidatesListIterator, outputWriter);
			crunchingDerivationsListComputer.writeMergedDerivationList();

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			FileUtil.closeCloseableIfNotNull(outputWriter);
		}

	}
	
	private static void testResultFile() {

		BufferedReader inputReader = null;
		try {
			inputReader = new BufferedReader(new FileReader(CRUNCHING_TEST_OUTPUT_FILE));
			
			String inputString = null;
			while ((inputString = inputReader.readLine()) != null) {
				System.out.println("inputString: " + inputString);
				String[] parts = JoshuaStyle.spitOnRuleSeparator(inputString);
				Double totalWeights = Double.parseDouble(parts[3]);
				Double totalLogProb = Math.log10(0.50);
				
				Assert.assertEquals(totalLogProb,totalWeights);
				double probability = Math.pow(10, totalWeights);
				System.out.println("probability: " + probability);
				
			}
			

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			FileUtil.closeCloseableIfNotNull(inputReader);
		}

	}

	@Test
	public void test() {
		writeInputFile();
		computeResultFile();
		testResultFile();

	}

}
