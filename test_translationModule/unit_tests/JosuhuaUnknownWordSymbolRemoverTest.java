package unit_tests;

import junit.framework.Assert;

import mt_pipeline.mbrInterface.JosuhuaUnknownWordSymbolRemover;

import org.junit.Test;

public class JosuhuaUnknownWordSymbolRemoverTest {

	@Test
	public void testCreateNBestStringWithoutUnknownWordSymbols() {
		String inputString = "0 ||| elle_OOV is très_OOV charmante_OOV ||| -304.632 -300.000 -0.000 -0.000 -0.000 -0.862 -5.000 -4.000 -0.000 -5.000 -1.000 -4.000 -0.000 -0.000 -5.000 -1.000 -1.000 -1.737 ||| -463.121";
		String resultString = JosuhuaUnknownWordSymbolRemover.createNBestStringWithoutUnknownWordSymbols(inputString);
		String desiredResultString = "0 ||| is ||| -304.632 -300.000 -0.000 -0.000 -0.000 -0.862 -5.000 -4.000 -0.000 -5.000 -1.000 -4.000 -0.000 -0.000 -5.000 -1.000 -1.000 -1.737 ||| -463.121"; 
		System.out.println("resultString: " + resultString);
		Assert.assertEquals(desiredResultString, resultString);
	}

}
