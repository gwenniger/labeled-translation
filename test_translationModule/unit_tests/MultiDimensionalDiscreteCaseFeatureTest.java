package unit_tests;

import java.util.Arrays;
import junit.framework.Assert;
import grammarExtraction.reorderingLabeling.MultiDimensionalDiscreteCaseFeature;

import org.junit.Test;

public class MultiDimensionalDiscreteCaseFeatureTest {

	@Test
	public void testGetIntegerValueCase() {
		MultiDimensionalDiscreteCaseFeature m = MultiDimensionalDiscreteCaseFeature.createMultiDimensionalDiscreteCaseFeature(Arrays.asList(2, 3, 2), Arrays.asList(1, 2, 1),"RF0");
		Assert.assertEquals(11, m.getIntegerValue());

		MultiDimensionalDiscreteCaseFeature m2 = MultiDimensionalDiscreteCaseFeature.createMultiDimensionalDiscreteCaseFeature(Arrays.asList(2, 3, 2), Arrays.asList(0, 2, 1),"RF1");
		Assert.assertEquals(10, m2.getIntegerValue());

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < 2; k++) {
					MultiDimensionalDiscreteCaseFeature mX = MultiDimensionalDiscreteCaseFeature.createMultiDimensionalDiscreteCaseFeature(Arrays.asList(2, 3, 2), Arrays.asList(i, j, k),"RF-X");
					System.out.println("dimensions mX are: " + i + " " + j + " " + k);
					System.out.println("Value mX is: " + mX.getIntegerValue());
				}
			}
		}
	}

}
