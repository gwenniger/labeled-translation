package unit_tests;


import integration_tests.MTPipelineTest;
import integration_tests.MTPipelineTestJoshua;
import integration_tests.MTPipelineTest.TUNER_TYPE;
import mt_pipeline.evaluation.IndividualSentencesBasedSystemsComparer;
import mt_pipeline.mtPipelineTest.TestIdentifierPair;
import mt_pipeline.mtPipelineTest.TestRerankingSettings;

import org.junit.Test;

import util.Pair;

public class IndividualSentencesBasedSystemsComparerTest {

	private static final String BASELINE_NAME = "JoshuaHatsHiero";
	private static final String INDIVIDUAL_SENTENCES_BASED_SYSTEMS_COMPARER_TEST_RESULT_FILE = "IndividualSentencesBasedSystemsComparerTest.resultFile.txt"; 
	@Test
	public void test() {

		MTPipelineTestJoshua mtPipelineTestJoshua = new MTPipelineTestJoshua();
		Pair<TestIdentifierPair> testIdentiers = mtPipelineTestJoshua.testHieroPipelineAndHieroPipelineWithReorderingLabels(TUNER_TYPE.MERT_TUNING,MTPipelineTest.MT_DATA_LOCATIONS_CONFIG_FILE_DE_EN,TestRerankingSettings.createNoRerankingSettings(),false,MTPipelineTest.SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_DE_EN);

		IndividualSentencesBasedSystemsComparer individualSentencesBasedSystemsComparer = IndividualSentencesBasedSystemsComparer.createIndividualSentencesBasedSystemsComparer(
				MTPipelineTest.getTestRootOutputDir(testIdentiers.getFirst()), MTPipelineTest.getTestRootOutputDir(testIdentiers.getSecond()), BASELINE_NAME);
		individualSentencesBasedSystemsComparer.writeSentenceBasedSystemComparisionsToFile(INDIVIDUAL_SENTENCES_BASED_SYSTEMS_COMPARER_TEST_RESULT_FILE);
	}

}
