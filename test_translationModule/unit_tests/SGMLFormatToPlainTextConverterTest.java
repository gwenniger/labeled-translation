package unit_tests;

import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import datapreparation.wmt.SGMLFormatToPlainTextConverter;
import junit.framework.Assert;
import util.FileUtil;

public class SGMLFormatToPlainTextConverterTest {

    private static String TEST_FOLDER_PATH = "./WMT-DataPreparation-TestFolder/";
    private static String TEST_FILE_PATH = TEST_FOLDER_PATH + "testFileWithSGMLLines.txt";
    private static String TEST_OUTPUT_FILE_PATH = TEST_FOLDER_PATH + "testOutputFilePlainText.txt";

    private static String LINE1 = "<srcset setid=\"newsdev2017\" srclang=\"any\">\"";
    private static String LINE2 = "<doc sysid=\"ref\" docid=\"abcnews.199680\" genre=\"news\" origlang=\"en\">";
    private static String LINE3 = "<p>";
    private static String LINE4 = "<seg id=\"1\">加利福尼亚州水务工程的新问题</seg>";
    private static String LINE5 = "<seg id=\"2\">在加利福尼亚州一个主要水务管理区披露州长杰瑞·布朗领导的行政当局将提供政府资金以完成两条巨型输水隧道的规划之后，有一些评论家和一位州议员表示，他们想进一步了解由谁来为州长所支持的拟耗资160亿美元的水务工程承担费用。</seg>";
    private static String LINE6 = "</p>";
    private static String LINE7 = "</doc>";
    private static String LINE8 = "</srcset>";

    private static String LINE1_RESULT = "加利福尼亚州水务工程的新问题";
    private static String LINE2_RESULT = "在加利福尼亚州一个主要水务管理区披露州长杰瑞·布朗领导的行政当局将提供政府资金以完成两条巨型输水隧道的规划之后，有一些评论家和一位州议员表示，他们想进一步了解由谁来为州长所支持的拟耗资160亿美元的水务工程承担费用。";

    private final static List<String> TEST_LINES = Arrays.asList(LINE1, LINE2, LINE3, LINE4, LINE5,
	    LINE6, LINE7, LINE8);
    private final static List<String> EXPECTED_RESULT_LINES = Arrays.asList(LINE1_RESULT,
	    LINE2_RESULT);

    private void setup() {
	FileUtil.createFolderIfNotExisting(TEST_FOLDER_PATH);
	FileUtil.writeLinesListToFile(TEST_LINES, TEST_FILE_PATH);
    }

    @Test
    public void test() {
	setup();
	// EmptyLineRemover.removeEmptyLinesInPlace(TEST_FILE_PATH);
	SGMLFormatToPlainTextConverter sgmlFormatToPlainTextConverter = SGMLFormatToPlainTextConverter
		.createSGMLFormatToPlainTextConverter(TEST_FILE_PATH, TEST_OUTPUT_FILE_PATH);
	sgmlFormatToPlainTextConverter.convertFile();
	List<String> resultFileStrings = FileUtil.getLinesInFile(TEST_OUTPUT_FILE_PATH, false);
	Assert.assertEquals(EXPECTED_RESULT_LINES, resultFileStrings);
    }
}
