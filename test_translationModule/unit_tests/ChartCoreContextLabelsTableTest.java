package unit_tests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import junit.framework.Assert;
import grammarExtraction.IntegerKeyRuleRepresentation;
import util.Span;
import grammarExtraction.translationRules.TranslationEquivalenceItem;
import org.junit.Test;
import coreContextLabeling.ChartCoreContextLabelsTable;
import coreContextLabeling.CoreContextLabel;

public class ChartCoreContextLabelsTableTest {

	private static int CHART_LENGTH = 10;

	private ChartCoreContextLabelsTable createTestChartCoreContextLabelsTableSimple() {
		CoreContextLabel[][] chartCoreContextLabels = new CoreContextLabel[CHART_LENGTH][CHART_LENGTH];
		for (int i = 0; i < CHART_LENGTH; i++) {
			double sourceSideEntropy = CHART_LENGTH - i;
			double ruleProbability = 1;
			double relativeEntropyWeight = 0.5;
			int amiguityScoreRank = 0;
			chartCoreContextLabels[i][i] = CoreContextLabel.createCoreContextLabel(createTranslationEquivalenceItem(i), sourceSideEntropy, ruleProbability, relativeEntropyWeight, amiguityScoreRank);
		}
		return ChartCoreContextLabelsTable.createCoreContextLabelsTable(chartCoreContextLabels);
	}

	private static TranslationEquivalenceItem createTranslationEquivalenceItem(int key) {
		List<Integer> partKeys = Arrays.asList(key);
		IntegerKeyRuleRepresentation ruleSideRepresentation = IntegerKeyRuleRepresentation.createIntegerKeyRuleRepresentation(partKeys);
		TranslationEquivalenceItem translationEquivalenceItem = TranslationEquivalenceItem.createTranslationEquivalenceItem(ruleSideRepresentation, ruleSideRepresentation);
		return translationEquivalenceItem;
	}

	private ChartCoreContextLabelsTable createTestChartCoreContextLabelsTableComplex() {
		CoreContextLabel[][] chartCoreContextLabels = new CoreContextLabel[CHART_LENGTH][CHART_LENGTH];
		double ruleProbability = 1;
		double relativeEntropyWeight = 0.5;
		int amiguityScoreRank = 0;
		chartCoreContextLabels[0][1] = CoreContextLabel.createCoreContextLabel(createTranslationEquivalenceItem(0), 0, ruleProbability, relativeEntropyWeight, amiguityScoreRank);
		chartCoreContextLabels[2][3] = CoreContextLabel.createCoreContextLabel(createTranslationEquivalenceItem(2), 4, ruleProbability, relativeEntropyWeight, amiguityScoreRank);
		chartCoreContextLabels[3][4] = CoreContextLabel.createCoreContextLabel(createTranslationEquivalenceItem(4), 1, ruleProbability, relativeEntropyWeight, amiguityScoreRank);
		return ChartCoreContextLabelsTable.createCoreContextLabelsTable(chartCoreContextLabels);
	}

	public CoreContextLabel getMaximumAmbiguityCoreContextLabelAmongSubEntries(ChartCoreContextLabelsTable chart, Span span) {
		List<CoreContextLabel> coreContextLabelsSubSpans = new ArrayList<CoreContextLabel>();
		for (Span subSpan : CoreContextLabeledChartTester.getAllSubSpans(span)) {
			CoreContextLabel contextLabel = chart.getCoreContextLabel(subSpan.getFirst(), subSpan.getSecond());
			if (contextLabel != null) {
				coreContextLabelsSubSpans.add(contextLabel);
			}
		}
		return CoreContextLabeledChartTester.getMaxUncertaintyCoreContextLabelFromList(coreContextLabelsSubSpans);
	}

	public void testConsistency(ChartCoreContextLabelsTable chartCoreContextLabelsTable) {
		for (int spanLength = 2; spanLength <= CHART_LENGTH; spanLength++) {
			for (int spanBegin = 0; spanBegin < CHART_LENGTH - spanLength + 1; spanBegin++) {
				int i = spanBegin;
				int j = spanBegin + spanLength - 1;

//				System.out.println("i:" + i + "j: " + j);
				CoreContextLabel entryLabel = chartCoreContextLabelsTable.getCoreContextLabel(i, j);
				CoreContextLabel maxUncertaintyLabelSubEntries = getMaximumAmbiguityCoreContextLabelAmongSubEntries(chartCoreContextLabelsTable, new Span(i, j));

				if ((entryLabel != null) && (maxUncertaintyLabelSubEntries != null)) {
					System.out.println("(entryLabel.getAmbiguityScore()" + entryLabel.getAmbiguityScore());
					Assert.assertTrue(entryLabel.getAmbiguityScore() >= maxUncertaintyLabelSubEntries.getAmbiguityScore());
				}
			}
		}
	}

	@Test
	public void testFillNonMinimalEntries() {
		ChartCoreContextLabelsTable chartCoreContextLabelsTable = createTestChartCoreContextLabelsTableSimple();
		chartCoreContextLabelsTable.fillNonMinimalEntries();
		chartCoreContextLabelsTable.printTable();
		testConsistency(chartCoreContextLabelsTable);
	}

	@Test
	public void testFillNonMinimalEntries2() {
		ChartCoreContextLabelsTable chartCoreContextLabelsTable = createTestChartCoreContextLabelsTableComplex();
		chartCoreContextLabelsTable.fillNonMinimalEntries();
		chartCoreContextLabelsTable.printTable();
		testConsistency(chartCoreContextLabelsTable);
	}
}
