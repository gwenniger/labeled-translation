package unit_tests;


import java.util.List;

import junit.framework.Assert;
import mt_pipeline.evaluation.SentencesNBestCandidatesListIterator;
import mt_pipeline.mtPipelineTest.TestNBestListFilesCreater;

import org.junit.Test;

public class SentencesNBestCandidatesListIteratorTest {

    @Test
    public void test() {
	TestNBestListFilesCreater.createTestTarGzFile();
	SentencesNBestCandidatesListIterator sentencesNBestCandidatesListIterator = SentencesNBestCandidatesListIterator.createSentencesNBestCandidatesListIteratorTarGzFile(TestNBestListFilesCreater.getTestNBestListTarGzFilePath());
	
	int numSentences = 0;
	while(sentencesNBestCandidatesListIterator.hasNext())
	{
	    List<String> candidateList = sentencesNBestCandidatesListIterator.next();
	    int numCandidates = candidateList.size();
	    Assert.assertEquals(TestNBestListFilesCreater.NUM_CANDIDATES_PER_SENTENCE, numCandidates);
	    numSentences++;
	}
	Assert.assertEquals(TestNBestListFilesCreater.NUM_SENTENCES, numSentences);
    }

}
