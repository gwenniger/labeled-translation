package unit_tests;

import junit.framework.Assert;

import org.junit.Test;

import alignment.AlignmentTriple;
import extended_bitg.EbitgChartBuilder;
import extended_bitg.EbitgChartBuilderBasic;
import extended_bitg.EbitgChartEntry;
import reorderingLabeling.FineParentRelativeReorderingLabel;
import reorderingLabeling.ParentRelativeReorderingLabelComputer;
import reorderingLabeling.ReorderingLabel;
import reorderingLabeling.ReorderingLabeler;

public class ReorderingLabelerTest {

	private static final int MAX_ALLOWED_INFERENCES_PERNODE = 100000;
	private static boolean USE_GREEDY_NULL_BINDING = true;
	private static boolean USE_CACHING = true;
	private static int TEST_SENTENCE_LENGTH = 10;

	private void assertIfTopNodeChartEntryReorderingLabelIsTopNode(EbitgChartEntry chartEntry, EbitgChartBuilderBasic ebitgChartBuilder, ReorderingLabel reorderingLabel) {
		if (ParentRelativeReorderingLabelComputer.isTopNodeChartEntry(chartEntry, ebitgChartBuilder.getChart())) {
			Assert.assertEquals(FineParentRelativeReorderingLabel.TOP_NODE_LABEL, reorderingLabel);
		}
	}

	private boolean chartEntryStartsAtLeftEdge(EbitgChartEntry chartEntry) {
		return (chartEntry.getI() == 0);
	}

	private boolean chartEntryEndsAtRightEdge(EbitgChartEntry chartEntry, EbitgChartBuilder<?, ?> ebitgChartBuilder) {
		return (chartEntry.getJ() == (ebitgChartBuilder.getChartSourceLength() - 1));
	}

	private boolean isLeftSideChartEntry(EbitgChartEntry chartEntry, EbitgChartBuilder<?, ?> ebitgChartBuilder) {
		return chartEntryStartsAtLeftEdge(chartEntry) && !chartEntryEndsAtRightEdge(chartEntry, ebitgChartBuilder);
	}

	private boolean isRightSideChartEntry(EbitgChartEntry chartEntry, EbitgChartBuilder<?, ?> ebitgChartBuilder) {

		return chartEntryEndsAtRightEdge(chartEntry, ebitgChartBuilder) && (!chartEntryStartsAtLeftEdge(chartEntry));
	}

	private boolean isTopChartEntry(EbitgChartEntry chartEntry, EbitgChartBuilder<?, ?> ebitgChartBuilder) {

		return chartEntryEndsAtRightEdge(chartEntry, ebitgChartBuilder) && chartEntryStartsAtLeftEdge(chartEntry);
	}

	private boolean isMiddleChartEntry(EbitgChartEntry chartEntry, EbitgChartBuilder<?, ?> ebitgChartBuilder) {
		return (!isLeftSideChartEntry(chartEntry, ebitgChartBuilder) && (!isRightSideChartEntry(chartEntry, ebitgChartBuilder))) && (!isTopChartEntry(chartEntry, ebitgChartBuilder));
	}

	private void Monotone_assertIfLeftSideChartEntryReorderingLabelIsRightBindingMonontone(EbitgChartEntry chartEntry, EbitgChartBuilderBasic ebitgChartBuilder, ReorderingLabel reorderingLabel) {
		if (isLeftSideChartEntry(chartEntry, ebitgChartBuilder)) {
			Assert.assertEquals(FineParentRelativeReorderingLabel.RIGHT_BINDING_MONOTONE_LABEL, reorderingLabel);
		}
	}

	private void Monotone_assertIfRightSideChartEntryReorderingLabelIsLeftBindingMonontone(EbitgChartEntry chartEntry, EbitgChartBuilderBasic ebitgChartBuilder, ReorderingLabel reorderingLabel) {
		if (isRightSideChartEntry(chartEntry, ebitgChartBuilder)) {
			Assert.assertEquals(FineParentRelativeReorderingLabel.LEFT_BINDING_MONOTONE_LABEL, reorderingLabel);
		}
	}

	private void Monotone_assertIfMiddleChartEntryReorderingLabelIsEmbeddedInFullyMonotone(EbitgChartEntry chartEntry, EbitgChartBuilderBasic ebitgChartBuilder, ReorderingLabel reorderingLabel) {
		if (isMiddleChartEntry(chartEntry, ebitgChartBuilder)) {
			Assert.assertEquals(FineParentRelativeReorderingLabel.EMBEDDED_FULLY_MONOTONE_LABEL, reorderingLabel);
		}
	}

	private void FullyInverted_assertIfLeftSideChartEntryReorderingLabelIsRightBindingInverted(EbitgChartEntry chartEntry, EbitgChartBuilderBasic ebitgChartBuilder, ReorderingLabel reorderingLabel) {
		if (isLeftSideChartEntry(chartEntry, ebitgChartBuilder)) {
			Assert.assertEquals(FineParentRelativeReorderingLabel.RIGHT_BINDING_INVERTED_LABEL, reorderingLabel);
		}
	}

	private void FullyInverted_assertIfRightSideChartEntryReorderingLabelIsLeftBindingInverted(EbitgChartEntry chartEntry, EbitgChartBuilderBasic ebitgChartBuilder, ReorderingLabel reorderingLabel) {
		if (isRightSideChartEntry(chartEntry, ebitgChartBuilder)) {
			Assert.assertEquals(FineParentRelativeReorderingLabel.LEFT_BINDING_INVERTED_LABEL, reorderingLabel);
		}
	}

	private void FullyInverted_assertIfMiddleChartEntryReorderingLabelIsEmbeddedInFullyInverted(EbitgChartEntry chartEntry, EbitgChartBuilderBasic ebitgChartBuilder, ReorderingLabel reorderingLabel) {
		if (isMiddleChartEntry(chartEntry, ebitgChartBuilder)) {
			Assert.assertEquals(FineParentRelativeReorderingLabel.EMBEDDED_FULLY_INVERTED_LABEL, reorderingLabel);
		}
	}

	private void NonInvertableFourPermutation_assertIfNotTopNodeThenFullyDiscontinuous(EbitgChartEntry chartEntry, EbitgChartBuilderBasic ebitgChartBuilder, ReorderingLabel reorderingLabel) {
		if (!isTopChartEntry(chartEntry, ebitgChartBuilder)) {
			Assert.assertEquals(FineParentRelativeReorderingLabel.EMBEDDED_FULLY_DISCONTINUOUS_LABEL, reorderingLabel);
		}
	}

	@Test
	public void testReorderingLabelerMonotoneExample() {
		AlignmentTriple monotoneAlignmentTriple = AlignmentTriple.createMonontoneAlignmentTriple(TEST_SENTENCE_LENGTH);
		EbitgChartBuilderBasic ebitgChartBuilder = EbitgChartBuilderBasic.createCompactHATsEbitgChartBuilder(monotoneAlignmentTriple.getSourceSentenceAsString(),
				monotoneAlignmentTriple.getTargetSentenceAsString(), monotoneAlignmentTriple.getAlignmentAsString(), MAX_ALLOWED_INFERENCES_PERNODE, USE_GREEDY_NULL_BINDING, USE_CACHING);
		ebitgChartBuilder.findDerivationsAlignment();
		ReorderingLabeler reorderingLabeler = ReorderingLabeler.createReorderingLabeler(ebitgChartBuilder.getChart());
		for (EbitgChartEntry chartEntry : ebitgChartBuilder.getChart().getChartEntriesAsList()) {
			if (chartEntry.isComplete()) {
				ReorderingLabel reorderingLabel = reorderingLabeler.getReorderingLabelForInference(chartEntry.getFirstCompleteMinimumTargetBindingInference());
				assertIfTopNodeChartEntryReorderingLabelIsTopNode(chartEntry, ebitgChartBuilder, reorderingLabel);
				Monotone_assertIfLeftSideChartEntryReorderingLabelIsRightBindingMonontone(chartEntry, ebitgChartBuilder, reorderingLabel);
				Monotone_assertIfRightSideChartEntryReorderingLabelIsLeftBindingMonontone(chartEntry, ebitgChartBuilder, reorderingLabel);
				Monotone_assertIfMiddleChartEntryReorderingLabelIsEmbeddedInFullyMonotone(chartEntry, ebitgChartBuilder, reorderingLabel);

				ReorderingLabel reorderingLabelFromChart = chartEntry.getFirstCompleteMinimumTargetBindingInference().getParentRelativeReorderingLabel();
				assertIfTopNodeChartEntryReorderingLabelIsTopNode(chartEntry, ebitgChartBuilder, reorderingLabelFromChart);
				Monotone_assertIfLeftSideChartEntryReorderingLabelIsRightBindingMonontone(chartEntry, ebitgChartBuilder, reorderingLabelFromChart);
				Monotone_assertIfRightSideChartEntryReorderingLabelIsLeftBindingMonontone(chartEntry, ebitgChartBuilder, reorderingLabelFromChart);
				Monotone_assertIfMiddleChartEntryReorderingLabelIsEmbeddedInFullyMonotone(chartEntry, ebitgChartBuilder, reorderingLabelFromChart);
			}
		}
	}

	@Test
	public void testReorderingLabelerInvertedExample() {
		AlignmentTriple invertedAlignmentTriple = AlignmentTriple.createFullyInvertedAlignmentTriple(TEST_SENTENCE_LENGTH);
		EbitgChartBuilderBasic ebitgChartBuilder = EbitgChartBuilderBasic.createCompactHATsEbitgChartBuilder(invertedAlignmentTriple.getSourceSentenceAsString(),
				invertedAlignmentTriple.getTargetSentenceAsString(), invertedAlignmentTriple.getAlignmentAsString(), MAX_ALLOWED_INFERENCES_PERNODE, USE_GREEDY_NULL_BINDING, USE_CACHING);
		ebitgChartBuilder.findDerivationsAlignment();
		ReorderingLabeler reorderingLabeler = ReorderingLabeler.createReorderingLabeler(ebitgChartBuilder.getChart());
		for (EbitgChartEntry chartEntry : ebitgChartBuilder.getChart().getChartEntriesAsList()) {
			if (chartEntry.isComplete()) {
				ReorderingLabel reorderingLabel = reorderingLabeler.getReorderingLabelForInference(chartEntry.getFirstCompleteMinimumTargetBindingInference());
				assertIfTopNodeChartEntryReorderingLabelIsTopNode(chartEntry, ebitgChartBuilder, reorderingLabel);
				FullyInverted_assertIfLeftSideChartEntryReorderingLabelIsRightBindingInverted(chartEntry, ebitgChartBuilder, reorderingLabel);
				FullyInverted_assertIfRightSideChartEntryReorderingLabelIsLeftBindingInverted(chartEntry, ebitgChartBuilder, reorderingLabel);
				FullyInverted_assertIfMiddleChartEntryReorderingLabelIsEmbeddedInFullyInverted(chartEntry, ebitgChartBuilder, reorderingLabel);

				ReorderingLabel reorderingLabelFromChart = chartEntry.getFirstCompleteMinimumTargetBindingInference().getParentRelativeReorderingLabel();
				assertIfTopNodeChartEntryReorderingLabelIsTopNode(chartEntry, ebitgChartBuilder, reorderingLabelFromChart);
				FullyInverted_assertIfLeftSideChartEntryReorderingLabelIsRightBindingInverted(chartEntry, ebitgChartBuilder, reorderingLabelFromChart);
				FullyInverted_assertIfRightSideChartEntryReorderingLabelIsLeftBindingInverted(chartEntry, ebitgChartBuilder, reorderingLabelFromChart);
				FullyInverted_assertIfMiddleChartEntryReorderingLabelIsEmbeddedInFullyInverted(chartEntry, ebitgChartBuilder, reorderingLabelFromChart);

			}

		}
	}

	@Test
	public void testNonInvertableAlignmentExample() {
		AlignmentTriple invertedAlignmentTriple = AlignmentTriple.createFourPermutationAlignmentTriple(1);
		EbitgChartBuilderBasic ebitgChartBuilder = EbitgChartBuilderBasic.createCompactHATsEbitgChartBuilder(invertedAlignmentTriple.getSourceSentenceAsString(),
				invertedAlignmentTriple.getTargetSentenceAsString(), invertedAlignmentTriple.getAlignmentAsString(), MAX_ALLOWED_INFERENCES_PERNODE, USE_GREEDY_NULL_BINDING, USE_CACHING);
		ebitgChartBuilder.findDerivationsAlignment();
		ReorderingLabeler reorderingLabeler = ReorderingLabeler.createReorderingLabeler(ebitgChartBuilder.getChart());
		for (EbitgChartEntry chartEntry : ebitgChartBuilder.getChart().getChartEntriesAsList()) {
			if (chartEntry.isComplete()) {
				ReorderingLabel reorderingLabel = reorderingLabeler.getReorderingLabelForInference(chartEntry.getFirstCompleteMinimumTargetBindingInference());
				assertIfTopNodeChartEntryReorderingLabelIsTopNode(chartEntry, ebitgChartBuilder, reorderingLabel);
				NonInvertableFourPermutation_assertIfNotTopNodeThenFullyDiscontinuous(chartEntry, ebitgChartBuilder, reorderingLabel);

				ReorderingLabel reorderingLabelFromChart = chartEntry.getFirstCompleteMinimumTargetBindingInference().getParentRelativeReorderingLabel();
				assertIfTopNodeChartEntryReorderingLabelIsTopNode(chartEntry, ebitgChartBuilder, reorderingLabelFromChart);
				NonInvertableFourPermutation_assertIfNotTopNodeThenFullyDiscontinuous(chartEntry, ebitgChartBuilder, reorderingLabelFromChart);
			}

		}
	}

}
