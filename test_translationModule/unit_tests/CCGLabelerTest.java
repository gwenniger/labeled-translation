package unit_tests;

import mt_pipeline.preAndPostProcessing.ExtraBracketsRemover;

import org.junit.Test;
import tsg.TSNodeLabel;
import ccgLabeling.CCGLabeler;
import ccgLabeling.LabelGenerator.UnaryCategoryHandlerType;

public class CCGLabelerTest {

	@Test(expected=AssertionError.class)
	public void testAssertionsEnabled() 
	{
		assert(false);
	}

	
	private void testCCGLabeler(String parse) 
	{
		try {
			CCGLabeler ccgLabeler = CCGLabeler.createTargetCCGLabeler(new TSNodeLabel(parse),UnaryCategoryHandlerType.ALL,true,true);
			assert(ccgLabeler != null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Test(expected=AssertionError.class)
	public void testCCGLabelerOneBreaking() 
	{
		String parse = "( (S (S (NP (DT the) (NN aim)) (VP (VBD was) (RB not) (S (VP (TO to) (VP (VB draw) (NP (NNS conclusions))))))) (PRN (: -) (S (NP (PRP it)) (VP (VBD was) (ADJP (JJ impossible)) (S (VP (TO to) (VP (VB draw) (NP (DT any))))))) (: -)) (CC but) (S (NP (DT the) (NN debate)) (VP (VBD was) (ADVP (RB certainly)) (ADJP (RB very) (JJ encouraging)))) (. .)) )";
		testCCGLabeler(parse);
	}
	
	public void testCCGLabelerOneFixed() 
	{
		String parse = "( (S (S (NP (DT the) (NN aim)) (VP (VBD was) (RB not) (S (VP (TO to) (VP (VB draw) (NP (NNS conclusions))))))) (PRN (: -) (S (NP (PRP it)) (VP (VBD was) (ADJP (JJ impossible)) (S (VP (TO to) (VP (VB draw) (NP (DT any))))))) (: -)) (CC but) (S (NP (DT the) (NN debate)) (VP (VBD was) (ADVP (RB certainly)) (ADJP (RB very) (JJ encouraging)))) (. .)) )";
		testCCGLabeler(ExtraBracketsRemover.removeExtraBrackets(parse));
	}
	
	@Test(expected=AssertionError.class)
	public void testCCGLabeler() 
	{
		String parse = "( (S (ADVP (RB however)) (, ,) (NP (PRP we)) (VP (MD shall) (VP (VB intensify) (NP (NP (PRP$ our) (NN collaboration)) (CC and) (NP (NP (PRP$ our) (NN dialogue)) (PP (IN with) (NP (NP (DT the) (JJ national) (NNS parliaments)) (PP (IN for) (NP (NP (DT the) (JJ entire) (NN duration)) (PP (IN of) (NP (DT the) (JJ intergovernmental) (NN conference))))))))))) (. .)) )";
		testCCGLabeler(parse);
	}
	@Test
	public void testCCGLabelerFixed() 
	{
		String parse = "( (S (ADVP (RB however)) (, ,) (NP (PRP we)) (VP (MD shall) (VP (VB intensify) (NP (NP (PRP$ our) (NN collaboration)) (CC and) (NP (NP (PRP$ our) (NN dialogue)) (PP (IN with) (NP (NP (DT the) (JJ national) (NNS parliaments)) (PP (IN for) (NP (NP (DT the) (JJ entire) (NN duration)) (PP (IN of) (NP (DT the) (JJ intergovernmental) (NN conference))))))))))) (. .)) )";
		testCCGLabeler(ExtraBracketsRemover.removeExtraBrackets(parse));
	}
	


}
