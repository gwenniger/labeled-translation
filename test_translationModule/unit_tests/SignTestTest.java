package unit_tests;

import static org.junit.Assert.*;
import mt_pipeline.evaluation.lrscoring.SignTest;

import org.junit.Test;

public class SignTestTest {

    @Test
    public void test() {
	//See: http://sphweb.bumc.bu.edu/otlt/MPH-Modules/BS/BS704_Nonparametric/BS704_Nonparametric5.html
	//http://courses.nus.edu.sg/course/stacar/internet/st2238/handouts/sign_table.pdf
	// For explanation
	
	int n = 100;
	int k = 40;
	double p = SignTest.computeProbabilityOneSpecificSystemBetterForUpToKSentencesGivenTwoSystemsEqual(
		n, k);
	System.out.println("p: " + p);
	System.out.println("2*p: " + 2*p);
	
	
	n = 120;
	k = 50;
        p = SignTest.computeProbabilityOneSpecificSystemBetterForUpToKSentencesGivenTwoSystemsEqual(
		n, k);
	System.out.println("p: " + p);
	System.out.println("2*p: " + 2*p);
	

	n = 1400;
	k = 700;
        p = SignTest.computeProbabilityOneSpecificSystemBetterForUpToKSentencesGivenTwoSystemsEqual(
		n, k);
	System.out.println("p: " + p);
	System.out.println("2*p: " + 2*p);
	
	
	
	
	//System.out.println("log(0.5) = " + Math.log(0.5));
	//System.out.println("Math.pow(Math.E,Math.log(0.5)) = " + Math.pow(Math.E, Math.log(0.5)));

//	n = 8;
//	k = 2;
//	p = SignTest.computeProbabilityOneSpecificSystemBetterForUpToKSentencesGivenTwoSystemsEqual(
//		n, k);
//	System.out.println("p: " + p);
//	double pAnyBetter = SignTest.computeProbabilityAnySystemBetterForUpToKSentencesGivenTwoSystemsEqual(
//		n, k);
//	System.out.println("pAnyBetter: " + pAnyBetter);


	
    }

}
