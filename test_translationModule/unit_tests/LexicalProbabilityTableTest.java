package unit_tests;


import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import grammarExtraction.translationRuleTrie.ConcurrentLexicalProbabilityTable;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

import org.junit.Test;

import util.InputReading;
import alignment.Alignment;
import alignment.AlignmentTriple;

public class LexicalProbabilityTableTest 
{

	
	
	private static List<AlignmentTriple> createSimpleAlignmentTriples()
	{
		List<AlignmentTriple> result = new ArrayList<AlignmentTriple>();
		result.add(AlignmentTriple.createAlignmentTriple("s1 s2 s3", "t1 t2 t3", "0-0 1-1 2-2"));
		result.add(AlignmentTriple.createAlignmentTriple("s1 s2 s3 s4", "t4 t5 t6 t7", "0-0 1-1 2-2 3-3"));
		result.add(AlignmentTriple.createAlignmentTriple("sA sB", "tA tB", "0-0 0-1 1-1"));
		return result;
	}
	
	
	
	private static ConcurrentLexicalProbabilityTable createSimpleConcurrentLexicalProbabilityTable()
	{
		List<AlignmentTriple> alignmentTriples = createSimpleAlignmentTriples();
		WordKeyMappingTable wordKeyMappingTable = WordKeyMappingTable.createWordKeyMappingTableFromAlignmentTriples(alignmentTriples, 2,16); 
		ConcurrentLexicalProbabilityTable result = ConcurrentLexicalProbabilityTable.createConcurrentLexicalProbabilityTable(wordKeyMappingTable,16);
		
		result.addAlignmentTriples(alignmentTriples);
		result.computeProbabilitiesFromCounts();
		
		return result;
	}
	
	 @Test(expected=AssertionError.class)
	  public void testAssertionsEnabled() 
	  {
	    assert(false);
	  }
	
	@Test
	public void testWordProbabilities() 
	{
		ConcurrentLexicalProbabilityTable lexicalProabilityTable = createSimpleConcurrentLexicalProbabilityTable();
		
		Assert.assertEquals(0.5, lexicalProabilityTable.getProbability("sA","tA"));
		Assert.assertEquals(0.5, lexicalProabilityTable.getProbability("sA","tB"));
		Assert.assertEquals(1.0, lexicalProabilityTable.getProbability("sB","tB"));
		
		
		double p1 = lexicalProabilityTable.getProbability("s1","t1");
		double p2 = lexicalProabilityTable.getProbability("s2","t2");
		double p3 = lexicalProabilityTable.getProbability("s3","t3");
		double pTotal = p1*p2*p3;
		Assert.assertEquals(pTotal,lexicalProabilityTable.computeAlignedPhrasePairProbability(InputReading.getWordsFromString("s1 s2 s3"), InputReading.getWordsFromString("t1 t2 t3"), Alignment.createAlignment("0-0 1-1 2-2")));
		Assert.assertEquals(0.0,lexicalProabilityTable.computeAlignedPhrasePairProbability(InputReading.getWordsFromString("s1 s2 s3"), InputReading.getWordsFromString("t1 t2 t1"), Alignment.createAlignment("0-0 1-1 2-2")));
	}

}
