package unit_tests;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import junit.framework.Assert;
import mt_pipeline.preAndPostProcessing.RandomString;
import mt_pipeline.preAndPostProcessing.TrueCasedLinesFileReconstructor;
import org.junit.Test;

public class TrueCasedLinesFileReconstructorTest {

	private static final String LOWER_CASED_TEST_FILE_NAME = "LowerCasedTestFile.txt";
	private static final String CASED_TEST_FILE_NAME = "CasedTestFile.txt";
	private static final String CASED_RESULT_FILE_NAME = "CasedResultFile.txt";
	private static final RandomString RANDOM_STRING_CREATOR = new RandomString(20);
	private static final int MAX_RANDOM_STRING_INTTERUPTION_LENGTH = 200;

	private static final WordCreationStrategy FIRST_LETTER_CAPITAL = new WordCreationStrategy() {

		@Override
		public String createWord(int sentanceNumber, int wordNumber) {
			return createWordWithCapitalFirstLetter(sentanceNumber, wordNumber);
		}
	};

	private static final WordCreationStrategy LOWER_CASE_WORD = new WordCreationStrategy() {

		@Override
		public String createWord(int sentanceNumber, int wordNumber) {
			return createLowerCaseWord(sentanceNumber, wordNumber);
		}
	};

	private static String createLowerCaseWord(int sentenceNumber, int wordNumber) {
		String result = "word";
		return createWordN(sentenceNumber, wordNumber, result);
	}

	private static String createWordWithCapitalFirstLetter(int sentenceNumber, int wordNumber) {
		String result = "Word";
		return createWordN(sentenceNumber, wordNumber, result);
	}

	private static String createWordN(int sentenceNumber, int wordNumber, String result) {

		result += sentenceNumber + "-" + wordNumber;
		return result;
	}

	private String createRandomInterrupintgNoiseString() {
		return RANDOM_STRING_CREATOR.nextStringRandomLength(MAX_RANDOM_STRING_INTTERUPTION_LENGTH);
	}

	private void createCasedTestFile() {
		try {
			final ArtificalSentenceCreator artificalSentenceCreator = new ArtificalSentenceCreator(FIRST_LETTER_CAPITAL);
			BufferedWriter fileWriter = new BufferedWriter(new FileWriter(CASED_TEST_FILE_NAME));

			for (int sentenceNumber = 0; sentenceNumber < 30; sentenceNumber += 3) {
				String doubleSentence = artificalSentenceCreator.createSentenceN(sentenceNumber) + artificalSentenceCreator.createSentenceN(sentenceNumber + 1);
				String singleSentence = artificalSentenceCreator.createSentenceN(sentenceNumber + 2);
				fileWriter.write(doubleSentence + createRandomInterrupintgNoiseString() + "\n");
				fileWriter.write(singleSentence + createRandomInterrupintgNoiseString() + "\n");
			}
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void createRandomOrderCasedTestFile() {
		try {
			final ArtificalSentenceCreator artificalSentenceCreator = new ArtificalSentenceCreator(FIRST_LETTER_CAPITAL);
			BufferedWriter fileWriter = new BufferedWriter(new FileWriter(CASED_TEST_FILE_NAME));

			List<Integer> randomOrderSentenceNumberList = new ArrayList<Integer>();
			for (int sentenceNumber = 0; sentenceNumber < 30; sentenceNumber++) {
				randomOrderSentenceNumberList.add(sentenceNumber);
			}
			Collections.shuffle(randomOrderSentenceNumberList);

			for (int number = 0; number < 30; number += 3) {
				String doubleSentence = artificalSentenceCreator.createSentenceN(randomOrderSentenceNumberList.get(number))
						+ artificalSentenceCreator.createSentenceN(randomOrderSentenceNumberList.get(number + 1));
				String singleSentence = artificalSentenceCreator.createSentenceN(randomOrderSentenceNumberList.get(number + 2));
				fileWriter.write(doubleSentence + createRandomInterrupintgNoiseString() + "\n");
				fileWriter.write(singleSentence + createRandomInterrupintgNoiseString() + "\n");
			}
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void createLowerCasedTestFile() {
		try {
			final ArtificalSentenceCreator artificalSentenceCreator = new ArtificalSentenceCreator(LOWER_CASE_WORD);
			BufferedWriter fileWriter = new BufferedWriter(new FileWriter(LOWER_CASED_TEST_FILE_NAME));

			for (int sentenceNumber = 0; sentenceNumber < 30; sentenceNumber++) {
				String singleSentence = artificalSentenceCreator.createSentenceN(sentenceNumber);
				fileWriter.write(singleSentence + "\n");
			}
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void checkEveryWordInResultFileHasUpperCasedFirstLetter(String resultFilePath) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(resultFilePath));
			String line = null;
			while ((line = reader.readLine()) != null) {
				String[] words = line.split("\\s");
				for (String word : words) {
					Assert.assertTrue(Character.isUpperCase(word.charAt(0)));
					for (int i = 1; i < word.length(); i++) {
						Assert.assertTrue(Character.isLowerCase(word.charAt(1)));
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	@Test(expected = AssertionError.class)
	public void testAssertionsEnabled() {
		assert (false);
	}

	@Test
	public void testSameOrderCasedFile() {
		createCasedTestFile();
		createLowerCasedTestFile();
		TrueCasedLinesFileReconstructor.produceTrueCasedEquivalentLowerCasedFile(LOWER_CASED_TEST_FILE_NAME, CASED_TEST_FILE_NAME, CASED_RESULT_FILE_NAME);
		TrueCasedLinesFileReconstructor.testCorrectnessResultFile(LOWER_CASED_TEST_FILE_NAME, CASED_RESULT_FILE_NAME);
		checkEveryWordInResultFileHasUpperCasedFirstLetter(CASED_RESULT_FILE_NAME);
	}

	/**
	 * This test tests the most difficult situation conceivable were TrueCasedLinesFileReconstructor may still be expected to work: - The original lines are
	 * differently distributed over the lines in the Cased file (i.e. multiple lines from the lower cased file are put together in the same line in the Cased
	 * file) - There is random noise with random lengths distributed within the sentences in the Cased file - The original order between sentences from the
	 * lower cased file is not preserved in the cased reference file
	 */
	@Test
	public void testRandomlyChangedOrderCasedFile() {
		createRandomOrderCasedTestFile();
		createLowerCasedTestFile();
		TrueCasedLinesFileReconstructor.produceTrueCasedEquivalentLowerCasedFile(LOWER_CASED_TEST_FILE_NAME, CASED_TEST_FILE_NAME, CASED_RESULT_FILE_NAME);
		TrueCasedLinesFileReconstructor.testCorrectnessResultFile(LOWER_CASED_TEST_FILE_NAME, CASED_RESULT_FILE_NAME);
		checkEveryWordInResultFileHasUpperCasedFirstLetter(CASED_RESULT_FILE_NAME);
	}

	private static interface WordCreationStrategy {

		String createWord(int sentenceNumber, int wordNumber);

	}

	private static class ArtificalSentenceCreator {
		private final WordCreationStrategy wordCreationStrategy;

		private ArtificalSentenceCreator(WordCreationStrategy wordCreationStrategy) {
			this.wordCreationStrategy = wordCreationStrategy;
		}

		private String createSentenceNDelegate(int sentenceNumber, int noWords) {
			String result = "";
			for (int wordNumber = 0; wordNumber < noWords; wordNumber++) {

				String temp = wordCreationStrategy.createWord(sentenceNumber, wordNumber);
				result += temp + " ";
			}
			return result;
		}

		private String createSentenceN(int n) {
			int i = n % 3;
			int noWords;
			switch (i) {
			case 0:
				noWords = 6;
				break;
			case 1:
				noWords = 7;
				break;
			case 3:
				noWords = 5;
				break;
			default:
				noWords = 4;
				break;
			}
			return createSentenceNDelegate(n, noWords);
		}

	}

}
