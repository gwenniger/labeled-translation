package unit_tests;

import grammarExtraction.translationRuleDiskTables.MultiThreadSort;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import junit.framework.Assert;
import org.junit.Test;
import util.FileStatistics;

public class MultiThreadSortTest {

	private static String UNSORTED_TEST_FILE_NAME = "MultiThreadSortTest-unsortedTestFile.raw";
	private static String SORTED_TEST_FILE_NAME = "MultiThreadSortTest-sortedTestFile";
	private static String RESULT_FILE_NAME = "MultiThreadSortTest-resultFile.txt";
	private static int NUM_TEST_ELEMENTS = 103;
	private static int NUM_THREADS = 3;

	private String createTestElement(int elementNumber) {
		return "" + elementNumber;
	}

	private List<String> createTestElementsList(int numElements) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < numElements; i++) {
			result.add(createTestElement(i));
		}
		return result;
	}

	private List<String> createSortedTestElementsList(int numElements) {
		List<String> sortedElements = createTestElementsList(numElements);
		Collections.sort(sortedElements);
		return sortedElements;
	}

	private List<String> createUnsortedTestElementsList(int numElements) {
		List<String> unsortedElements = createTestElementsList(numElements);
		Collections.shuffle(unsortedElements);
		return unsortedElements;

	}

	private void createSortedTestFileName() {
		BufferedWriter outputWriter = null;
		try {
			outputWriter = new BufferedWriter(new FileWriter(SORTED_TEST_FILE_NAME));
			for (String element : createSortedTestElementsList(NUM_TEST_ELEMENTS)) {
				outputWriter.write(element + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (outputWriter != null) {
					outputWriter.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void createUnsortedTestFileName() {
		BufferedWriter outputWriter = null;
		try {
			outputWriter = new BufferedWriter(new FileWriter(UNSORTED_TEST_FILE_NAME));
			for (String element : createUnsortedTestElementsList(NUM_TEST_ELEMENTS)) {
				outputWriter.write(element + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (outputWriter != null) {
					outputWriter.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private boolean haveEqualLengths(String file1, String file2) {
		try {
			int lengthFile1 = FileStatistics.countLines(file1);
			int lengthFile2 = FileStatistics.countLines(file2);
			System.out.println("lengt file 1:" + lengthFile1);
			System.out.println("lengt file 2:" + lengthFile2);
			return (lengthFile1 == lengthFile2);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Test
	public void testLexicographicality() {

		String s1 = "7";
		String s2 = "71";
		System.out.println(s1 + ".compareTo(" + s2 + ") : " + s1.compareTo(s2));

		s1 = "7";
		s2 = "61";
		System.out.println(s1 + ".compareTo(" + s2 + ") : " + s1.compareTo(s2));

		s1 = "7";
		s2 = "70";
		System.out.println(s1 + ".compareTo(" + s2 + ") :" + s1.compareTo(s2));

		s1 = "9";
		s2 = "10";
		System.out.println(s1 + ".compareTo(" + s2 + ") :" + s1.compareTo(s2));

	}

	@Test
	public void testIsSorted() {
		MultiThreadSort multiThreadSort = MultiThreadSort.createMultiThreadSort(NUM_THREADS, UNSORTED_TEST_FILE_NAME, RESULT_FILE_NAME,".");
		createSortedTestFileName();
		createUnsortedTestFileName();
		multiThreadSort.sortLexicographically();
		Assert.assertTrue(MultiThreadSort.isSortedLexicographically(SORTED_TEST_FILE_NAME));
		Assert.assertFalse(MultiThreadSort.isSortedLexicographically(UNSORTED_TEST_FILE_NAME));
		Assert.assertTrue(MultiThreadSort.isSortedLexicographically(RESULT_FILE_NAME));
		Assert.assertTrue(haveEqualLengths(UNSORTED_TEST_FILE_NAME, RESULT_FILE_NAME));
	}

}
