package unit_tests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import junit.framework.Assert;
import org.junit.Test;
import datapreparation.DuplicatesRemover;
import util.FileUtil;

public class DuplicatesRemoverTest {

    private static List<String> file1StringList = Arrays.asList("de man", "de vrouw", "de man",
	    "paard", "koning olifant", "koning olifant", "koning olifant");
    private static List<String> file2StringList = Arrays.asList("de tijger", "het meisje",
	    "de tijger", "kameel", "de man", "koning olifant", "koning olifant");

    private static List<String> file3StringList = Arrays.asList("1", "2", "3", "4", "5", "6", "7");

    private static String TEST_FOLDER_PATH = "./DuplicatesRemoverTestFolder/";
    private static String TEST_FOLDER_PATH_NO_DUPLICATES = "./DuplicatesRemoverTestFolder/no-duplicates/";
    private static String TEST_FILE1_PATH = TEST_FOLDER_PATH + "testFile1.txt";
    private static String TEST_FILE2_PATH = TEST_FOLDER_PATH + "testFile2.txt";
    private static String TEST_FILE3_PATH = TEST_FOLDER_PATH + "testFile3.txt";
    private static String TEST_FILE1_NO_DUPLICATES_PATH = TEST_FOLDER_PATH_NO_DUPLICATES
	    + "testFile1.txt";
    private static String TEST_FILE2_NO_DUPLICATES_PATH = TEST_FOLDER_PATH_NO_DUPLICATES
	    + "testFile2.txt";
    private static String TEST_FILE3_NO_DUPLICATES_PATH = TEST_FOLDER_PATH_NO_DUPLICATES
	    + "testFile3.txt";

    private void setup() {
	FileUtil.createFolderIfNotExisting(TEST_FOLDER_PATH);
	FileUtil.createFolderIfNotExisting(TEST_FOLDER_PATH_NO_DUPLICATES);
	FileUtil.writeLinesListToFile(file1StringList, TEST_FILE1_PATH);
	FileUtil.writeLinesListToFile(file2StringList, TEST_FILE2_PATH);
	FileUtil.writeLinesListToFile(file3StringList, TEST_FILE3_PATH);
    }

    @Test
    public void testDuplicatesRemoverWithOnlyDuplicateDefiningFiles() {
	setup();
	List<String> inputFilePaths = Arrays.asList(TEST_FILE1_PATH, TEST_FILE2_PATH);
	DuplicatesRemover duplicatesRemover = DuplicatesRemover.createDuplicatesRemover(
		inputFilePaths, new ArrayList<String>(), TEST_FOLDER_PATH_NO_DUPLICATES);
	duplicatesRemover.produceRewrittenFilesWithoutDuplicates();

	List<String> file1NoDuplicatesStrings = FileUtil.getLinesInFile(
		TEST_FILE1_NO_DUPLICATES_PATH, false);
	List<String> file2NoDuplicatesStrings = FileUtil.getLinesInFile(
		TEST_FILE2_NO_DUPLICATES_PATH, false);

	Assert.assertEquals(
		Arrays.asList("de man", "de vrouw", "paard", "koning olifant", "koning olifant"),
		file1NoDuplicatesStrings);

	Assert.assertEquals(
		Arrays.asList("de tijger", "het meisje", "kameel", "de man", "koning olifant"),
		file2NoDuplicatesStrings);

    }

    @Test
    public void testDuplicatesRemoverWithDuplicateDefiningAndDependentFiles() {
	setup();
	List<String> duplicateDefiningInputFilePaths = Arrays.asList(TEST_FILE1_PATH,
		TEST_FILE2_PATH);
	List<String> associatedDependentInputFilePats = Arrays.asList(TEST_FILE3_PATH);
	DuplicatesRemover duplicatesRemover = DuplicatesRemover.createDuplicatesRemover(
		duplicateDefiningInputFilePaths, associatedDependentInputFilePats,
		TEST_FOLDER_PATH_NO_DUPLICATES);
	duplicatesRemover.produceRewrittenFilesWithoutDuplicates();

	List<String> file1NoDuplicatesStrings = FileUtil.getLinesInFile(
		TEST_FILE1_NO_DUPLICATES_PATH, false);
	List<String> file2NoDuplicatesStrings = FileUtil.getLinesInFile(
		TEST_FILE2_NO_DUPLICATES_PATH, false);
	List<String> file3NoDuplicatesStrings = FileUtil.getLinesInFile(
		TEST_FILE3_NO_DUPLICATES_PATH, false);

	Assert.assertEquals(
		Arrays.asList("de man", "de vrouw", "paard", "koning olifant", "koning olifant"),
		file1NoDuplicatesStrings);

	Assert.assertEquals(
		Arrays.asList("de tijger", "het meisje", "kameel", "de man", "koning olifant"),
		file2NoDuplicatesStrings);

	Assert.assertEquals(Arrays.asList("1", "2", "4", "5", "6"), file3NoDuplicatesStrings);

    }

}
