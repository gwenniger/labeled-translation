package unit_tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import util.Pair;
import datapreparation.SplitpatternPreservingStringTokenizer;
import datapreparation.WhitespaceReformatter;

public class WhitespaceReformattterTest {

    private static String INPUT_TEST_STRING = "\"广东省 高新技术 产品 出口\" 37.6亿 美元 , 同比 增长 34.8% , 占 全省 出口 总值 的 25.5% .";
    private static String OUTPUT_TEST_STRING = "\" 广东省 高新技术 产品 出口 \" 37.6亿 美元 , 同比 增长 34.8 % , 占 全省 出口 总值 的 25.5 % .";

    @Test
    public void resegmentationWorks() {

	List<Pair<String>> padding = new ArrayList<Pair<String>>();

	padding.add(new Pair<String>("的25.5的", "的 25.5 的"));
	padding.add(new Pair<String>("的25.5的的25.5的", "的 25.5 的的 25.5 的"));
	padding.add(new Pair<String>("的 25.5% .", "的 25.5 % ."));
	padding.add(new Pair<String>("的 25.5 %.", "的 25.5 % ."));
	padding.add(new Pair<String>("的 25.5%.", "的 25.5 % ."));
	padding.add(new Pair<String>("的 25.5 % .", "的 25.5 % ."));
	padding.add(new Pair<String>("\"的 25.5 % .\"", "\" 的 25.5 % . \""));
	padding.add(new Pair<String>("'的 25.5 % .'", "' 的 25.5 % . '"));
	assertPadding(padding);
    }


    @Test
    public void tokenizationWorks() {

	SplitpatternPreservingStringTokenizer stringTokenizer = SplitpatternPreservingStringTokenizer
		.createSplitPatternPreservingStringTokenizer(WhitespaceReformatter.ALL_NUMBERS_MATCHING_REGULAR_EXPRESSION_STRING);

	assertEquals(Arrays.asList("abcde"),
		stringTokenizer.tokenizeReturnSimpleStringList("abcde"));
	assertEquals(Arrays.asList("a", "1", "b", "1", "c", "1", "d", "1", "e"),
		stringTokenizer.tokenizeReturnSimpleStringList("a1b1c1d1e"));
	assertEquals(Arrays.asList("1", "a", "1", "b", "1", "c", "1", "d", "1", "e", "1"),
		stringTokenizer.tokenizeReturnSimpleStringList("1a1b1c1d1e1"));
	assertEquals(Arrays.asList("abc", "1", "de"),
		stringTokenizer.tokenizeReturnSimpleStringList("abc1de"));
	assertEquals(Arrays.asList("abcde", "1"),
		stringTokenizer.tokenizeReturnSimpleStringList("abcde1"));
	assertEquals(Arrays.asList("1", "abcde"),
		stringTokenizer.tokenizeReturnSimpleStringList("1abcde"));
	assertEquals(Arrays.asList(), stringTokenizer.tokenize(""));

    }

    private void assertPadding(List<Pair<String>> padding) {
	for (Pair<String> pair : padding) {

	    String input = pair.getFirst();
	    String expectedOutput = pair.getSecond();

	    WhitespaceReformatter whitespaceReformatter = new WhitespaceReformatter();
	    String output = whitespaceReformatter.reformatWhiteSpace(input);
	    System.out.println("input: \"" + input + "\"");
	    System.out.println("output: \"" + output + "\"");
	    System.out.println("expectedOutput: \"" + expectedOutput + "\"");
	    assertEquals(expectedOutput, output);

	}
    }

    @Test
    public void canMatchNumbersCorrectly() {

	String regex = WhitespaceReformatter.ALL_NUMBERS_MATCHING_REGULAR_EXPRESSION_STRING;

	assertTrue("1".matches(regex));
	assertTrue("1,000".matches(regex));
	assertTrue("1,000.00".matches(regex));
	assertTrue("10.00".matches(regex));
	assertTrue("100000000000.00".matches(regex));
	assertFalse("100000000000a00".matches(regex));

	assertFalse("0.0.0".matches(regex));

	assertTrue("22".matches(regex));
	assertTrue("22.33".matches(regex));

	assertTrue("1,000.0".matches(regex));

	assertFalse(",000.0".matches(regex));
	assertFalse("000.".matches(regex));
	assertFalse(".".matches(regex));
	assertFalse(",".matches(regex));
	assertFalse(",0.".matches(regex));

    }

}
