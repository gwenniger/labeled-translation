package unit_tests;

import grammarExtraction.translationRuleTrie.BasicCountItem;
import grammarExtraction.translationRuleTrie.CountItem;
import grammarExtraction.translationRuleTrie.MTRuleTrieNode;
import grammarExtraction.translationRuleTrie.RuleCountsTable;

import org.junit.Test;

import util.MemoryTestBench;

public class MTRuleTrieNodeTest 
{

	  @Test(expected=AssertionError.class)
	  public void testAssertionsEnabled() 
	  {
	    assert(false);
	  }

	  /*
	  @Test
	  public void testMTRuleTrieNodeSize()
	  {
		    Runtime r = Runtime.getRuntime();
		    
		     for(int i = 0; i < 100; i++)
		     {	 
		    	 r.gc();
		     }	 
			 System.out.println("Before node creation: " + EbitgCorpusComputationMultiThread.heapSize());
			 
			 List<MTRuleTrieNode> nodeList = new ArrayList<MTRuleTrieNode>();
			 for(int i = 0; i < 10000; i ++)
			 {	 
				 nodeList.add(MTRuleTrieNode.createMTRuleTrieNode(-1,null));
			 }	 
			  for(int i = 0; i < 100; i++)
			 {	 
				  r.gc();
			 }
			 System.out.println("After node creation: " + EbitgCorpusComputationMultiThread.heapSize());
			 nodeList = null;
			 for(int i = 0; i < 100; i++)
			 {	 
				 r.gc();
			 }
			 System.out.println("After node null settin: " + EbitgCorpusComputationMultiThread.heapSize());
	  }
	  */
	  @Test
	  public void  testMTRuleTrieNodeSize2()
		{
			
			MemoryTestBench testBench = new MemoryTestBench();
			MTRuleTrieNode mtRuleTrieNode = MTRuleTrieNode.createMTRuleTrieNode(0,null);
			testBench.showMemoryUsage(mtRuleTrieNode);
			RuleCountsTable ruleCountsTable = RuleCountsTable.createRuleCountsTable();
			testBench.showMemoryUsage(ruleCountsTable);
			CountItem countItem = new BasicCountItem(null);
			testBench.showMemoryUsage(countItem);
			//ArrayListFactory arrayListFactory = new ArrayListFactory();
			//testBench.showMemoryUsage(arrayListFactory);
			
		}
}
