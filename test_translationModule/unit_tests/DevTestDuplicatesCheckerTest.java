package unit_tests;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import junit.framework.Assert;
import org.junit.Test;
import datapreparation.DevTestDuplicatesChecker;
import datapreparation.DuplicatesChecker;
import util.FileUtil;
import util.Pair;

public class DevTestDuplicatesCheckerTest {

    private static List<String> file1StringList = Arrays.asList("de man", "de vrouw", "de man",
	    "paard", "koning olifiant");
    private static List<String> file2StringList = Arrays.asList("de tijger", "het meisje",
	    "de jongen", "kameel", "de man", "kameel");

    private static String TEST_FOLDER_PATH = "./DuplicatesCheckerTestFolder/";
    private static String DEV_TEST_FILE_PATH = TEST_FOLDER_PATH + "devDuplicatesTestFile.txt";
    private static String TEST_TEST_FILE_PATH = TEST_FOLDER_PATH + "testDuplicatesTestFile.txt";

    private void createTestFiles() {
	FileUtil.createFolderIfNotExisting(TEST_FOLDER_PATH);
	FileUtil.writeLinesListToFile(file1StringList, DEV_TEST_FILE_PATH);
	FileUtil.writeLinesListToFile(file2StringList, TEST_TEST_FILE_PATH);
    }

    private void checkExpectedFrequencyDevTestPartCorrect(Map<String, Pair<Integer>> duplicatesMap,
	    String string, int expectedDevPartFrequency, int expectedTestPartFrequency) {
	Pair<Integer> value = duplicatesMap.get(string);
	Assert.assertEquals(new Pair<Integer>(expectedDevPartFrequency, expectedTestPartFrequency),
		value);
    }

    private void checkExpectedFrequencyCorrect(Map<String, Integer> duplicatesMap,
	    String string, Integer expectedFrequency) {
	Integer value = duplicatesMap.get(string);
	Assert.assertEquals(expectedFrequency, value);
    }

    @Test
    public void testDevTestAcrossDuplicatesCorrect() {
	createTestFiles();
	DevTestDuplicatesChecker devTestDuplicatesChecker = DevTestDuplicatesChecker
		.createDuplicatesChecker(DEV_TEST_FILE_PATH, TEST_TEST_FILE_PATH);
	devTestDuplicatesChecker.showDuplicates();
	Map<String, Pair<Integer>> duplicatesMap = devTestDuplicatesChecker
		.computeDevTestDuplicatesMap();
	checkExpectedFrequencyDevTestPartCorrect(duplicatesMap, "de man", 2, 1);
	Assert.assertEquals(1, duplicatesMap.size());
    }

    @Test
    public void devInternalDuplicatesCorrect() {
	DuplicatesChecker duplicatesChecker = DuplicatesChecker.createDuplicatesChecker(DEV_TEST_FILE_PATH);
	Map<String,Integer> duplicatesMap = duplicatesChecker.computeDuplicatesFrequencyMap();
	checkExpectedFrequencyCorrect(duplicatesMap, "de man", 2);
	Assert.assertEquals(1, duplicatesMap.size());
    }

    @Test
    public void testInternalDuplicatesCorrect() {
	DuplicatesChecker duplicatesChecker = DuplicatesChecker.createDuplicatesChecker(TEST_TEST_FILE_PATH);
	Map<String,Integer> duplicatesMap = duplicatesChecker.computeDuplicatesFrequencyMap();
	checkExpectedFrequencyCorrect(duplicatesMap, "kameel", 2);
	Assert.assertEquals(1, duplicatesMap.size());
    }
}
