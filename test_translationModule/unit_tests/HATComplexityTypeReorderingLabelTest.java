package unit_tests;

import junit.framework.Assert;
import org.junit.Test;
import util.Span;
import extended_bitg.EbitgChartBuilderBasic;
import extended_bitg.EbitgChartEntry.HATComplexityType;

public class HATComplexityTypeReorderingLabelTest {

    private static final int MAX_ALLOWED_INFERENCES_PERNODE = 100000;
    private static boolean USE_GREEDY_NULL_BINDING = true;
    private static boolean USE_CACHING = true;

    // Complicated alignment containing all sorts of sub alignments (BITT, MONO,
    // HAT)
    private static String TEST_SOURCE_SENTENCE = "s0 s1 s2 s3 s4 s5 s6 s7 s8 s9";
    private static String TEST_TARGET_SENTENCE = "s0 s1 s2 s3 s4 s5 s6 s7 s8 s9";
    private static String TEST_ALIGNMENT = "0-0 1-1 2-2 3-3 4-5 5-4 6-6 6-9 7-7 7-8 8-7 8-8 9-9";

    // The CLASSICAL PET Example With mononotne tail
    private static String TEST_SOURCE_SENTENCE2 = "s0 s1 s2 s3 s4 s5";
    private static String TEST_TARGET_SENTENCE2 = "t0 t1 t2 t3 t4 t5";
    private static String TEST_ALIGNMENT2 = "0-1 1-3 2-0 3-2 4-4 5-5";

    private void assertChartEntryHATComplexityTypeIsSpecifiedType(
	    EbitgChartBuilderBasic ebitgChartBuilderBasic, Span span,
	    HATComplexityType hatComplexityType) {
	HATComplexityType actualHATComplexityType = ebitgChartBuilderBasic.getChart()
		.getChartEntry(span.getFirst(), span.getSecond()).getHATComplexityType();
	Assert.assertEquals(hatComplexityType, actualHATComplexityType);
    }

    @Test
    public void test() {
	EbitgChartBuilderBasic ebitgChartBuilderBasic = EbitgChartBuilderBasic
		.createCompactHATsEbitgChartBuilder(TEST_SOURCE_SENTENCE, TEST_TARGET_SENTENCE,
			TEST_ALIGNMENT, MAX_ALLOWED_INFERENCES_PERNODE, USE_GREEDY_NULL_BINDING,
			USE_CACHING);
	ebitgChartBuilderBasic.findDerivationsAlignment();

	// Checking for the simple spans
	assertChartEntryHATComplexityTypeIsSpecifiedType(ebitgChartBuilderBasic, new Span(0, 0),
		HATComplexityType.ATOMIC);
	assertChartEntryHATComplexityTypeIsSpecifiedType(ebitgChartBuilderBasic, new Span(1, 1),
		HATComplexityType.ATOMIC);
	assertChartEntryHATComplexityTypeIsSpecifiedType(ebitgChartBuilderBasic, new Span(0, 1),
		HATComplexityType.BITT_MONO);
	assertChartEntryHATComplexityTypeIsSpecifiedType(ebitgChartBuilderBasic, new Span(2, 3),
		HATComplexityType.BITT_MONO);
	assertChartEntryHATComplexityTypeIsSpecifiedType(ebitgChartBuilderBasic, new Span(4, 5),
		HATComplexityType.BITT_INVERTED);
	assertChartEntryHATComplexityTypeIsSpecifiedType(ebitgChartBuilderBasic, new Span(6, 9),
		HATComplexityType.HAT);

	// Any span including the HAT node should have HATComplexity Type = HAT
	assertChartEntryHATComplexityTypeIsSpecifiedType(ebitgChartBuilderBasic, new Span(4, 9),
		HATComplexityType.HAT);
	assertChartEntryHATComplexityTypeIsSpecifiedType(ebitgChartBuilderBasic, new Span(3, 9),
		HATComplexityType.HAT);
	assertChartEntryHATComplexityTypeIsSpecifiedType(ebitgChartBuilderBasic, new Span(3, 9),
		HATComplexityType.HAT);
	assertChartEntryHATComplexityTypeIsSpecifiedType(ebitgChartBuilderBasic, new Span(2, 9),
		HATComplexityType.HAT);
	assertChartEntryHATComplexityTypeIsSpecifiedType(ebitgChartBuilderBasic, new Span(1, 9),
		HATComplexityType.HAT);
	assertChartEntryHATComplexityTypeIsSpecifiedType(ebitgChartBuilderBasic, new Span(0, 9),
		HATComplexityType.HAT);
    }

    @Test
    public void testWithPET() {
	EbitgChartBuilderBasic ebitgChartBuilderBasic = EbitgChartBuilderBasic
		.createCompactHATsEbitgChartBuilder(TEST_SOURCE_SENTENCE2, TEST_TARGET_SENTENCE2,
			TEST_ALIGNMENT2, MAX_ALLOWED_INFERENCES_PERNODE, USE_GREEDY_NULL_BINDING,
			USE_CACHING);
	ebitgChartBuilderBasic.findDerivationsAlignment();

	// Checking for the simple spans
	assertChartEntryHATComplexityTypeIsSpecifiedType(ebitgChartBuilderBasic, new Span(0, 0),
		HATComplexityType.ATOMIC);
	assertChartEntryHATComplexityTypeIsSpecifiedType(ebitgChartBuilderBasic, new Span(1, 1),
		HATComplexityType.ATOMIC);
	assertChartEntryHATComplexityTypeIsSpecifiedType(ebitgChartBuilderBasic, new Span(2, 2),
		HATComplexityType.ATOMIC);
	assertChartEntryHATComplexityTypeIsSpecifiedType(ebitgChartBuilderBasic, new Span(3, 3),
		HATComplexityType.ATOMIC);
	assertChartEntryHATComplexityTypeIsSpecifiedType(ebitgChartBuilderBasic, new Span(4, 4),
		HATComplexityType.ATOMIC);
	assertChartEntryHATComplexityTypeIsSpecifiedType(ebitgChartBuilderBasic, new Span(5, 5),
		HATComplexityType.ATOMIC);
	assertChartEntryHATComplexityTypeIsSpecifiedType(ebitgChartBuilderBasic, new Span(0, 3),
		HATComplexityType.PET);
	assertChartEntryHATComplexityTypeIsSpecifiedType(ebitgChartBuilderBasic, new Span(4, 5),
		HATComplexityType.BITT_MONO);
	assertChartEntryHATComplexityTypeIsSpecifiedType(ebitgChartBuilderBasic, new Span(0, 5),
		HATComplexityType.PET);

    }

}
