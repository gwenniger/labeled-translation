package unit_tests;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import util.ConfigFile;
import util.FileUtil;
import datapreparation.BigSentenceAlignedFilesCreater;
import datapreparation.ChineseDataPreparationConfigCreater;
import datapreparation.FileEncodingConverter;
import datapreparation.SegmentIndicesPairingExtracter;
import datapreparation.SegmentIndicesPairingExtracter.DocSegmentPairing;
import datapreparation.SentenceAlignedFilesCreaterFullCorpus;
import datapreparation.SegmentIndicesPairingExtracter.SegmentPairing;
import datapreparation.SegmentListExtracter;
import datapreparation.SegmentListExtracter.DocSegments;
import datapreparation.SentenceAlignedFilesCreater;

public class SegmentRecombinationClassesTest {
    // private static final String CHINESE_ENCODING = "big5";
    // private static final String ENGLISH_ENCODING = "utf-8";
    private static final String SOURCE_SEGMENT_FILE_PATH = "./datapreparationTestData/HK_Hansards/English/HKH19851030.sgm";
    private static final String TARGET_SEGMENT_FILE_PATH = "./datapreparationTestData/HK_Hansards/Chinese/HKH19851030.sgm";
    private static final String ALIGNMENT_FILE_PATH = "./datapreparationTestData/HK_Hansards/alignment/HKH19851030.sgm";

    private static final String SOURCE_SEGMENT_FILE_PATH2 = "./datapreparationTestData/HK_Laws/English/HKLCap_0050A.sgm";
    private static final String TARGET_SEGMENT_FILE_PATH2 = "./datapreparationTestData/HK_Laws/Chinese/HKLCap_0050A.sgm";
    private static final String ALIGNMENT_FILE_PATH2 = "./datapreparationTestData/HK_Laws/alignment/HKLCap_0050A.sgm";

    private static final String SOURCE_SEGMENT_FOLDER_SUB_PATH = "/HK_Hansards/English/";
    private static final String TARGET_SEGMENT_FOLDER_SUB_PATH = "/HK_Hansards/Chinese/";
    private static final String ALIGNMENT_FOLDER_SUB_PATH = "/HK_Hansards/alignment/";
    private static final String SOURCE_SEGMENT_FOLDER_SUB_PATH2 = "/HK_Laws/English/";

    private static final String SOURCE_SEGMENT_1 = "HONG KONG LEGISLATIVE COUNCIL  30 October 1985      1";
    private static final String SOURCE_SEGMENT_99 = "The quiet warmth of his personality and his selfless dedication to public service will be greatly missed.";
    private static final String SOUCRE_SEGMENT_614 = "We shall continue with localisation, taking care to ensure. however, that efficiency and morale are not jeopardised.";

    private static final String DATA_PREPRARTION_TEST_OUTPUT_DIRECTORY = "./datapreparationTestOutputDirectory";
    private static final String SOURCE_OUTPUT_FILE_PATH = DATA_PREPRARTION_TEST_OUTPUT_DIRECTORY
	    + "/" + "sourceSentenceAligned.txt";
    private static final String TARGET_OUTPUT_FILE_PATH = DATA_PREPRARTION_TEST_OUTPUT_DIRECTORY
	    + "/" + "targetSentenceAligned.txt";

    private static final String BIG_SOURCE_OUTPUT_FILE_PATH = DATA_PREPRARTION_TEST_OUTPUT_DIRECTORY
	    + "/" + "sourceSentenceAlignedBIG.txt";
    private static final String BIG_TARGET_OUTPUT_FILE_PATH = DATA_PREPRARTION_TEST_OUTPUT_DIRECTORY
	    + "/" + "targetSentenceAlignedBIG.txt";
    private static final String FULL_CORPUS_SOURCE_OUTPUT_FILE_PATH = DATA_PREPRARTION_TEST_OUTPUT_DIRECTORY
	    + "/" + "sourceSentenceAligned_FULL_CORPUS.txt";
    private static final String FULL_CORPUS_SOURCE_NORMALIZED_PUNCTUATION_OUTPUT_FILE_PATH = DATA_PREPRARTION_TEST_OUTPUT_DIRECTORY
	    + "/" + "sourceSentenceAlignednNormalizedPunctuation_FULL_CORPUS.txt";
    private static final String FULL_CORPUS_TARGET_OUTPUT_FILE_PATH = DATA_PREPRARTION_TEST_OUTPUT_DIRECTORY
	    + "/" + "targetSentenceAligned_FULL_CORPUS.txt";
    private static final String FULL_CORPUS_TARGET_NORMALIZED_PUNCTUATION_OUTPUT_FILE_PATH = DATA_PREPRARTION_TEST_OUTPUT_DIRECTORY
	    + "/" + "targetSentenceAlignedNormalizedPunctuation_FULL_CORPUS.txt";
    private static final String FULL_CORPUS_EXTRACTION_CONFIG_FILE_PATH = DATA_PREPRARTION_TEST_OUTPUT_DIRECTORY
	    + "/" + "fullCorpusExtractionConfig.txt";

    private static final String ENCODING_CONVERSION_TEST_FILE_PATH = SOURCE_SEGMENT_FOLDER_SUB_PATH2
	    + "HKLCap_0001A.sgm";
    private static final String ENCODING_CONVERSION_TEST_OUTPUT_FILE_PATH = DATA_PREPRARTION_TEST_OUTPUT_DIRECTORY
	    + "/" + "HKLCap_0001A-EncodingChanged.sgm";
    private static final String EXPECTED_ENCODING_STRING = "iso-8859-1";
    private static final String EXPECTED_ENCODING_STRING_OUTPUT = "utf-8";
    private static final String ENCODING_CONVERSION_TEST_INPUT_FILE_FOLDER_PATH = "./datapreparationTestData/";
    private static final String ENCODING_CONVERSION_TEST_OUTPUT_FILE_FOLDER_PATH = DATA_PREPRARTION_TEST_OUTPUT_DIRECTORY
	    + "/" + "Chinese_Data_Converted";

    private static final String DEFAULT_INPUT_ENCODING = "big5";// "big5hkscs";

    /**
     * Check for some selected segments that we have the right one in the right
     * location
     * 
     * @param sourceSegmentList
     */
    private void assertSelectedSourceSegmentsAreCorrect(DocSegments sourceSegmentList) {

	Assert.assertEquals(SOURCE_SEGMENT_1, sourceSegmentList.getSegment(1));
	Assert.assertEquals(SOURCE_SEGMENT_99, sourceSegmentList.getSegment(99));
	Assert.assertEquals(SOUCRE_SEGMENT_614, sourceSegmentList.getSegment(614));
    }

    // @Test
    public void testSegmentListExtracter() {
	SegmentListExtracter sourceSegmentListExtracter = SegmentListExtracter
		.createSegmentListExtracter(SOURCE_SEGMENT_FILE_PATH);
	List<DocSegments> sourceSegmentList = sourceSegmentListExtracter.extractDocSegmentList();
	System.out.println("Source Segments: ");
	sourceSegmentList.get(0).printSegments();

	SegmentListExtracter targetSegmentListExtractor = SegmentListExtracter
		.createSegmentListExtracter(TARGET_SEGMENT_FILE_PATH);
	List<DocSegments> targetSegmentList = targetSegmentListExtractor.extractDocSegmentList();
	System.out.println("\n\nTarget Segments: ");
	targetSegmentList.get(0).printSegments();

	assertSelectedSourceSegmentsAreCorrect(sourceSegmentList.get(0));
    }

    // @Test
    public void testSegmentIndexPairingExtracter() {
	SegmentIndicesPairingExtracter segmentIndicesPairingExtracter = SegmentIndicesPairingExtracter
		.createSegmentIndicesPairingExtracter(ALIGNMENT_FILE_PATH);
	List<DocSegmentPairing> docSegmentPairing = segmentIndicesPairingExtracter
		.extractSegmentIndicesPairingList();
	System.out.println("\n\n testSegmentIndexPairingExtracter: \n\n");
	List<SegmentPairing> segmentPairingList = docSegmentPairing.get(0)
		.getSegmentIndicesPairingList();
	for (SegmentPairing segmentPairing : segmentPairingList) {
	    System.out.println(segmentPairing);
	}
	// Try out some random pairs are correctly contained or not contained
	Assert.assertFalse(segmentPairingList.contains(new SegmentPairing(null, null)));
	Assert.assertTrue(segmentPairingList.contains(new SegmentPairing(Arrays.asList(2, 3, 4),
		Arrays.asList(2, 3))));
	Assert.assertTrue(segmentPairingList.contains(new SegmentPairing(Arrays.asList(574), Arrays
		.asList(479))));
	Assert.assertFalse(segmentPairingList.contains(new SegmentPairing(Arrays.asList(574),
		Arrays.asList(478))));
	Assert.assertTrue(segmentPairingList.contains(new SegmentPairing(Arrays.asList(630),
		new ArrayList<Integer>())));
    }

    // @Test
    public void testSentenceAlignedFilesCreater() {
	FileUtil.createFolderIfNotExisting(DATA_PREPRARTION_TEST_OUTPUT_DIRECTORY);
	try {
	    BufferedWriter sourceOutputWriter = new BufferedWriter(new FileWriter(
		    SOURCE_OUTPUT_FILE_PATH));
	    BufferedWriter targetOutputWriter = new BufferedWriter(new FileWriter(
		    TARGET_OUTPUT_FILE_PATH));
	    SentenceAlignedFilesCreater sentenceAlignedFilesCreater = SentenceAlignedFilesCreater
		    .createSentenceAlignedFilesCreater(SOURCE_SEGMENT_FILE_PATH,
			    TARGET_SEGMENT_FILE_PATH, ALIGNMENT_FILE_PATH, sourceOutputWriter,
			    targetOutputWriter, false);
	    sentenceAlignedFilesCreater.writeSourceAndTargetOutputToWriters();
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    @Test
    public void testSentenceAlignedFilesCreater2() {
	FileUtil.createFolderIfNotExisting(DATA_PREPRARTION_TEST_OUTPUT_DIRECTORY);
	try {
	    BufferedWriter sourceOutputWriter = new BufferedWriter(new FileWriter(
		    SOURCE_OUTPUT_FILE_PATH));
	    BufferedWriter targetOutputWriter = new BufferedWriter(new FileWriter(
		    TARGET_OUTPUT_FILE_PATH));
	    SentenceAlignedFilesCreater sentenceAlignedFilesCreater = SentenceAlignedFilesCreater
		    .createSentenceAlignedFilesCreater(SOURCE_SEGMENT_FILE_PATH2,
			    TARGET_SEGMENT_FILE_PATH2, ALIGNMENT_FILE_PATH2, sourceOutputWriter,
			    targetOutputWriter, false);
	    sentenceAlignedFilesCreater.writeSourceAndTargetOutputToWriters();
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    // @Test
    public void testBigSentenceAlignedFilesCreater() {
	BigSentenceAlignedFilesCreater bigSentenceAlignedFilesCreater = BigSentenceAlignedFilesCreater
		.createBigSentenceAlignedFilesCreater(SOURCE_SEGMENT_FOLDER_SUB_PATH,
			TARGET_SEGMENT_FOLDER_SUB_PATH, ALIGNMENT_FOLDER_SUB_PATH,
			BIG_SOURCE_OUTPUT_FILE_PATH, BIG_TARGET_OUTPUT_FILE_PATH);
	bigSentenceAlignedFilesCreater.writeBigSentenceAlignedFiles();
    }

    /*
    // @Test
    public void testSentenceAlignedFilesCreaterFullCorpus() {
	ChineseDataPreparationConfigCreater chineseDataPreparationConfigCreater = ChineseDataPreparationConfigCreater
		.createTestChineseDataConfigCreater("./datapreparationTestData");
	chineseDataPreparationConfigCreater.writeFullCorpusExtractionConfig(
		FULL_CORPUS_EXTRACTION_CONFIG_FILE_PATH, FULL_CORPUS_SOURCE_OUTPUT_FILE_PATH,
		FULL_CORPUS_SOURCE_NORMALIZED_PUNCTUATION_OUTPUT_FILE_PATH,
		FULL_CORPUS_TARGET_OUTPUT_FILE_PATH,
		FULL_CORPUS_TARGET_NORMALIZED_PUNCTUATION_OUTPUT_FILE_PATH,null,null, 1);
	ConfigFile configFile;
	try {
	    configFile = new ConfigFile(FULL_CORPUS_EXTRACTION_CONFIG_FILE_PATH);
	    SentenceAlignedFilesCreaterFullCorpus sentenceAlignedFilesCreaterFullCorpus = SentenceAlignedFilesCreaterFullCorpus
		    .createSentenceAlignedFilesCreateFullCorpus(configFile);
	    sentenceAlignedFilesCreaterFullCorpus.extractSentenceAlignedSourceAndTargetFullCorpus();
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}

    }*/

    // @Test
    public void testGetEncodingTypeMethod() {
	String result = FileEncodingConverter
		.getEncodingStringForFile(ENCODING_CONVERSION_TEST_FILE_PATH);
	Assert.assertEquals(EXPECTED_ENCODING_STRING, result);
    }

    // @Test
    public void testPerformConversion() {
	try {
	    FileEncodingConverter.performConversion(ENCODING_CONVERSION_TEST_FILE_PATH,
		    ENCODING_CONVERSION_TEST_OUTPUT_FILE_PATH, EXPECTED_ENCODING_STRING_OUTPUT,
		    DEFAULT_INPUT_ENCODING);
	    String reEncodedFileEncoding = FileEncodingConverter
		    .getEncodingStringForFile(ENCODING_CONVERSION_TEST_OUTPUT_FILE_PATH);
	    System.out.println("Expected encoding String: \"" + EXPECTED_ENCODING_STRING_OUTPUT
		    + "\"");
	    System.out.println("Actual encoding String: \"" + reEncodedFileEncoding + "\"");
	    Assert.assertEquals(EXPECTED_ENCODING_STRING_OUTPUT, reEncodedFileEncoding);
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    // @Test
    public void testEntireCorpusEncodingConversion() {
	FileEncodingConverter fileEncodingConverter = FileEncodingConverter
		.createFileEncodingConverter(ENCODING_CONVERSION_TEST_INPUT_FILE_FOLDER_PATH,
			ENCODING_CONVERSION_TEST_OUTPUT_FILE_FOLDER_PATH, "utf8",
			DEFAULT_INPUT_ENCODING);
	fileEncodingConverter.performConversion();
    }

    /*
    // @Test
    public void testSentenceAlignedFilesCreaterFullCorpusReEncodedData() {
	testEntireCorpusEncodingConversion();
	ChineseDataPreparationConfigCreater chineseDataPreparationConfigCreater = ChineseDataPreparationConfigCreater
		.createTestChineseDataConfigCreater(ENCODING_CONVERSION_TEST_OUTPUT_FILE_FOLDER_PATH
			+ "/");
	chineseDataPreparationConfigCreater.writeFullCorpusExtractionConfig(
		FULL_CORPUS_EXTRACTION_CONFIG_FILE_PATH, FULL_CORPUS_SOURCE_OUTPUT_FILE_PATH,
		FULL_CORPUS_SOURCE_NORMALIZED_PUNCTUATION_OUTPUT_FILE_PATH,
		FULL_CORPUS_TARGET_OUTPUT_FILE_PATH,
		FULL_CORPUS_TARGET_NORMALIZED_PUNCTUATION_OUTPUT_FILE_PATH,null,null, 1);
	ConfigFile configFile;
	try {
	    configFile = new ConfigFile(FULL_CORPUS_EXTRACTION_CONFIG_FILE_PATH);
	    SentenceAlignedFilesCreaterFullCorpus sentenceAlignedFilesCreaterFullCorpus = SentenceAlignedFilesCreaterFullCorpus
		    .createSentenceAlignedFilesCreateFullCorpus(configFile);
	    sentenceAlignedFilesCreaterFullCorpus.extractSentenceAlignedSourceAndTargetFullCorpus();
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }*/

}
