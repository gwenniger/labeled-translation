package unit_tests;


import java.util.Arrays;
import junit.framework.Assert;
import mt_pipeline.mbrInterface.WeightedTranslation;
import org.junit.Test;

public class WeightedTranslationTest {

	@Test
	public void testLogSumExponent() {
		double logProb1 = -10;
		double logProb2 = -11;

		double logSumExponentFormula = WeightedTranslation.logSumExponent(Arrays.asList(logProb1, logProb2));
		double logSumExponentManual = Math.log(Math.exp(logProb1) + Math.exp(logProb2));

		System.out.println("\n\n---------- Test testLogSumExponent ---------- \n\n");
		
		System.out.println("Test equality with manual computation for normal values");
		
		System.out.println("logProb1:" + logProb1);
		System.out.println("logProb2:" + logProb1);
		System.out.println("logSumExponentFormula:" + logSumExponentFormula);
		System.out.println("logSumExponentManual:" + logSumExponentManual);
		Assert.assertEquals(logSumExponentManual, logSumExponentFormula);
		
		System.out.println("Test working with very small values");



		logProb1 = -1000;
		logProb2 = -1001;
		logSumExponentFormula = WeightedTranslation.logSumExponent(Arrays.asList(logProb1, logProb2));
		logSumExponentManual = Math.log(Math.exp(logProb1) + Math.exp(logProb2));
		System.out.println("logProb1:" + logProb1);
		System.out.println("logProb2:" + logProb1);
		System.out.println("logSumExponentFormula:" + logSumExponentFormula);
		System.out.println("logSumExponentManual:" + logSumExponentManual);
		Assert.assertTrue(logSumExponentFormula > logProb1);
		Assert.assertTrue(logSumExponentFormula > logProb2);
		// In contrast the manual computation fails due to machine precision: negative infinity
		Assert.assertTrue(Double.isInfinite(logSumExponentManual));
	}

	@Test
	public void testLogSumExponent2() {
		System.out.println("\n\n---------- Test testLogSumExponent2 ---------- \n\n");
		
		double logProb1 = -10;
		double logProb2 = -11;
		
		double logSumExponentFormula = WeightedTranslation.logSumExponent(Arrays.asList(logProb1, logProb2));
		double logSumExponentManual = Math.log(Math.exp(logProb1) + Math.exp(logProb2));
		double logSumExponentFormulaBase10 = WeightedTranslation.logSumExponentBase10(Arrays.asList(logProb1, logProb2));
		double logSumExponentManualBase10 = Math.log10((Math.pow(10,logProb1) + Math.pow(10,logProb2)));
		System.out.println("logSumExponentManual:: " + logSumExponentManual);
		System.out.println("logSumExponentFormula:: " + logSumExponentFormula);
		System.out.println("logSumExponentManualBase10: " + logSumExponentManualBase10);
		System.out.println("logSumExponentFormulaBase10: " + logSumExponentFormulaBase10);
		Assert.assertEquals(logSumExponentManual, logSumExponentFormula);
		Assert.assertEquals(logSumExponentManualBase10, logSumExponentFormulaBase10);
		
		
		logProb1 = Math.log10(0.1);
		logProb2 = Math.log10(0.15);
		logSumExponentFormula = WeightedTranslation.logSumExponent(Arrays.asList(logProb1, logProb2));
		logSumExponentManual = Math.log(Math.exp(logProb1) + Math.exp(logProb2));
		logSumExponentFormulaBase10 = WeightedTranslation.logSumExponentBase10(Arrays.asList(logProb1, logProb2));
		logSumExponentManualBase10 = Math.log10((Math.pow(10,logProb1) + Math.pow(10,logProb2)));
		System.out.println("logSumExponentManual:: " + logSumExponentManual);
		System.out.println("logSumExponentFormula:: " + logSumExponentFormula);
		System.out.println("logSumExponentManualBase10: " + logSumExponentManualBase10);
		System.out.println("logSumExponentFormulaBase10: " + logSumExponentFormulaBase10);
		System.out.println("10^logSumExponentManual: " + Math.pow(10,logSumExponentManual));
		System.out.println("10^logSumExponentFormula: " + Math.pow(10,logSumExponentFormula));
		System.out.println("10^logSumExponentManualBase10: " + Math.pow(10,logSumExponentManualBase10));
		System.out.println("10^logSumExponentFormulaBase10: " + Math.pow(10,logSumExponentFormulaBase10));
		Assert.assertEquals(0.25, Math.pow(10,logSumExponentManualBase10));
		Assert.assertEquals(0.25, Math.pow(10,logSumExponentFormulaBase10));
		
		
		logProb1 = -345.623;
		logProb2 = -348.092;
		logSumExponentFormula = WeightedTranslation.logSumExponent(Arrays.asList(logProb1, logProb2));
		logSumExponentManual = Math.log(Math.exp(logProb1) + Math.exp(logProb2));
		logSumExponentFormulaBase10 = WeightedTranslation.logSumExponentBase10(Arrays.asList(logProb1, logProb2));
		logSumExponentManualBase10 = Math.log10((Math.pow(10,logProb1) + Math.pow(10,logProb2)));
		System.out.println("logSumExponentManual:: " + logSumExponentManual);
		System.out.println("logSumExponentFormula:: " + logSumExponentFormula);
		System.out.println("logSumExponentManualBase10: " + logSumExponentManualBase10);
		System.out.println("logSumExponentFormulaBase10: " + logSumExponentFormulaBase10);
		System.out.println("10^logSumExponentManual: " + Math.pow(10,logSumExponentManual));
		System.out.println("10^logSumExponentFormula: " + Math.pow(10,logSumExponentFormula));
		System.out.println("10^logSumExponentManualBase10: " + Math.pow(10,logSumExponentManualBase10));
		System.out.println("10^logSumExponentFormulaBase10: " + Math.pow(10,logSumExponentFormulaBase10));

	}
}
