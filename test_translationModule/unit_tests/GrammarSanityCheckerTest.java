package unit_tests;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import mt_pipeline.GrammarSanityChecker;
import util.FileUtil;

public class GrammarSanityCheckerTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();
    
    private static String TEST_FOLDER_PATH = "./GrammarSanityCheckerTestFolder/";
    private static String TEST_GRAMMAR_FILE_PATH_SANE = TEST_FOLDER_PATH + "TestGrammarSane.txt";
    private static String TEST_GRAMMAR_FILE_PATH_INSANE = TEST_FOLDER_PATH
	    + "TestGrammarInsane.txt";
    private static String TEST_GRAMMAR_FILE_PATH_INSANE_TWO = TEST_FOLDER_PATH
	    + "TestGrammarInsane2.txt";

    private static String RULE1 = "[DET:NOUN<<>>MONO] ||| die [NOUN:ADP<<>>MONO,1] beide programme ||| the [NOUN:ADP<<>>MONO,1] both programmes ||| -0.0 0.20411998 0.53147894 -0.0 4.5584683 1.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 3.0 2.629166 ";
    private static String RULE2 = "[UnknownWordPreNonterminal<<>>MONO] ||| [UnknownWord<<>>UnknownWord,1] ||| [UnknownWord<<>>UnknownWord,1] ||| 9.0 9.0 9.0 9.0 -0.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 0.0 2.7182817 ";
    private static String RULE3_SANE = "[ADJ:.<<>>MONO] ||| [ADJ:ADJ<<>>INV,1] abwegigkeit [.:.<<>>MONO,2] ||| [ADJ:ADJ<<>>INV,1] shift [.:.<<>>MONO,2] ||| -0.0 -0.0 -0.0 -0.0 3.1598785 1.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 1.0 1.1798865 ";
    private static String RULE3_INSANE = "[ADJ:.<<>>MONO] ||| [ADJ:ADJ<<>>INV,1] abwegigkeit [.:.<<>>MONO,2] ||| [ADJ:ADJ<<>>INV,1] shift [.:.<<>>MONO,1] ||| -0.0 -0.0 -0.0 -0.0 3.1598785 1.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 1.0 1.1798865 ";
    private static String RULE3_INSANE_TWO = "[ADJ:.<<>>MONO] ||| [ADJ:ADJ<<>>INV,1] abwegigkeit [.:.<<>>MONO,2] ||| [ADJ:ADJ<<>>INV,1] shift [.:.<<>>MONO,3] ||| -0.0 -0.0 -0.0 -0.0 3.1598785 1.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 1.0 1.1798865 ";

    private static final List<String> SANE_GRAMMAR_RULES = Arrays.asList(RULE1, RULE2, RULE3_SANE);
    private static final List<String> INSANE_GRAMMAR_RULES = Arrays.asList(RULE1, RULE2,
	    RULE3_INSANE);
    private static final List<String> INSANE_GRAMMAR_RULES_TWO = Arrays.asList(RULE1, RULE2,
	    RULE3_INSANE_TWO);

    private void createTestGrammars() {
	util.FileUtil.writeLinesListToFile(SANE_GRAMMAR_RULES, TEST_GRAMMAR_FILE_PATH_SANE);
	util.FileUtil.writeLinesListToFile(INSANE_GRAMMAR_RULES, TEST_GRAMMAR_FILE_PATH_INSANE);
	util.FileUtil.writeLinesListToFile(INSANE_GRAMMAR_RULES_TWO, TEST_GRAMMAR_FILE_PATH_INSANE_TWO);
    }

    private void setup() {
	FileUtil.createFolderIfNotExisting(TEST_FOLDER_PATH);
	createTestGrammars();
    }

    @Test
    public void testSaneGrammarSucceeds() {
	setup();
	GrammarSanityChecker grammarSanityChecker = new GrammarSanityChecker(
		TEST_GRAMMAR_FILE_PATH_SANE);
	grammarSanityChecker.checkGrammarRulesForErrors();
    }
    
    
    
    @Test
    public void testInsaneGrammarFails() {
	setup();
	GrammarSanityChecker grammarSanityChecker = new GrammarSanityChecker(
		TEST_GRAMMAR_FILE_PATH_INSANE);
	
	// See: http://stackoverflow.com/questions/156503/how-do-you-assert-that-a-certain-exception-is-thrown-in-junit-4-tests
	// Check that the expected RuntimeException is indeed thrown
	exception.expect(RuntimeException.class);
	grammarSanityChecker.checkGrammarRulesForErrors();
    }
    
    @Test
    public void testSecondInsaneGrammarFails() {
	setup();
	GrammarSanityChecker grammarSanityChecker = new GrammarSanityChecker(
		TEST_GRAMMAR_FILE_PATH_INSANE_TWO);
	
	// See: http://stackoverflow.com/questions/156503/how-do-you-assert-that-a-certain-exception-is-thrown-in-junit-4-tests
	// Check that the expected RuntimeException is indeed thrown
	exception.expect(RuntimeException.class);
	grammarSanityChecker.checkGrammarRulesForErrors();
    }

}
