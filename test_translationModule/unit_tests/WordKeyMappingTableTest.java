package unit_tests;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import junit.framework.Assert;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;

import org.junit.Test;

import alignment.AlignmentTriple;
import alignmentStatistics.MultiThreadCorpusDataGenerator;

public class WordKeyMappingTableTest {


	  @Test(expected=AssertionError.class)
	  public void testAssertionsEnabled() 
	  {
	    assert(false);
	  }
	
	
	@Test
	public void test() 
	{
		// Positive and negative lookahead, see
		// http://www.regular-expressions.info/lookaround.html
		String testString = "[]";
		Assert.assertTrue(testString.matches("\\[[^(,(?=\\d))]*\\]"));
		testString = ",u";
		Assert.assertTrue(testString.matches(",(?=u)."));
		testString = ",0";
		Assert.assertTrue(testString.matches(",(?=\\d)."));
		testString = ",0";
		Assert.assertFalse(testString.matches("(,(?!\\d)|[^,])."));
		testString = ",+";
		Assert.assertTrue(testString.matches("(,(?!\\d)|[^,])."));
		
		AlignmentTriple alignmentTriple = AlignmentTriple.createAlignmentTriple("the", "de", "0-0");
		WordKeyMappingTable wordKeyMappingTable = WordKeyMappingTable.createWordKeyMappingTable(alignmentTriple, 2,16);
		Assert.assertTrue(WordKeyMappingTable.isRuleLabel("[DT]"));
		Assert.assertTrue(!WordKeyMappingTable.isLabelOne("[DT]"));
		Assert.assertTrue(!WordKeyMappingTable.isLabelTwo("[DT]"));
		Assert.assertFalse(WordKeyMappingTable.isRuleLabel("[NP,1]"));
		Assert.assertTrue(WordKeyMappingTable.isLabelOne("[NP,1]"));
		Assert.assertTrue(WordKeyMappingTable.isRuleLabel("[NP+,+CC]"));
		Assert.assertTrue(wordKeyMappingTable.getItemHandler("[NP,1]") == wordKeyMappingTable.getLabelOneItemHandler());
		int key = wordKeyMappingTable.getKeyForWord("[NP,1]");
		assert(key > 0);
		Assert.assertFalse(WordKeyMappingTable.isRuleLabel("[NP+,,1]"));
		Assert.assertTrue(WordKeyMappingTable.isLabelOne("[NP+,,1]"));
	}
	
	@Test
	public void testWordKeyMappingTableSize()
	{
		String corpusDirectory = "/home/gemaille/AI/AlignedCorpera/en-fr/";
		String sourceFileName = "aligned.0.en";
		String targetFileName = "aligned.0.fr";
		List<String> wordFileNames = new ArrayList<String>();
		wordFileNames.add(corpusDirectory + sourceFileName);
		wordFileNames.add(corpusDirectory + targetFileName);
		WordKeyMappingTable table = WordKeyMappingTable.createWordKeyMappingTable(wordFileNames, 2,16);
		System.out.println("Max num items: " + table.getMaxNumItems() +  "  size : " + table.getSize());
		System.out.println("Key for \"villes \" : " + table.getKeyForWord("villes"));
		System.out.println("Key for \"towns \" : " + table.getKeyForWord("towns"));
		System.out.println("word for key 0 :"  + table.getWordForKey(0));
		System.out.println("word for key 899999 :"  + table.getWordForKey(899999));
		System.out.println("word for key 900000 :"  + table.getWordForKey(900000));
	}
	
	
	
	//@Test
	public void testMultiThread()
	{
		WordKeyMappingTable wordKeyMappingTable = WordKeyMappingTable.createWordKeyMappingTable(16);
		List<Callable<Integer>> threads = new ArrayList<Callable<Integer>>();
		int noThreads = 4;
		for(int i = 0; i < noThreads; i++)
		{
			threads.add(new EntryAdder(wordKeyMappingTable));
		}
		performThreadComputation(threads, 4);
	}

	public static <T> List<T> performThreadComputation(List<Callable<T>> threads, int noThreads)
	{
		List<Future<T>> registeredTasks = new ArrayList<Future<T>>();
		ExecutorService executorService = Executors.newFixedThreadPool(noThreads); 
		
		 for(Callable<T> tret :  threads)
		 {	 
			 Future<T> executingTask = executorService.submit(tret);
			 registeredTasks.add(executingTask);
		 }
		 // Wait until all extractor threads to finish
		 List<T> results = MultiThreadCorpusDataGenerator.joinThreads(registeredTasks);
		 System.out.println("\n\n Threads finished \n\n");
		
		return results;
	}
	
	private class EntryAdder implements Callable<Integer>
	{
		private final WordKeyMappingTable wordKeyMappingTable;
		private EntryAdder(WordKeyMappingTable wordKeyMappingTable){
			this.wordKeyMappingTable = wordKeyMappingTable;
		}
		@Override
		public Integer call() throws Exception {
			
			for(int i = 0; i < 100; i++)
			{	
				int key = this.wordKeyMappingTable.getKeyForWord("[NP+,,1]");
				System.out.println("key: " + key);
				Assert.assertEquals(100001, key);
			}	
			return 0;
		}
	
		
	}
	
}
