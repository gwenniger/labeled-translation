package unit_tests;

import junit.framework.Assert;
import grammarExtraction.translationRules.TranslationRuleStringLabelStripper;

import org.junit.Test;

public class TranslationRuleStringLabelStripperTest {

	private static final String TEST_RULE_STRING = "[<SOURCE-L>X<ReorderingLabel>E.F.M.</ReorderingLabel></SOURCE-L>] ||| [<SOURCE-L>X<ReorderingLabel>E.F.M.</ReorderingLabel></SOURCE-L>,1] ich ||| [<SOURCE-L>X<ReorderingLabel>E.F.M.</ReorderingLabel></SOURCE-L>,1] i ||| -0.0 0.17609125 0.17609125 -0.0 0.021581663 -0.0 -0.0 0.021581663 -0.0 -0.0 2.3976212 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 1.0 1.7400634";
	private static final String TEST_RESULT_REFERENCE_STRING = "[X] ||| [X,1] ich ||| [X,1] i ||| -0.0 0.17609125 0.17609125 -0.0 0.021581663 -0.0 -0.0 0.021581663 -0.0 -0.0 2.3976212 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 1.0 1.7400634";
	private static final String TEST_RESULT_REFERENCE_STRING_WITHOUT_WEIGHTS = "[X] ||| [X,1] ich ||| [X,1] i ||| ";

	@Test
	public void test() {

		TranslationRuleStringLabelStripper translationRuleStringLabelStripper = TranslationRuleStringLabelStripper.createTranslationRuleStringLabelStripper();
		Assert.assertEquals(TEST_RESULT_REFERENCE_STRING, translationRuleStringLabelStripper.getLabelStrippedRuleString(TEST_RULE_STRING));
		Assert.assertEquals(TEST_RESULT_REFERENCE_STRING_WITHOUT_WEIGHTS,
				translationRuleStringLabelStripper.getLabelStrippedRuleStringWithoutWeights(TEST_RULE_STRING));
	}

}
