package test_suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import unit_tests.*;
 
/**
 * JUnit Suite Test
 * @author gemaille
 *
 */
 
@RunWith(Suite.class)
@Suite.SuiteClasses(
{
        MTRuleTrieNodeTest.class,
        MinimalPhrasePairRuleExtractorTest.class,
        HeavyWeightChiangRuleExtractorTest.class,
        CoreContextLabeledChartTester.class,
        ComplexGlueGrammarCreaterTest.class,
        CCGLabelerTest.class,
        ExtraBracketsRemoverTest.class,
        TrueCasedLinesFileReconstructorTest.class,
        ReorderingLabelerTest.class,
        ChartCoreContextLabelsTableTest.class
})
public class UnitTestSuiteTranslationModule 
{
}