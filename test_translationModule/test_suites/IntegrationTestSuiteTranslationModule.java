package test_suites;

import integration_tests.HierarchicalPhraseBasedRuleExtractorTest;
import integration_tests.MTPipelineTestJoshua;
import integration_tests.MTPipelineTestMoses;
import integration_tests.MultiThreadGrammarExtractorMinimalPhrasePairsTest;
import integration_tests.MultiThreadGrammarExtractorTestFilteredTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

 
/**
 * JUnit Suite Test
 * @author gemmaille
 *
 */
 
@RunWith(Suite.class)
@Suite.SuiteClasses(
{
        HierarchicalPhraseBasedRuleExtractorTest.class,
        MultiThreadGrammarExtractorTestFilteredTest.class,
        MultiThreadGrammarExtractorMinimalPhrasePairsTest.class,
        MTPipelineTestMoses.class,
        MTPipelineTestJoshua.class
})
public class IntegrationTestSuiteTranslationModule {
}