package integration_tests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import junit.framework.Assert;
import junit.framework.TestCase;
import grammarExtraction.chiangGrammarExtraction.MultiThreadGrammarExtractorTestFiltered;
import grammarExtraction.grammarExtractionFoundation.MultiThreadGrammarExtractor;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import util.FileUtil;
import alignment.ArtificialCorpusProperties;
import artificalCorpusCreation.ArtificialTranslationCorpusCreater;

public class MultiThreadGrammarExtractorTestFilteredTest extends TestCase {

	final ArtificialCorpusProperties artificialCorpusProperties = ArtificialCorpusProperties.createArtificialCorpusProperties();

	public static void createTestCorpus(ArtificialCorpusProperties artificialCorpusProperties, String testCorpusFolderName) {

		try {
			ArtificialTranslationCorpusCreater.createArtificialCorpusCreatorTranslation(artificialCorpusProperties, testCorpusFolderName,true,false);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	public void testAssertionsEnabled() {
		try {
			assert (false);
			fail("should throw");
		} catch (AssertionError e) {
		}

	}

	public void testExtractGrammar() {
		String testCorpusFolderName = "./testCorpus/"; // "/home/gemaille/atv-bitg-workingdirectory/trunk/testCorpus/";
		FileUtil.createFolderIfNotExisting(testCorpusFolderName);
		createTestCorpus(artificialCorpusProperties, testCorpusFolderName);
		String configFileName = testCorpusFolderName + "configFile.txt";
		int numThreads = 4;
		SystemIdentity systemIdentity = SystemIdentity.createHieroSystemIdenty();
		MultiThreadGrammarExtractor grammarExtractor = MultiThreadGrammarExtractorTestFiltered.createMultiThreadGrammarExtractorTestFilteredHiero(
				configFileName, numThreads,systemIdentity);
		grammarExtractor.extractGrammarMultiThreadAndWriteResultToFile("translationGrammarSourceFiltered.txt",false);
		Assert.assertEquals(22, grammarExtractor.getLexicalCountSourceToTarget("s0", "t0"));
		Assert.assertEquals(22, grammarExtractor.getLexicalCountTargetToSource("t0", "s0"));
		Assert.assertEquals(0.328358200, grammarExtractor.getLexicalProbabilitySourceToTarget("s0", "t0"), 0.0001);
		Assert.assertTrue(grammarExtractor.getLexicalWeightSourceToTarget("s3", "t3", "0-0") >= 0.1);
		Assert.assertTrue(grammarExtractor.getLexicalWeightTargetToSource("s3", "t3", "0-0") >= 0);
		Assert.assertTrue(grammarExtractor.testLexicalTables());
	}

	public void testStupid() {
		int nodeInsertionIndex = 0;
		int level = 2;
		for (int i = nodeInsertionIndex + 3; i <= nodeInsertionIndex + 3 + level; i++) {
			assert (i >= 3);
			assert (i <= 5);
		}
		Assert.assertEquals(-1, Collections.binarySearch(new ArrayList<String>(), "blaa"));
		Assert.assertEquals(~(-1),0);
		List<String> filledList = new ArrayList<String>();
		filledList.add("blaa");
		Assert.assertEquals(0, Collections.binarySearch(filledList,"blaa"));
		
	}

	public void testExtractGrammarToy() {
		String testCorpusFolderName = "./testCorpusToyCar/"; // "/home/gemaille/atv-bitg-workingdirectory/trunk/testCorpus/";
		FileUtil.createFolderIfNotExisting(testCorpusFolderName);
		try {
			ArtificialTranslationCorpusCreater.createToyTranslationArtificialCorpusCreator(testCorpusFolderName,true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String configFileName = testCorpusFolderName + "configFile.txt";
		int numThreads = 4;
		SystemIdentity systemIdentity = SystemIdentity.createHieroSystemIdenty();
		MultiThreadGrammarExtractor grammarExtractor = MultiThreadGrammarExtractorTestFiltered.createMultiThreadGrammarExtractorTestFilteredHiero(
				configFileName, numThreads,systemIdentity);
		grammarExtractor.extractGrammarMultiThreadAndWriteResultToFile("translationGrammarSourceFiltered.txt",false);
		Assert.assertEquals(2, grammarExtractor.getLexicalCountSourceToTarget("man", "man"));
		Assert.assertEquals(2, grammarExtractor.getLexicalCountTargetToSource("man", "man"));
		Assert.assertEquals(2 / 3.0, grammarExtractor.getLexicalProbabilitySourceToTarget("man", "man"), 0.0001);
		Assert.assertTrue(grammarExtractor.getLexicalWeightSourceToTarget("man", "man", "0-0") >= 0.1);
		Assert.assertTrue(grammarExtractor.getLexicalWeightTargetToSource("the man walks", "de man loopt", "0-0 1-1 2-2") >= 0);
		Assert.assertEquals((1 / 3.0), grammarExtractor.getRuleConditionalPhraseProbabilitySourceToTarget("the man walks [X]", "de man loopt [X]"));
		Assert.assertEquals((1 / 3.0), grammarExtractor.getRuleConditionalPhraseProbabilitySourceToTarget("the man walks [X]", "de man wandelt [X]"));
		Assert.assertEquals((1 / 3.0), grammarExtractor.getRuleConditionalPhraseProbabilitySourceToTarget("the man walks [X]", "de persoon loopt [X]"));
		Assert.assertEquals((1 / 3.0), grammarExtractor.getRuleConditionalPhraseProbabilitySourceToTarget("the man [X,1] [X]", "de persoon [X,1] [X]"), 0.001);
		Assert.assertEquals((2 / 3.0), grammarExtractor.getRuleConditionalPhraseProbabilitySourceToTarget("the [X,1] car [X]", "de [X,1] auto [X]"), 0.001);
		Assert.assertEquals((1), grammarExtractor.getRuleConditionalPhraseProbabilitySourceToTarget("the [X,1] to [X,2] [X]", "de [X,1] naar [X,2] [X]"), 0.001);
		Assert.assertTrue(grammarExtractor.testLexicalTables());
		Assert.assertTrue(grammarExtractor.testLexicalTables());
	}

	public void testExtractGrammarToySAMT() {
		String testCorpusFolderName = "./testCorpusToyCarSAMT/"; // "/home/gemaille/atv-bitg-workingdirectory/trunk/testCorpus/";
		FileUtil.createFolderIfNotExisting(testCorpusFolderName);
		try {
			ArtificialTranslationCorpusCreater.createToyTranslationArtificialCorpusCreatorSAMT(testCorpusFolderName);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String configFileName = testCorpusFolderName + "configFile.txt";
		int numThreads = 4;
		SystemIdentity systemIdentity = SystemIdentity.createSAMTSystemIdentyWithoutSmoothing();
		MultiThreadGrammarExtractor grammarExtractor = MultiThreadGrammarExtractorTestFiltered.createMultiThreadGrammarExtractorTestFilteredSAMT(
				configFileName, numThreads,systemIdentity);
		grammarExtractor.extractGrammarMultiThreadAndWriteResultToFile("translationGrammarSourceFilteredSAMT.txt",false);
		// assert(grammarExtractor.getRuleExtracter() instanceof
		// SAMTRuleExtracter);
		// assert(grammarExtractor.getRuleExtracter().createTranslationRuleExtractor(AlignmentStringTriple.createTrimmedAlignmentStringTriple("s",
		// "t",
		// "0-0")).getHeavyWeightChiangRuleExtractor().getTranslationRuleCreater()
		// instanceof SAMTRuleCreater);

		Assert.assertTrue(grammarExtractor.testLexicalTables());

	}

	public void testExtractGrammarToySAMTVilles() {
		String testCorpusFolderName = "./testCorpusToyVillesSAMT/"; // "/home/gemaille/atv-bitg-workingdirectory/trunk/testCorpus/";
		FileUtil.createFolderIfNotExisting(testCorpusFolderName);
		try {
			ArtificialTranslationCorpusCreater.createToyTranslationArtificialCorpusCreatorSAMT2(testCorpusFolderName);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String configFileName = testCorpusFolderName + "configFile.txt";
		int numThreads = 4;
		SystemIdentity systemIdentity = SystemIdentity.createSAMTSystemIdentyWithoutSmoothing();
		MultiThreadGrammarExtractor grammarExtractor = MultiThreadGrammarExtractorTestFiltered.createMultiThreadGrammarExtractorTestFilteredSAMT(
				configFileName, numThreads,systemIdentity);
		grammarExtractor.extractGrammarMultiThreadAndWriteResultToFile("translationGrammarSourceFilteredSAMT_Villes.txt",false);
		// assert(grammarExtractor.getRuleExtracter() instanceof
		// SAMTRuleExtracter);
		// assert(grammarExtractor.getRuleExtracter().createTranslationRuleExtractor(AlignmentStringTriple.createTrimmedAlignmentStringTriple("s",
		// "t",
		// "0-0")).getHeavyWeightChiangRuleExtractor().getTranslationRuleCreater()
		// instanceof SAMTRuleCreater);

		Assert.assertTrue(grammarExtractor.testLexicalTables());

	}

	/*
	 * @Test public void testExtractGrammarSpecific() { String configFileName =
	 * "/home/gemaille/atv-bitg-workingdirectory-oldsvn/trunk/mtData_JoshuaPure/JoshuaHatsHiero/intermediate-results/test/hatsGrammarExtractorTestConfig.txt"
	 * ; int numThreads = 4; MultiThreadGrammarExtractor grammarExtractor =
	 * MultiThreadGrammarExtractor
	 * .createMultiThreadGrammarExtractor(configFileName, numThreads, true);
	 * grammarExtractor
	 * .extractGrammarMultiThread("translationGrammarSourceFiltered2.txt");
	 * Assert.assertTrue(grammarExtractor.testLexicalTables());
	 * grammarExtractor.printSourceTrie(); //Assert.assertEquals(0.75,
	 * grammarExtractor.getRuleConditionalPhraseProbabilitySourceToTarget("que",
	 * "that")); //Assert.assertEquals(0.25,
	 * grammarExtractor.getRuleConditionalPhraseProbabilitySourceToTarget("que",
	 * "which")); }
	 */
}
