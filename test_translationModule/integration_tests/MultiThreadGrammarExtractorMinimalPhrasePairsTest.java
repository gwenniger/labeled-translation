package integration_tests;

import grammarExtraction.chiangGrammarExtraction.ChiangRuleCreater;
import grammarExtraction.grammarExtractionFoundation.MultiThreadGrammarExtractor;
import grammarExtraction.minimalPhrasePairExtraction.MultiThreadGrammarExtractorMinimalPhrasePairs;

import java.io.FileNotFoundException;
import java.io.IOException;

import junit.framework.Assert;

import org.junit.Test;

import util.ConfigFile;
import util.FileUtil;
import alignment.ArtificialCorpusProperties;
import artificalCorpusCreation.ArtificialTranslationCorpusCreater;

public class MultiThreadGrammarExtractorMinimalPhrasePairsTest {

	private final String ChiangRuleLabelExtension = " " + ChiangRuleCreater.ChiangRuleLabel;
	final ArtificialCorpusProperties artificialCorpusProperties = ArtificialCorpusProperties.createArtificialCorpusProperties();

	public static void createTestCorpus(ArtificialCorpusProperties artificialCorpusProperties, String testCorpusFolderName) {

		try {
			ArtificialTranslationCorpusCreater.createArtificialCorpusCreatorTranslation(artificialCorpusProperties, testCorpusFolderName, false,false);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testExtractGrammarToy() throws FileNotFoundException {
		String testCorpusFolderName = "./testCorpusToyCar/"; // "/home/gemaille/atv-bitg-workingdirectory/trunk/testCorpus/";
		FileUtil.createFolderIfNotExisting(testCorpusFolderName);
		try {
			ArtificialTranslationCorpusCreater.createToyTranslationArtificialCorpusCreator(testCorpusFolderName, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String configFileName = testCorpusFolderName + "configFile.txt";
		int numThreads = 4;
		ConfigFile configFile = new ConfigFile(configFileName);
		MultiThreadGrammarExtractor grammarExtractor = MultiThreadGrammarExtractorMinimalPhrasePairs.createMultiThreadGrammarExtractorMinimalPhrasePairs(
				configFile, numThreads, true);
		grammarExtractor.extractGrammarMultiThreadAndWriteResultToFile("translationGrammarSourceFilteredMinimalPhrasePairs.txt", false);
		Assert.assertEquals(2, grammarExtractor.getLexicalCountSourceToTarget("man", "man"));
		Assert.assertEquals(2, grammarExtractor.getLexicalCountTargetToSource("man", "man"));
		Assert.assertEquals(2 / 3.0, grammarExtractor.getLexicalProbabilitySourceToTarget("man", "man"), 0.0001);
		Assert.assertTrue(grammarExtractor.getLexicalWeightSourceToTarget("man", "man", "0-0") >= 0.1);
		Assert.assertTrue(grammarExtractor.getLexicalWeightTargetToSource("the man walks", "de man loopt", "0-0 1-1 2-2") >= 0);
		Assert.assertEquals((1.0),
				grammarExtractor.getRuleConditionalPhraseProbabilitySourceToTarget("the" + ChiangRuleLabelExtension, "de" + ChiangRuleLabelExtension));
		Assert.assertEquals((2 / 3.0),
				grammarExtractor.getRuleConditionalPhraseProbabilitySourceToTarget("man" + ChiangRuleLabelExtension, "man" + ChiangRuleLabelExtension));
		Assert.assertEquals((2 / 3.0),
				grammarExtractor.getRuleConditionalPhraseProbabilitySourceToTarget("walks" + ChiangRuleLabelExtension, "loopt" + ChiangRuleLabelExtension));
		Assert.assertTrue(grammarExtractor.testLexicalTables());
	}

	// @Test
	public void testExtractGrammarToyFrenchEnglish() throws FileNotFoundException {
		String testCorpusFolderName = "./testCorpusToyFrenchEnglish/";
		FileUtil.createFolderIfNotExisting(testCorpusFolderName);
		try {
			ArtificialTranslationCorpusCreater.createToyTranslationArtificialCorpusCreatorFrenchEnglish(testCorpusFolderName, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
