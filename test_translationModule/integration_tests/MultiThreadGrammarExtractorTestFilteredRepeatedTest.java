package integration_tests;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.extensions.RepeatedTest;

// See also : http://www.torsten-horn.de/techdocs/java-junit.htm#RepeatedTest
// For more info how to construct repeated tests

public class MultiThreadGrammarExtractorTestFilteredRepeatedTest extends TestSuite {

	private static int NoRepetitions = 100;

	public static Test suite() {
		TestSuite mySuite = new TestSuite("Meine Test-Suite");
		mySuite.addTestSuite(MultiThreadGrammarExtractorTestFilteredTest.class);
		// ... weitere Testklassen hinzufügen
		return new RepeatedTest(mySuite, NoRepetitions);
	}
}