package integration_tests;

import grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagLabelingSettings;
import grammarExtraction.translationRules.TranslationRuleStringLabelStripper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import junit.framework.Assert;
import mt_pipeline.MTPipeline;
import mt_pipeline.MTPipelineHATsSAMT;
import mt_pipeline.MTPipelineHatsBoundaryTagLabeled;
import mt_pipeline.MTPipelineHatsHiero;
import mt_pipeline.mtPipelineTest.MTPipelineTestConfigFileCreater;
import mt_pipeline.mtPipelineTest.SourceAndTargetLanguageAbbreviations;
import mt_pipeline.mtPipelineTest.TestIdentifierPair;
import mt_pipeline.mtPipelineTest.TestLabelingSettings;
import mt_pipeline.mtPipelineTest.TestRerankingSettings;
import util.FileStatistics;
import util.FileUtil;
import util.Pair;
import util.ParseAndTagBracketAndLabelConverter;

public abstract class MTPipelineTest {

	public static String MT_PIPELINE_TEST_CONFIG_FILE_GENERATION_CONFIG_FILE_NAME = "./mtPipelineTestsSystemSpecificConfig.txt";
	protected static String MT_PIPELINE_TEST_CONFIG_FILE_NAME = "mtPipelineTestsGeneratedCompleteConfig.txt";
	public static TranslationRuleStringLabelStripper TRANSLATION_RULES_TRING_LABEL_STRIPPER = TranslationRuleStringLabelStripper
			.createTranslationRuleStringLabelStripper();
	public static String MT_DATA_LOCATIONS_CONFIG_FILE_DE_EN = "./mtPipelineTestSmallDataDeEn/dataLocationsConfig.txt";	
	public static String MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN = "./testCorpora/testCorpusToyFrenchEnglish/dataLocationsConfig.txt";
	public static String MT_DATA_LOCATIONS_CONFIG_FILE_FR_EN2 = "./testCorpora/testCorpusToyFrenchEnglish-MonotoneAndInvertedOnly/dataLocationsConfig.txt";
	public static String MT_DATA_LOCATIONS_CONFIG_FILE_DEBUGGING1 = "./testCorpora/testCorpusDebugging1/dataLocationsConfig.txt";
	public static String MT_DATA_LOCATIONS_CONFIG_FILE_DEBUGGING2 = "./testCorpora/testCorpusDebugging2/dataLocationsConfig.txt";
	public static String MT_DATA_LOCATIONS_CONFIG_FILE_DEBUGGING3 = "./testCorpora/testCorpusChineseGivingDifferencesInGrammars/dataLocationsConfig.txt";
	public static String MT_DATA_LOCATIONS_CONFIG_FILE_DEBUGGING4 = "./testCorpora/testCorpusChineseGivingDifferencesInGrammarsDifferentString/dataLocationsConfig.txt";
	public static String MT_DATA_LOCATIONS_CONFIG_FILE_DEBUGGING5 = "./testCorpora/testCorpusChineseGivingDifferencesInGrammarsSimplified/dataLocationsConfig.txt";
	public static String MT_DATA_LOCATIONS_CONFIG_FILE_ChineseEnglish = "./testCorpora/testCorpusChineseEnglish/dataLocationsConfig.txt";
	public static String MT_DATA_LOCATIONS_CONFIG_FILE_GermanEnglishThesisToy = "./testCorpora/testCorpusThesisExample/dataLocationsConfig.txt";
	public static String MT_DATA_LOCATIONS_CONFIG_FILE_DutchEnglishReorderingTest = "./testCorpora/testCorpusToyDutchEnglishReoredering/dataLocationsConfig.txt";
	
	public static SourceAndTargetLanguageAbbreviations  SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN= SourceAndTargetLanguageAbbreviations.createSourceAndTargetLanguageAbbreviations("fr", "en");
	public static SourceAndTargetLanguageAbbreviations  SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_NE_EN = SourceAndTargetLanguageAbbreviations.createSourceAndTargetLanguageAbbreviations("ne", "en");
	public static SourceAndTargetLanguageAbbreviations  SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_DE_EN = SourceAndTargetLanguageAbbreviations.createSourceAndTargetLanguageAbbreviations("de", "en");
	protected static SourceAndTargetLanguageAbbreviations  SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_EN_DE = SourceAndTargetLanguageAbbreviations.createSourceAndTargetLanguageAbbreviations("en", "de");
	protected static SourceAndTargetLanguageAbbreviations  SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_ZH_EN = SourceAndTargetLanguageAbbreviations.createSourceAndTargetLanguageAbbreviations("zh", "en");
	
	private static final BoundaryTagLabelingSettings ALL_EMPTY_BOUNDARY_TAG_LABELING_SETTINGS = BoundaryTagLabelingSettings.createAllEmptyBoundaryTagLabelingSettings();
	
	public static enum TUNER_TYPE {
		MERT_TUNING, PRO_TUNING, MIRA_TUNING
	};

	protected abstract MTPipelineTestConfigFileCreater createMTPipelineTestConfigFileCreater(
			TestIdentifierPair testIdentifierPair,
			TestLabelingSettings testReorderingLabelingSettings,
			TestRerankingSettings testRerankingSettings, TUNER_TYPE tunerType,
			boolean parseTargetFile, int trainDataLength,
			String testDataLocationsConfigFile, boolean useSoftSyntacticConstraintDecoding,
			SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations,
			boolean performTuning,boolean use_rule_application_features);

	protected abstract boolean isMosesSystem();

	protected static String getTestGrammarLocation(
			TestIdentifierPair testIdentifierPair) {
		return MTPipelineTestConfigFileCreater.getTestGrammarLocation(
				MT_PIPELINE_TEST_CONFIG_FILE_GENERATION_CONFIG_FILE_NAME,
				testIdentifierPair);
	}

	protected static String getMertGrammarLocation(
			TestIdentifierPair testIdentifierPair) {
		return MTPipelineTestConfigFileCreater.getMertGrammarLocation(
				MT_PIPELINE_TEST_CONFIG_FILE_GENERATION_CONFIG_FILE_NAME,
				testIdentifierPair);
	}

	public int getNumberOfRulesMertGrammar(TestIdentifierPair testIdentifierPair) {
		return (FileUtil.getSentences(new File(
				getMertGrammarLocation(testIdentifierPair)), true)).size();
	}

	private static String getTargetParseGrammarInputLocation(
			TestIdentifierPair testIdentifierPair) {
		return MTPipelineTestConfigFileCreater
				.getTargetParseGrammarInputFileName(
						MT_PIPELINE_TEST_CONFIG_FILE_NAME, testIdentifierPair);
	}

	private static String getTargetParseOutputFileName(
			TestIdentifierPair testIdentifierPair) {
		return MTPipelineTestConfigFileCreater.getTargetParseOytputFileName(
				MT_PIPELINE_TEST_CONFIG_FILE_NAME, testIdentifierPair);
	}

	protected static boolean fileContainsStrings(String fileName,
			List<String> stringList) {
		for (String string : stringList) {
			if (!fileContainsString(fileName, string)) {
				return false;
			}
		}
		return true;
	}

	protected static boolean fileContainsString(String fileName, String string) {
		List<String> lines = FileUtil.getSentences(new File(fileName), true);
		System.out.println("Searching for " + string + " in " + fileName);
		for (String line : lines) {
			if (line.contains(string)) {
				System.out.println("Found match");
				return true;
			}
		}
		return false;
	}

	private static boolean fileContainsPattern(String fileName, Pattern pattern) {
		List<String> lines = FileUtil.getSentences(new File(fileName), true);
		for (String line : lines) {
			System.out.println("Line to match: " + line);
			if (pattern.matcher(line).find()) {
				System.out.println("Found match");
				return true;
			}
		}
		return false;
	}

	private static boolean fileContainsRawX(String fileName) {
		return fileContainsPattern(fileName,
				Pattern.compile(ParseAndTagBracketAndLabelConverter
						.xPatternMatchingPatternString()));
	}

	private static boolean fileContainsSyntaxLabelX(String fileName) {
		return fileContainsPattern(fileName,
				Pattern.compile(ParseAndTagBracketAndLabelConverter
						.xSyntaxLabelPatternMatchingPatternString()));
	}

	public static void assert_X_In_Parse_Output_And_XSyntactic_In_Grammar_Input(
			TestIdentifierPair testIdentifierPair) {
		Assert.assertTrue(fileContainsRawX(getTargetParseOutputFileName(testIdentifierPair)));
		Assert.assertFalse(fileContainsSyntaxLabelX(getTargetParseOutputFileName(testIdentifierPair)));

		Assert.assertTrue(fileContainsSyntaxLabelX(getTargetParseGrammarInputLocation(testIdentifierPair)));
		Assert.assertFalse(fileContainsRawX(getTargetParseGrammarInputLocation(testIdentifierPair)));

	}

	public static String getTestRootOutputDir(
			TestIdentifierPair testIdentifierPair) {
		return MTPipelineTestConfigFileCreater.getTestRootOutputDir(
				MT_PIPELINE_TEST_CONFIG_FILE_GENERATION_CONFIG_FILE_NAME,
				testIdentifierPair);
	}

	private static int getDevGrammarSize(TestIdentifierPair testIdentifierPair) {
		try {
			return FileStatistics
					.countLines(getMertGrammarLocation(testIdentifierPair));
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Error : could not count the lines" + e);
		}
	}

	private static int getTestGrammarSize(TestIdentifierPair testIdentifierPair) {
		try {
			return FileStatistics
					.countLines(getTestGrammarLocation(testIdentifierPair));
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Error : could not count the lines" + e);
		}
	}

	private static boolean systemOneHasSmallerDevGrammarThanSystemTwo(
			TestIdentifierPair systemOne, TestIdentifierPair sytemTwo) {
		int systemOneDevGrammarSize = getDevGrammarSize(systemOne);
		int systemTwoDevGrammarSize = getDevGrammarSize(sytemTwo);
		System.out.println("System one dev grammar size: "
				+ systemOneDevGrammarSize);
		System.out.println("System two dev grammar size: "
				+ systemTwoDevGrammarSize);
		return systemOneDevGrammarSize < systemTwoDevGrammarSize;
	}

	private static boolean systemOneHasSmallerTestGrammarThanSystemTwo(
			TestIdentifierPair systemOne, TestIdentifierPair sytemTwo) {
		int systemOneTestGrammarSize = getTestGrammarSize(systemOne);
		int systemTwoTestGrammarSize = getTestGrammarSize(sytemTwo);
		System.out.println("System one test grammar size: "
				+ systemOneTestGrammarSize);
		System.out.println("System two test grammar size: "
				+ systemTwoTestGrammarSize);
		return systemOneTestGrammarSize < systemTwoTestGrammarSize;
	}

	public static void assertSystemOneHasSmallerGrammarsThanSystemTwo(
			TestIdentifierPair systemOne, TestIdentifierPair sytemTwo) {
		Assert.assertTrue(systemOneHasSmallerDevGrammarThanSystemTwo(systemOne,
				sytemTwo));
		Assert.assertTrue(systemOneHasSmallerTestGrammarThanSystemTwo(
				systemOne, sytemTwo));
	}

	public static MTPipelineTestConfigFileCreater createMTPipelineTestConfigFileCreater(
			TestIdentifierPair testIdentifierPair,
			TestLabelingSettings testReorderingLabelingSettings,
			TestRerankingSettings testRerankingSettings, TUNER_TYPE tunerType,

			boolean parseTargetFile, int trainDataLength,
			String testDataLocationsConfigFile, boolean useMosesSystem, boolean useSoftSyntacticConstraintDecoding,
			SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations,
			boolean performTuning,boolean use_rule_application_features) {

		
		boolean useLabelSubstitutionFeatures = useSoftSyntacticConstraintDecoding;
		System.out.println("createMTPipelineTestConfigFileCreater...");
		List<String> systemNames = Collections.singletonList(testIdentifierPair
				.getSystemName());
		return MTPipelineTestConfigFileCreater
				.createMTPipelineTestConfigFileCreater(
						MT_PIPELINE_TEST_CONFIG_FILE_GENERATION_CONFIG_FILE_NAME,
						MT_PIPELINE_TEST_CONFIG_FILE_NAME, systemNames,
						testIdentifierPair.getTestName(),
						testReorderingLabelingSettings, testRerankingSettings,
						tunerType, parseTargetFile, trainDataLength,
						testDataLocationsConfigFile, useMosesSystem,useSoftSyntacticConstraintDecoding,
						useLabelSubstitutionFeatures,sourceAndTargetLanguageAbbreviations,
						performTuning,use_rule_application_features);
	}
	
	public static BoundaryTagLabelingSettings createBoundaryTagLabelingSettings(boolean useTargetInsideBoundaryTags){
	    if(useTargetInsideBoundaryTags){
		return BoundaryTagLabelingSettings.createTargetInsideTagsBoundaryTagLabelingSettings();
	    }
	    else{
		return BoundaryTagLabelingSettings.createAllEmptyBoundaryTagLabelingSettings();
	    }
	}
	

	private static List<String> getTestRules(
			TestIdentifierPair testIdentifierPair) {
		return FileUtil.getSentences(new File(
				getTestGrammarLocation(testIdentifierPair)), true);
	}

	private static List<String> getDevRules(
			TestIdentifierPair testIdentifierPair) {
		return FileUtil.getSentences(new File(
				getMertGrammarLocation(testIdentifierPair)), true);
	}

	private static List<String> getLabelStrippedRulesWithoutWeights(
			List<String> originalRuleStrings) {
		List<String> result = new ArrayList<String>();
		for (String ruleString : originalRuleStrings) {
			result.add(TRANSLATION_RULES_TRING_LABEL_STRIPPER
					.getLabelStrippedRuleStringWithoutWeights(ruleString));
		}
		return result;
	}

	private static List<String> getOriginalRulesWithoutWeights(
			List<String> originalRuleStrings) {
		List<String> result = new ArrayList<String>();
		for (String ruleString : originalRuleStrings) {
			result.add(TRANSLATION_RULES_TRING_LABEL_STRIPPER
					.getOriginalRuleStringWithoutWeights(ruleString));
		}
		return result;
	}

	private static boolean systemTwoContainsAllTestRulesOfSystemOneWhenIgnoringLabels(
			TestIdentifierPair systemOne, TestIdentifierPair sytemTwo) {
		return getLabelStrippedRulesWithoutWeights(getTestRules(sytemTwo))
				.containsAll(
						getOriginalRulesWithoutWeights(getTestRules(systemOne)));
	}

	private static boolean systemTwoContainsAllDevRulesOfSystemOneWhenIgnoringLabels(
			TestIdentifierPair systemOne, TestIdentifierPair sytemTwo) {
		return getLabelStrippedRulesWithoutWeights(getDevRules(sytemTwo))
				.containsAll(
						getOriginalRulesWithoutWeights(getDevRules(systemOne)));
	}

	protected static void assertSystemTwoContainsAllRulesOfSystemOneWhenIgnoringLabels(
			TestIdentifierPair systemOne, TestIdentifierPair systemTwo) {

		Assert.assertTrue(systemTwoContainsAllDevRulesOfSystemOneWhenIgnoringLabels(
				systemOne, systemTwo));
		Assert.assertTrue(systemTwoContainsAllTestRulesOfSystemOneWhenIgnoringLabels(
				systemOne, systemTwo));
	}

	private void testSystem(TestIdentifierPair testIdentifierPair,
			TestLabelingSettings testReorderingLabelingSettings,
			TestRerankingSettings testRerankingSettings, TUNER_TYPE tunerType,
			boolean parseTargetFile,boolean useSoftSyntacticConstraintDecoding, int trainDataLength,
			String testDataLocationsConfigFilePath,SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations,
			boolean performTuning,boolean use_rule_application_features) {
		MTPipelineTestConfigFileCreater mtPipelineTestConfigFileCreater = createMTPipelineTestConfigFileCreater(
				testIdentifierPair, testReorderingLabelingSettings,
				testRerankingSettings, tunerType, parseTargetFile,
				trainDataLength, testDataLocationsConfigFilePath,useSoftSyntacticConstraintDecoding,
				sourceAndTargetLanguageAbbreviations,performTuning,use_rule_application_features
				);
		if (mtPipelineTestConfigFileCreater != null) {
			mtPipelineTestConfigFileCreater.writeExperimentsConfigFile();
			Assert.assertTrue(MTPipeline
					.runPipelinesForConfig(MT_PIPELINE_TEST_CONFIG_FILE_NAME));
		} else {
			Assert.fail();
		}

	}

	public Pair<TestIdentifierPair> testHieroPipelineAndHieroPipelineWithReorderingLabels(
			TUNER_TYPE tunerType, String dataLocationsConfigFilePath,
			TestRerankingSettings testRerankingSettings,boolean  outputOnlyCanonicalFormLabeledRules,
			SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations) {	    
	    
		TestIdentifierPair systemOne = TestIdentifierPair
				.createTestIdentifierPair(MTPipelineHatsHiero.SystemName,
						MTPipelineHatsHiero.SystemName + "-pure");
		testSystem(systemOne,
				TestLabelingSettings.createNoReorderingLabelingSettings(ALL_EMPTY_BOUNDARY_TAG_LABELING_SETTINGS,outputOnlyCanonicalFormLabeledRules),
				testRerankingSettings, tunerType, false,false, 10,
				dataLocationsConfigFilePath,sourceAndTargetLanguageAbbreviations,true,false);

		TestIdentifierPair systemTwo = TestIdentifierPair
				.createTestIdentifierPair(MTPipelineHatsHiero.SystemName,
						MTPipelineHatsHiero.SystemName + "-reorderingLabeled");
		testSystem(systemTwo,
				TestLabelingSettings
						.createBasicTestReorderingLabelingSettings(outputOnlyCanonicalFormLabeledRules,false,ALL_EMPTY_BOUNDARY_TAG_LABELING_SETTINGS),
				testRerankingSettings, tunerType, false,false, 10,
				dataLocationsConfigFilePath,sourceAndTargetLanguageAbbreviations,true,false);
		if (!isMosesSystem()) {
			assertSystemOneHasSmallerGrammarsThanSystemTwo(systemOne, systemTwo);
			assertSystemTwoContainsAllRulesOfSystemOneWhenIgnoringLabels(
					systemOne, systemTwo);
		}
		return new Pair<TestIdentifierPair>(systemOne, systemTwo);
	}

	public TestIdentifierPair testHieroPipeline(TUNER_TYPE tunerType,
			String dataLocationsConfigFilePath, String languageSuffix,
			TestRerankingSettings testRerankingSettings,SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations,
			boolean performTuning) {
		TestIdentifierPair systemWithReorderingLabelsAndReorderingBinaryFeatures = TestIdentifierPair
				.createTestIdentifierPair(MTPipelineHatsHiero.SystemName,
						MTPipelineHatsHiero.SystemName + "-Baseline" + "-"
								+ languageSuffix);
		testSystem(systemWithReorderingLabelsAndReorderingBinaryFeatures,
				TestLabelingSettings.createNoReorderingLabelingSettings(ALL_EMPTY_BOUNDARY_TAG_LABELING_SETTINGS,false),
				testRerankingSettings, tunerType, false,false, 10,
				dataLocationsConfigFilePath, sourceAndTargetLanguageAbbreviations,performTuning,false);
		return systemWithReorderingLabelsAndReorderingBinaryFeatures;
	}

	
	public TestIdentifierPair testHieroPipelineWithCoarseInsidePhraseBoundaryPosTagsTarget(TUNER_TYPE tunerType,
		String dataLocationsConfigFilePath, String languageSuffix,
		TestRerankingSettings testRerankingSettings,SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations,
		boolean performTuning,boolean softMatching) {
	TestIdentifierPair systemWithReorderingLabelsAndReorderingBinaryFeatures = TestIdentifierPair
			.createTestIdentifierPair(MTPipelineHatsBoundaryTagLabeled.SystemName,
					MTPipelineHatsBoundaryTagLabeled.SystemName + "-TargetInsideTags" + "-"
							+ languageSuffix);
	BoundaryTagLabelingSettings boundaryTagLabelingSettings = BoundaryTagLabelingSettings.createTargetInsideTagsBoundaryTagLabelingSettings();
	testSystem(systemWithReorderingLabelsAndReorderingBinaryFeatures,
			TestLabelingSettings.createNoReorderingLabelingSettings(boundaryTagLabelingSettings,softMatching),
			testRerankingSettings, tunerType, false,softMatching, 10,
			dataLocationsConfigFilePath, sourceAndTargetLanguageAbbreviations,performTuning,false);
	return systemWithReorderingLabelsAndReorderingBinaryFeatures;
}
	
	
	public TestIdentifierPair testHieroPipelineWithReorderingLabelsBasic(
			TUNER_TYPE tunerType, String dataLocationsConfigFilePath,
			String languageSuffix, TestRerankingSettings testRerankingSettings,boolean  outputOnlyCanonicalFormLabeledRules,boolean useOnlyHieroWeightsForCanonicalFormLabeledRules, boolean useSoftSyntacticConstraintDecoding,
			SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations,boolean performTuning,
			boolean use_rule_application_features) {
		TestIdentifierPair systemWithReorderingLabelsAndReorderingBinaryFeatures = TestIdentifierPair
				.createTestIdentifierPair(MTPipelineHatsHiero.SystemName,
						MTPipelineHatsHiero.SystemName
								+ "-reorderingLabeled-Basic" + "-"
								+ languageSuffix);
		testSystem(systemWithReorderingLabelsAndReorderingBinaryFeatures,
				TestLabelingSettings
						.createBasicTestReorderingLabelingSettings(outputOnlyCanonicalFormLabeledRules,useOnlyHieroWeightsForCanonicalFormLabeledRules,ALL_EMPTY_BOUNDARY_TAG_LABELING_SETTINGS),
				testRerankingSettings, tunerType, false,useSoftSyntacticConstraintDecoding, 10,
				dataLocationsConfigFilePath, sourceAndTargetLanguageAbbreviations,performTuning,use_rule_application_features);
		return systemWithReorderingLabelsAndReorderingBinaryFeatures;
	}

	public TestIdentifierPair testHieroPipelineWithReorderingLabelsBasicOnlySourceSideLabeled(
			TUNER_TYPE tunerType, String dataLocationsConfigFilePath,
			String languageSuffix, TestRerankingSettings testRerankingSettings,
			SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations) {
		TestIdentifierPair systemWithReorderingLabelsAndReorderingBinaryFeatures = TestIdentifierPair
				.createTestIdentifierPair(
						MTPipelineHatsHiero.SystemName,
						MTPipelineHatsHiero.SystemName
								+ "-reorderingLabeled-Basic-OnlySourceSideLabeled"
								+ "-" + languageSuffix);
		testSystem(
				systemWithReorderingLabelsAndReorderingBinaryFeatures,
				TestLabelingSettings
						.createBasicTestReorderingLabelingSettingsOnlySourceSideLabeled(ALL_EMPTY_BOUNDARY_TAG_LABELING_SETTINGS),
				testRerankingSettings, tunerType, false,false, 10,
				dataLocationsConfigFilePath, sourceAndTargetLanguageAbbreviations,true,false);
		return systemWithReorderingLabelsAndReorderingBinaryFeatures;
	}

	public TestIdentifierPair testHieroPipelineWithReorderingLabelsBasicOnlyTargetSideLabeled(
			TUNER_TYPE tunerType, String dataLocationsConfigFilePath,
			String languageSuffix, TestRerankingSettings testRerankingSettings,
			SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations) {
		TestIdentifierPair systemWithReorderingLabelsAndReorderingBinaryFeatures = TestIdentifierPair
				.createTestIdentifierPair(
						MTPipelineHatsHiero.SystemName,
						MTPipelineHatsHiero.SystemName
								+ "-reorderingLabeled-Basic-OnlyTargetSideLabeled"
								+ "-" + languageSuffix);
		testSystem(
				systemWithReorderingLabelsAndReorderingBinaryFeatures,
				TestLabelingSettings
						.createBasicTestReorderingLabelingSettingsOnlyTargetSideLabeled(ALL_EMPTY_BOUNDARY_TAG_LABELING_SETTINGS),
				testRerankingSettings, tunerType, false,false, 10,
				dataLocationsConfigFilePath, sourceAndTargetLanguageAbbreviations,true,false);
		return systemWithReorderingLabelsAndReorderingBinaryFeatures;
	}

	public TestIdentifierPair testHieroPipelineWithReorderingLabelsAbstractRulesOnly(
			TUNER_TYPE tunerType, String dataLocationsConfigFilePath,
			String languageSuffix, TestRerankingSettings testRerankingSettings,
			SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations) {
		TestIdentifierPair systemWithReorderingLabelsAndReorderingBinaryFeatures = TestIdentifierPair
				.createTestIdentifierPair(MTPipelineHatsHiero.SystemName,
						MTPipelineHatsHiero.SystemName
								+ "-reorderingLabeled-AbstractRulesOnly" + "-"
								+ languageSuffix);
		testSystem(
				systemWithReorderingLabelsAndReorderingBinaryFeatures,
				TestLabelingSettings
						.createTestReorderingLabelingSettingsReorderLabelOnlyAbstractRules(ALL_EMPTY_BOUNDARY_TAG_LABELING_SETTINGS),
				testRerankingSettings, tunerType, false,false, 10,
				dataLocationsConfigFilePath, sourceAndTargetLanguageAbbreviations,true,false);
		return systemWithReorderingLabelsAndReorderingBinaryFeatures;
	}

	public TestIdentifierPair testHieroPipelineWithReorderingLabelsBasicNoLabelSmoothingAndPureHieroRulesAdded(
			TUNER_TYPE tunerType, String dataLocationsConfigFilePath,
			String languageSuffix, TestRerankingSettings testRerankingSettings,
			SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations) {
		TestIdentifierPair systemWithReorderingLabelsAndReorderingBinaryFeatures = TestIdentifierPair
				.createTestIdentifierPair(MTPipelineHatsHiero.SystemName,
						MTPipelineHatsHiero.SystemName
								+ "-reorderingLabeled-Basic" + "-"
								+ languageSuffix);
		testSystem(
				systemWithReorderingLabelsAndReorderingBinaryFeatures,
				TestLabelingSettings
						.createPureHieroRuleFallbackTestReorderingLabelingSettings(ALL_EMPTY_BOUNDARY_TAG_LABELING_SETTINGS),
				testRerankingSettings, tunerType, false,false, 10,
				dataLocationsConfigFilePath, sourceAndTargetLanguageAbbreviations,true,false);
		return systemWithReorderingLabelsAndReorderingBinaryFeatures;
	}

	public TestIdentifierPair testHieroPipelineWithReorderingLabelsAndBinaryReorderingFeatures(
			TUNER_TYPE tunerType, String dataLocationsConfigFilePath,
			String languageSuffix, TestRerankingSettings testRerankingSettings,
			SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations) {
		TestIdentifierPair systemWithReorderingLabelsAndReorderingBinaryFeatures = TestIdentifierPair
				.createTestIdentifierPair(
						MTPipelineHatsHiero.SystemName,
						MTPipelineHatsHiero.SystemName
								+ "-reorderingLabeled-WithBinaryReorderingFeatures"
								+ "-" + languageSuffix);
		testSystem(
				systemWithReorderingLabelsAndReorderingBinaryFeatures,
				TestLabelingSettings
						.createBasicTestReorderingLabelingSettingsWithBinaryFeatures(ALL_EMPTY_BOUNDARY_TAG_LABELING_SETTINGS),
				testRerankingSettings, tunerType, false,false, 10,
				dataLocationsConfigFilePath, sourceAndTargetLanguageAbbreviations,true,false);
		return systemWithReorderingLabelsAndReorderingBinaryFeatures;
	}

	public TestIdentifierPair testHieroPipelineWithHatRefinedReorderingLabels(
			TUNER_TYPE tunerType, String dataLocationsConfigFilePath,
			String languageSuffix, TestRerankingSettings testRerankingSettings,
			SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations) {
		TestIdentifierPair systemWithReorderingLabelsAndReorderingBinaryFeatures = TestIdentifierPair
				.createTestIdentifierPair(MTPipelineHatsHiero.SystemName,
						MTPipelineHatsHiero.SystemName
								+ "-reorderingLabeled-HatRefined" + "-"
								+ languageSuffix);
		testSystem(systemWithReorderingLabelsAndReorderingBinaryFeatures,
				TestLabelingSettings
						.createHatRefinedTestReorderingLabelingSettings(ALL_EMPTY_BOUNDARY_TAG_LABELING_SETTINGS),
				testRerankingSettings, tunerType, false,false, 10,
				dataLocationsConfigFilePath, sourceAndTargetLanguageAbbreviations,true,false);
		return systemWithReorderingLabelsAndReorderingBinaryFeatures;
	}
	
	
	private static TestLabelingSettings createTestReorderingLabelingSettingsParentRelativeLabel(boolean  outputOnlyCanonicalFormLabeledRules, boolean useOnlyHieroWeightsForCanonicalFormLabeledRules,boolean restrictNumberLabels, boolean useCoarseVariant){
	    	TestLabelingSettings testLabelingSettings = null;
        	if(restrictNumberLabels){
        	    if(useCoarseVariant){
        		testLabelingSettings = TestLabelingSettings.createTestReorderingLabelingSettingsCoarseParentRelativeLabelWithLimitedLabelTypesPerSource(outputOnlyCanonicalFormLabeledRules,useOnlyHieroWeightsForCanonicalFormLabeledRules,ALL_EMPTY_BOUNDARY_TAG_LABELING_SETTINGS);
        	    }
        	    else{
        		testLabelingSettings = TestLabelingSettings.createTestReorderingLabelingSettingsFineParentRelativeLabelWithLimitedLabelTypesPerSource(outputOnlyCanonicalFormLabeledRules,useOnlyHieroWeightsForCanonicalFormLabeledRules,ALL_EMPTY_BOUNDARY_TAG_LABELING_SETTINGS);
        	    }
        	}
        	else{
        	    if(useCoarseVariant){
        		testLabelingSettings = TestLabelingSettings.createTestReorderingLabelingSettingsCoarseParentRelativeLabel(outputOnlyCanonicalFormLabeledRules,useOnlyHieroWeightsForCanonicalFormLabeledRules,ALL_EMPTY_BOUNDARY_TAG_LABELING_SETTINGS);
        	    }
        	    else{
        		testLabelingSettings = TestLabelingSettings.createTestReorderingLabelingSettingsFineParentRelativeLabel(outputOnlyCanonicalFormLabeledRules,useOnlyHieroWeightsForCanonicalFormLabeledRules,ALL_EMPTY_BOUNDARY_TAG_LABELING_SETTINGS);
        	    }
        	}
        	return testLabelingSettings;
	}
	
	public TestIdentifierPair testHieroPipelineWithParentRelativeReorderingLabels(
		TUNER_TYPE tunerType, String dataLocationsConfigFilePath,
		String languageSuffix, TestRerankingSettings testRerankingSettings, boolean outputOnlyCanonicalFormLabeledRules,boolean useOnlyHieroWeightsForCanonicalFormLabeledRules, boolean fuzzyMatching, 
		boolean restrictNumberLabels,boolean useCoarseVariant, SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations,
		boolean performTuning) {
        	
	    	String testName = MTPipelineHatsHiero.SystemName
			+ "-reorderingLabeled-ParentRelative" + "-"
			+ languageSuffix;
	    	
	    	if(useCoarseVariant){
	    	    testName += "-COARSE";
	    	}
	    	
	    	if(restrictNumberLabels){
	    	    testName += "-restrictNumberLabels";
	    	}
	    	    
	    	TestIdentifierPair systemWithDoubleReorderingLabels = TestIdentifierPair
        			.createTestIdentifierPair(MTPipelineHatsHiero.SystemName,
        				testName);
        	TestLabelingSettings testLabelingSettings = createTestReorderingLabelingSettingsParentRelativeLabel(outputOnlyCanonicalFormLabeledRules, useOnlyHieroWeightsForCanonicalFormLabeledRules, restrictNumberLabels,useCoarseVariant);
        	    
        	testSystem(systemWithDoubleReorderingLabels,
        			testLabelingSettings,
        			testRerankingSettings, tunerType, false,fuzzyMatching, 10,
        			dataLocationsConfigFilePath, sourceAndTargetLanguageAbbreviations,performTuning,false);
        	return systemWithDoubleReorderingLabels;
	}
	
	
	public TestIdentifierPair testHieroPipelineWithCoarsePhraseCentricReorderingLabels(
		TUNER_TYPE tunerType, String dataLocationsConfigFilePath,
		String languageSuffix, TestRerankingSettings testRerankingSettings, boolean outputOnlyCanonicalFormLabeledRules,boolean useOnlyHieroWeightsForCanonicalFormLabeledRules, boolean fuzzyMatching, 
		SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations,
		boolean performTuning) {
        	
	    	String testName = MTPipelineHatsHiero.SystemName
			+ "-reorderingLabeled-PhraseCentric" + "-"
			+ languageSuffix;
	    	
	    	
	    	testName += "-COARSE";
	    	
	    
	    	    
	    	TestIdentifierPair systemWithDoubleReorderingLabels = TestIdentifierPair
        			.createTestIdentifierPair(MTPipelineHatsHiero.SystemName,
        				testName);
        	TestLabelingSettings testLabelingSettings = TestLabelingSettings.createTestReorderingLabelingSettingsCoarsePhraseCentricLabel(outputOnlyCanonicalFormLabeledRules, useOnlyHieroWeightsForCanonicalFormLabeledRules,ALL_EMPTY_BOUNDARY_TAG_LABELING_SETTINGS);
        	    
        	testSystem(systemWithDoubleReorderingLabels,
        			testLabelingSettings,
        			testRerankingSettings, tunerType, false,fuzzyMatching, 10,
        			dataLocationsConfigFilePath, sourceAndTargetLanguageAbbreviations,performTuning,false);
        	return systemWithDoubleReorderingLabels;
	}
	
	public TestIdentifierPair testHieroPipelineWithPhraseCentricPlusParentRelativeReorderingLabels(
		TUNER_TYPE tunerType, String dataLocationsConfigFilePath,
		String languageSuffix, TestRerankingSettings testRerankingSettings, boolean outputOnlyCanonicalFormLabeledRules,boolean useOnlyHieroWeightsForCanonicalFormLabeledRules, boolean fuzzyMatching,
		SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations) {
        	TestIdentifierPair systemWithDoubleReorderingLabels = TestIdentifierPair
        			.createTestIdentifierPair(MTPipelineHatsHiero.SystemName,
        					MTPipelineHatsHiero.SystemName
        							+ "-reorderingLabeled-HatAndParentRelative" + "-"
        							+ languageSuffix);
        	testSystem(systemWithDoubleReorderingLabels,
        			TestLabelingSettings.createTestReorderingLabelingSettingsHatAndParentRelativeLabel(outputOnlyCanonicalFormLabeledRules,useOnlyHieroWeightsForCanonicalFormLabeledRules,ALL_EMPTY_BOUNDARY_TAG_LABELING_SETTINGS),
        			testRerankingSettings, tunerType, false,fuzzyMatching, 10,
        			dataLocationsConfigFilePath, sourceAndTargetLanguageAbbreviations,true,false);
        	return systemWithDoubleReorderingLabels;
	}
	
	public TestIdentifierPair testHieroPipelineWithHATComplexityTypeReorderingLabels(
		TUNER_TYPE tunerType, String dataLocationsConfigFilePath,
		String languageSuffix, TestRerankingSettings testRerankingSettings, boolean outputOnlyCanonicalFormLabeledRules,boolean useOnlyHieroWeightsForCanonicalFormLabeledRules, boolean fuzzyMatching,
		SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations) {
        	TestIdentifierPair systemWithDoubleReorderingLabels = TestIdentifierPair
        			.createTestIdentifierPair(MTPipelineHatsHiero.SystemName,
        					MTPipelineHatsHiero.SystemName
        							+ "-reorderingLabeled-HatComplexityType" + "-"
        							+ languageSuffix);
        	testSystem(systemWithDoubleReorderingLabels,
        			TestLabelingSettings.createTestReorderingLabelingSettingsHatComplexityTypeReorderingLabel(outputOnlyCanonicalFormLabeledRules, useOnlyHieroWeightsForCanonicalFormLabeledRules,ALL_EMPTY_BOUNDARY_TAG_LABELING_SETTINGS),
        			testRerankingSettings, tunerType, false,fuzzyMatching, 10,
        			dataLocationsConfigFilePath, sourceAndTargetLanguageAbbreviations,true,false);
        	return systemWithDoubleReorderingLabels;
	}

	public TestIdentifierPair testSAMTPipeline(TUNER_TYPE tunerType,
			String dataLocationsConfigFilePath, boolean performAssertions,
			TestRerankingSettings testRerankingSettings,
			SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations) {
		TestIdentifierPair samtSystemIdentifierPair = TestIdentifierPair
				.createTestIdentifierPair(MTPipelineHATsSAMT.SystemName,
						MTPipelineHATsSAMT.SystemName + "-pure");
		testSystem(samtSystemIdentifierPair,
				TestLabelingSettings.createNoReorderingLabelingSettings(ALL_EMPTY_BOUNDARY_TAG_LABELING_SETTINGS,false),
				testRerankingSettings,tunerType, true,false, 10,
				dataLocationsConfigFilePath, sourceAndTargetLanguageAbbreviations,true,false);
		//if (performAssertions) {
		//	assert_X_In_Parse_Output_And_XSyntactic_In_Grammar_Input(samtSystemIdentifierPair);
		//}
		return samtSystemIdentifierPair;
	}
}