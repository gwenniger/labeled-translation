package integration_tests;

import org.junit.Test;
import mt_pipeline.mtPipelineTest.MTPipelineTestConfigFileCreater;
import mt_pipeline.mtPipelineTest.SourceAndTargetLanguageAbbreviations;
import mt_pipeline.mtPipelineTest.TestIdentifierPair;
import mt_pipeline.mtPipelineTest.TestLabelingSettings;
import mt_pipeline.mtPipelineTest.TestRerankingSettings;

public class TestWordOrderImprovementForLabelTypes extends MTPipelineTest{


        
        @Override
        protected MTPipelineTestConfigFileCreater createMTPipelineTestConfigFileCreater(
    	    TestIdentifierPair testIdentifierPair,
    	    TestLabelingSettings testReorderingLabelingSettings,
    	    TestRerankingSettings testRerankingSettings, TUNER_TYPE tunerType,
    	    boolean parseTargetFile, int trainDataLength, String testDataLocationsConfigFile,
    	    boolean useSoftSyntacticConstraintDecoding,
    	    SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations,
    	    boolean performTuning,boolean use_rule_application_features) {

    	return createMTPipelineTestConfigFileCreater(testIdentifierPair,
    		testReorderingLabelingSettings, testRerankingSettings, tunerType, parseTargetFile,
    		trainDataLength, testDataLocationsConfigFile, false,
    		useSoftSyntacticConstraintDecoding, sourceAndTargetLanguageAbbreviations,
    		performTuning,false);
        }
        @Override
        protected boolean isMosesSystem() {
    	return false;
        }


        //@Test
        public void testParentRelativeReorderingLabels() {
    	testHieroPipelineWithParentRelativeReorderingLabels(TUNER_TYPE.MERT_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_DutchEnglishReorderingTest,
    		"NeEn", TestRerankingSettings.createNoRerankingSettings(),
    		false, false, false, false, false, SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_NE_EN, false);
        }

        @Test
        public void testPhraseCentricReorderingLabels() {
    	testHieroPipelineWithPhraseCentricPlusParentRelativeReorderingLabels(TUNER_TYPE.MERT_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_DutchEnglishReorderingTest,
    		"NeEn", TestRerankingSettings.createNoRerankingSettings(),
    		false, false, false, SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_NE_EN);
        }
        
        
        //@Test
        public void testHieroPipelinePro() {
    	testHieroPipeline(TUNER_TYPE.PRO_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_DE_EN,
    		"DeEn", TestRerankingSettings.createNoRerankingSettings(),
    		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_DE_EN, false);
        }

        //@Test
        public void testHieroPipelineMIRA() {
    	testHieroPipeline(TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_DE_EN,
    		"DeEn", TestRerankingSettings.createNoRerankingSettings(),
    		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_DE_EN, false);
        }

        
    }
