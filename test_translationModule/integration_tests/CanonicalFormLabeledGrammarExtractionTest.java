package integration_tests;

import grammarExtraction.chiangGrammarExtraction.ChiangRuleCreater;
import grammarExtraction.chiangGrammarExtraction.MultiThreadGrammarExtractorTestFiltered;
import grammarExtraction.grammarExtractionFoundation.LabelingSettings.CanonicalFormSettings;
import grammarExtraction.grammarExtractionFoundation.MultiThreadGrammarExtractor;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.labelSmoothing.LabeledRulesSmoother.LABEL_SMOOTHING_TYPE;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.RuleGap;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import junit.framework.Assert;
import org.junit.Test;
import util.FileUtil;
import artificalCorpusCreation.ArtificialTranslationCorpusCreater;

public class CanonicalFormLabeledGrammarExtractionTest {

    private static final String TEST_CORPUS_PATH = "./testCorpusCanonicalLabeledRuleGrammarExtraction/";

    public static void createTestCorpus(String testCorpusFolderName, int numCopiesMono,
	    int numCopiesAtomic, int numCopiesHAT) {

	try {
	    ArtificialTranslationCorpusCreater
		    .createToyTranslationArtificialCorpusCreatorFrenchEnglishCanonicalFormRulesTesting(
			    testCorpusFolderName, true, numCopiesMono, numCopiesAtomic,
			    numCopiesHAT);

	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    private String test(boolean useCanonicalFormLabeledRules, String grammarFileName,
	    int numCopiesMono, int numCopiesAtomic, int numCopiesHAT) {
	FileUtil.createFolderIfNotExisting(TEST_CORPUS_PATH);
	createTestCorpus(TEST_CORPUS_PATH, numCopiesMono, numCopiesAtomic, numCopiesHAT);

	String testCorpusFolderName = TEST_CORPUS_PATH;
	FileUtil.createFolderIfNotExisting(testCorpusFolderName);
	String configFileName = testCorpusFolderName + "configFile.txt";
	int numThreads = 1;
	SystemIdentity systemIdentity = SystemIdentity
		.createStandardReorderingLabeledSystemIdentity(CanonicalFormSettings
			.createCanonicalFormSettings(useCanonicalFormLabeledRules));
	MultiThreadGrammarExtractor grammarExtractor = MultiThreadGrammarExtractorTestFiltered
		.createMultiThreadGrammarExtractorTestFilteredHiero(configFileName, numThreads,
			systemIdentity);
	// Assert.assertTrue(systemIdentity.useCanonicalFormLabeledRules());
	Assert.assertTrue(systemIdentity.useReorderingLabelExtension());
	String createdGrammarFilePath = testCorpusFolderName + grammarFileName;
	String gapLabelConditionalProbabilityTableFilePath = testCorpusFolderName + "GapLabelConditionalProbabilityTableFile.txt";
	grammarExtractor.extractGrammarMultiThreadAndWriteResultToFile(createdGrammarFilePath,gapLabelConditionalProbabilityTableFilePath,
		false);
	return createdGrammarFilePath;
    }

    private CanonicalFormSettings createCanonicalFormSettingsWithRestrictedLabeling(
	    int numAllowedTypesPerLabelForSource) {
	return CanonicalFormSettings.createCanonicalFormSettings(true, true,
		LABEL_SMOOTHING_TYPE.NO_LABEL_SMOOTHING, true, numAllowedTypesPerLabelForSource);
	// return CanonicalFormSettings.createCanonicalFormSettings(true);

    }

    private String testWithRestrictedNumberOfLabelingVersionsPerSource(boolean useCanonicalFormLabeledRules, String grammarFileName,
	    int numCopiesMono, int numCopiesAtomic, int numCopiesHAT,
	    int numAllowedTypesPerLabelForSource) {
	FileUtil.createFolderIfNotExisting(TEST_CORPUS_PATH);
	createTestCorpus(TEST_CORPUS_PATH, numCopiesMono, numCopiesAtomic, numCopiesHAT);

	String testCorpusFolderName = TEST_CORPUS_PATH;
	FileUtil.createFolderIfNotExisting(testCorpusFolderName);
	String configFileName = testCorpusFolderName + "configFile.txt";
	int numThreads = 1;
	SystemIdentity systemIdentity = SystemIdentity
		.createStandardReorderingLabeledSystemIdentity(createCanonicalFormSettingsWithRestrictedLabeling(numAllowedTypesPerLabelForSource));
	MultiThreadGrammarExtractor grammarExtractor = MultiThreadGrammarExtractorTestFiltered
		.createMultiThreadGrammarExtractorTestFilteredHiero(configFileName, numThreads,
			systemIdentity);
	// Assert.assertTrue(systemIdentity.useCanonicalFormLabeledRules());
	Assert.assertTrue(systemIdentity.useReorderingLabelExtension());
	String createdGrammarFilePath = testCorpusFolderName + grammarFileName;
	String gapLabelConditionalProbabilityTableFilePath = testCorpusFolderName + "GapLabelConditionalProbabilityTableFile.txt";
	grammarExtractor.extractGrammarMultiThreadAndWriteResultToFile(createdGrammarFilePath,
		gapLabelConditionalProbabilityTableFilePath,
		false);
	return createdGrammarFilePath;

    }

    private void assertOnlyOneVersionOfLabelingOccursForEveryUnlabeledVersionRule(
	    String createdGrammarFilePath) {
	FullRuleVersionOccurenceChecker fullRuleVersionOccurenceChecker = new FullRuleVersionOccurenceChecker();
	Assert.assertTrue(fullRuleVersionOccurenceChecker
		.unlabeledRuleVersionOccursWithOnlyOneTypeOfLabels(createdGrammarFilePath));
    }

    private void assertMultipleVersionsOfLabelingOccurForSomeUnlabeledVersionRule(
	    String createdGrammarFilePath) {
	FullRuleVersionOccurenceChecker fullRuleVersionOccurenceChecker = new FullRuleVersionOccurenceChecker();
	Assert.assertFalse(fullRuleVersionOccurenceChecker
		.unlabeledRuleVersionOccursWithOnlyOneTypeOfLabels(createdGrammarFilePath));
    }

    private void assertOnlyOneVersionOfLabelingOccursForEveryUnlabeledSource(
	    String createdGrammarFilePath) {
	SourceSideOnlyRuleVersionOccurenceChecker sourceSideOnlyRuleVersionOccurenceChecker = new SourceSideOnlyRuleVersionOccurenceChecker();
	Assert.assertTrue(sourceSideOnlyRuleVersionOccurenceChecker
		.unlabeledSourceVersionOccursWithOnlyOneTypeOfLabels(createdGrammarFilePath));
    }

    private void assertMultipleVersionsOfLabelingOccurForSomeUnlabeledSource(
	    String createdGrammarFilePath) {
	SourceSideOnlyRuleVersionOccurenceChecker sourceSideOnlyRuleVersionOccurenceChecker = new SourceSideOnlyRuleVersionOccurenceChecker();
	Assert.assertFalse(sourceSideOnlyRuleVersionOccurenceChecker
		.unlabeledSourceVersionOccursWithOnlyOneTypeOfLabels(createdGrammarFilePath));
    }

    private boolean fileContainsPrefix(String prefix, String createdGrammarFilePath) {
	for (String line : FileUtil.getSentences(new File(createdGrammarFilePath), true)) {
	    if (line.startsWith(prefix)) {
		return true;
	    }
	}
	return false;
    }

    private boolean containsMonotoneLabeledVersionFullyLexicalizedRule(String createdGrammarFilePath) {
	String monotoneVersionRulePrefix = "[X-MONO] ||| de man loopt ||| the man walks |||";
	return fileContainsPrefix(monotoneVersionRulePrefix, createdGrammarFilePath);
    }

    private boolean containsAtomicLabeledVersionFullyLexicalizedRule(String createdGrammarFilePath) {
	String monotoneVersionRulePrefix = "[X-ATOMIC] ||| de man loopt ||| the man walks |||";
	return fileContainsPrefix(monotoneVersionRulePrefix, createdGrammarFilePath);
    }

    private boolean containsHATLabeledVersionFullyLexicalizedRule(String createdGrammarFilePath) {
	String monotoneVersionRulePrefix = "[X-HAT] ||| de man loopt ||| the man walks |||";
	return fileContainsPrefix(monotoneVersionRulePrefix, createdGrammarFilePath);
    }

    @Test
    public void testOnlyOneLabelVersionRulesWithCanonicalFormExtraction() {
	String createdGrammarFilePath = test(true,
		"translationGrammarSourceFilteredCanonicalForm.txt", 1, 1, 1);
	assertOnlyOneVersionOfLabelingOccursForEveryUnlabeledVersionRule(createdGrammarFilePath);
    }

    @Test
    public void testWithMoreMonotoneInstancesBecomesMonotone() {
	String createdGrammarFilePath = test(true,
		"translationGrammarSourceFilteredCanonicalFormMoreMonotone.txt", 100, 1, 10);
	assertOnlyOneVersionOfLabelingOccursForEveryUnlabeledVersionRule(createdGrammarFilePath);
	Assert.assertTrue(containsMonotoneLabeledVersionFullyLexicalizedRule(createdGrammarFilePath));
	Assert.assertFalse(containsAtomicLabeledVersionFullyLexicalizedRule(createdGrammarFilePath));
	Assert.assertFalse(containsHATLabeledVersionFullyLexicalizedRule(createdGrammarFilePath));
    }

    // This test failed with the bug: Atomic comes second, but with the bug
    // there was HAT after that
    // which was more frequent then Mono and become the best if the highestFreq
    // is not stored
    // @Test
    public void testWithMoreAtomicInstancesBecomesAtomic() {
	System.out.println("ATOMIC TEST");
	String createdGrammarFilePath = test(true,
		"translationGrammarSourceFilteredCanonicalFormMoreAtomic.txt", 1, 100, 10);
	assertOnlyOneVersionOfLabelingOccursForEveryUnlabeledVersionRule(createdGrammarFilePath);
	Assert.assertFalse(containsMonotoneLabeledVersionFullyLexicalizedRule(createdGrammarFilePath));
	Assert.assertTrue(containsAtomicLabeledVersionFullyLexicalizedRule(createdGrammarFilePath));
	Assert.assertFalse(containsHATLabeledVersionFullyLexicalizedRule(createdGrammarFilePath));
    }

    @Test
    public void testWithMoreHATInstancesBecomesHAT() {
	System.out.println("HAT TEST");
	String createdGrammarFilePath = test(true,
		"translationGrammarSourceFilteredCanonicalFormMoreHAT.txt", 1, 10, 21);
	assertOnlyOneVersionOfLabelingOccursForEveryUnlabeledVersionRule(createdGrammarFilePath);
	Assert.assertFalse(containsMonotoneLabeledVersionFullyLexicalizedRule(createdGrammarFilePath));
	Assert.assertFalse(containsAtomicLabeledVersionFullyLexicalizedRule(createdGrammarFilePath));
	Assert.assertTrue(containsHATLabeledVersionFullyLexicalizedRule(createdGrammarFilePath));
    }

    @Test
    public void testMultipleLabelVersionRulesWithoutCanonicalFormExtraction() {
	String createdGrammarFilePath = test(false,
		"translationGrammarSourceFilteredNormalForm.txt", 1, 1, 1);
	assertMultipleVersionsOfLabelingOccurForSomeUnlabeledVersionRule(createdGrammarFilePath);
    }

    @Test
    public void testNumberOfLabelVersionsRestrictedCanonicalRuleGrammarExtraction() {
	String createdGrammarFilePath = testWithRestrictedNumberOfLabelingVersionsPerSource(true,
		"translationGrammarSourceFilteredNormalFormRestrictedLabeling.txt", 1, 1, 1, 3);
	assertOnlyOneVersionOfLabelingOccursForEveryUnlabeledVersionRule(createdGrammarFilePath);
	Assert.assertTrue(fileContainsPrefix("[X-HAT] ||| de man loopt ||| the person moves ",
		createdGrammarFilePath));
	Assert.assertFalse(fileContainsPrefix("[X-ATOMIC] ||| de man loopt ||| the person moves ",
		createdGrammarFilePath));
	assertMultipleVersionsOfLabelingOccurForSomeUnlabeledSource(createdGrammarFilePath);
    }

    @Test
    public void testNumberOfLabelVersionsRestrictedCanonicalRuleGrammarExtractionOnlyOneLabelingVariantPerSource() {
	String createdGrammarFilePath = testWithRestrictedNumberOfLabelingVersionsPerSource(true,
		"translationGrammarSourceFilteredNormalFormRestrictedLabelingOneVariant.txt", 1, 1,
		1, 1);
	assertOnlyOneVersionOfLabelingOccursForEveryUnlabeledVersionRule(createdGrammarFilePath);
	Assert.assertFalse(fileContainsPrefix("[X-HAT] ||| de man loopt ||| the person moves ",
		createdGrammarFilePath));
	Assert.assertTrue(fileContainsPrefix("[X-ATOMIC] ||| de man loopt ||| the person moves ",
		createdGrammarFilePath));
	assertOnlyOneVersionOfLabelingOccursForEveryUnlabeledSource(createdGrammarFilePath);
    }

    private static abstract class RuleVersionOccurenceChecker {

	private String getLabelInformationRemovedRulePart(String originalRulePart) {
	    if (WordKeyMappingTable.isRuleLabel(originalRulePart)) {
		return ChiangRuleCreater.ChiangRuleLabel;
	    } else if (WordKeyMappingTable.isLabelOne(originalRulePart)) {
		return RuleGap.ruleGapString(ChiangRuleCreater.ChiangLabel, 1);
	    } else if (WordKeyMappingTable.isLabelTwo(originalRulePart)) {
		return RuleGap.ruleGapString(ChiangRuleCreater.ChiangLabel, 2);
	    } else {
		return originalRulePart;
	    }
	}

	private String convertRulePart(String rulePart) {
	    String rulePartConverted = "";
	    String[] ruleSubParts = rulePart.split("\\s");
	    for (String ruleSubPart : ruleSubParts) {
		rulePartConverted = rulePartConverted + " "
			+ getLabelInformationRemovedRulePart(ruleSubPart);
	    }
	    rulePartConverted = rulePartConverted.trim();
	    return rulePartConverted;

	}

	protected String getUnlabeldVersionRule(String ruleString) {
	    String result = "";
	    String[] ruleParts = ruleString.split("\\|\\|\\|");
	    for (int i = 0; i < ruleParts.length - 1; i++) {
		result = result + convertRulePart(ruleParts[i]) + " ||| ";
	    }
	    if (ruleParts.length > 0) {
		result = result + convertRulePart(ruleParts[ruleParts.length - 1]);
	    }
	    return result;
	}

	protected boolean unlabeledVersionRuleAlreadyEncountered(
		Map<String, String> unlabeledToLabeledRuleMap, String unlabeledVersionRule) {
	    return unlabeledToLabeledRuleMap.containsKey(unlabeledVersionRule);
	}

	protected boolean isUnknownWordRule(String ruleString) {
	    return ruleString.contains("UnknownWord,1]");
	}

    }

    private class FullRuleVersionOccurenceChecker extends RuleVersionOccurenceChecker {

	protected String getLabelPartRule(String ruleString) {
	    System.out.println("ruleString: " + ruleString);
	    return ruleString.substring(0, ruleString.lastIndexOf(" ||| "));
	}

	boolean unlabeledRuleVersionOccursWithOnlyOneTypeOfLabels(String resultFilePath) {
	    List<String> lines = FileUtil.getSentences(new File(resultFilePath), true);
	    Map<String, String> unlabeledToLabeledRuleMap = new HashMap<String, String>();

	    for (String line : lines) {
		if (line != "") {
		    String labelsPartRule = getLabelPartRule(line);
		    System.out.println("labelsPartRule: " + labelsPartRule);
		    String unlabeledVersionRule = getUnlabeldVersionRule(labelsPartRule);
		    System.out.println("unlabeledVersionRule: " + unlabeledVersionRule);
		    if (!isUnknownWordRule(labelsPartRule)) {
			if (unlabeledVersionRuleAlreadyEncountered(unlabeledToLabeledRuleMap,
				unlabeledVersionRule)) {
			    System.out.println(">>>> Already encountered unlabeled version rule: "
				    + "\"" + unlabeledVersionRule + "\"" + "\n\t>>>> "
				    + "Previously observed version: " + "\""
				    + unlabeledToLabeledRuleMap.get(unlabeledVersionRule) + "\""
				    + "\n\t>>>> " + "Newly observed version: " + "\""
				    + labelsPartRule + "\"");
			    return false;
			}
		    }
		    unlabeledToLabeledRuleMap.put(unlabeledVersionRule, labelsPartRule);
		}

	    }
	    return true;
	}

    }

    private static class SourceSideOnlyRuleVersionOccurenceChecker extends
	    RuleVersionOccurenceChecker {

	protected String getLabelPartRule(String ruleString) {
	    System.out.println("SourceSideOnlyRuleVersionOccurenceChecker - ruleString: "
		    + ruleString);
	    String tempString = ruleString.substring(0, ruleString.lastIndexOf(" ||| "));
	    String result = tempString.substring(0, tempString.lastIndexOf(" ||| "));
	    System.out.println("\tSourceSideOnlyRuleVersionOccurenceChecker - result: " + result);
	    return result;
	}

	public boolean unlabeledSourceVersionOccursWithOnlyOneTypeOfLabels(String resultFilePath) {
	    List<String> lines = FileUtil.getSentences(new File(resultFilePath), true);
	    Map<String, String> unlabeledToLabeledRuleMap = new HashMap<String, String>();

	    for (String line : lines) {
		if (line != "") {
		    String labelsPartRule = getLabelPartRule(line);
		    System.out.println("labelsPartRule: " + labelsPartRule);
		    String unlabeledVersionRule = getUnlabeldVersionRule(labelsPartRule);
		    System.out.println("unlabeledVersionRule: " + unlabeledVersionRule);
		    if (!isUnknownWordRule(labelsPartRule)) {
			if (unlabeledVersionRuleAlreadyEncountered(unlabeledToLabeledRuleMap,
				unlabeledVersionRule)) {
			    String previouslyLabeledVersionSource = unlabeledToLabeledRuleMap
				    .get(unlabeledVersionRule);
			    System.out.println(">>>> Already encountered unlabeled version rule: "
				    + "\"" + unlabeledVersionRule + "\"" + "\n\t>>>> "
				    + "Previously observed version: " + "\""
				    + previouslyLabeledVersionSource + "\"" + "\n\t>>>> "
				    + "Newly observed version: " + "\"" + labelsPartRule + "\"");

			    if (!labelsPartRule.equals(previouslyLabeledVersionSource)) {
				return false;
			    }
			}
		    }
		    unlabeledToLabeledRuleMap.put(unlabeledVersionRule, labelsPartRule);
		}

	    }
	    return true;
	}
    }

}
