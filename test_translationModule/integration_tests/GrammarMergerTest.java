package integration_tests;

import grammarExtraction.grammarMerging.GrammarMerger;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Test;

import util.FileUtil;

public class GrammarMergerTest {

    private static final String GRAMMAR1_FILE_PATH = "GrammarMergerTestGrammar1.txt";
    private static final String GRAMMAR2_FILE_PATH = "GrammarMergerTestGrammar2.txt";
    private static final String MERGE_RESULT_TEMP_GRAMMAR_FILE_PATH = "GrammarMergerTestTempMergedGrammar.txt";
    private static final String MERGE_RESULT_GRAMMAR_FILE_PATH = "GrammarMergerTestResultGrammar.txt";
    private static final int NUM_SORTING_THREADS = 3;  

    private static List<String> GRAMMAR1_RULE_LIST = Arrays
	    .asList("[WHPP+JJ] ||| 中 [WDT,1] 许多 ||| in [WDT,1] many ||| -0.0 0.6162113 1.5964622 0.6004485 1.1075144 0.6004485 -0.0 1.9456397 1.1075144 1.9456397 2.9668062 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 2.0 1.770795",
		    "[WHPP+JJ] ||| 中 [WDT+JJ,1] ||| in [WDT+JJ,1] ||| -0.0 0.47046044 1.322667 1.0607293 4.045566 1.0607293 -0.0 5.142246 1.0527722 2.1494522 2.1428974 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 1.0 0.15611805",
		    "[WHPP+JJ] ||| 中 ， [WDT+JJ,1] ||| in [WDT+JJ,1] ||| -0.0 1.9961917 1.2797054 2.2156312 3.878981 2.2156312 -0.0 6.2971478 0.8204275 3.238594 3.2977993 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 1.0 2.2255409",
		    "[WHPP+JJ] ||| 中 的 [JJ,1] ||| in which [JJ,1] ||| -0.0 1.3459188 3.351564 2.4976273 4.2562256 2.4976273 -0.0 3.8477564 3.322913 2.9144437 3.2008893 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 2.0 2.117");

    private static List<String> GRAMMAR2_RULE_LIST = Arrays
	    .asList("[WHNP] ||| [WDT,1] 种 问题 [CC+NNS,2] ||| [WDT,1] kind of problems [CC+NNS,2] ||| -0.0 3.285843 1.2411513 -0.0 3.4531133 -0.0 -0.0 0.47712126 2.9759922 -0.0 5.394186 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 3.0 2.629166",
		    "[WHNP] ||| [WDT,1] 科学 ||| [WDT,1] science ||| -0.0 0.4959222 0.17652032 -0.0 4.2810974 -0.0 -0.0 3.857013 0.5144152 0.0903306 4.394186 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 1.0 1.947734",
		    "[WHNP] ||| [WDT,1] 积极 [NNS,2] ||| [WDT,1] positive [NNS,2] ||| -0.0 0.5228994 0.14687839 -0.0 4.717424 -0.0 -0.0 4.408266 0.68552196 0.37636402 4.7621627 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 1.0 2.3564184",
		    "[WHPP+JJ] ||| 中 ， [WDT+JJ,1] ||| in [WDT+JJ,1] ||| 100000 1.2797054 2.2156312 3.878981 2.2156312 -0.0 6.2971478 0.8204275 3.238594 3.2977993 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 1.0 2.2255409");

    private static Set<String> MERGE_RESULT_EXPECTED_RULE_SET = new HashSet<String>(
	    Arrays.asList(
		    "[WHPP+JJ] ||| 中 [WDT,1] 许多 ||| in [WDT,1] many ||| -0.0 0.6162113 1.5964622 0.6004485 1.1075144 0.6004485 -0.0 1.9456397 1.1075144 1.9456397 2.9668062 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 2.0 1.770795",
		    "[WHPP+JJ] ||| 中 [WDT+JJ,1] ||| in [WDT+JJ,1] ||| -0.0 0.47046044 1.322667 1.0607293 4.045566 1.0607293 -0.0 5.142246 1.0527722 2.1494522 2.1428974 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 1.0 0.15611805",
		    "[WHPP+JJ] ||| 中 ， [WDT+JJ,1] ||| in [WDT+JJ,1] ||| -0.0 1.9961917 1.2797054 2.2156312 3.878981 2.2156312 -0.0 6.2971478 0.8204275 3.238594 3.2977993 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 1.0 2.2255409",
		    "[WHPP+JJ] ||| 中 的 [JJ,1] ||| in which [JJ,1] ||| -0.0 1.3459188 3.351564 2.4976273 4.2562256 2.4976273 -0.0 3.8477564 3.322913 2.9144437 3.2008893 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 2.0 2.117",
		    "[WHNP] ||| [WDT,1] 种 问题 [CC+NNS,2] ||| [WDT,1] kind of problems [CC+NNS,2] ||| -0.0 3.285843 1.2411513 -0.0 3.4531133 -0.0 -0.0 0.47712126 2.9759922 -0.0 5.394186 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 3.0 2.629166",
		    "[WHNP] ||| [WDT,1] 科学 ||| [WDT,1] science ||| -0.0 0.4959222 0.17652032 -0.0 4.2810974 -0.0 -0.0 3.857013 0.5144152 0.0903306 4.394186 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 1.0 1.947734",
		    "[WHNP] ||| [WDT,1] 积极 [NNS,2] ||| [WDT,1] positive [NNS,2] ||| -0.0 0.5228994 0.14687839 -0.0 4.717424 -0.0 -0.0 4.408266 0.68552196 0.37636402 4.7621627 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 1.0 2.3564184"));

    private void createTestGrammars() {
	FileUtil.writeLinesListToFile(GRAMMAR1_RULE_LIST, GRAMMAR1_FILE_PATH);
	FileUtil.writeLinesListToFile(GRAMMAR2_RULE_LIST, GRAMMAR2_FILE_PATH);
    }

    @Test
    public void testGrammarMerger() {
	createTestGrammars();

	GrammarMerger grammarMerger = GrammarMerger.createGrammarMerger(
		Arrays.asList(GRAMMAR1_FILE_PATH, GRAMMAR2_FILE_PATH),
		MERGE_RESULT_TEMP_GRAMMAR_FILE_PATH, MERGE_RESULT_GRAMMAR_FILE_PATH,NUM_SORTING_THREADS);
	grammarMerger.computeAndWriteMergedGrammar();
	List<String> mergeResultFileRuleLines = FileUtil.getLinesInFile(
		MERGE_RESULT_GRAMMAR_FILE_PATH, false);
	Set<String> mergeResultFileRuleSet = new HashSet<String>(mergeResultFileRuleLines);
	Assert.assertEquals(MERGE_RESULT_EXPECTED_RULE_SET, mergeResultFileRuleSet);
    }

}
