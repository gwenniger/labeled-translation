package integration_tests;

import integration_tests.MTPipelineTest.TUNER_TYPE;
import mt_pipeline.mtPipelineTest.MTPipelineTestConfigFileCreater;
import mt_pipeline.mtPipelineTest.SourceAndTargetLanguageAbbreviations;
import mt_pipeline.mtPipelineTest.TestIdentifierPair;
import mt_pipeline.mtPipelineTest.TestLabelingSettings;
import mt_pipeline.mtPipelineTest.TestRerankingSettings;

import org.junit.Test;

/**
 * 
 * This limited test of Hiero, with Joshua and the three different tuning algorithms
 * MERT, PRO and MIRA is meant to test - relatively quickly - if all three tuners work.
 *
 */
public class TestDifferentTuningAlgorithmsJoshuaForHiero extends MTPipelineTest{
    
    @Override
    protected MTPipelineTestConfigFileCreater createMTPipelineTestConfigFileCreater(
	    TestIdentifierPair testIdentifierPair,
	    TestLabelingSettings testReorderingLabelingSettings,
	    TestRerankingSettings testRerankingSettings, TUNER_TYPE tunerType,
	    boolean parseTargetFile, int trainDataLength, String testDataLocationsConfigFile,
	    boolean useSoftSyntacticConstraintDecoding,
	    SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations,
	    boolean performTuning,boolean use_rule_application_features) {

	return createMTPipelineTestConfigFileCreater(testIdentifierPair,
		testReorderingLabelingSettings, testRerankingSettings, tunerType, parseTargetFile,
		trainDataLength, testDataLocationsConfigFile, false,
		useSoftSyntacticConstraintDecoding, sourceAndTargetLanguageAbbreviations,
		performTuning,use_rule_application_features);
    }
    @Override
    protected boolean isMosesSystem() {
	return false;
    }


    @Test
    public void testHieroPipelineMert() {
	testHieroPipeline(TUNER_TYPE.MERT_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_DE_EN,
		"DeEn", TestRerankingSettings.createNoRerankingSettings(),
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_DE_EN, false);
    }

    
    @Test
    public void testHieroPipelinePro() {
	testHieroPipeline(TUNER_TYPE.PRO_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_DE_EN,
		"DeEn", TestRerankingSettings.createNoRerankingSettings(),
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_DE_EN, false);
    }

    @Test
    public void testHieroPipelineMIRA() {
	testHieroPipeline(TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_DE_EN,
		"DeEn", TestRerankingSettings.createNoRerankingSettings(),
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_DE_EN, false);
    }

    
}
