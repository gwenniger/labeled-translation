package integration_tests;

import java.util.ArrayList;
import java.util.List;

import mt_pipeline.mtPipelineTest.MTPipelineTestConfigFileCreater;
import mt_pipeline.mtPipelineTest.SourceAndTargetLanguageAbbreviations;
import mt_pipeline.mtPipelineTest.TestIdentifierPair;
import mt_pipeline.mtPipelineTest.TestLabelingSettings;
import mt_pipeline.mtPipelineTest.TestRerankingSettings;

import org.junit.Assert;
import org.junit.Test;

public class MTPipelineTestMoses extends MTPipelineTest {

	public static String FULLY_MONOTONE_RULE_STRING_MOSES = "[X][X-MONO] [X][X-MONO] [X] ||| [X][X-MONO] [X][X-MONO] [X-MONO]";
	public static String MONOTONE_RULE_STRING_MOSES_FIRST_SMOOTHED = "[X][X] [X][X-MONO] [X] ||| [X][X] [X][X-MONO] [X-MONO]";
	public static String MONOTONE_RULE_STRING_MOSES_SECOND_SMOOTHED = "[X][X-MONO] [X][X] [X] ||| [X][X-MONO] [X][X] [X-MONO]";
	public static String MONOTONE_RULE_STRING_MOSES_FULL_SMOOTHED = "[X][X] [X][X] [X] ||| [X][X] [X][X] [X-MONO]";
	
	public static String FULLY_INVERTED_RULE_STRING_MOSES = "[X][X-BITT] [X][X-BITT] [X] ||| [X][X-BITT] [X][X-BITT] [X-BITT]";
	public static String INVERTED_RULE_STRING_MOSES_FIRST_SMOOTHED = "[X][X] [X][X-BITT] [X] ||| [X][X-BITT] [X][X] [X-BITT]";
	public static String INVERTED_RULE_STRING_MOSES_SECOND_SMOOTHED = "[X][X-BITT] [X][X] [X] ||| [X][X] [X][X-BITT] [X-BITT]";
	public static String INVERTED_RULE_STRING_MOSES_FULL_SMOOTHED = "[X][X] [X][X] [X] ||| [X][X] [X][X] [X-BITT]";

	
	@Override
	protected MTPipelineTestConfigFileCreater createMTPipelineTestConfigFileCreater(TestIdentifierPair testIdentifierPair, TestLabelingSettings testReorderingLabelingSettings,
			TestRerankingSettings testRerankingSettings, TUNER_TYPE tunerType, boolean parseTargetFile, int trainDataLength, String testDataLocationsConfigFile, boolean useSoftSyntacticConstraintDecoding,
			SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations,
			boolean performTuning,boolean use_rule_application_features) {

		return createMTPipelineTestConfigFileCreater(testIdentifierPair, testReorderingLabelingSettings, testRerankingSettings,tunerType, parseTargetFile, trainDataLength,
				testDataLocationsConfigFile, true,false,sourceAndTargetLanguageAbbreviations,performTuning,false);
	}

	//@Test
	public void testHieroPipelineAndHieroPipelineWithReorderingLabelsMert() {
		testHieroPipelineAndHieroPipelineWithReorderingLabels(TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_DE_EN, TestRerankingSettings.createNoRerankingSettings(),false,SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_DE_EN);
	}

	//@Test
	public void testHieroPipelineAndHieroPipelineWithReorderingLabelsMert2() {
		testHieroPipelineAndHieroPipelineWithReorderingLabels(TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN, TestRerankingSettings.createNoRerankingSettings(),false,SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN);
	}

	//@Test
	public void testHieroPipelineWithReorderingLabelsMert2() {
		testHieroPipelineWithReorderingLabelsBasic(TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN, "FrEn", TestRerankingSettings.createNoRerankingSettings(),false,false,false,SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN, true,false);
	}
	
	//@Test
	public void testHieroPipelineWithReorderingLabelsOnlySourceSideLabeled() {
		testHieroPipelineWithReorderingLabelsBasicOnlySourceSideLabeled(TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN, "EnFr", TestRerankingSettings.createNoRerankingSettings(),SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN);
	}
	
	//@Test
	public void testHieroPipelineWithReorderingLabelsOnlyTargetSideLabeled() {
		testHieroPipelineWithReorderingLabelsBasicOnlyTargetSideLabeled(TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN, "EnFr", TestRerankingSettings.createNoRerankingSettings(),SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN);
	}
	
	//@Test
	public void testOneSideLabeledSystemsHaveSameNumberOfRulesAsTwoSidesLabeledSystems()
	{
		TestIdentifierPair tipBothSidesLabeled = testHieroPipelineWithReorderingLabelsBasic(TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN, "EnFr", TestRerankingSettings.createNoRerankingSettings(),false, false,false,SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN, true,false);
		TestIdentifierPair tipSourceSideLabeled = testHieroPipelineWithReorderingLabelsBasicOnlySourceSideLabeled(TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN, "EnFr", TestRerankingSettings.createNoRerankingSettings(),SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN);
		TestIdentifierPair tipTargetSideLabeled = testHieroPipelineWithReorderingLabelsBasicOnlyTargetSideLabeled(TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN, "EnFr", TestRerankingSettings.createNoRerankingSettings(),SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN);
		
		int numDevRulesBothSidesLabeled = getNumberOfRulesMertGrammar(tipBothSidesLabeled);
		int numDevRulesSourceSideLabeled = getNumberOfRulesMertGrammar(tipSourceSideLabeled);
		int numDevRulesTargetSideLabeled = getNumberOfRulesMertGrammar(tipTargetSideLabeled);
		
		Assert.assertEquals(numDevRulesBothSidesLabeled, numDevRulesSourceSideLabeled);
		Assert.assertEquals(numDevRulesBothSidesLabeled, numDevRulesTargetSideLabeled);
		
	}


	private void assertMertGrammarContainsMonotoneAndInversionAbstractRules(TestIdentifierPair testIdentifierPair) {
		List<String> abstractRuleStringList = new ArrayList<String>();
		abstractRuleStringList.add(FULLY_MONOTONE_RULE_STRING_MOSES);
		abstractRuleStringList.add(FULLY_INVERTED_RULE_STRING_MOSES);
		//abstractRuleStringList.add(MIXED_RULE_STRING_MOSES);
		Assert.assertTrue(fileContainsStrings(getMertGrammarLocation(testIdentifierPair), abstractRuleStringList));
	}
	
	private void assertMertGrammarContainsMonotoneAndInversionAbstractRuleSmoothingRules(TestIdentifierPair testIdentifierPair) {
		List<String> abstractRuleStringList = new ArrayList<String>();
		abstractRuleStringList.add(MONOTONE_RULE_STRING_MOSES_FIRST_SMOOTHED);
		abstractRuleStringList.add(MONOTONE_RULE_STRING_MOSES_SECOND_SMOOTHED);
		abstractRuleStringList.add(MONOTONE_RULE_STRING_MOSES_FULL_SMOOTHED);
		abstractRuleStringList.add(INVERTED_RULE_STRING_MOSES_FIRST_SMOOTHED);
		abstractRuleStringList.add(INVERTED_RULE_STRING_MOSES_SECOND_SMOOTHED);
		abstractRuleStringList.add(INVERTED_RULE_STRING_MOSES_FULL_SMOOTHED);
		//abstractRuleStringList.add(MIXED_RULE_STRING_MOSES);
		Assert.assertTrue(fileContainsStrings(getMertGrammarLocation(testIdentifierPair), abstractRuleStringList));
	}

	//@Test
	public void testHieroPipelineWithReorderingLabelsAbstractRulesOnly() {
		TestIdentifierPair testIdentifierPair = testHieroPipelineWithReorderingLabelsAbstractRulesOnly(TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_FR_EN2, "EnFr",
				TestRerankingSettings.createNoRerankingSettings(),SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN);
		assertMertGrammarContainsMonotoneAndInversionAbstractRules(testIdentifierPair);
		assertMertGrammarContainsMonotoneAndInversionAbstractRuleSmoothingRules(testIdentifierPair);
	}

	// @Test
	public void testHieroPipelineWithReorderingLabelsNoLabelSmoothingAndPureHieroRulesAdded() {
		testHieroPipelineWithReorderingLabelsBasicNoLabelSmoothingAndPureHieroRulesAdded(TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN, "EnFr-PureHieroRulesAdded",
				TestRerankingSettings.createNoRerankingSettings(),SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN);
	}

	// @Test
	public void testHieroPipelineWithReorderingLabelsAndCrunching() {
		testHieroPipelineWithReorderingLabelsBasic(TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN, "EnFr-Crunching", TestRerankingSettings.createCrunchingTestRerankingSettings(),false, false,false,SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN, true,false);
	}

	// @Test
	public void testHieroPipelineWithReorderingLabelsAndNBestListMBR() {
		testHieroPipelineWithReorderingLabelsBasic(TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN, "EnFr-NBestListMBR", TestRerankingSettings.createNBestListMBRRerankingSettings(),false, false,false,SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN, true,false);
	}

	@Test
	public void testHieroPipelineWithHatRefinedReorderingLabelsMert2() {
		testHieroPipelineWithHatRefinedReorderingLabels(TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN, "EnFr", TestRerankingSettings.createNoRerankingSettings(),
			SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN);
	}
	
	//@Test
	public void testHieroPipelineMert2() {
		testHieroPipeline(TUNER_TYPE.MERT_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN, "EnFr", TestRerankingSettings.createNoRerankingSettings(),
			SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN,true);
	}

	// @Test
	public void testHieroPipelineAndHieroPipelineWithReorderingLabelsPro() {
		testHieroPipelineAndHieroPipelineWithReorderingLabels(TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_DE_EN, TestRerankingSettings.createNoRerankingSettings(),false,
			SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_DE_EN);
	}

	@Test
	public void testSAMT() {
		testSAMTPipeline(TUNER_TYPE.MERT_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_FR_EN2, false, TestRerankingSettings.createNoRerankingSettings(),SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN);
	}

	// @Test
	public void testSAMT2() {
		testSAMTPipeline(TUNER_TYPE.MERT_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN, false, TestRerankingSettings.createNoRerankingSettings(),SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN);
	}

	// @Test
	public void testHieroPipelineAndHieroPipelineWithReorderingLabelsEnFr() {
		testHieroPipelineAndHieroPipelineWithReorderingLabels(TUNER_TYPE.MERT_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN, TestRerankingSettings.createNoRerankingSettings(),false,SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN);
	}

	// @Test
	public void testHHieroPipelineWithReorderingLabelsAndReorderingBinaryFeaturesEnFr() {
		System.out.println(System.getProperty("java.vm.name"));
		// testHieroPipelineWithReorderingLabelsAndBinaryReorderingFeatures(true,
		// MT_DATA_LOCATIONS_CONFIG_FILE_PATH2,"EnFr");
	}

	// @Test
	public void testHHieroPipelineWithReorderingLabelsAndReorderingBinaryFeaturesGeEn() {
		testHieroPipelineWithReorderingLabelsAndBinaryReorderingFeatures(TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_DE_EN, "EnGe", TestRerankingSettings.createNoRerankingSettings(),SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_DE_EN);
	}

	@Override
	protected boolean isMosesSystem() {
		return true;
	}

	/*
	 * public static String createBasicHieroWithReorderingConfigFileEnFr() { TestIdentifierPair systemTwo = TestIdentifierPair
	 * .createTestIdentifierPair(MTPipelineHatsHiero.SystemName, MTPipelineHatsHiero.SystemName + "-reorderingLabeled");
	 * 
	 * MTPipelineTestMoses mtPipelineTestMoses = new MTPipelineTestMoses(); MTPipelineTestConfigFileCreater mtPipelineTestConfigFileCreater =
	 * mtPipelineTestMoses .createMTPipelineTestConfigFileCreater(systemTwo, TestReorderingLabelingSettings .createBasicTestReorderingLabelingSettings(), false,
	 * false, 100, MT_DATA_LOCATIONS_CONFIG_FILE_PATH2); mtPipelineTestConfigFileCreater.writeExperimentsConfigFile(); return MT_PIPELINE_TEST_CONFIG_FILE_NAME;
	 * }
	 * 
	 * public static String createBasicHieroWithHatRefinedReorderingConfigFileEnFr() { TestIdentifierPair systemTwo = TestIdentifierPair
	 * .createTestIdentifierPair(MTPipelineHatsHiero.SystemName, MTPipelineHatsHiero.SystemName + "-reorderingLabeled-hatRefined");
	 * 
	 * MTPipelineTestMoses mtPipelineTestMoses = new MTPipelineTestMoses(); MTPipelineTestConfigFileCreater mtPipelineTestConfigFileCreater =
	 * mtPipelineTestMoses .createMTPipelineTestConfigFileCreater( systemTwo, TestReorderingLabelingSettings .createHatRefinedTestReorderingLabelingSettings(),
	 * false, false, 100, MT_DATA_LOCATIONS_CONFIG_FILE_PATH2); mtPipelineTestConfigFileCreater.writeExperimentsConfigFile(); return
	 * MT_PIPELINE_TEST_CONFIG_FILE_NAME; }
	 */
}
