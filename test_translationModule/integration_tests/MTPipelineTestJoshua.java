package integration_tests;

import grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagLabelingSettings;

import java.util.ArrayList;
import java.util.List;

import mt_pipeline.MTPipelineHatsHiero;
import mt_pipeline.mtPipelineTest.MTPipelineTestConfigFileCreater;
import mt_pipeline.mtPipelineTest.SourceAndTargetLanguageAbbreviations;
import mt_pipeline.mtPipelineTest.TestIdentifierPair;
import mt_pipeline.mtPipelineTest.TestLabelingSettings;
import mt_pipeline.mtPipelineTest.TestRerankingSettings;

import org.junit.Assert;
import org.junit.Test;

public class MTPipelineTestJoshua extends MTPipelineTest {

    @Override
    protected MTPipelineTestConfigFileCreater createMTPipelineTestConfigFileCreater(
	    TestIdentifierPair testIdentifierPair,
	    TestLabelingSettings testReorderingLabelingSettings,
	    TestRerankingSettings testRerankingSettings, TUNER_TYPE tunerType,
	    boolean parseTargetFile, int trainDataLength, String testDataLocationsConfigFile,
	    boolean useSoftSyntacticConstraintDecoding,
	    SourceAndTargetLanguageAbbreviations sourceAndTargetLanguageAbbreviations,
	    boolean performTuning,boolean use_rule_application_features) {

	return createMTPipelineTestConfigFileCreater(testIdentifierPair,
		testReorderingLabelingSettings, testRerankingSettings, tunerType, parseTargetFile,
		trainDataLength, testDataLocationsConfigFile, false,
		useSoftSyntacticConstraintDecoding, sourceAndTargetLanguageAbbreviations,
		performTuning,use_rule_application_features);
    }

    //@Test
    public void testHieroPipelineAndHieroPipelineWithReorderingLabelsMert() {
	testHieroPipelineAndHieroPipelineWithReorderingLabels(TUNER_TYPE.MERT_TUNING,
		MT_DATA_LOCATIONS_CONFIG_FILE_DE_EN,
		TestRerankingSettings.createNoRerankingSettings(), false,
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_DE_EN);
    }

    // @Test
    public void testHieroPipelineAndHieroPipelineWithReorderingLabelsMert2() {
	testHieroPipelineAndHieroPipelineWithReorderingLabels(TUNER_TYPE.MERT_TUNING,
		MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN,
		TestRerankingSettings.createNoRerankingSettings(), false,
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN);
    }

    //@Test
    public void testHieroPipelineAndHieroPipelineWithReorderingLabelsMira() {
	testHieroPipelineAndHieroPipelineWithReorderingLabels(TUNER_TYPE.MIRA_TUNING,
		MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN,
		TestRerankingSettings.createNoRerankingSettings(), true,
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN);
    }

    //@Test
    public void testHieroPipelineWithReorderingLabelsMert2() {
	testHieroPipelineWithReorderingLabelsBasic(TUNER_TYPE.MERT_TUNING,
		MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN, "EnFr",
		TestRerankingSettings.createNoRerankingSettings(), true, true, true,
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN, true,false);
    }

    //@Test
    public void testHieroPipelineWithParentRelativeReorderingLabels() {
	testHieroPipelineWithParentRelativeReorderingLabels(TUNER_TYPE.MIRA_TUNING,
		MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN, "EnFr",
		TestRerankingSettings.createNoRerankingSettings(), true, true, true, false, false,
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN, true);
    }

    //@Test
    public void testHieroPipelineChineseEnglish() {
	testHieroPipeline(TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_ChineseEnglish,
		"ZhEn", TestRerankingSettings.createNoRerankingSettings(),
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_ZH_EN, true);
    }

    //@Test
    public void testHieroPipelineChineseEnglishCourseInsideBoundaryTags() {
	testHieroPipelineWithCoarseInsidePhraseBoundaryPosTagsTarget(TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_ChineseEnglish,
		"ZhEn", TestRerankingSettings.createNoRerankingSettings(),
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_ZH_EN, true,true);
    }
  
    
    
    //@Test
    public void testHieroPipelineGermanEnglishHiero() {
	
	testHieroPipeline(TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_DEBUGGING2,
		"DeEn", TestRerankingSettings.createNoRerankingSettings(),
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_DE_EN, true);
    }
    
    @Test
    public void testHieroPipelineGermanEnglishTheisExampleToy() {
	
	
	testHieroPipelineWithReorderingLabelsBasic(TUNER_TYPE.MIRA_TUNING,
		MT_DATA_LOCATIONS_CONFIG_FILE_GermanEnglishThesisToy, "DeEN",
		TestRerankingSettings.createNoRerankingSettings(), true, true, true,
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_DE_EN, true,true);
	
    }
    
    //@Test
    public void testHieroPipelineGermanEnglishTheisExampleToy2(){
	testHieroPipelineWithCoarseInsidePhraseBoundaryPosTagsTarget(TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_DEBUGGING2,
		"DeEn", TestRerankingSettings.createNoRerankingSettings(),
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_DE_EN, true,true);
    }

 
    //@Test
    public void testHieroPipelineGermanEnglishReordering() {
	
	
	testHieroPipelineWithReorderingLabelsBasic(TUNER_TYPE.MIRA_TUNING,
		MT_DATA_LOCATIONS_CONFIG_FILE_DEBUGGING2, "DeEN",
		TestRerankingSettings.createNoRerankingSettings(), true, true, true,
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_DE_EN, true,false);
	
    }

    //@Test
    public void testHieroPipelineWithParentRelativeReorderingLabelsRestrictedLabeling() {
	testHieroPipelineWithParentRelativeReorderingLabels();
    }

    //@Test
    public void testHieroPipelineWithCoarseParentRelativeReorderingLabels() {
	testHieroPipelineWithParentRelativeReorderingLabels(TUNER_TYPE.MIRA_TUNING,
		MT_DATA_LOCATIONS_CONFIG_FILE_FR_EN2, "FrEn",
		TestRerankingSettings.createNoRerankingSettings(), true, true, false, false, true,
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN, true);
    }
    
    //@Test
    public void testHieroPipelineWithCoarsePhraseCentricReorderingLabels() {
	testHieroPipelineWithCoarsePhraseCentricReorderingLabels(TUNER_TYPE.MIRA_TUNING,
		MT_DATA_LOCATIONS_CONFIG_FILE_FR_EN2, "EnFr",
		TestRerankingSettings.createNoRerankingSettings(), true, true, true, SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN, true);
    }


    //@Test
    public void testReorderingLabelsAreReallyInvertedForInvertedRules() {
	TestIdentifierPair testIdentifierPair = testHieroPipelineWithReorderingLabelsBasic(
		TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_FR_EN2, "EnFr-BLAA",
		TestRerankingSettings.createNoRerankingSettings(), true, true, true,
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN, true,false);
	List<String> ruleStringList = new ArrayList<String>();
	ruleStringList
		.add("[X-BITT] ||| [X-ATOMIC,1] rouge [X-ATOMIC,2] ||| [X-ATOMIC,2] red [X-ATOMIC,1]");
	ruleStringList
		.add("[X-BITT] ||| [X-ATOMIC,1] maison [X-ATOMIC,2] ||| [X-ATOMIC,2] house [X-ATOMIC,1]");
	Assert.assertTrue(fileContainsStrings(getMertGrammarLocation(testIdentifierPair),
		ruleStringList));
    }

    /**
     * This test checks whether the correct rules are extracted and indirectly
     * checks that way if the tokenization is done correctly. This test was
     * added after discovering a bug that resulted from using the wrong source
     * abbreviation with the tokenizer.
     */
    //@Test
    public void testCorrectRulesExtractedHATLabeled() {
	TestIdentifierPair testIdentifierPair = testHieroPipelineWithReorderingLabelsBasic(
		TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_DEBUGGING1,
		"EmGe-Debugging1", TestRerankingSettings.createNoRerankingSettings(), true, true,
		true, SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_EN_DE, false,false);
	List<String> ruleStringList = new ArrayList<String>();
	ruleStringList
		.add("[X-MONO] ||| i [X-MONO,1] anti-personnel mines ||| ich [X-MONO,1] antipersonenminen sind |||");
	Assert.assertTrue(fileContainsStrings(getMertGrammarLocation(testIdentifierPair),
		ruleStringList));
    }

    /**
     * This test checks whether the correct rules are extracted and indirectly
     * checks that way if the tokenization is done correctly. This test was
     * added after discovering a bug that resulted from using the wrong source
     * abbreviation with the tokenizer.
     */
    //@Test
    public void testCorrectRulesExtractedBaseline() {
	TestIdentifierPair testIdentifierPair = testHieroPipeline(TUNER_TYPE.MIRA_TUNING,
		MT_DATA_LOCATIONS_CONFIG_FILE_DEBUGGING1, "EnGe-Debugging1",
		TestRerankingSettings.createNoRerankingSettings(),
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_EN_DE, false);
	List<String> ruleStringList = new ArrayList<String>();
	ruleStringList
		.add("[X] ||| i [X,1] anti-personnel mines ||| ich [X,1] antipersonenminen sind |||");
	Assert.assertTrue(fileContainsStrings(getMertGrammarLocation(testIdentifierPair),
		ruleStringList));
    }

    /**
     * This test checks whether the correct rules are extracted and indirectly
     * checks that way if the tokenization is done correctly. This test was
     * added after discovering a bug that resulted from using the wrong source
     * abbreviation with the tokenizer.
     */
    //@Test
    public void testCorrectRulesExtractedParentRelative() {
	TestIdentifierPair testIdentifierPair = testHieroPipelineWithParentRelativeReorderingLabels(
		TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_DEBUGGING1,
		"EnGe-Debugging1", TestRerankingSettings.createNoRerankingSettings(), true, true,
		true, false, false, SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_EN_DE, false);
	List<String> ruleStringList = new ArrayList<String>();
	ruleStringList
		.add("[X-E.F.M.] ||| i [X-E.F.M.,1] anti-personnel mines ||| ich [X-E.F.M.,1] antipersonenminen sind |||");
	Assert.assertTrue(fileContainsStrings(getMertGrammarLocation(testIdentifierPair),
		ruleStringList));
    }

     //@Test
    public void testHieroPipelineWithPhraseCentricPlusParentRelativeReorderingLabels() {
	testHieroPipelineWithPhraseCentricPlusParentRelativeReorderingLabels(
		TUNER_TYPE.MIRA_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN, "EnFr",
		TestRerankingSettings.createNoRerankingSettings(), true, true, true,
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN);
    }

    // @Test
    public void testHieroPipelineWithHATComplexityTypeReorderingLabels() {
	testHieroPipelineWithHATComplexityTypeReorderingLabels(TUNER_TYPE.MIRA_TUNING,
		MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN, "EnFr",
		TestRerankingSettings.createNoRerankingSettings(), true, true, true,
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN);
    }

    //@Test
    public void testHieroPipelineAndHieroPipelineWithReorderingLabelsPro() {
	testHieroPipelineAndHieroPipelineWithReorderingLabels(TUNER_TYPE.PRO_TUNING,
		MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN,
		TestRerankingSettings.createNoRerankingSettings(), false,
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN);
    }

    //@Test
    public void testHieroPipelinePro() {
	testHieroPipeline(TUNER_TYPE.PRO_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_DE_EN,
		"DeEn", TestRerankingSettings.createNoRerankingSettings(),
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_DE_EN, false);
    }

    
    //@Test
    public void testSAMT() {
	testSAMTPipeline(TUNER_TYPE.MERT_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_DE_EN, true,
		TestRerankingSettings.createNoRerankingSettings(),
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_DE_EN);
    }

    // @Test
    public void testSAMT2() {
	testSAMTPipeline(TUNER_TYPE.MERT_TUNING, MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN, false,
		TestRerankingSettings.createNoRerankingSettings(),
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN);
    }

    // @Test
    public void testHieroPipelineAndHieroPipelineWithReorderingLabelsEnFr() {
	testHieroPipelineAndHieroPipelineWithReorderingLabels(TUNER_TYPE.MERT_TUNING,
		MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN,
		TestRerankingSettings.createNoRerankingSettings(), false,
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN);
    }

    // @Test
    public void testHHieroPipelineWithReorderingLabelsAndReorderingBinaryFeaturesEnFr() {
	System.out.println(System.getProperty("java.vm.name"));
	// testHieroPipelineWithReorderingLabelsAndBinaryReorderingFeatures(true,
	// MT_DATA_LOCATIONS_CONFIG_FILE_PATH2,"EnFr");
    }

    // @Test
    public void testHHieroPipelineWithReorderingLabelsAndReorderingBinaryFeaturesGeEn() {
	testHieroPipelineWithReorderingLabelsAndBinaryReorderingFeatures(TUNER_TYPE.PRO_TUNING,
		MT_DATA_LOCATIONS_CONFIG_FILE_DE_EN, "EnGe",
		TestRerankingSettings.createNoRerankingSettings(),
		SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_DE_EN);
    }

    public static String createBasicHieroWithReorderingConfigFileEnFr() {
	TestIdentifierPair systemTwo = TestIdentifierPair.createTestIdentifierPair(
		MTPipelineHatsHiero.SystemName, MTPipelineHatsHiero.SystemName
			+ "-reorderingLabeled");

	MTPipelineTestJoshua mtPipelineTestJoshua = new MTPipelineTestJoshua();
	MTPipelineTestConfigFileCreater mtPipelineTestConfigFileCreater = mtPipelineTestJoshua
		.createMTPipelineTestConfigFileCreater(systemTwo, TestLabelingSettings
			.createBasicTestReorderingLabelingSettings(false, false,BoundaryTagLabelingSettings.createAllEmptyBoundaryTagLabelingSettings()),
			TestRerankingSettings.createNoRerankingSettings(), TUNER_TYPE.MERT_TUNING,
			false, 100, MT_DATA_LOCATIONS_CONFIG_FILE_PATH_FR_EN, false,
			SOURCE_AND_TARGET_LANGUAGE_ABBREVIATIONS_FR_EN, true,false);
	mtPipelineTestConfigFileCreater.writeExperimentsConfigFile();
	return MT_PIPELINE_TEST_CONFIG_FILE_NAME;
    }

    @Override
    protected boolean isMosesSystem() {
	return false;
    }

}
