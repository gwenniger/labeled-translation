package integration_tests;

import java.util.List;

import junit.framework.Assert;
import extended_bitg.EbitgChartBuilderBasic;
import extended_bitg.EbitgChartBuilderProperties;
import extended_bitg.EbitgLexicalizedInference;
import grammarExtraction.chiangGrammarExtraction.ChiangGrammarExtractionConstraints;
import grammarExtraction.chiangGrammarExtraction.ChiangRuleCreater;
import grammarExtraction.chiangGrammarExtraction.HierarchicalPhraseBasedRuleExtractor;
import grammarExtraction.chiangGrammarExtraction.LabelSideSettings;
import grammarExtraction.chiangGrammarExtraction.ReorderLabelingSettings;
import grammarExtraction.grammarExtractionFoundation.LightWeightPhrasePairRuleSet;
import grammarExtraction.grammarExtractionFoundation.SystemIdentity;
import grammarExtraction.samtGrammarExtraction.SAMTGrammarExtractionConstraints;
import grammarExtraction.samtGrammarExtraction.SAMTQuadruple;
import grammarExtraction.samtGrammarExtraction.SAMTRuleExtracter;
import grammarExtraction.translationRuleTrie.WordKeyMappingTable;
import grammarExtraction.translationRules.LightWeightTranslationRule;

import org.junit.Test;

import ccgLabeling.LabelGenerator.UnaryCategoryHandlerType;

import util.InputReading;
import alignment.AlignmentTriple;

public class HierarchicalPhraseBasedRuleExtractorTest {
	private static final int MaxAllowedInferencesPerNode = 100000;
	private static final ChiangGrammarExtractionConstraints chiangGrammarExtractionConstraints = ChiangGrammarExtractionConstraints.createStandardChiangGrammarExtractionConstraints(false);

	private static HierarchicalPhraseBasedRuleExtractor<?> createRuleExtractor(AlignmentTriple alignmentTriple) {
		EbitgChartBuilderBasic chartBuilder = EbitgChartBuilderBasic.createEbitgChartBuilder(alignmentTriple.getSourceSentenceAsString(), alignmentTriple.getTargetSentenceAsString(),
				alignmentTriple.getAlignmentAsString(), MaxAllowedInferencesPerNode, EbitgChartBuilderProperties.USE_GREEDY_NULL_BINDING, EbitgChartBuilderProperties.USE_CACHING);
		chartBuilder.findDerivationsAlignment();
		return HierarchicalPhraseBasedRuleExtractor.createHierarchicalPhraseBasedRulesExtractorChiangPure(chartBuilder, chiangGrammarExtractionConstraints, new ChiangRuleCreater(
				chiangGrammarExtractionConstraints.getReorderLabelingSettings(),chiangGrammarExtractionConstraints.createRuleLabelCreater()));
	}

	private static HierarchicalPhraseBasedRuleExtractor<EbitgLexicalizedInference> createRuleExtractorSAMT(SAMTQuadruple alignmentTriple) {
		SAMTGrammarExtractionConstraints samtGrammarExtractionConstraints = SAMTGrammarExtractionConstraints.createSAMTGrammarExtractionConstraints(SystemIdentity.STANDARD_MAX_SOURCE_AND_TARGET_LENGTH,true, true, true, true,true, true,
				ReorderLabelingSettings.createNoReorderingLabelingSettings(), LabelSideSettings.createLabelSideSettingsBothSides(), UnaryCategoryHandlerType.BOTTOM);
		SAMTRuleExtracter ruleExtracter = SAMTRuleExtracter.createSAMTRuleExtracter(samtGrammarExtractionConstraints, MaxAllowedInferencesPerNode, EbitgChartBuilderProperties.USE_GREEDY_NULL_BINDING,
				EbitgChartBuilderProperties.USE_CACHING);
		return ruleExtracter.createTranslationRuleExtractor(alignmentTriple);

	}

	@Test
	public void testRealAlignment() {
		String sourceString = "after the four years in which schengen has been in operation , this first debate has come not a moment too soon .";
		String targetString = "na vier jaar schengenwerking komt dit eerste debat niets te vroeg .";
		String alignmentString = "0-0 2-1 3-2 4-3 5-3 6-3 7-3 7-4 8-3 9-3 10-3 11-4 12-5 13-6 14-7 16-8 17-8 20-9 21-10 22-11";
		AlignmentTriple alignment = AlignmentTriple.createAlignmentTriple(sourceString, targetString, alignmentString);

		System.out.println("Aligment:" + alignment);

		HierarchicalPhraseBasedRuleExtractor<?> hieararchicalPhraseBasedRuleExtractor = createRuleExtractor(alignment);
		WordKeyMappingTable wordKeyMappingTable = WordKeyMappingTable.createWordKeyMappingTable(alignment, 2, 16);
		List<LightWeightPhrasePairRuleSet> ruleSets = hieararchicalPhraseBasedRuleExtractor.createLightWeightTranslationRuleSets(wordKeyMappingTable);

		for (LightWeightPhrasePairRuleSet ruleSet : ruleSets) {
			for (LightWeightTranslationRule rule : ruleSet.getRules()) {
				System.out.println("Rule: " + rule);

				System.out.println("Source keys: " + rule.getSourceKeys());
				System.out.println("Target keys: " + rule.getTargetKeys());
				Assert.assertTrue (rule.getSourceKeys().size() > 0);
				Assert.assertTrue (rule.getTargetKeys().size() > 0);
			}
		}

	}

	@Test
	public void testRealAlignment2() {
		String sourceString = "je déclare reprise la session du parlement européen qui avait été interrompue le vendredi 17 décembre dernier et je vous renouvelle tous mes vux en espérant que vous avez passé de bonnes vacances .";
		String targetString = "i declare resumed the session of the european parliament adjourned on friday 17 december 1999 , and i would like once again to wish you a happy new year in the hope that you enjoyed a pleasant festive period .";
		String alignmentString = "0-0 1-1 2-2 3-3 4-4 5-5 5-6 7-7 6-8 8-9 9-9 10-9 11-9 12-10 13-11 14-12 15-13 16-13 16-14 17-16 18-17 19-24 24-29 25-30 25-31 26-32 27-33 28-33 29-34 30-35 31-36 20-37 21-37 22-37 23-37 32-37 32-38 33-39";
		AlignmentTriple alignment = AlignmentTriple.createAlignmentTriple(sourceString, targetString, alignmentString);

		System.out.println("Aligment:" + alignment);
		HierarchicalPhraseBasedRuleExtractor<?> ruleExtractor = createRuleExtractor(alignment);
		WordKeyMappingTable wordKeyMappingTable = WordKeyMappingTable.createWordKeyMappingTable(alignment, 2, 16);
		List<LightWeightPhrasePairRuleSet> ruleSets = ruleExtractor.createLightWeightTranslationRuleSets(wordKeyMappingTable);

		for (LightWeightPhrasePairRuleSet ruleSet : ruleSets) {
			for (LightWeightTranslationRule rule : ruleSet.getRules()) {
				System.out.println("Rule: " + rule);

				System.out.println("Source keys: " + rule.getSourceKeys());
				System.out.println("Target keys: " + rule.getTargetKeys());
				Assert.assertTrue (rule.getSourceKeys().size() > 0);
				Assert.assertTrue (rule.getTargetKeys().size() > 0);
			}
		}

	}

	@Test
	public void testRealAlignment3() {
		String sourceString = "les villes tombent alors en ruines et sont incapables de mener à bien les modernisations nécessaires .";
		String targetString = "this is why these cities fall into disrepair and are unable to carry out the necessary renovation works .";
		String alignmentString = "0-3 1-4 2-5 4-6 3-7 5-7 6-8 7-9 8-10 9-11 10-12 10-13 11-13 12-13 13-14 15-15 14-16 14-17 16-18";
		String parseString = "(S (NP (DT this)) (VP (VBZ is) (SBAR (WHADVP (WRB why)) (S (NP (DT these) (NNS cities)) (VP (VP (VBP fall) (PP (IN into) (NP (NN disrepair)))) (CC and) (VP (VBP are) (ADJP (JJ unable) (S (VP (TO to) (VP (VB carry) (PRT (RP out)) (NP (DT the) (JJ necessary) (NN renovation) (NNS works))))))))))) (. .))";
		AlignmentTriple alignment = AlignmentTriple.createAlignmentTriple(sourceString, targetString, alignmentString);
		SAMTQuadruple alignmentQuadruple = SAMTQuadruple.createSAMTQuadruple(sourceString, targetString, alignmentString, parseString);

		System.out.println("Aligment:" + alignment);
		HierarchicalPhraseBasedRuleExtractor<?> ruleExtractor = createRuleExtractorSAMT(alignmentQuadruple);
		WordKeyMappingTable wordKeyMappingTable = WordKeyMappingTable.createWordKeyMappingTable(alignment, 2, 16);
		List<LightWeightPhrasePairRuleSet> ruleSets = ruleExtractor.createLightWeightTranslationRuleSets(wordKeyMappingTable);

		for (LightWeightPhrasePairRuleSet ruleSet : ruleSets) {
			for (LightWeightTranslationRule rule : ruleSet.getRules()) {
				Assert.assertTrue (rule.getSourceKeys().size() > 0);
				Assert.assertTrue (rule.getTargetKeys().size() > 0);
				System.out.println("rule jooshu style: " + rule.getJoshuaRuleRepresentation(wordKeyMappingTable));
			}
		}

	}

	@Test
	public void testArtificialMonotoneAlignment() {
		String sourceString = "the man walks";
		String targetString = "de man loopt";
		String alignmentString = "0-0 1-1 2-2";
		String parseString = "(S (NP (DT the) (NN man)) (VP (VBZ walks))) )";
		AlignmentTriple alignment = AlignmentTriple.createAlignmentTriple(sourceString, targetString, alignmentString);
		SAMTQuadruple alignmentQuadruple = SAMTQuadruple.createSAMTQuadruple(sourceString, targetString, alignmentString, parseString);

		System.out.println("Aligment:" + alignment);
		HierarchicalPhraseBasedRuleExtractor<?> ruleExtractor = createRuleExtractorSAMT(alignmentQuadruple);
		WordKeyMappingTable wordKeyMappingTable = WordKeyMappingTable.createWordKeyMappingTable(alignment, 2, 16);
		List<LightWeightPhrasePairRuleSet> ruleSets = ruleExtractor.createLightWeightTranslationRuleSets(wordKeyMappingTable);

		for (LightWeightPhrasePairRuleSet ruleSet : ruleSets) {
			System.out.println("Rule set:");
			for (LightWeightTranslationRule rule : ruleSet.getRules()) {
				Assert.assertTrue (rule.getSourceKeys().size() > 0);
				Assert.assertTrue (rule.getTargetKeys().size() > 0);
				System.out.println("rule jooshu style: " + rule.getJoshuaRuleRepresentation(wordKeyMappingTable));
			}
		}

	}

	public void testRealFilterAlignment() {
		String sourceString = "notre déclaration des droits est la première que voie ce millénaire .";
		String targetString = sourceString;
		String alignmentString = AlignmentTriple.createMonotoneAlignmentString(InputReading.getWordsFromString(sourceString).size());
		AlignmentTriple alignment = AlignmentTriple.createAlignmentTriple(sourceString, targetString, alignmentString);

		System.out.println("Aligment:" + alignment);
		HierarchicalPhraseBasedRuleExtractor<?> ruleExtractor = createRuleExtractor(alignment);
		WordKeyMappingTable wordKeyMappingTable = WordKeyMappingTable.createWordKeyMappingTable(alignment, 2, 16);
		List<LightWeightPhrasePairRuleSet> ruleSets = ruleExtractor.createLightWeightTranslationRuleSets(wordKeyMappingTable);

		for (LightWeightPhrasePairRuleSet ruleSet : ruleSets) {
			for (LightWeightTranslationRule rule : ruleSet.getRules()) {
				System.out.println("Rule: " + rule);
				System.out.println("Source keys: " + rule.getSourceKeys());
				System.out.println("Target keys: " + rule.getTargetKeys());
				Assert.assertTrue (rule.getSourceKeys().size() > 0);
				Assert.assertTrue (rule.getTargetKeys().size() > 0);
			}
		}

	}

}
