labeledtranslation : Hiero and Hiero-derived grammar extraction, 
extraction of grammars with bilingual labels, extraction of 
grammars with soft constraints, translation pipelines for Joshua and Moses, 
preprocessing and evaluation interfaces.
Copyright (C) <2013>  <Gideon Maillette de Buy Wenniger>
contact : gemdbw AT gmail DOT com  
 
 Short overview:
 
Labeled Translation is a software repository that builds upon the open source 
hatparsing repository for  Visualization, Search and Analysis of Hierarchical 
Translation Equivalence in Machine Translation Data.

Note: The code depends on the hatparsing repository, available from 
https://bitbucket.org/teamwildtreechase/hatparsing/src/master/. 
This repository needs to be cloned separately, and the "src" and "test" folders of this second
repository should be included as additional source folders in the IDEA. 
 
 <About Hatpparsing>
 
 Hatparsing implements a parser that parses word alignments and outputs 
 Hieararchical Alignment Trees (HATs), which are themselves a generalization of 
 Normalized Decomposition Trees (NDTs) used for concisely representing canonical 
 form maximal factorizations of word alignments.
 HATs differ from NDTs in that they make explicit the compositional reordering 
 relations between nested phrase pairs, this information is at most implicitly 
 available in NDTs, while certain information regarding the specifics of internal 
 alignments is simply lost in NDTs.
 Hatparsing explicitly creates and compactly stores all HATs induced by the 
 word alignment as a packed forest (hypergraph).
 
<Grammar Extraction>

Labeled Translation implements a reasonably generic grammar extractor that focuses on 
Hiero and Hiero grammars with labels such as SAMT.
The grammar extractor has two important backbones:
  1. HATParsing is used to create the set of HATs for a word alignment, thereby also 
  representing all phrase pairs. Based on the phrases in the forest of HATs it is also 
  relatively straightforward to create hierarchical phrase pairs (Hiero rules), with or 
  without enrichment by labeling.
  2. The grammar extraction requires computing counts over rules. Unfortunately the amount 
  of rule types observed in Hiero grammar extraction typically goes in the tens of millions.
  A very basic but scalable approach to compute rule counts is to extract rules, write them 
  as lines in a textfile to disk, sort the textfile and merge the multiple tokens of the same 
  type to compute counts and finally based on the counts compute all the count-derived features.
  In Labeled Translation we did not follow this basic approach but instead opted for sorting 
  extracted grammar rules in a compactly represented "double" Trie structure.
  

<Rule Representations>  
  
Before rules are stored in the Trie, 
they are transformed into a list of keys represented as simple Integers. These keys are 
divided into different ranges, so that based on the range a Key falls in we can determine 
directly whether it is: 1. A word, 2. A left-Hand-Side symbol, 3. A first gap symbol,
4. A second gap symbol. Note that this design choice means that the software does not currently 
support grammars with more than 2 gaps, while a generalization to support more gaps 
would not be very complicated. This design choice is mainly an optimization, because it avoids the 
need to map a key back to its referred symbol in many cases where it is enough to know the type of 
the key.

A grammar rule has the form:  

Source-LHS/Target-LHS ||| S1 ... NT1 ... Sm ... NT2 ...  Sn ||| T1 ... NT1 ... Tm ... NT2 ...  Tn  

  or 

Source-LHS/Target-LHS ||| S1 ... NT1 ... Sm ... NT2 ...  Sn ||| T1 ... NT2 ... Tm ... NT1 ...  Tn  

for monotonic or inverted Hiero rules with two gaps. For rules with one gap or without any gaps the 
rules are changed correspondingly.

When translation rules are initially generated based on the chart of HATs they are of the type
"grammarExtraction.translationRules.TranslationRule". This is what we call the "heavy-weight"
format of rule representation. This format allows effective manipulation of rules, such as 
adding another gap to a base rule, which is important for the rule creation during Hiero rule
extraction. For memory efficient storage the format is not effective however, which is why 
we use a second class called "grammarExtraction.translationRules.LightWeightTranslationRule".
This second class stores rules using lists of keys that encode the source- and target side 
symbols of the rules as simple lists of Integers.
Each of the symbols in the list is mapped to a key.
The mapping is stored in a class called "grammarExtraction.translationRuleTrie.WordKeyMappingTable". The class allows 
a translation back and forth between a key and the symbol it stands for.

When storing a LightWeightTranslationRule in a Trie the left-hand-side (LHS) of the rule is split in a source part (Source-LHS) and target part
(Target-LHS), and the source part is combined with the source right-hand-side and the target part
with the target right-hand-side. This produces a source- and a target rule side, and each of the rule 
sides is basically a list of symbols. These rule sides are stored in the class "IntegerKeyRuleRepresentationWithLabel".
To support some other usage patterns on the low level rule representations there is also a class "IntegerKeyRuleRepresentation"
which store the source/target side of a rule, but not the label part. This class is also used in the LightWeightTranslationRule
class, which wraps "TranslationRuleSignatureTriple", that keeps the LHS separate using a "LightWeightRuleLabel" class instance.

To summarize, the rule representation is somewhat involved, the main distinction is between the "TranslationRule" class instances
and the LightWeightTranslationRule instances, whereby the former is designed for easy of rule manipulation and the second for 
simplicity in preparation of minimal memory usage storage in the Trie representation.


<Grammar Rule Trie>

Grammar rules are stored in a "double" Trie structure.
The first Trie stores the source-side of rules, starting from the source-part
of the LHS of the rule. From the root node, the Trie branches out into a list of 
alternatives for this LHS source-part. Each alternative in this list in turn expands 
in a list of alternatives for the first source RHS element, and so on for multiple levels
until the entire source RHS is represented. The leave nodes of the source Trie represent 
the entire source rule side: by moving up the tree from these nodes towards the root, and 
keeping track of the intermediate nodes finished, the entire list of source side elements is
reconstructed. 
But more interestingly, the source Trie leaf nodes also contain a list of target Trie leaf nodes,
corresponding to the target sides that can be produced from the source side. 
This is why it is called a "double Trie": by smartly using the (references to) target Trie leaf nodes
inside the lists of possible target sides for source Trie leaf nodes, it is possibly to efficiently 
recover both the source side and target side given a source- and target Trie leaf node, and have 
a representation that can store both source-to-target and target-to-source patterns very efficiently
within the same shared structure.

Despite the fact that the basic structure is efficient, Java is Unfortunately inherently memory inefficient.
This is due to the fact that objects in (64bit) Java each take 2 bytes of memory (64 bit).
This is the main motivation to use a Integer representation inside Tries, reducing the amount of objects 
as much as possible. But we can still not go around representing Trie nodes as objects, because in each 
Trie nodes we have to keep track of a list of children. This list must also be quickly searchable, so that 
we can efficiently find whether a certain rule side is already present in the source- or target Trie.
To this end we introduce a datastructure called "IntegerSkipList". This is a highly memory efficient skip list 
implementation, that uses a simple list of Integers to represent the skiplist, again minimizing the amount of
created objects by Java, which is huge when using standard datastructures such as the HashMap, making their use 
infeasible given the amount of Trie nodes.

At the root nodes of the source/target Trie we keep lists of source/target side alternatives with associated counts.
These are stored under the object "grammarExtraction.translationRuleTrie.RuleCountsTable" wich wraps two class instances 
of "RuleCountsSubTable" which contains the actual lists of target (or source) sides for labeled/unlabeled source (or target) sides.
Again we have the requirement of fast enough insertion and more importantly: fast finding of a certain target side if it is present in the list
of alternatives. But here there is another requirement, which is that extra memory should be minimized as much as possible, since these 
objects are stored in the leaf nodes of the Trie, which are most frequent in number. Hence, rather than creating another 
IntegerSkipList for each of these leaf nodes, we store the alternatives in a simple list, and use binary search with insert-sort 
to keep the list sorted when adding new items. This is more expensive for insertion than the skiplist, but since the amount of alternatives 
remains typically limited, and retrieval of the existing elements is the most common scenario, it is a reasonable tradeoff.


<Grammar Filtering>

Grammar filtering is implemented by initializing the source Trie of the double <Source,Target> grammar Trie.
The initialization is done by adding rule templates for rules that are usable for source sides observed in the dev/test 
set for which filtering is to be done.The rule templates (also "filter rules") are formed based on the source side of the sentence pairs in 
the dev/test set. They are actual monotone rules with zero, one or two gaps satisfying the Hiero grammar extraction constraints as far as applicable.
These "filter rules" are created by assuming no additional constraints on rule extraction from the word alignments, which amounts to fully monotonic 
word alignments. This means the filter set form a superset of the rules that are needed for selecting the relevant rules from the unfiltered rules during 
grammar extraction. On the other hand it is guaranteed that every rule matching the source side of a filter rule is guaranteed to be usable for translating the 
test/dev set sentences. And secondly it is guaranteed that all usable rules will have a corresponding filter rule that is added to select them as well.
One thing that needs to be taken care of is that the filter rules do not corrupt the counts of the actual "real" rules that are extracted from the training 
data. This is simple to guarantee by assigning the filter rules each a count of 0, so that they do not influence the actual rule counts.
During grammar extraction, for every sentence pair from the training set, the full set of rules satisfying the Hiero extraction constraints is created.
Then when the rules are added to the Trie, first it is made sure they are actually needed for translating the dev/test sentences.
This check is done by checking whether an unlabeled version of the rule source side (added as a filter rule) is present in the source Trie. If so, the rule 
is usable and is added to the Trie. If not, it is discarded.

A note on efficiency. One might wonder whether it is efficient to extract rules that are not needed and will be discarded before adding them to the Trie.
But the question is if the alternative, of carefully selecting what rules to create is actually faster than this filtering approach.
In a multithreaded environment, were we are typically dividing the grammar extraction over many (as much as 16) threads that extract rules in parallel 
for multiple sentence pairs at the same time. It is crucial for performance that the time these threads have to wait for a common resource that is monopolized 
by one Thread is minimized. When the Trie needs to be updated with a new rule type, part of it has to be locked (blocked by one thread) to avoid inconsistent 
updates by multiple threads a the same time. However, this is minimized as part of the Trie design, and therefore most of the time the different grammar 
extraction threads can efficiently extract rules without hindering each other.
Secondly, the alternative of predicting upon generation exactly what rules are required is more complicated and leads to a bigger mix of responsibilities 
between the Trie and the Grammar extractors. This is bad from the point of view of modularity, maintainability and testability of the code.
It would lead to what is known in software engineering as a more "tight coupling". The taken approach gives a more "loose coupling" and a more pipeline-like
structure of the process, which is easier to understand as well. It might be slightly less efficient than guaranteeing that only rules are created that are needed,
but the latter seems to come at a high price in terms of added complexity and reduced modularity of the code.

<Implementation Grammar Extraction with Different Labels>

In order to implement a grammar extractor that introduces different labels, we follow the strategy of extending the EbitgChartBuilder class to produces 
class instances of a class type T extending EbitgLexicalizedInference. T is extended in such a way that it contains the additional required labels.
While this could be implemented differently this chosen design has certain advantages. One advantage is that the rule creator classes that use the 
EbitgChartBuilder to get T instances (phrase pairs plus the label information) automatically have the labels with the phrase pairs.
This keeps the structure modular, and therefore easier to maintain and test. If the labels were to be incorporated from another source at a later state, modularity 
and clarity of the code might suffer.
When making a grammar extractor for a new type of labels using the existing structure, the following main steps should be taken:

1. Create a structure to build a label chart or alternatively a on-demand label creator. Labels are generated based on (optionally) a source and/or target 
input string in combination with any additional information such as POS tags, constituency or dependency parses etc.
As an example we suggest to look at the package "boundaryTagging" in labeled translation, and in particular at the "BoundaryTagger" class. 

2. Combine the label chart or on-demand label creator with an extension of the EbitgChartBuilder. The extended EbitgChartBuilder creates T extending
EbitgLexixalizedInference such that T contains the phrase pair as well as the required label. 
As an example, look at the class "EbitgChartBuilderBoundaryTagged" extends "EbitgChartBuilder" in the package boundaryTagging.
The custom method "createLabelEnrichedInferenceFromLexicalizedInference" produces instances of T= EbitgLexicalizedInferenceBoundaryTagged.
This method is used by the generic method "public T getTightPhrasePair(Span originalSourceSpan)" in the extended class "EbitgChartBuilder",
which is used turn by the method "protected T getNoNullsOnEdgesSatisfyingFirstCompleteInference(Span absoluteSourceSpan)" inside the 
class "grammarExtraction.grammarExtractionFoundation.HeavyWeightRuleExtractor" to get T instances (phrase pairs with labels) for 
arbitrary source spans.

3. Use the thus extended EbitgChartBuilder inside a custom grammar extractor that creates labeled Hiero rules using the custom labels provided by T 
extending EbitgLexixalizedInference.
Staying with our boundary tagged rule example, there is a class 
"grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagLabeledRuleExtracter" extends RuleExtracter which does exactly this.

A few more classes need to be created, analogously to these for the BoundaryTaggeLabeldRule example:

  a. "grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagLabeledRuleGapCreater" :
    A custom rule gap creater is needed. This class takes care of creating the custom labels based on the information in T extending
  EbitgLexixalizedInference (in this example EbitgLexicalizedInferenceBoundaryTagged).

  b. A custom rule creater is required to create the base translation rules, which are further extended by adding gaps. 
      "grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagLabeledRuleCreater" is an example of such a custom rule creater

  c. A custom RuleExtracterThreadFactoryCreater is required, in this example  
     "grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagLabeledRuleExtracterThreadFactoryCreater"
     This RuleExtracterThreadFactoryCreater produces an ExtractorThreadFactory which in turn produces ExtracterThread instances of some 
     type T extends "BareTranslationRuleExtracterThread<?>".
     This is a bit complicated, but the idea is that you have to make an object that knows how to make a thread factory for 
     either source or target filtered rule extraction, and to combine this in one place we have the   RuleExtracterThreadFactoryCreater .

  d. BoundaryTagLabeledRuleExtracter extends RuleExtracter - this class takes care of creating an emumeration of input strings needed to extract rules, by the method
     "createInputStringEnumeration"
     The method "createTranslationRuleExtractor"  which creates a "HierarchicalPhraseBasedRuleExtractor<EbitgLexicalizedInferenceBoundaryTagged>" 
     (BoundaryTagPentuple inputType) which implements the abstract method 
     "public abstract HierarchicalPhraseBasedRuleExtractor<T> createTranslationRuleExtractor(InputType input)"
     that creates a  "HierarchicalPhraseBasedRuleExtractor<T>" for a certain input type, where the input type bundles all the required 
     input strings (Source,Target,Alignment) and optionally more extra strings such as a sourceTagsString and targetTagsString in case of "BoundaryTagPentuple".

  e. Some Rule Creater class, in the example case this is "grammarExtraction.boundaryTagLabeledGrammarExtraction.BoundaryTagLabeledRuleCreater".
     This rule creater creates basic rules and also knows how to create rule gaps to generalize those rules to Hiero rules with gaps.
     It implements the interface "TranslationRuleCreater<T extends EbitgLexicalizedInference>" and knows how to create rules of different types.     
     
This list is not complete, but gives some idea of what infrastructure is needed to build new rules with a different labeling scheme within the current architecture.


4. Finally "grammarExtraction.chiangGrammarExtraction.MultiThreadGrammarExtractorTestFiltered" should be adapted to use the new grammar extractor.

There are some more higher upstream things that need to be changed and/or new classes that must be created, but these are the main things to get started.
Once the new labeling scheme is fully added to the existing structure, grammars for decoding that can be used in a strict or soft matching setting can 
be extracted in principle without additional changes using the shared generic structure.

A note on the grammars extracted: It may be desired to make  a grammar extractor that uses the unlabeled versions of the rules, but adds some additional features 
based on the labeled versions. To this end, the GrammarWriter class should be adapted and/or classes it refers to in order to
1. Compute the features based on the rule-count statistics for the labeled rules captured in the Trie.
2. Combine the features with the unlabeled Hiero rule representation (conceptually "stripping the labels") before writing out the rules.

Note that something very similar to this is already done in GrammarWriter for the case of canonical labeled rules that are used for soft matching which typically
1. Use the canonical labeled rule version of Hiero rules
2. Uses as features for these rule versions the Hiero rule features themselves, so that the created grammar is really weakly equivalent to Hiero, 
except for soft labeling 
constraints.










 

 

